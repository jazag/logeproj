﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

/// <summary> Classe clg_Connection </summary>
public class clg_Connection
{
    Hashtable m_htab_Compteurs = new Hashtable();
    //Déclaration des types énumérés
    public enum E_EtatConnexion
    {
        Fermee = 0,
        Ouverte = 1
    }

    //Crée l'instance de l'objet connexion
    private clg_ConnexionBD withEventsField_obj_Connection;

    internal clg_ConnexionBD obj_Connection
    {
        get { return withEventsField_obj_Connection; }
        set
        {
            if (withEventsField_obj_Connection != null)
            {
                withEventsField_obj_Connection.TypeAccesChange -= obj_Connection_TypeAccesChange;
            }
            withEventsField_obj_Connection = value;
            if (withEventsField_obj_Connection != null)
            {
                withEventsField_obj_Connection.TypeAccesChange += obj_Connection_TypeAccesChange;
            }
        }
    }

    public event ChangeEtatConnexionEventHandler ChangeEtatConnexion;
    public delegate void ChangeEtatConnexionEventHandler(object Sender, bool pLecteurSeule);
    internal clg_ConnexionBD.E_TypeAcces c_EtatPourTimer = clg_ConnexionBD.E_TypeAcces.Inaccessible;

    public clg_Connection(ref clg_Connecteur pConnecteur)
    {
        try
        {
            obj_Connection = new clg_ConnexionBD(pConnecteur);
        }
        catch (Exception l_err)
        {
            throw new Exception(l_err.Message);
        }
    }

    public bool LectureSeule
    {
        get
        {
            if ((obj_Connection != null))
            {
                if (obj_Connection.TypeAcces == clg_ConnexionBD.E_TypeAcces.EnLectureEcriture)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
    }

    public clg_Connection(string pNomHote, int pNumpPort)
    {

    }


    public clg_Connection()
    {
    }

    //Destructeur
    public void Dispose()
    {
        try
        {
            //Fermeture de la base
            Ferme();
            //Destruction de la référence à la connexion
            obj_Connection = null;
            m_htab_Compteurs.Clear();
        }
        catch (Exception l_err)
        {
            throw new Exception(l_err.Message);
        }
    }

    //Propriété qui renvoie le chemin de la base
    public string CheminBase
    {
        get
        {
            string functionReturnValue = null;
            try
            {
                string l_Chaine = null;
                string l_Chaine2 = null;
                string l_Chaine3 = null;
                int l_pos = 0;

                l_Chaine = obj_Connection.ConnectionString;
                l_Chaine2 = "Data Source=";
                l_pos = l_Chaine.IndexOf(l_Chaine2);
                l_Chaine3 = l_Chaine.Substring(l_pos + l_Chaine2.Length - 1);
                l_pos = l_Chaine3.IndexOf(";");
                if (l_pos > 0)
                {
                    functionReturnValue = l_Chaine3.Substring(0, l_pos);
                }
                else
                {
                    functionReturnValue = l_Chaine3;
                }

            }
            catch (Exception l_err)
            {
                throw new Exception(l_err.Message);
            }
            return functionReturnValue;
        }
    }

    //Méthode qui ferme la connexion à la base
    public void Ferme()
    {
        try
        {
            //Si la connexion est ouverte
            if (obj_Connection.State == 1)
            {
                obj_Connection.Close();
            }
        }
        catch (Exception l_err)
        {
            throw new Exception(l_err.Message);
        }
    }

    [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
    public string ChaineConnexion
    {
        get
        {
            string functionReturnValue = null;
            try
            {
                functionReturnValue = obj_Connection.ConnectionString;
            }
            catch (Exception l_err)
            {
                throw new Exception(l_err.Message);
            }
            return functionReturnValue;
        }
        set
        {
            try
            {
                obj_Connection.ConnectionString = value;
            }
            catch (Exception l_err)
            {
                throw new Exception(l_err.Message);
            }
        }
    }

    //Public Sub Ouvre(Optional ByRef pTraces As DLL_Utilitaires.Trace = Nothing)
    public void Ouvre()
    {
        try
        {
            //Si la connexion BD est déjà ouverte
            if (obj_Connection.State == 1)
            {
                //Ferme la connexion
                obj_Connection.Close();
            }
            obj_Connection.Ouvre();
        }
        catch (Exception l_err)
        {
            throw new Exception(l_err.Message);
        }
    }

    //Propriété en lecture seule qui renvoi l'état de la connexion
    public E_EtatConnexion Etat
    {
        get
        {
            E_EtatConnexion functionReturnValue = default(E_EtatConnexion);
            try
            {
                if (obj_Connection.State == 1)
                {
                    functionReturnValue = E_EtatConnexion.Ouverte;
                }
                else
                {
                    functionReturnValue = E_EtatConnexion.Fermee;
                }
            }
            catch (Exception l_err)
            {
                throw new Exception(l_err.Message);
            }
            return functionReturnValue;
        }
    }

    ////Propriété qui renvoi la connexion ADO soujacente
    //public ADODB.Connection GetADODBConnection()
    //{
    //    try
    //    {
    //        return ((clg_ConnecteurADODB)obj_Connection.c_Connecteur).ConnexionADODB;
    //    }
    //    catch (Exception l_err)
    //    {
    //        throw new Exception(l_err.Message);
    //    }
    //}

    public clg_ConnexionBD GetObjConnexion
    {
        get { return obj_Connection; }
    }

    private void obj_Connection_TypeAccesChange(object Sender, clg_ConnexionBD.E_TypeAcces pNouveauTypeAcces)
    {
        if (ChangeEtatConnexion != null)
        {
            ChangeEtatConnexion(this, LectureSeule);
        }
    }
}