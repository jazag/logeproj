﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

/// <summary> Classe JeuEnregistrement </summary>
public class clg_JeuEnregistrement
{
    public enum Etat : int
    {
        Ouvert = 1,
        Ferme = 0
    }

    clg_ConnexionBD c_Connexion;
    clg_ResultatRequete c_ResultatRequete;
    Etat c_Etat = Etat.Ferme;

    /// <summary> Internal constructeur </summary>
    internal clg_JeuEnregistrement(clg_ConnexionBD pConnexion, clg_ResultatRequete pResultatRequete)
    {
        try
        {
            c_Connexion = pConnexion;
            c_ResultatRequete = pResultatRequete;
            c_Etat = Etat.Ouvert;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    /// <summary> Accesseur de la propriete ResultatRequete </summary>
    public clg_ResultatRequete ResultatRequete
    {
        get { return c_ResultatRequete; }
    }

    /// <summary> Accesseur de la propriete State </summary>
    public int State
    {
        get { return (int)c_Etat; }
    }

    /// <summary> Accesseur de la propriete NombreChamps </summary>
    public int NombreChamps
    {
        get
        {
            try
            {
                return c_ResultatRequete.c_tabChamp.Length;
            }
            catch (Exception ex)
            {
                //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
                throw;
            }
        }
    }

    /// <summary> Accesseur de la propriete ActiveConnexion </summary>
    public clg_ConnexionBD ActiveConnexion
    {
        get
        {
            try
            {
                return c_Connexion;
            }
            catch (Exception ex)
            {
                //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
                throw;
            }
        }
    }

    /// <summary> Methode Close </summary>
    public void Close()
    {
        try
        {
            c_ResultatRequete = null;
            c_Etat = Etat.Ferme;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    /// <summary> Accesseur de la propriete NombreLigne </summary>
    public int NombreLignes
    {
        get
        {
            try
            {
                return c_ResultatRequete.c_tabDonnee.Count;
            }
            catch (Exception ex)
            {
                //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
                throw;
            }
        }
    }

    /// <summary> Accesseur de la propriete TempsExecution </summary>
    public int TempsExecution
    {
        get
        {
            try
            {
                return c_ResultatRequete.c_TempsExecution;
            }
            catch (Exception ex)
            {
                //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
                throw;
            }
        }
    }

    /// <summary> Fonction Donnee </summary>
    /// <param name="pIndiceLigne"> Indice de la ligne de la donnee a recuperer </param>
    /// <param name="pIndiceColonne"> Indice de la colonne de la donnee a recuperer </param>
    /// <returns> Donnee de type chaine </returns>
    public string Donnee(int pIndiceLigne, int pIndiceColonne)
    {
        try
        {
            Array l_tab = (Array) c_ResultatRequete.c_tabDonnee[pIndiceLigne];
            return l_tab.GetValue(pIndiceColonne).ToString();
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    /// <summary> Fonction NomChamp </summary>
    /// <param name="pIndiceChamp"> Indice du champ dont on veut recuperer le nom </param>
    /// <returns> Nom du champ de type chaine </returns>
    public string NomChamp(int pIndiceChamp)
    {
        try
        {
            return c_ResultatRequete.c_tabChamp[pIndiceChamp].c_Nom;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    /// <summary> Fonction TypeChamp </summary>
    /// <param name="pIndiceChamp"> Indice du champ dont on veut recuperer le type </param>
    /// <returns> Type du champ de type chaine </returns>
    public string TypeChamp(int pIndiceChamp)
    {
        try
        {
            return c_ResultatRequete.c_tabChamp[pIndiceChamp].c_Type;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }
}