﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

/// <summary> Classe clg_ConnexionBD </summary>
public class clg_ConnexionBD
{
    public enum E_TypeAcces
    {
        Inaccessible = -1,
        EnLectureSeule = 0,
        EnLectureEcriture = 1
    }

    public clg_Connecteur c_Connecteur;
    E_TypeAcces c_TypeAcces = E_TypeAcces.Inaccessible;
    internal bool c_ModifCompteurs = false;
    static List<string> c_Liste_Ordres = new List<string>();
    public bool c_TransacEnCours = false;
    public event TypeAccesChangeEventHandler TypeAccesChange;
    public delegate void TypeAccesChangeEventHandler(object Sender, E_TypeAcces pNouveauTypeAcces);

    public clg_ConnexionBD(clg_Connecteur pConnecteur)
    {
        c_Connecteur = pConnecteur;
    }

    internal void setTypeAcces(E_TypeAcces pVal)
    {
        if (c_TypeAcces != pVal)
        {
            c_TypeAcces = pVal;
            if (TypeAccesChange != null)
            {
                TypeAccesChange(this, c_TypeAcces);
            }
        }
    }

    public E_TypeAcces TypeAcces
    {
        get { return c_TypeAcces; }
    }

    public bool TransacEnCours
    {
        get { return c_TransacEnCours; }
    }

    public void DemarrerTransaction(int pTimeOutAttenteReponse = 120000)
    {
        try
        {
            c_TransacEnCours = c_Connecteur.DemarrerTransaction()==1;
        }
        catch (Exception ex)
        {
            c_TransacEnCours = false;
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    internal void DetruitObjets()
    {
        try
        {
            if ((c_Connecteur != null))
            {
                try
                {
                    c_Connecteur.FermeConnexion();
                }
                catch (Exception ex)
                {
                    //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
                }
                finally
                {
                    c_Connecteur = null;
                }
            }
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
        }
    }

    public bool ModifCompteurs
    {
        get { return c_ModifCompteurs; }
        set { c_ModifCompteurs = value; }
    }

    public int State
    {
        get { return c_Connecteur.Etat; }
    }

    public bool Ouvre()
    {
        try
        {
            c_Connecteur.EtabliConnexion();
            c_TypeAcces = E_TypeAcces.EnLectureEcriture;
            return true;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    public void Close()
    {
        try
        {
            c_Connecteur.FermeConnexion();
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    public clg_JeuEnregistrement ExecuteSELECT(string pOrdreSQL, ref string pErr)
    {
        //// Enlève le point-virgule et limite les données chargées à 30
        //String ordreSQL = pOrdreSQL.TrimEnd(';');
        //ordreSQL = ordreSQL + " LIMIT 30";

        try
        {
            clg_JeuEnregistrement l_JeuEnregistrement = default(clg_JeuEnregistrement);
            l_JeuEnregistrement = new clg_JeuEnregistrement(this, c_Connecteur.ResultatSELECT(pOrdreSQL));
            return l_JeuEnregistrement;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    public clg_JeuEnregistrement ExecuteSELECT(string pOrdreSQL)
    {

        try
        {
            clg_JeuEnregistrement l_JeuEnregistrement = default(clg_JeuEnregistrement);
            l_JeuEnregistrement = new clg_JeuEnregistrement(this, c_Connecteur.ResultatSELECT(pOrdreSQL));
            return l_JeuEnregistrement;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            throw;
        }
    }

    public string ConnectionString
    {
        get { return c_Connecteur.ChaineConnexion; }
        //Throw New Exception("La propriété ConnectionString est en lecture seule")
        set { c_Connecteur.ChaineConnexion = value; }
    }

    public clg_ResultatCmdSQL ExecCmdSansSync(string pOrdreSQL)
    {
        clg_ResultatCmdSQL l_ResultatCmdSQL = default(clg_ResultatCmdSQL);
        try
        {
            l_ResultatCmdSQL = c_Connecteur.ResultatCmdSQL(pOrdreSQL);
            return l_ResultatCmdSQL;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            return null;
        }
    }

    public clg_ResultatCmdSQL ExecCmdSansSync(string pOrdreSQL, List<string> pParams, List<object> pValParams)
    {
        clg_ResultatCmdSQL l_ResultatCmdSQL = default(clg_ResultatCmdSQL);
        try
        {
            l_ResultatCmdSQL = c_Connecteur.ResultatCmdSQL(pOrdreSQL, pParams, pValParams);
            return l_ResultatCmdSQL;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            return null;
        }
    }

    public void Ferme()
    {
        Close();
    }
}