﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

/// <summary> Classe abstraite clg_Connecteur </summary>
public abstract class clg_Connecteur
{
    public abstract string ChaineConnexion { get; set; }

    public abstract void EtabliConnexion();

    public abstract clg_ResultatRequete ResultatSELECT(string pRequeteSQL);

    public abstract clg_ResultatCmdSQL ResultatCmdSQL(string pRequeteSQL);

    public abstract clg_ResultatCmdSQL ResultatCmdSQL(string pRequeteSQL, List<string> pParams, List<object> pValParams);

    public abstract void FermeConnexion();

    public abstract void DetruitObjets();

    public abstract int Etat { get; }

    public abstract int DemarrerTransaction();

    public abstract string TerminerTransaction(bool pCommit);
}