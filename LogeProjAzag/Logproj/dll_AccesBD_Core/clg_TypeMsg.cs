﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

[Serializable()]
public class clg_ResultatCmdSQL
{
    public int c_TempsExecution;
    public int c_NbLignesMAJ;
    public string c_MsgErr;
    public clg_ResultatCmdSQL(int pNbLignesMAJ, int pTempsExecution, string pMsgErr)
    {
        c_TempsExecution = pTempsExecution;
        c_NbLignesMAJ = pNbLignesMAJ;
        c_MsgErr = pMsgErr;
    }
}

[Serializable()]
public class clg_ResultatTransfertCmdSQL
{
    public clg_ResultatCmdSQL c_ResultatCmdSQL;
    public int c_TempsExecution;

    public bool c_Erreur;
    public clg_ResultatTransfertCmdSQL(clg_ResultatCmdSQL pResultatCmdSQL, int pTempsExecution, bool pErreur)
    {
        c_ResultatCmdSQL = pResultatCmdSQL;
        c_TempsExecution = pTempsExecution;
        c_Erreur = pErreur;
    }
}

[Serializable()]
public class clg_ResultatRequete
{
    public ArrayList c_tabDonnee;
    public clg_Champ[] c_tabChamp;
    public int c_TempsExecution;
    public bool c_Jointure = false;

    public string c_MessageErreur;
    public clg_ResultatRequete(ArrayList ptabDonnee, clg_Champ[] ptabChamp, int pTempsExecution, bool pJointure, string pMsgErreur)
    {
        c_tabDonnee = ptabDonnee;
        c_tabChamp = ptabChamp;
        c_TempsExecution = pTempsExecution;
        c_Jointure = pJointure;
        c_MessageErreur = pMsgErreur;
    }
}

[Serializable()]
public class clg_Champ
{
    public string c_Nom;
    public string c_Type;
    public string c_NomTable;
    public bool c_KeyPrimaire;
    public string c_ChampOrigine;
}