﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Npgsql;

public abstract class clg_ConnecteurNPGSQL : clg_Connecteur
{
    NpgsqlConnection c_Cnn;
    NpgsqlTransaction c_Transaction;

    public clg_ConnecteurNPGSQL(string pChaineConnexion)
    {
        c_Cnn = new Npgsql.NpgsqlConnection();
        c_Cnn.ConnectionString = pChaineConnexion;
    }

    public override string ChaineConnexion
    {
        get { return c_Cnn.ConnectionString; }
        set { c_Cnn.ConnectionString = value; }
    }

    public override void EtabliConnexion()
    {
        c_Cnn.Open();
    }

    public override int DemarrerTransaction()
    {
        try
        {
            c_Transaction = c_Cnn.BeginTransaction();
            return 1;
        }
        catch (Exception ex)
        {
            c_Transaction = null;
            return 0;
        }
    }

    public override string TerminerTransaction(bool pCommit)
    {
        try
        {
            if (pCommit)
                c_Transaction.Commit();
            else
                c_Transaction.Rollback();
            return "OK";
        }
        catch (Exception ex)
        {
            c_Transaction = null;
            return ex.Message;
        }
    }

    public override void FermeConnexion()
    {
        try
        {
            c_Cnn.Close();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public override void DetruitObjets()
    {
        try
        {
            if ((c_Cnn != null))
            {
                try
                {
                    c_Cnn.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        catch (Exception ex)
        {

        }
        finally
        {
            c_Cnn = null;
        }
    }

    public override int Etat
    {
        get
        {
            //Commentable
            try
            {
                return (int)c_Cnn.State;
                //Commentable
            }
            catch (Exception ex)
            {
                return -1;
            }
            //Commentable
        }
    }

    public override clg_ResultatRequete ResultatSELECT(string pRequeteSQL)
    {
        int l_tps1 = 0;
        int l_Tps2 = 0;
        l_tps1 = Environment.TickCount;
        clg_Champ[] l_tabNomCol = null;

        NpgsqlCommand l_cmd = c_Cnn.CreateCommand();
        l_cmd.CommandText = pRequeteSQL;

        NpgsqlDataReader l_Reader = l_cmd.ExecuteReader();

        ArrayList l_Lignes = new ArrayList();
        int i = 0;
        l_tabNomCol = new clg_Champ[l_Reader.FieldCount];
        for (i = 0; i <= l_Reader.FieldCount - 1; i++)
        {
            clg_Champ l_Champ = new clg_Champ();
            l_Champ.c_Nom = l_Reader.GetName(i);
            l_Champ.c_Type = l_Reader.GetFieldType(i).ToString();
            l_tabNomCol[i] = l_Champ;
        }
        while (l_Reader.Read())
        {
            object[] l_cols = new object[l_Reader.FieldCount];
            for (i = 0; i <= l_Reader.FieldCount - 1; i++)
            {
                l_cols[i] = l_Reader.GetValue(i);
            }
            l_Lignes.Add(l_cols);
        }
        l_Reader.Close();
        l_Tps2 = Environment.TickCount;
        clg_ResultatRequete l_ResultatRequete = new clg_ResultatRequete(l_Lignes, l_tabNomCol, l_Tps2 - l_tps1, false, "");
        return l_ResultatRequete;
    }

    public override clg_ResultatCmdSQL ResultatCmdSQL(string pRequeteSQL)
    {
        //Commentable
        try
        {
            NpgsqlCommand l_cmd = new NpgsqlCommand();
            l_cmd.Connection = c_Cnn;
            l_cmd.CommandText = pRequeteSQL;
            int l_temps1 = 0;
            int l_temps2 = 0;
            object l_NbLignesMAJ = 0;
            string l_MsgErreur = "";
            l_temps1 = Environment.TickCount;
            try
            {
                l_NbLignesMAJ = l_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //clg_GereTrace.TraceFonctionnement(ex.Source & "." & ex.TargetSite.ToString, ex.Message & " " & pRequeteSQL.ToString) 'Commentable
                l_NbLignesMAJ = -1;
                l_MsgErreur = ex.Message;
            }
            finally
            {
                l_temps2 = Environment.TickCount;
            }
            clg_ResultatCmdSQL l_ResultatCmdSQL = new clg_ResultatCmdSQL((int)l_NbLignesMAJ, l_temps2 - l_temps1, l_MsgErreur);
            return l_ResultatCmdSQL;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            return null;
        }
    }

    public override clg_ResultatCmdSQL ResultatCmdSQL(string pRequeteSQL, List<string> pParams, List<object> pValParams)
    {
        //Commentable
        try
        {
            NpgsqlCommand l_cmd = new NpgsqlCommand();
            l_cmd.Connection = c_Cnn;
            l_cmd.CommandText = pRequeteSQL;

            //Precision des champs numeriques (en dur pour l'instant)
            byte[] l_Precisions = new byte[pValParams.Count];

            //Creation des parametres de la requete
            for (int i = 0; i < pValParams.Count; i++)
                CreeParametre(ref l_cmd, pParams[i], pValParams[i], l_Precisions[i]);

            l_cmd.CommandText = pRequeteSQL;
            int l_temps1 = 0;
            int l_temps2 = 0;
            object l_NbLignesMAJ = 0;
            string l_MsgErreur = "";
            l_temps1 = Environment.TickCount;
            try
            {
                l_NbLignesMAJ = l_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //clg_GereTrace.TraceFonctionnement(ex.Source & "." & ex.TargetSite.ToString, ex.Message & " " & pRequeteSQL.ToString) 'Commentable
                l_NbLignesMAJ = -1;
                l_MsgErreur = ex.Message;
            }
            finally
            {
                l_temps2 = Environment.TickCount;
            }
            clg_ResultatCmdSQL l_ResultatCmdSQL = new clg_ResultatCmdSQL((int)l_NbLignesMAJ, l_temps2 - l_temps1, l_MsgErreur);
            return l_ResultatCmdSQL;
        }
        catch (Exception ex)
        {
            //ICEDA.Program.Log.Ajouter(dll_Log.E_NiveauTrace.Error, ex.Message);
            return null;
        }
    }

    private void CreeParametre(ref NpgsqlCommand pCmd, string pNomParam, object pValeur, byte pPrecision)
    {
       NpgsqlParameter l_Param = pCmd.CreateParameter();
       l_Param.Direction = ParameterDirection.Input;
       l_Param.Precision = pPrecision;
       l_Param.ParameterName = pNomParam;

        string l_Type = "";
        if (pValeur != null)
            l_Type = pValeur.GetType().Name;

        switch (l_Type)
        {
            case "String":
                l_Param.DbType = DbType.String;
                break;
            case "Decimal":
                l_Param.DbType = DbType.Decimal;
                break;
            case "Single":
                l_Param.DbType = DbType.Single;
                break;
            case "int":
                l_Param.DbType = DbType.Int32;
                break;
            case "DateTime":
                l_Param.DbType = DbType.DateTime;
                break;
            case "Boolean":
                l_Param.DbType = DbType.Boolean;
                break;
            case "Int16":
                l_Param.DbType = DbType.Int16;
                break;
            case "Int32":
                l_Param.DbType = DbType.Int32;
                break;
            case "Int64":
                l_Param.DbType = DbType.Int64;
                break;
            case "Double":
                l_Param.DbType = DbType.Double;
                break;
            default:
                l_Param.DbType = DbType.Object;
                break;
        }

        if (pValeur == null)
            l_Param.Value = DBNull.Value;
        else
            l_Param.Value = pValeur;

        pCmd.Parameters.Add(l_Param);
    }
}

public class clg_ConnecteurNPGSQL_PostGre : clg_ConnecteurNPGSQL
{
    public clg_ConnecteurNPGSQL_PostGre(string pServeur, string pNomBase, string pUtilisateur, string pMotDePasse, string pPort)
        : base(String.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};", pServeur, pPort, pUtilisateur, pMotDePasse, pNomBase))
    {
    }
}