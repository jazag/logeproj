﻿using System;
using System.Collections.Generic;
using System.Text;

namespace clg_ReflexionV3_Core
{
    /// <summary> Classe Controleur Base herite par tous les controleurs de tables </summary>
    public abstract class clg_ControleurBase
    {
        public abstract void Update(clg_ObjetBase pObjet);
        public abstract void Insert(clg_ObjetBase pObjet);
        public abstract void Delete(clg_ObjetBase pObjet);

        private Dictionary<Int64, clg_ObjetBase> c_DicUpdate;
        private Dictionary<Int64, clg_ObjetBase> c_DicInsert;
        private Dictionary<Int64, clg_ObjetBase> c_DicDelete;

        /// <summary> Constructeur de la classe </summary>
        public clg_ControleurBase()
        {
            c_DicUpdate = new Dictionary<Int64, clg_ObjetBase>();
            c_DicInsert = new Dictionary<Int64, clg_ObjetBase>();
            c_DicDelete = new Dictionary<Int64, clg_ObjetBase>();
        }

        /// <summary> Methode de suppression en base de donnees de tous les objets listes a supprimer</summary>
        public void Delete_BASE()
        {
            foreach (Int64 cle in c_DicDelete.Keys)
            {
                //verifie si la cle est dans les updates et les inserts
                if (c_DicUpdate.ContainsKey(cle))
                {
                    c_DicUpdate.Remove(cle);
                }
                if (c_DicInsert.ContainsKey(cle))
                {
                    c_DicInsert.Remove(cle);
                }

                clg_ObjetBase l_table = c_DicDelete[cle];
                Delete(l_table);
            }
            c_DicDelete.Clear();
        }

        /// <summary> Methode d'insertion et de maj en base de donnees de tous les objets listes a inserer et a maj</summary>
        public void MAJ_AJOUT_BASE()
        {
            if (c_DicInsert.Count > 0 || c_DicUpdate.Count > 0)
            {
                foreach (Int64 cle in c_DicInsert.Keys)
                {
                    //verifie si la cle est ds les delete
                    if (c_DicDelete.ContainsKey(cle) == false)
                    {
                        clg_ObjetBase l_table = c_DicInsert[cle];
                        Insert(l_table);
                    }
                }
                c_DicInsert.Clear();

                foreach (Int64 cle in c_DicUpdate.Keys)
                {
                    clg_ObjetBase l_table = c_DicUpdate[cle];
                    //verifie si la cle est ds les delete
                    if (c_DicDelete.ContainsKey(cle) == false)
                    {
                        Update(l_table);
                    }
                }
                c_DicUpdate.Clear();
            }
        }

        /// <summary> Suppression de tous les objets des listes AAjouter, AMAJ et ASupprimer</summary>
        public void Annule_MAJ_Base()
        {
            c_DicInsert.Clear();
            c_DicUpdate.Clear();
            c_DicDelete.Clear();
        }

        /// <summary> Methode d'ajout d'un objet dans la liste des objets a MAJ </summary>
        public void AjouteUpdate(Int64 pCle, clg_ObjetBase ptable)
        {
            if (c_DicUpdate.ContainsKey(pCle) == false)
                c_DicUpdate.Add(pCle, ptable);
        }

        /// <summary> Methode d'ajout d'un objet dans la liste des objets a Inserer </summary>
        public void AjouteInsert(Int64 pCle, clg_ObjetBase ptable)
        {
            if (c_DicInsert.ContainsKey(pCle) == false)
                c_DicInsert.Add(pCle, ptable);
        }

        /// <summary> Methode d'ajout d'un objet dans la liste des objets a Supprimer </summary>
        public void AjouteDelete(Int64 pCle, clg_ObjetBase ptable)
        {
            if (c_DicDelete.ContainsKey(pCle) == false)
                c_DicDelete.Add(pCle, ptable);
        }

        /// <summary> Methode de suppression d'un objet de la liste des objets a MAJ </summary>
        public void AnnuleUpdate(Int64 pCle)
        {
            if (c_DicUpdate.ContainsKey(pCle))
                c_DicUpdate.Remove(pCle);
        }

        /// <summary> Methode de suppression d'un objet de la liste des objets a Inserer </summary>
        public void AnnuleInsert(Int64 pCle)
        {
            if (c_DicInsert.ContainsKey(pCle))
                c_DicInsert.Remove(pCle);
        }

        /// <summary> Methode de suppression d'un objet de la liste des objets a Supprimer </summary>
        public void AnnuleDelete(Int64 pCle)
        {
            if (c_DicDelete.ContainsKey(pCle))
                c_DicDelete.Remove(pCle);
        }
    }
}