﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace clg_ReflexionV3_Core
{
    /// <summary> Classe Table Base heritee par toutes les classes Modele </summary>
    public abstract class clg_ObjetBase
    {
        protected string c_SeparateurCSV = ((char)31).ToString();

        private object c_ID;

        private clg_ObjetBase c_Clone;
        protected Dictionary<string, object> c_dicReferences;
        protected clg_ModeleBase c_ModeleBase;
        protected bool c_EstReference;

        /// <summary> Constructeur pur une cle de type long </summary>
        public clg_ObjetBase(clg_ModeleBase pModeleBase, Int64 pID, bool pAMAJ)
        {
            c_ID = pID;
            c_ModeleBase = pModeleBase;
            c_dicReferences = new Dictionary<string, object>();
            if (pAMAJ)
                c_ModeleBase.AjouteObjetAMAJ(this);
        }

        /// <summary> Constructeur pour une cle de type date </summary>
        public clg_ObjetBase(clg_ModeleBase pModeleBase, DateTime pDate, bool pAMAJ)
        {
            c_ID = pDate;
            c_ModeleBase = pModeleBase;
            c_dicReferences = new Dictionary<string, object>();
            if (pAMAJ)
                c_ModeleBase.AjouteObjetAMAJ(this);
        }

        /// <summary> Constructeur pour une cle de type chaine </summary>
        public clg_ObjetBase(clg_ModeleBase pModeleBase, string pChaine, bool pAMAJ)
        {
            c_ID = pChaine;
            c_ModeleBase = pModeleBase;
            c_dicReferences = new Dictionary<string, object>();
            if (pAMAJ)
                c_ModeleBase.AjouteObjetAMAJ(this);
        }

        /// <summary> Accesseur vers la propriete ID </summary>
        public object ID
        {
            get { return c_ID; }
        }

        protected abstract void CreerClone();
        public abstract clg_ObjetBase Cloner();
        public abstract void CreeReferences();
        public abstract void DetruitReferences();
        public abstract void CreeLiens();
        public abstract void SupprimeDansListe();
        public abstract void AjouteDansListe();
        public abstract void AnnuleModification();
        public abstract void Detruit();

        public virtual void InsereEnBase()
        {
        }

        public virtual void MAJEnBase()
        {
        }

        public virtual void SupprimeEnBase()
        {

        }

        protected virtual void Initialise(long pID)
        {
            c_ID = pID;
        }

        protected virtual void Initialise(DateTime pDate)
        {
            c_ID = pDate.Ticks;
        }

        protected virtual void Initialise(string pChaine)
        {
            c_ID = pChaine;
        }

        /// <summary> Methode heritable d'analyse de l'existence d'une reference vers cet objet </summary>
        protected virtual void AnalyseReferences()
        {
            c_EstReference = false;
        }

        /// <summary> Methode de suppression de cet objet dans la liste des objets a Supprimer </summary>
        public void AnnuleSuppression()
        {
            c_ModeleBase.ListeObjetsASupprimer.Remove(this);
        }

        /// <summary> Acces ou definition de la propriete Clone </summary>
        public clg_ObjetBase Clone
        {
            get { return c_Clone; }
            set { c_Clone = value; }
        }

        /// <summary> Accesseur du controleur de cette classe </summary>
        public abstract clg_ControleurBase Controleur
        {
            get;
        }

        /// <summary> Fonction de suppression de l'objet </summary>
        /// <returns> Vrai si l'objet n'est pas reference et qu'il a donc ete supprime </returns>
        public bool Supprime()
        {
            if (EstReference)
            {
                return false;
            }
            else
            {
                if (Clone == null) Clone = Cloner();
                c_ModeleBase.AjouteObjetASupprimer(this);
                return true;
            }
        }

        /// <summary>
        /// Retourne la definition de l'objet au format CSV
        /// </summary>
        /// <param name="pID"></param>
        /// <returns></returns>
        public string CSV(bool pID = true)
        {
            string l_CSV = "";
            if (pID)
                l_CSV = c_ID.ToString() + c_SeparateurCSV;

            foreach(string chaine in ListeValeursProprietes())
                l_CSV += chaine + c_SeparateurCSV;

            return l_CSV;
        }

        /// <summary>
        /// Retourne la definition de l'objet au format JSON
        /// </summary>
        /// <param name="pID"></param>
        /// <returns></returns>
        public string JSON(bool pID = true)
        {
            string l_JSON = "[";
            if (pID)
                l_JSON = "\"" + c_ID.ToString() + "\",";

            foreach (string chaine in ListeValeursProprietes())
                l_JSON += "\"" + chaine.Replace("\"", "'") + "\",";

            l_JSON = l_JSON.Remove(l_JSON.Length - 1) + "]";

            return l_JSON;
        }

        /// <summary>
        /// Fonction qui renvoie la liste des proprietes de l'objet
        /// </summary>
        /// <returns></returns>
        public virtual List<string> ListeValeursProprietes()
        {
            return new List<string>();
        }

        /// <summary>
        /// Retourne la liste des noms des champs
        /// </summary>
        /// <returns></returns>
        public virtual List<string> ListeNomsProprietes()
        {
            return new List<string>();
        }

        /// <summary> Accesseur vers le dictionnaire des references de l'objet </summary>
        public Dictionary<string, object> DicReferences
        {
            get { return c_dicReferences; }
        }

        /// <summary> Renvoi vrai si l'objet est reference par d'autres objets </summary>
        public bool EstReference
        {
            get
            {
                AnalyseReferences();
                return c_EstReference;
            }
        }
    }
}