﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

namespace clg_ReflexionV3_Core
{
    public class clg_OutilsIP
    {
        public static string getIPActive()
        {
            string ip = "127.0.0.1";
            foreach (NetworkInterface f in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (f.OperationalStatus == OperationalStatus.Up)
                {
                    IPInterfaceProperties ipInterface = f.GetIPProperties();
                    if (ipInterface.GatewayAddresses.Count > 0)
                    {
                        foreach (UnicastIPAddressInformation unicastAddress in ipInterface.UnicastAddresses)
                        {
                            if ((unicastAddress.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) && (unicastAddress.IPv4Mask.ToString() != "0.0.0.0"))
                            {
                                ip = unicastAddress.Address.ToString();
                                return ip;
                            }
                        }
                    }
                }
            }
            return ip;
        }

      
    }
}
