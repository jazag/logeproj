using System;
using System.Collections.Generic;
using System.Linq;

public class clg_Liste<K, T> : IEnumerable<T>
{
    private Dictionary<K, T> c_dicValeurs;

    public clg_Liste()
    {
        c_dicValeurs = new Dictionary<K, T>();
    }

    public IEnumerator<T> GetEnumerator()
    {
        return c_dicValeurs.Values.GetEnumerator();
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
        return c_dicValeurs.Values.GetEnumerator();
    }

    public Dictionary<K, T> Dictionnaire
    {
        get
        {
            return c_dicValeurs;
        }
    }
}