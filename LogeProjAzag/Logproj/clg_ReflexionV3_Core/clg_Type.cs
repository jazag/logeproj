﻿﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
namespace clg_ReflexionV3_Core
{
    public class clg_Type
    {
        private static Dictionary<string, PropertyInfo> c_dicPropertyInfos;
        private static Dictionary<string, MethodInfo> c_dicMethodInfos;

        static clg_Type()
        {
            c_dicPropertyInfos = new Dictionary<string, PropertyInfo>();
            c_dicMethodInfos = new Dictionary<string, MethodInfo>();
        }

        public static PropertyInfo getPropertyInfo(Type pType, string pNomPropriete)
        {
            if (pType != null && pNomPropriete != "" && pNomPropriete != null)
            {
                PropertyInfo l_PropertyInfo = null;
                string[] ListePropriete = pNomPropriete.Split(new Char[] { '(' });
                pNomPropriete = ListePropriete[0];

                if (c_dicPropertyInfos.ContainsKey(pType.ToString() + "." + pNomPropriete) == false)
                {
                    l_PropertyInfo = pType.GetProperty(pNomPropriete);
                    c_dicPropertyInfos.Add(pType.ToString() + "." + pNomPropriete, l_PropertyInfo);
                }
                else
                {
                    l_PropertyInfo = c_dicPropertyInfos[pType.ToString() + "." + pNomPropriete];
                }
                return l_PropertyInfo;
            }
            else
            {
                throw new Exception("Paramètres invalides");
            }
        }

        public static MethodInfo getMethodInfo(Type pType, string pNomMethod)
        {
            if (pType != null && pNomMethod != "" && pNomMethod != null)
            {
                MethodInfo l_MethodInfo = null;
                //string[] ListeMethod = pNomMethod.Split(new Char[] { '(' });
                //pNomMethod = ListeMethod[0];
                if (c_dicMethodInfos.ContainsKey(pType.ToString() + "." + pNomMethod) == false)
                {
                    l_MethodInfo = pType.GetMethod(pNomMethod);
                    c_dicMethodInfos.Add(pType.ToString() + "." + pNomMethod, l_MethodInfo);
                }
                else
                {
                    l_MethodInfo = c_dicMethodInfos[pType.ToString() + "." + pNomMethod];
                }
                return l_MethodInfo;
            }
            else
            {
                throw new Exception("Paramètres invalides");
            }
        }
    }
}