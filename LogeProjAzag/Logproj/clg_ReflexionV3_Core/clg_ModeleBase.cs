﻿using System;
using System.Collections.Generic;

namespace clg_ReflexionV3_Core
{
    /// <summary> Classe Modele Base heritee par la classe Modele generale </summary>
    public abstract class clg_ModeleBase
    {
        protected List<clg_ObjetBase> c_ListeObjets;
        private List<clg_ObjetBase> c_ListeObjetsAMAJ;
        private List<clg_ObjetBase> c_ListeObjetsASupprimer;

        /// <summary> Instanciation de la classe </summary>
        public clg_ModeleBase()
        {
            c_ListeObjets = new List<clg_ObjetBase>();
            c_ListeObjetsAMAJ = new List<clg_ObjetBase>();
            c_ListeObjetsASupprimer = new List<clg_ObjetBase>();
        }

        /// <summary> Fonction qui renvoie un objet ou null en fonction de la valeur passee en parametre </summary>
        public clg_ObjetBase RenvoieObjet(object pVal, string pNomClasse)
        {
            if (pVal.ToString() == "-1" || pVal.ToString() == "null")
            {
                return null;
            }
            else
            {
                return ListeObjetsParClasse(pNomClasse)[pVal];
            }
        }

        /// <summary> Accesseur de la liste generale de tous les objets du modele </summary>
        public List<clg_ObjetBase> ListeObjetsAMAJ
        {
            get { return c_ListeObjetsAMAJ; }
        }

        /// <summary> Accesseur de la liste generale des objets A Supprimer </summary>
        public List<clg_ObjetBase> ListeObjetsASupprimer
        {
            get { return c_ListeObjetsASupprimer; }
        }

        /// <summary> Methode de suppression d'un objet du modele </summary>
        public void Supprime(clg_ObjetBase pObjet)
        {
            if (c_ListeObjets.Contains(pObjet))
            {
                c_ListeObjets.Remove(pObjet);
                pObjet.DetruitReferences();
                pObjet.Detruit();
                pObjet.SupprimeDansListe();
            }
        }

        /// <summary> Methode d'ajout d'un objet dans le modele </summary>
        public void Ajoute(clg_ObjetBase pObjet)
        {
            //if (c_ListeObjets.Contains(pObjet))
            //{                 
            //    c_ListeObjets.Remove(pObjet);
            //    pObjet.SupprimeDansListe();
            //}
            c_ListeObjets.Add(pObjet);
            pObjet.AjouteDansListe();
        }

        /// <summary> Methode d'ajout d'un objet a MAJ </summary>
        public void AjouteObjetAMAJ(clg_ObjetBase pObjet)
        {
            if (!c_ListeObjetsAMAJ.Contains(pObjet))
                c_ListeObjetsAMAJ.Add(pObjet);
        }

        /// <summary> Methode d'ajout d'un objet a Supprimer </summary>
        public void AjouteObjetASupprimer(clg_ObjetBase pObjet)
        {
            if (!c_ListeObjetsASupprimer.Contains(pObjet))
                c_ListeObjetsASupprimer.Add(pObjet);
        }

        /// <summary> Methode d'annulation des objets a MAJ </summary>
        public void AnnuleObjetsAMAJ()
        {
            foreach (clg_ObjetBase objet in c_ListeObjetsAMAJ)
            {
                if (c_ListeObjets.Contains(objet) && objet.Clone != null)
                {
                    objet.AnnuleModification();
                    objet.Clone = null;
                }
            }
            c_ListeObjetsAMAJ.Clear();
        }

        /// <summary> Methode d'annulation d'un objet a Inserer ou a MAJ </summary>
        public void AnnuleObjetAMAJ(clg_ObjetBase pObjet)
        {
            if (c_ListeObjets.Contains(pObjet) && pObjet.Clone != null)
            {
                pObjet.AnnuleModification();
                pObjet.Clone = null;
            }
            if (c_ListeObjetsAMAJ.Contains(pObjet))
                c_ListeObjetsAMAJ.Remove(pObjet);
        }

        /// <summary> Fonction de formatage de l'id court de l'objet en id long a l'aide du compteur de la table </summary>
        public static Int64 FormateCompteur(int pConst, Int32 pCle)
        {
            string l_compteur = pCle.ToString();
            while (l_compteur.Length < 10)
                l_compteur = "0" + l_compteur;

            l_compteur = pConst.ToString() + l_compteur;
            return Convert.ToInt64(l_compteur);
        }

        /// <summary> Creation des liens et des references dans les objets a MAJ </summary>
        public void CreeLiensEtReferences()
        {
            foreach (clg_ObjetBase obj in c_ListeObjetsAMAJ)
            {
                if (!c_ListeObjets.Contains(obj))
                    Ajoute(obj);
                if (obj.Clone != null)
                {
                    obj.DetruitReferences();
                    obj.Clone = null;
                }
                obj.CreeReferences();
            }
        }

        /// <summary> Suppression des objets dans la BDD </summary>
        public void SupprimeObjets()
        {
            foreach (clg_ObjetBase obj in c_ListeObjetsASupprimer)
                Supprime(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pNomClasse"></param>
        /// <returns></returns>
        public abstract Dictionary<object, clg_ObjetBase> ListeObjetsParClasse(string pNomClasse);

        /// <summary>
        /// Renvoie la liste complète des objets du modèle
        /// </summary>
        public List<clg_ObjetBase> ListeObjets
        {
            get { return c_ListeObjets; }
        }
    }
}
