﻿using clg_ReflexionV3_Core;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dll_ServeurLogeproj_Core
{
    public class clg_ServeurLogeproj
    {
        private static clg_Trace c_Trace;
        private static clg_Controleur c_Controleur;
        private static clg_Modele c_Modele;
        string c_Serveur;
        int c_Port;
        string c_Base;
        string c_Utilisateur;
        string c_MDP;

        public clg_ServeurLogeproj(string pServeurBD, int pPortBD,
            string pNomBaseBD, string pUtilisateurBD, string pMotDePasseBD)
        {
            c_Trace = new clg_TraceConsole();
            c_Serveur = pServeurBD;
            c_Port = pPortBD;
            c_Base = pNomBaseBD;
            c_Utilisateur = pUtilisateurBD;
            c_MDP = pMotDePasseBD;
        }

        public static clg_Controleur Controleur
        {
            get { return c_Controleur; }
        }

        public static clg_Modele Modele
        {
            get { return c_Modele; }
        }

        public static clg_Trace Traces
        {
            get { return c_Trace; }
        }

        public void Demarrer()
        {
            c_Trace.Ajoute("Démarrage du serveur de contacts", "clg_ServeurLogeproj.Demarrer");
            ChargementModele();
            DemarrerServeurHTTP();
            c_Trace.Ajoute("Serveur de contact démarré", "clg_ServeurLogeproj.Demarrer");
        }

        private void ChargementModele()
        {
            c_Trace.Ajoute("Chargement du modèle", "clg_ServeurLogeproj.ChargementModele");
            //connexion à la base de données
            clg_Connecteur l_Connecteur = new clg_ConnecteurNPGSQL_PostGre(c_Serveur,
                c_Base, c_Utilisateur, c_MDP, c_Port.ToString());
            clg_Connection l_Cnn = new clg_Connection(ref l_Connecteur);
            l_Cnn.Ouvre();
            c_Modele = new clg_Modele();
            c_Modele.InitListes(true);
            c_Controleur = new clg_Controleur(l_Cnn);
            c_Controleur.ChargeModeleDepuisBase(c_Modele);
            c_Trace.Ajoute("Modèle chargé", "clg_ServeurLogeproj.ChargementModele");
        }

        private void DemarrerServeurHTTP()
        {
            c_Trace.Ajoute("Démarrage du serveur HTTP", "clg_ServeurLogeproj.DemarrerServeurHTTP");
            //clg_SimpleHTTPServer l_ServeurHTTP;
            //l_ServeurHTTP = new clg_SimpleHTTPServer(@"E:\Affaires\CILOG\AppliContact\FichierServeurContact", 2121, c_Trace);
            c_Trace.Ajoute("Serveur HTTP démarré", "clg_ServeurLogeproj.DemarrerServeurHTTP");
        }

        /// <summary>
        /// Renvoie les donnees du modele au format JSON
        /// </summary>
        public string ObjetsJSON(string pNomListe)
        {
            List<clg_ObjetBase> l_listeObjets = new List<clg_ObjetBase>();

            string l_JSON = "{\"columns\" : [" + Environment.NewLine;
            foreach (string col in l_listeObjets.First().ListeNomsProprietes())
                l_JSON += "{\"name\" : \"" + col + "\" }," + Environment.NewLine;
            l_JSON += "]," + Environment.NewLine + " \"records\": [" + Environment.NewLine;

            foreach (clg_ObjetBase obj in l_listeObjets)
            {
                l_JSON += obj.JSON(false) + "," + Environment.NewLine;
            }

            l_JSON += "]}";

            return JToken.Parse(l_JSON).ToString();
        }
    }
}
