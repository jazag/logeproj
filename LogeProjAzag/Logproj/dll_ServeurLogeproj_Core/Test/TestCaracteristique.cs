﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dll_ServeurLogeproj_Core
{
    public class TestCaracteristique
    {
        private static long c_IdMax;

        /// <summary>
        /// Le constructeur initialise la variable c_IdMax
        /// </summary>
         static TestCaracteristique()
        {
            c_IdMax = IdMax();
        }

        /// <summary>
        /// Cette fonction permet d'avoir l'idMax de la table dans la base de donnée
        /// </summary>
        /// <returns></returns>
        public static long IdMax()
        {
            long l_IDMax = 0;
            foreach (clg_carac_element l_caracaEle in clg_ServeurLogeproj.Modele.Listecarac_element)
            {
                if (l_caracaEle.cel_cn > l_IDMax)
                {
                    l_IDMax = l_caracaEle.cel_cn;
                }
            }
            return l_IDMax;
        }

        /// <summary>
        /// Cette fonction permet de faire des testes d'ajouts de modifications et de supressions dans la base avec la table Carac_Element les testes sont fais automatiquements
        /// </summary>
        public static void LancerTest()
        {
            clg_Connecteur l_Connecteur = new clg_ConnecteurNPGSQL_PostGre("127.0.0.1", "LogProj", "postgres", "asse42800", "5432");
            clg_Connection l_Cnn = new clg_Connection(ref l_Connecteur);
            l_Cnn.Ouvre();

            String l_Reussi = null;
            clg_carac_element l_caracaEle;
            clg_ServeurLogeproj.Traces.Ajoute("Ajout de la Carac_Element", "TestCaracteristique");
            l_caracaEle = Ajouter();
            c_IdMax = c_IdMax - 1;
                if (VerificationAjout(l_caracaEle, l_Cnn))
                {
                    l_Reussi = "Ajout en BDD réussie !";
                }
                else
                {
                    l_Reussi = "Erreur dans l'ajout en BDD !";
                }
            
            clg_ServeurLogeproj.Traces.Ajoute(l_Reussi, "clg_Test_Envoi");
            clg_ServeurLogeproj.Traces.Ajoute("Ajout de l'élément_Caractéristique " + c_IdMax, "TestCaracteristique");
            c_IdMax = c_IdMax + 1;
            clg_ServeurLogeproj.Traces.Ajoute("Ajout de l'élément_Caractéristique " + c_IdMax, "TestCaracteristique");

            clg_ServeurLogeproj.Traces.Modifie("Modification de l'élément_Caractéristique", "TestCaracteristique");
            l_caracaEle = Modifier();

                if (VerificationModificationt(l_caracaEle, l_Cnn))
                {
                    l_Reussi = "Modification en BDD réussie !";
                }
                else
                {
                    l_Reussi = "Erreur dans l'ajout en BDD !";
                }

            clg_ServeurLogeproj.Traces.Ajoute(l_Reussi, "TestCaracteristique");
            c_IdMax = c_IdMax - 1;
            clg_ServeurLogeproj.Traces.Modifie("Modification de l'élément_Caractéristique " + c_IdMax, "TestCaracteristique");
            c_IdMax = c_IdMax + 1;
            clg_ServeurLogeproj.Traces.Modifie("Modification de l'élément_Caractéristique " + c_IdMax, "TestCaracteristique");

            clg_ServeurLogeproj.Traces.Supprime("Suppression de l'élément_Caractéristique", "TestCaracteristique");
            l_caracaEle = Supprimer();

                if (VerificationSuppression(l_caracaEle, l_Cnn))
                {
                    l_Reussi = "Suppression en BDD réussie !";
                }
                else
                {
                    l_Reussi = "Erreur dans l'ajout en BDD !";
                }

            clg_ServeurLogeproj.Traces.Ajoute(l_Reussi, "TestCaracteristique");
            clg_ServeurLogeproj.Traces.Supprime("Suppression de l'élément_Caractéristique " + c_IdMax, "TestCaracteristique");
            c_IdMax = c_IdMax + 1;
            clg_ServeurLogeproj.Traces.Supprime("Suppression de l'élément_Caractéristique " + c_IdMax, "TestCaracteristique");
        }

        /// <summary>
        /// Cette fonction appele la fonction d'ajout dans la base
        /// </summary>
        /// <returns></returns>
        public static clg_carac_element Ajouter()
        {
            c_IdMax++;
            string date = "11/03/1995";
            DateTime dateModif = DateTime.Now; //DateTime.Parse(date);
            clg_carac_element l_caracEle = clg_fct_carac_element.Ajouter(c_IdMax,3005,1009, "Caracteristique " + c_IdMax,null, dateModif);
            c_IdMax++;
            l_caracEle = clg_fct_carac_element.Ajouter(c_IdMax, 3005, 1009, "Caracteristique " + c_IdMax, null, dateModif);
            return l_caracEle;
        }

        /// <summary>
        /// Cette fonction appele la fonction de modifications dans la base
        /// </summary>
        /// <returns></returns>
        public static clg_carac_element Modifier()
        {
            c_IdMax--;
            string date = "11/04/1995";
            DateTime dateModif = DateTime.Parse(date);
            clg_carac_element l_caracEle = clg_fct_carac_element.Modifier(c_IdMax, 3005, 1009, "Caracteristique " + c_IdMax, null, dateModif);
            c_IdMax++;
            l_caracEle = clg_fct_carac_element.Modifier(c_IdMax, 3005, 1009, "Caracteristique " + c_IdMax, null, dateModif);
            return l_caracEle;
        }

        /// <summary>
        /// Cette fonction appele la fonction les suppressions dans la base
        /// </summary>
        /// <returns></returns>
        public static clg_carac_element Supprimer()
        {

            clg_carac_element l_caracEle = clg_fct_carac_element.Supprimer(c_IdMax);
            c_IdMax--;
            l_caracEle = clg_fct_carac_element.Supprimer(c_IdMax);
            return l_caracEle;
        }

        /// <summary>
        /// Cette fonction vérifie si l'ajout dans la base a bien été faite
        /// </summary>
        /// <param name="pCarac"></param>
        /// <param name="pCnn"></param>
        /// <returns></returns>
        public static bool VerificationAjout(clg_carac_element pCarac, clg_Connection pCnn)
        {

            clg_JeuEnregistrement l_rds;
            string l_ordreSQL = "SELECT * FROM carac_element WHERE cel_cn = " + pCarac.cel_cn;
            l_rds = pCnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL);
            if (l_rds.NombreLignes != 0)
            {
                Int64 carac_cn = Int64.Parse(l_rds.Donnee(0, 0));

                if (pCarac.cel_cn == carac_cn)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Cette fonction vérifie si les modifications dans la base ont bien été faite
        /// </summary>
        /// <param name="pCaracEle"></param>
        /// <param name="pCnn"></param>
        /// <returns></returns>
        public static bool VerificationModificationt(clg_carac_element pCaracEle, clg_Connection pCnn)
        {

            clg_JeuEnregistrement l_rds;
            string l_ordreSQL = "SELECT * FROM carac_element WHERE cel_cn = " + pCaracEle.cel_cn;
            l_rds = pCnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL);
            if (l_rds.NombreLignes != 0)
            {
                Int64 Carac_cn = Int64.Parse(l_rds.Donnee(0, 0));


                if (pCaracEle.cel_cn == Carac_cn)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Cette fonction vérifie si les suppressions dans la base ont bien été faite
        /// </summary>
        /// <param name="pCaracEle"></param>
        /// <param name="pCnn"></param>
        /// <returns></returns>
        public static bool VerificationSuppression(clg_carac_element pCaracEle, clg_Connection pCnn)
        {
            clg_JeuEnregistrement l_rds;
            string l_ordreSQL = "SELECT * FROM carac_element WHERE cel_cn = " + pCaracEle.cel_cn;
            l_rds = pCnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL);
            if (l_rds.NombreLignes == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
