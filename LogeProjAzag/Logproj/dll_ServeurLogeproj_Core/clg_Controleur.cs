namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
    using clg_ReflexionV3_Core;
    using System;

    /// <summary> Classe du controleur general des donnees </summary>
    public class clg_Controleur
    {
        public clg_DAL_annee_comptable c_DAL_annee_comptable;
        public clg_DAL_financeurs c_DAL_financeurs;
        public clg_DAL_c_operation c_DAL_c_operation;
        public clg_DAL_assembly c_DAL_assembly;
        public clg_DAL_c_qualif_temps c_DAL_c_qualif_temps;
        public clg_DAL_annee_civile c_DAL_annee_civile;
        public clg_DAL_t_lien_sage c_DAL_t_lien_sage;
        public clg_DAL_comment c_DAL_comment;
        public clg_DAL_categorie_frais c_DAL_categorie_frais;
        public clg_DAL_comment_element_bis c_DAL_comment_element_bis;
        public clg_DAL_comptes_financeur c_DAL_comptes_financeur;
        public clg_DAL_demandes_subventions c_DAL_demandes_subventions;
        public clg_DAL_comment_financeur c_DAL_comment_financeur;
        public clg_DAL_cpt_ana_exclu c_DAL_cpt_ana_exclu;
        public clg_DAL_etat_arrete c_DAL_etat_arrete;
        public clg_DAL_elements_bis c_DAL_elements_bis;
        public clg_DAL_filtre_car_perso c_DAL_filtre_car_perso;
        public clg_DAL_format_carac c_DAL_format_carac;
        public clg_DAL_groupe_metaprojet c_DAL_groupe_metaprojet;
        public clg_DAL_fournisseurs c_DAL_fournisseurs;
        public clg_DAL_forfait c_DAL_forfait;
        public clg_DAL_groupes c_DAL_groupes;
        public clg_DAL_heures_sup_prevues c_DAL_heures_sup_prevues;
        public clg_DAL_import_sage c_DAL_import_sage;
        public clg_DAL_infos_conservatoire c_DAL_infos_conservatoire;
        public clg_DAL_frais_generaux c_DAL_frais_generaux;
        public clg_DAL_jours c_DAL_jours;
        public clg_DAL_jours_feries c_DAL_jours_feries;
        public clg_DAL_journal c_DAL_journal;
        public clg_DAL_message_lectureseule c_DAL_message_lectureseule;
        public clg_DAL_personnels c_DAL_personnels;
        public clg_DAL_mois c_DAL_mois;
        public clg_DAL_param_temps_travail c_DAL_param_temps_travail;
        public clg_DAL_periode_planning c_DAL_periode_planning;
        public clg_DAL_plugin c_DAL_plugin;
        public clg_DAL_projets_bis c_DAL_projets_bis;
        public clg_DAL_reajustement_fonds_dedies c_DAL_reajustement_fonds_dedies;
        public clg_DAL_regle c_DAL_regle;
        public clg_DAL_t_base c_DAL_t_base;
        public clg_DAL_solde_conges c_DAL_solde_conges;
        public clg_DAL_t_prestation c_DAL_t_prestation;
        public clg_DAL_t_fonctions c_DAL_t_fonctions;
        public clg_DAL_synchro_connect_base c_DAL_synchro_connect_base;
        public clg_DAL_t_element c_DAL_t_element;
        public clg_DAL_t_compte c_DAL_t_compte;
        public clg_DAL_t_conge c_DAL_t_conge;
        public clg_DAL_t_cout_projet c_DAL_t_cout_projet;
        public clg_DAL_t_init_appli c_DAL_t_init_appli;
        public clg_DAL_t_fonction_contact c_DAL_t_fonction_contact;
        public clg_DAL_t_info c_DAL_t_info;
        public clg_DAL_t_limitation_donnees c_DAL_t_limitation_donnees;
        public clg_DAL_t_journaux c_DAL_t_journaux;
        public clg_DAL_t_limitation_interface c_DAL_t_limitation_interface;
        public clg_DAL_t_ope_financiere c_DAL_t_ope_financiere;
        public clg_DAL_t_etat c_DAL_t_etat;
        public clg_DAL_t_parametre c_DAL_t_parametre;
        public clg_DAL_t_projet c_DAL_t_projet;
        public clg_DAL_t_rubriques_temps c_DAL_t_rubriques_temps;
        public clg_DAL_t_sens_comptes c_DAL_t_sens_comptes;
        public clg_DAL_type_fonct_compta c_DAL_type_fonct_compta;
        public clg_DAL_type_base_mae c_DAL_type_base_mae;
        public clg_DAL_t_objets_interface c_DAL_t_objets_interface;
        public clg_DAL_utilisateur c_DAL_utilisateur;
        public clg_DAL_vehicules c_DAL_vehicules;
        public clg_DAL_validation_temps_perso c_DAL_validation_temps_perso;
        public clg_DAL_t_maj c_DAL_t_maj;
        public clg_DAL_validation_temps_semaine c_DAL_validation_temps_semaine;
        public clg_DAL_temps_travail_annuel c_DAL_temps_travail_annuel;
        public clg_DAL_comment_demandes c_DAL_comment_demandes;
        public clg_DAL_fenetre c_DAL_fenetre;
        public clg_DAL_arretes c_DAL_arretes;
        public clg_DAL_forfait_personnel c_DAL_forfait_personnel;
        public clg_DAL_carac c_DAL_carac;
        public clg_DAL_basemae c_DAL_basemae;
        public clg_DAL_comptes_non_affichables c_DAL_comptes_non_affichables;
        public clg_DAL_comment_vehicule c_DAL_comment_vehicule;
        public clg_DAL_comment_fournisseur c_DAL_comment_fournisseur;
        public clg_DAL_convention c_DAL_convention;
        public clg_DAL_contacts c_DAL_contacts;
        public clg_DAL_etats c_DAL_etats;
        public clg_DAL_filtre_car_t_elements c_DAL_filtre_car_t_elements;
        public clg_DAL_infos_base c_DAL_infos_base;
        public clg_DAL_fonctions_perso c_DAL_fonctions_perso;
        public clg_DAL_limitation_donnees c_DAL_limitation_donnees;
        public clg_DAL_init_appli c_DAL_init_appli;
        public clg_DAL_limitation_fenetre c_DAL_limitation_fenetre;
        public clg_DAL_mensuel c_DAL_mensuel;
        public clg_DAL_param_compte c_DAL_param_compte;
        public clg_DAL_parametrage c_DAL_parametrage;
        public clg_DAL_poste c_DAL_poste;
        public clg_DAL_param_conservatoire c_DAL_param_conservatoire;
        public clg_DAL_profiles c_DAL_profiles;
        public clg_DAL_taux_km_reels c_DAL_taux_km_reels;
        public clg_DAL_rubriques_temps c_DAL_rubriques_temps;
        public clg_DAL_taux_km_prevus c_DAL_taux_km_prevus;
        public clg_DAL_valeur_param c_DAL_valeur_param;
        public clg_DAL_valeur_carac c_DAL_valeur_carac;
        public clg_DAL_temps_perso_generaux c_DAL_temps_perso_generaux;
        public clg_DAL_convention_regle c_DAL_convention_regle;
        public clg_DAL_comment_arretes c_DAL_comment_arretes;
        public clg_DAL_tb_sage c_DAL_tb_sage;
        public clg_DAL_calendrier c_DAL_calendrier;
        public clg_DAL_conge_prevu c_DAL_conge_prevu;
        public clg_DAL_conge_realise c_DAL_conge_realise;
        public clg_DAL_deplacements_realises c_DAL_deplacements_realises;
        public clg_DAL_corbeille_deplacement c_DAL_corbeille_deplacement;
        public clg_DAL_frais_mission c_DAL_frais_mission;
        public clg_DAL_planning c_DAL_planning;
        public clg_DAL_elements c_DAL_elements;
        public clg_DAL_carac_element c_DAL_carac_element;
        public clg_DAL_lien_sage c_DAL_lien_sage;
        public clg_DAL_financement_prevus c_DAL_financement_prevus;
        public clg_DAL_comment_element c_DAL_comment_element;
        public clg_DAL_concepteurs c_DAL_concepteurs;
        public clg_DAL_deplacements_prevus c_DAL_deplacements_prevus;
        public clg_DAL_facture c_DAL_facture;
        public clg_DAL_lien_arretes c_DAL_lien_arretes;
        public clg_DAL_limitation_interface c_DAL_limitation_interface;
        public clg_DAL_intervenants c_DAL_intervenants;
        public clg_DAL_projets c_DAL_projets;
        public clg_DAL_prestation_prevus c_DAL_prestation_prevus;
        public clg_DAL_qualif_temps c_DAL_qualif_temps;
        public clg_DAL_temps_realises c_DAL_temps_realises;
        public clg_DAL_temps_intervenants c_DAL_temps_intervenants;
        public clg_DAL_taux c_DAL_taux;
        public clg_DAL_report c_DAL_report;
        public clg_DAL_ventilation_deplacement c_DAL_ventilation_deplacement;
        public clg_DAL_ventilation_deplacement_csa c_DAL_ventilation_deplacement_csa;
        public clg_DAL_taux_fin c_DAL_taux_fin;
        public clg_DAL_objets_interface c_DAL_objets_interface;
        public clg_DAL_operations c_DAL_operations;
        public clg_DAL_operations_reportees c_DAL_operations_reportees;


        /// <summary> Constructeur de la classe </summary>
        public clg_Controleur(clg_Connection pCnn)
        {
            c_DAL_annee_comptable = new clg_DAL_annee_comptable(pCnn);
            clg_annee_comptable.c_CTLannee_comptable = c_DAL_annee_comptable;

            c_DAL_financeurs = new clg_DAL_financeurs(pCnn);
            clg_financeurs.c_CTLfinanceurs = c_DAL_financeurs;

            c_DAL_c_operation = new clg_DAL_c_operation(pCnn);
            clg_c_operation.c_CTLc_operation = c_DAL_c_operation;

            c_DAL_assembly = new clg_DAL_assembly(pCnn);
            clg_assembly.c_CTLassembly = c_DAL_assembly;

            c_DAL_c_qualif_temps = new clg_DAL_c_qualif_temps(pCnn);
            clg_c_qualif_temps.c_CTLc_qualif_temps = c_DAL_c_qualif_temps;

            c_DAL_annee_civile = new clg_DAL_annee_civile(pCnn);
            clg_annee_civile.c_CTLannee_civile = c_DAL_annee_civile;

            c_DAL_t_lien_sage = new clg_DAL_t_lien_sage(pCnn);
            clg_t_lien_sage.c_CTLt_lien_sage = c_DAL_t_lien_sage;

            c_DAL_comment = new clg_DAL_comment(pCnn);
            clg_comment.c_CTLcomment = c_DAL_comment;

            c_DAL_categorie_frais = new clg_DAL_categorie_frais(pCnn);
            clg_categorie_frais.c_CTLcategorie_frais = c_DAL_categorie_frais;

            c_DAL_comment_element_bis = new clg_DAL_comment_element_bis(pCnn);
            clg_comment_element_bis.c_CTLcomment_element_bis = c_DAL_comment_element_bis;

            c_DAL_comptes_financeur = new clg_DAL_comptes_financeur(pCnn);
            clg_comptes_financeur.c_CTLcomptes_financeur = c_DAL_comptes_financeur;

            c_DAL_demandes_subventions = new clg_DAL_demandes_subventions(pCnn);
            clg_demandes_subventions.c_CTLdemandes_subventions = c_DAL_demandes_subventions;

            c_DAL_comment_financeur = new clg_DAL_comment_financeur(pCnn);

            c_DAL_cpt_ana_exclu = new clg_DAL_cpt_ana_exclu(pCnn);
            clg_cpt_ana_exclu.c_CTLcpt_ana_exclu = c_DAL_cpt_ana_exclu;

            c_DAL_etat_arrete = new clg_DAL_etat_arrete(pCnn);
            clg_etat_arrete.c_CTLetat_arrete = c_DAL_etat_arrete;

            c_DAL_elements_bis = new clg_DAL_elements_bis(pCnn);
            clg_elements_bis.c_CTLelements_bis = c_DAL_elements_bis;

            c_DAL_filtre_car_perso = new clg_DAL_filtre_car_perso(pCnn);
            clg_filtre_car_perso.c_CTLfiltre_car_perso = c_DAL_filtre_car_perso;

            c_DAL_format_carac = new clg_DAL_format_carac(pCnn);
            clg_format_carac.c_CTLformat_carac = c_DAL_format_carac;

            c_DAL_groupe_metaprojet = new clg_DAL_groupe_metaprojet(pCnn);
            clg_groupe_metaprojet.c_CTLgroupe_metaprojet = c_DAL_groupe_metaprojet;

            c_DAL_fournisseurs = new clg_DAL_fournisseurs(pCnn);
            clg_fournisseurs.c_CTLfournisseurs = c_DAL_fournisseurs;

            c_DAL_forfait = new clg_DAL_forfait(pCnn);
            clg_forfait.c_CTLforfait = c_DAL_forfait;

            c_DAL_groupes = new clg_DAL_groupes(pCnn);
            clg_groupes.c_CTLgroupes = c_DAL_groupes;

            c_DAL_heures_sup_prevues = new clg_DAL_heures_sup_prevues(pCnn);
            clg_heures_sup_prevues.c_CTLheures_sup_prevues = c_DAL_heures_sup_prevues;

            c_DAL_import_sage = new clg_DAL_import_sage(pCnn);
            clg_import_sage.c_CTLimport_sage = c_DAL_import_sage;

            c_DAL_infos_conservatoire = new clg_DAL_infos_conservatoire(pCnn);
            clg_infos_conservatoire.c_CTLinfos_conservatoire = c_DAL_infos_conservatoire;

            c_DAL_frais_generaux = new clg_DAL_frais_generaux(pCnn);
            clg_frais_generaux.c_CTLfrais_generaux = c_DAL_frais_generaux;

            c_DAL_jours = new clg_DAL_jours(pCnn);
            clg_jours.c_CTLjours = c_DAL_jours;

            c_DAL_jours_feries = new clg_DAL_jours_feries(pCnn);
            clg_jours_feries.c_CTLjours_feries = c_DAL_jours_feries;

            c_DAL_journal = new clg_DAL_journal(pCnn);
            clg_journal.c_CTLjournal = c_DAL_journal;

            c_DAL_message_lectureseule = new clg_DAL_message_lectureseule(pCnn);
            clg_message_lectureseule.c_CTLmessage_lectureseule = c_DAL_message_lectureseule;

            c_DAL_personnels = new clg_DAL_personnels(pCnn);
            clg_personnels.c_CTLpersonnels = c_DAL_personnels;

            c_DAL_mois = new clg_DAL_mois(pCnn);
            clg_mois.c_CTLmois = c_DAL_mois;

            c_DAL_param_temps_travail = new clg_DAL_param_temps_travail(pCnn);
            clg_param_temps_travail.c_CTLparam_temps_travail = c_DAL_param_temps_travail;

            c_DAL_periode_planning = new clg_DAL_periode_planning(pCnn);
            clg_periode_planning.c_CTLperiode_planning = c_DAL_periode_planning;

            c_DAL_plugin = new clg_DAL_plugin(pCnn);
            clg_plugin.c_CTLplugin = c_DAL_plugin;

            c_DAL_projets_bis = new clg_DAL_projets_bis(pCnn);
            clg_projets_bis.c_CTLprojets_bis = c_DAL_projets_bis;

            c_DAL_reajustement_fonds_dedies = new clg_DAL_reajustement_fonds_dedies(pCnn);
            clg_reajustement_fonds_dedies.c_CTLreajustement_fonds_dedies = c_DAL_reajustement_fonds_dedies;

            c_DAL_regle = new clg_DAL_regle(pCnn);
            clg_regle.c_CTLregle = c_DAL_regle;

            c_DAL_t_base = new clg_DAL_t_base(pCnn);
            clg_t_base.c_CTLt_base = c_DAL_t_base;

            c_DAL_solde_conges = new clg_DAL_solde_conges(pCnn);
            clg_solde_conges.c_CTLsolde_conges = c_DAL_solde_conges;

            c_DAL_t_prestation = new clg_DAL_t_prestation(pCnn);
            clg_t_prestation.c_CTLt_prestation = c_DAL_t_prestation;

            c_DAL_t_fonctions = new clg_DAL_t_fonctions(pCnn);
            clg_t_fonctions.c_CTLt_fonctions = c_DAL_t_fonctions;

            c_DAL_synchro_connect_base = new clg_DAL_synchro_connect_base(pCnn);
            clg_synchro_connect_base.c_CTLsynchro_connect_base = c_DAL_synchro_connect_base;

            c_DAL_t_element = new clg_DAL_t_element(pCnn);
            clg_t_element.c_CTLt_element = c_DAL_t_element;

            c_DAL_t_compte = new clg_DAL_t_compte(pCnn);
            clg_t_compte.c_CTLt_compte = c_DAL_t_compte;

            c_DAL_t_conge = new clg_DAL_t_conge(pCnn);
            clg_t_conge.c_CTLt_conge = c_DAL_t_conge;

            c_DAL_t_cout_projet = new clg_DAL_t_cout_projet(pCnn);
            clg_t_cout_projet.c_CTLt_cout_projet = c_DAL_t_cout_projet;

            c_DAL_t_init_appli = new clg_DAL_t_init_appli(pCnn);
            clg_t_init_appli.c_CTLt_init_appli = c_DAL_t_init_appli;

            c_DAL_t_fonction_contact = new clg_DAL_t_fonction_contact(pCnn);
            clg_t_fonction_contact.c_CTLt_fonction_contact = c_DAL_t_fonction_contact;

            c_DAL_t_info = new clg_DAL_t_info(pCnn);
            clg_t_info.c_CTLt_info = c_DAL_t_info;

            c_DAL_t_limitation_donnees = new clg_DAL_t_limitation_donnees(pCnn);
            clg_t_limitation_donnees.c_CTLt_limitation_donnees = c_DAL_t_limitation_donnees;

            c_DAL_t_journaux = new clg_DAL_t_journaux(pCnn);
            clg_t_journaux.c_CTLt_journaux = c_DAL_t_journaux;

            c_DAL_t_limitation_interface = new clg_DAL_t_limitation_interface(pCnn);
            clg_t_limitation_interface.c_CTLt_limitation_interface = c_DAL_t_limitation_interface;

            c_DAL_t_ope_financiere = new clg_DAL_t_ope_financiere(pCnn);
            clg_t_ope_financiere.c_CTLt_ope_financiere = c_DAL_t_ope_financiere;

            c_DAL_t_etat = new clg_DAL_t_etat(pCnn);
            clg_t_etat.c_CTLt_etat = c_DAL_t_etat;

            c_DAL_t_parametre = new clg_DAL_t_parametre(pCnn);
            clg_t_parametre.c_CTLt_parametre = c_DAL_t_parametre;

            c_DAL_t_projet = new clg_DAL_t_projet(pCnn);
            clg_t_projet.c_CTLt_projet = c_DAL_t_projet;

            c_DAL_t_rubriques_temps = new clg_DAL_t_rubriques_temps(pCnn);
            clg_t_rubriques_temps.c_CTLt_rubriques_temps = c_DAL_t_rubriques_temps;

            c_DAL_t_sens_comptes = new clg_DAL_t_sens_comptes(pCnn);
            clg_t_sens_comptes.c_CTLt_sens_comptes = c_DAL_t_sens_comptes;

            c_DAL_type_fonct_compta = new clg_DAL_type_fonct_compta(pCnn);
            clg_type_fonct_compta.c_CTLtype_fonct_compta = c_DAL_type_fonct_compta;

            c_DAL_type_base_mae = new clg_DAL_type_base_mae(pCnn);
            clg_type_base_mae.c_CTLtype_base_mae = c_DAL_type_base_mae;

            c_DAL_t_objets_interface = new clg_DAL_t_objets_interface(pCnn);
            clg_t_objets_interface.c_CTLt_objets_interface = c_DAL_t_objets_interface;

            c_DAL_utilisateur = new clg_DAL_utilisateur(pCnn);
            clg_utilisateur.c_CTLutilisateur = c_DAL_utilisateur;

            c_DAL_vehicules = new clg_DAL_vehicules(pCnn);
            clg_vehicules.c_CTLvehicules = c_DAL_vehicules;

            c_DAL_validation_temps_perso = new clg_DAL_validation_temps_perso(pCnn);
            clg_validation_temps_perso.c_CTLvalidation_temps_perso = c_DAL_validation_temps_perso;

            c_DAL_t_maj = new clg_DAL_t_maj(pCnn);
            clg_t_maj.c_CTLt_maj = c_DAL_t_maj;

            c_DAL_validation_temps_semaine = new clg_DAL_validation_temps_semaine(pCnn);
            clg_validation_temps_semaine.c_CTLvalidation_temps_semaine = c_DAL_validation_temps_semaine;

            c_DAL_temps_travail_annuel = new clg_DAL_temps_travail_annuel(pCnn);
            clg_temps_travail_annuel.c_CTLtemps_travail_annuel = c_DAL_temps_travail_annuel;

            c_DAL_comment_demandes = new clg_DAL_comment_demandes(pCnn);
            clg_comment_demandes.c_CTLcomment_demandes = c_DAL_comment_demandes;

            c_DAL_fenetre = new clg_DAL_fenetre(pCnn);
            clg_fenetre.c_CTLfenetre = c_DAL_fenetre;

            c_DAL_arretes = new clg_DAL_arretes(pCnn);
            clg_arretes.c_CTLarretes = c_DAL_arretes;

            c_DAL_forfait_personnel = new clg_DAL_forfait_personnel(pCnn);
            clg_forfait_personnel.c_CTLforfait_personnel = c_DAL_forfait_personnel;

            c_DAL_carac = new clg_DAL_carac(pCnn);
            clg_carac.c_CTLcarac = c_DAL_carac;

            c_DAL_basemae = new clg_DAL_basemae(pCnn);
            clg_basemae.c_CTLbasemae = c_DAL_basemae;

            c_DAL_comptes_non_affichables = new clg_DAL_comptes_non_affichables(pCnn);
            clg_comptes_non_affichables.c_CTLcomptes_non_affichables = c_DAL_comptes_non_affichables;

            c_DAL_comment_vehicule = new clg_DAL_comment_vehicule(pCnn);

            c_DAL_comment_fournisseur = new clg_DAL_comment_fournisseur(pCnn);

            c_DAL_convention = new clg_DAL_convention(pCnn);
            clg_convention.c_CTLconvention = c_DAL_convention;

            c_DAL_contacts = new clg_DAL_contacts(pCnn);
            clg_contacts.c_CTLcontacts = c_DAL_contacts;

            c_DAL_etats = new clg_DAL_etats(pCnn);
            clg_etats.c_CTLetats = c_DAL_etats;

            c_DAL_filtre_car_t_elements = new clg_DAL_filtre_car_t_elements(pCnn);

            c_DAL_infos_base = new clg_DAL_infos_base(pCnn);
            clg_infos_base.c_CTLinfos_base = c_DAL_infos_base;

            c_DAL_fonctions_perso = new clg_DAL_fonctions_perso(pCnn);
            clg_fonctions_perso.c_CTLfonctions_perso = c_DAL_fonctions_perso;

            c_DAL_limitation_donnees = new clg_DAL_limitation_donnees(pCnn);
            clg_limitation_donnees.c_CTLlimitation_donnees = c_DAL_limitation_donnees;

            c_DAL_init_appli = new clg_DAL_init_appli(pCnn);
            clg_init_appli.c_CTLinit_appli = c_DAL_init_appli;

            c_DAL_limitation_fenetre = new clg_DAL_limitation_fenetre(pCnn);
            clg_limitation_fenetre.c_CTLlimitation_fenetre = c_DAL_limitation_fenetre;

            c_DAL_mensuel = new clg_DAL_mensuel(pCnn);
            clg_mensuel.c_CTLmensuel = c_DAL_mensuel;

            c_DAL_param_compte = new clg_DAL_param_compte(pCnn);
            clg_param_compte.c_CTLparam_compte = c_DAL_param_compte;

            c_DAL_parametrage = new clg_DAL_parametrage(pCnn);
            clg_parametrage.c_CTLparametrage = c_DAL_parametrage;

            c_DAL_poste = new clg_DAL_poste(pCnn);
            clg_poste.c_CTLposte = c_DAL_poste;

            c_DAL_param_conservatoire = new clg_DAL_param_conservatoire(pCnn);
            clg_param_conservatoire.c_CTLparam_conservatoire = c_DAL_param_conservatoire;

            c_DAL_profiles = new clg_DAL_profiles(pCnn);

            c_DAL_taux_km_reels = new clg_DAL_taux_km_reels(pCnn);
            clg_taux_km_reels.c_CTLtaux_km_reels = c_DAL_taux_km_reels;

            c_DAL_rubriques_temps = new clg_DAL_rubriques_temps(pCnn);
            clg_rubriques_temps.c_CTLrubriques_temps = c_DAL_rubriques_temps;

            c_DAL_taux_km_prevus = new clg_DAL_taux_km_prevus(pCnn);
            clg_taux_km_prevus.c_CTLtaux_km_prevus = c_DAL_taux_km_prevus;

            c_DAL_valeur_param = new clg_DAL_valeur_param(pCnn);
            clg_valeur_param.c_CTLvaleur_param = c_DAL_valeur_param;

            c_DAL_valeur_carac = new clg_DAL_valeur_carac(pCnn);
            clg_valeur_carac.c_CTLvaleur_carac = c_DAL_valeur_carac;

            c_DAL_temps_perso_generaux = new clg_DAL_temps_perso_generaux(pCnn);
            clg_temps_perso_generaux.c_CTLtemps_perso_generaux = c_DAL_temps_perso_generaux;

            c_DAL_convention_regle = new clg_DAL_convention_regle(pCnn);
            clg_convention_regle.c_CTLconvention_regle = c_DAL_convention_regle;

            c_DAL_comment_arretes = new clg_DAL_comment_arretes(pCnn);

            c_DAL_tb_sage = new clg_DAL_tb_sage(pCnn);
            clg_tb_sage.c_CTLtb_sage = c_DAL_tb_sage;

            c_DAL_calendrier = new clg_DAL_calendrier(pCnn);
            clg_calendrier.c_CTLcalendrier = c_DAL_calendrier;

            c_DAL_conge_prevu = new clg_DAL_conge_prevu(pCnn);
            clg_conge_prevu.c_CTLconge_prevu = c_DAL_conge_prevu;

            c_DAL_conge_realise = new clg_DAL_conge_realise(pCnn);
            clg_conge_realise.c_CTLconge_realise = c_DAL_conge_realise;

            c_DAL_deplacements_realises = new clg_DAL_deplacements_realises(pCnn);
            clg_deplacements_realises.c_CTLdeplacements_realises = c_DAL_deplacements_realises;

            c_DAL_corbeille_deplacement = new clg_DAL_corbeille_deplacement(pCnn);
            clg_corbeille_deplacement.c_CTLcorbeille_deplacement = c_DAL_corbeille_deplacement;

            c_DAL_frais_mission = new clg_DAL_frais_mission(pCnn);
            clg_frais_mission.c_CTLfrais_mission = c_DAL_frais_mission;

            c_DAL_planning = new clg_DAL_planning(pCnn);
            clg_planning.c_CTLplanning = c_DAL_planning;

            c_DAL_elements = new clg_DAL_elements(pCnn);
            clg_elements.c_CTLelements = c_DAL_elements;

            c_DAL_carac_element = new clg_DAL_carac_element(pCnn);
            clg_carac_element.c_CTLcarac_element = c_DAL_carac_element;

            c_DAL_lien_sage = new clg_DAL_lien_sage(pCnn);
            clg_lien_sage.c_CTLlien_sage = c_DAL_lien_sage;

            c_DAL_financement_prevus = new clg_DAL_financement_prevus(pCnn);
            clg_financement_prevus.c_CTLfinancement_prevus = c_DAL_financement_prevus;

            c_DAL_comment_element = new clg_DAL_comment_element(pCnn);

            c_DAL_concepteurs = new clg_DAL_concepteurs(pCnn);
            clg_concepteurs.c_CTLconcepteurs = c_DAL_concepteurs;

            c_DAL_deplacements_prevus = new clg_DAL_deplacements_prevus(pCnn);
            clg_deplacements_prevus.c_CTLdeplacements_prevus = c_DAL_deplacements_prevus;

            c_DAL_facture = new clg_DAL_facture(pCnn);
            clg_facture.c_CTLfacture = c_DAL_facture;

            c_DAL_lien_arretes = new clg_DAL_lien_arretes(pCnn);
            clg_lien_arretes.c_CTLlien_arretes = c_DAL_lien_arretes;

            c_DAL_limitation_interface = new clg_DAL_limitation_interface(pCnn);
            clg_limitation_interface.c_CTLlimitation_interface = c_DAL_limitation_interface;

            c_DAL_intervenants = new clg_DAL_intervenants(pCnn);
            clg_intervenants.c_CTLintervenants = c_DAL_intervenants;

            c_DAL_projets = new clg_DAL_projets(pCnn);
            clg_projets.c_CTLprojets = c_DAL_projets;

            c_DAL_prestation_prevus = new clg_DAL_prestation_prevus(pCnn);
            clg_prestation_prevus.c_CTLprestation_prevus = c_DAL_prestation_prevus;

            c_DAL_qualif_temps = new clg_DAL_qualif_temps(pCnn);

            c_DAL_temps_realises = new clg_DAL_temps_realises(pCnn);
            clg_temps_realises.c_CTLtemps_realises = c_DAL_temps_realises;

            c_DAL_temps_intervenants = new clg_DAL_temps_intervenants(pCnn);
            clg_temps_intervenants.c_CTLtemps_intervenants = c_DAL_temps_intervenants;

            c_DAL_taux = new clg_DAL_taux(pCnn);
            clg_taux.c_CTLtaux = c_DAL_taux;

            c_DAL_report = new clg_DAL_report(pCnn);
            clg_report.c_CTLreport = c_DAL_report;

            c_DAL_ventilation_deplacement = new clg_DAL_ventilation_deplacement(pCnn);
            clg_ventilation_deplacement.c_CTLventilation_deplacement = c_DAL_ventilation_deplacement;

            c_DAL_ventilation_deplacement_csa = new clg_DAL_ventilation_deplacement_csa(pCnn);
            clg_ventilation_deplacement_csa.c_CTLventilation_deplacement_csa = c_DAL_ventilation_deplacement_csa;

            c_DAL_taux_fin = new clg_DAL_taux_fin(pCnn);
            clg_taux_fin.c_CTLtaux_fin = c_DAL_taux_fin;

            c_DAL_objets_interface = new clg_DAL_objets_interface(pCnn);
            clg_objets_interface.c_CTLobjets_interface = c_DAL_objets_interface;

            c_DAL_operations = new clg_DAL_operations(pCnn);
            clg_operations.c_CTLoperations = c_DAL_operations;

            c_DAL_operations_reportees = new clg_DAL_operations_reportees(pCnn);


        }

        /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
        public void ChargeModeleDepuisBase(clg_Modele pModele)
        {
            var l_Temps = DateTime.Now;

            c_DAL_annee_comptable.ChargeDepuisBase(pModele);
            c_DAL_financeurs.ChargeDepuisBase(pModele);
            c_DAL_c_operation.ChargeDepuisBase(pModele);
            c_DAL_assembly.ChargeDepuisBase(pModele);
            c_DAL_c_qualif_temps.ChargeDepuisBase(pModele);
            c_DAL_annee_civile.ChargeDepuisBase(pModele);
            c_DAL_t_lien_sage.ChargeDepuisBase(pModele);
            c_DAL_comment.ChargeDepuisBase(pModele);
            c_DAL_categorie_frais.ChargeDepuisBase(pModele);
            c_DAL_comment_element_bis.ChargeDepuisBase(pModele);
            c_DAL_comptes_financeur.ChargeDepuisBase(pModele);
            c_DAL_demandes_subventions.ChargeDepuisBase(pModele);
            c_DAL_cpt_ana_exclu.ChargeDepuisBase(pModele);
            c_DAL_etat_arrete.ChargeDepuisBase(pModele);
            c_DAL_elements_bis.ChargeDepuisBase(pModele);
            c_DAL_filtre_car_perso.ChargeDepuisBase(pModele);
            c_DAL_format_carac.ChargeDepuisBase(pModele);
            c_DAL_groupe_metaprojet.ChargeDepuisBase(pModele);
            c_DAL_fournisseurs.ChargeDepuisBase(pModele);
            c_DAL_forfait.ChargeDepuisBase(pModele);
            c_DAL_groupes.ChargeDepuisBase(pModele);
            c_DAL_heures_sup_prevues.ChargeDepuisBase(pModele);
            c_DAL_import_sage.ChargeDepuisBase(pModele);
            c_DAL_infos_conservatoire.ChargeDepuisBase(pModele);
            c_DAL_frais_generaux.ChargeDepuisBase(pModele);
            c_DAL_jours.ChargeDepuisBase(pModele);
            c_DAL_jours_feries.ChargeDepuisBase(pModele);
            c_DAL_journal.ChargeDepuisBase(pModele);
            c_DAL_message_lectureseule.ChargeDepuisBase(pModele);
            c_DAL_personnels.ChargeDepuisBase(pModele);
            c_DAL_mois.ChargeDepuisBase(pModele);
            c_DAL_param_temps_travail.ChargeDepuisBase(pModele);
            c_DAL_periode_planning.ChargeDepuisBase(pModele);
            c_DAL_plugin.ChargeDepuisBase(pModele);
            c_DAL_projets_bis.ChargeDepuisBase(pModele);
            c_DAL_reajustement_fonds_dedies.ChargeDepuisBase(pModele);
            c_DAL_regle.ChargeDepuisBase(pModele);
            c_DAL_t_base.ChargeDepuisBase(pModele);
            c_DAL_solde_conges.ChargeDepuisBase(pModele);
            c_DAL_t_prestation.ChargeDepuisBase(pModele);
            c_DAL_t_fonctions.ChargeDepuisBase(pModele);
            c_DAL_synchro_connect_base.ChargeDepuisBase(pModele);
            c_DAL_t_element.ChargeDepuisBase(pModele);
            c_DAL_t_compte.ChargeDepuisBase(pModele);
            c_DAL_t_conge.ChargeDepuisBase(pModele);
            c_DAL_t_cout_projet.ChargeDepuisBase(pModele);
            c_DAL_t_init_appli.ChargeDepuisBase(pModele);
            c_DAL_t_fonction_contact.ChargeDepuisBase(pModele);
            c_DAL_t_info.ChargeDepuisBase(pModele);
            c_DAL_t_limitation_donnees.ChargeDepuisBase(pModele);
            c_DAL_t_journaux.ChargeDepuisBase(pModele);
            c_DAL_t_limitation_interface.ChargeDepuisBase(pModele);
            c_DAL_t_ope_financiere.ChargeDepuisBase(pModele);
            c_DAL_t_etat.ChargeDepuisBase(pModele);
            c_DAL_t_parametre.ChargeDepuisBase(pModele);
            c_DAL_t_projet.ChargeDepuisBase(pModele);
            c_DAL_t_rubriques_temps.ChargeDepuisBase(pModele);
            c_DAL_t_sens_comptes.ChargeDepuisBase(pModele);
            c_DAL_type_fonct_compta.ChargeDepuisBase(pModele);
            c_DAL_type_base_mae.ChargeDepuisBase(pModele);
            c_DAL_t_objets_interface.ChargeDepuisBase(pModele);
            c_DAL_utilisateur.ChargeDepuisBase(pModele);
            c_DAL_vehicules.ChargeDepuisBase(pModele);
            c_DAL_validation_temps_perso.ChargeDepuisBase(pModele);
            c_DAL_t_maj.ChargeDepuisBase(pModele);
            c_DAL_validation_temps_semaine.ChargeDepuisBase(pModele);
            c_DAL_temps_travail_annuel.ChargeDepuisBase(pModele);
            c_DAL_comment_demandes.ChargeDepuisBase(pModele);
            c_DAL_fenetre.ChargeDepuisBase(pModele);
            c_DAL_arretes.ChargeDepuisBase(pModele);
            c_DAL_forfait_personnel.ChargeDepuisBase(pModele);
            c_DAL_carac.ChargeDepuisBase(pModele);
            c_DAL_basemae.ChargeDepuisBase(pModele);
            c_DAL_comptes_non_affichables.ChargeDepuisBase(pModele);
            c_DAL_convention.ChargeDepuisBase(pModele);
            c_DAL_contacts.ChargeDepuisBase(pModele);
            c_DAL_etats.ChargeDepuisBase(pModele);
            c_DAL_infos_base.ChargeDepuisBase(pModele);
            c_DAL_fonctions_perso.ChargeDepuisBase(pModele);
            c_DAL_limitation_donnees.ChargeDepuisBase(pModele);
            c_DAL_init_appli.ChargeDepuisBase(pModele);
            c_DAL_limitation_fenetre.ChargeDepuisBase(pModele);
            c_DAL_mensuel.ChargeDepuisBase(pModele);
            c_DAL_param_compte.ChargeDepuisBase(pModele);
            c_DAL_parametrage.ChargeDepuisBase(pModele);
            c_DAL_poste.ChargeDepuisBase(pModele);
            c_DAL_param_conservatoire.ChargeDepuisBase(pModele);
            c_DAL_taux_km_reels.ChargeDepuisBase(pModele);
            c_DAL_rubriques_temps.ChargeDepuisBase(pModele);
            c_DAL_taux_km_prevus.ChargeDepuisBase(pModele);
            c_DAL_valeur_param.ChargeDepuisBase(pModele);
            c_DAL_valeur_carac.ChargeDepuisBase(pModele);
            c_DAL_temps_perso_generaux.ChargeDepuisBase(pModele);
            c_DAL_convention_regle.ChargeDepuisBase(pModele);
            c_DAL_tb_sage.ChargeDepuisBase(pModele);
            c_DAL_calendrier.ChargeDepuisBase(pModele);
            c_DAL_conge_prevu.ChargeDepuisBase(pModele);
            c_DAL_conge_realise.ChargeDepuisBase(pModele);
            c_DAL_deplacements_realises.ChargeDepuisBase(pModele);
            c_DAL_corbeille_deplacement.ChargeDepuisBase(pModele);
            c_DAL_frais_mission.ChargeDepuisBase(pModele);
            c_DAL_planning.ChargeDepuisBase(pModele);
            c_DAL_elements.ChargeDepuisBase(pModele);
            c_DAL_carac_element.ChargeDepuisBase(pModele);
            c_DAL_lien_sage.ChargeDepuisBase(pModele);
            c_DAL_financement_prevus.ChargeDepuisBase(pModele);
            c_DAL_concepteurs.ChargeDepuisBase(pModele);
            c_DAL_deplacements_prevus.ChargeDepuisBase(pModele);
            c_DAL_facture.ChargeDepuisBase(pModele);
            c_DAL_lien_arretes.ChargeDepuisBase(pModele);
            c_DAL_limitation_interface.ChargeDepuisBase(pModele);
            c_DAL_intervenants.ChargeDepuisBase(pModele);
            c_DAL_projets.ChargeDepuisBase(pModele);
            c_DAL_prestation_prevus.ChargeDepuisBase(pModele);
            c_DAL_temps_realises.ChargeDepuisBase(pModele);
            c_DAL_temps_intervenants.ChargeDepuisBase(pModele);
            c_DAL_taux.ChargeDepuisBase(pModele);
            c_DAL_report.ChargeDepuisBase(pModele);
            c_DAL_ventilation_deplacement.ChargeDepuisBase(pModele);
            c_DAL_ventilation_deplacement_csa.ChargeDepuisBase(pModele);
            c_DAL_taux_fin.ChargeDepuisBase(pModele);
            c_DAL_objets_interface.ChargeDepuisBase(pModele);
            c_DAL_operations.ChargeDepuisBase(pModele);
            c_DAL_comment_financeur.ChargeDepuisBase(pModele);
            c_DAL_comment_vehicule.ChargeDepuisBase(pModele);
            c_DAL_comment_fournisseur.ChargeDepuisBase(pModele);
            c_DAL_filtre_car_t_elements.ChargeDepuisBase(pModele);
            c_DAL_profiles.ChargeDepuisBase(pModele);
            c_DAL_comment_arretes.ChargeDepuisBase(pModele);
            c_DAL_comment_element.ChargeDepuisBase(pModele);
            c_DAL_qualif_temps.ChargeDepuisBase(pModele);
            c_DAL_operations_reportees.ChargeDepuisBase(pModele);

            double l_DureeChargement = (DateTime.Now - l_Temps).TotalSeconds;
            l_Temps = DateTime.Now;

            foreach (clg_ObjetBase obj in pModele.ListeObjets)
            {
                try
                {
                    obj.CreeLiens();
                    obj.CreeReferences();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            double l_DureeCreationLiens = (DateTime.Now - l_Temps).TotalSeconds;            
        }
        #endregion
    }
}
