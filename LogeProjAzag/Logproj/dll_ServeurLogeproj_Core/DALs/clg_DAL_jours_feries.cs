namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : jours_feries </summary>
public class clg_DAL_jours_feries : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_jours_feries(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT jrf_cn, jrf_d_date, jrf_n_heures, jrf_a_comment FROM jours_feries";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_jours_feries l_Objet;
			l_Objet = new clg_jours_feries(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : double.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_jours_feries l_Objet;
        if (pModele.Listejours_feries.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_jours_feries) pModele.Listejours_feries.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_jours_feries(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : double.Parse(l_Chaines[3]), l_Chaines[4]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_jours_feries l_Objet = (clg_jours_feries)pObjet;
        string l_ordreSQL = "UPDATE jours_feries SET jrf_cn=@jrf_cn, jrf_d_date=@jrf_d_date, jrf_n_heures=@jrf_n_heures, jrf_a_comment=@jrf_a_comment WHERE jrf_cn= @jrf_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet jours_feries");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_jours_feries l_Objet = (clg_jours_feries)pObjet;
        string l_ordreSQL = "INSERT INTO jours_feries (jrf_cn, jrf_d_date, jrf_n_heures, jrf_a_comment) VALUES (@jrf_cn, @jrf_d_date, @jrf_n_heures, @jrf_a_comment);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet jours_feries");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_jours_feries l_Objet = (clg_jours_feries)pObjet;
        string l_ordreSQL = "DELETE FROM jours_feries WHERE jrf_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet jours_feries");
    }

    private void InjecterDonnees(clg_jours_feries pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@jrf_cn");
		pValParams.Add(pObjet.jrf_cn);
		pParams.Add("@jrf_d_date");
		pValParams.Add(pObjet.jrf_d_date);
		pParams.Add("@jrf_n_heures");
		pValParams.Add(pObjet.jrf_n_heures);
		pParams.Add("@jrf_a_comment");
		pValParams.Add(pObjet.jrf_a_comment);

    }

#endregion
}
}
