namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : t_element </summary>
public class clg_DAL_t_element : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_t_element(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT t_ele_cn, t_ele_a_lib, t_ele_niv_min, t_ele_niv_max, t_ele_a_chemin_icone, t_ele_a_lib_msg, t_ele_a_chemin_icone_grise FROM t_element";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_t_element l_Objet;
			l_Objet = new clg_t_element(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_t_element l_Objet;
        if (pModele.Listet_element.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_t_element) pModele.Listet_element.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_t_element(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5], l_Chaines[6], l_Chaines[7]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_t_element l_Objet = (clg_t_element)pObjet;
        string l_ordreSQL = "UPDATE t_element SET t_ele_cn=@t_ele_cn, t_ele_a_lib=@t_ele_a_lib, t_ele_niv_min=@t_ele_niv_min, t_ele_niv_max=@t_ele_niv_max, t_ele_a_chemin_icone=@t_ele_a_chemin_icone, t_ele_a_lib_msg=@t_ele_a_lib_msg, t_ele_a_chemin_icone_grise=@t_ele_a_chemin_icone_grise WHERE t_ele_cn= @t_ele_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet t_element");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_t_element l_Objet = (clg_t_element)pObjet;
        string l_ordreSQL = "INSERT INTO t_element (t_ele_cn, t_ele_a_lib, t_ele_niv_min, t_ele_niv_max, t_ele_a_chemin_icone, t_ele_a_lib_msg, t_ele_a_chemin_icone_grise) VALUES (@t_ele_cn, @t_ele_a_lib, @t_ele_niv_min, @t_ele_niv_max, @t_ele_a_chemin_icone, @t_ele_a_lib_msg, @t_ele_a_chemin_icone_grise);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet t_element");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_t_element l_Objet = (clg_t_element)pObjet;
        string l_ordreSQL = "DELETE FROM t_element WHERE t_ele_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet t_element");
    }

    private void InjecterDonnees(clg_t_element pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@t_ele_cn");
		pValParams.Add(pObjet.t_ele_cn);
		pParams.Add("@t_ele_a_lib");
		pValParams.Add(pObjet.t_ele_a_lib);
		pParams.Add("@t_ele_niv_min");
		pValParams.Add(pObjet.t_ele_niv_min);
		pParams.Add("@t_ele_niv_max");
		pValParams.Add(pObjet.t_ele_niv_max);
		pParams.Add("@t_ele_a_chemin_icone");
		pValParams.Add(pObjet.t_ele_a_chemin_icone);
		pParams.Add("@t_ele_a_lib_msg");
		pValParams.Add(pObjet.t_ele_a_lib_msg);
		pParams.Add("@t_ele_a_chemin_icone_grise");
		pValParams.Add(pObjet.t_ele_a_chemin_icone_grise);

    }


#endregion
}
}
