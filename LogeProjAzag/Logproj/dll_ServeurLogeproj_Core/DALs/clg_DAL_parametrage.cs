namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : parametrage </summary>
public class clg_DAL_parametrage : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_parametrage(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT prm_cn, prm_a_lib, prm_a_desc, prm_b_activation, prm_t_prm_cn, prm_b_act_util FROM parametrage";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_parametrage l_Objet;
			l_Objet = new clg_parametrage(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3)=="True", l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5)=="True");

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_parametrage l_Objet;
        if (pModele.Listeparametrage.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_parametrage) pModele.Listeparametrage.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_parametrage(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4]=="True", l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6]=="True");

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_parametrage l_Objet = (clg_parametrage)pObjet;
        string l_ordreSQL = "UPDATE parametrage SET prm_cn=@prm_cn, prm_a_lib=@prm_a_lib, prm_a_desc=@prm_a_desc, prm_b_activation=@prm_b_activation, prm_t_prm_cn=@prm_t_prm_cn, prm_b_act_util=@prm_b_act_util WHERE prm_cn= @prm_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet parametrage");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_parametrage l_Objet = (clg_parametrage)pObjet;
        string l_ordreSQL = "INSERT INTO parametrage (prm_cn, prm_a_lib, prm_a_desc, prm_b_activation, prm_t_prm_cn, prm_b_act_util) VALUES (@prm_cn, @prm_a_lib, @prm_a_desc, @prm_b_activation, @prm_t_prm_cn, @prm_b_act_util);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet parametrage");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_parametrage l_Objet = (clg_parametrage)pObjet;
        string l_ordreSQL = "DELETE FROM parametrage WHERE prm_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet parametrage");
    }

    private void InjecterDonnees(clg_parametrage pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@prm_cn");
		pValParams.Add(pObjet.prm_cn);
		pParams.Add("@prm_a_lib");
		pValParams.Add(pObjet.prm_a_lib);
		pParams.Add("@prm_a_desc");
		pValParams.Add(pObjet.prm_a_desc);
		pParams.Add("@prm_b_activation");
		pValParams.Add(pObjet.prm_b_activation);
		pParams.Add("@prm_t_prm_cn");
		pValParams.Add(pObjet.t_parametre != null ? pObjet.t_parametre.t_prm_cn : (Int64?)null);
		pParams.Add("@prm_b_act_util");
		pValParams.Add(pObjet.prm_b_act_util);

    }

#endregion
}
}
