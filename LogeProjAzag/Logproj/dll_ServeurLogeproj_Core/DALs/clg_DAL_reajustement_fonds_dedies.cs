namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : reajustement_fonds_dedies </summary>
public class clg_DAL_reajustement_fonds_dedies : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_reajustement_fonds_dedies(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT rfd_prj_cn, rfd_acp_cn, rfd_ann_cn, rfd_n_montant, rfd_a_comment FROM reajustement_fonds_dedies";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_reajustement_fonds_dedies l_Objet;
			l_Objet = new clg_reajustement_fonds_dedies(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : double.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_reajustement_fonds_dedies l_Objet;
        if (pModele.Listereajustement_fonds_dedies.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_reajustement_fonds_dedies) pModele.Listereajustement_fonds_dedies.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_reajustement_fonds_dedies(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : double.Parse(l_Chaines[4]), l_Chaines[5]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_reajustement_fonds_dedies l_Objet = (clg_reajustement_fonds_dedies)pObjet;
        string l_ordreSQL = "UPDATE reajustement_fonds_dedies SET rfd_prj_cn=@rfd_prj_cn, rfd_acp_cn=@rfd_acp_cn, rfd_ann_cn=@rfd_ann_cn, rfd_n_montant=@rfd_n_montant, rfd_a_comment=@rfd_a_comment WHERE rfd_prj_cn= @rfd_prj_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet reajustement_fonds_dedies");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_reajustement_fonds_dedies l_Objet = (clg_reajustement_fonds_dedies)pObjet;
        string l_ordreSQL = "INSERT INTO reajustement_fonds_dedies (rfd_prj_cn, rfd_acp_cn, rfd_ann_cn, rfd_n_montant, rfd_a_comment) VALUES (@rfd_prj_cn, @rfd_acp_cn, @rfd_ann_cn, @rfd_n_montant, @rfd_a_comment);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet reajustement_fonds_dedies");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_reajustement_fonds_dedies l_Objet = (clg_reajustement_fonds_dedies)pObjet;
        string l_ordreSQL = "DELETE FROM reajustement_fonds_dedies WHERE rfd_prj_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet reajustement_fonds_dedies");
    }

    private void InjecterDonnees(clg_reajustement_fonds_dedies pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@rfd_prj_cn");
		pValParams.Add(pObjet.rfd_prj_cn);
		pParams.Add("@rfd_acp_cn");
		pValParams.Add(pObjet.rfd_acp_cn);
		pParams.Add("@rfd_ann_cn");
		pValParams.Add(pObjet.rfd_ann_cn);
		pParams.Add("@rfd_n_montant");
		pValParams.Add(pObjet.rfd_n_montant);
		pParams.Add("@rfd_a_comment");
		pValParams.Add(pObjet.rfd_a_comment);

    }

#endregion
}
}
