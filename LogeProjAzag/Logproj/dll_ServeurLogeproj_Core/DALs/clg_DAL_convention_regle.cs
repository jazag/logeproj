namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : convention_regle </summary>
public class clg_DAL_convention_regle : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_convention_regle(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cvr_cn, cvr_cvn_cn, cvr_reg_cn, cvr_n_valeur, cvr_b_bloquant FROM convention_regle";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_convention_regle l_Objet;
			l_Objet = new clg_convention_regle(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4)=="True");

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_convention_regle l_Objet;
        if (pModele.Listeconvention_regle.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_convention_regle) pModele.Listeconvention_regle.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_convention_regle(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4], l_Chaines[5]=="True");

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_convention_regle l_Objet = (clg_convention_regle)pObjet;
        string l_ordreSQL = "UPDATE convention_regle SET cvr_cn=@cvr_cn, cvr_cvn_cn=@cvr_cvn_cn, cvr_reg_cn=@cvr_reg_cn, cvr_n_valeur=@cvr_n_valeur, cvr_b_bloquant=@cvr_b_bloquant WHERE cvr_cn= @cvr_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet convention_regle");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_convention_regle l_Objet = (clg_convention_regle)pObjet;
        string l_ordreSQL = "INSERT INTO convention_regle (cvr_cn, cvr_cvn_cn, cvr_reg_cn, cvr_n_valeur, cvr_b_bloquant) VALUES (@cvr_cn, @cvr_cvn_cn, @cvr_reg_cn, @cvr_n_valeur, @cvr_b_bloquant);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet convention_regle");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_convention_regle l_Objet = (clg_convention_regle)pObjet;
        string l_ordreSQL = "DELETE FROM convention_regle WHERE cvr_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet convention_regle");
    }

    private void InjecterDonnees(clg_convention_regle pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@cvr_cn");
		pValParams.Add(pObjet.cvr_cn);
		pParams.Add("@cvr_cvn_cn");
		pValParams.Add(pObjet.convention != null ? pObjet.convention.cvn_cn : (Int64?)null);
		pParams.Add("@cvr_reg_cn");
		pValParams.Add(pObjet.regle != null ? pObjet.regle.reg_cn : (Int64?)null);
		pParams.Add("@cvr_n_valeur");
		pValParams.Add(pObjet.cvr_n_valeur);
		pParams.Add("@cvr_b_bloquant");
		pValParams.Add(pObjet.cvr_b_bloquant);

    }

#endregion
}
}
