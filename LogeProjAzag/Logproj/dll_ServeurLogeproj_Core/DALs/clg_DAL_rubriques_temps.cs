namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : rubriques_temps </summary>
public class clg_DAL_rubriques_temps : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_rubriques_temps(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT rbr_cn, rbr_a_libel, rbr_t_rbr_cn, rbr_n_archive, rbr_n_ordre FROM rubriques_temps";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_rubriques_temps l_Objet;
			l_Objet = new clg_rubriques_temps(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_rubriques_temps l_Objet;
        if (pModele.Listerubriques_temps.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_rubriques_temps) pModele.Listerubriques_temps.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_rubriques_temps(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_rubriques_temps l_Objet = (clg_rubriques_temps)pObjet;
        string l_ordreSQL = "UPDATE rubriques_temps SET rbr_cn=@rbr_cn, rbr_a_libel=@rbr_a_libel, rbr_t_rbr_cn=@rbr_t_rbr_cn, rbr_n_archive=@rbr_n_archive, rbr_n_ordre=@rbr_n_ordre WHERE rbr_cn= @rbr_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet rubriques_temps");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_rubriques_temps l_Objet = (clg_rubriques_temps)pObjet;
        string l_ordreSQL = "INSERT INTO rubriques_temps (rbr_cn, rbr_a_libel, rbr_t_rbr_cn, rbr_n_archive, rbr_n_ordre) VALUES (@rbr_cn, @rbr_a_libel, @rbr_t_rbr_cn, @rbr_n_archive, @rbr_n_ordre);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet rubriques_temps");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_rubriques_temps l_Objet = (clg_rubriques_temps)pObjet;
        string l_ordreSQL = "DELETE FROM rubriques_temps WHERE rbr_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet rubriques_temps");
    }

    private void InjecterDonnees(clg_rubriques_temps pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@rbr_cn");
		pValParams.Add(pObjet.rbr_cn);
		pParams.Add("@rbr_a_libel");
		pValParams.Add(pObjet.rbr_a_libel);
		pParams.Add("@rbr_t_rbr_cn");
		pValParams.Add(pObjet.t_rubriques_temps != null ? pObjet.t_rubriques_temps.t_rbr_cn : (Int64?)null);
		pParams.Add("@rbr_n_archive");
		pValParams.Add(pObjet.rbr_n_archive);
		pParams.Add("@rbr_n_ordre");
		pValParams.Add(pObjet.rbr_n_ordre);

    }

#endregion
}
}
