namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : regle </summary>
public class clg_DAL_regle : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_regle(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT reg_cn, reg_a_lib, reg_n_t_forfait, reg_n_bloquant_possible FROM regle";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_regle l_Objet;
			l_Objet = new clg_regle(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3)=="True");

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_regle l_Objet;
        if (pModele.Listeregle.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_regle) pModele.Listeregle.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_regle(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4]=="True");

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_regle l_Objet = (clg_regle)pObjet;
        string l_ordreSQL = "UPDATE regle SET reg_cn=@reg_cn, reg_a_lib=@reg_a_lib, reg_n_t_forfait=@reg_n_t_forfait, reg_n_bloquant_possible=@reg_n_bloquant_possible WHERE reg_cn= @reg_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet regle");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_regle l_Objet = (clg_regle)pObjet;
        string l_ordreSQL = "INSERT INTO regle (reg_cn, reg_a_lib, reg_n_t_forfait, reg_n_bloquant_possible) VALUES (@reg_cn, @reg_a_lib, @reg_n_t_forfait, @reg_n_bloquant_possible);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet regle");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_regle l_Objet = (clg_regle)pObjet;
        string l_ordreSQL = "DELETE FROM regle WHERE reg_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet regle");
    }

    private void InjecterDonnees(clg_regle pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@reg_cn");
		pValParams.Add(pObjet.reg_cn);
		pParams.Add("@reg_a_lib");
		pValParams.Add(pObjet.reg_a_lib);
		pParams.Add("@reg_n_t_forfait");
		pValParams.Add(pObjet.reg_n_t_forfait);
		pParams.Add("@reg_n_bloquant_possible");
		pValParams.Add(pObjet.reg_n_bloquant_possible);

    }

#endregion
}
}
