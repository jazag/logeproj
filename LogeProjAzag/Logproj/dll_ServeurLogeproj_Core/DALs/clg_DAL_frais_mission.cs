namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : frais_mission </summary>
public class clg_DAL_frais_mission : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_frais_mission(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT frm_cn, frm_cfr_cn, frm_a_objet, frm_n_montant, frm_dpr_cn FROM frais_mission";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_frais_mission l_Objet;
			l_Objet = new clg_frais_mission(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3) == "" ? -1 : double.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_frais_mission l_Objet;
        if (pModele.Listefrais_mission.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_frais_mission) pModele.Listefrais_mission.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_frais_mission(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3], l_Chaines[4] == "" ? -1 : double.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_frais_mission l_Objet = (clg_frais_mission)pObjet;
        string l_ordreSQL = "UPDATE frais_mission SET frm_cn=@frm_cn, frm_cfr_cn=@frm_cfr_cn, frm_a_objet=@frm_a_objet, frm_n_montant=@frm_n_montant, frm_dpr_cn=@frm_dpr_cn WHERE frm_cn= @frm_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet frais_mission");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_frais_mission l_Objet = (clg_frais_mission)pObjet;
        string l_ordreSQL = "INSERT INTO frais_mission (frm_cn, frm_cfr_cn, frm_a_objet, frm_n_montant, frm_dpr_cn) VALUES (@frm_cn, @frm_cfr_cn, @frm_a_objet, @frm_n_montant, @frm_dpr_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet frais_mission");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_frais_mission l_Objet = (clg_frais_mission)pObjet;
        string l_ordreSQL = "DELETE FROM frais_mission WHERE frm_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet frais_mission");
    }

    private void InjecterDonnees(clg_frais_mission pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@frm_cn");
		pValParams.Add(pObjet.frm_cn);
		pParams.Add("@frm_cfr_cn");
		pValParams.Add(pObjet.categorie_frais != null ? pObjet.categorie_frais.cfr_cn : (Int64?)null);
		pParams.Add("@frm_a_objet");
		pValParams.Add(pObjet.frm_a_objet);
		pParams.Add("@frm_n_montant");
		pValParams.Add(pObjet.frm_n_montant);
		pParams.Add("@frm_dpr_cn");
		pValParams.Add(pObjet.deplacements_realises != null ? pObjet.deplacements_realises.dpr_cn : (Int64?)null);

    }

#endregion
}
}
