namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : personnels </summary>
public class clg_DAL_personnels : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_personnels(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT per_cn, per_a_nom, per_a_prenom, per_n_archive, per_a_code, per_a_comment, per_d_arrivee, per_d_depart, per_a_tel, per_a_fax, per_a_mail, per_d_debut_saisie FROM personnels";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_personnels l_Objet;
			l_Objet = new clg_personnels(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8), l_rds.Donnee(i, 9), l_rds.Donnee(i, 10), l_rds.Donnee(i, 11) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 11)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_personnels l_Objet;
        if (pModele.Listepersonnels.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_personnels) pModele.Listepersonnels.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_personnels(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5], l_Chaines[6], l_Chaines[7] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[7]), l_Chaines[8] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[8]), l_Chaines[9], l_Chaines[10], l_Chaines[11], l_Chaines[12] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[12]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_personnels l_Objet = (clg_personnels)pObjet;
        string l_ordreSQL = "UPDATE personnels SET per_cn=@per_cn, per_a_nom=@per_a_nom, per_a_prenom=@per_a_prenom, per_n_archive=@per_n_archive, per_a_code=@per_a_code, per_a_comment=@per_a_comment, per_d_arrivee=@per_d_arrivee, per_d_depart=@per_d_depart, per_a_tel=@per_a_tel, per_a_fax=@per_a_fax, per_a_mail=@per_a_mail, per_d_debut_saisie=@per_d_debut_saisie WHERE per_cn= @per_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet personnels");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_personnels l_Objet = (clg_personnels)pObjet;
        string l_ordreSQL = "INSERT INTO personnels (per_cn, per_a_nom, per_a_prenom, per_n_archive, per_a_code, per_a_comment, per_d_arrivee, per_d_depart, per_a_tel, per_a_fax, per_a_mail, per_d_debut_saisie) VALUES (@per_cn, @per_a_nom, @per_a_prenom, @per_n_archive, @per_a_code, @per_a_comment, @per_d_arrivee, @per_d_depart, @per_a_tel, @per_a_fax, @per_a_mail, @per_d_debut_saisie);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet personnels");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_personnels l_Objet = (clg_personnels)pObjet;
        string l_ordreSQL = "DELETE FROM personnels WHERE per_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet personnels");
    }

    private void InjecterDonnees(clg_personnels pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@per_cn");
		pValParams.Add(pObjet.per_cn);
		pParams.Add("@per_a_nom");
		pValParams.Add(pObjet.per_a_nom);
		pParams.Add("@per_a_prenom");
		pValParams.Add(pObjet.per_a_prenom);
		pParams.Add("@per_n_archive");
		pValParams.Add(pObjet.per_n_archive);
		pParams.Add("@per_a_code");
		pValParams.Add(pObjet.per_a_code);
		pParams.Add("@per_a_comment");
		pValParams.Add(pObjet.per_a_comment);
		pParams.Add("@per_d_arrivee");
		pValParams.Add(pObjet.per_d_arrivee);
		pParams.Add("@per_d_depart");
		pValParams.Add(pObjet.per_d_depart);
		pParams.Add("@per_a_tel");
		pValParams.Add(pObjet.per_a_tel);
		pParams.Add("@per_a_fax");
		pValParams.Add(pObjet.per_a_fax);
		pParams.Add("@per_a_mail");
		pValParams.Add(pObjet.per_a_mail);
		pParams.Add("@per_d_debut_saisie");
		pValParams.Add(pObjet.per_d_debut_saisie);

    }

#endregion
}
}
