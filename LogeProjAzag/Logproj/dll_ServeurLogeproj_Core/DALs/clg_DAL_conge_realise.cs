namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : conge_realise </summary>
public class clg_DAL_conge_realise : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_conge_realise(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cgr_cn, cgr_cal_d_date, cgr_per_cn, cgr_t_cge_cn, cgr_n_nombre FROM conge_realise";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_conge_realise l_Objet;
			l_Objet = new clg_conge_realise(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : double.Parse(l_rds.Donnee(i, 4)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_conge_realise l_Objet;
        if (pModele.Listeconge_realise.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_conge_realise) pModele.Listeconge_realise.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_conge_realise(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : double.Parse(l_Chaines[5]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_conge_realise l_Objet = (clg_conge_realise)pObjet;
        string l_ordreSQL = "UPDATE conge_realise SET cgr_cn=@cgr_cn, cgr_cal_d_date=@cgr_cal_d_date, cgr_per_cn=@cgr_per_cn, cgr_t_cge_cn=@cgr_t_cge_cn, cgr_n_nombre=@cgr_n_nombre WHERE cgr_cn= @cgr_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet conge_realise");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_conge_realise l_Objet = (clg_conge_realise)pObjet;
        string l_ordreSQL = "INSERT INTO conge_realise (cgr_cn, cgr_cal_d_date, cgr_per_cn, cgr_t_cge_cn, cgr_n_nombre) VALUES (@cgr_cn, @cgr_cal_d_date, @cgr_per_cn, @cgr_t_cge_cn, @cgr_n_nombre);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet conge_realise");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_conge_realise l_Objet = (clg_conge_realise)pObjet;
        string l_ordreSQL = "DELETE FROM conge_realise WHERE cgr_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet conge_realise");
    }

    private void InjecterDonnees(clg_conge_realise pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@cgr_cn");
		pValParams.Add(pObjet.cgr_cn);
		pParams.Add("@cgr_cal_d_date");
		pValParams.Add(pObjet.calendrier != null ? pObjet.calendrier.cal_d_date : (DateTime?)null);
		pParams.Add("@cgr_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@cgr_t_cge_cn");
		pValParams.Add(pObjet.t_conge != null ? pObjet.t_conge.t_cge_cn : (Int64?)null);
		pParams.Add("@cgr_n_nombre");
		pValParams.Add(pObjet.cgr_n_nombre);

    }

#endregion
}
}
