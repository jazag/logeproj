namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : param_compte </summary>
public class clg_DAL_param_compte : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_param_compte(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT pct_cn, pct_jou_a_code, pct_a_cpt, pct_tsc_cn, pct_tfc_cn, pct_a_cheminico, pct_a_cmt FROM param_compte";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_param_compte l_Objet;
			l_Objet = new clg_param_compte(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_param_compte l_Objet;
        if (pModele.Listeparam_compte.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_param_compte) pModele.Listeparam_compte.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_param_compte(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4], l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6], l_Chaines[7]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_param_compte l_Objet = (clg_param_compte)pObjet;
        string l_ordreSQL = "UPDATE param_compte SET pct_cn=@pct_cn, pct_jou_a_code=@pct_jou_a_code, pct_a_cpt=@pct_a_cpt, pct_tsc_cn=@pct_tsc_cn, pct_tfc_cn=@pct_tfc_cn, pct_a_cheminico=@pct_a_cheminico, pct_a_cmt=@pct_a_cmt WHERE pct_cn= @pct_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet param_compte");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_param_compte l_Objet = (clg_param_compte)pObjet;
        string l_ordreSQL = "INSERT INTO param_compte (pct_cn, pct_jou_a_code, pct_a_cpt, pct_tsc_cn, pct_tfc_cn, pct_a_cheminico, pct_a_cmt) VALUES (@pct_cn, @pct_jou_a_code, @pct_a_cpt, @pct_tsc_cn, @pct_tfc_cn, @pct_a_cheminico, @pct_a_cmt);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet param_compte");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_param_compte l_Objet = (clg_param_compte)pObjet;
        string l_ordreSQL = "DELETE FROM param_compte WHERE pct_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet param_compte");
    }

    private void InjecterDonnees(clg_param_compte pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@pct_cn");
		pValParams.Add(pObjet.pct_cn);
		pParams.Add("@pct_jou_a_code");
		pValParams.Add(pObjet.pct_jou_a_code);
		pParams.Add("@pct_a_cpt");
		pValParams.Add(pObjet.pct_a_cpt);
		pParams.Add("@pct_tsc_cn");
		pValParams.Add(pObjet.pct_tsc_cn);
		pParams.Add("@pct_tfc_cn");
		pValParams.Add(pObjet.type_fonct_compta != null ? pObjet.type_fonct_compta.tfc_cn : (Int64?)null);
		pParams.Add("@pct_a_cheminico");
		pValParams.Add(pObjet.pct_a_cheminico);
		pParams.Add("@pct_a_cmt");
		pValParams.Add(pObjet.pct_a_cmt);

    }

#endregion
}
}
