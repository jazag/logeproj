namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : deplacements_prevus </summary>
public class clg_DAL_deplacements_prevus : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_deplacements_prevus(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT dep_cn, dep_veh_cn, dep_n_nbkm, dep_d_debut, dep_d_fin, dep_n_montant, dep_tkp_cn, dep_ele_cn FROM deplacements_prevus";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_deplacements_prevus l_Objet;
			l_Objet = new clg_deplacements_prevus(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : double.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : double.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 7)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_deplacements_prevus l_Objet;
        if (pModele.Listedeplacements_prevus.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_deplacements_prevus) pModele.Listedeplacements_prevus.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_deplacements_prevus(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : double.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : double.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : Int64.Parse(l_Chaines[8]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_deplacements_prevus l_Objet = (clg_deplacements_prevus)pObjet;
        string l_ordreSQL = "UPDATE deplacements_prevus SET dep_cn=@dep_cn, dep_veh_cn=@dep_veh_cn, dep_n_nbkm=@dep_n_nbkm, dep_d_debut=@dep_d_debut, dep_d_fin=@dep_d_fin, dep_n_montant=@dep_n_montant, dep_tkp_cn=@dep_tkp_cn, dep_ele_cn=@dep_ele_cn WHERE dep_cn= @dep_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet deplacements_prevus");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_deplacements_prevus l_Objet = (clg_deplacements_prevus)pObjet;
        string l_ordreSQL = "INSERT INTO deplacements_prevus (dep_cn, dep_veh_cn, dep_n_nbkm, dep_d_debut, dep_d_fin, dep_n_montant, dep_tkp_cn, dep_ele_cn) VALUES (@dep_cn, @dep_veh_cn, @dep_n_nbkm, @dep_d_debut, @dep_d_fin, @dep_n_montant, @dep_tkp_cn, @dep_ele_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet deplacements_prevus");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_deplacements_prevus l_Objet = (clg_deplacements_prevus)pObjet;
        string l_ordreSQL = "DELETE FROM deplacements_prevus WHERE dep_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet deplacements_prevus");
    }

    private void InjecterDonnees(clg_deplacements_prevus pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@dep_cn");
		pValParams.Add(pObjet.dep_cn);
		pParams.Add("@dep_veh_cn");
		pValParams.Add(pObjet.vehicules != null ? pObjet.vehicules.veh_cn : (Int64?)null);
		pParams.Add("@dep_n_nbkm");
		pValParams.Add(pObjet.dep_n_nbkm);
		pParams.Add("@dep_d_debut");
		pValParams.Add(pObjet.dep_d_debut);
		pParams.Add("@dep_d_fin");
		pValParams.Add(pObjet.dep_d_fin);
		pParams.Add("@dep_n_montant");
		pValParams.Add(pObjet.dep_n_montant);
		pParams.Add("@dep_tkp_cn");
		pValParams.Add(pObjet.taux_km_prevus != null ? pObjet.taux_km_prevus.tkp_cn : (Int64?)null);
		pParams.Add("@dep_ele_cn");
		pValParams.Add(pObjet.elements != null ? pObjet.elements.ele_cn : (Int64?)null);

    }

#endregion
}
}
