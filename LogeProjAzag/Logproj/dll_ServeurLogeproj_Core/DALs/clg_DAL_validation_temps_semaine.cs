namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : validation_temps_semaine </summary>
public class clg_DAL_validation_temps_semaine : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_validation_temps_semaine(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT vts_cn, vts_n_nb_heure_sup, vts_n_validation_ok, vts_sem_cn, vts_per_cn, vts_n_recup_possible, vts_n_heures_recuperee, vts_n_tot_heures, vts_n_modulation, vts_n_ecart_theorique, vts_n_total_ok FROM validation_temps_semaine";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_validation_temps_semaine l_Objet;
			l_Objet = new clg_validation_temps_semaine(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : double.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : double.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : double.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : double.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : double.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? -1 : double.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 10)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_validation_temps_semaine l_Objet;
        if (pModele.Listevalidation_temps_semaine.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_validation_temps_semaine) pModele.Listevalidation_temps_semaine.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_validation_temps_semaine(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : double.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : double.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : double.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : double.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : double.Parse(l_Chaines[9]), l_Chaines[10] == "" ? -1 : double.Parse(l_Chaines[10]), l_Chaines[11] == "" ? -1 : Int64.Parse(l_Chaines[11]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_validation_temps_semaine l_Objet = (clg_validation_temps_semaine)pObjet;
        string l_ordreSQL = "UPDATE validation_temps_semaine SET vts_cn=@vts_cn, vts_n_nb_heure_sup=@vts_n_nb_heure_sup, vts_n_validation_ok=@vts_n_validation_ok, vts_sem_cn=@vts_sem_cn, vts_per_cn=@vts_per_cn, vts_n_recup_possible=@vts_n_recup_possible, vts_n_heures_recuperee=@vts_n_heures_recuperee, vts_n_tot_heures=@vts_n_tot_heures, vts_n_modulation=@vts_n_modulation, vts_n_ecart_theorique=@vts_n_ecart_theorique, vts_n_total_ok=@vts_n_total_ok WHERE vts_cn= @vts_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet validation_temps_semaine");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_validation_temps_semaine l_Objet = (clg_validation_temps_semaine)pObjet;
        string l_ordreSQL = "INSERT INTO validation_temps_semaine (vts_cn, vts_n_nb_heure_sup, vts_n_validation_ok, vts_sem_cn, vts_per_cn, vts_n_recup_possible, vts_n_heures_recuperee, vts_n_tot_heures, vts_n_modulation, vts_n_ecart_theorique, vts_n_total_ok) VALUES (@vts_cn, @vts_n_nb_heure_sup, @vts_n_validation_ok, @vts_sem_cn, @vts_per_cn, @vts_n_recup_possible, @vts_n_heures_recuperee, @vts_n_tot_heures, @vts_n_modulation, @vts_n_ecart_theorique, @vts_n_total_ok);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet validation_temps_semaine");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_validation_temps_semaine l_Objet = (clg_validation_temps_semaine)pObjet;
        string l_ordreSQL = "DELETE FROM validation_temps_semaine WHERE vts_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet validation_temps_semaine");
    }

    private void InjecterDonnees(clg_validation_temps_semaine pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@vts_cn");
		pValParams.Add(pObjet.vts_cn);
		pParams.Add("@vts_n_nb_heure_sup");
		pValParams.Add(pObjet.vts_n_nb_heure_sup);
		pParams.Add("@vts_n_validation_ok");
		pValParams.Add(pObjet.vts_n_validation_ok);
		pParams.Add("@vts_sem_cn");
		pValParams.Add(pObjet.vts_sem_cn);
		pParams.Add("@vts_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@vts_n_recup_possible");
		pValParams.Add(pObjet.vts_n_recup_possible);
		pParams.Add("@vts_n_heures_recuperee");
		pValParams.Add(pObjet.vts_n_heures_recuperee);
		pParams.Add("@vts_n_tot_heures");
		pValParams.Add(pObjet.vts_n_tot_heures);
		pParams.Add("@vts_n_modulation");
		pValParams.Add(pObjet.vts_n_modulation);
		pParams.Add("@vts_n_ecart_theorique");
		pValParams.Add(pObjet.vts_n_ecart_theorique);
		pParams.Add("@vts_n_total_ok");
		pValParams.Add(pObjet.vts_n_total_ok);

    }

#endregion
}
}
