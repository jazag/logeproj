namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : infos_base </summary>
public class clg_DAL_infos_base : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_infos_base(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ifb_cn, ifb_a_guid, ifb_a_nom, ifb_csr_cn, ifb_a_version, ifb_t_bas_cn, ifb_n_synchro, ifb_n_der_id_sync, ifb_t_maj_cn, ifb_a_ip, ifb_m_msg_maj, ifb_n_desynchro FROM infos_base";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_infos_base l_Objet;
			l_Objet = new clg_infos_base(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9), l_rds.Donnee(i, 10), l_rds.Donnee(i, 11) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 11)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_infos_base l_Objet;
        if (pModele.Listeinfos_base.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_infos_base) pModele.Listeinfos_base.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_infos_base(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5], l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : Int64.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10], l_Chaines[11], l_Chaines[12] == "" ? -1 : Int64.Parse(l_Chaines[12]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_infos_base l_Objet = (clg_infos_base)pObjet;
        string l_ordreSQL = "UPDATE infos_base SET ifb_cn=@ifb_cn, ifb_a_guid=@ifb_a_guid, ifb_a_nom=@ifb_a_nom, ifb_csr_cn=@ifb_csr_cn, ifb_a_version=@ifb_a_version, ifb_t_bas_cn=@ifb_t_bas_cn, ifb_n_synchro=@ifb_n_synchro, ifb_n_der_id_sync=@ifb_n_der_id_sync, ifb_t_maj_cn=@ifb_t_maj_cn, ifb_a_ip=@ifb_a_ip, ifb_m_msg_maj=@ifb_m_msg_maj, ifb_n_desynchro=@ifb_n_desynchro WHERE ifb_cn= @ifb_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet infos_base");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_infos_base l_Objet = (clg_infos_base)pObjet;
        string l_ordreSQL = "INSERT INTO infos_base (ifb_cn, ifb_a_guid, ifb_a_nom, ifb_csr_cn, ifb_a_version, ifb_t_bas_cn, ifb_n_synchro, ifb_n_der_id_sync, ifb_t_maj_cn, ifb_a_ip, ifb_m_msg_maj, ifb_n_desynchro) VALUES (@ifb_cn, @ifb_a_guid, @ifb_a_nom, @ifb_csr_cn, @ifb_a_version, @ifb_t_bas_cn, @ifb_n_synchro, @ifb_n_der_id_sync, @ifb_t_maj_cn, @ifb_a_ip, @ifb_m_msg_maj, @ifb_n_desynchro);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet infos_base");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_infos_base l_Objet = (clg_infos_base)pObjet;
        string l_ordreSQL = "DELETE FROM infos_base WHERE ifb_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet infos_base");
    }

    private void InjecterDonnees(clg_infos_base pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ifb_cn");
		pValParams.Add(pObjet.ifb_cn);
		pParams.Add("@ifb_a_guid");
		pValParams.Add(pObjet.ifb_a_guid);
		pParams.Add("@ifb_a_nom");
		pValParams.Add(pObjet.ifb_a_nom);
		pParams.Add("@ifb_csr_cn");
		pValParams.Add(pObjet.infos_conservatoire != null ? pObjet.infos_conservatoire.csr_cn : (Int64?)null);
		pParams.Add("@ifb_a_version");
		pValParams.Add(pObjet.ifb_a_version);
		pParams.Add("@ifb_t_bas_cn");
		pValParams.Add(pObjet.t_base != null ? pObjet.t_base.t_bas_cn : (Int64?)null);
		pParams.Add("@ifb_n_synchro");
		pValParams.Add(pObjet.ifb_n_synchro);
		pParams.Add("@ifb_n_der_id_sync");
		pValParams.Add(pObjet.ifb_n_der_id_sync);
		pParams.Add("@ifb_t_maj_cn");
		pValParams.Add(pObjet.t_maj != null ? pObjet.t_maj.t_maj_cn : (Int64?)null);
		pParams.Add("@ifb_a_ip");
		pValParams.Add(pObjet.ifb_a_ip);
		pParams.Add("@ifb_m_msg_maj");
		pValParams.Add(pObjet.ifb_m_msg_maj);
		pParams.Add("@ifb_n_desynchro");
		pValParams.Add(pObjet.ifb_n_desynchro);

    }

#endregion
}
}
