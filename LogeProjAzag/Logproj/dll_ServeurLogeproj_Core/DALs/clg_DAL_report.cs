namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : report </summary>
public class clg_DAL_report : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_report(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT rpt_ope_ele_cn, rpt_ann_cn, rpt_tit_cn, rpt_n_heures, rpt_cn FROM report";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_report l_Objet;
			l_Objet = new clg_report(pModele, Int64.Parse(l_rds.Donnee(i, 4)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 0) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : double.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_report l_Objet;
        if (pModele.Listereport.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_report) pModele.Listereport.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_report(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[5]), l_Chaines[1] == "" ? -1 : Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : double.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_report l_Objet = (clg_report)pObjet;
        string l_ordreSQL = "UPDATE report SET rpt_ope_ele_cn=@rpt_ope_ele_cn, rpt_ann_cn=@rpt_ann_cn, rpt_tit_cn=@rpt_tit_cn, rpt_n_heures=@rpt_n_heures, rpt_cn=@rpt_cn WHERE rpt_cn= @rpt_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet report");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_report l_Objet = (clg_report)pObjet;
        string l_ordreSQL = "INSERT INTO report (rpt_ope_ele_cn, rpt_ann_cn, rpt_tit_cn, rpt_n_heures, rpt_cn) VALUES (@rpt_ope_ele_cn, @rpt_ann_cn, @rpt_tit_cn, @rpt_n_heures, @rpt_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet report");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_report l_Objet = (clg_report)pObjet;
        string l_ordreSQL = "DELETE FROM report WHERE rpt_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet report");
    }

    private void InjecterDonnees(clg_report pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@rpt_ope_ele_cn");
		pValParams.Add(pObjet.operations != null ? pObjet.operations.ele_cn : (Int64?)null);
		pParams.Add("@rpt_ann_cn");
		pValParams.Add(pObjet.annee_civile != null ? pObjet.annee_civile.ann_cn : (Int64?)null);
		pParams.Add("@rpt_tit_cn");
		pValParams.Add(pObjet.temps_intervenants != null ? pObjet.temps_intervenants.tit_cn : (Int64?)null);
		pParams.Add("@rpt_n_heures");
		pValParams.Add(pObjet.rpt_n_heures);
		pParams.Add("@rpt_cn");
		pValParams.Add(pObjet.rpt_cn);

    }

#endregion
}
}
