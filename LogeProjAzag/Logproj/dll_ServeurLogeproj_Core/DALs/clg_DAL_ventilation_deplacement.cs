namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : ventilation_deplacement </summary>
public class clg_DAL_ventilation_deplacement : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_ventilation_deplacement(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT vdr_dpr_cn, vdr_d_daterappro, vdr_ele_cn, dpr_veh_cn, dpr_d_date, dpr_n_nbkm, dpr_n_kmmin, dpr_n_kmmax, dpr_n_montant, dpr_tkr_cn, dpr_a_dest, dpr_a_utilisateur, dpr_a_codeana, dpr_a_objet FROM ventilation_deplacement, deplacements_realises WHERE 1=1 AND dpr_cn = vdr_dpr_cn";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_ventilation_deplacement l_Objet;
			l_Objet = new clg_ventilation_deplacement(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : double.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : double.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : double.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : double.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10), l_rds.Donnee(i, 11), l_rds.Donnee(i, 12), l_rds.Donnee(i, 13));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_ventilation_deplacement l_Objet;
        if (pModele.Listeventilation_deplacement.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_ventilation_deplacement) pModele.Listeventilation_deplacement.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_ventilation_deplacement(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[3]), l_Chaines[1] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : double.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : double.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : double.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : double.Parse(l_Chaines[9]), l_Chaines[10] == "" ? -1 : Int64.Parse(l_Chaines[10]), l_Chaines[11], l_Chaines[12], l_Chaines[13], l_Chaines[14]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_ventilation_deplacement l_Objet = (clg_ventilation_deplacement)pObjet;
        string l_ordreSQL = "UPDATE ventilation_deplacement SET vdr_dpr_cn=@vdr_dpr_cn, vdr_d_daterappro=@vdr_d_daterappro, vdr_ele_cn=@vdr_ele_cn WHERE vdr_dpr_cn= @vdr_dpr_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet ventilation_deplacement");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_ventilation_deplacement l_Objet = (clg_ventilation_deplacement)pObjet;
        string l_ordreSQL = "INSERT INTO ventilation_deplacement (vdr_dpr_cn, vdr_d_daterappro, vdr_ele_cn) VALUES (@vdr_dpr_cn, @vdr_d_daterappro, @vdr_ele_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet ventilation_deplacement");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_ventilation_deplacement l_Objet = (clg_ventilation_deplacement)pObjet;
        string l_ordreSQL = "DELETE FROM ventilation_deplacement WHERE vdr_dpr_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet ventilation_deplacement");
    }

    private void InjecterDonnees(clg_ventilation_deplacement pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@vdr_dpr_cn");
		pValParams.Add(pObjet.dpr_cn);
		pParams.Add("@vdr_d_daterappro");
		pValParams.Add(pObjet.vdr_d_daterappro);
		pParams.Add("@vdr_ele_cn");
		pValParams.Add(pObjet.elements != null ? pObjet.elements.ele_cn : (Int64?)null);

    }

#endregion
}
}
