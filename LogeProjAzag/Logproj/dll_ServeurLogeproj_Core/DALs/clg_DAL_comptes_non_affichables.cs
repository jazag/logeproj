namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : comptes_non_affichables </summary>
public class clg_DAL_comptes_non_affichables : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_comptes_non_affichables(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cna_cn, cna_a_num_compte, cna_tsc_cn, cna_m_desc FROM comptes_non_affichables";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_comptes_non_affichables l_Objet;
			l_Objet = new clg_comptes_non_affichables(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_comptes_non_affichables l_Objet;
        if (pModele.Listecomptes_non_affichables.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_comptes_non_affichables) pModele.Listecomptes_non_affichables.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_comptes_non_affichables(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_comptes_non_affichables l_Objet = (clg_comptes_non_affichables)pObjet;
        string l_ordreSQL = "UPDATE comptes_non_affichables SET cna_cn=@cna_cn, cna_a_num_compte=@cna_a_num_compte, cna_tsc_cn=@cna_tsc_cn, cna_m_desc=@cna_m_desc WHERE cna_cn= @cna_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet comptes_non_affichables");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_comptes_non_affichables l_Objet = (clg_comptes_non_affichables)pObjet;
        string l_ordreSQL = "INSERT INTO comptes_non_affichables (cna_cn, cna_a_num_compte, cna_tsc_cn, cna_m_desc) VALUES (@cna_cn, @cna_a_num_compte, @cna_tsc_cn, @cna_m_desc);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet comptes_non_affichables");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_comptes_non_affichables l_Objet = (clg_comptes_non_affichables)pObjet;
        string l_ordreSQL = "DELETE FROM comptes_non_affichables WHERE cna_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet comptes_non_affichables");
    }

    private void InjecterDonnees(clg_comptes_non_affichables pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@cna_cn");
		pValParams.Add(pObjet.cna_cn);
		pParams.Add("@cna_a_num_compte");
		pValParams.Add(pObjet.cna_a_num_compte);
		pParams.Add("@cna_tsc_cn");
		pValParams.Add(pObjet.t_sens_comptes != null ? pObjet.t_sens_comptes.tsc_cn : (Int64?)null);
		pParams.Add("@cna_m_desc");
		pValParams.Add(pObjet.cna_m_desc);

    }

#endregion
}
}
