namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : projets </summary>
public class clg_DAL_projets : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_projets(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT prj_ele_cn, prj_t_prj_cn, prj_per_cn_resp, prj_t_cout_projet_cn, prj_acp_cn, ele_cn_pere, ele_n_niveau, ele_a_lib, ele_t_ele_cn, ele_a_code, ele_cn_racine, ele_n_archive, ele_d_debut_prev, ele_d_fin_reelle, ele_d_fin_prev, ele_d_debut_reelle, ele_d_archivage, ele_n_visibleetat FROM projets, elements WHERE 1=1 AND ele_cn = prj_ele_cn";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_projets l_Objet;
			l_Objet = new clg_projets(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9), l_rds.Donnee(i, 10) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 11)), l_rds.Donnee(i, 12) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 13)), l_rds.Donnee(i, 14) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 14)), l_rds.Donnee(i, 15) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 15)), l_rds.Donnee(i, 16) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 16)), l_rds.Donnee(i, 17) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 17)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_projets l_Objet;
        if (pModele.Listeprojets.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_projets) pModele.Listeprojets.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_projets(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[5]), l_Chaines[1] == "" ? -1 : Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8], l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10], l_Chaines[11] == "" ? -1 : Int64.Parse(l_Chaines[11]), l_Chaines[12] == "" ? -1 : Int64.Parse(l_Chaines[12]), l_Chaines[13] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[13]), l_Chaines[14] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[14]), l_Chaines[15] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[15]), l_Chaines[16] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[16]), l_Chaines[17] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[17]), l_Chaines[18] == "" ? -1 : Int64.Parse(l_Chaines[18]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_projets l_Objet = (clg_projets)pObjet;
        string l_ordreSQL = "UPDATE projets SET prj_ele_cn=@prj_ele_cn, prj_t_prj_cn=@prj_t_prj_cn, prj_per_cn_resp=@prj_per_cn_resp, prj_t_cout_projet_cn=@prj_t_cout_projet_cn, prj_acp_cn=@prj_acp_cn WHERE prj_ele_cn= @prj_ele_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet projets");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_projets l_Objet = (clg_projets)pObjet;
        string l_ordreSQL = "INSERT INTO projets (prj_ele_cn, prj_t_prj_cn, prj_per_cn_resp, prj_t_cout_projet_cn, prj_acp_cn) VALUES (@prj_ele_cn, @prj_t_prj_cn, @prj_per_cn_resp, @prj_t_cout_projet_cn, @prj_acp_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet projets");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_projets l_Objet = (clg_projets)pObjet;
        string l_ordreSQL = "DELETE FROM projets WHERE prj_ele_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet projets");
    }

    private void InjecterDonnees(clg_projets pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@prj_ele_cn");
		pValParams.Add(pObjet.ele_cn);
		pParams.Add("@prj_t_prj_cn");
		pValParams.Add(pObjet.t_projet != null ? pObjet.t_projet.t_prj_cn : (Int64?)null);
		pParams.Add("@prj_per_cn_resp");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@prj_t_cout_projet_cn");
		pValParams.Add(pObjet.t_cout_projet != null ? pObjet.t_cout_projet.t_cout_projet_cn : (Int64?)null);
		pParams.Add("@prj_acp_cn");
		pValParams.Add(pObjet.prj_acp_cn);

    }

#endregion
}
}
