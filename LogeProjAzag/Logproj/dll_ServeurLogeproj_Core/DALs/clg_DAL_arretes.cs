namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : arretes </summary>
public class clg_DAL_arretes : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_arretes(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ara_cn, ara_a_lib, ara_d_piece, ara_d_limite, ara_acp_cn, ara_n_montant, ara_n_ecno, ara_n_cpt, ara_fin_cn, ara_d_solde, ara_d_engagement, ara_n_archive, ara_n_etat, ara_a_numara, ara_n_engage FROM arretes";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_arretes l_Objet;
			l_Objet = new clg_arretes(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : double.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 11)), l_rds.Donnee(i, 12) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13), l_rds.Donnee(i, 14) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 14)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_arretes l_Objet;
        if (pModele.Listearretes.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_arretes) pModele.Listearretes.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_arretes(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : double.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8], l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[10]), l_Chaines[11] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[11]), l_Chaines[12] == "" ? -1 : Int64.Parse(l_Chaines[12]), l_Chaines[13] == "" ? -1 : Int64.Parse(l_Chaines[13]), l_Chaines[14], l_Chaines[15] == "" ? -1 : Int64.Parse(l_Chaines[15]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_arretes l_Objet = (clg_arretes)pObjet;
        string l_ordreSQL = "UPDATE arretes SET ara_cn=@ara_cn, ara_a_lib=@ara_a_lib, ara_d_piece=@ara_d_piece, ara_d_limite=@ara_d_limite, ara_acp_cn=@ara_acp_cn, ara_n_montant=@ara_n_montant, ara_n_ecno=@ara_n_ecno, ara_n_cpt=@ara_n_cpt, ara_fin_cn=@ara_fin_cn, ara_d_solde=@ara_d_solde, ara_d_engagement=@ara_d_engagement, ara_n_archive=@ara_n_archive, ara_n_etat=@ara_n_etat, ara_a_numara=@ara_a_numara, ara_n_engage=@ara_n_engage WHERE ara_cn= @ara_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet arretes");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_arretes l_Objet = (clg_arretes)pObjet;
        string l_ordreSQL = "INSERT INTO arretes (ara_cn, ara_a_lib, ara_d_piece, ara_d_limite, ara_acp_cn, ara_n_montant, ara_n_ecno, ara_n_cpt, ara_fin_cn, ara_d_solde, ara_d_engagement, ara_n_archive, ara_n_etat, ara_a_numara, ara_n_engage) VALUES (@ara_cn, @ara_a_lib, @ara_d_piece, @ara_d_limite, @ara_acp_cn, @ara_n_montant, @ara_n_ecno, @ara_n_cpt, @ara_fin_cn, @ara_d_solde, @ara_d_engagement, @ara_n_archive, @ara_n_etat, @ara_a_numara, @ara_n_engage);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet arretes");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_arretes l_Objet = (clg_arretes)pObjet;
        string l_ordreSQL = "DELETE FROM arretes WHERE ara_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet arretes");
    }

    private void InjecterDonnees(clg_arretes pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ara_cn");
		pValParams.Add(pObjet.ara_cn);
		pParams.Add("@ara_a_lib");
		pValParams.Add(pObjet.ara_a_lib);
		pParams.Add("@ara_d_piece");
		pValParams.Add(pObjet.ara_d_piece);
		pParams.Add("@ara_d_limite");
		pValParams.Add(pObjet.ara_d_limite);
		pParams.Add("@ara_acp_cn");
		pValParams.Add(pObjet.annee_comptable != null ? pObjet.annee_comptable.acp_cn : (Int64?)null);
		pParams.Add("@ara_n_montant");
		pValParams.Add(pObjet.ara_n_montant);
		pParams.Add("@ara_n_ecno");
		pValParams.Add(pObjet.ara_n_ecno);
		pParams.Add("@ara_n_cpt");
		pValParams.Add(pObjet.ara_n_cpt);
		pParams.Add("@ara_fin_cn");
		pValParams.Add(pObjet.financeurs != null ? pObjet.financeurs.fin_cn : (Int64?)null);
		pParams.Add("@ara_d_solde");
		pValParams.Add(pObjet.ara_d_solde);
		pParams.Add("@ara_d_engagement");
		pValParams.Add(pObjet.ara_d_engagement);
		pParams.Add("@ara_n_archive");
		pValParams.Add(pObjet.ara_n_archive);
		pParams.Add("@ara_n_etat");
		pValParams.Add(pObjet.etat_arrete != null ? pObjet.etat_arrete.eta_cn : (Int64?)null);
		pParams.Add("@ara_a_numara");
		pValParams.Add(pObjet.ara_a_numara);
		pParams.Add("@ara_n_engage");
		pValParams.Add(pObjet.ara_n_engage);

    }


#endregion
}
}
