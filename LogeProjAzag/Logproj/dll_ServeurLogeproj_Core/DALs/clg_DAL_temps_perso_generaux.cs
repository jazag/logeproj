namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : temps_perso_generaux </summary>
public class clg_DAL_temps_perso_generaux : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_temps_perso_generaux(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT tpg_cn, tpg_per_cn, tpg_n_heures, tpg_d_date, tpg_rbr_cn, tpg_fct_cn FROM temps_perso_generaux";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_temps_perso_generaux l_Objet;
			l_Objet = new clg_temps_perso_generaux(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : double.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_temps_perso_generaux l_Objet;
        if (pModele.Listetemps_perso_generaux.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_temps_perso_generaux) pModele.Listetemps_perso_generaux.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_temps_perso_generaux(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : double.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_temps_perso_generaux l_Objet = (clg_temps_perso_generaux)pObjet;
        string l_ordreSQL = "UPDATE temps_perso_generaux SET tpg_cn=@tpg_cn, tpg_per_cn=@tpg_per_cn, tpg_n_heures=@tpg_n_heures, tpg_d_date=@tpg_d_date, tpg_rbr_cn=@tpg_rbr_cn, tpg_fct_cn=@tpg_fct_cn WHERE tpg_cn= @tpg_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet temps_perso_generaux");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_temps_perso_generaux l_Objet = (clg_temps_perso_generaux)pObjet;
        string l_ordreSQL = "INSERT INTO temps_perso_generaux (tpg_cn, tpg_per_cn, tpg_n_heures, tpg_d_date, tpg_rbr_cn, tpg_fct_cn) VALUES (@tpg_cn, @tpg_per_cn, @tpg_n_heures, @tpg_d_date, @tpg_rbr_cn, @tpg_fct_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet temps_perso_generaux");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_temps_perso_generaux l_Objet = (clg_temps_perso_generaux)pObjet;
        string l_ordreSQL = "DELETE FROM temps_perso_generaux WHERE tpg_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet temps_perso_generaux");
    }

    private void InjecterDonnees(clg_temps_perso_generaux pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@tpg_cn");
		pValParams.Add(pObjet.tpg_cn);
		pParams.Add("@tpg_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@tpg_n_heures");
		pValParams.Add(pObjet.tpg_n_heures);
		pParams.Add("@tpg_d_date");
		pValParams.Add(pObjet.tpg_d_date);
		pParams.Add("@tpg_rbr_cn");
		pValParams.Add(pObjet.rubriques_temps != null ? pObjet.rubriques_temps.rbr_cn : (Int64?)null);
		pParams.Add("@tpg_fct_cn");
		pValParams.Add(pObjet.tpg_fct_cn);

    }

#endregion
}
}
