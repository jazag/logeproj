namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : c_operation </summary>
public class clg_DAL_c_operation : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_c_operation(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT c_ope_cn, c_ope_a_lib, c_ope_m_desc, c_ope_cn_pere, c_ope_cn_racine, c_ope_a_code, c_ope_n_niveau, c_ope_a_cheminicone, c_ope_a_cheminicogrise FROM c_operation";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_c_operation l_Objet;
			l_Objet = new clg_c_operation(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_c_operation l_Objet;
        if (pModele.Listec_operation.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_c_operation) pModele.Listec_operation.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_c_operation(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6], l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8], l_Chaines[9]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_c_operation l_Objet = (clg_c_operation)pObjet;
        string l_ordreSQL = "UPDATE c_operation SET c_ope_cn=@c_ope_cn, c_ope_a_lib=@c_ope_a_lib, c_ope_m_desc=@c_ope_m_desc, c_ope_cn_pere=@c_ope_cn_pere, c_ope_cn_racine=@c_ope_cn_racine, c_ope_a_code=@c_ope_a_code, c_ope_n_niveau=@c_ope_n_niveau, c_ope_a_cheminicone=@c_ope_a_cheminicone, c_ope_a_cheminicogrise=@c_ope_a_cheminicogrise WHERE c_ope_cn= @c_ope_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet c_operation");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_c_operation l_Objet = (clg_c_operation)pObjet;
        string l_ordreSQL = "INSERT INTO c_operation (c_ope_cn, c_ope_a_lib, c_ope_m_desc, c_ope_cn_pere, c_ope_cn_racine, c_ope_a_code, c_ope_n_niveau, c_ope_a_cheminicone, c_ope_a_cheminicogrise) VALUES (@c_ope_cn, @c_ope_a_lib, @c_ope_m_desc, @c_ope_cn_pere, @c_ope_cn_racine, @c_ope_a_code, @c_ope_n_niveau, @c_ope_a_cheminicone, @c_ope_a_cheminicogrise);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet c_operation");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_c_operation l_Objet = (clg_c_operation)pObjet;
        string l_ordreSQL = "DELETE FROM c_operation WHERE c_ope_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet c_operation");
    }

    private void InjecterDonnees(clg_c_operation pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@c_ope_cn");
		pValParams.Add(pObjet.c_ope_cn);
		pParams.Add("@c_ope_a_lib");
		pValParams.Add(pObjet.c_ope_a_lib);
		pParams.Add("@c_ope_m_desc");
		pValParams.Add(pObjet.c_ope_m_desc);
		pParams.Add("@c_ope_cn_pere");
		pValParams.Add(pObjet.c_ope_cn_pere);
		pParams.Add("@c_ope_cn_racine");
		pValParams.Add(pObjet.c_ope_cn_racine);
		pParams.Add("@c_ope_a_code");
		pValParams.Add(pObjet.c_ope_a_code);
		pParams.Add("@c_ope_n_niveau");
		pValParams.Add(pObjet.c_ope_n_niveau);
		pParams.Add("@c_ope_a_cheminicone");
		pValParams.Add(pObjet.c_ope_a_cheminicone);
		pParams.Add("@c_ope_a_cheminicogrise");
		pValParams.Add(pObjet.c_ope_a_cheminicogrise);

    }

#endregion
}
}
