namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : elements_bis </summary>
public class clg_DAL_elements_bis : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_elements_bis(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ele_cn, ele_gpm_cn, ele_cn_pere, ele_n_niveau, ele_a_lib, ele_t_ele_cn, ele_a_code, ele_cn_racine, ele_n_archive, ele_d_debut_prev, ele_d_fin_reelle, ele_d_fin_prev, ele_d_debut_reelle, ele_d_archivage, ele_n_visibleetat FROM elements_bis";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_elements_bis l_Objet;
			l_Objet = new clg_elements_bis(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 11)), l_rds.Donnee(i, 12) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 13)), l_rds.Donnee(i, 14) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 14)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_elements_bis l_Objet;
        if (pModele.Listeelements_bis.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_elements_bis) pModele.Listeelements_bis.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_elements_bis(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5], l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]), l_Chaines[7], l_Chaines[8] == "" ? -1 : Int64.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[10]), l_Chaines[11] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[11]), l_Chaines[12] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[12]), l_Chaines[13] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[13]), l_Chaines[14] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[14]), l_Chaines[15] == "" ? -1 : Int64.Parse(l_Chaines[15]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_elements_bis l_Objet = (clg_elements_bis)pObjet;
        string l_ordreSQL = "UPDATE elements_bis SET ele_cn=@ele_cn, ele_gpm_cn=@ele_gpm_cn, ele_cn_pere=@ele_cn_pere, ele_n_niveau=@ele_n_niveau, ele_a_lib=@ele_a_lib, ele_t_ele_cn=@ele_t_ele_cn, ele_a_code=@ele_a_code, ele_cn_racine=@ele_cn_racine, ele_n_archive=@ele_n_archive, ele_d_debut_prev=@ele_d_debut_prev, ele_d_fin_reelle=@ele_d_fin_reelle, ele_d_fin_prev=@ele_d_fin_prev, ele_d_debut_reelle=@ele_d_debut_reelle, ele_d_archivage=@ele_d_archivage, ele_n_visibleetat=@ele_n_visibleetat WHERE ele_cn= @ele_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet elements_bis");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_elements_bis l_Objet = (clg_elements_bis)pObjet;
        string l_ordreSQL = "INSERT INTO elements_bis (ele_cn, ele_gpm_cn, ele_cn_pere, ele_n_niveau, ele_a_lib, ele_t_ele_cn, ele_a_code, ele_cn_racine, ele_n_archive, ele_d_debut_prev, ele_d_fin_reelle, ele_d_fin_prev, ele_d_debut_reelle, ele_d_archivage, ele_n_visibleetat) VALUES (@ele_cn, @ele_gpm_cn, @ele_cn_pere, @ele_n_niveau, @ele_a_lib, @ele_t_ele_cn, @ele_a_code, @ele_cn_racine, @ele_n_archive, @ele_d_debut_prev, @ele_d_fin_reelle, @ele_d_fin_prev, @ele_d_debut_reelle, @ele_d_archivage, @ele_n_visibleetat);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet elements_bis");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_elements_bis l_Objet = (clg_elements_bis)pObjet;
        string l_ordreSQL = "DELETE FROM elements_bis WHERE ele_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet elements_bis");
    }

    private void InjecterDonnees(clg_elements_bis pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ele_cn");
		pValParams.Add(pObjet.ele_cn);
		pParams.Add("@ele_gpm_cn");
		pValParams.Add(pObjet.ele_gpm_cn);
		pParams.Add("@ele_cn_pere");
		pValParams.Add(pObjet.ele_cn_pere);
		pParams.Add("@ele_n_niveau");
		pValParams.Add(pObjet.ele_n_niveau);
		pParams.Add("@ele_a_lib");
		pValParams.Add(pObjet.ele_a_lib);
		pParams.Add("@ele_t_ele_cn");
		pValParams.Add(pObjet.ele_t_ele_cn);
		pParams.Add("@ele_a_code");
		pValParams.Add(pObjet.ele_a_code);
		pParams.Add("@ele_cn_racine");
		pValParams.Add(pObjet.ele_cn_racine);
		pParams.Add("@ele_n_archive");
		pValParams.Add(pObjet.ele_n_archive);
		pParams.Add("@ele_d_debut_prev");
		pValParams.Add(pObjet.ele_d_debut_prev);
		pParams.Add("@ele_d_fin_reelle");
		pValParams.Add(pObjet.ele_d_fin_reelle);
		pParams.Add("@ele_d_fin_prev");
		pValParams.Add(pObjet.ele_d_fin_prev);
		pParams.Add("@ele_d_debut_reelle");
		pValParams.Add(pObjet.ele_d_debut_reelle);
		pParams.Add("@ele_d_archivage");
		pValParams.Add(pObjet.ele_d_archivage);
		pParams.Add("@ele_n_visibleetat");
		pValParams.Add(pObjet.ele_n_visibleetat);

    }

#endregion
}
}
