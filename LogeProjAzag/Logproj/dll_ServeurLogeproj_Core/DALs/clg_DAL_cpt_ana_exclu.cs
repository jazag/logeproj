namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : cpt_ana_exclu </summary>
public class clg_DAL_cpt_ana_exclu : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_cpt_ana_exclu(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cae_cn, cae_a_cpt FROM cpt_ana_exclu";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_cpt_ana_exclu l_Objet;
			l_Objet = new clg_cpt_ana_exclu(pModele, int.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(int.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_cpt_ana_exclu l_Objet;
        if (pModele.Listecpt_ana_exclu.Dictionnaire.ContainsKey(int.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_cpt_ana_exclu) pModele.Listecpt_ana_exclu.Dictionnaire[int.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_cpt_ana_exclu(pModele, int.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(int.Parse(l_Chaines[1]), l_Chaines[2]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_cpt_ana_exclu l_Objet = (clg_cpt_ana_exclu)pObjet;
        string l_ordreSQL = "UPDATE cpt_ana_exclu SET cae_cn=@cae_cn, cae_a_cpt=@cae_a_cpt WHERE cae_cn= @cae_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet cpt_ana_exclu");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_cpt_ana_exclu l_Objet = (clg_cpt_ana_exclu)pObjet;
        string l_ordreSQL = "INSERT INTO cpt_ana_exclu (cae_cn, cae_a_cpt) VALUES (@cae_cn, @cae_a_cpt);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet cpt_ana_exclu");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_cpt_ana_exclu l_Objet = (clg_cpt_ana_exclu)pObjet;
        string l_ordreSQL = "DELETE FROM cpt_ana_exclu WHERE cae_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet cpt_ana_exclu");
    }

    private void InjecterDonnees(clg_cpt_ana_exclu pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@cae_cn");
		pValParams.Add(pObjet.cae_cn);
		pParams.Add("@cae_a_cpt");
		pValParams.Add(pObjet.cae_a_cpt);

    }

#endregion
}
}
