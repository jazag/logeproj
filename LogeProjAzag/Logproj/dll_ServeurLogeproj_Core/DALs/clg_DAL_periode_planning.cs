namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : periode_planning </summary>
public class clg_DAL_periode_planning : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_periode_planning(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ppl_cn, ppl_a_lib, ppl_d_debut, ppl_d_fin FROM periode_planning";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_periode_planning l_Objet;
			l_Objet = new clg_periode_planning(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_periode_planning l_Objet;
        if (pModele.Listeperiode_planning.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_periode_planning) pModele.Listeperiode_planning.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_periode_planning(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_periode_planning l_Objet = (clg_periode_planning)pObjet;
        string l_ordreSQL = "UPDATE periode_planning SET ppl_cn=@ppl_cn, ppl_a_lib=@ppl_a_lib, ppl_d_debut=@ppl_d_debut, ppl_d_fin=@ppl_d_fin WHERE ppl_cn= @ppl_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet periode_planning");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_periode_planning l_Objet = (clg_periode_planning)pObjet;
        string l_ordreSQL = "INSERT INTO periode_planning (ppl_cn, ppl_a_lib, ppl_d_debut, ppl_d_fin) VALUES (@ppl_cn, @ppl_a_lib, @ppl_d_debut, @ppl_d_fin);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet periode_planning");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_periode_planning l_Objet = (clg_periode_planning)pObjet;
        string l_ordreSQL = "DELETE FROM periode_planning WHERE ppl_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet periode_planning");
    }

    private void InjecterDonnees(clg_periode_planning pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ppl_cn");
		pValParams.Add(pObjet.ppl_cn);
		pParams.Add("@ppl_a_lib");
		pValParams.Add(pObjet.ppl_a_lib);
		pParams.Add("@ppl_d_debut");
		pValParams.Add(pObjet.ppl_d_debut);
		pParams.Add("@ppl_d_fin");
		pValParams.Add(pObjet.ppl_d_fin);

    }

#endregion
}
}
