namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : calendrier </summary>
public class clg_DAL_calendrier : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_calendrier(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cal_d_date, cal_jour_cn, cal_mpa_cn, cal_sem_cn FROM calendrier";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_calendrier l_Objet;
			l_Objet = new clg_calendrier(pModele, DateTime.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(DateTime.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_calendrier l_Objet;
        if (pModele.Listecalendrier.Dictionnaire.ContainsKey(DateTime.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_calendrier) pModele.Listecalendrier.Dictionnaire[DateTime.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_calendrier(pModele, DateTime.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(DateTime.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_calendrier l_Objet = (clg_calendrier)pObjet;
        string l_ordreSQL = "UPDATE calendrier SET cal_d_date=@cal_d_date, cal_jour_cn=@cal_jour_cn, cal_mpa_cn=@cal_mpa_cn, cal_sem_cn=@cal_sem_cn WHERE cal_d_date= @cal_d_date;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet calendrier");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_calendrier l_Objet = (clg_calendrier)pObjet;
        string l_ordreSQL = "INSERT INTO calendrier (cal_d_date, cal_jour_cn, cal_mpa_cn, cal_sem_cn) VALUES (@cal_d_date, @cal_jour_cn, @cal_mpa_cn, @cal_sem_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet calendrier");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_calendrier l_Objet = (clg_calendrier)pObjet;
        string l_ordreSQL = "DELETE FROM calendrier WHERE cal_d_date=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet calendrier");
    }

    private void InjecterDonnees(clg_calendrier pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@cal_d_date");
		pValParams.Add(pObjet.cal_d_date);
		pParams.Add("@cal_jour_cn");
		pValParams.Add(pObjet.jours != null ? pObjet.jours.jour_cn : (Int64?)null);
		pParams.Add("@cal_mpa_cn");
		pValParams.Add(pObjet.mensuel != null ? pObjet.mensuel.mpa_cn : (Int64?)null);
		pParams.Add("@cal_sem_cn");
		pValParams.Add(pObjet.cal_sem_cn);

    }

#endregion
}
}
