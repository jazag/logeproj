namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : operations_reportees </summary>
public class clg_DAL_operations_reportees : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_operations_reportees(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT orp_ope_ele_cn, orp_ann_cn FROM operations_reportees";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_operations l_operations = (clg_operations) pModele.Listeoperations.Dictionnaire[Int64.Parse(l_rds.Donnee(i, 0))];
            clg_annee_civile l_annee_civile = (clg_annee_civile) pModele.Listeannee_civile.Dictionnaire[Int64.Parse(l_rds.Donnee(i, 1))];

            l_operations.Listeannee_civile.Add(l_annee_civile);
            l_annee_civile.Listeoperations.Add(l_operations);
        }
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {

    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {

    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {

    }


#endregion
}
}
