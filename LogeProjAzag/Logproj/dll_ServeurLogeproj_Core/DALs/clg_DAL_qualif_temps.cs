namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : qualif_temps </summary>
public class clg_DAL_qualif_temps : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_qualif_temps(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT qlt_c_qlt_cn, qlt_tpr_cn FROM qualif_temps";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_c_qualif_temps l_c_qualif_temps = (clg_c_qualif_temps) pModele.Listec_qualif_temps.Dictionnaire[Int64.Parse(l_rds.Donnee(i, 0))];
            clg_temps_realises l_temps_realises = (clg_temps_realises) pModele.Listetemps_realises.Dictionnaire[Int64.Parse(l_rds.Donnee(i, 1))];

            l_c_qualif_temps.Listetemps_realises.Add(l_temps_realises);
            l_temps_realises.Listec_qualif_temps.Add(l_c_qualif_temps);
        }
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {

    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {

    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {

    }


#endregion
}
}
