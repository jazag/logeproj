namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : type_fonct_compta </summary>
public class clg_DAL_type_fonct_compta : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_type_fonct_compta(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT tfc_cn, tfc_a_code, tfc_a_cmt, tfc_a_fonction FROM type_fonct_compta";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_type_fonct_compta l_Objet;
			l_Objet = new clg_type_fonct_compta(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_type_fonct_compta l_Objet;
        if (pModele.Listetype_fonct_compta.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_type_fonct_compta) pModele.Listetype_fonct_compta.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_type_fonct_compta(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_type_fonct_compta l_Objet = (clg_type_fonct_compta)pObjet;
        string l_ordreSQL = "UPDATE type_fonct_compta SET tfc_cn=@tfc_cn, tfc_a_code=@tfc_a_code, tfc_a_cmt=@tfc_a_cmt, tfc_a_fonction=@tfc_a_fonction WHERE tfc_cn= @tfc_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet type_fonct_compta");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_type_fonct_compta l_Objet = (clg_type_fonct_compta)pObjet;
        string l_ordreSQL = "INSERT INTO type_fonct_compta (tfc_cn, tfc_a_code, tfc_a_cmt, tfc_a_fonction) VALUES (@tfc_cn, @tfc_a_code, @tfc_a_cmt, @tfc_a_fonction);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet type_fonct_compta");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_type_fonct_compta l_Objet = (clg_type_fonct_compta)pObjet;
        string l_ordreSQL = "DELETE FROM type_fonct_compta WHERE tfc_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet type_fonct_compta");
    }

    private void InjecterDonnees(clg_type_fonct_compta pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@tfc_cn");
		pValParams.Add(pObjet.tfc_cn);
		pParams.Add("@tfc_a_code");
		pValParams.Add(pObjet.tfc_a_code);
		pParams.Add("@tfc_a_cmt");
		pValParams.Add(pObjet.tfc_a_cmt);
		pParams.Add("@tfc_a_fonction");
		pValParams.Add(pObjet.tfc_a_fonction);

    }

#endregion
}
}
