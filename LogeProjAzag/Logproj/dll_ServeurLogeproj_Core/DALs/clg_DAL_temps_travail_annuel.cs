namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : temps_travail_annuel </summary>
public class clg_DAL_temps_travail_annuel : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_temps_travail_annuel(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT tta_cn, tta_per_cn, tta_ann_cn, tta_a_valeur FROM temps_travail_annuel";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_temps_travail_annuel l_Objet;
			l_Objet = new clg_temps_travail_annuel(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_temps_travail_annuel l_Objet;
        if (pModele.Listetemps_travail_annuel.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_temps_travail_annuel) pModele.Listetemps_travail_annuel.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_temps_travail_annuel(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_temps_travail_annuel l_Objet = (clg_temps_travail_annuel)pObjet;
        string l_ordreSQL = "UPDATE temps_travail_annuel SET tta_cn=@tta_cn, tta_per_cn=@tta_per_cn, tta_ann_cn=@tta_ann_cn, tta_a_valeur=@tta_a_valeur WHERE tta_cn= @tta_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet temps_travail_annuel");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_temps_travail_annuel l_Objet = (clg_temps_travail_annuel)pObjet;
        string l_ordreSQL = "INSERT INTO temps_travail_annuel (tta_cn, tta_per_cn, tta_ann_cn, tta_a_valeur) VALUES (@tta_cn, @tta_per_cn, @tta_ann_cn, @tta_a_valeur);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet temps_travail_annuel");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_temps_travail_annuel l_Objet = (clg_temps_travail_annuel)pObjet;
        string l_ordreSQL = "DELETE FROM temps_travail_annuel WHERE tta_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet temps_travail_annuel");
    }

    private void InjecterDonnees(clg_temps_travail_annuel pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@tta_cn");
		pValParams.Add(pObjet.tta_cn);
		pParams.Add("@tta_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@tta_ann_cn");
		pValParams.Add(pObjet.annee_civile != null ? pObjet.annee_civile.ann_cn : (Int64?)null);
		pParams.Add("@tta_a_valeur");
		pValParams.Add(pObjet.tta_a_valeur);

    }

#endregion
}
}
