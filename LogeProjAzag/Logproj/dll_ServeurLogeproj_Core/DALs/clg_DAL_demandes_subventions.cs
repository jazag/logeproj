namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : demandes_subventions </summary>
public class clg_DAL_demandes_subventions : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_demandes_subventions(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT dms_cn, dms_acp_cn, dms_a_lib, dms_d_envoi, dms_n_montant, dms_fin_cn FROM demandes_subventions";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_demandes_subventions l_Objet;
			l_Objet = new clg_demandes_subventions(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : double.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_demandes_subventions l_Objet;
        if (pModele.Listedemandes_subventions.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_demandes_subventions) pModele.Listedemandes_subventions.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_demandes_subventions(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3], l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : double.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_demandes_subventions l_Objet = (clg_demandes_subventions)pObjet;
        string l_ordreSQL = "UPDATE demandes_subventions SET dms_cn=@dms_cn, dms_acp_cn=@dms_acp_cn, dms_a_lib=@dms_a_lib, dms_d_envoi=@dms_d_envoi, dms_n_montant=@dms_n_montant, dms_fin_cn=@dms_fin_cn WHERE dms_cn= @dms_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet demandes_subventions");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_demandes_subventions l_Objet = (clg_demandes_subventions)pObjet;
        string l_ordreSQL = "INSERT INTO demandes_subventions (dms_cn, dms_acp_cn, dms_a_lib, dms_d_envoi, dms_n_montant, dms_fin_cn) VALUES (@dms_cn, @dms_acp_cn, @dms_a_lib, @dms_d_envoi, @dms_n_montant, @dms_fin_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet demandes_subventions");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_demandes_subventions l_Objet = (clg_demandes_subventions)pObjet;
        string l_ordreSQL = "DELETE FROM demandes_subventions WHERE dms_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet demandes_subventions");
    }

    private void InjecterDonnees(clg_demandes_subventions pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@dms_cn");
		pValParams.Add(pObjet.dms_cn);
		pParams.Add("@dms_acp_cn");
		pValParams.Add(pObjet.annee_comptable != null ? pObjet.annee_comptable.acp_cn : (Int64?)null);
		pParams.Add("@dms_a_lib");
		pValParams.Add(pObjet.dms_a_lib);
		pParams.Add("@dms_d_envoi");
		pValParams.Add(pObjet.dms_d_envoi);
		pParams.Add("@dms_n_montant");
		pValParams.Add(pObjet.dms_n_montant);
		pParams.Add("@dms_fin_cn");
		pValParams.Add(pObjet.financeurs != null ? pObjet.financeurs.fin_cn : (Int64?)null);

    }

#endregion
}
}
