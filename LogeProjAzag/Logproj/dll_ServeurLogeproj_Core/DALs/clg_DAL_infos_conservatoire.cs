namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : infos_conservatoire </summary>
public class clg_DAL_infos_conservatoire : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_infos_conservatoire(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT csr_cn, csr_a_guid, csr_a_version_3_0, csr_a_nom, csr_a_libel, csr_n_lock, csr_a_msglock, csr_a_adresse_serv_sync, csr_n_port_serv_sync_env, csr_n_port_serv_sync_rec, csr_n_der_id_sync, csr_a_adresse_serv_maj, csr_n_port_serv_maj_env, csr_n_port_serv_maj_rec, csr_basemae_cn, csr_v_tx, csr_d_debut_tx FROM infos_conservatoire";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_infos_conservatoire l_Objet;
			l_Objet = new clg_infos_conservatoire(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11), l_rds.Donnee(i, 12) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 13)), l_rds.Donnee(i, 14) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 14)), l_rds.Donnee(i, 15) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 15)), l_rds.Donnee(i, 16) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 16)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_infos_conservatoire l_Objet;
        if (pModele.Listeinfos_conservatoire.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_infos_conservatoire) pModele.Listeinfos_conservatoire.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_infos_conservatoire(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4], l_Chaines[5], l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]), l_Chaines[7], l_Chaines[8], l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10] == "" ? -1 : Int64.Parse(l_Chaines[10]), l_Chaines[11] == "" ? -1 : Int64.Parse(l_Chaines[11]), l_Chaines[12], l_Chaines[13] == "" ? -1 : Int64.Parse(l_Chaines[13]), l_Chaines[14] == "" ? -1 : Int64.Parse(l_Chaines[14]), l_Chaines[15] == "" ? -1 : Int64.Parse(l_Chaines[15]), l_Chaines[16] == "" ? -1 : Int64.Parse(l_Chaines[16]), l_Chaines[17] == "" ? -1 : Int64.Parse(l_Chaines[17]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_infos_conservatoire l_Objet = (clg_infos_conservatoire)pObjet;
        string l_ordreSQL = "UPDATE infos_conservatoire SET csr_cn=@csr_cn, csr_a_guid=@csr_a_guid, csr_a_version_3_0=@csr_a_version_3_0, csr_a_nom=@csr_a_nom, csr_a_libel=@csr_a_libel, csr_n_lock=@csr_n_lock, csr_a_msglock=@csr_a_msglock, csr_a_adresse_serv_sync=@csr_a_adresse_serv_sync, csr_n_port_serv_sync_env=@csr_n_port_serv_sync_env, csr_n_port_serv_sync_rec=@csr_n_port_serv_sync_rec, csr_n_der_id_sync=@csr_n_der_id_sync, csr_a_adresse_serv_maj=@csr_a_adresse_serv_maj, csr_n_port_serv_maj_env=@csr_n_port_serv_maj_env, csr_n_port_serv_maj_rec=@csr_n_port_serv_maj_rec, csr_basemae_cn=@csr_basemae_cn, csr_v_tx=@csr_v_tx, csr_d_debut_tx=@csr_d_debut_tx WHERE csr_cn= @csr_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet infos_conservatoire");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_infos_conservatoire l_Objet = (clg_infos_conservatoire)pObjet;
        string l_ordreSQL = "INSERT INTO infos_conservatoire (csr_cn, csr_a_guid, csr_a_version_3_0, csr_a_nom, csr_a_libel, csr_n_lock, csr_a_msglock, csr_a_adresse_serv_sync, csr_n_port_serv_sync_env, csr_n_port_serv_sync_rec, csr_n_der_id_sync, csr_a_adresse_serv_maj, csr_n_port_serv_maj_env, csr_n_port_serv_maj_rec, csr_basemae_cn, csr_v_tx, csr_d_debut_tx) VALUES (@csr_cn, @csr_a_guid, @csr_a_version_3_0, @csr_a_nom, @csr_a_libel, @csr_n_lock, @csr_a_msglock, @csr_a_adresse_serv_sync, @csr_n_port_serv_sync_env, @csr_n_port_serv_sync_rec, @csr_n_der_id_sync, @csr_a_adresse_serv_maj, @csr_n_port_serv_maj_env, @csr_n_port_serv_maj_rec, @csr_basemae_cn, @csr_v_tx, @csr_d_debut_tx);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet infos_conservatoire");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_infos_conservatoire l_Objet = (clg_infos_conservatoire)pObjet;
        string l_ordreSQL = "DELETE FROM infos_conservatoire WHERE csr_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet infos_conservatoire");
    }

    private void InjecterDonnees(clg_infos_conservatoire pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@csr_cn");
		pValParams.Add(pObjet.csr_cn);
		pParams.Add("@csr_a_guid");
		pValParams.Add(pObjet.csr_a_guid);
		pParams.Add("@csr_a_version_3_0");
		pValParams.Add(pObjet.csr_a_version_3_0);
		pParams.Add("@csr_a_nom");
		pValParams.Add(pObjet.csr_a_nom);
		pParams.Add("@csr_a_libel");
		pValParams.Add(pObjet.csr_a_libel);
		pParams.Add("@csr_n_lock");
		pValParams.Add(pObjet.csr_n_lock);
		pParams.Add("@csr_a_msglock");
		pValParams.Add(pObjet.csr_a_msglock);
		pParams.Add("@csr_a_adresse_serv_sync");
		pValParams.Add(pObjet.csr_a_adresse_serv_sync);
		pParams.Add("@csr_n_port_serv_sync_env");
		pValParams.Add(pObjet.csr_n_port_serv_sync_env);
		pParams.Add("@csr_n_port_serv_sync_rec");
		pValParams.Add(pObjet.csr_n_port_serv_sync_rec);
		pParams.Add("@csr_n_der_id_sync");
		pValParams.Add(pObjet.csr_n_der_id_sync);
		pParams.Add("@csr_a_adresse_serv_maj");
		pValParams.Add(pObjet.csr_a_adresse_serv_maj);
		pParams.Add("@csr_n_port_serv_maj_env");
		pValParams.Add(pObjet.csr_n_port_serv_maj_env);
		pParams.Add("@csr_n_port_serv_maj_rec");
		pValParams.Add(pObjet.csr_n_port_serv_maj_rec);
		pParams.Add("@csr_basemae_cn");
		pValParams.Add(pObjet.csr_basemae_cn);
		pParams.Add("@csr_v_tx");
		pValParams.Add(pObjet.csr_v_tx);
		pParams.Add("@csr_d_debut_tx");
		pValParams.Add(pObjet.csr_d_debut_tx);

    }

#endregion
}
}
