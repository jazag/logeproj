namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : taux </summary>
public class clg_DAL_taux : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_taux(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT tx_cn, tx_n_taux_prev, tx_n_taux_real, tx_n_cout_revient, tx_a_lib, tx_ele_cn, tx_t_fct_cn, tx_acp_cn, tx_d_debut, tx_d_fin, tx_per_cn, tx_com, tx_fin_cn, tx_t_cout_projet_cn, tx_b_archive FROM taux";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_taux l_Objet;
			l_Objet = new clg_taux(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : double.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : double.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11), l_rds.Donnee(i, 12) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 13)), l_rds.Donnee(i, 14)=="True");

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_taux l_Objet;
        if (pModele.Listetaux.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_taux) pModele.Listetaux.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_taux(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : double.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : double.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5], l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : Int64.Parse(l_Chaines[8]), l_Chaines[9] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[9]), l_Chaines[10] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[10]), l_Chaines[11] == "" ? -1 : Int64.Parse(l_Chaines[11]), l_Chaines[12], l_Chaines[13] == "" ? -1 : Int64.Parse(l_Chaines[13]), l_Chaines[14] == "" ? -1 : Int64.Parse(l_Chaines[14]), l_Chaines[15]=="True");

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_taux l_Objet = (clg_taux)pObjet;
        string l_ordreSQL = "UPDATE taux SET tx_cn=@tx_cn, tx_n_taux_prev=@tx_n_taux_prev, tx_n_taux_real=@tx_n_taux_real, tx_n_cout_revient=@tx_n_cout_revient, tx_a_lib=@tx_a_lib, tx_ele_cn=@tx_ele_cn, tx_t_fct_cn=@tx_t_fct_cn, tx_acp_cn=@tx_acp_cn, tx_d_debut=@tx_d_debut, tx_d_fin=@tx_d_fin, tx_per_cn=@tx_per_cn, tx_com=@tx_com, tx_fin_cn=@tx_fin_cn, tx_t_cout_projet_cn=@tx_t_cout_projet_cn, tx_b_archive=@tx_b_archive WHERE tx_cn= @tx_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet taux");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_taux l_Objet = (clg_taux)pObjet;
        string l_ordreSQL = "INSERT INTO taux (tx_cn, tx_n_taux_prev, tx_n_taux_real, tx_n_cout_revient, tx_a_lib, tx_ele_cn, tx_t_fct_cn, tx_acp_cn, tx_d_debut, tx_d_fin, tx_per_cn, tx_com, tx_fin_cn, tx_t_cout_projet_cn, tx_b_archive) VALUES (@tx_cn, @tx_n_taux_prev, @tx_n_taux_real, @tx_n_cout_revient, @tx_a_lib, @tx_ele_cn, @tx_t_fct_cn, @tx_acp_cn, @tx_d_debut, @tx_d_fin, @tx_per_cn, @tx_com, @tx_fin_cn, @tx_t_cout_projet_cn, @tx_b_archive);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet taux");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_taux l_Objet = (clg_taux)pObjet;
        string l_ordreSQL = "DELETE FROM taux WHERE tx_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet taux");
    }

    private void InjecterDonnees(clg_taux pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@tx_cn");
		pValParams.Add(pObjet.tx_cn);
		pParams.Add("@tx_n_taux_prev");
		pValParams.Add(pObjet.tx_n_taux_prev);
		pParams.Add("@tx_n_taux_real");
		pValParams.Add(pObjet.tx_n_taux_real);
		pParams.Add("@tx_n_cout_revient");
		pValParams.Add(pObjet.tx_n_cout_revient);
		pParams.Add("@tx_a_lib");
		pValParams.Add(pObjet.tx_a_lib);
		pParams.Add("@tx_ele_cn");
		pValParams.Add(pObjet.elements != null ? pObjet.elements.ele_cn : (Int64?)null);
		pParams.Add("@tx_t_fct_cn");
		pValParams.Add(pObjet.t_fonctions != null ? pObjet.t_fonctions.t_fct_cn : (Int64?)null);
		pParams.Add("@tx_acp_cn");
		pValParams.Add(pObjet.annee_comptable != null ? pObjet.annee_comptable.acp_cn : (Int64?)null);
		pParams.Add("@tx_d_debut");
		pValParams.Add(pObjet.tx_d_debut);
		pParams.Add("@tx_d_fin");
		pValParams.Add(pObjet.tx_d_fin);
		pParams.Add("@tx_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@tx_com");
		pValParams.Add(pObjet.tx_com);
		pParams.Add("@tx_fin_cn");
		pValParams.Add(pObjet.tx_fin_cn);
		pParams.Add("@tx_t_cout_projet_cn");
		pValParams.Add(pObjet.t_cout_projet != null ? pObjet.t_cout_projet.t_cout_projet_cn : (Int64?)null);
		pParams.Add("@tx_b_archive");
		pValParams.Add(pObjet.tx_b_archive);

    }

#endregion
}
}
