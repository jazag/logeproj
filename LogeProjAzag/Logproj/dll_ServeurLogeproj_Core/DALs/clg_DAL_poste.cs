namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : poste </summary>
public class clg_DAL_poste : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_poste(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT pst_cn, pst_a_guid, pst_ifb_cn, pst_a_nom, pst_a_ip, pst_d_creation, pst_d_der_connexion, pst_d_der_sdv, pst_n_connecte FROM poste";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_poste l_Objet;
			l_Objet = new clg_poste(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_poste l_Objet;
        if (pModele.Listeposte.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_poste) pModele.Listeposte.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_poste(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4], l_Chaines[5], l_Chaines[6] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[6]), l_Chaines[7] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[7]), l_Chaines[8] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_poste l_Objet = (clg_poste)pObjet;
        string l_ordreSQL = "UPDATE poste SET pst_cn=@pst_cn, pst_a_guid=@pst_a_guid, pst_ifb_cn=@pst_ifb_cn, pst_a_nom=@pst_a_nom, pst_a_ip=@pst_a_ip, pst_d_creation=@pst_d_creation, pst_d_der_connexion=@pst_d_der_connexion, pst_d_der_sdv=@pst_d_der_sdv, pst_n_connecte=@pst_n_connecte WHERE pst_cn= @pst_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet poste");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_poste l_Objet = (clg_poste)pObjet;
        string l_ordreSQL = "INSERT INTO poste (pst_cn, pst_a_guid, pst_ifb_cn, pst_a_nom, pst_a_ip, pst_d_creation, pst_d_der_connexion, pst_d_der_sdv, pst_n_connecte) VALUES (@pst_cn, @pst_a_guid, @pst_ifb_cn, @pst_a_nom, @pst_a_ip, @pst_d_creation, @pst_d_der_connexion, @pst_d_der_sdv, @pst_n_connecte);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet poste");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_poste l_Objet = (clg_poste)pObjet;
        string l_ordreSQL = "DELETE FROM poste WHERE pst_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet poste");
    }

    private void InjecterDonnees(clg_poste pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@pst_cn");
		pValParams.Add(pObjet.pst_cn);
		pParams.Add("@pst_a_guid");
		pValParams.Add(pObjet.pst_a_guid);
		pParams.Add("@pst_ifb_cn");
		pValParams.Add(pObjet.infos_base != null ? pObjet.infos_base.ifb_cn : (Int64?)null);
		pParams.Add("@pst_a_nom");
		pValParams.Add(pObjet.pst_a_nom);
		pParams.Add("@pst_a_ip");
		pValParams.Add(pObjet.pst_a_ip);
		pParams.Add("@pst_d_creation");
		pValParams.Add(pObjet.pst_d_creation);
		pParams.Add("@pst_d_der_connexion");
		pValParams.Add(pObjet.pst_d_der_connexion);
		pParams.Add("@pst_d_der_sdv");
		pValParams.Add(pObjet.pst_d_der_sdv);
		pParams.Add("@pst_n_connecte");
		pValParams.Add(pObjet.pst_n_connecte);

    }

#endregion
}
}
