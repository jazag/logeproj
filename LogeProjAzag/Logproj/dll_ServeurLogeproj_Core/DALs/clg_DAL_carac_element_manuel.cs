﻿using clg_ReflexionV3_Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace dll_ServeurLogeproj_Core
{
    public partial class clg_DAL_carac_element
    {
        public static clg_ObjetBase ChargerDepuisParametre(long pcel_cn, Int64 pelement, Int64 pcarac, string pcel_a_val, string pcel_m_val, DateTime pcel_date_modif, clg_Modele pModele)
        {
            clg_carac_element l_Objet;
            object l_Cel_cn = pcel_cn;
            clg_elements pClgElement = new clg_elements(pModele, pelement);
            clg_carac pClgCarac = new clg_carac(pModele, pcarac);
            if (pModele.Listecarac_element.Dictionnaire.ContainsKey(l_Cel_cn))
            { 
                l_Objet = (clg_carac_element)pModele.Listecarac_element.Dictionnaire[pcel_cn];
                l_Objet.DetruitReferences();
            }
            else
            {
                l_Objet = new clg_carac_element(pModele, pcel_cn, true);
            }
            l_Objet.DicReferences.Clear();
            l_Objet.Initialise(pcel_cn, pelement, pcarac, pcel_a_val, pcel_m_val, pcel_date_modif);
            l_Objet.CreeLiens();
            return l_Objet;
        }

        public static clg_carac_element ChargerDepuisID(Int64 pID)
        {
            if (clg_ServeurLogeproj.Modele.Listecarac_element.Dictionnaire.ContainsKey(pID))
            {
                return (clg_carac_element)clg_ServeurLogeproj.Modele.Listecarac_element.Dictionnaire[pID];
            }
            return null;
        }
    }
}
