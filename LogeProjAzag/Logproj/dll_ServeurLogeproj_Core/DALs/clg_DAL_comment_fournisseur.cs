namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : comment_fournisseur </summary>
public class clg_DAL_comment_fournisseur : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_comment_fournisseur(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cmf_com_cn, cmf_four_cn FROM comment_fournisseur";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_comment l_comment = (clg_comment) pModele.Listecomment.Dictionnaire[Int64.Parse(l_rds.Donnee(i, 0))];
            clg_fournisseurs l_fournisseurs = (clg_fournisseurs) pModele.Listefournisseurs.Dictionnaire[Int64.Parse(l_rds.Donnee(i, 1))];

            l_comment.Listefournisseurs.Add(l_fournisseurs);
            l_fournisseurs.Listecomment.Add(l_comment);
        }
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {

    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {

    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {

    }


#endregion
}
}
