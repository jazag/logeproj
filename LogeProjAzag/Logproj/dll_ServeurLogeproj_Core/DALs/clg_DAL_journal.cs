namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : journal </summary>
public class clg_DAL_journal : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_journal(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT jou_a_code, jou_a_lib, jou_a_cmt, jou_a_chemin_icone, jou_a_chemin_icone_ventil FROM journal";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_journal l_Objet;
			l_Objet = new clg_journal(pModele, l_rds.Donnee(i, 0).ToString());
			l_Objet.Initialise(l_rds.Donnee(i, 0).ToString(), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_journal l_Objet;
        if (pModele.Listejournal.Dictionnaire.ContainsKey(l_Chaines[0]))
        {
            l_Objet = (clg_journal) pModele.Listejournal.Dictionnaire[l_Chaines[0]];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_journal(pModele, l_Chaines[0]);
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(l_Chaines[1].ToString(), l_Chaines[2], l_Chaines[3], l_Chaines[4], l_Chaines[5]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_journal l_Objet = (clg_journal)pObjet;
        string l_ordreSQL = "UPDATE journal SET jou_a_code=@jou_a_code, jou_a_lib=@jou_a_lib, jou_a_cmt=@jou_a_cmt, jou_a_chemin_icone=@jou_a_chemin_icone, jou_a_chemin_icone_ventil=@jou_a_chemin_icone_ventil WHERE jou_a_code= @jou_a_code;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet journal");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_journal l_Objet = (clg_journal)pObjet;
        string l_ordreSQL = "INSERT INTO journal (jou_a_code, jou_a_lib, jou_a_cmt, jou_a_chemin_icone, jou_a_chemin_icone_ventil) VALUES (@jou_a_code, @jou_a_lib, @jou_a_cmt, @jou_a_chemin_icone, @jou_a_chemin_icone_ventil);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet journal");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_journal l_Objet = (clg_journal)pObjet;
        string l_ordreSQL = "DELETE FROM journal WHERE jou_a_code=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet journal");
    }

    private void InjecterDonnees(clg_journal pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@jou_a_code");
		pValParams.Add(pObjet.jou_a_code);
		pParams.Add("@jou_a_lib");
		pValParams.Add(pObjet.jou_a_lib);
		pParams.Add("@jou_a_cmt");
		pValParams.Add(pObjet.jou_a_cmt);
		pParams.Add("@jou_a_chemin_icone");
		pValParams.Add(pObjet.jou_a_chemin_icone);
		pParams.Add("@jou_a_chemin_icone_ventil");
		pValParams.Add(pObjet.jou_a_chemin_icone_ventil);

    }

#endregion
}
}
