namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : financement_prevus </summary>
public class clg_DAL_financement_prevus : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_financement_prevus(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT fnp_cn, fnp_fin_cn, fnp_ope_ele_cn, fnp_n_montant, fmp_n_pourcent, fmp_n_bloc_montant FROM financement_prevus";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_financement_prevus l_Objet;
			l_Objet = new clg_financement_prevus(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : double.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : double.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_financement_prevus l_Objet;
        if (pModele.Listefinancement_prevus.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_financement_prevus) pModele.Listefinancement_prevus.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_financement_prevus(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : double.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : double.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_financement_prevus l_Objet = (clg_financement_prevus)pObjet;
        string l_ordreSQL = "UPDATE financement_prevus SET fnp_cn=@fnp_cn, fnp_fin_cn=@fnp_fin_cn, fnp_ope_ele_cn=@fnp_ope_ele_cn, fnp_n_montant=@fnp_n_montant, fmp_n_pourcent=@fmp_n_pourcent, fmp_n_bloc_montant=@fmp_n_bloc_montant WHERE fnp_cn= @fnp_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet financement_prevus");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_financement_prevus l_Objet = (clg_financement_prevus)pObjet;
        string l_ordreSQL = "INSERT INTO financement_prevus (fnp_cn, fnp_fin_cn, fnp_ope_ele_cn, fnp_n_montant, fmp_n_pourcent, fmp_n_bloc_montant) VALUES (@fnp_cn, @fnp_fin_cn, @fnp_ope_ele_cn, @fnp_n_montant, @fmp_n_pourcent, @fmp_n_bloc_montant);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet financement_prevus");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_financement_prevus l_Objet = (clg_financement_prevus)pObjet;
        string l_ordreSQL = "DELETE FROM financement_prevus WHERE fnp_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet financement_prevus");
    }

    private void InjecterDonnees(clg_financement_prevus pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@fnp_cn");
		pValParams.Add(pObjet.fnp_cn);
		pParams.Add("@fnp_fin_cn");
		pValParams.Add(pObjet.financeurs != null ? pObjet.financeurs.fin_cn : (Int64?)null);
		pParams.Add("@fnp_ope_ele_cn");
		pValParams.Add(pObjet.operations != null ? pObjet.operations.ele_cn : (Int64?)null);
		pParams.Add("@fnp_n_montant");
		pValParams.Add(pObjet.fnp_n_montant);
		pParams.Add("@fmp_n_pourcent");
		pValParams.Add(pObjet.fmp_n_pourcent);
		pParams.Add("@fmp_n_bloc_montant");
		pValParams.Add(pObjet.fmp_n_bloc_montant);

    }

#endregion
}
}
