namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : forfait_personnel </summary>
public class clg_DAL_forfait_personnel : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_forfait_personnel(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT frp_cn, frp_per_cn, frp_frf_cn, frp_d_debut, frp_n_pourcentage FROM forfait_personnel";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_forfait_personnel l_Objet;
			l_Objet = new clg_forfait_personnel(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_forfait_personnel l_Objet;
        if (pModele.Listeforfait_personnel.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_forfait_personnel) pModele.Listeforfait_personnel.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_forfait_personnel(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_forfait_personnel l_Objet = (clg_forfait_personnel)pObjet;
        string l_ordreSQL = "UPDATE forfait_personnel SET frp_cn=@frp_cn, frp_per_cn=@frp_per_cn, frp_frf_cn=@frp_frf_cn, frp_d_debut=@frp_d_debut, frp_n_pourcentage=@frp_n_pourcentage WHERE frp_cn= @frp_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet forfait_personnel");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_forfait_personnel l_Objet = (clg_forfait_personnel)pObjet;
        string l_ordreSQL = "INSERT INTO forfait_personnel (frp_cn, frp_per_cn, frp_frf_cn, frp_d_debut, frp_n_pourcentage) VALUES (@frp_cn, @frp_per_cn, @frp_frf_cn, @frp_d_debut, @frp_n_pourcentage);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet forfait_personnel");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_forfait_personnel l_Objet = (clg_forfait_personnel)pObjet;
        string l_ordreSQL = "DELETE FROM forfait_personnel WHERE frp_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet forfait_personnel");
    }

    private void InjecterDonnees(clg_forfait_personnel pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@frp_cn");
		pValParams.Add(pObjet.frp_cn);
		pParams.Add("@frp_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@frp_frf_cn");
		pValParams.Add(pObjet.forfait != null ? pObjet.forfait.frf_cn : (Int64?)null);
		pParams.Add("@frp_d_debut");
		pValParams.Add(pObjet.frp_d_debut);
		pParams.Add("@frp_n_pourcentage");
		pValParams.Add(pObjet.frp_n_pourcentage);

    }

#endregion
}
}
