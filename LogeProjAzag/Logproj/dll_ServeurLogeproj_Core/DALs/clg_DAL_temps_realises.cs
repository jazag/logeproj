namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : temps_realises </summary>
public class clg_DAL_temps_realises : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_temps_realises(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT tpr_cn, tpr_n_heures, tpr_d_date, cle_ele_t_fct, tpr_tit_cn FROM temps_realises";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_temps_realises l_Objet;
			l_Objet = new clg_temps_realises(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : double.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_temps_realises l_Objet;
        if (pModele.Listetemps_realises.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_temps_realises) pModele.Listetemps_realises.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_temps_realises(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : double.Parse(l_Chaines[2]), l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4], l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_temps_realises l_Objet = (clg_temps_realises)pObjet;
        string l_ordreSQL = "UPDATE temps_realises SET tpr_cn=@tpr_cn, tpr_n_heures=@tpr_n_heures, tpr_d_date=@tpr_d_date, cle_ele_t_fct=@cle_ele_t_fct, tpr_tit_cn=@tpr_tit_cn WHERE tpr_cn= @tpr_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet temps_realises");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_temps_realises l_Objet = (clg_temps_realises)pObjet;
        string l_ordreSQL = "INSERT INTO temps_realises (tpr_cn, tpr_n_heures, tpr_d_date, cle_ele_t_fct, tpr_tit_cn) VALUES (@tpr_cn, @tpr_n_heures, @tpr_d_date, @cle_ele_t_fct, @tpr_tit_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet temps_realises");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_temps_realises l_Objet = (clg_temps_realises)pObjet;
        string l_ordreSQL = "DELETE FROM temps_realises WHERE tpr_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet temps_realises");
    }

    private void InjecterDonnees(clg_temps_realises pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@tpr_cn");
		pValParams.Add(pObjet.tpr_cn);
		pParams.Add("@tpr_n_heures");
		pValParams.Add(pObjet.tpr_n_heures);
		pParams.Add("@tpr_d_date");
		pValParams.Add(pObjet.tpr_d_date);
		pParams.Add("@cle_ele_t_fct");
		pValParams.Add(pObjet.cle_ele_t_fct);
		pParams.Add("@tpr_tit_cn");
		pValParams.Add(pObjet.temps_intervenants != null ? pObjet.temps_intervenants.tit_cn : (Int64?)null);

    }


#endregion
}
}
