namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : import_sage </summary>
public class clg_DAL_import_sage : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_import_sage(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ims_cn, ims_base_cn, ims_n_numlig, ims_n_operation, ims_n_ordre, ims_jou_a_code, ims_d_piece, ims_a_cptgen, ims_a_libcpt, ims_n_piece, ims_n_fact, ims_a_ref, ims_a_refrapp, ims_n_cpttiers, ims_a_libec, ims_a_sens, ims_n_mtt, ims_t_ec, ims_n_planana, ims_n_section FROM import_sage";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_import_sage l_Objet;
			l_Objet = new clg_import_sage(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8), l_rds.Donnee(i, 9), l_rds.Donnee(i, 10), l_rds.Donnee(i, 11), l_rds.Donnee(i, 12), l_rds.Donnee(i, 13), l_rds.Donnee(i, 14), l_rds.Donnee(i, 15), l_rds.Donnee(i, 16) == "" ? -1 : double.Parse(l_rds.Donnee(i, 16)), l_rds.Donnee(i, 17), l_rds.Donnee(i, 18) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 18)), l_rds.Donnee(i, 19));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_import_sage l_Objet;
        if (pModele.Listeimport_sage.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_import_sage) pModele.Listeimport_sage.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_import_sage(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6], l_Chaines[7] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[7]), l_Chaines[8], l_Chaines[9], l_Chaines[10], l_Chaines[11], l_Chaines[12], l_Chaines[13], l_Chaines[14], l_Chaines[15], l_Chaines[16], l_Chaines[17] == "" ? -1 : double.Parse(l_Chaines[17]), l_Chaines[18], l_Chaines[19] == "" ? -1 : Int64.Parse(l_Chaines[19]), l_Chaines[20]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_import_sage l_Objet = (clg_import_sage)pObjet;
        string l_ordreSQL = "UPDATE import_sage SET ims_cn=@ims_cn, ims_base_cn=@ims_base_cn, ims_n_numlig=@ims_n_numlig, ims_n_operation=@ims_n_operation, ims_n_ordre=@ims_n_ordre, ims_jou_a_code=@ims_jou_a_code, ims_d_piece=@ims_d_piece, ims_a_cptgen=@ims_a_cptgen, ims_a_libcpt=@ims_a_libcpt, ims_n_piece=@ims_n_piece, ims_n_fact=@ims_n_fact, ims_a_ref=@ims_a_ref, ims_a_refrapp=@ims_a_refrapp, ims_n_cpttiers=@ims_n_cpttiers, ims_a_libec=@ims_a_libec, ims_a_sens=@ims_a_sens, ims_n_mtt=@ims_n_mtt, ims_t_ec=@ims_t_ec, ims_n_planana=@ims_n_planana, ims_n_section=@ims_n_section WHERE ims_cn= @ims_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet import_sage");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_import_sage l_Objet = (clg_import_sage)pObjet;
        string l_ordreSQL = "INSERT INTO import_sage (ims_cn, ims_base_cn, ims_n_numlig, ims_n_operation, ims_n_ordre, ims_jou_a_code, ims_d_piece, ims_a_cptgen, ims_a_libcpt, ims_n_piece, ims_n_fact, ims_a_ref, ims_a_refrapp, ims_n_cpttiers, ims_a_libec, ims_a_sens, ims_n_mtt, ims_t_ec, ims_n_planana, ims_n_section) VALUES (@ims_cn, @ims_base_cn, @ims_n_numlig, @ims_n_operation, @ims_n_ordre, @ims_jou_a_code, @ims_d_piece, @ims_a_cptgen, @ims_a_libcpt, @ims_n_piece, @ims_n_fact, @ims_a_ref, @ims_a_refrapp, @ims_n_cpttiers, @ims_a_libec, @ims_a_sens, @ims_n_mtt, @ims_t_ec, @ims_n_planana, @ims_n_section);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet import_sage");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_import_sage l_Objet = (clg_import_sage)pObjet;
        string l_ordreSQL = "DELETE FROM import_sage WHERE ims_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet import_sage");
    }

    private void InjecterDonnees(clg_import_sage pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ims_cn");
		pValParams.Add(pObjet.ims_cn);
		pParams.Add("@ims_base_cn");
		pValParams.Add(pObjet.ims_base_cn);
		pParams.Add("@ims_n_numlig");
		pValParams.Add(pObjet.ims_n_numlig);
		pParams.Add("@ims_n_operation");
		pValParams.Add(pObjet.ims_n_operation);
		pParams.Add("@ims_n_ordre");
		pValParams.Add(pObjet.ims_n_ordre);
		pParams.Add("@ims_jou_a_code");
		pValParams.Add(pObjet.ims_jou_a_code);
		pParams.Add("@ims_d_piece");
		pValParams.Add(pObjet.ims_d_piece);
		pParams.Add("@ims_a_cptgen");
		pValParams.Add(pObjet.ims_a_cptgen);
		pParams.Add("@ims_a_libcpt");
		pValParams.Add(pObjet.ims_a_libcpt);
		pParams.Add("@ims_n_piece");
		pValParams.Add(pObjet.ims_n_piece);
		pParams.Add("@ims_n_fact");
		pValParams.Add(pObjet.ims_n_fact);
		pParams.Add("@ims_a_ref");
		pValParams.Add(pObjet.ims_a_ref);
		pParams.Add("@ims_a_refrapp");
		pValParams.Add(pObjet.ims_a_refrapp);
		pParams.Add("@ims_n_cpttiers");
		pValParams.Add(pObjet.ims_n_cpttiers);
		pParams.Add("@ims_a_libec");
		pValParams.Add(pObjet.ims_a_libec);
		pParams.Add("@ims_a_sens");
		pValParams.Add(pObjet.ims_a_sens);
		pParams.Add("@ims_n_mtt");
		pValParams.Add(pObjet.ims_n_mtt);
		pParams.Add("@ims_t_ec");
		pValParams.Add(pObjet.ims_t_ec);
		pParams.Add("@ims_n_planana");
		pValParams.Add(pObjet.ims_n_planana);
		pParams.Add("@ims_n_section");
		pValParams.Add(pObjet.ims_n_section);

    }

#endregion
}
}
