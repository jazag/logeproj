namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : utilisateur </summary>
public class clg_DAL_utilisateur : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_utilisateur(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT uti_cn, uti_a_nom_login, uti_per_cn, uti_a_mp FROM utilisateur";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_utilisateur l_Objet;
			l_Objet = new clg_utilisateur(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_utilisateur l_Objet;
        if (pModele.Listeutilisateur.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_utilisateur) pModele.Listeutilisateur.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_utilisateur(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_utilisateur l_Objet = (clg_utilisateur)pObjet;
        string l_ordreSQL = "UPDATE utilisateur SET uti_cn=@uti_cn, uti_a_nom_login=@uti_a_nom_login, uti_per_cn=@uti_per_cn, uti_a_mp=@uti_a_mp WHERE uti_cn= @uti_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet utilisateur");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_utilisateur l_Objet = (clg_utilisateur)pObjet;
        string l_ordreSQL = "INSERT INTO utilisateur (uti_cn, uti_a_nom_login, uti_per_cn, uti_a_mp) VALUES (@uti_cn, @uti_a_nom_login, @uti_per_cn, @uti_a_mp);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet utilisateur");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_utilisateur l_Objet = (clg_utilisateur)pObjet;
        string l_ordreSQL = "DELETE FROM utilisateur WHERE uti_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet utilisateur");
    }

    private void InjecterDonnees(clg_utilisateur pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@uti_cn");
		pValParams.Add(pObjet.uti_cn);
		pParams.Add("@uti_a_nom_login");
		pValParams.Add(pObjet.uti_a_nom_login);
		pParams.Add("@uti_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@uti_a_mp");
		pValParams.Add(pObjet.uti_a_mp);

    }


#endregion
}
}
