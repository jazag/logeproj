namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : limitation_interface </summary>
public class clg_DAL_limitation_interface : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_limitation_interface(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT lmi_cn, lmi_grp_cn, lmi_obi_cn, lmi_t_lmi_cn FROM limitation_interface";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_limitation_interface l_Objet;
			l_Objet = new clg_limitation_interface(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_limitation_interface l_Objet;
        if (pModele.Listelimitation_interface.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_limitation_interface) pModele.Listelimitation_interface.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_limitation_interface(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_limitation_interface l_Objet = (clg_limitation_interface)pObjet;
        string l_ordreSQL = "UPDATE limitation_interface SET lmi_cn=@lmi_cn, lmi_grp_cn=@lmi_grp_cn, lmi_obi_cn=@lmi_obi_cn, lmi_t_lmi_cn=@lmi_t_lmi_cn WHERE lmi_cn= @lmi_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet limitation_interface");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_limitation_interface l_Objet = (clg_limitation_interface)pObjet;
        string l_ordreSQL = "INSERT INTO limitation_interface (lmi_cn, lmi_grp_cn, lmi_obi_cn, lmi_t_lmi_cn) VALUES (@lmi_cn, @lmi_grp_cn, @lmi_obi_cn, @lmi_t_lmi_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet limitation_interface");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_limitation_interface l_Objet = (clg_limitation_interface)pObjet;
        string l_ordreSQL = "DELETE FROM limitation_interface WHERE lmi_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet limitation_interface");
    }

    private void InjecterDonnees(clg_limitation_interface pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@lmi_cn");
		pValParams.Add(pObjet.lmi_cn);
		pParams.Add("@lmi_grp_cn");
		pValParams.Add(pObjet.groupes != null ? pObjet.groupes.grp_cn : (Int64?)null);
		pParams.Add("@lmi_obi_cn");
		pValParams.Add(pObjet.objets_interface != null ? pObjet.objets_interface.obi_cn : (Int64?)null);
		pParams.Add("@lmi_t_lmi_cn");
		pValParams.Add(pObjet.t_limitation_interface != null ? pObjet.t_limitation_interface.t_lmi_cn : (Int64?)null);

    }

#endregion
}
}
