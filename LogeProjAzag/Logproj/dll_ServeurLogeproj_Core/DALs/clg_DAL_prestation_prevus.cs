namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : prestation_prevus </summary>
public class clg_DAL_prestation_prevus : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_prestation_prevus(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ptp_cn, ptp_four_cn, ptp_ope_ele_cn, ptp_n_montant, ptp_d_debut, ptp_d_fin, ptp_t_ptp_cn, ptp_n_nb_unite, ptp_n_prix_unit FROM prestation_prevus";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_prestation_prevus l_Objet;
			l_Objet = new clg_prestation_prevus(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : double.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : double.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : double.Parse(l_rds.Donnee(i, 8)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_prestation_prevus l_Objet;
        if (pModele.Listeprestation_prevus.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_prestation_prevus) pModele.Listeprestation_prevus.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_prestation_prevus(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : double.Parse(l_Chaines[4]), l_Chaines[5] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[5]), l_Chaines[6] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : double.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : double.Parse(l_Chaines[9]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_prestation_prevus l_Objet = (clg_prestation_prevus)pObjet;
        string l_ordreSQL = "UPDATE prestation_prevus SET ptp_cn=@ptp_cn, ptp_four_cn=@ptp_four_cn, ptp_ope_ele_cn=@ptp_ope_ele_cn, ptp_n_montant=@ptp_n_montant, ptp_d_debut=@ptp_d_debut, ptp_d_fin=@ptp_d_fin, ptp_t_ptp_cn=@ptp_t_ptp_cn, ptp_n_nb_unite=@ptp_n_nb_unite, ptp_n_prix_unit=@ptp_n_prix_unit WHERE ptp_cn= @ptp_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet prestation_prevus");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_prestation_prevus l_Objet = (clg_prestation_prevus)pObjet;
        string l_ordreSQL = "INSERT INTO prestation_prevus (ptp_cn, ptp_four_cn, ptp_ope_ele_cn, ptp_n_montant, ptp_d_debut, ptp_d_fin, ptp_t_ptp_cn, ptp_n_nb_unite, ptp_n_prix_unit) VALUES (@ptp_cn, @ptp_four_cn, @ptp_ope_ele_cn, @ptp_n_montant, @ptp_d_debut, @ptp_d_fin, @ptp_t_ptp_cn, @ptp_n_nb_unite, @ptp_n_prix_unit);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet prestation_prevus");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_prestation_prevus l_Objet = (clg_prestation_prevus)pObjet;
        string l_ordreSQL = "DELETE FROM prestation_prevus WHERE ptp_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet prestation_prevus");
    }

    private void InjecterDonnees(clg_prestation_prevus pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ptp_cn");
		pValParams.Add(pObjet.ptp_cn);
		pParams.Add("@ptp_four_cn");
		pValParams.Add(pObjet.ptp_four_cn);
		pParams.Add("@ptp_ope_ele_cn");
		pValParams.Add(pObjet.operations != null ? pObjet.operations.ele_cn : (Int64?)null);
		pParams.Add("@ptp_n_montant");
		pValParams.Add(pObjet.ptp_n_montant);
		pParams.Add("@ptp_d_debut");
		pValParams.Add(pObjet.ptp_d_debut);
		pParams.Add("@ptp_d_fin");
		pValParams.Add(pObjet.ptp_d_fin);
		pParams.Add("@ptp_t_ptp_cn");
		pValParams.Add(pObjet.t_prestation != null ? pObjet.t_prestation.t_ptp_cn : (Int64?)null);
		pParams.Add("@ptp_n_nb_unite");
		pValParams.Add(pObjet.ptp_n_nb_unite);
		pParams.Add("@ptp_n_prix_unit");
		pValParams.Add(pObjet.ptp_n_prix_unit);

    }

#endregion
}
}
