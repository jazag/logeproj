namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : fonctions_perso </summary>
public class clg_DAL_fonctions_perso : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_fonctions_perso(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT fctp_cn, fctp_per_cn, fctp_t_fct_cn, fctp_d_debut, fctp_d_fin, fctp_b_archive FROM fonctions_perso";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_fonctions_perso l_Objet;
			l_Objet = new clg_fonctions_perso(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5)=="True");

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_fonctions_perso l_Objet;
        if (pModele.Listefonctions_perso.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_fonctions_perso) pModele.Listefonctions_perso.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_fonctions_perso(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[5]), l_Chaines[6]=="True");

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_fonctions_perso l_Objet = (clg_fonctions_perso)pObjet;
        string l_ordreSQL = "UPDATE fonctions_perso SET fctp_cn=@fctp_cn, fctp_per_cn=@fctp_per_cn, fctp_t_fct_cn=@fctp_t_fct_cn, fctp_d_debut=@fctp_d_debut, fctp_d_fin=@fctp_d_fin, fctp_b_archive=@fctp_b_archive WHERE fctp_cn= @fctp_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet fonctions_perso");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_fonctions_perso l_Objet = (clg_fonctions_perso)pObjet;
        string l_ordreSQL = "INSERT INTO fonctions_perso (fctp_cn, fctp_per_cn, fctp_t_fct_cn, fctp_d_debut, fctp_d_fin, fctp_b_archive) VALUES (@fctp_cn, @fctp_per_cn, @fctp_t_fct_cn, @fctp_d_debut, @fctp_d_fin, @fctp_b_archive);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet fonctions_perso");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_fonctions_perso l_Objet = (clg_fonctions_perso)pObjet;
        string l_ordreSQL = "DELETE FROM fonctions_perso WHERE fctp_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet fonctions_perso");
    }

    private void InjecterDonnees(clg_fonctions_perso pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@fctp_cn");
		pValParams.Add(pObjet.fctp_cn);
		pParams.Add("@fctp_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@fctp_t_fct_cn");
		pValParams.Add(pObjet.t_fonctions != null ? pObjet.t_fonctions.t_fct_cn : (Int64?)null);
		pParams.Add("@fctp_d_debut");
		pValParams.Add(pObjet.fctp_d_debut);
		pParams.Add("@fctp_d_fin");
		pValParams.Add(pObjet.fctp_d_fin);
		pParams.Add("@fctp_b_archive");
		pValParams.Add(pObjet.fctp_b_archive);

    }

#endregion
}
}
