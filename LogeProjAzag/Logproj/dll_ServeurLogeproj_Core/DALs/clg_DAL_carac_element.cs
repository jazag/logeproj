namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : carac_element </summary>
public partial class clg_DAL_carac_element : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_carac_element(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cel_cn, cel_ele_cn, cel_car_cn, cel_a_val, cel_m_val, cel_date_modif FROM carac_element";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_carac_element l_Objet;
			l_Objet = new clg_carac_element(pModele, Int64.Parse(l_rds.Donnee(i, 0))); 
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5) == "" ? DateTime.MinValue: DateTime.Parse(l_rds.Donnee(i,5)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_carac_element l_Objet;
        if (pModele.Listecarac_element.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_carac_element) pModele.Listecarac_element.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_carac_element(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4], l_Chaines[5], l_Chaines[6] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[6]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_carac_element l_Objet = (clg_carac_element)pObjet;
        string l_ordreSQL = "UPDATE carac_element SET cel_cn=@cel_cn, cel_ele_cn=@cel_ele_cn, cel_car_cn=@cel_car_cn, cel_a_val=@cel_a_val, cel_m_val=@cel_m_val, cel_date_modif=@cel_date_modif WHERE cel_cn= @cel_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet carac_element");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_carac_element l_Objet = (clg_carac_element)pObjet;
        string l_ordreSQL = "INSERT INTO carac_element (cel_cn, cel_ele_cn, cel_car_cn, cel_a_val, cel_m_val, cel_date_modif) VALUES (@cel_cn, @cel_ele_cn, @cel_car_cn, @cel_a_val, @cel_m_val, @cel_date_modif);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet carac_element");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_carac_element l_Objet = (clg_carac_element)pObjet;
        string l_ordreSQL = "DELETE FROM carac_element WHERE cel_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet carac_element");
    }

    private void InjecterDonnees(clg_carac_element pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@cel_cn");
		pValParams.Add(pObjet.cel_cn);
		pParams.Add("@cel_ele_cn");
		pValParams.Add(pObjet.elements != null ? pObjet.elements.ele_cn : (Int64?)null);
		pParams.Add("@cel_car_cn");
		pValParams.Add(pObjet.carac != null ? pObjet.carac.car_cn : (Int64?)null);
		pParams.Add("@cel_a_val");
		pValParams.Add(pObjet.cel_a_val);
		pParams.Add("@cel_m_val");
		pValParams.Add(pObjet.cel_m_val);
        pParams.Add("@cel_date_modif");
            pValParams.Add(pObjet.cel_date_modif);

    }

#endregion
}
}
