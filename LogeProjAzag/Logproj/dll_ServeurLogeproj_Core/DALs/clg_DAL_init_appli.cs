namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : init_appli </summary>
public class clg_DAL_init_appli : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_init_appli(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ini_cn, ini_uti_cn, ini_d_activation, ini_t_ini_cn, ini_a_valeur FROM init_appli";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_init_appli l_Objet;
			l_Objet = new clg_init_appli(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_init_appli l_Objet;
        if (pModele.Listeinit_appli.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_init_appli) pModele.Listeinit_appli.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_init_appli(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_init_appli l_Objet = (clg_init_appli)pObjet;
        string l_ordreSQL = "UPDATE init_appli SET ini_cn=@ini_cn, ini_uti_cn=@ini_uti_cn, ini_d_activation=@ini_d_activation, ini_t_ini_cn=@ini_t_ini_cn, ini_a_valeur=@ini_a_valeur WHERE ini_cn= @ini_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet init_appli");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_init_appli l_Objet = (clg_init_appli)pObjet;
        string l_ordreSQL = "INSERT INTO init_appli (ini_cn, ini_uti_cn, ini_d_activation, ini_t_ini_cn, ini_a_valeur) VALUES (@ini_cn, @ini_uti_cn, @ini_d_activation, @ini_t_ini_cn, @ini_a_valeur);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet init_appli");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_init_appli l_Objet = (clg_init_appli)pObjet;
        string l_ordreSQL = "DELETE FROM init_appli WHERE ini_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet init_appli");
    }

    private void InjecterDonnees(clg_init_appli pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ini_cn");
		pValParams.Add(pObjet.ini_cn);
		pParams.Add("@ini_uti_cn");
		pValParams.Add(pObjet.utilisateur != null ? pObjet.utilisateur.uti_cn : (Int64?)null);
		pParams.Add("@ini_d_activation");
		pValParams.Add(pObjet.ini_d_activation);
		pParams.Add("@ini_t_ini_cn");
		pValParams.Add(pObjet.t_init_appli != null ? pObjet.t_init_appli.t_ini_cn : (Int64?)null);
		pParams.Add("@ini_a_valeur");
		pValParams.Add(pObjet.ini_a_valeur);

    }

#endregion
}
}
