namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : planning </summary>
public class clg_DAL_planning : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_planning(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT pln_cn, pln_cal_d_date, pln_per_cn, pln_h_deb_mat_pre, pln_h_fin_mat_pre, pln_h_deb_apr_pre, pln_h_fin_apr_pre, pln_h_deb_mat_rea, pln_h_fin_mat_rea, pln_h_deb_apr_rea, pln_h_fin_apr_rea, pln_h_trajet, pln_b_bloque, pln_b_bloque_real FROM planning";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_planning l_Objet;
			l_Objet = new clg_planning(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 11)), l_rds.Donnee(i, 12)=="True", l_rds.Donnee(i, 13)=="True");

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_planning l_Objet;
        if (pModele.Listeplanning.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_planning) pModele.Listeplanning.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_planning(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[5]), l_Chaines[6] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[6]), l_Chaines[7] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[7]), l_Chaines[8] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[8]), l_Chaines[9] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[9]), l_Chaines[10] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[10]), l_Chaines[11] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[11]), l_Chaines[12] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[12]), l_Chaines[13]=="True", l_Chaines[14]=="True");

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_planning l_Objet = (clg_planning)pObjet;
        string l_ordreSQL = "UPDATE planning SET pln_cn=@pln_cn, pln_cal_d_date=@pln_cal_d_date, pln_per_cn=@pln_per_cn, pln_h_deb_mat_pre=@pln_h_deb_mat_pre, pln_h_fin_mat_pre=@pln_h_fin_mat_pre, pln_h_deb_apr_pre=@pln_h_deb_apr_pre, pln_h_fin_apr_pre=@pln_h_fin_apr_pre, pln_h_deb_mat_rea=@pln_h_deb_mat_rea, pln_h_fin_mat_rea=@pln_h_fin_mat_rea, pln_h_deb_apr_rea=@pln_h_deb_apr_rea, pln_h_fin_apr_rea=@pln_h_fin_apr_rea, pln_h_trajet=@pln_h_trajet, pln_b_bloque=@pln_b_bloque, pln_b_bloque_real=@pln_b_bloque_real WHERE pln_cn= @pln_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet planning");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_planning l_Objet = (clg_planning)pObjet;
        string l_ordreSQL = "INSERT INTO planning (pln_cn, pln_cal_d_date, pln_per_cn, pln_h_deb_mat_pre, pln_h_fin_mat_pre, pln_h_deb_apr_pre, pln_h_fin_apr_pre, pln_h_deb_mat_rea, pln_h_fin_mat_rea, pln_h_deb_apr_rea, pln_h_fin_apr_rea, pln_h_trajet, pln_b_bloque, pln_b_bloque_real) VALUES (@pln_cn, @pln_cal_d_date, @pln_per_cn, @pln_h_deb_mat_pre, @pln_h_fin_mat_pre, @pln_h_deb_apr_pre, @pln_h_fin_apr_pre, @pln_h_deb_mat_rea, @pln_h_fin_mat_rea, @pln_h_deb_apr_rea, @pln_h_fin_apr_rea, @pln_h_trajet, @pln_b_bloque, @pln_b_bloque_real);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet planning");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_planning l_Objet = (clg_planning)pObjet;
        string l_ordreSQL = "DELETE FROM planning WHERE pln_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet planning");
    }

    private void InjecterDonnees(clg_planning pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@pln_cn");
		pValParams.Add(pObjet.pln_cn);
		pParams.Add("@pln_cal_d_date");
		pValParams.Add(pObjet.calendrier != null ? pObjet.calendrier.cal_d_date : (DateTime?)null);
		pParams.Add("@pln_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@pln_h_deb_mat_pre");
		pValParams.Add(pObjet.pln_h_deb_mat_pre);
		pParams.Add("@pln_h_fin_mat_pre");
		pValParams.Add(pObjet.pln_h_fin_mat_pre);
		pParams.Add("@pln_h_deb_apr_pre");
		pValParams.Add(pObjet.pln_h_deb_apr_pre);
		pParams.Add("@pln_h_fin_apr_pre");
		pValParams.Add(pObjet.pln_h_fin_apr_pre);
		pParams.Add("@pln_h_deb_mat_rea");
		pValParams.Add(pObjet.pln_h_deb_mat_rea);
		pParams.Add("@pln_h_fin_mat_rea");
		pValParams.Add(pObjet.pln_h_fin_mat_rea);
		pParams.Add("@pln_h_deb_apr_rea");
		pValParams.Add(pObjet.pln_h_deb_apr_rea);
		pParams.Add("@pln_h_fin_apr_rea");
		pValParams.Add(pObjet.pln_h_fin_apr_rea);
		pParams.Add("@pln_h_trajet");
		pValParams.Add(pObjet.pln_h_trajet);
		pParams.Add("@pln_b_bloque");
		pValParams.Add(pObjet.pln_b_bloque);
		pParams.Add("@pln_b_bloque_real");
		pValParams.Add(pObjet.pln_b_bloque_real);

    }

#endregion
}
}
