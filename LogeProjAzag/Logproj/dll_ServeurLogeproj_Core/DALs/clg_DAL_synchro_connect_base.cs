namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : synchro_connect_base </summary>
public class clg_DAL_synchro_connect_base : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_synchro_connect_base(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT sbs_cn, sbs_a_ip, sbs_d_date_heure, sbs_n_tick FROM synchro_connect_base";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_synchro_connect_base l_Objet;
			l_Objet = new clg_synchro_connect_base(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_synchro_connect_base l_Objet;
        if (pModele.Listesynchro_connect_base.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_synchro_connect_base) pModele.Listesynchro_connect_base.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_synchro_connect_base(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_synchro_connect_base l_Objet = (clg_synchro_connect_base)pObjet;
        string l_ordreSQL = "UPDATE synchro_connect_base SET sbs_cn=@sbs_cn, sbs_a_ip=@sbs_a_ip, sbs_d_date_heure=@sbs_d_date_heure, sbs_n_tick=@sbs_n_tick WHERE sbs_cn= @sbs_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet synchro_connect_base");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_synchro_connect_base l_Objet = (clg_synchro_connect_base)pObjet;
        string l_ordreSQL = "INSERT INTO synchro_connect_base (sbs_cn, sbs_a_ip, sbs_d_date_heure, sbs_n_tick) VALUES (@sbs_cn, @sbs_a_ip, @sbs_d_date_heure, @sbs_n_tick);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet synchro_connect_base");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_synchro_connect_base l_Objet = (clg_synchro_connect_base)pObjet;
        string l_ordreSQL = "DELETE FROM synchro_connect_base WHERE sbs_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet synchro_connect_base");
    }

    private void InjecterDonnees(clg_synchro_connect_base pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@sbs_cn");
		pValParams.Add(pObjet.sbs_cn);
		pParams.Add("@sbs_a_ip");
		pValParams.Add(pObjet.sbs_a_ip);
		pParams.Add("@sbs_d_date_heure");
		pValParams.Add(pObjet.sbs_d_date_heure);
		pParams.Add("@sbs_n_tick");
		pValParams.Add(pObjet.sbs_n_tick);

    }

#endregion
}
}
