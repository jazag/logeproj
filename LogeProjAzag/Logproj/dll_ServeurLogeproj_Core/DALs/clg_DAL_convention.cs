namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : convention </summary>
public class clg_DAL_convention : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_convention(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cvn_cn, cvn_frf_cn, cvn_a_lib, cvn_d_debut FROM convention";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_convention l_Objet;
			l_Objet = new clg_convention(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_convention l_Objet;
        if (pModele.Listeconvention.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_convention) pModele.Listeconvention.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_convention(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3], l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_convention l_Objet = (clg_convention)pObjet;
        string l_ordreSQL = "UPDATE convention SET cvn_cn=@cvn_cn, cvn_frf_cn=@cvn_frf_cn, cvn_a_lib=@cvn_a_lib, cvn_d_debut=@cvn_d_debut WHERE cvn_cn= @cvn_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet convention");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_convention l_Objet = (clg_convention)pObjet;
        string l_ordreSQL = "INSERT INTO convention (cvn_cn, cvn_frf_cn, cvn_a_lib, cvn_d_debut) VALUES (@cvn_cn, @cvn_frf_cn, @cvn_a_lib, @cvn_d_debut);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet convention");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_convention l_Objet = (clg_convention)pObjet;
        string l_ordreSQL = "DELETE FROM convention WHERE cvn_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet convention");
    }

    private void InjecterDonnees(clg_convention pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@cvn_cn");
		pValParams.Add(pObjet.cvn_cn);
		pParams.Add("@cvn_frf_cn");
		pValParams.Add(pObjet.forfait != null ? pObjet.forfait.frf_cn : (Int64?)null);
		pParams.Add("@cvn_a_lib");
		pValParams.Add(pObjet.cvn_a_lib);
		pParams.Add("@cvn_d_debut");
		pValParams.Add(pObjet.cvn_d_debut);

    }

#endregion
}
}
