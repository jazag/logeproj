namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : lien_sage </summary>
public class clg_DAL_lien_sage : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_lien_sage(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT lsg_cn, lsg_n_ecno, lsg_n_mtt, lsg_a_lib, lsg_a_ncompte, lsg_d_date, lsg_a_code, lsg_ele_cn, lsg_jou_cn, lsg_n_lig, lsg_n_t_multi, lsg_n_lien_multi, lsg_tls_cn, lsg_base_cn, lsg_ctf_cn, lsg_ara_cn, lsg_four_cn, lsg_a_sens, lsg_d_sage, lsg_n_annee, lsg_sag_cn, lsg_jou_a_code, lsg_lien_lsg_cn FROM lien_sage";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_lien_sage l_Objet;
			l_Objet = new clg_lien_sage(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : double.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 11)), l_rds.Donnee(i, 12) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 13)), l_rds.Donnee(i, 14) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 14)), l_rds.Donnee(i, 15) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 15)), l_rds.Donnee(i, 16) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 16)), l_rds.Donnee(i, 17), l_rds.Donnee(i, 18) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 18)), l_rds.Donnee(i, 19) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 19)), l_rds.Donnee(i, 20) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 20)), l_rds.Donnee(i, 21), l_rds.Donnee(i, 22) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 22)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_lien_sage l_Objet;
        if (pModele.Listelien_sage.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_lien_sage) pModele.Listelien_sage.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_lien_sage(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : double.Parse(l_Chaines[3]), l_Chaines[4], l_Chaines[5], l_Chaines[6] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[6]), l_Chaines[7], l_Chaines[8] == "" ? -1 : Int64.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10] == "" ? -1 : Int64.Parse(l_Chaines[10]), l_Chaines[11] == "" ? -1 : Int64.Parse(l_Chaines[11]), l_Chaines[12] == "" ? -1 : Int64.Parse(l_Chaines[12]), l_Chaines[13] == "" ? -1 : Int64.Parse(l_Chaines[13]), l_Chaines[14] == "" ? -1 : Int64.Parse(l_Chaines[14]), l_Chaines[15] == "" ? -1 : Int64.Parse(l_Chaines[15]), l_Chaines[16] == "" ? -1 : Int64.Parse(l_Chaines[16]), l_Chaines[17] == "" ? -1 : Int64.Parse(l_Chaines[17]), l_Chaines[18], l_Chaines[19] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[19]), l_Chaines[20] == "" ? -1 : Int64.Parse(l_Chaines[20]), l_Chaines[21] == "" ? -1 : Int64.Parse(l_Chaines[21]), l_Chaines[22], l_Chaines[23] == "" ? -1 : Int64.Parse(l_Chaines[23]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_lien_sage l_Objet = (clg_lien_sage)pObjet;
        string l_ordreSQL = "UPDATE lien_sage SET lsg_cn=@lsg_cn, lsg_n_ecno=@lsg_n_ecno, lsg_n_mtt=@lsg_n_mtt, lsg_a_lib=@lsg_a_lib, lsg_a_ncompte=@lsg_a_ncompte, lsg_d_date=@lsg_d_date, lsg_a_code=@lsg_a_code, lsg_ele_cn=@lsg_ele_cn, lsg_jou_cn=@lsg_jou_cn, lsg_n_lig=@lsg_n_lig, lsg_n_t_multi=@lsg_n_t_multi, lsg_n_lien_multi=@lsg_n_lien_multi, lsg_tls_cn=@lsg_tls_cn, lsg_base_cn=@lsg_base_cn, lsg_ctf_cn=@lsg_ctf_cn, lsg_ara_cn=@lsg_ara_cn, lsg_four_cn=@lsg_four_cn, lsg_a_sens=@lsg_a_sens, lsg_d_sage=@lsg_d_sage, lsg_n_annee=@lsg_n_annee, lsg_sag_cn=@lsg_sag_cn, lsg_jou_a_code=@lsg_jou_a_code, lsg_lien_lsg_cn=@lsg_lien_lsg_cn WHERE lsg_cn= @lsg_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet lien_sage");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_lien_sage l_Objet = (clg_lien_sage)pObjet;
        string l_ordreSQL = "INSERT INTO lien_sage (lsg_cn, lsg_n_ecno, lsg_n_mtt, lsg_a_lib, lsg_a_ncompte, lsg_d_date, lsg_a_code, lsg_ele_cn, lsg_jou_cn, lsg_n_lig, lsg_n_t_multi, lsg_n_lien_multi, lsg_tls_cn, lsg_base_cn, lsg_ctf_cn, lsg_ara_cn, lsg_four_cn, lsg_a_sens, lsg_d_sage, lsg_n_annee, lsg_sag_cn, lsg_jou_a_code, lsg_lien_lsg_cn) VALUES (@lsg_cn, @lsg_n_ecno, @lsg_n_mtt, @lsg_a_lib, @lsg_a_ncompte, @lsg_d_date, @lsg_a_code, @lsg_ele_cn, @lsg_jou_cn, @lsg_n_lig, @lsg_n_t_multi, @lsg_n_lien_multi, @lsg_tls_cn, @lsg_base_cn, @lsg_ctf_cn, @lsg_ara_cn, @lsg_four_cn, @lsg_a_sens, @lsg_d_sage, @lsg_n_annee, @lsg_sag_cn, @lsg_jou_a_code, @lsg_lien_lsg_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet lien_sage");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_lien_sage l_Objet = (clg_lien_sage)pObjet;
        string l_ordreSQL = "DELETE FROM lien_sage WHERE lsg_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet lien_sage");
    }

    private void InjecterDonnees(clg_lien_sage pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@lsg_cn");
		pValParams.Add(pObjet.lsg_cn);
		pParams.Add("@lsg_n_ecno");
		pValParams.Add(pObjet.lsg_n_ecno);
		pParams.Add("@lsg_n_mtt");
		pValParams.Add(pObjet.lsg_n_mtt);
		pParams.Add("@lsg_a_lib");
		pValParams.Add(pObjet.lsg_a_lib);
		pParams.Add("@lsg_a_ncompte");
		pValParams.Add(pObjet.lsg_a_ncompte);
		pParams.Add("@lsg_d_date");
		pValParams.Add(pObjet.lsg_d_date);
		pParams.Add("@lsg_a_code");
		pValParams.Add(pObjet.lsg_a_code);
		pParams.Add("@lsg_ele_cn");
		pValParams.Add(pObjet.elements != null ? pObjet.elements.ele_cn : (Int64?)null);
		pParams.Add("@lsg_jou_cn");
		pValParams.Add(pObjet.lsg_jou_cn);
		pParams.Add("@lsg_n_lig");
		pValParams.Add(pObjet.lsg_n_lig);
		pParams.Add("@lsg_n_t_multi");
		pValParams.Add(pObjet.lsg_n_t_multi);
		pParams.Add("@lsg_n_lien_multi");
		pValParams.Add(pObjet.lsg_n_lien_multi);
		pParams.Add("@lsg_tls_cn");
		pValParams.Add(pObjet.t_lien_sage != null ? pObjet.t_lien_sage.tls_cn : (Int64?)null);
		pParams.Add("@lsg_base_cn");
		pValParams.Add(pObjet.basemae != null ? pObjet.basemae.base_cn : (Int64?)null);
		pParams.Add("@lsg_ctf_cn");
		pValParams.Add(pObjet.lsg_ctf_cn);
		pParams.Add("@lsg_ara_cn");
		pValParams.Add(pObjet.arretes != null ? pObjet.arretes.ara_cn : (Int64?)null);
		pParams.Add("@lsg_four_cn");
		pValParams.Add(pObjet.fournisseurs != null ? pObjet.fournisseurs.four_cn : (Int64?)null);
		pParams.Add("@lsg_a_sens");
		pValParams.Add(pObjet.lsg_a_sens);
		pParams.Add("@lsg_d_sage");
		pValParams.Add(pObjet.lsg_d_sage);
		pParams.Add("@lsg_n_annee");
		pValParams.Add(pObjet.lsg_n_annee);
		pParams.Add("@lsg_sag_cn");
		pValParams.Add(pObjet.lsg_sag_cn);
		pParams.Add("@lsg_jou_a_code");
		pValParams.Add(pObjet.journal != null ? pObjet.journal.jou_a_code : string.Empty);
		pParams.Add("@lsg_lien_lsg_cn");
		pValParams.Add(pObjet.lsg_lien_lsg_cn);

    }

#endregion
}
}
