namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : param_temps_travail </summary>
public class clg_DAL_param_temps_travail : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_param_temps_travail(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ptt_cn, ptt_n_nb_h_semaine, ptt_n_nb_h_semaine_max, ptt_n_recup_max, ptt_n_nb_h_jour_normal, ptt_n_nb_h_jour_max, ptt_n_nb_h_jour_conges, ptt_n_delai_recup, ptt_d_validite, ptt_d_fin_validite, ptt_n_mod_debloquer FROM param_temps_travail";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_param_temps_travail l_Objet;
			l_Objet = new clg_param_temps_travail(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : double.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : double.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : double.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : double.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 10)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_param_temps_travail l_Objet;
        if (pModele.Listeparam_temps_travail.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_param_temps_travail) pModele.Listeparam_temps_travail.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_param_temps_travail(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : double.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : double.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : double.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : double.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : Int64.Parse(l_Chaines[8]), l_Chaines[9] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[9]), l_Chaines[10] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[10]), l_Chaines[11] == "" ? -1 : Int64.Parse(l_Chaines[11]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_param_temps_travail l_Objet = (clg_param_temps_travail)pObjet;
        string l_ordreSQL = "UPDATE param_temps_travail SET ptt_cn=@ptt_cn, ptt_n_nb_h_semaine=@ptt_n_nb_h_semaine, ptt_n_nb_h_semaine_max=@ptt_n_nb_h_semaine_max, ptt_n_recup_max=@ptt_n_recup_max, ptt_n_nb_h_jour_normal=@ptt_n_nb_h_jour_normal, ptt_n_nb_h_jour_max=@ptt_n_nb_h_jour_max, ptt_n_nb_h_jour_conges=@ptt_n_nb_h_jour_conges, ptt_n_delai_recup=@ptt_n_delai_recup, ptt_d_validite=@ptt_d_validite, ptt_d_fin_validite=@ptt_d_fin_validite, ptt_n_mod_debloquer=@ptt_n_mod_debloquer WHERE ptt_cn= @ptt_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet param_temps_travail");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_param_temps_travail l_Objet = (clg_param_temps_travail)pObjet;
        string l_ordreSQL = "INSERT INTO param_temps_travail (ptt_cn, ptt_n_nb_h_semaine, ptt_n_nb_h_semaine_max, ptt_n_recup_max, ptt_n_nb_h_jour_normal, ptt_n_nb_h_jour_max, ptt_n_nb_h_jour_conges, ptt_n_delai_recup, ptt_d_validite, ptt_d_fin_validite, ptt_n_mod_debloquer) VALUES (@ptt_cn, @ptt_n_nb_h_semaine, @ptt_n_nb_h_semaine_max, @ptt_n_recup_max, @ptt_n_nb_h_jour_normal, @ptt_n_nb_h_jour_max, @ptt_n_nb_h_jour_conges, @ptt_n_delai_recup, @ptt_d_validite, @ptt_d_fin_validite, @ptt_n_mod_debloquer);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet param_temps_travail");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_param_temps_travail l_Objet = (clg_param_temps_travail)pObjet;
        string l_ordreSQL = "DELETE FROM param_temps_travail WHERE ptt_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet param_temps_travail");
    }

    private void InjecterDonnees(clg_param_temps_travail pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ptt_cn");
		pValParams.Add(pObjet.ptt_cn);
		pParams.Add("@ptt_n_nb_h_semaine");
		pValParams.Add(pObjet.ptt_n_nb_h_semaine);
		pParams.Add("@ptt_n_nb_h_semaine_max");
		pValParams.Add(pObjet.ptt_n_nb_h_semaine_max);
		pParams.Add("@ptt_n_recup_max");
		pValParams.Add(pObjet.ptt_n_recup_max);
		pParams.Add("@ptt_n_nb_h_jour_normal");
		pValParams.Add(pObjet.ptt_n_nb_h_jour_normal);
		pParams.Add("@ptt_n_nb_h_jour_max");
		pValParams.Add(pObjet.ptt_n_nb_h_jour_max);
		pParams.Add("@ptt_n_nb_h_jour_conges");
		pValParams.Add(pObjet.ptt_n_nb_h_jour_conges);
		pParams.Add("@ptt_n_delai_recup");
		pValParams.Add(pObjet.ptt_n_delai_recup);
		pParams.Add("@ptt_d_validite");
		pValParams.Add(pObjet.ptt_d_validite);
		pParams.Add("@ptt_d_fin_validite");
		pValParams.Add(pObjet.ptt_d_fin_validite);
		pParams.Add("@ptt_n_mod_debloquer");
		pValParams.Add(pObjet.ptt_n_mod_debloquer);

    }

#endregion
}
}
