namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : fournisseurs </summary>
public class clg_DAL_fournisseurs : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_fournisseurs(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT four_cn, four_a_lib, four_a_nom_res, four_a_prenom_res, four_n_archive, four_a_code, four_a_adresse, four_a_cp, four_a_ville, four_a_tel, four_a_fax, four_a_mail FROM fournisseurs";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_fournisseurs l_Objet;
			l_Objet = new clg_fournisseurs(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8), l_rds.Donnee(i, 9), l_rds.Donnee(i, 10), l_rds.Donnee(i, 11));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_fournisseurs l_Objet;
        if (pModele.Listefournisseurs.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_fournisseurs) pModele.Listefournisseurs.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_fournisseurs(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4], l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6], l_Chaines[7], l_Chaines[8], l_Chaines[9], l_Chaines[10], l_Chaines[11], l_Chaines[12]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_fournisseurs l_Objet = (clg_fournisseurs)pObjet;
        string l_ordreSQL = "UPDATE fournisseurs SET four_cn=@four_cn, four_a_lib=@four_a_lib, four_a_nom_res=@four_a_nom_res, four_a_prenom_res=@four_a_prenom_res, four_n_archive=@four_n_archive, four_a_code=@four_a_code, four_a_adresse=@four_a_adresse, four_a_cp=@four_a_cp, four_a_ville=@four_a_ville, four_a_tel=@four_a_tel, four_a_fax=@four_a_fax, four_a_mail=@four_a_mail WHERE four_cn= @four_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet fournisseurs");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_fournisseurs l_Objet = (clg_fournisseurs)pObjet;
        string l_ordreSQL = "INSERT INTO fournisseurs (four_cn, four_a_lib, four_a_nom_res, four_a_prenom_res, four_n_archive, four_a_code, four_a_adresse, four_a_cp, four_a_ville, four_a_tel, four_a_fax, four_a_mail) VALUES (@four_cn, @four_a_lib, @four_a_nom_res, @four_a_prenom_res, @four_n_archive, @four_a_code, @four_a_adresse, @four_a_cp, @four_a_ville, @four_a_tel, @four_a_fax, @four_a_mail);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet fournisseurs");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_fournisseurs l_Objet = (clg_fournisseurs)pObjet;
        string l_ordreSQL = "DELETE FROM fournisseurs WHERE four_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet fournisseurs");
    }

    private void InjecterDonnees(clg_fournisseurs pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@four_cn");
		pValParams.Add(pObjet.four_cn);
		pParams.Add("@four_a_lib");
		pValParams.Add(pObjet.four_a_lib);
		pParams.Add("@four_a_nom_res");
		pValParams.Add(pObjet.four_a_nom_res);
		pParams.Add("@four_a_prenom_res");
		pValParams.Add(pObjet.four_a_prenom_res);
		pParams.Add("@four_n_archive");
		pValParams.Add(pObjet.four_n_archive);
		pParams.Add("@four_a_code");
		pValParams.Add(pObjet.four_a_code);
		pParams.Add("@four_a_adresse");
		pValParams.Add(pObjet.four_a_adresse);
		pParams.Add("@four_a_cp");
		pValParams.Add(pObjet.four_a_cp);
		pParams.Add("@four_a_ville");
		pValParams.Add(pObjet.four_a_ville);
		pParams.Add("@four_a_tel");
		pValParams.Add(pObjet.four_a_tel);
		pParams.Add("@four_a_fax");
		pValParams.Add(pObjet.four_a_fax);
		pParams.Add("@four_a_mail");
		pValParams.Add(pObjet.four_a_mail);

    }


#endregion
}
}
