namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : contacts </summary>
public class clg_DAL_contacts : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_contacts(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ctc_cn, ctc_a_nom, ctc_a_prenom, ctc_fin_cn, ctc_t_fct_ctc_cn, ctc_a_tel, ctc_a_fax, ctc_a_mail FROM contacts";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_contacts l_Objet;
			l_Objet = new clg_contacts(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_contacts l_Objet;
        if (pModele.Listecontacts.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_contacts) pModele.Listecontacts.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_contacts(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6], l_Chaines[7], l_Chaines[8]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_contacts l_Objet = (clg_contacts)pObjet;
        string l_ordreSQL = "UPDATE contacts SET ctc_cn=@ctc_cn, ctc_a_nom=@ctc_a_nom, ctc_a_prenom=@ctc_a_prenom, ctc_fin_cn=@ctc_fin_cn, ctc_t_fct_ctc_cn=@ctc_t_fct_ctc_cn, ctc_a_tel=@ctc_a_tel, ctc_a_fax=@ctc_a_fax, ctc_a_mail=@ctc_a_mail WHERE ctc_cn= @ctc_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet contacts");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_contacts l_Objet = (clg_contacts)pObjet;
        string l_ordreSQL = "INSERT INTO contacts (ctc_cn, ctc_a_nom, ctc_a_prenom, ctc_fin_cn, ctc_t_fct_ctc_cn, ctc_a_tel, ctc_a_fax, ctc_a_mail) VALUES (@ctc_cn, @ctc_a_nom, @ctc_a_prenom, @ctc_fin_cn, @ctc_t_fct_ctc_cn, @ctc_a_tel, @ctc_a_fax, @ctc_a_mail);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet contacts");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_contacts l_Objet = (clg_contacts)pObjet;
        string l_ordreSQL = "DELETE FROM contacts WHERE ctc_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet contacts");
    }

    private void InjecterDonnees(clg_contacts pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ctc_cn");
		pValParams.Add(pObjet.ctc_cn);
		pParams.Add("@ctc_a_nom");
		pValParams.Add(pObjet.ctc_a_nom);
		pParams.Add("@ctc_a_prenom");
		pValParams.Add(pObjet.ctc_a_prenom);
		pParams.Add("@ctc_fin_cn");
		pValParams.Add(pObjet.financeurs != null ? pObjet.financeurs.fin_cn : (Int64?)null);
		pParams.Add("@ctc_t_fct_ctc_cn");
		pValParams.Add(pObjet.t_fonction_contact != null ? pObjet.t_fonction_contact.t_fct_ctc_cn : (Int64?)null);
		pParams.Add("@ctc_a_tel");
		pValParams.Add(pObjet.ctc_a_tel);
		pParams.Add("@ctc_a_fax");
		pValParams.Add(pObjet.ctc_a_fax);
		pParams.Add("@ctc_a_mail");
		pValParams.Add(pObjet.ctc_a_mail);

    }

#endregion
}
}
