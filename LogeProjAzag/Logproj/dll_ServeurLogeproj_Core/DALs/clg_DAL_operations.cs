namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : operations </summary>
public class clg_DAL_operations : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_operations(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT ope_ele_cn, ope_acp_cn, ope_t_opf_cn, ope_c_ope_cn, ope_n_validation_prev, ope_n_validation_real, ope_d_date_deb_valid_real, ope_d_date_fin_valid_real, ope_n_synchro_tps_prevus, ope_ann_cn, ope_n_taux_financeurs, ope_b_ope_terminee, ele_cn_pere, ele_n_niveau, ele_a_lib, ele_t_ele_cn, ele_a_code, ele_cn_racine, ele_n_archive, ele_d_debut_prev, ele_d_fin_reelle, ele_d_fin_prev, ele_d_debut_reelle, ele_d_archivage, ele_n_visibleetat FROM operations, elements WHERE 1=1 AND ele_cn = ope_ele_cn";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_operations l_Objet;
			l_Objet = new clg_operations(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11)=="True", l_rds.Donnee(i, 12) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 13)), l_rds.Donnee(i, 14), l_rds.Donnee(i, 15) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 15)), l_rds.Donnee(i, 16), l_rds.Donnee(i, 17) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 17)), l_rds.Donnee(i, 18) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 18)), l_rds.Donnee(i, 19) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 19)), l_rds.Donnee(i, 20) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 20)), l_rds.Donnee(i, 21) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 21)), l_rds.Donnee(i, 22) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 22)), l_rds.Donnee(i, 23) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 23)), l_rds.Donnee(i, 24) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 24)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_operations l_Objet;
        if (pModele.Listeoperations.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_operations) pModele.Listeoperations.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_operations(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[12]), l_Chaines[1] == "" ? -1 : Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6], l_Chaines[7], l_Chaines[8] == "" ? -1 : Int64.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10] == "" ? -1 : Int64.Parse(l_Chaines[10]), l_Chaines[11]=="True", l_Chaines[13] == "" ? -1 : Int64.Parse(l_Chaines[13]), l_Chaines[14] == "" ? -1 : Int64.Parse(l_Chaines[14]), l_Chaines[15], l_Chaines[16] == "" ? -1 : Int64.Parse(l_Chaines[16]), l_Chaines[17], l_Chaines[18] == "" ? -1 : Int64.Parse(l_Chaines[18]), l_Chaines[19] == "" ? -1 : Int64.Parse(l_Chaines[19]), l_Chaines[20] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[20]), l_Chaines[21] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[21]), l_Chaines[22] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[22]), l_Chaines[23] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[23]), l_Chaines[24] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[24]), l_Chaines[25] == "" ? -1 : Int64.Parse(l_Chaines[25]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_operations l_Objet = (clg_operations)pObjet;
        string l_ordreSQL = "UPDATE operations SET ope_ele_cn=@ope_ele_cn, ope_acp_cn=@ope_acp_cn, ope_t_opf_cn=@ope_t_opf_cn, ope_c_ope_cn=@ope_c_ope_cn, ope_n_validation_prev=@ope_n_validation_prev, ope_n_validation_real=@ope_n_validation_real, ope_d_date_deb_valid_real=@ope_d_date_deb_valid_real, ope_d_date_fin_valid_real=@ope_d_date_fin_valid_real, ope_n_synchro_tps_prevus=@ope_n_synchro_tps_prevus, ope_ann_cn=@ope_ann_cn, ope_n_taux_financeurs=@ope_n_taux_financeurs, ope_b_ope_terminee=@ope_b_ope_terminee WHERE ope_ele_cn= @ope_ele_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet operations");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_operations l_Objet = (clg_operations)pObjet;
        string l_ordreSQL = "INSERT INTO operations (ope_ele_cn, ope_acp_cn, ope_t_opf_cn, ope_c_ope_cn, ope_n_validation_prev, ope_n_validation_real, ope_d_date_deb_valid_real, ope_d_date_fin_valid_real, ope_n_synchro_tps_prevus, ope_ann_cn, ope_n_taux_financeurs, ope_b_ope_terminee) VALUES (@ope_ele_cn, @ope_acp_cn, @ope_t_opf_cn, @ope_c_ope_cn, @ope_n_validation_prev, @ope_n_validation_real, @ope_d_date_deb_valid_real, @ope_d_date_fin_valid_real, @ope_n_synchro_tps_prevus, @ope_ann_cn, @ope_n_taux_financeurs, @ope_b_ope_terminee);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet operations");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_operations l_Objet = (clg_operations)pObjet;
        string l_ordreSQL = "DELETE FROM operations WHERE ope_ele_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet operations");
    }

    private void InjecterDonnees(clg_operations pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@ope_ele_cn");
		pValParams.Add(pObjet.ele_cn);
		pParams.Add("@ope_acp_cn");
		pValParams.Add(pObjet.annee_comptable != null ? pObjet.annee_comptable.acp_cn : (Int64?)null);
		pParams.Add("@ope_t_opf_cn");
		pValParams.Add(pObjet.t_ope_financiere != null ? pObjet.t_ope_financiere.t_opf_cn : (Int64?)null);
		pParams.Add("@ope_c_ope_cn");
		pValParams.Add(pObjet.c_operation != null ? pObjet.c_operation.c_ope_cn : (Int64?)null);
		pParams.Add("@ope_n_validation_prev");
		pValParams.Add(pObjet.ope_n_validation_prev);
		pParams.Add("@ope_n_validation_real");
		pValParams.Add(pObjet.ope_n_validation_real);
		pParams.Add("@ope_d_date_deb_valid_real");
		pValParams.Add(pObjet.ope_d_date_deb_valid_real);
		pParams.Add("@ope_d_date_fin_valid_real");
		pValParams.Add(pObjet.ope_d_date_fin_valid_real);
		pParams.Add("@ope_n_synchro_tps_prevus");
		pValParams.Add(pObjet.ope_n_synchro_tps_prevus);
		pParams.Add("@ope_ann_cn");
		pValParams.Add(pObjet.ope_ann_cn);
		pParams.Add("@ope_n_taux_financeurs");
		pValParams.Add(pObjet.ope_n_taux_financeurs);
		pParams.Add("@ope_b_ope_terminee");
		pValParams.Add(pObjet.ope_b_ope_terminee);

    }


#endregion
}
}
