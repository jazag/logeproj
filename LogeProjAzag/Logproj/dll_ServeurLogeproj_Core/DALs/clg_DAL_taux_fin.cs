namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : taux_fin </summary>
public class clg_DAL_taux_fin : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_taux_fin(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT txf_cn, txf_tx_cn, txf_tit_cn, txf_fnp_cn FROM taux_fin";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_taux_fin l_Objet;
			l_Objet = new clg_taux_fin(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_taux_fin l_Objet;
        if (pModele.Listetaux_fin.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_taux_fin) pModele.Listetaux_fin.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_taux_fin(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_taux_fin l_Objet = (clg_taux_fin)pObjet;
        string l_ordreSQL = "UPDATE taux_fin SET txf_cn=@txf_cn, txf_tx_cn=@txf_tx_cn, txf_tit_cn=@txf_tit_cn, txf_fnp_cn=@txf_fnp_cn WHERE txf_cn= @txf_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet taux_fin");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_taux_fin l_Objet = (clg_taux_fin)pObjet;
        string l_ordreSQL = "INSERT INTO taux_fin (txf_cn, txf_tx_cn, txf_tit_cn, txf_fnp_cn) VALUES (@txf_cn, @txf_tx_cn, @txf_tit_cn, @txf_fnp_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet taux_fin");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_taux_fin l_Objet = (clg_taux_fin)pObjet;
        string l_ordreSQL = "DELETE FROM taux_fin WHERE txf_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet taux_fin");
    }

    private void InjecterDonnees(clg_taux_fin pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@txf_cn");
		pValParams.Add(pObjet.txf_cn);
		pParams.Add("@txf_tx_cn");
		pValParams.Add(pObjet.taux != null ? pObjet.taux.tx_cn : (Int64?)null);
		pParams.Add("@txf_tit_cn");
		pValParams.Add(pObjet.temps_intervenants != null ? pObjet.temps_intervenants.tit_cn : (Int64?)null);
		pParams.Add("@txf_fnp_cn");
		pValParams.Add(pObjet.financement_prevus != null ? pObjet.financement_prevus.fnp_cn : (Int64?)null);

    }

#endregion
}
}
