namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : vehicules </summary>
public class clg_DAL_vehicules : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_vehicules(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT veh_cn, veh_a_identifiant, veh_a_matricule, veh_a_description, veh_n_kmmax, veh_a_marque, veh_a_modele, veh_n_archive, veh_n_annee, veh_a_code FROM vehicules";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_vehicules l_Objet;
			l_Objet = new clg_vehicules(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_vehicules l_Objet;
        if (pModele.Listevehicules.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_vehicules) pModele.Listevehicules.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_vehicules(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4], l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6], l_Chaines[7], l_Chaines[8] == "" ? -1 : Int64.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_vehicules l_Objet = (clg_vehicules)pObjet;
        string l_ordreSQL = "UPDATE vehicules SET veh_cn=@veh_cn, veh_a_identifiant=@veh_a_identifiant, veh_a_matricule=@veh_a_matricule, veh_a_description=@veh_a_description, veh_n_kmmax=@veh_n_kmmax, veh_a_marque=@veh_a_marque, veh_a_modele=@veh_a_modele, veh_n_archive=@veh_n_archive, veh_n_annee=@veh_n_annee, veh_a_code=@veh_a_code WHERE veh_cn= @veh_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet vehicules");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_vehicules l_Objet = (clg_vehicules)pObjet;
        string l_ordreSQL = "INSERT INTO vehicules (veh_cn, veh_a_identifiant, veh_a_matricule, veh_a_description, veh_n_kmmax, veh_a_marque, veh_a_modele, veh_n_archive, veh_n_annee, veh_a_code) VALUES (@veh_cn, @veh_a_identifiant, @veh_a_matricule, @veh_a_description, @veh_n_kmmax, @veh_a_marque, @veh_a_modele, @veh_n_archive, @veh_n_annee, @veh_a_code);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet vehicules");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_vehicules l_Objet = (clg_vehicules)pObjet;
        string l_ordreSQL = "DELETE FROM vehicules WHERE veh_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet vehicules");
    }

    private void InjecterDonnees(clg_vehicules pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@veh_cn");
		pValParams.Add(pObjet.veh_cn);
		pParams.Add("@veh_a_identifiant");
		pValParams.Add(pObjet.veh_a_identifiant);
		pParams.Add("@veh_a_matricule");
		pValParams.Add(pObjet.veh_a_matricule);
		pParams.Add("@veh_a_description");
		pValParams.Add(pObjet.veh_a_description);
		pParams.Add("@veh_n_kmmax");
		pValParams.Add(pObjet.veh_n_kmmax);
		pParams.Add("@veh_a_marque");
		pValParams.Add(pObjet.veh_a_marque);
		pParams.Add("@veh_a_modele");
		pValParams.Add(pObjet.veh_a_modele);
		pParams.Add("@veh_n_archive");
		pValParams.Add(pObjet.veh_n_archive);
		pParams.Add("@veh_n_annee");
		pValParams.Add(pObjet.veh_n_annee);
		pParams.Add("@veh_a_code");
		pValParams.Add(pObjet.veh_a_code);

    }


#endregion
}
}
