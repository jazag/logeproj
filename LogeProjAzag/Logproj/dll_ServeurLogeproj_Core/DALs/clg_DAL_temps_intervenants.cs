namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : temps_intervenants </summary>
public class clg_DAL_temps_intervenants : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_temps_intervenants(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT tit_cn, tit_per_cn, tit_n_heures, tit_d_debut, tit_d_fin, tit_ele_cn, tit_n_affiche, tit_n_heures_sync, tit_tx_cn, tit_n_heures_visible, tit_b_plan_charge, tit_n_heures_plan_charge FROM temps_intervenants";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_temps_intervenants l_Objet;
			l_Objet = new clg_temps_intervenants(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : double.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : double.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9) == "" ? -1 : double.Parse(l_rds.Donnee(i, 9)), l_rds.Donnee(i, 10)=="True", l_rds.Donnee(i, 11) == "" ? -1 : double.Parse(l_rds.Donnee(i, 11)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_temps_intervenants l_Objet;
        if (pModele.Listetemps_intervenants.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_temps_intervenants) pModele.Listetemps_intervenants.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_temps_intervenants(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : double.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]), l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : double.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10] == "" ? -1 : double.Parse(l_Chaines[10]), l_Chaines[11]=="True", l_Chaines[12] == "" ? -1 : double.Parse(l_Chaines[12]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_temps_intervenants l_Objet = (clg_temps_intervenants)pObjet;
        string l_ordreSQL = "UPDATE temps_intervenants SET tit_cn=@tit_cn, tit_per_cn=@tit_per_cn, tit_n_heures=@tit_n_heures, tit_d_debut=@tit_d_debut, tit_d_fin=@tit_d_fin, tit_ele_cn=@tit_ele_cn, tit_n_affiche=@tit_n_affiche, tit_n_heures_sync=@tit_n_heures_sync, tit_tx_cn=@tit_tx_cn, tit_n_heures_visible=@tit_n_heures_visible, tit_b_plan_charge=@tit_b_plan_charge, tit_n_heures_plan_charge=@tit_n_heures_plan_charge WHERE tit_cn= @tit_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet temps_intervenants");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_temps_intervenants l_Objet = (clg_temps_intervenants)pObjet;
        string l_ordreSQL = "INSERT INTO temps_intervenants (tit_cn, tit_per_cn, tit_n_heures, tit_d_debut, tit_d_fin, tit_ele_cn, tit_n_affiche, tit_n_heures_sync, tit_tx_cn, tit_n_heures_visible, tit_b_plan_charge, tit_n_heures_plan_charge) VALUES (@tit_cn, @tit_per_cn, @tit_n_heures, @tit_d_debut, @tit_d_fin, @tit_ele_cn, @tit_n_affiche, @tit_n_heures_sync, @tit_tx_cn, @tit_n_heures_visible, @tit_b_plan_charge, @tit_n_heures_plan_charge);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet temps_intervenants");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_temps_intervenants l_Objet = (clg_temps_intervenants)pObjet;
        string l_ordreSQL = "DELETE FROM temps_intervenants WHERE tit_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet temps_intervenants");
    }

    private void InjecterDonnees(clg_temps_intervenants pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@tit_cn");
		pValParams.Add(pObjet.tit_cn);
		pParams.Add("@tit_per_cn");
		pValParams.Add(pObjet.personnels != null ? pObjet.personnels.per_cn : (Int64?)null);
		pParams.Add("@tit_n_heures");
		pValParams.Add(pObjet.tit_n_heures);
		pParams.Add("@tit_d_debut");
		pValParams.Add(pObjet.tit_d_debut);
		pParams.Add("@tit_d_fin");
		pValParams.Add(pObjet.tit_d_fin);
		pParams.Add("@tit_ele_cn");
		pValParams.Add(pObjet.operations != null ? pObjet.operations.ele_cn : (Int64?)null);
		pParams.Add("@tit_n_affiche");
		pValParams.Add(pObjet.tit_n_affiche);
		pParams.Add("@tit_n_heures_sync");
		pValParams.Add(pObjet.tit_n_heures_sync);
		pParams.Add("@tit_tx_cn");
		pValParams.Add(pObjet.tit_tx_cn);
		pParams.Add("@tit_n_heures_visible");
		pValParams.Add(pObjet.tit_n_heures_visible);
		pParams.Add("@tit_b_plan_charge");
		pValParams.Add(pObjet.tit_b_plan_charge);
		pParams.Add("@tit_n_heures_plan_charge");
		pValParams.Add(pObjet.tit_n_heures_plan_charge);

    }

#endregion
}
}
