namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : etat_arrete </summary>
public class clg_DAL_etat_arrete : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_etat_arrete(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT eta_cn, eta_a_lib, eta_a_color FROM etat_arrete";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_etat_arrete l_Objet;
			l_Objet = new clg_etat_arrete(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_etat_arrete l_Objet;
        if (pModele.Listeetat_arrete.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_etat_arrete) pModele.Listeetat_arrete.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_etat_arrete(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_etat_arrete l_Objet = (clg_etat_arrete)pObjet;
        string l_ordreSQL = "UPDATE etat_arrete SET eta_cn=@eta_cn, eta_a_lib=@eta_a_lib, eta_a_color=@eta_a_color WHERE eta_cn= @eta_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet etat_arrete");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_etat_arrete l_Objet = (clg_etat_arrete)pObjet;
        string l_ordreSQL = "INSERT INTO etat_arrete (eta_cn, eta_a_lib, eta_a_color) VALUES (@eta_cn, @eta_a_lib, @eta_a_color);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet etat_arrete");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_etat_arrete l_Objet = (clg_etat_arrete)pObjet;
        string l_ordreSQL = "DELETE FROM etat_arrete WHERE eta_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet etat_arrete");
    }

    private void InjecterDonnees(clg_etat_arrete pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@eta_cn");
		pValParams.Add(pObjet.eta_cn);
		pParams.Add("@eta_a_lib");
		pValParams.Add(pObjet.eta_a_lib);
		pParams.Add("@eta_a_color");
		pValParams.Add(pObjet.eta_a_color);

    }

#endregion
}
}
