namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : annee_comptable </summary>
public class clg_DAL_annee_comptable : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_annee_comptable(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT acp_cn, acp_d_debut, acp_d_fin, acp_m_comment, acp_a_libel FROM annee_comptable";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_annee_comptable l_Objet;
			l_Objet = new clg_annee_comptable(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_annee_comptable l_Objet;
        if (pModele.Listeannee_comptable.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_annee_comptable) pModele.Listeannee_comptable.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_annee_comptable(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[2]), l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4], l_Chaines[5]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_annee_comptable l_Objet = (clg_annee_comptable)pObjet;
        string l_ordreSQL = "UPDATE annee_comptable SET acp_cn=@acp_cn, acp_d_debut=@acp_d_debut, acp_d_fin=@acp_d_fin, acp_m_comment=@acp_m_comment, acp_a_libel=@acp_a_libel WHERE acp_cn= @acp_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet annee_comptable");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_annee_comptable l_Objet = (clg_annee_comptable)pObjet;
        string l_ordreSQL = "INSERT INTO annee_comptable (acp_cn, acp_d_debut, acp_d_fin, acp_m_comment, acp_a_libel) VALUES (@acp_cn, @acp_d_debut, @acp_d_fin, @acp_m_comment, @acp_a_libel);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet annee_comptable");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_annee_comptable l_Objet = (clg_annee_comptable)pObjet;
        string l_ordreSQL = "DELETE FROM annee_comptable WHERE acp_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet annee_comptable");
    }

    private void InjecterDonnees(clg_annee_comptable pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@acp_cn");
		pValParams.Add(pObjet.acp_cn);
		pParams.Add("@acp_d_debut");
		pValParams.Add(pObjet.acp_d_debut);
		pParams.Add("@acp_d_fin");
		pValParams.Add(pObjet.acp_d_fin);
		pParams.Add("@acp_m_comment");
		pValParams.Add(pObjet.acp_m_comment);
		pParams.Add("@acp_a_libel");
		pValParams.Add(pObjet.acp_a_libel);

    }

#endregion
}
}
