namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : tb_sage </summary>
public class clg_DAL_tb_sage : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_tb_sage(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT sag_ec_no, sag_ea_ligne, sag_d_sage, sag_jm_date, sag_ec_jour, sag_ec_sens, sag_ec_intitule, sag_ca_num, sag_ea_montant, sag_jo_num, sag_cg_num, sag_annee_exe, sag_bas_cn, sag_ara_cn, sag_four_cn, sag_tfc_cn, sag_cn, sag_fin_cn, sag_n_piece FROM tb_sage";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_tb_sage l_Objet;
			l_Objet = new clg_tb_sage(pModele, int.Parse(l_rds.Donnee(i, 16)));
			l_Objet.Initialise(int.Parse(l_rds.Donnee(i, 16)), l_rds.Donnee(i, 0) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8) == "" ? -1 : double.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9), l_rds.Donnee(i, 10), l_rds.Donnee(i, 11) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 11)), l_rds.Donnee(i, 12) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 13)), l_rds.Donnee(i, 14) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 14)), l_rds.Donnee(i, 15) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 15)), l_rds.Donnee(i, 17) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 17)), l_rds.Donnee(i, 18));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_tb_sage l_Objet;
        if (pModele.Listetb_sage.Dictionnaire.ContainsKey(int.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_tb_sage) pModele.Listetb_sage.Dictionnaire[int.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_tb_sage(pModele, int.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(int.Parse(l_Chaines[17]), l_Chaines[1] == "" ? -1 : Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]), l_Chaines[7], l_Chaines[8], l_Chaines[9] == "" ? -1 : double.Parse(l_Chaines[9]), l_Chaines[10], l_Chaines[11], l_Chaines[12] == "" ? -1 : Int64.Parse(l_Chaines[12]), l_Chaines[13] == "" ? -1 : Int64.Parse(l_Chaines[13]), l_Chaines[14] == "" ? -1 : Int64.Parse(l_Chaines[14]), l_Chaines[15] == "" ? -1 : Int64.Parse(l_Chaines[15]), l_Chaines[16] == "" ? -1 : Int64.Parse(l_Chaines[16]), l_Chaines[18] == "" ? -1 : Int64.Parse(l_Chaines[18]), l_Chaines[19]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_tb_sage l_Objet = (clg_tb_sage)pObjet;
        string l_ordreSQL = "UPDATE tb_sage SET sag_ec_no=@sag_ec_no, sag_ea_ligne=@sag_ea_ligne, sag_d_sage=@sag_d_sage, sag_jm_date=@sag_jm_date, sag_ec_jour=@sag_ec_jour, sag_ec_sens=@sag_ec_sens, sag_ec_intitule=@sag_ec_intitule, sag_ca_num=@sag_ca_num, sag_ea_montant=@sag_ea_montant, sag_jo_num=@sag_jo_num, sag_cg_num=@sag_cg_num, sag_annee_exe=@sag_annee_exe, sag_bas_cn=@sag_bas_cn, sag_ara_cn=@sag_ara_cn, sag_four_cn=@sag_four_cn, sag_tfc_cn=@sag_tfc_cn, sag_cn=@sag_cn, sag_fin_cn=@sag_fin_cn, sag_n_piece=@sag_n_piece WHERE sag_cn= @sag_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet tb_sage");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_tb_sage l_Objet = (clg_tb_sage)pObjet;
        string l_ordreSQL = "INSERT INTO tb_sage (sag_ec_no, sag_ea_ligne, sag_d_sage, sag_jm_date, sag_ec_jour, sag_ec_sens, sag_ec_intitule, sag_ca_num, sag_ea_montant, sag_jo_num, sag_cg_num, sag_annee_exe, sag_bas_cn, sag_ara_cn, sag_four_cn, sag_tfc_cn, sag_cn, sag_fin_cn, sag_n_piece) VALUES (@sag_ec_no, @sag_ea_ligne, @sag_d_sage, @sag_jm_date, @sag_ec_jour, @sag_ec_sens, @sag_ec_intitule, @sag_ca_num, @sag_ea_montant, @sag_jo_num, @sag_cg_num, @sag_annee_exe, @sag_bas_cn, @sag_ara_cn, @sag_four_cn, @sag_tfc_cn, @sag_cn, @sag_fin_cn, @sag_n_piece);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet tb_sage");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_tb_sage l_Objet = (clg_tb_sage)pObjet;
        string l_ordreSQL = "DELETE FROM tb_sage WHERE sag_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet tb_sage");
    }

    private void InjecterDonnees(clg_tb_sage pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@sag_ec_no");
		pValParams.Add(pObjet.sag_ec_no);
		pParams.Add("@sag_ea_ligne");
		pValParams.Add(pObjet.sag_ea_ligne);
		pParams.Add("@sag_d_sage");
		pValParams.Add(pObjet.sag_d_sage);
		pParams.Add("@sag_jm_date");
		pValParams.Add(pObjet.sag_jm_date);
		pParams.Add("@sag_ec_jour");
		pValParams.Add(pObjet.sag_ec_jour);
		pParams.Add("@sag_ec_sens");
		pValParams.Add(pObjet.sag_ec_sens);
		pParams.Add("@sag_ec_intitule");
		pValParams.Add(pObjet.sag_ec_intitule);
		pParams.Add("@sag_ca_num");
		pValParams.Add(pObjet.sag_ca_num);
		pParams.Add("@sag_ea_montant");
		pValParams.Add(pObjet.sag_ea_montant);
		pParams.Add("@sag_jo_num");
		pValParams.Add(pObjet.sag_jo_num);
		pParams.Add("@sag_cg_num");
		pValParams.Add(pObjet.sag_cg_num);
		pParams.Add("@sag_annee_exe");
		pValParams.Add(pObjet.sag_annee_exe);
		pParams.Add("@sag_bas_cn");
		pValParams.Add(pObjet.basemae != null ? pObjet.basemae.base_cn : (Int64?)null);
		pParams.Add("@sag_ara_cn");
		pValParams.Add(pObjet.sag_ara_cn);
		pParams.Add("@sag_four_cn");
		pValParams.Add(pObjet.sag_four_cn);
		pParams.Add("@sag_tfc_cn");
		pValParams.Add(pObjet.type_fonct_compta != null ? pObjet.type_fonct_compta.tfc_cn : (Int64?)null);
		pParams.Add("@sag_cn");
		pValParams.Add(pObjet.sag_cn);
		pParams.Add("@sag_fin_cn");
		pValParams.Add(pObjet.financeurs != null ? pObjet.financeurs.fin_cn : (Int64?)null);
		pParams.Add("@sag_n_piece");
		pValParams.Add(pObjet.sag_n_piece);

    }

#endregion
}
}
