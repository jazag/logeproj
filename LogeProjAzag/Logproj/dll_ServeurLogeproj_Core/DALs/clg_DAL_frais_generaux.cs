namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : frais_generaux </summary>
public class clg_DAL_frais_generaux : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_frais_generaux(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT frg_cn, frg_acp_cn, frg_n_frais, frg_n_massesal FROM frais_generaux";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_frais_generaux l_Objet;
			l_Objet = new clg_frais_generaux(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : double.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_frais_generaux l_Objet;
        if (pModele.Listefrais_generaux.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_frais_generaux) pModele.Listefrais_generaux.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_frais_generaux(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : double.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_frais_generaux l_Objet = (clg_frais_generaux)pObjet;
        string l_ordreSQL = "UPDATE frais_generaux SET frg_cn=@frg_cn, frg_acp_cn=@frg_acp_cn, frg_n_frais=@frg_n_frais, frg_n_massesal=@frg_n_massesal WHERE frg_cn= @frg_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet frais_generaux");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_frais_generaux l_Objet = (clg_frais_generaux)pObjet;
        string l_ordreSQL = "INSERT INTO frais_generaux (frg_cn, frg_acp_cn, frg_n_frais, frg_n_massesal) VALUES (@frg_cn, @frg_acp_cn, @frg_n_frais, @frg_n_massesal);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet frais_generaux");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_frais_generaux l_Objet = (clg_frais_generaux)pObjet;
        string l_ordreSQL = "DELETE FROM frais_generaux WHERE frg_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet frais_generaux");
    }

    private void InjecterDonnees(clg_frais_generaux pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@frg_cn");
		pValParams.Add(pObjet.frg_cn);
		pParams.Add("@frg_acp_cn");
		pValParams.Add(pObjet.annee_comptable != null ? pObjet.annee_comptable.acp_cn : (Int64?)null);
		pParams.Add("@frg_n_frais");
		pValParams.Add(pObjet.frg_n_frais);
		pParams.Add("@frg_n_massesal");
		pValParams.Add(pObjet.frg_n_massesal);

    }

#endregion
}
}
