namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : heures_sup_prevues </summary>
public class clg_DAL_heures_sup_prevues : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_heures_sup_prevues(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT hsp_cn, hsp_n_nb_heure_sup, hsp_sem_cn, hsp_per_cn FROM heures_sup_prevues";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_heures_sup_prevues l_Objet;
			l_Objet = new clg_heures_sup_prevues(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : double.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_heures_sup_prevues l_Objet;
        if (pModele.Listeheures_sup_prevues.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_heures_sup_prevues) pModele.Listeheures_sup_prevues.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_heures_sup_prevues(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : double.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_heures_sup_prevues l_Objet = (clg_heures_sup_prevues)pObjet;
        string l_ordreSQL = "UPDATE heures_sup_prevues SET hsp_cn=@hsp_cn, hsp_n_nb_heure_sup=@hsp_n_nb_heure_sup, hsp_sem_cn=@hsp_sem_cn, hsp_per_cn=@hsp_per_cn WHERE hsp_cn= @hsp_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet heures_sup_prevues");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_heures_sup_prevues l_Objet = (clg_heures_sup_prevues)pObjet;
        string l_ordreSQL = "INSERT INTO heures_sup_prevues (hsp_cn, hsp_n_nb_heure_sup, hsp_sem_cn, hsp_per_cn) VALUES (@hsp_cn, @hsp_n_nb_heure_sup, @hsp_sem_cn, @hsp_per_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet heures_sup_prevues");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_heures_sup_prevues l_Objet = (clg_heures_sup_prevues)pObjet;
        string l_ordreSQL = "DELETE FROM heures_sup_prevues WHERE hsp_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet heures_sup_prevues");
    }

    private void InjecterDonnees(clg_heures_sup_prevues pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@hsp_cn");
		pValParams.Add(pObjet.hsp_cn);
		pParams.Add("@hsp_n_nb_heure_sup");
		pValParams.Add(pObjet.hsp_n_nb_heure_sup);
		pParams.Add("@hsp_sem_cn");
		pValParams.Add(pObjet.hsp_sem_cn);
		pParams.Add("@hsp_per_cn");
		pValParams.Add(pObjet.hsp_per_cn);

    }

#endregion
}
}
