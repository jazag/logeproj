namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : comment_vehicule </summary>
public class clg_DAL_comment_vehicule : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_comment_vehicule(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT cmv_com_cn, cmv_veh_cn FROM comment_vehicule";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_comment l_comment = (clg_comment) pModele.Listecomment.Dictionnaire[Int64.Parse(l_rds.Donnee(i, 0))];
            clg_vehicules l_vehicules = (clg_vehicules) pModele.Listevehicules.Dictionnaire[Int64.Parse(l_rds.Donnee(i, 1))];

            l_comment.Listevehicules.Add(l_vehicules);
            l_vehicules.Listecomment.Add(l_comment);
        }
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {

    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {

    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {

    }


#endregion
}
}
