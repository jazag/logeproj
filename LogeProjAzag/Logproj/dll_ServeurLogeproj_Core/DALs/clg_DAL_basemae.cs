namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : basemae </summary>
public class clg_DAL_basemae : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_basemae(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT base_cn, base_a_nom, base_d_debut, base_d_fin, base_a_odbc, base_a_chemin, base_tba_cn, base_a_copie FROM basemae";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_basemae l_Objet;
			l_Objet = new clg_basemae(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_basemae l_Objet;
        if (pModele.Listebasemae.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_basemae) pModele.Listebasemae.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_basemae(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5], l_Chaines[6], l_Chaines[7] == "" ? -1 : Int64.Parse(l_Chaines[7]), l_Chaines[8]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_basemae l_Objet = (clg_basemae)pObjet;
        string l_ordreSQL = "UPDATE basemae SET base_cn=@base_cn, base_a_nom=@base_a_nom, base_d_debut=@base_d_debut, base_d_fin=@base_d_fin, base_a_odbc=@base_a_odbc, base_a_chemin=@base_a_chemin, base_tba_cn=@base_tba_cn, base_a_copie=@base_a_copie WHERE base_cn= @base_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet basemae");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_basemae l_Objet = (clg_basemae)pObjet;
        string l_ordreSQL = "INSERT INTO basemae (base_cn, base_a_nom, base_d_debut, base_d_fin, base_a_odbc, base_a_chemin, base_tba_cn, base_a_copie) VALUES (@base_cn, @base_a_nom, @base_d_debut, @base_d_fin, @base_a_odbc, @base_a_chemin, @base_tba_cn, @base_a_copie);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet basemae");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_basemae l_Objet = (clg_basemae)pObjet;
        string l_ordreSQL = "DELETE FROM basemae WHERE base_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet basemae");
    }

    private void InjecterDonnees(clg_basemae pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@base_cn");
		pValParams.Add(pObjet.base_cn);
		pParams.Add("@base_a_nom");
		pValParams.Add(pObjet.base_a_nom);
		pParams.Add("@base_d_debut");
		pValParams.Add(pObjet.base_d_debut);
		pParams.Add("@base_d_fin");
		pValParams.Add(pObjet.base_d_fin);
		pParams.Add("@base_a_odbc");
		pValParams.Add(pObjet.base_a_odbc);
		pParams.Add("@base_a_chemin");
		pValParams.Add(pObjet.base_a_chemin);
		pParams.Add("@base_tba_cn");
		pValParams.Add(pObjet.type_base_mae != null ? pObjet.type_base_mae.tba_cn : (Int64?)null);
		pParams.Add("@base_a_copie");
		pValParams.Add(pObjet.base_a_copie);

    }

#endregion
}
}
