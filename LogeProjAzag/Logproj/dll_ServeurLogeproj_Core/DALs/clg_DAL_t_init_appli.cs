namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : t_init_appli </summary>
public class clg_DAL_t_init_appli : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_t_init_appli(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT t_ini_cn, t_ini_a_lib, t_ini_m_comment FROM t_init_appli";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_t_init_appli l_Objet;
			l_Objet = new clg_t_init_appli(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_t_init_appli l_Objet;
        if (pModele.Listet_init_appli.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_t_init_appli) pModele.Listet_init_appli.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_t_init_appli(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_t_init_appli l_Objet = (clg_t_init_appli)pObjet;
        string l_ordreSQL = "UPDATE t_init_appli SET t_ini_cn=@t_ini_cn, t_ini_a_lib=@t_ini_a_lib, t_ini_m_comment=@t_ini_m_comment WHERE t_ini_cn= @t_ini_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet t_init_appli");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_t_init_appli l_Objet = (clg_t_init_appli)pObjet;
        string l_ordreSQL = "INSERT INTO t_init_appli (t_ini_cn, t_ini_a_lib, t_ini_m_comment) VALUES (@t_ini_cn, @t_ini_a_lib, @t_ini_m_comment);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet t_init_appli");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_t_init_appli l_Objet = (clg_t_init_appli)pObjet;
        string l_ordreSQL = "DELETE FROM t_init_appli WHERE t_ini_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet t_init_appli");
    }

    private void InjecterDonnees(clg_t_init_appli pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@t_ini_cn");
		pValParams.Add(pObjet.t_ini_cn);
		pParams.Add("@t_ini_a_lib");
		pValParams.Add(pObjet.t_ini_a_lib);
		pParams.Add("@t_ini_m_comment");
		pValParams.Add(pObjet.t_ini_m_comment);

    }

#endregion
}
}
