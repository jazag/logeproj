namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : projets_bis </summary>
public class clg_DAL_projets_bis : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_projets_bis(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT prj_ele_cn, prj_t_prj_cn, prj_per_cn_resp, prj_t_cout_projet_cn, prj_acp_cn, prj_gpm_cn FROM projets_bis";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_projets_bis l_Objet;
			l_Objet = new clg_projets_bis(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 5)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_projets_bis l_Objet;
        if (pModele.Listeprojets_bis.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_projets_bis) pModele.Listeprojets_bis.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_projets_bis(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? -1 : Int64.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6] == "" ? -1 : Int64.Parse(l_Chaines[6]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_projets_bis l_Objet = (clg_projets_bis)pObjet;
        string l_ordreSQL = "UPDATE projets_bis SET prj_ele_cn=@prj_ele_cn, prj_t_prj_cn=@prj_t_prj_cn, prj_per_cn_resp=@prj_per_cn_resp, prj_t_cout_projet_cn=@prj_t_cout_projet_cn, prj_acp_cn=@prj_acp_cn, prj_gpm_cn=@prj_gpm_cn WHERE prj_ele_cn= @prj_ele_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet projets_bis");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_projets_bis l_Objet = (clg_projets_bis)pObjet;
        string l_ordreSQL = "INSERT INTO projets_bis (prj_ele_cn, prj_t_prj_cn, prj_per_cn_resp, prj_t_cout_projet_cn, prj_acp_cn, prj_gpm_cn) VALUES (@prj_ele_cn, @prj_t_prj_cn, @prj_per_cn_resp, @prj_t_cout_projet_cn, @prj_acp_cn, @prj_gpm_cn);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet projets_bis");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_projets_bis l_Objet = (clg_projets_bis)pObjet;
        string l_ordreSQL = "DELETE FROM projets_bis WHERE prj_ele_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet projets_bis");
    }

    private void InjecterDonnees(clg_projets_bis pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@prj_ele_cn");
		pValParams.Add(pObjet.prj_ele_cn);
		pParams.Add("@prj_t_prj_cn");
		pValParams.Add(pObjet.prj_t_prj_cn);
		pParams.Add("@prj_per_cn_resp");
		pValParams.Add(pObjet.prj_per_cn_resp);
		pParams.Add("@prj_t_cout_projet_cn");
		pValParams.Add(pObjet.prj_t_cout_projet_cn);
		pParams.Add("@prj_acp_cn");
		pValParams.Add(pObjet.prj_acp_cn);
		pParams.Add("@prj_gpm_cn");
		pValParams.Add(pObjet.prj_gpm_cn);

    }

#endregion
}
}
