namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : taux_km_reels </summary>
public class clg_DAL_taux_km_reels : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_taux_km_reels(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT tkr_cn, tkr_veh_cn, tkr_d_debut, tkr_d_fin, tkr_n_taux FROM taux_km_reels";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_taux_km_reels l_Objet;
			l_Objet = new clg_taux_km_reels(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 1)), l_rds.Donnee(i, 2) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : double.Parse(l_rds.Donnee(i, 4)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_taux_km_reels l_Objet;
        if (pModele.Listetaux_km_reels.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_taux_km_reels) pModele.Listetaux_km_reels.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_taux_km_reels(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2] == "" ? -1 : Int64.Parse(l_Chaines[2]), l_Chaines[3] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : double.Parse(l_Chaines[5]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_taux_km_reels l_Objet = (clg_taux_km_reels)pObjet;
        string l_ordreSQL = "UPDATE taux_km_reels SET tkr_cn=@tkr_cn, tkr_veh_cn=@tkr_veh_cn, tkr_d_debut=@tkr_d_debut, tkr_d_fin=@tkr_d_fin, tkr_n_taux=@tkr_n_taux WHERE tkr_cn= @tkr_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet taux_km_reels");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_taux_km_reels l_Objet = (clg_taux_km_reels)pObjet;
        string l_ordreSQL = "INSERT INTO taux_km_reels (tkr_cn, tkr_veh_cn, tkr_d_debut, tkr_d_fin, tkr_n_taux) VALUES (@tkr_cn, @tkr_veh_cn, @tkr_d_debut, @tkr_d_fin, @tkr_n_taux);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet taux_km_reels");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_taux_km_reels l_Objet = (clg_taux_km_reels)pObjet;
        string l_ordreSQL = "DELETE FROM taux_km_reels WHERE tkr_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet taux_km_reels");
    }

    private void InjecterDonnees(clg_taux_km_reels pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@tkr_cn");
		pValParams.Add(pObjet.tkr_cn);
		pParams.Add("@tkr_veh_cn");
		pValParams.Add(pObjet.vehicules != null ? pObjet.vehicules.veh_cn : (Int64?)null);
		pParams.Add("@tkr_d_debut");
		pValParams.Add(pObjet.tkr_d_debut);
		pParams.Add("@tkr_d_fin");
		pValParams.Add(pObjet.tkr_d_fin);
		pParams.Add("@tkr_n_taux");
		pValParams.Add(pObjet.tkr_n_taux);

    }

#endregion
}
}
