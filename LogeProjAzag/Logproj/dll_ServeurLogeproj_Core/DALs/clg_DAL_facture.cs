namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : facture </summary>
public class clg_DAL_facture : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_facture(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT fac_cn, fac_num, fac_ele_cn, fac_dt_generation, fac_acp_cn, fac_dt_debut, fac_dt_fin, fac_mtt_tot, fac_fin_cn, fac_commentaire, fac_mtt_tps, fac_mtt_prestation, fac_mtt_deplacement, fac_mtt_avoir, fac_n_mtt_sollicite FROM facture";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_facture l_Objet;
			l_Objet = new clg_facture(pModele, int.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(int.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 2)), l_rds.Donnee(i, 3) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 3)), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 5)), l_rds.Donnee(i, 6) == "" ? DateTime.MinValue : DateTime.Parse(l_rds.Donnee(i, 6)), l_rds.Donnee(i, 7) == "" ? -1 : double.Parse(l_rds.Donnee(i, 7)), l_rds.Donnee(i, 8) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 8)), l_rds.Donnee(i, 9), l_rds.Donnee(i, 10) == "" ? -1 : double.Parse(l_rds.Donnee(i, 10)), l_rds.Donnee(i, 11) == "" ? -1 : double.Parse(l_rds.Donnee(i, 11)), l_rds.Donnee(i, 12) == "" ? -1 : double.Parse(l_rds.Donnee(i, 12)), l_rds.Donnee(i, 13) == "" ? -1 : double.Parse(l_rds.Donnee(i, 13)), l_rds.Donnee(i, 14) == "" ? -1 : double.Parse(l_rds.Donnee(i, 14)));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_facture l_Objet;
        if (pModele.Listefacture.Dictionnaire.ContainsKey(int.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_facture) pModele.Listefacture.Dictionnaire[int.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_facture(pModele, int.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(int.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3] == "" ? -1 : Int64.Parse(l_Chaines[3]), l_Chaines[4] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[4]), l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[6]), l_Chaines[7] == "" ? DateTime.MinValue : DateTime.Parse(l_Chaines[7]), l_Chaines[8] == "" ? -1 : double.Parse(l_Chaines[8]), l_Chaines[9] == "" ? -1 : Int64.Parse(l_Chaines[9]), l_Chaines[10], l_Chaines[11] == "" ? -1 : double.Parse(l_Chaines[11]), l_Chaines[12] == "" ? -1 : double.Parse(l_Chaines[12]), l_Chaines[13] == "" ? -1 : double.Parse(l_Chaines[13]), l_Chaines[14] == "" ? -1 : double.Parse(l_Chaines[14]), l_Chaines[15] == "" ? -1 : double.Parse(l_Chaines[15]));

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_facture l_Objet = (clg_facture)pObjet;
        string l_ordreSQL = "UPDATE facture SET fac_cn=@fac_cn, fac_num=@fac_num, fac_ele_cn=@fac_ele_cn, fac_dt_generation=@fac_dt_generation, fac_acp_cn=@fac_acp_cn, fac_dt_debut=@fac_dt_debut, fac_dt_fin=@fac_dt_fin, fac_mtt_tot=@fac_mtt_tot, fac_fin_cn=@fac_fin_cn, fac_commentaire=@fac_commentaire, fac_mtt_tps=@fac_mtt_tps, fac_mtt_prestation=@fac_mtt_prestation, fac_mtt_deplacement=@fac_mtt_deplacement, fac_mtt_avoir=@fac_mtt_avoir, fac_n_mtt_sollicite=@fac_n_mtt_sollicite WHERE fac_cn= @fac_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet facture");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_facture l_Objet = (clg_facture)pObjet;
        string l_ordreSQL = "INSERT INTO facture (fac_cn, fac_num, fac_ele_cn, fac_dt_generation, fac_acp_cn, fac_dt_debut, fac_dt_fin, fac_mtt_tot, fac_fin_cn, fac_commentaire, fac_mtt_tps, fac_mtt_prestation, fac_mtt_deplacement, fac_mtt_avoir, fac_n_mtt_sollicite) VALUES (@fac_cn, @fac_num, @fac_ele_cn, @fac_dt_generation, @fac_acp_cn, @fac_dt_debut, @fac_dt_fin, @fac_mtt_tot, @fac_fin_cn, @fac_commentaire, @fac_mtt_tps, @fac_mtt_prestation, @fac_mtt_deplacement, @fac_mtt_avoir, @fac_n_mtt_sollicite);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet facture");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_facture l_Objet = (clg_facture)pObjet;
        string l_ordreSQL = "DELETE FROM facture WHERE fac_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet facture");
    }

    private void InjecterDonnees(clg_facture pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@fac_cn");
		pValParams.Add(pObjet.fac_cn);
		pParams.Add("@fac_num");
		pValParams.Add(pObjet.fac_num);
		pParams.Add("@fac_ele_cn");
		pValParams.Add(pObjet.elements != null ? pObjet.elements.ele_cn : (Int64?)null);
		pParams.Add("@fac_dt_generation");
		pValParams.Add(pObjet.fac_dt_generation);
		pParams.Add("@fac_acp_cn");
		pValParams.Add(pObjet.annee_comptable != null ? pObjet.annee_comptable.acp_cn : (Int64?)null);
		pParams.Add("@fac_dt_debut");
		pValParams.Add(pObjet.fac_dt_debut);
		pParams.Add("@fac_dt_fin");
		pValParams.Add(pObjet.fac_dt_fin);
		pParams.Add("@fac_mtt_tot");
		pValParams.Add(pObjet.fac_mtt_tot);
		pParams.Add("@fac_fin_cn");
		pValParams.Add(pObjet.fac_fin_cn);
		pParams.Add("@fac_commentaire");
		pValParams.Add(pObjet.fac_commentaire);
		pParams.Add("@fac_mtt_tps");
		pValParams.Add(pObjet.fac_mtt_tps);
		pParams.Add("@fac_mtt_prestation");
		pValParams.Add(pObjet.fac_mtt_prestation);
		pParams.Add("@fac_mtt_deplacement");
		pValParams.Add(pObjet.fac_mtt_deplacement);
		pParams.Add("@fac_mtt_avoir");
		pValParams.Add(pObjet.fac_mtt_avoir);
		pParams.Add("@fac_n_mtt_sollicite");
		pValParams.Add(pObjet.fac_n_mtt_sollicite);

    }

#endregion
}
}
