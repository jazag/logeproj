namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;

/// <summary> Classe DAL de la table : financeurs </summary>
public class clg_DAL_financeurs : clg_ControleurBase
{
    public clg_Connection c_Cnn;

    /// <summary> Constructeur de la classe </summary>
    public clg_DAL_financeurs(clg_Connection pCnn)
    {
        c_Cnn = pCnn;
    }

    /// <summary> Methode de chargement des objets depuis une base de donnees </summary>
    public void ChargeDepuisBase(clg_Modele pModele)
    {
        clg_JeuEnregistrement l_rds;
        string l_MsgErr = "";
        string l_ordreSQL = "SELECT fin_cn, fin_a_code, fin_a_nom, fin_a_lib, fin_n_archive, fin_a_adresse, fin_a_cp, fin_a_ville, fin_a_tel, fin_a_fax, fin_a_mail FROM financeurs";
        l_rds = c_Cnn.GetObjConnexion.ExecuteSELECT(l_ordreSQL, ref l_MsgErr);
        for (int i = 0; i <= l_rds.NombreLignes - 1; i++)
        {
            clg_financeurs l_Objet;
			l_Objet = new clg_financeurs(pModele, Int64.Parse(l_rds.Donnee(i, 0)));
			l_Objet.Initialise(Int64.Parse(l_rds.Donnee(i, 0)), l_rds.Donnee(i, 1), l_rds.Donnee(i, 2), l_rds.Donnee(i, 3), l_rds.Donnee(i, 4) == "" ? -1 : Int64.Parse(l_rds.Donnee(i, 4)), l_rds.Donnee(i, 5), l_rds.Donnee(i, 6), l_rds.Donnee(i, 7), l_rds.Donnee(i, 8), l_rds.Donnee(i, 9), l_rds.Donnee(i, 10));

            pModele.Ajoute(l_Objet);
        }
    }

    /// <summary> Methode de chargement des objets depuis une chaine CSV </summary>
    public static clg_ObjetBase ChargeDepuisCSV(string pChaineCSV, clg_Modele pModele)
    {
        string[] l_Chaines = pChaineCSV.Split((char)31);
        clg_financeurs l_Objet;
        if (pModele.Listefinanceurs.Dictionnaire.ContainsKey(Int64.Parse(l_Chaines[0])))
        {
            l_Objet = (clg_financeurs) pModele.Listefinanceurs.Dictionnaire[Int64.Parse(l_Chaines[0])];
            l_Objet.DetruitReferences();
        }
        else
        {
            l_Objet = new clg_financeurs(pModele, Int64.Parse(l_Chaines[0]));
        }
        l_Objet.DicReferences.Clear();
		l_Objet.Initialise(Int64.Parse(l_Chaines[1]), l_Chaines[2], l_Chaines[3], l_Chaines[4], l_Chaines[5] == "" ? -1 : Int64.Parse(l_Chaines[5]), l_Chaines[6], l_Chaines[7], l_Chaines[8], l_Chaines[9], l_Chaines[10], l_Chaines[11]);

        return l_Objet;
    }

    /// <summary> Methode de mise a jour de l'objet en base de donnees </summary>
    public override void Update(clg_ObjetBase pObjet)
    {
        clg_financeurs l_Objet = (clg_financeurs)pObjet;
        string l_ordreSQL = "UPDATE financeurs SET fin_cn=@fin_cn, fin_a_code=@fin_a_code, fin_a_nom=@fin_a_nom, fin_a_lib=@fin_a_lib, fin_n_archive=@fin_n_archive, fin_a_adresse=@fin_a_adresse, fin_a_cp=@fin_a_cp, fin_a_ville=@fin_a_ville, fin_a_tel=@fin_a_tel, fin_a_fax=@fin_a_fax, fin_a_mail=@fin_a_mail WHERE fin_cn= @fin_cn;";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la mise a jour d'un objet financeurs");
    }

    /// <summary> Methode d'insertion de l'objet en base de donnees </summary>
    public override void Insert(clg_ObjetBase pObjet)
    {
        clg_financeurs l_Objet = (clg_financeurs)pObjet;
        string l_ordreSQL = "INSERT INTO financeurs (fin_cn, fin_a_code, fin_a_nom, fin_a_lib, fin_n_archive, fin_a_adresse, fin_a_cp, fin_a_ville, fin_a_tel, fin_a_fax, fin_a_mail) VALUES (@fin_cn, @fin_a_code, @fin_a_nom, @fin_a_lib, @fin_n_archive, @fin_a_adresse, @fin_a_cp, @fin_a_ville, @fin_a_tel, @fin_a_fax, @fin_a_mail);";
        List<string> l_params = new List<string>();
        List<object> l_valParams = new List<object>();
        InjecterDonnees(l_Objet, ref l_params, ref l_valParams);
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL, l_params, l_valParams).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de l'insertion d'un objet financeurs");
    }

    /// <summary> Methode de suppression de l'objet en base de donnees </summary>
    public override void Delete(clg_ObjetBase pObjet)
    {
        clg_financeurs l_Objet = (clg_financeurs)pObjet;
        string l_ordreSQL = "DELETE FROM financeurs WHERE fin_cn=" + pObjet.ID;
        if (c_Cnn.GetObjConnexion.ExecCmdSansSync(l_ordreSQL).c_NbLignesMAJ < 1)
            throw new System.InvalidOperationException("Erreur lors de la suppression d'un objet financeurs");
    }

    private void InjecterDonnees(clg_financeurs pObjet, ref List<string>pParams, ref List<object> pValParams)
    {
		pParams.Add("@fin_cn");
		pValParams.Add(pObjet.fin_cn);
		pParams.Add("@fin_a_code");
		pValParams.Add(pObjet.fin_a_code);
		pParams.Add("@fin_a_nom");
		pValParams.Add(pObjet.fin_a_nom);
		pParams.Add("@fin_a_lib");
		pValParams.Add(pObjet.fin_a_lib);
		pParams.Add("@fin_n_archive");
		pValParams.Add(pObjet.fin_n_archive);
		pParams.Add("@fin_a_adresse");
		pValParams.Add(pObjet.fin_a_adresse);
		pParams.Add("@fin_a_cp");
		pValParams.Add(pObjet.fin_a_cp);
		pParams.Add("@fin_a_ville");
		pValParams.Add(pObjet.fin_a_ville);
		pParams.Add("@fin_a_tel");
		pValParams.Add(pObjet.fin_a_tel);
		pParams.Add("@fin_a_fax");
		pValParams.Add(pObjet.fin_a_fax);
		pParams.Add("@fin_a_mail");
		pValParams.Add(pObjet.fin_a_mail);

    }


#endregion
}
}
