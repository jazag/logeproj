﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dll_ServeurLogeproj_Core
{
    public class clg_fct_carac_element
    {
        /// <summary>
        /// Charge les paramettres puis appele la fonction d'insertion des données en base
        /// </summary>
        /// <param name="pID"></param>
        /// <param name="pLibelle"></param>
        /// <returns></returns>
        public static clg_carac_element Ajouter(Int64 pcel_cn, Int64 pelement, Int64 pcarac, string pcel_a_val, string pcel_m_val, DateTime pcel_date_modif)
        {
            clg_carac_element l_Obj = (clg_carac_element)clg_DAL_carac_element.ChargerDepuisParametre(pcel_cn, pelement, pcarac, pcel_a_val, pcel_m_val, pcel_date_modif, clg_ServeurLogeproj.Modele);
            l_Obj.CreeReferences();
            l_Obj.AjouteDansListe();
            l_Obj.InsereEnBase();
            return l_Obj;
        }


        /// <summary>
        /// Charge les paramettres puis appele la fonction de modification des données en base
        /// </summary>
        /// <param name="pID"></param>
        /// <param name="pLibelle"></param>
        /// <returns></returns>
        public static clg_carac_element Modifier(Int64 pcel_cn, Int64 pelement, Int64 pcarac, string pcel_a_val, string pcel_m_val, DateTime pcel_date_modif)
        {
            clg_carac_element l_Obj = (clg_carac_element)clg_DAL_carac_element.ChargerDepuisParametre(pcel_cn, pelement, pcarac, pcel_a_val, pcel_m_val, pcel_date_modif, clg_ServeurLogeproj.Modele);
            l_Obj.MAJEnBase();
            return l_Obj;
        }
        /// <summary>
        /// Charge les paramettres puis appele la fonction de suppression des données en base
        /// </summary>
        /// <param name="pID"></param>
        /// <param name="pLibelle"></param>
        /// <returns></returns>
        public static clg_carac_element Supprimer(Int64 pcel_cn)
        {
            //clg_carac_element l_Obj = (clg_carac_element)clg_DAL_carac_element.ChargerDepuisParametre(pcel_cn, pelement, pcarac, pcel_a_val, pcel_m_val, clg_ServeurLogeproj.Modele);
            clg_carac_element l_Obj = clg_DAL_carac_element.ChargerDepuisID(pcel_cn);
            l_Obj.SupprimeEnBase();
            l_Obj.elements.Listecarac_element.Remove(l_Obj);
            l_Obj.carac.Listecarac_element.Remove(l_Obj);
            l_Obj.SupprimeDansListe();
            l_Obj.Detruit();


            return l_Obj;
        }
    }
}
