﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dll_ServeurLogeproj_Core
{
    public class clg_TraceConsole:clg_Trace 
    {
        public override void Ajoute(string pContenu, string pOrigine, bool pErreur = false)
        {
            string l_strTypeTrace="TRACE";
            if(pErreur)
            {
                l_strTypeTrace = "ERREUR";
            }
            Console.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " | " + l_strTypeTrace + " | " + pOrigine + " | " + pContenu );
        }

        public override void Modifie(string pContenu, string pOrigine, bool pErreur = false)
        {
            string l_strTypeTrace = "TRACE";
            if (pErreur)
            {
                l_strTypeTrace = "ERREUR";
            }
            Console.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " | " + l_strTypeTrace + " | " + pOrigine + " | " + pContenu);
        }

        public override void Supprime(string pContenu, string pOrigine, bool pErreur = false)
        {
            string l_strTypeTrace = "TRACE";
            if (pErreur)
            {
                l_strTypeTrace = "ERREUR";
            }
            Console.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " | " + l_strTypeTrace + " | " + pOrigine + " | " + pContenu);
        }
    }
}
