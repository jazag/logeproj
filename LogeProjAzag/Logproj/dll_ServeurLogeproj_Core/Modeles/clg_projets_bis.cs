namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : projets_bis </summary>
public partial class clg_projets_bis : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLprojets_bis;
    protected clg_Modele c_Modele;

	private Int64 c_prj_ele_cn;
	private Int64 c_prj_t_prj_cn;
	private Int64 c_prj_per_cn_resp;
	private Int64 c_prj_t_cout_projet_cn;
	private Int64 c_prj_acp_cn;
	private Int64 c_prj_gpm_cn;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_projets_bis(clg_Modele pModele, Int64 pprj_ele_cn, bool pAMAJ = false) : base(pModele, pprj_ele_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_projets_bis(clg_Modele pModele, Int64 pprj_ele_cn, Int64 pprj_t_prj_cn, Int64 pprj_per_cn_resp, Int64 pprj_t_cout_projet_cn, Int64 pprj_acp_cn, Int64 pprj_gpm_cn, bool pAMAJ = true) : base(pModele, pprj_ele_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pprj_ele_cn != Int64.MinValue)
            c_prj_ele_cn = pprj_ele_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_prj_t_prj_cn = pprj_t_prj_cn;
		        c_prj_per_cn_resp = pprj_per_cn_resp;
		        if(pprj_t_cout_projet_cn != Int64.MinValue)
            c_prj_t_cout_projet_cn = pprj_t_cout_projet_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_prj_acp_cn = pprj_acp_cn;
		        c_prj_gpm_cn = pprj_gpm_cn;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pprj_ele_cn, Int64 pprj_t_prj_cn, Int64 pprj_per_cn_resp, Int64 pprj_t_cout_projet_cn, Int64 pprj_acp_cn, Int64 pprj_gpm_cn)
    {   
		        c_prj_ele_cn = pprj_ele_cn;
		        c_prj_t_prj_cn = pprj_t_prj_cn;
		        c_prj_per_cn_resp = pprj_per_cn_resp;
		        c_prj_t_cout_projet_cn = pprj_t_cout_projet_cn;
		        c_prj_acp_cn = pprj_acp_cn;
		        c_prj_gpm_cn = pprj_gpm_cn;

        base.Initialise(pprj_ele_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLprojets_bis; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeprojets_bis.Dictionnaire.Add(prj_ele_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeprojets_bis.Dictionnaire.Remove(prj_ele_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_projets_bis l_Clone = (clg_projets_bis) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_projets_bis(null, prj_ele_cn, prj_t_prj_cn, prj_per_cn_resp, prj_t_cout_projet_cn, prj_acp_cn, prj_gpm_cn,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_projets_bis l_clone = (clg_projets_bis) this.Clone;
		c_prj_ele_cn = l_clone.prj_ele_cn;
		c_prj_t_prj_cn = l_clone.prj_t_prj_cn;
		c_prj_per_cn_resp = l_clone.prj_per_cn_resp;
		c_prj_t_cout_projet_cn = l_clone.prj_t_cout_projet_cn;
		c_prj_acp_cn = l_clone.prj_acp_cn;
		c_prj_gpm_cn = l_clone.prj_gpm_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLprojets_bis.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLprojets_bis.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLprojets_bis.Delete(this);
    }

    /* Accesseur de la propriete prj_ele_cn (prj_ele_cn)
    * @return c_prj_ele_cn */
    public Int64 prj_ele_cn
    {
        get{return c_prj_ele_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_prj_ele_cn != value)
                {
                    CreerClone();
                    c_prj_ele_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete prj_t_prj_cn (prj_t_prj_cn)
    * @return c_prj_t_prj_cn */
    public Int64 prj_t_prj_cn
    {
        get{return c_prj_t_prj_cn;}
        set
        {
            if(c_prj_t_prj_cn != value)
            {
                CreerClone();
                c_prj_t_prj_cn = value;
            }
        }
    }
    /* Accesseur de la propriete prj_per_cn_resp (prj_per_cn_resp)
    * @return c_prj_per_cn_resp */
    public Int64 prj_per_cn_resp
    {
        get{return c_prj_per_cn_resp;}
        set
        {
            if(c_prj_per_cn_resp != value)
            {
                CreerClone();
                c_prj_per_cn_resp = value;
            }
        }
    }
    /* Accesseur de la propriete prj_t_cout_projet_cn (prj_t_cout_projet_cn)
    * @return c_prj_t_cout_projet_cn */
    public Int64 prj_t_cout_projet_cn
    {
        get{return c_prj_t_cout_projet_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_prj_t_cout_projet_cn != value)
                {
                    CreerClone();
                    c_prj_t_cout_projet_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete prj_acp_cn (prj_acp_cn)
    * @return c_prj_acp_cn */
    public Int64 prj_acp_cn
    {
        get{return c_prj_acp_cn;}
        set
        {
            if(c_prj_acp_cn != value)
            {
                CreerClone();
                c_prj_acp_cn = value;
            }
        }
    }
    /* Accesseur de la propriete prj_gpm_cn (prj_gpm_cn)
    * @return c_prj_gpm_cn */
    public Int64 prj_gpm_cn
    {
        get{return c_prj_gpm_cn;}
        set
        {
            if(c_prj_gpm_cn != value)
            {
                CreerClone();
                c_prj_gpm_cn = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_prj_ele_cn.ToString());
		l_Valeurs.Add(c_prj_t_prj_cn.ToString());
		l_Valeurs.Add(c_prj_per_cn_resp.ToString());
		l_Valeurs.Add(c_prj_t_cout_projet_cn.ToString());
		l_Valeurs.Add(c_prj_acp_cn.ToString());
		l_Valeurs.Add(c_prj_gpm_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("prj_ele_cn");
		l_Noms.Add("prj_t_prj_cn");
		l_Noms.Add("prj_per_cn_resp");
		l_Noms.Add("prj_t_cout_projet_cn");
		l_Noms.Add("prj_acp_cn");
		l_Noms.Add("prj_gpm_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
