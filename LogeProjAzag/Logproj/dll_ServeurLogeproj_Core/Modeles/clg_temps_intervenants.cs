namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : temps_intervenants </summary>
public partial class clg_temps_intervenants : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtemps_intervenants;
    protected clg_Modele c_Modele;

	private Int64 c_tit_cn;
	private clg_personnels c_personnels;
	private double c_tit_n_heures;
	private DateTime c_tit_d_debut;
	private DateTime c_tit_d_fin;
	private clg_operations c_operations;
	private Int64 c_tit_n_affiche;
	private double c_tit_n_heures_sync;
	private Int64 c_tit_tx_cn;
	private double c_tit_n_heures_visible;
	private bool c_tit_b_plan_charge;
	private double c_tit_n_heures_plan_charge;
	private List<clg_report> c_Listereport;
	private List<clg_taux_fin> c_Listetaux_fin;
	private List<clg_temps_realises> c_Listetemps_realises;
	private List<clg_ventilation_deplacement_csa> c_Listeventilation_deplacement_csa;


    private void Init()
    {
		c_Listereport = new List<clg_report>();
		c_Listetaux_fin = new List<clg_taux_fin>();
		c_Listetemps_realises = new List<clg_temps_realises>();
		c_Listeventilation_deplacement_csa = new List<clg_ventilation_deplacement_csa>();

    }

    public override void Detruit()
    {
		c_Listereport.Clear();
		c_Listetaux_fin.Clear();
		c_Listetemps_realises.Clear();
		c_Listeventilation_deplacement_csa.Clear();
		this.c_personnels = null;
		this.c_operations = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_temps_intervenants(clg_Modele pModele, Int64 ptit_cn, bool pAMAJ = false) : base(pModele, ptit_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_temps_intervenants(clg_Modele pModele, Int64 ptit_cn, clg_personnels ppersonnels, double ptit_n_heures, DateTime ptit_d_debut, DateTime ptit_d_fin, clg_operations poperations, Int64 ptit_n_affiche, double ptit_n_heures_sync, Int64 ptit_tx_cn, double ptit_n_heures_visible, bool ptit_b_plan_charge, double ptit_n_heures_plan_charge, bool pAMAJ = true) : base(pModele, ptit_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptit_cn != Int64.MinValue)
            c_tit_cn = ptit_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_personnels = ppersonnels;
		        c_tit_n_heures = ptit_n_heures;
		        c_tit_d_debut = ptit_d_debut;
		        c_tit_d_fin = ptit_d_fin;
		        c_operations = poperations;
		        c_tit_n_affiche = ptit_n_affiche;
		        c_tit_n_heures_sync = ptit_n_heures_sync;
		        c_tit_tx_cn = ptit_tx_cn;
		        c_tit_n_heures_visible = ptit_n_heures_visible;
		        c_tit_b_plan_charge = ptit_b_plan_charge;
		        c_tit_n_heures_plan_charge = ptit_n_heures_plan_charge;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptit_cn, Int64 ppersonnels, double ptit_n_heures, DateTime ptit_d_debut, DateTime ptit_d_fin, Int64 poperations, Int64 ptit_n_affiche, double ptit_n_heures_sync, Int64 ptit_tx_cn, double ptit_n_heures_visible, bool ptit_b_plan_charge, double ptit_n_heures_plan_charge)
    {   
		        c_tit_cn = ptit_cn;
		c_dicReferences.Add("personnels", ppersonnels);
		        c_tit_n_heures = ptit_n_heures;
		        c_tit_d_debut = ptit_d_debut;
		        c_tit_d_fin = ptit_d_fin;
		c_dicReferences.Add("operations", poperations);
		        c_tit_n_affiche = ptit_n_affiche;
		        c_tit_n_heures_sync = ptit_n_heures_sync;
		        c_tit_tx_cn = ptit_tx_cn;
		        c_tit_n_heures_visible = ptit_n_heures_visible;
		        c_tit_b_plan_charge = ptit_b_plan_charge;
		        c_tit_n_heures_plan_charge = ptit_n_heures_plan_charge;

        base.Initialise(ptit_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtemps_intervenants; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_operations = (clg_operations) c_ModeleBase.RenvoieObjet(c_dicReferences["operations"], "clg_operations");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetemps_intervenants.Dictionnaire.Add(tit_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetemps_intervenants.Dictionnaire.Remove(tit_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listetemps_intervenants.Contains(this)) c_personnels.Listetemps_intervenants.Add(this);
		if(c_operations != null)if(!c_operations.Listetemps_intervenants.Contains(this)) c_operations.Listetemps_intervenants.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_temps_intervenants l_Clone = (clg_temps_intervenants) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listetemps_intervenants.Contains(this)) l_Clone.personnels.Listetemps_intervenants.Remove(this);
		if(l_Clone.operations != null)if(l_Clone.operations.Listetemps_intervenants.Contains(this)) l_Clone.operations.Listetemps_intervenants.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listereport.Count > 0) c_EstReference = true;
		if(c_Listetaux_fin.Count > 0) c_EstReference = true;
		if(c_Listetemps_realises.Count > 0) c_EstReference = true;
		if(c_Listeventilation_deplacement_csa.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_temps_intervenants(null, tit_cn, personnels, tit_n_heures, tit_d_debut, tit_d_fin, operations, tit_n_affiche, tit_n_heures_sync, tit_tx_cn, tit_n_heures_visible, tit_b_plan_charge, tit_n_heures_plan_charge,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_temps_intervenants l_clone = (clg_temps_intervenants) this.Clone;
		c_tit_cn = l_clone.tit_cn;
		c_personnels = l_clone.personnels;
		c_tit_n_heures = l_clone.tit_n_heures;
		c_tit_d_debut = l_clone.tit_d_debut;
		c_tit_d_fin = l_clone.tit_d_fin;
		c_operations = l_clone.operations;
		c_tit_n_affiche = l_clone.tit_n_affiche;
		c_tit_n_heures_sync = l_clone.tit_n_heures_sync;
		c_tit_tx_cn = l_clone.tit_tx_cn;
		c_tit_n_heures_visible = l_clone.tit_n_heures_visible;
		c_tit_b_plan_charge = l_clone.tit_b_plan_charge;
		c_tit_n_heures_plan_charge = l_clone.tit_n_heures_plan_charge;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtemps_intervenants.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtemps_intervenants.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtemps_intervenants.Delete(this);
    }

    /* Accesseur de la propriete tit_cn (tit_cn)
    * @return c_tit_cn */
    public Int64 tit_cn
    {
        get{return c_tit_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tit_cn != value)
                {
                    CreerClone();
                    c_tit_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (tit_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete tit_n_heures (tit_n_heures)
    * @return c_tit_n_heures */
    public double tit_n_heures
    {
        get{return c_tit_n_heures;}
        set
        {
            if(c_tit_n_heures != value)
            {
                CreerClone();
                c_tit_n_heures = value;
            }
        }
    }
    /* Accesseur de la propriete tit_d_debut (tit_d_debut)
    * @return c_tit_d_debut */
    public DateTime tit_d_debut
    {
        get{return c_tit_d_debut;}
        set
        {
            if(c_tit_d_debut != value)
            {
                CreerClone();
                c_tit_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete tit_d_fin (tit_d_fin)
    * @return c_tit_d_fin */
    public DateTime tit_d_fin
    {
        get{return c_tit_d_fin;}
        set
        {
            if(c_tit_d_fin != value)
            {
                CreerClone();
                c_tit_d_fin = value;
            }
        }
    }
    /* Accesseur de la propriete operations (tit_ele_cn)
    * @return c_operations */
    public clg_operations operations
    {
        get{return c_operations;}
        set
        {
            if(c_operations != value)
            {
                CreerClone();
                c_operations = value;
            }
        }
    }
    /* Accesseur de la propriete tit_n_affiche (tit_n_affiche)
    * @return c_tit_n_affiche */
    public Int64 tit_n_affiche
    {
        get{return c_tit_n_affiche;}
        set
        {
            if(c_tit_n_affiche != value)
            {
                CreerClone();
                c_tit_n_affiche = value;
            }
        }
    }
    /* Accesseur de la propriete tit_n_heures_sync (tit_n_heures_sync)
    * @return c_tit_n_heures_sync */
    public double tit_n_heures_sync
    {
        get{return c_tit_n_heures_sync;}
        set
        {
            if(c_tit_n_heures_sync != value)
            {
                CreerClone();
                c_tit_n_heures_sync = value;
            }
        }
    }
    /* Accesseur de la propriete tit_tx_cn (tit_tx_cn)
    * @return c_tit_tx_cn */
    public Int64 tit_tx_cn
    {
        get{return c_tit_tx_cn;}
        set
        {
            if(c_tit_tx_cn != value)
            {
                CreerClone();
                c_tit_tx_cn = value;
            }
        }
    }
    /* Accesseur de la propriete tit_n_heures_visible (tit_n_heures_visible)
    * @return c_tit_n_heures_visible */
    public double tit_n_heures_visible
    {
        get{return c_tit_n_heures_visible;}
        set
        {
            if(c_tit_n_heures_visible != value)
            {
                CreerClone();
                c_tit_n_heures_visible = value;
            }
        }
    }
    /* Accesseur de la propriete tit_b_plan_charge (tit_b_plan_charge)
    * @return c_tit_b_plan_charge */
    public bool tit_b_plan_charge
    {
        get{return c_tit_b_plan_charge;}
        set
        {
            if(c_tit_b_plan_charge != value)
            {
                CreerClone();
                c_tit_b_plan_charge = value;
            }
        }
    }
    /* Accesseur de la propriete tit_n_heures_plan_charge (tit_n_heures_plan_charge)
    * @return c_tit_n_heures_plan_charge */
    public double tit_n_heures_plan_charge
    {
        get{return c_tit_n_heures_plan_charge;}
        set
        {
            if(c_tit_n_heures_plan_charge != value)
            {
                CreerClone();
                c_tit_n_heures_plan_charge = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type report */
    public List<clg_report> Listereport
    {
        get { return c_Listereport; }
    }    /* Accesseur de la liste des objets de type taux_fin */
    public List<clg_taux_fin> Listetaux_fin
    {
        get { return c_Listetaux_fin; }
    }    /* Accesseur de la liste des objets de type temps_realises */
    public List<clg_temps_realises> Listetemps_realises
    {
        get { return c_Listetemps_realises; }
    }    /* Accesseur de la liste des objets de type ventilation_deplacement_csa */
    public List<clg_ventilation_deplacement_csa> Listeventilation_deplacement_csa
    {
        get { return c_Listeventilation_deplacement_csa; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tit_cn.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_tit_n_heures.ToString());
		l_Valeurs.Add(c_tit_d_debut.ToString());
		l_Valeurs.Add(c_tit_d_fin.ToString());
		l_Valeurs.Add(c_operations==null ? "-1" : c_operations.ID.ToString());
		l_Valeurs.Add(c_tit_n_affiche.ToString());
		l_Valeurs.Add(c_tit_n_heures_sync.ToString());
		l_Valeurs.Add(c_tit_tx_cn.ToString());
		l_Valeurs.Add(c_tit_n_heures_visible.ToString());
		l_Valeurs.Add(c_tit_b_plan_charge.ToString());
		l_Valeurs.Add(c_tit_n_heures_plan_charge.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tit_cn");
		l_Noms.Add("personnels");
		l_Noms.Add("tit_n_heures");
		l_Noms.Add("tit_d_debut");
		l_Noms.Add("tit_d_fin");
		l_Noms.Add("operations");
		l_Noms.Add("tit_n_affiche");
		l_Noms.Add("tit_n_heures_sync");
		l_Noms.Add("tit_tx_cn");
		l_Noms.Add("tit_n_heures_visible");
		l_Noms.Add("tit_b_plan_charge");
		l_Noms.Add("tit_n_heures_plan_charge");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
