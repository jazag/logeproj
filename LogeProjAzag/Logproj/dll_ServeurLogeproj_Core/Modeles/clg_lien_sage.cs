namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : lien_sage </summary>
public partial class clg_lien_sage : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLlien_sage;
    protected clg_Modele c_Modele;

	private Int64 c_lsg_cn;
	private Int64 c_lsg_n_ecno;
	private double c_lsg_n_mtt;
	private string c_lsg_a_lib;
	private string c_lsg_a_ncompte;
	private DateTime c_lsg_d_date;
	private string c_lsg_a_code;
	private clg_elements c_elements;
	private Int64 c_lsg_jou_cn;
	private Int64 c_lsg_n_lig;
	private Int64 c_lsg_n_t_multi;
	private Int64 c_lsg_n_lien_multi;
	private clg_t_lien_sage c_t_lien_sage;
	private clg_basemae c_basemae;
	private Int64 c_lsg_ctf_cn;
	private clg_arretes c_arretes;
	private clg_fournisseurs c_fournisseurs;
	private string c_lsg_a_sens;
	private DateTime c_lsg_d_sage;
	private Int64 c_lsg_n_annee;
	private Int64 c_lsg_sag_cn;
	private clg_journal c_journal;
	private Int64 c_lsg_lien_lsg_cn;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_elements = null;
		this.c_t_lien_sage = null;
		this.c_basemae = null;
		this.c_arretes = null;
		this.c_fournisseurs = null;
		this.c_journal = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_lien_sage(clg_Modele pModele, Int64 plsg_cn, bool pAMAJ = false) : base(pModele, plsg_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_lien_sage(clg_Modele pModele, Int64 plsg_cn, Int64 plsg_n_ecno, double plsg_n_mtt, string plsg_a_lib, string plsg_a_ncompte, DateTime plsg_d_date, string plsg_a_code, clg_elements pelements, Int64 plsg_jou_cn, Int64 plsg_n_lig, Int64 plsg_n_t_multi, Int64 plsg_n_lien_multi, clg_t_lien_sage pt_lien_sage, clg_basemae pbasemae, Int64 plsg_ctf_cn, clg_arretes parretes, clg_fournisseurs pfournisseurs, string plsg_a_sens, DateTime plsg_d_sage, Int64 plsg_n_annee, Int64 plsg_sag_cn, clg_journal pjournal, Int64 plsg_lien_lsg_cn, bool pAMAJ = true) : base(pModele, plsg_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(plsg_cn != Int64.MinValue)
            c_lsg_cn = plsg_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_lsg_n_ecno = plsg_n_ecno;
		        c_lsg_n_mtt = plsg_n_mtt;
		        c_lsg_a_lib = plsg_a_lib;
		        c_lsg_a_ncompte = plsg_a_ncompte;
		        c_lsg_d_date = plsg_d_date;
		        c_lsg_a_code = plsg_a_code;
		        c_elements = pelements;
		        c_lsg_jou_cn = plsg_jou_cn;
		        c_lsg_n_lig = plsg_n_lig;
		        c_lsg_n_t_multi = plsg_n_t_multi;
		        c_lsg_n_lien_multi = plsg_n_lien_multi;
		        c_t_lien_sage = pt_lien_sage;
		        c_basemae = pbasemae;
		        c_lsg_ctf_cn = plsg_ctf_cn;
		        c_arretes = parretes;
		        c_fournisseurs = pfournisseurs;
		        c_lsg_a_sens = plsg_a_sens;
		        c_lsg_d_sage = plsg_d_sage;
		        c_lsg_n_annee = plsg_n_annee;
		        c_lsg_sag_cn = plsg_sag_cn;
		        c_journal = pjournal;
		        c_lsg_lien_lsg_cn = plsg_lien_lsg_cn;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 plsg_cn, Int64 plsg_n_ecno, double plsg_n_mtt, string plsg_a_lib, string plsg_a_ncompte, DateTime plsg_d_date, string plsg_a_code, Int64 pelements, Int64 plsg_jou_cn, Int64 plsg_n_lig, Int64 plsg_n_t_multi, Int64 plsg_n_lien_multi, Int64 pt_lien_sage, Int64 pbasemae, Int64 plsg_ctf_cn, Int64 parretes, Int64 pfournisseurs, string plsg_a_sens, DateTime plsg_d_sage, Int64 plsg_n_annee, Int64 plsg_sag_cn, string pjournal, Int64 plsg_lien_lsg_cn)
    {   
		        c_lsg_cn = plsg_cn;
		        c_lsg_n_ecno = plsg_n_ecno;
		        c_lsg_n_mtt = plsg_n_mtt;
		        c_lsg_a_lib = plsg_a_lib;
		        c_lsg_a_ncompte = plsg_a_ncompte;
		        c_lsg_d_date = plsg_d_date;
		        c_lsg_a_code = plsg_a_code;
		c_dicReferences.Add("elements", pelements);
		        c_lsg_jou_cn = plsg_jou_cn;
		        c_lsg_n_lig = plsg_n_lig;
		        c_lsg_n_t_multi = plsg_n_t_multi;
		        c_lsg_n_lien_multi = plsg_n_lien_multi;
		c_dicReferences.Add("t_lien_sage", pt_lien_sage);
		c_dicReferences.Add("basemae", pbasemae);
		        c_lsg_ctf_cn = plsg_ctf_cn;
		c_dicReferences.Add("arretes", parretes);
		c_dicReferences.Add("fournisseurs", pfournisseurs);
		        c_lsg_a_sens = plsg_a_sens;
		        c_lsg_d_sage = plsg_d_sage;
		        c_lsg_n_annee = plsg_n_annee;
		        c_lsg_sag_cn = plsg_sag_cn;
		c_dicReferences.Add("journal", pjournal);
		        c_lsg_lien_lsg_cn = plsg_lien_lsg_cn;

        base.Initialise(plsg_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLlien_sage; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_elements = (clg_elements) c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");
		c_t_lien_sage = (clg_t_lien_sage) c_ModeleBase.RenvoieObjet(c_dicReferences["t_lien_sage"], "clg_t_lien_sage");
		c_basemae = (clg_basemae) c_ModeleBase.RenvoieObjet(c_dicReferences["basemae"], "clg_basemae");
		c_arretes = (clg_arretes) c_ModeleBase.RenvoieObjet(c_dicReferences["arretes"], "clg_arretes");
		c_fournisseurs = (clg_fournisseurs) c_ModeleBase.RenvoieObjet(c_dicReferences["fournisseurs"], "clg_fournisseurs");
		c_journal = (clg_journal) c_ModeleBase.RenvoieObjet(c_dicReferences["journal"], "clg_journal");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listelien_sage.Dictionnaire.Add(lsg_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listelien_sage.Dictionnaire.Remove(lsg_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_elements != null)if(!c_elements.Listelien_sage.Contains(this)) c_elements.Listelien_sage.Add(this);
		if(c_t_lien_sage != null)if(!c_t_lien_sage.Listelien_sage.Contains(this)) c_t_lien_sage.Listelien_sage.Add(this);
		if(c_basemae != null)if(!c_basemae.Listelien_sage.Contains(this)) c_basemae.Listelien_sage.Add(this);
		if(c_arretes != null)if(!c_arretes.Listelien_sage.Contains(this)) c_arretes.Listelien_sage.Add(this);
		if(c_fournisseurs != null)if(!c_fournisseurs.Listelien_sage.Contains(this)) c_fournisseurs.Listelien_sage.Add(this);
		if(c_journal != null)if(!c_journal.Listelien_sage.Contains(this)) c_journal.Listelien_sage.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_lien_sage l_Clone = (clg_lien_sage) Clone;
		if(l_Clone.elements != null)if(l_Clone.elements.Listelien_sage.Contains(this)) l_Clone.elements.Listelien_sage.Remove(this);
		if(l_Clone.t_lien_sage != null)if(l_Clone.t_lien_sage.Listelien_sage.Contains(this)) l_Clone.t_lien_sage.Listelien_sage.Remove(this);
		if(l_Clone.basemae != null)if(l_Clone.basemae.Listelien_sage.Contains(this)) l_Clone.basemae.Listelien_sage.Remove(this);
		if(l_Clone.arretes != null)if(l_Clone.arretes.Listelien_sage.Contains(this)) l_Clone.arretes.Listelien_sage.Remove(this);
		if(l_Clone.fournisseurs != null)if(l_Clone.fournisseurs.Listelien_sage.Contains(this)) l_Clone.fournisseurs.Listelien_sage.Remove(this);
		if(l_Clone.journal != null)if(l_Clone.journal.Listelien_sage.Contains(this)) l_Clone.journal.Listelien_sage.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_lien_sage(null, lsg_cn, lsg_n_ecno, lsg_n_mtt, lsg_a_lib, lsg_a_ncompte, lsg_d_date, lsg_a_code, elements, lsg_jou_cn, lsg_n_lig, lsg_n_t_multi, lsg_n_lien_multi, t_lien_sage, basemae, lsg_ctf_cn, arretes, fournisseurs, lsg_a_sens, lsg_d_sage, lsg_n_annee, lsg_sag_cn, journal, lsg_lien_lsg_cn,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_lien_sage l_clone = (clg_lien_sage) this.Clone;
		c_lsg_cn = l_clone.lsg_cn;
		c_lsg_n_ecno = l_clone.lsg_n_ecno;
		c_lsg_n_mtt = l_clone.lsg_n_mtt;
		c_lsg_a_lib = l_clone.lsg_a_lib;
		c_lsg_a_ncompte = l_clone.lsg_a_ncompte;
		c_lsg_d_date = l_clone.lsg_d_date;
		c_lsg_a_code = l_clone.lsg_a_code;
		c_elements = l_clone.elements;
		c_lsg_jou_cn = l_clone.lsg_jou_cn;
		c_lsg_n_lig = l_clone.lsg_n_lig;
		c_lsg_n_t_multi = l_clone.lsg_n_t_multi;
		c_lsg_n_lien_multi = l_clone.lsg_n_lien_multi;
		c_t_lien_sage = l_clone.t_lien_sage;
		c_basemae = l_clone.basemae;
		c_lsg_ctf_cn = l_clone.lsg_ctf_cn;
		c_arretes = l_clone.arretes;
		c_fournisseurs = l_clone.fournisseurs;
		c_lsg_a_sens = l_clone.lsg_a_sens;
		c_lsg_d_sage = l_clone.lsg_d_sage;
		c_lsg_n_annee = l_clone.lsg_n_annee;
		c_lsg_sag_cn = l_clone.lsg_sag_cn;
		c_journal = l_clone.journal;
		c_lsg_lien_lsg_cn = l_clone.lsg_lien_lsg_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLlien_sage.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLlien_sage.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLlien_sage.Delete(this);
    }

    /* Accesseur de la propriete lsg_cn (lsg_cn)
    * @return c_lsg_cn */
    public Int64 lsg_cn
    {
        get{return c_lsg_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_lsg_cn != value)
                {
                    CreerClone();
                    c_lsg_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete lsg_n_ecno (lsg_n_ecno)
    * @return c_lsg_n_ecno */
    public Int64 lsg_n_ecno
    {
        get{return c_lsg_n_ecno;}
        set
        {
            if(c_lsg_n_ecno != value)
            {
                CreerClone();
                c_lsg_n_ecno = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_n_mtt (lsg_n_mtt)
    * @return c_lsg_n_mtt */
    public double lsg_n_mtt
    {
        get{return c_lsg_n_mtt;}
        set
        {
            if(c_lsg_n_mtt != value)
            {
                CreerClone();
                c_lsg_n_mtt = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_a_lib (lsg_a_lib)
    * @return c_lsg_a_lib */
    public string lsg_a_lib
    {
        get{return c_lsg_a_lib;}
        set
        {
            if(c_lsg_a_lib != value)
            {
                CreerClone();
                c_lsg_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_a_ncompte (lsg_a_ncompte)
    * @return c_lsg_a_ncompte */
    public string lsg_a_ncompte
    {
        get{return c_lsg_a_ncompte;}
        set
        {
            if(c_lsg_a_ncompte != value)
            {
                CreerClone();
                c_lsg_a_ncompte = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_d_date (lsg_d_date)
    * @return c_lsg_d_date */
    public DateTime lsg_d_date
    {
        get{return c_lsg_d_date;}
        set
        {
            if(c_lsg_d_date != value)
            {
                CreerClone();
                c_lsg_d_date = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_a_code (lsg_a_code)
    * @return c_lsg_a_code */
    public string lsg_a_code
    {
        get{return c_lsg_a_code;}
        set
        {
            if(c_lsg_a_code != value)
            {
                CreerClone();
                c_lsg_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete elements (lsg_ele_cn)
    * @return c_elements */
    public clg_elements elements
    {
        get{return c_elements;}
        set
        {
            if(c_elements != value)
            {
                CreerClone();
                c_elements = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_jou_cn (lsg_jou_cn)
    * @return c_lsg_jou_cn */
    public Int64 lsg_jou_cn
    {
        get{return c_lsg_jou_cn;}
        set
        {
            if(c_lsg_jou_cn != value)
            {
                CreerClone();
                c_lsg_jou_cn = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_n_lig (lsg_n_lig)
    * @return c_lsg_n_lig */
    public Int64 lsg_n_lig
    {
        get{return c_lsg_n_lig;}
        set
        {
            if(c_lsg_n_lig != value)
            {
                CreerClone();
                c_lsg_n_lig = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_n_t_multi (lsg_n_t_multi)
    * @return c_lsg_n_t_multi */
    public Int64 lsg_n_t_multi
    {
        get{return c_lsg_n_t_multi;}
        set
        {
            if(c_lsg_n_t_multi != value)
            {
                CreerClone();
                c_lsg_n_t_multi = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_n_lien_multi (lsg_n_lien_multi)
    * @return c_lsg_n_lien_multi */
    public Int64 lsg_n_lien_multi
    {
        get{return c_lsg_n_lien_multi;}
        set
        {
            if(c_lsg_n_lien_multi != value)
            {
                CreerClone();
                c_lsg_n_lien_multi = value;
            }
        }
    }
    /* Accesseur de la propriete t_lien_sage (lsg_tls_cn)
    * @return c_t_lien_sage */
    public clg_t_lien_sage t_lien_sage
    {
        get{return c_t_lien_sage;}
        set
        {
            if(c_t_lien_sage != value)
            {
                CreerClone();
                c_t_lien_sage = value;
            }
        }
    }
    /* Accesseur de la propriete basemae (lsg_base_cn)
    * @return c_basemae */
    public clg_basemae basemae
    {
        get{return c_basemae;}
        set
        {
            if(c_basemae != value)
            {
                CreerClone();
                c_basemae = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_ctf_cn (lsg_ctf_cn)
    * @return c_lsg_ctf_cn */
    public Int64 lsg_ctf_cn
    {
        get{return c_lsg_ctf_cn;}
        set
        {
            if(c_lsg_ctf_cn != value)
            {
                CreerClone();
                c_lsg_ctf_cn = value;
            }
        }
    }
    /* Accesseur de la propriete arretes (lsg_ara_cn)
    * @return c_arretes */
    public clg_arretes arretes
    {
        get{return c_arretes;}
        set
        {
            if(c_arretes != value)
            {
                CreerClone();
                c_arretes = value;
            }
        }
    }
    /* Accesseur de la propriete fournisseurs (lsg_four_cn)
    * @return c_fournisseurs */
    public clg_fournisseurs fournisseurs
    {
        get{return c_fournisseurs;}
        set
        {
            if(c_fournisseurs != value)
            {
                CreerClone();
                c_fournisseurs = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_a_sens (lsg_a_sens)
    * @return c_lsg_a_sens */
    public string lsg_a_sens
    {
        get{return c_lsg_a_sens;}
        set
        {
            if(c_lsg_a_sens != value)
            {
                CreerClone();
                c_lsg_a_sens = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_d_sage (lsg_d_sage)
    * @return c_lsg_d_sage */
    public DateTime lsg_d_sage
    {
        get{return c_lsg_d_sage;}
        set
        {
            if(c_lsg_d_sage != value)
            {
                CreerClone();
                c_lsg_d_sage = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_n_annee (lsg_n_annee)
    * @return c_lsg_n_annee */
    public Int64 lsg_n_annee
    {
        get{return c_lsg_n_annee;}
        set
        {
            if(c_lsg_n_annee != value)
            {
                CreerClone();
                c_lsg_n_annee = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_sag_cn (lsg_sag_cn)
    * @return c_lsg_sag_cn */
    public Int64 lsg_sag_cn
    {
        get{return c_lsg_sag_cn;}
        set
        {
            if(c_lsg_sag_cn != value)
            {
                CreerClone();
                c_lsg_sag_cn = value;
            }
        }
    }
    /* Accesseur de la propriete journal (lsg_jou_a_code)
    * @return c_journal */
    public clg_journal journal
    {
        get{return c_journal;}
        set
        {
            if(c_journal != value)
            {
                CreerClone();
                c_journal = value;
            }
        }
    }
    /* Accesseur de la propriete lsg_lien_lsg_cn (lsg_lien_lsg_cn)
    * @return c_lsg_lien_lsg_cn */
    public Int64 lsg_lien_lsg_cn
    {
        get{return c_lsg_lien_lsg_cn;}
        set
        {
            if(c_lsg_lien_lsg_cn != value)
            {
                CreerClone();
                c_lsg_lien_lsg_cn = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_lsg_cn.ToString());
		l_Valeurs.Add(c_lsg_n_ecno.ToString());
		l_Valeurs.Add(c_lsg_n_mtt.ToString());
		l_Valeurs.Add(c_lsg_a_lib.ToString());
		l_Valeurs.Add(c_lsg_a_ncompte.ToString());
		l_Valeurs.Add(c_lsg_d_date.ToString());
		l_Valeurs.Add(c_lsg_a_code.ToString());
		l_Valeurs.Add(c_elements==null ? "-1" : c_elements.ID.ToString());
		l_Valeurs.Add(c_lsg_jou_cn.ToString());
		l_Valeurs.Add(c_lsg_n_lig.ToString());
		l_Valeurs.Add(c_lsg_n_t_multi.ToString());
		l_Valeurs.Add(c_lsg_n_lien_multi.ToString());
		l_Valeurs.Add(c_t_lien_sage==null ? "-1" : c_t_lien_sage.ID.ToString());
		l_Valeurs.Add(c_basemae==null ? "-1" : c_basemae.ID.ToString());
		l_Valeurs.Add(c_lsg_ctf_cn.ToString());
		l_Valeurs.Add(c_arretes==null ? "-1" : c_arretes.ID.ToString());
		l_Valeurs.Add(c_fournisseurs==null ? "-1" : c_fournisseurs.ID.ToString());
		l_Valeurs.Add(c_lsg_a_sens.ToString());
		l_Valeurs.Add(c_lsg_d_sage.ToString());
		l_Valeurs.Add(c_lsg_n_annee.ToString());
		l_Valeurs.Add(c_lsg_sag_cn.ToString());
		l_Valeurs.Add(c_journal==null ? "-1" : c_journal.ID.ToString());
		l_Valeurs.Add(c_lsg_lien_lsg_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("lsg_cn");
		l_Noms.Add("lsg_n_ecno");
		l_Noms.Add("lsg_n_mtt");
		l_Noms.Add("lsg_a_lib");
		l_Noms.Add("lsg_a_ncompte");
		l_Noms.Add("lsg_d_date");
		l_Noms.Add("lsg_a_code");
		l_Noms.Add("elements");
		l_Noms.Add("lsg_jou_cn");
		l_Noms.Add("lsg_n_lig");
		l_Noms.Add("lsg_n_t_multi");
		l_Noms.Add("lsg_n_lien_multi");
		l_Noms.Add("t_lien_sage");
		l_Noms.Add("basemae");
		l_Noms.Add("lsg_ctf_cn");
		l_Noms.Add("arretes");
		l_Noms.Add("fournisseurs");
		l_Noms.Add("lsg_a_sens");
		l_Noms.Add("lsg_d_sage");
		l_Noms.Add("lsg_n_annee");
		l_Noms.Add("lsg_sag_cn");
		l_Noms.Add("journal");
		l_Noms.Add("lsg_lien_lsg_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
