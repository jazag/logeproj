﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dll_ServeurLogeproj_Core
{
    public partial class clg_valeur_carac
    {
        public string ToJson()
        {
            StringBuilder l_sb = new StringBuilder();
            StringWriter l_sw = new StringWriter(l_sb);

            using (JsonWriter l_writer = new JsonTextWriter(l_sw))
            {
                l_writer.Formatting = Formatting.Indented;
                l_writer.WriteStartObject();

                l_writer.WritePropertyName("lcr_cn");
                l_writer.WriteValue(lcr_cn);

                l_writer.WritePropertyName("carac");
                l_writer.WriteValue(carac.car_cn);

                l_writer.WritePropertyName("lcr_a_lib");
                l_writer.WriteValue(lcr_a_lib);

            }
            return l_sb.ToString();
        }
    }
}
