namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : groupes </summary>
public partial class clg_groupes : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLgroupes;
    protected clg_Modele c_Modele;

	private Int64 c_grp_cn;
	private string c_grp_a_libel;
	private string c_grp_m_comment;
	private List<clg_utilisateur> c_Listeutilisateur;
	private List<clg_limitation_donnees> c_Listelimitation_donnees;
	private List<clg_limitation_fenetre> c_Listelimitation_fenetre;
	private List<clg_limitation_interface> c_Listelimitation_interface;


    private void Init()
    {
		c_Listeutilisateur = new List<clg_utilisateur>();
		c_Listelimitation_donnees = new List<clg_limitation_donnees>();
		c_Listelimitation_fenetre = new List<clg_limitation_fenetre>();
		c_Listelimitation_interface = new List<clg_limitation_interface>();

    }

    public override void Detruit()
    {
		c_Listeutilisateur.Clear();
		c_Listelimitation_donnees.Clear();
		c_Listelimitation_fenetre.Clear();
		c_Listelimitation_interface.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_groupes(clg_Modele pModele, Int64 pgrp_cn, bool pAMAJ = false) : base(pModele, pgrp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_groupes(clg_Modele pModele, Int64 pgrp_cn, string pgrp_a_libel, string pgrp_m_comment, bool pAMAJ = true) : base(pModele, pgrp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pgrp_cn != Int64.MinValue)
            c_grp_cn = pgrp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pgrp_a_libel != null)
            c_grp_a_libel = pgrp_a_libel;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_grp_m_comment = pgrp_m_comment;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pgrp_cn, string pgrp_a_libel, string pgrp_m_comment)
    {   
		        c_grp_cn = pgrp_cn;
		        c_grp_a_libel = pgrp_a_libel;
		        c_grp_m_comment = pgrp_m_comment;

        base.Initialise(pgrp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLgroupes; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listegroupes.Dictionnaire.Add(grp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listegroupes.Dictionnaire.Remove(grp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_groupes l_Clone = (clg_groupes) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeutilisateur.Count > 0) c_EstReference = true;
		if(c_Listelimitation_donnees.Count > 0) c_EstReference = true;
		if(c_Listelimitation_fenetre.Count > 0) c_EstReference = true;
		if(c_Listelimitation_interface.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_groupes(null, grp_cn, grp_a_libel, grp_m_comment,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_groupes l_clone = (clg_groupes) this.Clone;
		c_grp_cn = l_clone.grp_cn;
		c_grp_a_libel = l_clone.grp_a_libel;
		c_grp_m_comment = l_clone.grp_m_comment;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLgroupes.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLgroupes.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLgroupes.Delete(this);
    }

    /* Accesseur de la propriete grp_cn (grp_cn)
    * @return c_grp_cn */
    public Int64 grp_cn
    {
        get{return c_grp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_grp_cn != value)
                {
                    CreerClone();
                    c_grp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete grp_a_libel (grp_a_libel)
    * @return c_grp_a_libel */
    public string grp_a_libel
    {
        get{return c_grp_a_libel;}
        set
        {
            if(value != null)
            {
                if(c_grp_a_libel != value)
                {
                    CreerClone();
                    c_grp_a_libel = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete grp_m_comment (grp_m_comment)
    * @return c_grp_m_comment */
    public string grp_m_comment
    {
        get{return c_grp_m_comment;}
        set
        {
            if(c_grp_m_comment != value)
            {
                CreerClone();
                c_grp_m_comment = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type utilisateur */
    public List<clg_utilisateur> Listeutilisateur
    {
        get { return c_Listeutilisateur; }
    }    /* Accesseur de la liste des objets de type limitation_donnees */
    public List<clg_limitation_donnees> Listelimitation_donnees
    {
        get { return c_Listelimitation_donnees; }
    }    /* Accesseur de la liste des objets de type limitation_fenetre */
    public List<clg_limitation_fenetre> Listelimitation_fenetre
    {
        get { return c_Listelimitation_fenetre; }
    }    /* Accesseur de la liste des objets de type limitation_interface */
    public List<clg_limitation_interface> Listelimitation_interface
    {
        get { return c_Listelimitation_interface; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_grp_cn.ToString());
		l_Valeurs.Add(c_grp_a_libel.ToString());
		l_Valeurs.Add(c_grp_m_comment.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("grp_cn");
		l_Noms.Add("grp_a_libel");
		l_Noms.Add("grp_m_comment");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
