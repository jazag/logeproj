﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dll_ServeurLogeproj_Core
{
    public partial class clg_carac
    {
        public string ToJson()
        {
            StringBuilder l_sb = new StringBuilder();
            StringWriter l_sw = new StringWriter(l_sb);

            using (JsonWriter l_writer = new JsonTextWriter(l_sw))
            {
                l_writer.Formatting = Formatting.Indented;
                l_writer.WriteStartObject();

                l_writer.WritePropertyName("car_cn");
                l_writer.WriteValue(car_cn);

                l_writer.WritePropertyName("car_a_lib");
                l_writer.WriteValue(car_a_lib);

                l_writer.WritePropertyName("c_format_carac");
                l_writer.WriteRawValue(c_format_carac.ToJson());

                l_writer.WriteEndObject();

            }
            return l_sb.ToString();
        }
    }
    
    
}
