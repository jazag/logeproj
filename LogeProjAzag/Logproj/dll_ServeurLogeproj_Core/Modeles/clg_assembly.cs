namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : assembly </summary>
public partial class clg_assembly : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLassembly;
    protected clg_Modele c_Modele;

	private Int64 c_asb_cn;
	private string c_asb_a_libel;
	private string c_asb_a_version;
	private List<clg_fenetre> c_Listefenetre;
	private List<clg_objets_interface> c_Listeobjets_interface;


    private void Init()
    {
		c_Listefenetre = new List<clg_fenetre>();
		c_Listeobjets_interface = new List<clg_objets_interface>();

    }

    public override void Detruit()
    {
		c_Listefenetre.Clear();
		c_Listeobjets_interface.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_assembly(clg_Modele pModele, Int64 pasb_cn, bool pAMAJ = false) : base(pModele, pasb_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_assembly(clg_Modele pModele, Int64 pasb_cn, string pasb_a_libel, string pasb_a_version, bool pAMAJ = true) : base(pModele, pasb_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pasb_cn != Int64.MinValue)
            c_asb_cn = pasb_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pasb_a_libel != null)
            c_asb_a_libel = pasb_a_libel;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pasb_a_version != null)
            c_asb_a_version = pasb_a_version;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pasb_cn, string pasb_a_libel, string pasb_a_version)
    {   
		        c_asb_cn = pasb_cn;
		        c_asb_a_libel = pasb_a_libel;
		        c_asb_a_version = pasb_a_version;

        base.Initialise(pasb_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLassembly; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeassembly.Dictionnaire.Add(asb_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeassembly.Dictionnaire.Remove(asb_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_assembly l_Clone = (clg_assembly) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listefenetre.Count > 0) c_EstReference = true;
		if(c_Listeobjets_interface.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_assembly(null, asb_cn, asb_a_libel, asb_a_version,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_assembly l_clone = (clg_assembly) this.Clone;
		c_asb_cn = l_clone.asb_cn;
		c_asb_a_libel = l_clone.asb_a_libel;
		c_asb_a_version = l_clone.asb_a_version;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLassembly.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLassembly.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLassembly.Delete(this);
    }

    /* Accesseur de la propriete asb_cn (asb_cn)
    * @return c_asb_cn */
    public Int64 asb_cn
    {
        get{return c_asb_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_asb_cn != value)
                {
                    CreerClone();
                    c_asb_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete asb_a_libel (asb_a_libel)
    * @return c_asb_a_libel */
    public string asb_a_libel
    {
        get{return c_asb_a_libel;}
        set
        {
            if(value != null)
            {
                if(c_asb_a_libel != value)
                {
                    CreerClone();
                    c_asb_a_libel = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete asb_a_version (asb_a_version)
    * @return c_asb_a_version */
    public string asb_a_version
    {
        get{return c_asb_a_version;}
        set
        {
            if(value != null)
            {
                if(c_asb_a_version != value)
                {
                    CreerClone();
                    c_asb_a_version = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type fenetre */
    public List<clg_fenetre> Listefenetre
    {
        get { return c_Listefenetre; }
    }    /* Accesseur de la liste des objets de type objets_interface */
    public List<clg_objets_interface> Listeobjets_interface
    {
        get { return c_Listeobjets_interface; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_asb_cn.ToString());
		l_Valeurs.Add(c_asb_a_libel.ToString());
		l_Valeurs.Add(c_asb_a_version.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("asb_cn");
		l_Noms.Add("asb_a_libel");
		l_Noms.Add("asb_a_version");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
