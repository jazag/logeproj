namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : lien_arretes </summary>
public partial class clg_lien_arretes : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLlien_arretes;
    protected clg_Modele c_Modele;

	private Int64 c_lar_cn;
	private clg_elements c_elements;
	private clg_arretes c_arretes;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_elements = null;
		this.c_arretes = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_lien_arretes(clg_Modele pModele, Int64 plar_cn, bool pAMAJ = false) : base(pModele, plar_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_lien_arretes(clg_Modele pModele, Int64 plar_cn, clg_elements pelements, clg_arretes parretes, bool pAMAJ = true) : base(pModele, plar_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(plar_cn != Int64.MinValue)
            c_lar_cn = plar_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_elements = pelements;
		        c_arretes = parretes;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 plar_cn, Int64 pelements, Int64 parretes)
    {   
		        c_lar_cn = plar_cn;
		c_dicReferences.Add("elements", pelements);
		c_dicReferences.Add("arretes", parretes);

        base.Initialise(plar_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLlien_arretes; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_elements = (clg_elements) c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");
		c_arretes = (clg_arretes) c_ModeleBase.RenvoieObjet(c_dicReferences["arretes"], "clg_arretes");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listelien_arretes.Dictionnaire.Add(lar_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listelien_arretes.Dictionnaire.Remove(lar_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_elements != null)if(!c_elements.Listelien_arretes.Contains(this)) c_elements.Listelien_arretes.Add(this);
		if(c_arretes != null)if(!c_arretes.Listelien_arretes.Contains(this)) c_arretes.Listelien_arretes.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_lien_arretes l_Clone = (clg_lien_arretes) Clone;
		if(l_Clone.elements != null)if(l_Clone.elements.Listelien_arretes.Contains(this)) l_Clone.elements.Listelien_arretes.Remove(this);
		if(l_Clone.arretes != null)if(l_Clone.arretes.Listelien_arretes.Contains(this)) l_Clone.arretes.Listelien_arretes.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_lien_arretes(null, lar_cn, elements, arretes,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_lien_arretes l_clone = (clg_lien_arretes) this.Clone;
		c_lar_cn = l_clone.lar_cn;
		c_elements = l_clone.elements;
		c_arretes = l_clone.arretes;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLlien_arretes.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLlien_arretes.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLlien_arretes.Delete(this);
    }

    /* Accesseur de la propriete lar_cn (lar_cn)
    * @return c_lar_cn */
    public Int64 lar_cn
    {
        get{return c_lar_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_lar_cn != value)
                {
                    CreerClone();
                    c_lar_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete elements (lar_ele_cn)
    * @return c_elements */
    public clg_elements elements
    {
        get{return c_elements;}
        set
        {
            if(c_elements != value)
            {
                CreerClone();
                c_elements = value;
            }
        }
    }
    /* Accesseur de la propriete arretes (lar_ara_cn)
    * @return c_arretes */
    public clg_arretes arretes
    {
        get{return c_arretes;}
        set
        {
            if(c_arretes != value)
            {
                CreerClone();
                c_arretes = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_lar_cn.ToString());
		l_Valeurs.Add(c_elements==null ? "-1" : c_elements.ID.ToString());
		l_Valeurs.Add(c_arretes==null ? "-1" : c_arretes.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("lar_cn");
		l_Noms.Add("elements");
		l_Noms.Add("arretes");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
