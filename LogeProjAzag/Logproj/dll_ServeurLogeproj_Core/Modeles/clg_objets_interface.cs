namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : objets_interface </summary>
public partial class clg_objets_interface : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLobjets_interface;
    protected clg_Modele c_Modele;

	private Int64 c_obi_cn;
	private string c_obi_a_libel;
	private clg_objets_interface c_objets_interface;
	private clg_objets_interface c_objets_interface2;
	private Int64 c_obi_n_niveau;
	private clg_t_objets_interface c_t_objets_interface;
	private clg_assembly c_assembly;
	private clg_objets_interface c_objets_interface3;
	private List<clg_limitation_interface> c_Listelimitation_interface;
	private List<clg_objets_interface> c_Listeobjets_interface;
	private List<clg_objets_interface> c_Listeobjets_interface2;
	private List<clg_objets_interface> c_Listeobjets_interface3;


    private void Init()
    {
		c_Listelimitation_interface = new List<clg_limitation_interface>();
		c_Listeobjets_interface = new List<clg_objets_interface>();
		c_Listeobjets_interface2 = new List<clg_objets_interface>();
		c_Listeobjets_interface3 = new List<clg_objets_interface>();

    }

    public override void Detruit()
    {
		c_Listelimitation_interface.Clear();
		c_Listeobjets_interface.Clear();
		c_Listeobjets_interface2.Clear();
		c_Listeobjets_interface3.Clear();
		this.c_objets_interface = null;
		this.c_objets_interface2 = null;
		this.c_t_objets_interface = null;
		this.c_assembly = null;
		this.c_objets_interface3 = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_objets_interface(clg_Modele pModele, Int64 pobi_cn, bool pAMAJ = false) : base(pModele, pobi_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_objets_interface(clg_Modele pModele, Int64 pobi_cn, string pobi_a_libel, clg_objets_interface pobjets_interface, clg_objets_interface pobjets_interface2, Int64 pobi_n_niveau, clg_t_objets_interface pt_objets_interface, clg_assembly passembly, clg_objets_interface pobjets_interface3, bool pAMAJ = true) : base(pModele, pobi_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pobi_cn != Int64.MinValue)
            c_obi_cn = pobi_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pobi_a_libel != null)
            c_obi_a_libel = pobi_a_libel;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_objets_interface = pobjets_interface;
		        if(pobjets_interface2 != null)
            c_objets_interface2 = pobjets_interface2;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pobi_n_niveau != Int64.MinValue)
            c_obi_n_niveau = pobi_n_niveau;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_objets_interface != null)
            c_t_objets_interface = pt_objets_interface;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_assembly = passembly;
		        c_objets_interface3 = pobjets_interface3;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pobi_cn, string pobi_a_libel, Int64 pobjets_interface, Int64 pobjets_interface2, Int64 pobi_n_niveau, Int64 pt_objets_interface, Int64 passembly, Int64 pobjets_interface3)
    {   
		        c_obi_cn = pobi_cn;
		        c_obi_a_libel = pobi_a_libel;
		c_dicReferences.Add("objets_interface", pobjets_interface);
		c_dicReferences.Add("objets_interface2", pobjets_interface2);
		        c_obi_n_niveau = pobi_n_niveau;
		c_dicReferences.Add("t_objets_interface", pt_objets_interface);
		c_dicReferences.Add("assembly", passembly);
		c_dicReferences.Add("objets_interface3", pobjets_interface3);

        base.Initialise(pobi_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLobjets_interface; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_objets_interface = (clg_objets_interface) c_ModeleBase.RenvoieObjet(c_dicReferences["objets_interface"], "clg_objets_interface");
		c_objets_interface2 = (clg_objets_interface) c_ModeleBase.RenvoieObjet(c_dicReferences["objets_interface2"], "clg_objets_interface");
		c_t_objets_interface = (clg_t_objets_interface) c_ModeleBase.RenvoieObjet(c_dicReferences["t_objets_interface"], "clg_t_objets_interface");
		c_assembly = (clg_assembly) c_ModeleBase.RenvoieObjet(c_dicReferences["assembly"], "clg_assembly");
		c_objets_interface3 = (clg_objets_interface) c_ModeleBase.RenvoieObjet(c_dicReferences["objets_interface3"], "clg_objets_interface");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeobjets_interface.Dictionnaire.Add(obi_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeobjets_interface.Dictionnaire.Remove(obi_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_objets_interface != null)if(!c_objets_interface.Listeobjets_interface2.Contains(this)) c_objets_interface.Listeobjets_interface2.Add(this);
		if(c_objets_interface2 != null)if(!c_objets_interface2.Listeobjets_interface3.Contains(this)) c_objets_interface2.Listeobjets_interface3.Add(this);
		if(c_t_objets_interface != null)if(!c_t_objets_interface.Listeobjets_interface.Contains(this)) c_t_objets_interface.Listeobjets_interface.Add(this);
		if(c_assembly != null)if(!c_assembly.Listeobjets_interface.Contains(this)) c_assembly.Listeobjets_interface.Add(this);
		if(c_objets_interface3 != null)if(!c_objets_interface3.Listeobjets_interface.Contains(this)) c_objets_interface3.Listeobjets_interface.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_objets_interface l_Clone = (clg_objets_interface) Clone;
		if(l_Clone.objets_interface != null)if(l_Clone.objets_interface.Listeobjets_interface2.Contains(this)) l_Clone.objets_interface.Listeobjets_interface2.Remove(this);
		if(l_Clone.objets_interface2 != null)if(l_Clone.objets_interface2.Listeobjets_interface3.Contains(this)) l_Clone.objets_interface2.Listeobjets_interface3.Remove(this);
		if(l_Clone.t_objets_interface != null)if(l_Clone.t_objets_interface.Listeobjets_interface.Contains(this)) l_Clone.t_objets_interface.Listeobjets_interface.Remove(this);
		if(l_Clone.assembly != null)if(l_Clone.assembly.Listeobjets_interface.Contains(this)) l_Clone.assembly.Listeobjets_interface.Remove(this);
		if(l_Clone.objets_interface3 != null)if(l_Clone.objets_interface3.Listeobjets_interface.Contains(this)) l_Clone.objets_interface3.Listeobjets_interface.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listelimitation_interface.Count > 0) c_EstReference = true;
		if(c_Listeobjets_interface.Count > 0) c_EstReference = true;
		if(c_Listeobjets_interface2.Count > 0) c_EstReference = true;
		if(c_Listeobjets_interface3.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_objets_interface(null, obi_cn, obi_a_libel, objets_interface, objets_interface2, obi_n_niveau, t_objets_interface, assembly, objets_interface3,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_objets_interface l_clone = (clg_objets_interface) this.Clone;
		c_obi_cn = l_clone.obi_cn;
		c_obi_a_libel = l_clone.obi_a_libel;
		c_objets_interface = l_clone.objets_interface;
		c_objets_interface2 = l_clone.objets_interface2;
		c_obi_n_niveau = l_clone.obi_n_niveau;
		c_t_objets_interface = l_clone.t_objets_interface;
		c_assembly = l_clone.assembly;
		c_objets_interface3 = l_clone.objets_interface3;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLobjets_interface.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLobjets_interface.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLobjets_interface.Delete(this);
    }

    /* Accesseur de la propriete obi_cn (obi_cn)
    * @return c_obi_cn */
    public Int64 obi_cn
    {
        get{return c_obi_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_obi_cn != value)
                {
                    CreerClone();
                    c_obi_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete obi_a_libel (obi_a_libel)
    * @return c_obi_a_libel */
    public string obi_a_libel
    {
        get{return c_obi_a_libel;}
        set
        {
            if(value != null)
            {
                if(c_obi_a_libel != value)
                {
                    CreerClone();
                    c_obi_a_libel = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete objets_interface (obi_cn_pere)
    * @return c_objets_interface */
    public clg_objets_interface objets_interface
    {
        get{return c_objets_interface;}
        set
        {
            if(c_objets_interface != value)
            {
                CreerClone();
                c_objets_interface = value;
            }
        }
    }
    /* Accesseur de la propriete objets_interface2 (obi_cn_racine)
    * @return c_objets_interface2 */
    public clg_objets_interface objets_interface2
    {
        get{return c_objets_interface2;}
        set
        {
            if(value != null)
            {
                if(c_objets_interface2 != value)
                {
                    CreerClone();
                    c_objets_interface2 = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete obi_n_niveau (obi_n_niveau)
    * @return c_obi_n_niveau */
    public Int64 obi_n_niveau
    {
        get{return c_obi_n_niveau;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_obi_n_niveau != value)
                {
                    CreerClone();
                    c_obi_n_niveau = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_objets_interface (obi_t_obi_cn)
    * @return c_t_objets_interface */
    public clg_t_objets_interface t_objets_interface
    {
        get{return c_t_objets_interface;}
        set
        {
            if(value != null)
            {
                if(c_t_objets_interface != value)
                {
                    CreerClone();
                    c_t_objets_interface = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete assembly (obi_asb_cn)
    * @return c_assembly */
    public clg_assembly assembly
    {
        get{return c_assembly;}
        set
        {
            if(c_assembly != value)
            {
                CreerClone();
                c_assembly = value;
            }
        }
    }
    /* Accesseur de la propriete objets_interface3 (obi_cn_conteneur_racine)
    * @return c_objets_interface3 */
    public clg_objets_interface objets_interface3
    {
        get{return c_objets_interface3;}
        set
        {
            if(c_objets_interface3 != value)
            {
                CreerClone();
                c_objets_interface3 = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type limitation_interface */
    public List<clg_limitation_interface> Listelimitation_interface
    {
        get { return c_Listelimitation_interface; }
    }    /* Accesseur de la liste des objets de type objets_interface */
    public List<clg_objets_interface> Listeobjets_interface
    {
        get { return c_Listeobjets_interface; }
    }    /* Accesseur de la liste des objets de type objets_interface */
    public List<clg_objets_interface> Listeobjets_interface2
    {
        get { return c_Listeobjets_interface2; }
    }    /* Accesseur de la liste des objets de type objets_interface */
    public List<clg_objets_interface> Listeobjets_interface3
    {
        get { return c_Listeobjets_interface3; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_obi_cn.ToString());
		l_Valeurs.Add(c_obi_a_libel.ToString());
		l_Valeurs.Add(c_objets_interface==null ? "-1" : c_objets_interface.ID.ToString());
		l_Valeurs.Add(c_objets_interface2==null ? "-1" : c_objets_interface2.ID.ToString());
		l_Valeurs.Add(c_obi_n_niveau.ToString());
		l_Valeurs.Add(c_t_objets_interface==null ? "-1" : c_t_objets_interface.ID.ToString());
		l_Valeurs.Add(c_assembly==null ? "-1" : c_assembly.ID.ToString());
		l_Valeurs.Add(c_objets_interface3==null ? "-1" : c_objets_interface3.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("obi_cn");
		l_Noms.Add("obi_a_libel");
		l_Noms.Add("objets_interface");
		l_Noms.Add("objets_interface2");
		l_Noms.Add("obi_n_niveau");
		l_Noms.Add("t_objets_interface");
		l_Noms.Add("assembly");
		l_Noms.Add("objets_interface3");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
