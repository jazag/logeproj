namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_journaux </summary>
public partial class clg_t_journaux : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_journaux;
    protected clg_Modele c_Modele;

	private Int64 c_t_jou_cn;
	private string c_t_jou_a_code;
	private string c_t_jou_a_lib;
	private string c_t_jou_a_cmt;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_journaux(clg_Modele pModele, Int64 pt_jou_cn, bool pAMAJ = false) : base(pModele, pt_jou_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_journaux(clg_Modele pModele, Int64 pt_jou_cn, string pt_jou_a_code, string pt_jou_a_lib, string pt_jou_a_cmt, bool pAMAJ = true) : base(pModele, pt_jou_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_jou_cn != Int64.MinValue)
            c_t_jou_cn = pt_jou_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_t_jou_a_code = pt_jou_a_code;
		        c_t_jou_a_lib = pt_jou_a_lib;
		        c_t_jou_a_cmt = pt_jou_a_cmt;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_jou_cn, string pt_jou_a_code, string pt_jou_a_lib, string pt_jou_a_cmt)
    {   
		        c_t_jou_cn = pt_jou_cn;
		        c_t_jou_a_code = pt_jou_a_code;
		        c_t_jou_a_lib = pt_jou_a_lib;
		        c_t_jou_a_cmt = pt_jou_a_cmt;

        base.Initialise(pt_jou_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_journaux; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_journaux.Dictionnaire.Add(t_jou_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_journaux.Dictionnaire.Remove(t_jou_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_journaux l_Clone = (clg_t_journaux) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_journaux(null, t_jou_cn, t_jou_a_code, t_jou_a_lib, t_jou_a_cmt,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_journaux l_clone = (clg_t_journaux) this.Clone;
		c_t_jou_cn = l_clone.t_jou_cn;
		c_t_jou_a_code = l_clone.t_jou_a_code;
		c_t_jou_a_lib = l_clone.t_jou_a_lib;
		c_t_jou_a_cmt = l_clone.t_jou_a_cmt;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_journaux.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_journaux.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_journaux.Delete(this);
    }

    /* Accesseur de la propriete t_jou_cn (t_jou_cn)
    * @return c_t_jou_cn */
    public Int64 t_jou_cn
    {
        get{return c_t_jou_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_jou_cn != value)
                {
                    CreerClone();
                    c_t_jou_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_jou_a_code (t_jou_a_code)
    * @return c_t_jou_a_code */
    public string t_jou_a_code
    {
        get{return c_t_jou_a_code;}
        set
        {
            if(c_t_jou_a_code != value)
            {
                CreerClone();
                c_t_jou_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete t_jou_a_lib (t_jou_a_lib)
    * @return c_t_jou_a_lib */
    public string t_jou_a_lib
    {
        get{return c_t_jou_a_lib;}
        set
        {
            if(c_t_jou_a_lib != value)
            {
                CreerClone();
                c_t_jou_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete t_jou_a_cmt (t_jou_a_cmt)
    * @return c_t_jou_a_cmt */
    public string t_jou_a_cmt
    {
        get{return c_t_jou_a_cmt;}
        set
        {
            if(c_t_jou_a_cmt != value)
            {
                CreerClone();
                c_t_jou_a_cmt = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_jou_cn.ToString());
		l_Valeurs.Add(c_t_jou_a_code.ToString());
		l_Valeurs.Add(c_t_jou_a_lib.ToString());
		l_Valeurs.Add(c_t_jou_a_cmt.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_jou_cn");
		l_Noms.Add("t_jou_a_code");
		l_Noms.Add("t_jou_a_lib");
		l_Noms.Add("t_jou_a_cmt");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
