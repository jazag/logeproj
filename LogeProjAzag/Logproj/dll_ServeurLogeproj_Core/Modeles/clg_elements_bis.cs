namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : elements_bis </summary>
public partial class clg_elements_bis : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLelements_bis;
    protected clg_Modele c_Modele;

	private Int64 c_ele_cn;
	private Int64 c_ele_gpm_cn;
	private Int64 c_ele_cn_pere;
	private Int64 c_ele_n_niveau;
	private string c_ele_a_lib;
	private Int64 c_ele_t_ele_cn;
	private string c_ele_a_code;
	private Int64 c_ele_cn_racine;
	private Int64 c_ele_n_archive;
	private DateTime c_ele_d_debut_prev;
	private DateTime c_ele_d_fin_reelle;
	private DateTime c_ele_d_fin_prev;
	private DateTime c_ele_d_debut_reelle;
	private DateTime c_ele_d_archivage;
	private Int64 c_ele_n_visibleetat;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_elements_bis(clg_Modele pModele, Int64 pele_cn, bool pAMAJ = false) : base(pModele, pele_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_elements_bis(clg_Modele pModele, Int64 pele_cn, Int64 pele_gpm_cn, Int64 pele_cn_pere, Int64 pele_n_niveau, string pele_a_lib, Int64 pele_t_ele_cn, string pele_a_code, Int64 pele_cn_racine, Int64 pele_n_archive, DateTime pele_d_debut_prev, DateTime pele_d_fin_reelle, DateTime pele_d_fin_prev, DateTime pele_d_debut_reelle, DateTime pele_d_archivage, Int64 pele_n_visibleetat, bool pAMAJ = true) : base(pModele, pele_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pele_cn != Int64.MinValue)
            c_ele_cn = pele_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pele_gpm_cn != Int64.MinValue)
            c_ele_gpm_cn = pele_gpm_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ele_cn_pere = pele_cn_pere;
		        if(pele_n_niveau != Int64.MinValue)
            c_ele_n_niveau = pele_n_niveau;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ele_a_lib = pele_a_lib;
		        if(pele_t_ele_cn != Int64.MinValue)
            c_ele_t_ele_cn = pele_t_ele_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ele_a_code = pele_a_code;
		        if(pele_cn_racine != Int64.MinValue)
            c_ele_cn_racine = pele_cn_racine;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ele_n_archive = pele_n_archive;
		        c_ele_d_debut_prev = pele_d_debut_prev;
		        c_ele_d_fin_reelle = pele_d_fin_reelle;
		        c_ele_d_fin_prev = pele_d_fin_prev;
		        c_ele_d_debut_reelle = pele_d_debut_reelle;
		        c_ele_d_archivage = pele_d_archivage;
		        if(pele_n_visibleetat != Int64.MinValue)
            c_ele_n_visibleetat = pele_n_visibleetat;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pele_cn, Int64 pele_gpm_cn, Int64 pele_cn_pere, Int64 pele_n_niveau, string pele_a_lib, Int64 pele_t_ele_cn, string pele_a_code, Int64 pele_cn_racine, Int64 pele_n_archive, DateTime pele_d_debut_prev, DateTime pele_d_fin_reelle, DateTime pele_d_fin_prev, DateTime pele_d_debut_reelle, DateTime pele_d_archivage, Int64 pele_n_visibleetat)
    {   
		        c_ele_cn = pele_cn;
		        c_ele_gpm_cn = pele_gpm_cn;
		        c_ele_cn_pere = pele_cn_pere;
		        c_ele_n_niveau = pele_n_niveau;
		        c_ele_a_lib = pele_a_lib;
		        c_ele_t_ele_cn = pele_t_ele_cn;
		        c_ele_a_code = pele_a_code;
		        c_ele_cn_racine = pele_cn_racine;
		        c_ele_n_archive = pele_n_archive;
		        c_ele_d_debut_prev = pele_d_debut_prev;
		        c_ele_d_fin_reelle = pele_d_fin_reelle;
		        c_ele_d_fin_prev = pele_d_fin_prev;
		        c_ele_d_debut_reelle = pele_d_debut_reelle;
		        c_ele_d_archivage = pele_d_archivage;
		        c_ele_n_visibleetat = pele_n_visibleetat;

        base.Initialise(pele_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLelements_bis; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeelements_bis.Dictionnaire.Add(ele_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeelements_bis.Dictionnaire.Remove(ele_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_elements_bis l_Clone = (clg_elements_bis) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_elements_bis(null, ele_cn, ele_gpm_cn, ele_cn_pere, ele_n_niveau, ele_a_lib, ele_t_ele_cn, ele_a_code, ele_cn_racine, ele_n_archive, ele_d_debut_prev, ele_d_fin_reelle, ele_d_fin_prev, ele_d_debut_reelle, ele_d_archivage, ele_n_visibleetat,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_elements_bis l_clone = (clg_elements_bis) this.Clone;
		c_ele_cn = l_clone.ele_cn;
		c_ele_gpm_cn = l_clone.ele_gpm_cn;
		c_ele_cn_pere = l_clone.ele_cn_pere;
		c_ele_n_niveau = l_clone.ele_n_niveau;
		c_ele_a_lib = l_clone.ele_a_lib;
		c_ele_t_ele_cn = l_clone.ele_t_ele_cn;
		c_ele_a_code = l_clone.ele_a_code;
		c_ele_cn_racine = l_clone.ele_cn_racine;
		c_ele_n_archive = l_clone.ele_n_archive;
		c_ele_d_debut_prev = l_clone.ele_d_debut_prev;
		c_ele_d_fin_reelle = l_clone.ele_d_fin_reelle;
		c_ele_d_fin_prev = l_clone.ele_d_fin_prev;
		c_ele_d_debut_reelle = l_clone.ele_d_debut_reelle;
		c_ele_d_archivage = l_clone.ele_d_archivage;
		c_ele_n_visibleetat = l_clone.ele_n_visibleetat;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLelements_bis.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLelements_bis.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLelements_bis.Delete(this);
    }

    /* Accesseur de la propriete ele_cn (ele_cn)
    * @return c_ele_cn */
    public Int64 ele_cn
    {
        get{return c_ele_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ele_cn != value)
                {
                    CreerClone();
                    c_ele_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ele_gpm_cn (ele_gpm_cn)
    * @return c_ele_gpm_cn */
    public Int64 ele_gpm_cn
    {
        get{return c_ele_gpm_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ele_gpm_cn != value)
                {
                    CreerClone();
                    c_ele_gpm_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ele_cn_pere (ele_cn_pere)
    * @return c_ele_cn_pere */
    public Int64 ele_cn_pere
    {
        get{return c_ele_cn_pere;}
        set
        {
            if(c_ele_cn_pere != value)
            {
                CreerClone();
                c_ele_cn_pere = value;
            }
        }
    }
    /* Accesseur de la propriete ele_n_niveau (ele_n_niveau)
    * @return c_ele_n_niveau */
    public Int64 ele_n_niveau
    {
        get{return c_ele_n_niveau;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ele_n_niveau != value)
                {
                    CreerClone();
                    c_ele_n_niveau = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ele_a_lib (ele_a_lib)
    * @return c_ele_a_lib */
    public string ele_a_lib
    {
        get{return c_ele_a_lib;}
        set
        {
            if(c_ele_a_lib != value)
            {
                CreerClone();
                c_ele_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete ele_t_ele_cn (ele_t_ele_cn)
    * @return c_ele_t_ele_cn */
    public Int64 ele_t_ele_cn
    {
        get{return c_ele_t_ele_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ele_t_ele_cn != value)
                {
                    CreerClone();
                    c_ele_t_ele_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ele_a_code (ele_a_code)
    * @return c_ele_a_code */
    public string ele_a_code
    {
        get{return c_ele_a_code;}
        set
        {
            if(c_ele_a_code != value)
            {
                CreerClone();
                c_ele_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete ele_cn_racine (ele_cn_racine)
    * @return c_ele_cn_racine */
    public Int64 ele_cn_racine
    {
        get{return c_ele_cn_racine;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ele_cn_racine != value)
                {
                    CreerClone();
                    c_ele_cn_racine = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ele_n_archive (ele_n_archive)
    * @return c_ele_n_archive */
    public Int64 ele_n_archive
    {
        get{return c_ele_n_archive;}
        set
        {
            if(c_ele_n_archive != value)
            {
                CreerClone();
                c_ele_n_archive = value;
            }
        }
    }
    /* Accesseur de la propriete ele_d_debut_prev (ele_d_debut_prev)
    * @return c_ele_d_debut_prev */
    public DateTime ele_d_debut_prev
    {
        get{return c_ele_d_debut_prev;}
        set
        {
            if(c_ele_d_debut_prev != value)
            {
                CreerClone();
                c_ele_d_debut_prev = value;
            }
        }
    }
    /* Accesseur de la propriete ele_d_fin_reelle (ele_d_fin_reelle)
    * @return c_ele_d_fin_reelle */
    public DateTime ele_d_fin_reelle
    {
        get{return c_ele_d_fin_reelle;}
        set
        {
            if(c_ele_d_fin_reelle != value)
            {
                CreerClone();
                c_ele_d_fin_reelle = value;
            }
        }
    }
    /* Accesseur de la propriete ele_d_fin_prev (ele_d_fin_prev)
    * @return c_ele_d_fin_prev */
    public DateTime ele_d_fin_prev
    {
        get{return c_ele_d_fin_prev;}
        set
        {
            if(c_ele_d_fin_prev != value)
            {
                CreerClone();
                c_ele_d_fin_prev = value;
            }
        }
    }
    /* Accesseur de la propriete ele_d_debut_reelle (ele_d_debut_reelle)
    * @return c_ele_d_debut_reelle */
    public DateTime ele_d_debut_reelle
    {
        get{return c_ele_d_debut_reelle;}
        set
        {
            if(c_ele_d_debut_reelle != value)
            {
                CreerClone();
                c_ele_d_debut_reelle = value;
            }
        }
    }
    /* Accesseur de la propriete ele_d_archivage (ele_d_archivage)
    * @return c_ele_d_archivage */
    public DateTime ele_d_archivage
    {
        get{return c_ele_d_archivage;}
        set
        {
            if(c_ele_d_archivage != value)
            {
                CreerClone();
                c_ele_d_archivage = value;
            }
        }
    }
    /* Accesseur de la propriete ele_n_visibleetat (ele_n_visibleetat)
    * @return c_ele_n_visibleetat */
    public Int64 ele_n_visibleetat
    {
        get{return c_ele_n_visibleetat;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ele_n_visibleetat != value)
                {
                    CreerClone();
                    c_ele_n_visibleetat = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ele_cn.ToString());
		l_Valeurs.Add(c_ele_gpm_cn.ToString());
		l_Valeurs.Add(c_ele_cn_pere.ToString());
		l_Valeurs.Add(c_ele_n_niveau.ToString());
		l_Valeurs.Add(c_ele_a_lib.ToString());
		l_Valeurs.Add(c_ele_t_ele_cn.ToString());
		l_Valeurs.Add(c_ele_a_code.ToString());
		l_Valeurs.Add(c_ele_cn_racine.ToString());
		l_Valeurs.Add(c_ele_n_archive.ToString());
		l_Valeurs.Add(c_ele_d_debut_prev.ToString());
		l_Valeurs.Add(c_ele_d_fin_reelle.ToString());
		l_Valeurs.Add(c_ele_d_fin_prev.ToString());
		l_Valeurs.Add(c_ele_d_debut_reelle.ToString());
		l_Valeurs.Add(c_ele_d_archivage.ToString());
		l_Valeurs.Add(c_ele_n_visibleetat.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ele_cn");
		l_Noms.Add("ele_gpm_cn");
		l_Noms.Add("ele_cn_pere");
		l_Noms.Add("ele_n_niveau");
		l_Noms.Add("ele_a_lib");
		l_Noms.Add("ele_t_ele_cn");
		l_Noms.Add("ele_a_code");
		l_Noms.Add("ele_cn_racine");
		l_Noms.Add("ele_n_archive");
		l_Noms.Add("ele_d_debut_prev");
		l_Noms.Add("ele_d_fin_reelle");
		l_Noms.Add("ele_d_fin_prev");
		l_Noms.Add("ele_d_debut_reelle");
		l_Noms.Add("ele_d_archivage");
		l_Noms.Add("ele_n_visibleetat");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
