namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : temps_travail_annuel </summary>
public partial class clg_temps_travail_annuel : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtemps_travail_annuel;
    protected clg_Modele c_Modele;

	private Int64 c_tta_cn;
	private clg_personnels c_personnels;
	private clg_annee_civile c_annee_civile;
	private string c_tta_a_valeur;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_personnels = null;
		this.c_annee_civile = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_temps_travail_annuel(clg_Modele pModele, Int64 ptta_cn, bool pAMAJ = false) : base(pModele, ptta_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_temps_travail_annuel(clg_Modele pModele, Int64 ptta_cn, clg_personnels ppersonnels, clg_annee_civile pannee_civile, string ptta_a_valeur, bool pAMAJ = true) : base(pModele, ptta_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptta_cn != Int64.MinValue)
            c_tta_cn = ptta_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_personnels = ppersonnels;
		        c_annee_civile = pannee_civile;
		        c_tta_a_valeur = ptta_a_valeur;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptta_cn, Int64 ppersonnels, Int64 pannee_civile, string ptta_a_valeur)
    {   
		        c_tta_cn = ptta_cn;
		c_dicReferences.Add("personnels", ppersonnels);
		c_dicReferences.Add("annee_civile", pannee_civile);
		        c_tta_a_valeur = ptta_a_valeur;

        base.Initialise(ptta_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtemps_travail_annuel; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_annee_civile = (clg_annee_civile) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_civile"], "clg_annee_civile");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetemps_travail_annuel.Dictionnaire.Add(tta_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetemps_travail_annuel.Dictionnaire.Remove(tta_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listetemps_travail_annuel.Contains(this)) c_personnels.Listetemps_travail_annuel.Add(this);
		if(c_annee_civile != null)if(!c_annee_civile.Listetemps_travail_annuel.Contains(this)) c_annee_civile.Listetemps_travail_annuel.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_temps_travail_annuel l_Clone = (clg_temps_travail_annuel) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listetemps_travail_annuel.Contains(this)) l_Clone.personnels.Listetemps_travail_annuel.Remove(this);
		if(l_Clone.annee_civile != null)if(l_Clone.annee_civile.Listetemps_travail_annuel.Contains(this)) l_Clone.annee_civile.Listetemps_travail_annuel.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_temps_travail_annuel(null, tta_cn, personnels, annee_civile, tta_a_valeur,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_temps_travail_annuel l_clone = (clg_temps_travail_annuel) this.Clone;
		c_tta_cn = l_clone.tta_cn;
		c_personnels = l_clone.personnels;
		c_annee_civile = l_clone.annee_civile;
		c_tta_a_valeur = l_clone.tta_a_valeur;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtemps_travail_annuel.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtemps_travail_annuel.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtemps_travail_annuel.Delete(this);
    }

    /* Accesseur de la propriete tta_cn (tta_cn)
    * @return c_tta_cn */
    public Int64 tta_cn
    {
        get{return c_tta_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tta_cn != value)
                {
                    CreerClone();
                    c_tta_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (tta_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete annee_civile (tta_ann_cn)
    * @return c_annee_civile */
    public clg_annee_civile annee_civile
    {
        get{return c_annee_civile;}
        set
        {
            if(c_annee_civile != value)
            {
                CreerClone();
                c_annee_civile = value;
            }
        }
    }
    /* Accesseur de la propriete tta_a_valeur (tta_a_valeur)
    * @return c_tta_a_valeur */
    public string tta_a_valeur
    {
        get{return c_tta_a_valeur;}
        set
        {
            if(c_tta_a_valeur != value)
            {
                CreerClone();
                c_tta_a_valeur = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tta_cn.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_annee_civile==null ? "-1" : c_annee_civile.ID.ToString());
		l_Valeurs.Add(c_tta_a_valeur.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tta_cn");
		l_Noms.Add("personnels");
		l_Noms.Add("annee_civile");
		l_Noms.Add("tta_a_valeur");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
