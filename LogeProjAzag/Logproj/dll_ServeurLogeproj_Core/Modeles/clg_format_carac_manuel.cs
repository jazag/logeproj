﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dll_ServeurLogeproj_Core
{
    public partial class clg_format_carac
    {
        public string ToJson()
        {
            StringBuilder l_sb = new StringBuilder();
            StringWriter l_sw = new StringWriter(l_sb);

            using (JsonWriter l_writer = new JsonTextWriter(l_sw))
            {
                l_writer.Formatting = Formatting.Indented;
                l_writer.WriteStartObject();

                l_writer.WritePropertyName("f_car_cn");
                l_writer.WriteValue(f_car_cn);

                l_writer.WritePropertyName("f_car_a_lib");
                l_writer.WriteValue(f_car_a_lib);

                l_writer.WriteEndObject();

            }
            return l_sb.ToString();
        }
    }
}
