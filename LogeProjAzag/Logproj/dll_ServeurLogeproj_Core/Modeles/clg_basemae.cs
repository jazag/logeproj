namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : basemae </summary>
public partial class clg_basemae : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLbasemae;
    protected clg_Modele c_Modele;

	private Int64 c_base_cn;
	private string c_base_a_nom;
	private DateTime c_base_d_debut;
	private DateTime c_base_d_fin;
	private string c_base_a_odbc;
	private string c_base_a_chemin;
	private clg_type_base_mae c_type_base_mae;
	private string c_base_a_copie;
	private List<clg_lien_sage> c_Listelien_sage;
	private List<clg_tb_sage> c_Listetb_sage;


    private void Init()
    {
		c_Listelien_sage = new List<clg_lien_sage>();
		c_Listetb_sage = new List<clg_tb_sage>();

    }

    public override void Detruit()
    {
		c_Listelien_sage.Clear();
		c_Listetb_sage.Clear();
		this.c_type_base_mae = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_basemae(clg_Modele pModele, Int64 pbase_cn, bool pAMAJ = false) : base(pModele, pbase_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_basemae(clg_Modele pModele, Int64 pbase_cn, string pbase_a_nom, DateTime pbase_d_debut, DateTime pbase_d_fin, string pbase_a_odbc, string pbase_a_chemin, clg_type_base_mae ptype_base_mae, string pbase_a_copie, bool pAMAJ = true) : base(pModele, pbase_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pbase_cn != Int64.MinValue)
            c_base_cn = pbase_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_base_a_nom = pbase_a_nom;
		        c_base_d_debut = pbase_d_debut;
		        c_base_d_fin = pbase_d_fin;
		        c_base_a_odbc = pbase_a_odbc;
		        c_base_a_chemin = pbase_a_chemin;
		        c_type_base_mae = ptype_base_mae;
		        c_base_a_copie = pbase_a_copie;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pbase_cn, string pbase_a_nom, DateTime pbase_d_debut, DateTime pbase_d_fin, string pbase_a_odbc, string pbase_a_chemin, Int64 ptype_base_mae, string pbase_a_copie)
    {   
		        c_base_cn = pbase_cn;
		        c_base_a_nom = pbase_a_nom;
		        c_base_d_debut = pbase_d_debut;
		        c_base_d_fin = pbase_d_fin;
		        c_base_a_odbc = pbase_a_odbc;
		        c_base_a_chemin = pbase_a_chemin;
		c_dicReferences.Add("type_base_mae", ptype_base_mae);
		        c_base_a_copie = pbase_a_copie;

        base.Initialise(pbase_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLbasemae; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_type_base_mae = (clg_type_base_mae) c_ModeleBase.RenvoieObjet(c_dicReferences["type_base_mae"], "clg_type_base_mae");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listebasemae.Dictionnaire.Add(base_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listebasemae.Dictionnaire.Remove(base_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_type_base_mae != null)if(!c_type_base_mae.Listebasemae.Contains(this)) c_type_base_mae.Listebasemae.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_basemae l_Clone = (clg_basemae) Clone;
		if(l_Clone.type_base_mae != null)if(l_Clone.type_base_mae.Listebasemae.Contains(this)) l_Clone.type_base_mae.Listebasemae.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listelien_sage.Count > 0) c_EstReference = true;
		if(c_Listetb_sage.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_basemae(null, base_cn, base_a_nom, base_d_debut, base_d_fin, base_a_odbc, base_a_chemin, type_base_mae, base_a_copie,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_basemae l_clone = (clg_basemae) this.Clone;
		c_base_cn = l_clone.base_cn;
		c_base_a_nom = l_clone.base_a_nom;
		c_base_d_debut = l_clone.base_d_debut;
		c_base_d_fin = l_clone.base_d_fin;
		c_base_a_odbc = l_clone.base_a_odbc;
		c_base_a_chemin = l_clone.base_a_chemin;
		c_type_base_mae = l_clone.type_base_mae;
		c_base_a_copie = l_clone.base_a_copie;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLbasemae.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLbasemae.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLbasemae.Delete(this);
    }

    /* Accesseur de la propriete base_cn (base_cn)
    * @return c_base_cn */
    public Int64 base_cn
    {
        get{return c_base_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_base_cn != value)
                {
                    CreerClone();
                    c_base_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete base_a_nom (base_a_nom)
    * @return c_base_a_nom */
    public string base_a_nom
    {
        get{return c_base_a_nom;}
        set
        {
            if(c_base_a_nom != value)
            {
                CreerClone();
                c_base_a_nom = value;
            }
        }
    }
    /* Accesseur de la propriete base_d_debut (base_d_debut)
    * @return c_base_d_debut */
    public DateTime base_d_debut
    {
        get{return c_base_d_debut;}
        set
        {
            if(c_base_d_debut != value)
            {
                CreerClone();
                c_base_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete base_d_fin (base_d_fin)
    * @return c_base_d_fin */
    public DateTime base_d_fin
    {
        get{return c_base_d_fin;}
        set
        {
            if(c_base_d_fin != value)
            {
                CreerClone();
                c_base_d_fin = value;
            }
        }
    }
    /* Accesseur de la propriete base_a_odbc (base_a_odbc)
    * @return c_base_a_odbc */
    public string base_a_odbc
    {
        get{return c_base_a_odbc;}
        set
        {
            if(c_base_a_odbc != value)
            {
                CreerClone();
                c_base_a_odbc = value;
            }
        }
    }
    /* Accesseur de la propriete base_a_chemin (base_a_chemin)
    * @return c_base_a_chemin */
    public string base_a_chemin
    {
        get{return c_base_a_chemin;}
        set
        {
            if(c_base_a_chemin != value)
            {
                CreerClone();
                c_base_a_chemin = value;
            }
        }
    }
    /* Accesseur de la propriete type_base_mae (base_tba_cn)
    * @return c_type_base_mae */
    public clg_type_base_mae type_base_mae
    {
        get{return c_type_base_mae;}
        set
        {
            if(c_type_base_mae != value)
            {
                CreerClone();
                c_type_base_mae = value;
            }
        }
    }
    /* Accesseur de la propriete base_a_copie (base_a_copie)
    * @return c_base_a_copie */
    public string base_a_copie
    {
        get{return c_base_a_copie;}
        set
        {
            if(c_base_a_copie != value)
            {
                CreerClone();
                c_base_a_copie = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type lien_sage */
    public List<clg_lien_sage> Listelien_sage
    {
        get { return c_Listelien_sage; }
    }    /* Accesseur de la liste des objets de type tb_sage */
    public List<clg_tb_sage> Listetb_sage
    {
        get { return c_Listetb_sage; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_base_cn.ToString());
		l_Valeurs.Add(c_base_a_nom.ToString());
		l_Valeurs.Add(c_base_d_debut.ToString());
		l_Valeurs.Add(c_base_d_fin.ToString());
		l_Valeurs.Add(c_base_a_odbc.ToString());
		l_Valeurs.Add(c_base_a_chemin.ToString());
		l_Valeurs.Add(c_type_base_mae==null ? "-1" : c_type_base_mae.ID.ToString());
		l_Valeurs.Add(c_base_a_copie.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("base_cn");
		l_Noms.Add("base_a_nom");
		l_Noms.Add("base_d_debut");
		l_Noms.Add("base_d_fin");
		l_Noms.Add("base_a_odbc");
		l_Noms.Add("base_a_chemin");
		l_Noms.Add("type_base_mae");
		l_Noms.Add("base_a_copie");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
