namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : plugin </summary>
public partial class clg_plugin : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLplugin;
    protected clg_Modele c_Modele;

	private Int64 c_plg_cn;
	private string c_plg_a_nom;
	private string c_plg_a_texte;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_plugin(clg_Modele pModele, Int64 pplg_cn, bool pAMAJ = false) : base(pModele, pplg_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_plugin(clg_Modele pModele, Int64 pplg_cn, string pplg_a_nom, string pplg_a_texte, bool pAMAJ = true) : base(pModele, pplg_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pplg_cn != Int64.MinValue)
            c_plg_cn = pplg_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_plg_a_nom = pplg_a_nom;
		        c_plg_a_texte = pplg_a_texte;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pplg_cn, string pplg_a_nom, string pplg_a_texte)
    {   
		        c_plg_cn = pplg_cn;
		        c_plg_a_nom = pplg_a_nom;
		        c_plg_a_texte = pplg_a_texte;

        base.Initialise(pplg_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLplugin; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeplugin.Dictionnaire.Add(plg_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeplugin.Dictionnaire.Remove(plg_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_plugin l_Clone = (clg_plugin) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_plugin(null, plg_cn, plg_a_nom, plg_a_texte,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_plugin l_clone = (clg_plugin) this.Clone;
		c_plg_cn = l_clone.plg_cn;
		c_plg_a_nom = l_clone.plg_a_nom;
		c_plg_a_texte = l_clone.plg_a_texte;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLplugin.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLplugin.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLplugin.Delete(this);
    }

    /* Accesseur de la propriete plg_cn (plg_cn)
    * @return c_plg_cn */
    public Int64 plg_cn
    {
        get{return c_plg_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_plg_cn != value)
                {
                    CreerClone();
                    c_plg_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete plg_a_nom (plg_a_nom)
    * @return c_plg_a_nom */
    public string plg_a_nom
    {
        get{return c_plg_a_nom;}
        set
        {
            if(c_plg_a_nom != value)
            {
                CreerClone();
                c_plg_a_nom = value;
            }
        }
    }
    /* Accesseur de la propriete plg_a_texte (plg_a_texte)
    * @return c_plg_a_texte */
    public string plg_a_texte
    {
        get{return c_plg_a_texte;}
        set
        {
            if(c_plg_a_texte != value)
            {
                CreerClone();
                c_plg_a_texte = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_plg_cn.ToString());
		l_Valeurs.Add(c_plg_a_nom.ToString());
		l_Valeurs.Add(c_plg_a_texte.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("plg_cn");
		l_Noms.Add("plg_a_nom");
		l_Noms.Add("plg_a_texte");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
