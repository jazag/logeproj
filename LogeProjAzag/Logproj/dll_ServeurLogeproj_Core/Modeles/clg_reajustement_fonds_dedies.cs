namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : reajustement_fonds_dedies </summary>
public partial class clg_reajustement_fonds_dedies : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLreajustement_fonds_dedies;
    protected clg_Modele c_Modele;

	private Int64 c_rfd_prj_cn;
	private Int64 c_rfd_acp_cn;
	private Int64 c_rfd_ann_cn;
	private double c_rfd_n_montant;
	private string c_rfd_a_comment;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_reajustement_fonds_dedies(clg_Modele pModele, Int64 prfd_prj_cn, bool pAMAJ = false) : base(pModele, prfd_prj_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_reajustement_fonds_dedies(clg_Modele pModele, Int64 prfd_prj_cn, Int64 prfd_acp_cn, Int64 prfd_ann_cn, double prfd_n_montant, string prfd_a_comment, bool pAMAJ = true) : base(pModele, prfd_prj_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(prfd_prj_cn != Int64.MinValue)
            c_rfd_prj_cn = prfd_prj_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(prfd_acp_cn != Int64.MinValue)
            c_rfd_acp_cn = prfd_acp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_rfd_ann_cn = prfd_ann_cn;
		        c_rfd_n_montant = prfd_n_montant;
		        c_rfd_a_comment = prfd_a_comment;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 prfd_prj_cn, Int64 prfd_acp_cn, Int64 prfd_ann_cn, double prfd_n_montant, string prfd_a_comment)
    {   
		        c_rfd_prj_cn = prfd_prj_cn;
		        c_rfd_acp_cn = prfd_acp_cn;
		        c_rfd_ann_cn = prfd_ann_cn;
		        c_rfd_n_montant = prfd_n_montant;
		        c_rfd_a_comment = prfd_a_comment;

        base.Initialise(prfd_prj_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLreajustement_fonds_dedies; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listereajustement_fonds_dedies.Dictionnaire.Add(rfd_prj_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listereajustement_fonds_dedies.Dictionnaire.Remove(rfd_prj_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_reajustement_fonds_dedies l_Clone = (clg_reajustement_fonds_dedies) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_reajustement_fonds_dedies(null, rfd_prj_cn, rfd_acp_cn, rfd_ann_cn, rfd_n_montant, rfd_a_comment,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_reajustement_fonds_dedies l_clone = (clg_reajustement_fonds_dedies) this.Clone;
		c_rfd_prj_cn = l_clone.rfd_prj_cn;
		c_rfd_acp_cn = l_clone.rfd_acp_cn;
		c_rfd_ann_cn = l_clone.rfd_ann_cn;
		c_rfd_n_montant = l_clone.rfd_n_montant;
		c_rfd_a_comment = l_clone.rfd_a_comment;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLreajustement_fonds_dedies.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLreajustement_fonds_dedies.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLreajustement_fonds_dedies.Delete(this);
    }

    /* Accesseur de la propriete rfd_prj_cn (rfd_prj_cn)
    * @return c_rfd_prj_cn */
    public Int64 rfd_prj_cn
    {
        get{return c_rfd_prj_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_rfd_prj_cn != value)
                {
                    CreerClone();
                    c_rfd_prj_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete rfd_acp_cn (rfd_acp_cn)
    * @return c_rfd_acp_cn */
    public Int64 rfd_acp_cn
    {
        get{return c_rfd_acp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_rfd_acp_cn != value)
                {
                    CreerClone();
                    c_rfd_acp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete rfd_ann_cn (rfd_ann_cn)
    * @return c_rfd_ann_cn */
    public Int64 rfd_ann_cn
    {
        get{return c_rfd_ann_cn;}
        set
        {
            if(c_rfd_ann_cn != value)
            {
                CreerClone();
                c_rfd_ann_cn = value;
            }
        }
    }
    /* Accesseur de la propriete rfd_n_montant (rfd_n_montant)
    * @return c_rfd_n_montant */
    public double rfd_n_montant
    {
        get{return c_rfd_n_montant;}
        set
        {
            if(c_rfd_n_montant != value)
            {
                CreerClone();
                c_rfd_n_montant = value;
            }
        }
    }
    /* Accesseur de la propriete rfd_a_comment (rfd_a_comment)
    * @return c_rfd_a_comment */
    public string rfd_a_comment
    {
        get{return c_rfd_a_comment;}
        set
        {
            if(c_rfd_a_comment != value)
            {
                CreerClone();
                c_rfd_a_comment = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_rfd_prj_cn.ToString());
		l_Valeurs.Add(c_rfd_acp_cn.ToString());
		l_Valeurs.Add(c_rfd_ann_cn.ToString());
		l_Valeurs.Add(c_rfd_n_montant.ToString());
		l_Valeurs.Add(c_rfd_a_comment.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("rfd_prj_cn");
		l_Noms.Add("rfd_acp_cn");
		l_Noms.Add("rfd_ann_cn");
		l_Noms.Add("rfd_n_montant");
		l_Noms.Add("rfd_a_comment");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
