namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_conge </summary>
public partial class clg_t_conge : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_conge;
    protected clg_Modele c_Modele;

	private Int64 c_t_cge_cn;
	private string c_t_cge_a_lib;
	private double c_t_cge_n_min;
	private bool c_t_cge_b_prev;
	private List<clg_conge_prevu> c_Listeconge_prevu;
	private List<clg_conge_realise> c_Listeconge_realise;


    private void Init()
    {
		c_Listeconge_prevu = new List<clg_conge_prevu>();
		c_Listeconge_realise = new List<clg_conge_realise>();

    }

    public override void Detruit()
    {
		c_Listeconge_prevu.Clear();
		c_Listeconge_realise.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_conge(clg_Modele pModele, Int64 pt_cge_cn, bool pAMAJ = false) : base(pModele, pt_cge_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_conge(clg_Modele pModele, Int64 pt_cge_cn, string pt_cge_a_lib, double pt_cge_n_min, bool pt_cge_b_prev, bool pAMAJ = true) : base(pModele, pt_cge_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_cge_cn != Int64.MinValue)
            c_t_cge_cn = pt_cge_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_t_cge_a_lib = pt_cge_a_lib;
		        c_t_cge_n_min = pt_cge_n_min;
		        c_t_cge_b_prev = pt_cge_b_prev;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_cge_cn, string pt_cge_a_lib, double pt_cge_n_min, bool pt_cge_b_prev)
    {   
		        c_t_cge_cn = pt_cge_cn;
		        c_t_cge_a_lib = pt_cge_a_lib;
		        c_t_cge_n_min = pt_cge_n_min;
		        c_t_cge_b_prev = pt_cge_b_prev;

        base.Initialise(pt_cge_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_conge; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_conge.Dictionnaire.Add(t_cge_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_conge.Dictionnaire.Remove(t_cge_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_conge l_Clone = (clg_t_conge) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeconge_prevu.Count > 0) c_EstReference = true;
		if(c_Listeconge_realise.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_conge(null, t_cge_cn, t_cge_a_lib, t_cge_n_min, t_cge_b_prev,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_conge l_clone = (clg_t_conge) this.Clone;
		c_t_cge_cn = l_clone.t_cge_cn;
		c_t_cge_a_lib = l_clone.t_cge_a_lib;
		c_t_cge_n_min = l_clone.t_cge_n_min;
		c_t_cge_b_prev = l_clone.t_cge_b_prev;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_conge.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_conge.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_conge.Delete(this);
    }

    /* Accesseur de la propriete t_cge_cn (t_cge_cn)
    * @return c_t_cge_cn */
    public Int64 t_cge_cn
    {
        get{return c_t_cge_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_cge_cn != value)
                {
                    CreerClone();
                    c_t_cge_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_cge_a_lib (t_cge_a_lib)
    * @return c_t_cge_a_lib */
    public string t_cge_a_lib
    {
        get{return c_t_cge_a_lib;}
        set
        {
            if(c_t_cge_a_lib != value)
            {
                CreerClone();
                c_t_cge_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete t_cge_n_min (t_cge_n_min)
    * @return c_t_cge_n_min */
    public double t_cge_n_min
    {
        get{return c_t_cge_n_min;}
        set
        {
            if(c_t_cge_n_min != value)
            {
                CreerClone();
                c_t_cge_n_min = value;
            }
        }
    }
    /* Accesseur de la propriete t_cge_b_prev (t_cge_b_prev)
    * @return c_t_cge_b_prev */
    public bool t_cge_b_prev
    {
        get{return c_t_cge_b_prev;}
        set
        {
            if(c_t_cge_b_prev != value)
            {
                CreerClone();
                c_t_cge_b_prev = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type conge_prevu */
    public List<clg_conge_prevu> Listeconge_prevu
    {
        get { return c_Listeconge_prevu; }
    }    /* Accesseur de la liste des objets de type conge_realise */
    public List<clg_conge_realise> Listeconge_realise
    {
        get { return c_Listeconge_realise; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_cge_cn.ToString());
		l_Valeurs.Add(c_t_cge_a_lib.ToString());
		l_Valeurs.Add(c_t_cge_n_min.ToString());
		l_Valeurs.Add(c_t_cge_b_prev.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_cge_cn");
		l_Noms.Add("t_cge_a_lib");
		l_Noms.Add("t_cge_n_min");
		l_Noms.Add("t_cge_b_prev");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
