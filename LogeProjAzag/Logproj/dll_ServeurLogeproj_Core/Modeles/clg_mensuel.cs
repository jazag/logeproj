namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : mensuel </summary>
public partial class clg_mensuel : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLmensuel;
    protected clg_Modele c_Modele;

	private Int64 c_mpa_cn;
	private clg_annee_civile c_annee_civile;
	private clg_mois c_mois;
	private List<clg_calendrier> c_Listecalendrier;


    private void Init()
    {
		c_Listecalendrier = new List<clg_calendrier>();

    }

    public override void Detruit()
    {
		c_Listecalendrier.Clear();
		this.c_annee_civile = null;
		this.c_mois = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_mensuel(clg_Modele pModele, Int64 pmpa_cn, bool pAMAJ = false) : base(pModele, pmpa_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_mensuel(clg_Modele pModele, Int64 pmpa_cn, clg_annee_civile pannee_civile, clg_mois pmois, bool pAMAJ = true) : base(pModele, pmpa_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pmpa_cn != Int64.MinValue)
            c_mpa_cn = pmpa_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pannee_civile != null)
            c_annee_civile = pannee_civile;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pmois != null)
            c_mois = pmois;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pmpa_cn, Int64 pannee_civile, Int64 pmois)
    {   
		        c_mpa_cn = pmpa_cn;
		c_dicReferences.Add("annee_civile", pannee_civile);
		c_dicReferences.Add("mois", pmois);

        base.Initialise(pmpa_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLmensuel; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_annee_civile = (clg_annee_civile) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_civile"], "clg_annee_civile");
		c_mois = (clg_mois) c_ModeleBase.RenvoieObjet(c_dicReferences["mois"], "clg_mois");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listemensuel.Dictionnaire.Add(mpa_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listemensuel.Dictionnaire.Remove(mpa_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_annee_civile != null)if(!c_annee_civile.Listemensuel.Contains(this)) c_annee_civile.Listemensuel.Add(this);
		if(c_mois != null)if(!c_mois.Listemensuel.Contains(this)) c_mois.Listemensuel.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_mensuel l_Clone = (clg_mensuel) Clone;
		if(l_Clone.annee_civile != null)if(l_Clone.annee_civile.Listemensuel.Contains(this)) l_Clone.annee_civile.Listemensuel.Remove(this);
		if(l_Clone.mois != null)if(l_Clone.mois.Listemensuel.Contains(this)) l_Clone.mois.Listemensuel.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecalendrier.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_mensuel(null, mpa_cn, annee_civile, mois,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_mensuel l_clone = (clg_mensuel) this.Clone;
		c_mpa_cn = l_clone.mpa_cn;
		c_annee_civile = l_clone.annee_civile;
		c_mois = l_clone.mois;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLmensuel.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLmensuel.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLmensuel.Delete(this);
    }

    /* Accesseur de la propriete mpa_cn (mpa_cn)
    * @return c_mpa_cn */
    public Int64 mpa_cn
    {
        get{return c_mpa_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_mpa_cn != value)
                {
                    CreerClone();
                    c_mpa_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete annee_civile (mpa_ann_cn)
    * @return c_annee_civile */
    public clg_annee_civile annee_civile
    {
        get{return c_annee_civile;}
        set
        {
            if(value != null)
            {
                if(c_annee_civile != value)
                {
                    CreerClone();
                    c_annee_civile = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete mois (mpa_mois_cn)
    * @return c_mois */
    public clg_mois mois
    {
        get{return c_mois;}
        set
        {
            if(value != null)
            {
                if(c_mois != value)
                {
                    CreerClone();
                    c_mois = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type calendrier */
    public List<clg_calendrier> Listecalendrier
    {
        get { return c_Listecalendrier; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_mpa_cn.ToString());
		l_Valeurs.Add(c_annee_civile==null ? "-1" : c_annee_civile.ID.ToString());
		l_Valeurs.Add(c_mois==null ? "-1" : c_mois.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("mpa_cn");
		l_Noms.Add("annee_civile");
		l_Noms.Add("mois");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
