namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : conge_prevu </summary>
public partial class clg_conge_prevu : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLconge_prevu;
    protected clg_Modele c_Modele;

	private Int64 c_cgp_cn;
	private clg_calendrier c_calendrier;
	private clg_personnels c_personnels;
	private clg_t_conge c_t_conge;
	private double c_cgp_n_nombre;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_calendrier = null;
		this.c_personnels = null;
		this.c_t_conge = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_conge_prevu(clg_Modele pModele, Int64 pcgp_cn, bool pAMAJ = false) : base(pModele, pcgp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_conge_prevu(clg_Modele pModele, Int64 pcgp_cn, clg_calendrier pcalendrier, clg_personnels ppersonnels, clg_t_conge pt_conge, double pcgp_n_nombre, bool pAMAJ = true) : base(pModele, pcgp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcgp_cn != Int64.MinValue)
            c_cgp_cn = pcgp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_calendrier = pcalendrier;
		        c_personnels = ppersonnels;
		        c_t_conge = pt_conge;
		        c_cgp_n_nombre = pcgp_n_nombre;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcgp_cn, DateTime pcalendrier, Int64 ppersonnels, Int64 pt_conge, double pcgp_n_nombre)
    {   
		        c_cgp_cn = pcgp_cn;
		c_dicReferences.Add("calendrier", pcalendrier);
		c_dicReferences.Add("personnels", ppersonnels);
		c_dicReferences.Add("t_conge", pt_conge);
		        c_cgp_n_nombre = pcgp_n_nombre;

        base.Initialise(pcgp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLconge_prevu; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_calendrier = (clg_calendrier) c_ModeleBase.RenvoieObjet(c_dicReferences["calendrier"], "clg_calendrier");
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_t_conge = (clg_t_conge) c_ModeleBase.RenvoieObjet(c_dicReferences["t_conge"], "clg_t_conge");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeconge_prevu.Dictionnaire.Add(cgp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeconge_prevu.Dictionnaire.Remove(cgp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_calendrier != null)if(!c_calendrier.Listeconge_prevu.Contains(this)) c_calendrier.Listeconge_prevu.Add(this);
		if(c_personnels != null)if(!c_personnels.Listeconge_prevu.Contains(this)) c_personnels.Listeconge_prevu.Add(this);
		if(c_t_conge != null)if(!c_t_conge.Listeconge_prevu.Contains(this)) c_t_conge.Listeconge_prevu.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_conge_prevu l_Clone = (clg_conge_prevu) Clone;
		if(l_Clone.calendrier != null)if(l_Clone.calendrier.Listeconge_prevu.Contains(this)) l_Clone.calendrier.Listeconge_prevu.Remove(this);
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listeconge_prevu.Contains(this)) l_Clone.personnels.Listeconge_prevu.Remove(this);
		if(l_Clone.t_conge != null)if(l_Clone.t_conge.Listeconge_prevu.Contains(this)) l_Clone.t_conge.Listeconge_prevu.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_conge_prevu(null, cgp_cn, calendrier, personnels, t_conge, cgp_n_nombre,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_conge_prevu l_clone = (clg_conge_prevu) this.Clone;
		c_cgp_cn = l_clone.cgp_cn;
		c_calendrier = l_clone.calendrier;
		c_personnels = l_clone.personnels;
		c_t_conge = l_clone.t_conge;
		c_cgp_n_nombre = l_clone.cgp_n_nombre;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLconge_prevu.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLconge_prevu.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLconge_prevu.Delete(this);
    }

    /* Accesseur de la propriete cgp_cn (cgp_cn)
    * @return c_cgp_cn */
    public Int64 cgp_cn
    {
        get{return c_cgp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cgp_cn != value)
                {
                    CreerClone();
                    c_cgp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete calendrier (cgp_cal_d_date)
    * @return c_calendrier */
    public clg_calendrier calendrier
    {
        get{return c_calendrier;}
        set
        {
            if(c_calendrier != value)
            {
                CreerClone();
                c_calendrier = value;
            }
        }
    }
    /* Accesseur de la propriete personnels (cgp_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete t_conge (cgp_t_cge_cn)
    * @return c_t_conge */
    public clg_t_conge t_conge
    {
        get{return c_t_conge;}
        set
        {
            if(c_t_conge != value)
            {
                CreerClone();
                c_t_conge = value;
            }
        }
    }
    /* Accesseur de la propriete cgp_n_nombre (cgp_n_nombre)
    * @return c_cgp_n_nombre */
    public double cgp_n_nombre
    {
        get{return c_cgp_n_nombre;}
        set
        {
            if(c_cgp_n_nombre != value)
            {
                CreerClone();
                c_cgp_n_nombre = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cgp_cn.ToString());
		l_Valeurs.Add(c_calendrier==null ? "-1" : c_calendrier.ID.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_t_conge==null ? "-1" : c_t_conge.ID.ToString());
		l_Valeurs.Add(c_cgp_n_nombre.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cgp_cn");
		l_Noms.Add("calendrier");
		l_Noms.Add("personnels");
		l_Noms.Add("t_conge");
		l_Noms.Add("cgp_n_nombre");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
