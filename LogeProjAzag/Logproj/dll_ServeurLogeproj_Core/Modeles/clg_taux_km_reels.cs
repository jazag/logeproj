namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : taux_km_reels </summary>
public partial class clg_taux_km_reels : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtaux_km_reels;
    protected clg_Modele c_Modele;

	private Int64 c_tkr_cn;
	private clg_vehicules c_vehicules;
	private DateTime c_tkr_d_debut;
	private DateTime c_tkr_d_fin;
	private double c_tkr_n_taux;
	private List<clg_deplacements_realises> c_Listedeplacements_realises;


    private void Init()
    {
		c_Listedeplacements_realises = new List<clg_deplacements_realises>();

    }

    public override void Detruit()
    {
		c_Listedeplacements_realises.Clear();
		this.c_vehicules = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_taux_km_reels(clg_Modele pModele, Int64 ptkr_cn, bool pAMAJ = false) : base(pModele, ptkr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_taux_km_reels(clg_Modele pModele, Int64 ptkr_cn, clg_vehicules pvehicules, DateTime ptkr_d_debut, DateTime ptkr_d_fin, double ptkr_n_taux, bool pAMAJ = true) : base(pModele, ptkr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptkr_cn != Int64.MinValue)
            c_tkr_cn = ptkr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_vehicules = pvehicules;
		        c_tkr_d_debut = ptkr_d_debut;
		        c_tkr_d_fin = ptkr_d_fin;
		        c_tkr_n_taux = ptkr_n_taux;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptkr_cn, Int64 pvehicules, DateTime ptkr_d_debut, DateTime ptkr_d_fin, double ptkr_n_taux)
    {   
		        c_tkr_cn = ptkr_cn;
		c_dicReferences.Add("vehicules", pvehicules);
		        c_tkr_d_debut = ptkr_d_debut;
		        c_tkr_d_fin = ptkr_d_fin;
		        c_tkr_n_taux = ptkr_n_taux;

        base.Initialise(ptkr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtaux_km_reels; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_vehicules = (clg_vehicules) c_ModeleBase.RenvoieObjet(c_dicReferences["vehicules"], "clg_vehicules");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetaux_km_reels.Dictionnaire.Add(tkr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetaux_km_reels.Dictionnaire.Remove(tkr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_vehicules != null)if(!c_vehicules.Listetaux_km_reels.Contains(this)) c_vehicules.Listetaux_km_reels.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_taux_km_reels l_Clone = (clg_taux_km_reels) Clone;
		if(l_Clone.vehicules != null)if(l_Clone.vehicules.Listetaux_km_reels.Contains(this)) l_Clone.vehicules.Listetaux_km_reels.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listedeplacements_realises.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_taux_km_reels(null, tkr_cn, vehicules, tkr_d_debut, tkr_d_fin, tkr_n_taux,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_taux_km_reels l_clone = (clg_taux_km_reels) this.Clone;
		c_tkr_cn = l_clone.tkr_cn;
		c_vehicules = l_clone.vehicules;
		c_tkr_d_debut = l_clone.tkr_d_debut;
		c_tkr_d_fin = l_clone.tkr_d_fin;
		c_tkr_n_taux = l_clone.tkr_n_taux;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtaux_km_reels.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtaux_km_reels.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtaux_km_reels.Delete(this);
    }

    /* Accesseur de la propriete tkr_cn (tkr_cn)
    * @return c_tkr_cn */
    public Int64 tkr_cn
    {
        get{return c_tkr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tkr_cn != value)
                {
                    CreerClone();
                    c_tkr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete vehicules (tkr_veh_cn)
    * @return c_vehicules */
    public clg_vehicules vehicules
    {
        get{return c_vehicules;}
        set
        {
            if(c_vehicules != value)
            {
                CreerClone();
                c_vehicules = value;
            }
        }
    }
    /* Accesseur de la propriete tkr_d_debut (tkr_d_debut)
    * @return c_tkr_d_debut */
    public DateTime tkr_d_debut
    {
        get{return c_tkr_d_debut;}
        set
        {
            if(c_tkr_d_debut != value)
            {
                CreerClone();
                c_tkr_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete tkr_d_fin (tkr_d_fin)
    * @return c_tkr_d_fin */
    public DateTime tkr_d_fin
    {
        get{return c_tkr_d_fin;}
        set
        {
            if(c_tkr_d_fin != value)
            {
                CreerClone();
                c_tkr_d_fin = value;
            }
        }
    }
    /* Accesseur de la propriete tkr_n_taux (tkr_n_taux)
    * @return c_tkr_n_taux */
    public double tkr_n_taux
    {
        get{return c_tkr_n_taux;}
        set
        {
            if(c_tkr_n_taux != value)
            {
                CreerClone();
                c_tkr_n_taux = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type deplacements_realises */
    public List<clg_deplacements_realises> Listedeplacements_realises
    {
        get { return c_Listedeplacements_realises; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tkr_cn.ToString());
		l_Valeurs.Add(c_vehicules==null ? "-1" : c_vehicules.ID.ToString());
		l_Valeurs.Add(c_tkr_d_debut.ToString());
		l_Valeurs.Add(c_tkr_d_fin.ToString());
		l_Valeurs.Add(c_tkr_n_taux.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tkr_cn");
		l_Noms.Add("vehicules");
		l_Noms.Add("tkr_d_debut");
		l_Noms.Add("tkr_d_fin");
		l_Noms.Add("tkr_n_taux");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
