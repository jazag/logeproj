namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_objets_interface </summary>
public partial class clg_t_objets_interface : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_objets_interface;
    protected clg_Modele c_Modele;

	private Int64 c_t_obi_cn;
	private string c_t_obi_a_libel;
	private List<clg_objets_interface> c_Listeobjets_interface;


    private void Init()
    {
		c_Listeobjets_interface = new List<clg_objets_interface>();

    }

    public override void Detruit()
    {
		c_Listeobjets_interface.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_objets_interface(clg_Modele pModele, Int64 pt_obi_cn, bool pAMAJ = false) : base(pModele, pt_obi_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_objets_interface(clg_Modele pModele, Int64 pt_obi_cn, string pt_obi_a_libel, bool pAMAJ = true) : base(pModele, pt_obi_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_obi_cn != Int64.MinValue)
            c_t_obi_cn = pt_obi_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_obi_a_libel != null)
            c_t_obi_a_libel = pt_obi_a_libel;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_obi_cn, string pt_obi_a_libel)
    {   
		        c_t_obi_cn = pt_obi_cn;
		        c_t_obi_a_libel = pt_obi_a_libel;

        base.Initialise(pt_obi_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_objets_interface; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_objets_interface.Dictionnaire.Add(t_obi_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_objets_interface.Dictionnaire.Remove(t_obi_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_objets_interface l_Clone = (clg_t_objets_interface) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeobjets_interface.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_objets_interface(null, t_obi_cn, t_obi_a_libel,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_objets_interface l_clone = (clg_t_objets_interface) this.Clone;
		c_t_obi_cn = l_clone.t_obi_cn;
		c_t_obi_a_libel = l_clone.t_obi_a_libel;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_objets_interface.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_objets_interface.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_objets_interface.Delete(this);
    }

    /* Accesseur de la propriete t_obi_cn (t_obi_cn)
    * @return c_t_obi_cn */
    public Int64 t_obi_cn
    {
        get{return c_t_obi_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_obi_cn != value)
                {
                    CreerClone();
                    c_t_obi_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_obi_a_libel (t_obi_a_libel)
    * @return c_t_obi_a_libel */
    public string t_obi_a_libel
    {
        get{return c_t_obi_a_libel;}
        set
        {
            if(value != null)
            {
                if(c_t_obi_a_libel != value)
                {
                    CreerClone();
                    c_t_obi_a_libel = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type objets_interface */
    public List<clg_objets_interface> Listeobjets_interface
    {
        get { return c_Listeobjets_interface; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_obi_cn.ToString());
		l_Valeurs.Add(c_t_obi_a_libel.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_obi_cn");
		l_Noms.Add("t_obi_a_libel");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
