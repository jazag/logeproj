namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : calendrier </summary>
public partial class clg_calendrier : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcalendrier;
    protected clg_Modele c_Modele;

	private DateTime c_cal_d_date;
	private clg_jours c_jours;
	private clg_mensuel c_mensuel;
	private Int64 c_cal_sem_cn;
	private List<clg_conge_prevu> c_Listeconge_prevu;
	private List<clg_conge_realise> c_Listeconge_realise;
	private List<clg_planning> c_Listeplanning;


    private void Init()
    {
		c_Listeconge_prevu = new List<clg_conge_prevu>();
		c_Listeconge_realise = new List<clg_conge_realise>();
		c_Listeplanning = new List<clg_planning>();

    }

    public override void Detruit()
    {
		c_Listeconge_prevu.Clear();
		c_Listeconge_realise.Clear();
		c_Listeplanning.Clear();
		this.c_jours = null;
		this.c_mensuel = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_calendrier(clg_Modele pModele, DateTime pcal_d_date, bool pAMAJ = false) : base(pModele, pcal_d_date, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_calendrier(clg_Modele pModele, DateTime pcal_d_date, clg_jours pjours, clg_mensuel pmensuel, Int64 pcal_sem_cn, bool pAMAJ = true) : base(pModele, pcal_d_date, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcal_d_date != DateTime.MinValue)
            c_cal_d_date = pcal_d_date;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pjours != null)
            c_jours = pjours;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pmensuel != null)
            c_mensuel = pmensuel;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pcal_sem_cn != Int64.MinValue)
            c_cal_sem_cn = pcal_sem_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(DateTime pcal_d_date, Int64 pjours, Int64 pmensuel, Int64 pcal_sem_cn)
    {   
		        c_cal_d_date = pcal_d_date;
		c_dicReferences.Add("jours", pjours);
		c_dicReferences.Add("mensuel", pmensuel);
		        c_cal_sem_cn = pcal_sem_cn;

        base.Initialise(pcal_d_date);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcalendrier; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_jours = (clg_jours) c_ModeleBase.RenvoieObjet(c_dicReferences["jours"], "clg_jours");
		c_mensuel = (clg_mensuel) c_ModeleBase.RenvoieObjet(c_dicReferences["mensuel"], "clg_mensuel");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecalendrier.Dictionnaire.Add(cal_d_date, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecalendrier.Dictionnaire.Remove(cal_d_date);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_jours != null)if(!c_jours.Listecalendrier.Contains(this)) c_jours.Listecalendrier.Add(this);
		if(c_mensuel != null)if(!c_mensuel.Listecalendrier.Contains(this)) c_mensuel.Listecalendrier.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_calendrier l_Clone = (clg_calendrier) Clone;
		if(l_Clone.jours != null)if(l_Clone.jours.Listecalendrier.Contains(this)) l_Clone.jours.Listecalendrier.Remove(this);
		if(l_Clone.mensuel != null)if(l_Clone.mensuel.Listecalendrier.Contains(this)) l_Clone.mensuel.Listecalendrier.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeconge_prevu.Count > 0) c_EstReference = true;
		if(c_Listeconge_realise.Count > 0) c_EstReference = true;
		if(c_Listeplanning.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_calendrier(null, cal_d_date, jours, mensuel, cal_sem_cn,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_calendrier l_clone = (clg_calendrier) this.Clone;
		c_cal_d_date = l_clone.cal_d_date;
		c_jours = l_clone.jours;
		c_mensuel = l_clone.mensuel;
		c_cal_sem_cn = l_clone.cal_sem_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcalendrier.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcalendrier.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcalendrier.Delete(this);
    }

    /* Accesseur de la propriete cal_d_date (cal_d_date)
    * @return c_cal_d_date */
    public DateTime cal_d_date
    {
        get{return c_cal_d_date;}
        set
        {
            if(value != null)
            {
                if(c_cal_d_date != value)
                {
                    CreerClone();
                    c_cal_d_date = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete jours (cal_jour_cn)
    * @return c_jours */
    public clg_jours jours
    {
        get{return c_jours;}
        set
        {
            if(value != null)
            {
                if(c_jours != value)
                {
                    CreerClone();
                    c_jours = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete mensuel (cal_mpa_cn)
    * @return c_mensuel */
    public clg_mensuel mensuel
    {
        get{return c_mensuel;}
        set
        {
            if(value != null)
            {
                if(c_mensuel != value)
                {
                    CreerClone();
                    c_mensuel = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete cal_sem_cn (cal_sem_cn)
    * @return c_cal_sem_cn */
    public Int64 cal_sem_cn
    {
        get{return c_cal_sem_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cal_sem_cn != value)
                {
                    CreerClone();
                    c_cal_sem_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type conge_prevu */
    public List<clg_conge_prevu> Listeconge_prevu
    {
        get { return c_Listeconge_prevu; }
    }    /* Accesseur de la liste des objets de type conge_realise */
    public List<clg_conge_realise> Listeconge_realise
    {
        get { return c_Listeconge_realise; }
    }    /* Accesseur de la liste des objets de type planning */
    public List<clg_planning> Listeplanning
    {
        get { return c_Listeplanning; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cal_d_date.ToString());
		l_Valeurs.Add(c_jours==null ? "-1" : c_jours.ID.ToString());
		l_Valeurs.Add(c_mensuel==null ? "-1" : c_mensuel.ID.ToString());
		l_Valeurs.Add(c_cal_sem_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cal_d_date");
		l_Noms.Add("jours");
		l_Noms.Add("mensuel");
		l_Noms.Add("cal_sem_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
