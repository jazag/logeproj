namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : cpt_ana_exclu </summary>
public partial class clg_cpt_ana_exclu : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcpt_ana_exclu;
    protected clg_Modele c_Modele;

	private int c_cae_cn;
	private string c_cae_a_cpt;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_cpt_ana_exclu(clg_Modele pModele, int pcae_cn, bool pAMAJ = false) : base(pModele, pcae_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_cpt_ana_exclu(clg_Modele pModele, int pcae_cn, string pcae_a_cpt, bool pAMAJ = true) : base(pModele, pcae_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcae_cn != int.MinValue)
            c_cae_cn = pcae_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_cae_a_cpt = pcae_a_cpt;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(int pcae_cn, string pcae_a_cpt)
    {   
		        c_cae_cn = pcae_cn;
		        c_cae_a_cpt = pcae_a_cpt;

        base.Initialise(pcae_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcpt_ana_exclu; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecpt_ana_exclu.Dictionnaire.Add(cae_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecpt_ana_exclu.Dictionnaire.Remove(cae_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_cpt_ana_exclu l_Clone = (clg_cpt_ana_exclu) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_cpt_ana_exclu(null, cae_cn, cae_a_cpt,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_cpt_ana_exclu l_clone = (clg_cpt_ana_exclu) this.Clone;
		c_cae_cn = l_clone.cae_cn;
		c_cae_a_cpt = l_clone.cae_a_cpt;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcpt_ana_exclu.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcpt_ana_exclu.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcpt_ana_exclu.Delete(this);
    }

    /* Accesseur de la propriete cae_cn (cae_cn)
    * @return c_cae_cn */
    public int cae_cn
    {
        get{return c_cae_cn;}
        set
        {
            if(value != int.MinValue)
            {
                if(c_cae_cn != value)
                {
                    CreerClone();
                    c_cae_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete cae_a_cpt (cae_a_cpt)
    * @return c_cae_a_cpt */
    public string cae_a_cpt
    {
        get{return c_cae_a_cpt;}
        set
        {
            if(c_cae_a_cpt != value)
            {
                CreerClone();
                c_cae_a_cpt = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cae_cn.ToString());
		l_Valeurs.Add(c_cae_a_cpt.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cae_cn");
		l_Noms.Add("cae_a_cpt");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
