namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_info </summary>
public partial class clg_t_info : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_info;
    protected clg_Modele c_Modele;

	private Int64 c_tinfo_cn;
	private string c_tinfo_a_nom;
	private string c_tinfo_a_description;
	private string c_s_guid;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_info(clg_Modele pModele, Int64 ptinfo_cn, bool pAMAJ = false) : base(pModele, ptinfo_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_info(clg_Modele pModele, Int64 ptinfo_cn, string ptinfo_a_nom, string ptinfo_a_description, string ps_guid, bool pAMAJ = true) : base(pModele, ptinfo_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptinfo_cn != Int64.MinValue)
            c_tinfo_cn = ptinfo_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_tinfo_a_nom = ptinfo_a_nom;
		        c_tinfo_a_description = ptinfo_a_description;
		        c_s_guid = ps_guid;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptinfo_cn, string ptinfo_a_nom, string ptinfo_a_description, string ps_guid)
    {   
		        c_tinfo_cn = ptinfo_cn;
		        c_tinfo_a_nom = ptinfo_a_nom;
		        c_tinfo_a_description = ptinfo_a_description;
		        c_s_guid = ps_guid;

        base.Initialise(ptinfo_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_info; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_info.Dictionnaire.Add(tinfo_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_info.Dictionnaire.Remove(tinfo_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_info l_Clone = (clg_t_info) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_info(null, tinfo_cn, tinfo_a_nom, tinfo_a_description, s_guid,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_info l_clone = (clg_t_info) this.Clone;
		c_tinfo_cn = l_clone.tinfo_cn;
		c_tinfo_a_nom = l_clone.tinfo_a_nom;
		c_tinfo_a_description = l_clone.tinfo_a_description;
		c_s_guid = l_clone.s_guid;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_info.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_info.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_info.Delete(this);
    }

    /* Accesseur de la propriete tinfo_cn (tinfo_cn)
    * @return c_tinfo_cn */
    public Int64 tinfo_cn
    {
        get{return c_tinfo_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tinfo_cn != value)
                {
                    CreerClone();
                    c_tinfo_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete tinfo_a_nom (tinfo_a_nom)
    * @return c_tinfo_a_nom */
    public string tinfo_a_nom
    {
        get{return c_tinfo_a_nom;}
        set
        {
            if(c_tinfo_a_nom != value)
            {
                CreerClone();
                c_tinfo_a_nom = value;
            }
        }
    }
    /* Accesseur de la propriete tinfo_a_description (tinfo_a_description)
    * @return c_tinfo_a_description */
    public string tinfo_a_description
    {
        get{return c_tinfo_a_description;}
        set
        {
            if(c_tinfo_a_description != value)
            {
                CreerClone();
                c_tinfo_a_description = value;
            }
        }
    }
    /* Accesseur de la propriete s_guid (s_guid)
    * @return c_s_guid */
    public string s_guid
    {
        get{return c_s_guid;}
        set
        {
            if(c_s_guid != value)
            {
                CreerClone();
                c_s_guid = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tinfo_cn.ToString());
		l_Valeurs.Add(c_tinfo_a_nom.ToString());
		l_Valeurs.Add(c_tinfo_a_description.ToString());
		l_Valeurs.Add(c_s_guid.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tinfo_cn");
		l_Noms.Add("tinfo_a_nom");
		l_Noms.Add("tinfo_a_description");
		l_Noms.Add("s_guid");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
