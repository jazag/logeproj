namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : vehicules </summary>
public partial class clg_vehicules : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLvehicules;
    protected clg_Modele c_Modele;

	private Int64 c_veh_cn;
	private string c_veh_a_identifiant;
	private string c_veh_a_matricule;
	private string c_veh_a_description;
	private Int64 c_veh_n_kmmax;
	private string c_veh_a_marque;
	private string c_veh_a_modele;
	private Int64 c_veh_n_archive;
	private Int64 c_veh_n_annee;
	private string c_veh_a_code;
	private List<clg_comment> c_Listecomment;
	private List<clg_deplacements_prevus> c_Listedeplacements_prevus;
	private List<clg_deplacements_realises> c_Listedeplacements_realises;
	private List<clg_taux_km_prevus> c_Listetaux_km_prevus;
	private List<clg_taux_km_reels> c_Listetaux_km_reels;


    private void Init()
    {
		c_Listecomment = new List<clg_comment>();
		c_Listedeplacements_prevus = new List<clg_deplacements_prevus>();
		c_Listedeplacements_realises = new List<clg_deplacements_realises>();
		c_Listetaux_km_prevus = new List<clg_taux_km_prevus>();
		c_Listetaux_km_reels = new List<clg_taux_km_reels>();

    }

    public override void Detruit()
    {
		c_Listecomment.Clear();
		c_Listedeplacements_prevus.Clear();
		c_Listedeplacements_realises.Clear();
		c_Listetaux_km_prevus.Clear();
		c_Listetaux_km_reels.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_vehicules(clg_Modele pModele, Int64 pveh_cn, bool pAMAJ = false) : base(pModele, pveh_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_vehicules(clg_Modele pModele, Int64 pveh_cn, string pveh_a_identifiant, string pveh_a_matricule, string pveh_a_description, Int64 pveh_n_kmmax, string pveh_a_marque, string pveh_a_modele, Int64 pveh_n_archive, Int64 pveh_n_annee, string pveh_a_code, bool pAMAJ = true) : base(pModele, pveh_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pveh_cn != Int64.MinValue)
            c_veh_cn = pveh_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pveh_a_identifiant != null)
            c_veh_a_identifiant = pveh_a_identifiant;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_veh_a_matricule = pveh_a_matricule;
		        c_veh_a_description = pveh_a_description;
		        c_veh_n_kmmax = pveh_n_kmmax;
		        c_veh_a_marque = pveh_a_marque;
		        c_veh_a_modele = pveh_a_modele;
		        c_veh_n_archive = pveh_n_archive;
		        c_veh_n_annee = pveh_n_annee;
		        c_veh_a_code = pveh_a_code;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pveh_cn, string pveh_a_identifiant, string pveh_a_matricule, string pveh_a_description, Int64 pveh_n_kmmax, string pveh_a_marque, string pveh_a_modele, Int64 pveh_n_archive, Int64 pveh_n_annee, string pveh_a_code)
    {   
		        c_veh_cn = pveh_cn;
		        c_veh_a_identifiant = pveh_a_identifiant;
		        c_veh_a_matricule = pveh_a_matricule;
		        c_veh_a_description = pveh_a_description;
		        c_veh_n_kmmax = pveh_n_kmmax;
		        c_veh_a_marque = pveh_a_marque;
		        c_veh_a_modele = pveh_a_modele;
		        c_veh_n_archive = pveh_n_archive;
		        c_veh_n_annee = pveh_n_annee;
		        c_veh_a_code = pveh_a_code;

        base.Initialise(pveh_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLvehicules; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listevehicules.Dictionnaire.Add(veh_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listevehicules.Dictionnaire.Remove(veh_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_vehicules l_Clone = (clg_vehicules) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecomment.Count > 0) c_EstReference = true;
		if(c_Listedeplacements_prevus.Count > 0) c_EstReference = true;
		if(c_Listedeplacements_realises.Count > 0) c_EstReference = true;
		if(c_Listetaux_km_prevus.Count > 0) c_EstReference = true;
		if(c_Listetaux_km_reels.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_vehicules(null, veh_cn, veh_a_identifiant, veh_a_matricule, veh_a_description, veh_n_kmmax, veh_a_marque, veh_a_modele, veh_n_archive, veh_n_annee, veh_a_code,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_vehicules l_clone = (clg_vehicules) this.Clone;
		c_veh_cn = l_clone.veh_cn;
		c_veh_a_identifiant = l_clone.veh_a_identifiant;
		c_veh_a_matricule = l_clone.veh_a_matricule;
		c_veh_a_description = l_clone.veh_a_description;
		c_veh_n_kmmax = l_clone.veh_n_kmmax;
		c_veh_a_marque = l_clone.veh_a_marque;
		c_veh_a_modele = l_clone.veh_a_modele;
		c_veh_n_archive = l_clone.veh_n_archive;
		c_veh_n_annee = l_clone.veh_n_annee;
		c_veh_a_code = l_clone.veh_a_code;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLvehicules.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLvehicules.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLvehicules.Delete(this);
    }

    /* Accesseur de la propriete veh_cn (veh_cn)
    * @return c_veh_cn */
    public Int64 veh_cn
    {
        get{return c_veh_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_veh_cn != value)
                {
                    CreerClone();
                    c_veh_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete veh_a_identifiant (veh_a_identifiant)
    * @return c_veh_a_identifiant */
    public string veh_a_identifiant
    {
        get{return c_veh_a_identifiant;}
        set
        {
            if(value != null)
            {
                if(c_veh_a_identifiant != value)
                {
                    CreerClone();
                    c_veh_a_identifiant = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete veh_a_matricule (veh_a_matricule)
    * @return c_veh_a_matricule */
    public string veh_a_matricule
    {
        get{return c_veh_a_matricule;}
        set
        {
            if(c_veh_a_matricule != value)
            {
                CreerClone();
                c_veh_a_matricule = value;
            }
        }
    }
    /* Accesseur de la propriete veh_a_description (veh_a_description)
    * @return c_veh_a_description */
    public string veh_a_description
    {
        get{return c_veh_a_description;}
        set
        {
            if(c_veh_a_description != value)
            {
                CreerClone();
                c_veh_a_description = value;
            }
        }
    }
    /* Accesseur de la propriete veh_n_kmmax (veh_n_kmmax)
    * @return c_veh_n_kmmax */
    public Int64 veh_n_kmmax
    {
        get{return c_veh_n_kmmax;}
        set
        {
            if(c_veh_n_kmmax != value)
            {
                CreerClone();
                c_veh_n_kmmax = value;
            }
        }
    }
    /* Accesseur de la propriete veh_a_marque (veh_a_marque)
    * @return c_veh_a_marque */
    public string veh_a_marque
    {
        get{return c_veh_a_marque;}
        set
        {
            if(c_veh_a_marque != value)
            {
                CreerClone();
                c_veh_a_marque = value;
            }
        }
    }
    /* Accesseur de la propriete veh_a_modele (veh_a_modele)
    * @return c_veh_a_modele */
    public string veh_a_modele
    {
        get{return c_veh_a_modele;}
        set
        {
            if(c_veh_a_modele != value)
            {
                CreerClone();
                c_veh_a_modele = value;
            }
        }
    }
    /* Accesseur de la propriete veh_n_archive (veh_n_archive)
    * @return c_veh_n_archive */
    public Int64 veh_n_archive
    {
        get{return c_veh_n_archive;}
        set
        {
            if(c_veh_n_archive != value)
            {
                CreerClone();
                c_veh_n_archive = value;
            }
        }
    }
    /* Accesseur de la propriete veh_n_annee (veh_n_annee)
    * @return c_veh_n_annee */
    public Int64 veh_n_annee
    {
        get{return c_veh_n_annee;}
        set
        {
            if(c_veh_n_annee != value)
            {
                CreerClone();
                c_veh_n_annee = value;
            }
        }
    }
    /* Accesseur de la propriete veh_a_code (veh_a_code)
    * @return c_veh_a_code */
    public string veh_a_code
    {
        get{return c_veh_a_code;}
        set
        {
            if(c_veh_a_code != value)
            {
                CreerClone();
                c_veh_a_code = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type comment */
    public List<clg_comment> Listecomment
    {
        get { return c_Listecomment; }
    }    /* Accesseur de la liste des objets de type deplacements_prevus */
    public List<clg_deplacements_prevus> Listedeplacements_prevus
    {
        get { return c_Listedeplacements_prevus; }
    }    /* Accesseur de la liste des objets de type deplacements_realises */
    public List<clg_deplacements_realises> Listedeplacements_realises
    {
        get { return c_Listedeplacements_realises; }
    }    /* Accesseur de la liste des objets de type taux_km_prevus */
    public List<clg_taux_km_prevus> Listetaux_km_prevus
    {
        get { return c_Listetaux_km_prevus; }
    }    /* Accesseur de la liste des objets de type taux_km_reels */
    public List<clg_taux_km_reels> Listetaux_km_reels
    {
        get { return c_Listetaux_km_reels; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_veh_cn.ToString());
		l_Valeurs.Add(c_veh_a_identifiant.ToString());
		l_Valeurs.Add(c_veh_a_matricule.ToString());
		l_Valeurs.Add(c_veh_a_description.ToString());
		l_Valeurs.Add(c_veh_n_kmmax.ToString());
		l_Valeurs.Add(c_veh_a_marque.ToString());
		l_Valeurs.Add(c_veh_a_modele.ToString());
		l_Valeurs.Add(c_veh_n_archive.ToString());
		l_Valeurs.Add(c_veh_n_annee.ToString());
		l_Valeurs.Add(c_veh_a_code.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("veh_cn");
		l_Noms.Add("veh_a_identifiant");
		l_Noms.Add("veh_a_matricule");
		l_Noms.Add("veh_a_description");
		l_Noms.Add("veh_n_kmmax");
		l_Noms.Add("veh_a_marque");
		l_Noms.Add("veh_a_modele");
		l_Noms.Add("veh_n_archive");
		l_Noms.Add("veh_n_annee");
		l_Noms.Add("veh_a_code");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
    }
}
