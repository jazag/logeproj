namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : deplacements_prevus </summary>
public partial class clg_deplacements_prevus : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLdeplacements_prevus;
    protected clg_Modele c_Modele;

	private Int64 c_dep_cn;
	private clg_vehicules c_vehicules;
	private double c_dep_n_nbkm;
	private DateTime c_dep_d_debut;
	private DateTime c_dep_d_fin;
	private double c_dep_n_montant;
	private clg_taux_km_prevus c_taux_km_prevus;
	private clg_elements c_elements;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_vehicules = null;
		this.c_taux_km_prevus = null;
		this.c_elements = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_deplacements_prevus(clg_Modele pModele, Int64 pdep_cn, bool pAMAJ = false) : base(pModele, pdep_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_deplacements_prevus(clg_Modele pModele, Int64 pdep_cn, clg_vehicules pvehicules, double pdep_n_nbkm, DateTime pdep_d_debut, DateTime pdep_d_fin, double pdep_n_montant, clg_taux_km_prevus ptaux_km_prevus, clg_elements pelements, bool pAMAJ = true) : base(pModele, pdep_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pdep_cn != Int64.MinValue)
            c_dep_cn = pdep_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_vehicules = pvehicules;
		        c_dep_n_nbkm = pdep_n_nbkm;
		        c_dep_d_debut = pdep_d_debut;
		        c_dep_d_fin = pdep_d_fin;
		        c_dep_n_montant = pdep_n_montant;
		        c_taux_km_prevus = ptaux_km_prevus;
		        c_elements = pelements;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pdep_cn, Int64 pvehicules, double pdep_n_nbkm, DateTime pdep_d_debut, DateTime pdep_d_fin, double pdep_n_montant, Int64 ptaux_km_prevus, Int64 pelements)
    {   
		        c_dep_cn = pdep_cn;
		c_dicReferences.Add("vehicules", pvehicules);
		        c_dep_n_nbkm = pdep_n_nbkm;
		        c_dep_d_debut = pdep_d_debut;
		        c_dep_d_fin = pdep_d_fin;
		        c_dep_n_montant = pdep_n_montant;
		c_dicReferences.Add("taux_km_prevus", ptaux_km_prevus);
		c_dicReferences.Add("elements", pelements);

        base.Initialise(pdep_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLdeplacements_prevus; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_vehicules = (clg_vehicules) c_ModeleBase.RenvoieObjet(c_dicReferences["vehicules"], "clg_vehicules");
		c_taux_km_prevus = (clg_taux_km_prevus) c_ModeleBase.RenvoieObjet(c_dicReferences["taux_km_prevus"], "clg_taux_km_prevus");
		c_elements = (clg_elements) c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listedeplacements_prevus.Dictionnaire.Add(dep_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listedeplacements_prevus.Dictionnaire.Remove(dep_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_vehicules != null)if(!c_vehicules.Listedeplacements_prevus.Contains(this)) c_vehicules.Listedeplacements_prevus.Add(this);
		if(c_taux_km_prevus != null)if(!c_taux_km_prevus.Listedeplacements_prevus.Contains(this)) c_taux_km_prevus.Listedeplacements_prevus.Add(this);
		if(c_elements != null)if(!c_elements.Listedeplacements_prevus.Contains(this)) c_elements.Listedeplacements_prevus.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_deplacements_prevus l_Clone = (clg_deplacements_prevus) Clone;
		if(l_Clone.vehicules != null)if(l_Clone.vehicules.Listedeplacements_prevus.Contains(this)) l_Clone.vehicules.Listedeplacements_prevus.Remove(this);
		if(l_Clone.taux_km_prevus != null)if(l_Clone.taux_km_prevus.Listedeplacements_prevus.Contains(this)) l_Clone.taux_km_prevus.Listedeplacements_prevus.Remove(this);
		if(l_Clone.elements != null)if(l_Clone.elements.Listedeplacements_prevus.Contains(this)) l_Clone.elements.Listedeplacements_prevus.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_deplacements_prevus(null, dep_cn, vehicules, dep_n_nbkm, dep_d_debut, dep_d_fin, dep_n_montant, taux_km_prevus, elements,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_deplacements_prevus l_clone = (clg_deplacements_prevus) this.Clone;
		c_dep_cn = l_clone.dep_cn;
		c_vehicules = l_clone.vehicules;
		c_dep_n_nbkm = l_clone.dep_n_nbkm;
		c_dep_d_debut = l_clone.dep_d_debut;
		c_dep_d_fin = l_clone.dep_d_fin;
		c_dep_n_montant = l_clone.dep_n_montant;
		c_taux_km_prevus = l_clone.taux_km_prevus;
		c_elements = l_clone.elements;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLdeplacements_prevus.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLdeplacements_prevus.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLdeplacements_prevus.Delete(this);
    }

    /* Accesseur de la propriete dep_cn (dep_cn)
    * @return c_dep_cn */
    public Int64 dep_cn
    {
        get{return c_dep_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_dep_cn != value)
                {
                    CreerClone();
                    c_dep_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete vehicules (dep_veh_cn)
    * @return c_vehicules */
    public clg_vehicules vehicules
    {
        get{return c_vehicules;}
        set
        {
            if(c_vehicules != value)
            {
                CreerClone();
                c_vehicules = value;
            }
        }
    }
    /* Accesseur de la propriete dep_n_nbkm (dep_n_nbkm)
    * @return c_dep_n_nbkm */
    public double dep_n_nbkm
    {
        get{return c_dep_n_nbkm;}
        set
        {
            if(c_dep_n_nbkm != value)
            {
                CreerClone();
                c_dep_n_nbkm = value;
            }
        }
    }
    /* Accesseur de la propriete dep_d_debut (dep_d_debut)
    * @return c_dep_d_debut */
    public DateTime dep_d_debut
    {
        get{return c_dep_d_debut;}
        set
        {
            if(c_dep_d_debut != value)
            {
                CreerClone();
                c_dep_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete dep_d_fin (dep_d_fin)
    * @return c_dep_d_fin */
    public DateTime dep_d_fin
    {
        get{return c_dep_d_fin;}
        set
        {
            if(c_dep_d_fin != value)
            {
                CreerClone();
                c_dep_d_fin = value;
            }
        }
    }
    /* Accesseur de la propriete dep_n_montant (dep_n_montant)
    * @return c_dep_n_montant */
    public double dep_n_montant
    {
        get{return c_dep_n_montant;}
        set
        {
            if(c_dep_n_montant != value)
            {
                CreerClone();
                c_dep_n_montant = value;
            }
        }
    }
    /* Accesseur de la propriete taux_km_prevus (dep_tkp_cn)
    * @return c_taux_km_prevus */
    public clg_taux_km_prevus taux_km_prevus
    {
        get{return c_taux_km_prevus;}
        set
        {
            if(c_taux_km_prevus != value)
            {
                CreerClone();
                c_taux_km_prevus = value;
            }
        }
    }
    /* Accesseur de la propriete elements (dep_ele_cn)
    * @return c_elements */
    public clg_elements elements
    {
        get{return c_elements;}
        set
        {
            if(c_elements != value)
            {
                CreerClone();
                c_elements = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_dep_cn.ToString());
		l_Valeurs.Add(c_vehicules==null ? "-1" : c_vehicules.ID.ToString());
		l_Valeurs.Add(c_dep_n_nbkm.ToString());
		l_Valeurs.Add(c_dep_d_debut.ToString());
		l_Valeurs.Add(c_dep_d_fin.ToString());
		l_Valeurs.Add(c_dep_n_montant.ToString());
		l_Valeurs.Add(c_taux_km_prevus==null ? "-1" : c_taux_km_prevus.ID.ToString());
		l_Valeurs.Add(c_elements==null ? "-1" : c_elements.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("dep_cn");
		l_Noms.Add("vehicules");
		l_Noms.Add("dep_n_nbkm");
		l_Noms.Add("dep_d_debut");
		l_Noms.Add("dep_d_fin");
		l_Noms.Add("dep_n_montant");
		l_Noms.Add("taux_km_prevus");
		l_Noms.Add("elements");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
