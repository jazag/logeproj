namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_rubriques_temps </summary>
public partial class clg_t_rubriques_temps : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_rubriques_temps;
    protected clg_Modele c_Modele;

	private Int64 c_t_rbr_cn;
	private string c_t_rbr_a_lib;
	private Int64 c_t_rbr_n_archive;
	private Int64 c_t_rbr_n_ordre;
	private List<clg_rubriques_temps> c_Listerubriques_temps;


    private void Init()
    {
		c_Listerubriques_temps = new List<clg_rubriques_temps>();

    }

    public override void Detruit()
    {
		c_Listerubriques_temps.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_rubriques_temps(clg_Modele pModele, Int64 pt_rbr_cn, bool pAMAJ = false) : base(pModele, pt_rbr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_rubriques_temps(clg_Modele pModele, Int64 pt_rbr_cn, string pt_rbr_a_lib, Int64 pt_rbr_n_archive, Int64 pt_rbr_n_ordre, bool pAMAJ = true) : base(pModele, pt_rbr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_rbr_cn != Int64.MinValue)
            c_t_rbr_cn = pt_rbr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_rbr_a_lib != null)
            c_t_rbr_a_lib = pt_rbr_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pt_rbr_n_archive != Int64.MinValue)
            c_t_rbr_n_archive = pt_rbr_n_archive;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_rbr_n_ordre != Int64.MinValue)
            c_t_rbr_n_ordre = pt_rbr_n_ordre;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_rbr_cn, string pt_rbr_a_lib, Int64 pt_rbr_n_archive, Int64 pt_rbr_n_ordre)
    {   
		        c_t_rbr_cn = pt_rbr_cn;
		        c_t_rbr_a_lib = pt_rbr_a_lib;
		        c_t_rbr_n_archive = pt_rbr_n_archive;
		        c_t_rbr_n_ordre = pt_rbr_n_ordre;

        base.Initialise(pt_rbr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_rubriques_temps; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_rubriques_temps.Dictionnaire.Add(t_rbr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_rubriques_temps.Dictionnaire.Remove(t_rbr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_rubriques_temps l_Clone = (clg_t_rubriques_temps) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listerubriques_temps.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_rubriques_temps(null, t_rbr_cn, t_rbr_a_lib, t_rbr_n_archive, t_rbr_n_ordre,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_rubriques_temps l_clone = (clg_t_rubriques_temps) this.Clone;
		c_t_rbr_cn = l_clone.t_rbr_cn;
		c_t_rbr_a_lib = l_clone.t_rbr_a_lib;
		c_t_rbr_n_archive = l_clone.t_rbr_n_archive;
		c_t_rbr_n_ordre = l_clone.t_rbr_n_ordre;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_rubriques_temps.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_rubriques_temps.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_rubriques_temps.Delete(this);
    }

    /* Accesseur de la propriete t_rbr_cn (t_rbr_cn)
    * @return c_t_rbr_cn */
    public Int64 t_rbr_cn
    {
        get{return c_t_rbr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_rbr_cn != value)
                {
                    CreerClone();
                    c_t_rbr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_rbr_a_lib (t_rbr_a_lib)
    * @return c_t_rbr_a_lib */
    public string t_rbr_a_lib
    {
        get{return c_t_rbr_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_t_rbr_a_lib != value)
                {
                    CreerClone();
                    c_t_rbr_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_rbr_n_archive (t_rbr_n_archive)
    * @return c_t_rbr_n_archive */
    public Int64 t_rbr_n_archive
    {
        get{return c_t_rbr_n_archive;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_rbr_n_archive != value)
                {
                    CreerClone();
                    c_t_rbr_n_archive = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_rbr_n_ordre (t_rbr_n_ordre)
    * @return c_t_rbr_n_ordre */
    public Int64 t_rbr_n_ordre
    {
        get{return c_t_rbr_n_ordre;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_rbr_n_ordre != value)
                {
                    CreerClone();
                    c_t_rbr_n_ordre = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type rubriques_temps */
    public List<clg_rubriques_temps> Listerubriques_temps
    {
        get { return c_Listerubriques_temps; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_rbr_cn.ToString());
		l_Valeurs.Add(c_t_rbr_a_lib.ToString());
		l_Valeurs.Add(c_t_rbr_n_archive.ToString());
		l_Valeurs.Add(c_t_rbr_n_ordre.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_rbr_cn");
		l_Noms.Add("t_rbr_a_lib");
		l_Noms.Add("t_rbr_n_archive");
		l_Noms.Add("t_rbr_n_ordre");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
