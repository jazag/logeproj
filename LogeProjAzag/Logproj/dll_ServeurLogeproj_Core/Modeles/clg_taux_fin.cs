namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : taux_fin </summary>
public partial class clg_taux_fin : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtaux_fin;
    protected clg_Modele c_Modele;

	private Int64 c_txf_cn;
	private clg_taux c_taux;
	private clg_temps_intervenants c_temps_intervenants;
	private clg_financement_prevus c_financement_prevus;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_taux = null;
		this.c_temps_intervenants = null;
		this.c_financement_prevus = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_taux_fin(clg_Modele pModele, Int64 ptxf_cn, bool pAMAJ = false) : base(pModele, ptxf_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_taux_fin(clg_Modele pModele, Int64 ptxf_cn, clg_taux ptaux, clg_temps_intervenants ptemps_intervenants, clg_financement_prevus pfinancement_prevus, bool pAMAJ = true) : base(pModele, ptxf_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptxf_cn != Int64.MinValue)
            c_txf_cn = ptxf_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_taux = ptaux;
		        c_temps_intervenants = ptemps_intervenants;
		        c_financement_prevus = pfinancement_prevus;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptxf_cn, Int64 ptaux, Int64 ptemps_intervenants, Int64 pfinancement_prevus)
    {   
		        c_txf_cn = ptxf_cn;
		c_dicReferences.Add("taux", ptaux);
		c_dicReferences.Add("temps_intervenants", ptemps_intervenants);
		c_dicReferences.Add("financement_prevus", pfinancement_prevus);

        base.Initialise(ptxf_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtaux_fin; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_taux = (clg_taux) c_ModeleBase.RenvoieObjet(c_dicReferences["taux"], "clg_taux");
		c_temps_intervenants = (clg_temps_intervenants) c_ModeleBase.RenvoieObjet(c_dicReferences["temps_intervenants"], "clg_temps_intervenants");
		c_financement_prevus = (clg_financement_prevus) c_ModeleBase.RenvoieObjet(c_dicReferences["financement_prevus"], "clg_financement_prevus");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetaux_fin.Dictionnaire.Add(txf_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetaux_fin.Dictionnaire.Remove(txf_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_taux != null)if(!c_taux.Listetaux_fin.Contains(this)) c_taux.Listetaux_fin.Add(this);
		if(c_temps_intervenants != null)if(!c_temps_intervenants.Listetaux_fin.Contains(this)) c_temps_intervenants.Listetaux_fin.Add(this);
		if(c_financement_prevus != null)if(!c_financement_prevus.Listetaux_fin.Contains(this)) c_financement_prevus.Listetaux_fin.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_taux_fin l_Clone = (clg_taux_fin) Clone;
		if(l_Clone.taux != null)if(l_Clone.taux.Listetaux_fin.Contains(this)) l_Clone.taux.Listetaux_fin.Remove(this);
		if(l_Clone.temps_intervenants != null)if(l_Clone.temps_intervenants.Listetaux_fin.Contains(this)) l_Clone.temps_intervenants.Listetaux_fin.Remove(this);
		if(l_Clone.financement_prevus != null)if(l_Clone.financement_prevus.Listetaux_fin.Contains(this)) l_Clone.financement_prevus.Listetaux_fin.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_taux_fin(null, txf_cn, taux, temps_intervenants, financement_prevus,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_taux_fin l_clone = (clg_taux_fin) this.Clone;
		c_txf_cn = l_clone.txf_cn;
		c_taux = l_clone.taux;
		c_temps_intervenants = l_clone.temps_intervenants;
		c_financement_prevus = l_clone.financement_prevus;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtaux_fin.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtaux_fin.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtaux_fin.Delete(this);
    }

    /* Accesseur de la propriete txf_cn (txf_cn)
    * @return c_txf_cn */
    public Int64 txf_cn
    {
        get{return c_txf_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_txf_cn != value)
                {
                    CreerClone();
                    c_txf_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete taux (txf_tx_cn)
    * @return c_taux */
    public clg_taux taux
    {
        get{return c_taux;}
        set
        {
            if(c_taux != value)
            {
                CreerClone();
                c_taux = value;
            }
        }
    }
    /* Accesseur de la propriete temps_intervenants (txf_tit_cn)
    * @return c_temps_intervenants */
    public clg_temps_intervenants temps_intervenants
    {
        get{return c_temps_intervenants;}
        set
        {
            if(c_temps_intervenants != value)
            {
                CreerClone();
                c_temps_intervenants = value;
            }
        }
    }
    /* Accesseur de la propriete financement_prevus (txf_fnp_cn)
    * @return c_financement_prevus */
    public clg_financement_prevus financement_prevus
    {
        get{return c_financement_prevus;}
        set
        {
            if(c_financement_prevus != value)
            {
                CreerClone();
                c_financement_prevus = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_txf_cn.ToString());
		l_Valeurs.Add(c_taux==null ? "-1" : c_taux.ID.ToString());
		l_Valeurs.Add(c_temps_intervenants==null ? "-1" : c_temps_intervenants.ID.ToString());
		l_Valeurs.Add(c_financement_prevus==null ? "-1" : c_financement_prevus.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("txf_cn");
		l_Noms.Add("taux");
		l_Noms.Add("temps_intervenants");
		l_Noms.Add("financement_prevus");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
