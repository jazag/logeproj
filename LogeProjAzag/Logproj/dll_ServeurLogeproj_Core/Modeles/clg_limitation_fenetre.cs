namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : limitation_fenetre </summary>
public partial class clg_limitation_fenetre : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLlimitation_fenetre;
    protected clg_Modele c_Modele;

	private Int64 c_lmf_cn;
	private clg_fenetre c_fenetre;
	private clg_groupes c_groupes;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_fenetre = null;
		this.c_groupes = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_limitation_fenetre(clg_Modele pModele, Int64 plmf_cn, bool pAMAJ = false) : base(pModele, plmf_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_limitation_fenetre(clg_Modele pModele, Int64 plmf_cn, clg_fenetre pfenetre, clg_groupes pgroupes, bool pAMAJ = true) : base(pModele, plmf_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(plmf_cn != Int64.MinValue)
            c_lmf_cn = plmf_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pfenetre != null)
            c_fenetre = pfenetre;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pgroupes != null)
            c_groupes = pgroupes;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 plmf_cn, Int64 pfenetre, Int64 pgroupes)
    {   
		        c_lmf_cn = plmf_cn;
		c_dicReferences.Add("fenetre", pfenetre);
		c_dicReferences.Add("groupes", pgroupes);

        base.Initialise(plmf_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLlimitation_fenetre; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_fenetre = (clg_fenetre) c_ModeleBase.RenvoieObjet(c_dicReferences["fenetre"], "clg_fenetre");
		c_groupes = (clg_groupes) c_ModeleBase.RenvoieObjet(c_dicReferences["groupes"], "clg_groupes");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listelimitation_fenetre.Dictionnaire.Add(lmf_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listelimitation_fenetre.Dictionnaire.Remove(lmf_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_fenetre != null)if(!c_fenetre.Listelimitation_fenetre.Contains(this)) c_fenetre.Listelimitation_fenetre.Add(this);
		if(c_groupes != null)if(!c_groupes.Listelimitation_fenetre.Contains(this)) c_groupes.Listelimitation_fenetre.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_limitation_fenetre l_Clone = (clg_limitation_fenetre) Clone;
		if(l_Clone.fenetre != null)if(l_Clone.fenetre.Listelimitation_fenetre.Contains(this)) l_Clone.fenetre.Listelimitation_fenetre.Remove(this);
		if(l_Clone.groupes != null)if(l_Clone.groupes.Listelimitation_fenetre.Contains(this)) l_Clone.groupes.Listelimitation_fenetre.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_limitation_fenetre(null, lmf_cn, fenetre, groupes,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_limitation_fenetre l_clone = (clg_limitation_fenetre) this.Clone;
		c_lmf_cn = l_clone.lmf_cn;
		c_fenetre = l_clone.fenetre;
		c_groupes = l_clone.groupes;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLlimitation_fenetre.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLlimitation_fenetre.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLlimitation_fenetre.Delete(this);
    }

    /* Accesseur de la propriete lmf_cn (lmf_cn)
    * @return c_lmf_cn */
    public Int64 lmf_cn
    {
        get{return c_lmf_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_lmf_cn != value)
                {
                    CreerClone();
                    c_lmf_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete fenetre (lmf_fen_cn)
    * @return c_fenetre */
    public clg_fenetre fenetre
    {
        get{return c_fenetre;}
        set
        {
            if(value != null)
            {
                if(c_fenetre != value)
                {
                    CreerClone();
                    c_fenetre = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete groupes (lmf_grp_cn)
    * @return c_groupes */
    public clg_groupes groupes
    {
        get{return c_groupes;}
        set
        {
            if(value != null)
            {
                if(c_groupes != value)
                {
                    CreerClone();
                    c_groupes = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_lmf_cn.ToString());
		l_Valeurs.Add(c_fenetre==null ? "-1" : c_fenetre.ID.ToString());
		l_Valeurs.Add(c_groupes==null ? "-1" : c_groupes.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("lmf_cn");
		l_Noms.Add("fenetre");
		l_Noms.Add("groupes");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
