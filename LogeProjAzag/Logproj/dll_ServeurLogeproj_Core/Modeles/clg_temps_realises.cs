namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : temps_realises </summary>
public partial class clg_temps_realises : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtemps_realises;
    protected clg_Modele c_Modele;

	private Int64 c_tpr_cn;
	private double c_tpr_n_heures;
	private DateTime c_tpr_d_date;
	private string c_cle_ele_t_fct;
	private clg_temps_intervenants c_temps_intervenants;
	private List<clg_c_qualif_temps> c_Listec_qualif_temps;


    private void Init()
    {
		c_Listec_qualif_temps = new List<clg_c_qualif_temps>();

    }

    public override void Detruit()
    {
		c_Listec_qualif_temps.Clear();
		this.c_temps_intervenants = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_temps_realises(clg_Modele pModele, Int64 ptpr_cn, bool pAMAJ = false) : base(pModele, ptpr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_temps_realises(clg_Modele pModele, Int64 ptpr_cn, double ptpr_n_heures, DateTime ptpr_d_date, string pcle_ele_t_fct, clg_temps_intervenants ptemps_intervenants, bool pAMAJ = true) : base(pModele, ptpr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptpr_cn != Int64.MinValue)
            c_tpr_cn = ptpr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_tpr_n_heures = ptpr_n_heures;
		        if(ptpr_d_date != DateTime.MinValue)
            c_tpr_d_date = ptpr_d_date;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_cle_ele_t_fct = pcle_ele_t_fct;
		        c_temps_intervenants = ptemps_intervenants;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptpr_cn, double ptpr_n_heures, DateTime ptpr_d_date, string pcle_ele_t_fct, Int64 ptemps_intervenants)
    {   
		        c_tpr_cn = ptpr_cn;
		        c_tpr_n_heures = ptpr_n_heures;
		        c_tpr_d_date = ptpr_d_date;
		        c_cle_ele_t_fct = pcle_ele_t_fct;
		c_dicReferences.Add("temps_intervenants", ptemps_intervenants);

        base.Initialise(ptpr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtemps_realises; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_temps_intervenants = (clg_temps_intervenants) c_ModeleBase.RenvoieObjet(c_dicReferences["temps_intervenants"], "clg_temps_intervenants");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetemps_realises.Dictionnaire.Add(tpr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetemps_realises.Dictionnaire.Remove(tpr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_temps_intervenants != null)if(!c_temps_intervenants.Listetemps_realises.Contains(this)) c_temps_intervenants.Listetemps_realises.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_temps_realises l_Clone = (clg_temps_realises) Clone;
		if(l_Clone.temps_intervenants != null)if(l_Clone.temps_intervenants.Listetemps_realises.Contains(this)) l_Clone.temps_intervenants.Listetemps_realises.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listec_qualif_temps.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_temps_realises(null, tpr_cn, tpr_n_heures, tpr_d_date, cle_ele_t_fct, temps_intervenants,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_temps_realises l_clone = (clg_temps_realises) this.Clone;
		c_tpr_cn = l_clone.tpr_cn;
		c_tpr_n_heures = l_clone.tpr_n_heures;
		c_tpr_d_date = l_clone.tpr_d_date;
		c_cle_ele_t_fct = l_clone.cle_ele_t_fct;
		c_temps_intervenants = l_clone.temps_intervenants;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtemps_realises.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtemps_realises.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtemps_realises.Delete(this);
    }

    /* Accesseur de la propriete tpr_cn (tpr_cn)
    * @return c_tpr_cn */
    public Int64 tpr_cn
    {
        get{return c_tpr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tpr_cn != value)
                {
                    CreerClone();
                    c_tpr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete tpr_n_heures (tpr_n_heures)
    * @return c_tpr_n_heures */
    public double tpr_n_heures
    {
        get{return c_tpr_n_heures;}
        set
        {
            if(c_tpr_n_heures != value)
            {
                CreerClone();
                c_tpr_n_heures = value;
            }
        }
    }
    /* Accesseur de la propriete tpr_d_date (tpr_d_date)
    * @return c_tpr_d_date */
    public DateTime tpr_d_date
    {
        get{return c_tpr_d_date;}
        set
        {
            if(value != null)
            {
                if(c_tpr_d_date != value)
                {
                    CreerClone();
                    c_tpr_d_date = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete cle_ele_t_fct (cle_ele_t_fct)
    * @return c_cle_ele_t_fct */
    public string cle_ele_t_fct
    {
        get{return c_cle_ele_t_fct;}
        set
        {
            if(c_cle_ele_t_fct != value)
            {
                CreerClone();
                c_cle_ele_t_fct = value;
            }
        }
    }
    /* Accesseur de la propriete temps_intervenants (tpr_tit_cn)
    * @return c_temps_intervenants */
    public clg_temps_intervenants temps_intervenants
    {
        get{return c_temps_intervenants;}
        set
        {
            if(c_temps_intervenants != value)
            {
                CreerClone();
                c_temps_intervenants = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type c_qualif_temps */
    public List<clg_c_qualif_temps> Listec_qualif_temps
    {
        get { return c_Listec_qualif_temps; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tpr_cn.ToString());
		l_Valeurs.Add(c_tpr_n_heures.ToString());
		l_Valeurs.Add(c_tpr_d_date.ToString());
		l_Valeurs.Add(c_cle_ele_t_fct.ToString());
		l_Valeurs.Add(c_temps_intervenants==null ? "-1" : c_temps_intervenants.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tpr_cn");
		l_Noms.Add("tpr_n_heures");
		l_Noms.Add("tpr_d_date");
		l_Noms.Add("cle_ele_t_fct");
		l_Noms.Add("temps_intervenants");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
