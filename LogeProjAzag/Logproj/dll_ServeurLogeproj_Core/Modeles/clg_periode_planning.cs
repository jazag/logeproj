namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : periode_planning </summary>
public partial class clg_periode_planning : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLperiode_planning;
    protected clg_Modele c_Modele;

	private Int64 c_ppl_cn;
	private string c_ppl_a_lib;
	private DateTime c_ppl_d_debut;
	private DateTime c_ppl_d_fin;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_periode_planning(clg_Modele pModele, Int64 pppl_cn, bool pAMAJ = false) : base(pModele, pppl_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_periode_planning(clg_Modele pModele, Int64 pppl_cn, string pppl_a_lib, DateTime pppl_d_debut, DateTime pppl_d_fin, bool pAMAJ = true) : base(pModele, pppl_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pppl_cn != Int64.MinValue)
            c_ppl_cn = pppl_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ppl_a_lib = pppl_a_lib;
		        c_ppl_d_debut = pppl_d_debut;
		        c_ppl_d_fin = pppl_d_fin;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pppl_cn, string pppl_a_lib, DateTime pppl_d_debut, DateTime pppl_d_fin)
    {   
		        c_ppl_cn = pppl_cn;
		        c_ppl_a_lib = pppl_a_lib;
		        c_ppl_d_debut = pppl_d_debut;
		        c_ppl_d_fin = pppl_d_fin;

        base.Initialise(pppl_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLperiode_planning; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeperiode_planning.Dictionnaire.Add(ppl_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeperiode_planning.Dictionnaire.Remove(ppl_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_periode_planning l_Clone = (clg_periode_planning) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_periode_planning(null, ppl_cn, ppl_a_lib, ppl_d_debut, ppl_d_fin,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_periode_planning l_clone = (clg_periode_planning) this.Clone;
		c_ppl_cn = l_clone.ppl_cn;
		c_ppl_a_lib = l_clone.ppl_a_lib;
		c_ppl_d_debut = l_clone.ppl_d_debut;
		c_ppl_d_fin = l_clone.ppl_d_fin;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLperiode_planning.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLperiode_planning.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLperiode_planning.Delete(this);
    }

    /* Accesseur de la propriete ppl_cn (ppl_cn)
    * @return c_ppl_cn */
    public Int64 ppl_cn
    {
        get{return c_ppl_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ppl_cn != value)
                {
                    CreerClone();
                    c_ppl_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ppl_a_lib (ppl_a_lib)
    * @return c_ppl_a_lib */
    public string ppl_a_lib
    {
        get{return c_ppl_a_lib;}
        set
        {
            if(c_ppl_a_lib != value)
            {
                CreerClone();
                c_ppl_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete ppl_d_debut (ppl_d_debut)
    * @return c_ppl_d_debut */
    public DateTime ppl_d_debut
    {
        get{return c_ppl_d_debut;}
        set
        {
            if(c_ppl_d_debut != value)
            {
                CreerClone();
                c_ppl_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete ppl_d_fin (ppl_d_fin)
    * @return c_ppl_d_fin */
    public DateTime ppl_d_fin
    {
        get{return c_ppl_d_fin;}
        set
        {
            if(c_ppl_d_fin != value)
            {
                CreerClone();
                c_ppl_d_fin = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ppl_cn.ToString());
		l_Valeurs.Add(c_ppl_a_lib.ToString());
		l_Valeurs.Add(c_ppl_d_debut.ToString());
		l_Valeurs.Add(c_ppl_d_fin.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ppl_cn");
		l_Noms.Add("ppl_a_lib");
		l_Noms.Add("ppl_d_debut");
		l_Noms.Add("ppl_d_fin");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
