namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : etat_arrete </summary>
public partial class clg_etat_arrete : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLetat_arrete;
    protected clg_Modele c_Modele;

	private Int64 c_eta_cn;
	private string c_eta_a_lib;
	private string c_eta_a_color;
	private List<clg_arretes> c_Listearretes;


    private void Init()
    {
		c_Listearretes = new List<clg_arretes>();

    }

    public override void Detruit()
    {
		c_Listearretes.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_etat_arrete(clg_Modele pModele, Int64 peta_cn, bool pAMAJ = false) : base(pModele, peta_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_etat_arrete(clg_Modele pModele, Int64 peta_cn, string peta_a_lib, string peta_a_color, bool pAMAJ = true) : base(pModele, peta_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(peta_cn != Int64.MinValue)
            c_eta_cn = peta_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_eta_a_lib = peta_a_lib;
		        c_eta_a_color = peta_a_color;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 peta_cn, string peta_a_lib, string peta_a_color)
    {   
		        c_eta_cn = peta_cn;
		        c_eta_a_lib = peta_a_lib;
		        c_eta_a_color = peta_a_color;

        base.Initialise(peta_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLetat_arrete; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeetat_arrete.Dictionnaire.Add(eta_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeetat_arrete.Dictionnaire.Remove(eta_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_etat_arrete l_Clone = (clg_etat_arrete) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listearretes.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_etat_arrete(null, eta_cn, eta_a_lib, eta_a_color,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_etat_arrete l_clone = (clg_etat_arrete) this.Clone;
		c_eta_cn = l_clone.eta_cn;
		c_eta_a_lib = l_clone.eta_a_lib;
		c_eta_a_color = l_clone.eta_a_color;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLetat_arrete.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLetat_arrete.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLetat_arrete.Delete(this);
    }

    /* Accesseur de la propriete eta_cn (eta_cn)
    * @return c_eta_cn */
    public Int64 eta_cn
    {
        get{return c_eta_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_eta_cn != value)
                {
                    CreerClone();
                    c_eta_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete eta_a_lib (eta_a_lib)
    * @return c_eta_a_lib */
    public string eta_a_lib
    {
        get{return c_eta_a_lib;}
        set
        {
            if(c_eta_a_lib != value)
            {
                CreerClone();
                c_eta_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete eta_a_color (eta_a_color)
    * @return c_eta_a_color */
    public string eta_a_color
    {
        get{return c_eta_a_color;}
        set
        {
            if(c_eta_a_color != value)
            {
                CreerClone();
                c_eta_a_color = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type arretes */
    public List<clg_arretes> Listearretes
    {
        get { return c_Listearretes; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_eta_cn.ToString());
		l_Valeurs.Add(c_eta_a_lib.ToString());
		l_Valeurs.Add(c_eta_a_color.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("eta_cn");
		l_Noms.Add("eta_a_lib");
		l_Noms.Add("eta_a_color");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
