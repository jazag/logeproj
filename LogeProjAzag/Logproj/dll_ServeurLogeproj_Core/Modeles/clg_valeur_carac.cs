namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : valeur_carac </summary>
public partial class clg_valeur_carac : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLvaleur_carac;
    protected clg_Modele c_Modele;

	private Int64 c_lcr_cn;
	private clg_carac c_carac;
	private string c_lcr_a_lib;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_carac = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_valeur_carac(clg_Modele pModele, Int64 plcr_cn, bool pAMAJ = false) : base(pModele, plcr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_valeur_carac(clg_Modele pModele, Int64 plcr_cn, clg_carac pcarac, string plcr_a_lib, bool pAMAJ = true) : base(pModele, plcr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(plcr_cn != Int64.MinValue)
            c_lcr_cn = plcr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pcarac != null)
            c_carac = pcarac;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_lcr_a_lib = plcr_a_lib;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 plcr_cn, Int64 pcarac, string plcr_a_lib)
    {   
		        c_lcr_cn = plcr_cn;
		c_dicReferences.Add("carac", pcarac);
		        c_lcr_a_lib = plcr_a_lib;

        base.Initialise(plcr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLvaleur_carac; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_carac = (clg_carac) c_ModeleBase.RenvoieObjet(c_dicReferences["carac"], "clg_carac");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listevaleur_carac.Dictionnaire.Add(lcr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listevaleur_carac.Dictionnaire.Remove(lcr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_carac != null)if(!c_carac.Listevaleur_carac.Contains(this)) c_carac.Listevaleur_carac.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_valeur_carac l_Clone = (clg_valeur_carac) Clone;
		if(l_Clone.carac != null)if(l_Clone.carac.Listevaleur_carac.Contains(this)) l_Clone.carac.Listevaleur_carac.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_valeur_carac(null, lcr_cn, carac, lcr_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_valeur_carac l_clone = (clg_valeur_carac) this.Clone;
		c_lcr_cn = l_clone.lcr_cn;
		c_carac = l_clone.carac;
		c_lcr_a_lib = l_clone.lcr_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLvaleur_carac.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLvaleur_carac.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLvaleur_carac.Delete(this);
    }

    /* Accesseur de la propriete lcr_cn (lcr_cn)
    * @return c_lcr_cn */
    public Int64 lcr_cn
    {
        get{return c_lcr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_lcr_cn != value)
                {
                    CreerClone();
                    c_lcr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete carac (lcr_car_cn)
    * @return c_carac */
    public clg_carac carac
    {
        get{return c_carac;}
        set
        {
            if(value != null)
            {
                if(c_carac != value)
                {
                    CreerClone();
                    c_carac = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete lcr_a_lib (lcr_a_lib)
    * @return c_lcr_a_lib */
    public string lcr_a_lib
    {
        get{return c_lcr_a_lib;}
        set
        {
            if(c_lcr_a_lib != value)
            {
                CreerClone();
                c_lcr_a_lib = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_lcr_cn.ToString());
		l_Valeurs.Add(c_carac==null ? "-1" : c_carac.ID.ToString());
		l_Valeurs.Add(c_lcr_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("lcr_cn");
		l_Noms.Add("carac");
		l_Noms.Add("lcr_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
