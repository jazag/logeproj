namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : carac </summary>
public partial class clg_carac : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcarac;
    protected clg_Modele c_Modele;

	private Int64 c_car_cn;
	private string c_car_a_lib;
	private string c_car_a_desc;
	private clg_format_carac c_format_carac;
	private List<clg_carac_element> c_Listecarac_element;
	private List<clg_t_element> c_Listet_element;
	private List<clg_valeur_carac> c_Listevaleur_carac;


    private void Init()
    {
		c_Listecarac_element = new List<clg_carac_element>();
		c_Listet_element = new List<clg_t_element>();
		c_Listevaleur_carac = new List<clg_valeur_carac>();

    }

    public override void Detruit()
    {
		c_Listecarac_element.Clear();
		c_Listet_element.Clear();
		c_Listevaleur_carac.Clear();
		this.c_format_carac = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_carac(clg_Modele pModele, Int64 pcar_cn, bool pAMAJ = false) : base(pModele, pcar_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_carac(clg_Modele pModele, Int64 pcar_cn, string pcar_a_lib, string pcar_a_desc, clg_format_carac pformat_carac, bool pAMAJ = true) : base(pModele, pcar_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcar_cn != Int64.MinValue)
            c_car_cn = pcar_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pcar_a_lib != null)
            c_car_a_lib = pcar_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_car_a_desc = pcar_a_desc;
		        if(pformat_carac != null)
            c_format_carac = pformat_carac;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcar_cn, string pcar_a_lib, string pcar_a_desc, Int64 pformat_carac)
    {   
		        c_car_cn = pcar_cn;
		        c_car_a_lib = pcar_a_lib;
		        c_car_a_desc = pcar_a_desc;
		c_dicReferences.Add("format_carac", pformat_carac);

        base.Initialise(pcar_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcarac; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_format_carac = (clg_format_carac) c_ModeleBase.RenvoieObjet(c_dicReferences["format_carac"], "clg_format_carac");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecarac.Dictionnaire.Add(car_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecarac.Dictionnaire.Remove(car_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_format_carac != null)if(!c_format_carac.Listecarac.Contains(this)) c_format_carac.Listecarac.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_carac l_Clone = (clg_carac) Clone;
		if(l_Clone.format_carac != null)if(l_Clone.format_carac.Listecarac.Contains(this)) l_Clone.format_carac.Listecarac.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecarac_element.Count > 0) c_EstReference = true;
		if(c_Listet_element.Count > 0) c_EstReference = true;
		if(c_Listevaleur_carac.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_carac(null, car_cn, car_a_lib, car_a_desc, format_carac,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_carac l_clone = (clg_carac) this.Clone;
		c_car_cn = l_clone.car_cn;
		c_car_a_lib = l_clone.car_a_lib;
		c_car_a_desc = l_clone.car_a_desc;
		c_format_carac = l_clone.format_carac;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcarac.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcarac.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcarac.Delete(this);
    }

    /* Accesseur de la propriete car_cn (car_cn)
    * @return c_car_cn */
    public Int64 car_cn
    {
        get{return c_car_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_car_cn != value)
                {
                    CreerClone();
                    c_car_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete car_a_lib (car_a_lib)
    * @return c_car_a_lib */
    public string car_a_lib
    {
        get{return c_car_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_car_a_lib != value)
                {
                    CreerClone();
                    c_car_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete car_a_desc (car_a_desc)
    * @return c_car_a_desc */
    public string car_a_desc
    {
        get{return c_car_a_desc;}
        set
        {
            if(c_car_a_desc != value)
            {
                CreerClone();
                c_car_a_desc = value;
            }
        }
    }
    /* Accesseur de la propriete format_carac (car_f_car_cn)
    * @return c_format_carac */
    public clg_format_carac format_carac
    {
        get{return c_format_carac;}
        set
        {
            if(value != null)
            {
                if(c_format_carac != value)
                {
                    CreerClone();
                    c_format_carac = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type carac_element */
    public List<clg_carac_element> Listecarac_element
    {
        get { return c_Listecarac_element; }
    }    /* Accesseur de la liste des objets de type t_element */
    public List<clg_t_element> Listet_element
    {
        get { return c_Listet_element; }
    }    /* Accesseur de la liste des objets de type valeur_carac */
    public List<clg_valeur_carac> Listevaleur_carac
    {
        get { return c_Listevaleur_carac; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_car_cn.ToString());
		l_Valeurs.Add(c_car_a_lib.ToString());
		l_Valeurs.Add(c_car_a_desc.ToString());
		l_Valeurs.Add(c_format_carac==null ? "-1" : c_format_carac.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("car_cn");
		l_Noms.Add("car_a_lib");
		l_Noms.Add("car_a_desc");
		l_Noms.Add("format_carac");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
