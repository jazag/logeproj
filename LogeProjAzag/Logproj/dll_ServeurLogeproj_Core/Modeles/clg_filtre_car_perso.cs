namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : filtre_car_perso </summary>
public partial class clg_filtre_car_perso : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfiltre_car_perso;
    protected clg_Modele c_Modele;

	private Int64 c_fcp_per_cn;
	private Int64 c_fcp_car_cn;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_filtre_car_perso(clg_Modele pModele, Int64 pfcp_per_cn, bool pAMAJ = false) : base(pModele, pfcp_per_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_filtre_car_perso(clg_Modele pModele, Int64 pfcp_per_cn, Int64 pfcp_car_cn, bool pAMAJ = true) : base(pModele, pfcp_per_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfcp_per_cn != Int64.MinValue)
            c_fcp_per_cn = pfcp_per_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pfcp_car_cn != Int64.MinValue)
            c_fcp_car_cn = pfcp_car_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfcp_per_cn, Int64 pfcp_car_cn)
    {   
		        c_fcp_per_cn = pfcp_per_cn;
		        c_fcp_car_cn = pfcp_car_cn;

        base.Initialise(pfcp_per_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfiltre_car_perso; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefiltre_car_perso.Dictionnaire.Add(fcp_per_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefiltre_car_perso.Dictionnaire.Remove(fcp_per_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_filtre_car_perso l_Clone = (clg_filtre_car_perso) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_filtre_car_perso(null, fcp_per_cn, fcp_car_cn,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_filtre_car_perso l_clone = (clg_filtre_car_perso) this.Clone;
		c_fcp_per_cn = l_clone.fcp_per_cn;
		c_fcp_car_cn = l_clone.fcp_car_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfiltre_car_perso.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfiltre_car_perso.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfiltre_car_perso.Delete(this);
    }

    /* Accesseur de la propriete fcp_per_cn (fcp_per_cn)
    * @return c_fcp_per_cn */
    public Int64 fcp_per_cn
    {
        get{return c_fcp_per_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_fcp_per_cn != value)
                {
                    CreerClone();
                    c_fcp_per_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete fcp_car_cn (fcp_car_cn)
    * @return c_fcp_car_cn */
    public Int64 fcp_car_cn
    {
        get{return c_fcp_car_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_fcp_car_cn != value)
                {
                    CreerClone();
                    c_fcp_car_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_fcp_per_cn.ToString());
		l_Valeurs.Add(c_fcp_car_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("fcp_per_cn");
		l_Noms.Add("fcp_car_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
