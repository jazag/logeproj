namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : forfait </summary>
public partial class clg_forfait : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLforfait;
    protected clg_Modele c_Modele;

	private Int64 c_frf_cn;
	private string c_frf_a_lib;
	private List<clg_convention> c_Listeconvention;
	private List<clg_forfait_personnel> c_Listeforfait_personnel;


    private void Init()
    {
		c_Listeconvention = new List<clg_convention>();
		c_Listeforfait_personnel = new List<clg_forfait_personnel>();

    }

    public override void Detruit()
    {
		c_Listeconvention.Clear();
		c_Listeforfait_personnel.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_forfait(clg_Modele pModele, Int64 pfrf_cn, bool pAMAJ = false) : base(pModele, pfrf_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_forfait(clg_Modele pModele, Int64 pfrf_cn, string pfrf_a_lib, bool pAMAJ = true) : base(pModele, pfrf_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfrf_cn != Int64.MinValue)
            c_frf_cn = pfrf_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_frf_a_lib = pfrf_a_lib;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfrf_cn, string pfrf_a_lib)
    {   
		        c_frf_cn = pfrf_cn;
		        c_frf_a_lib = pfrf_a_lib;

        base.Initialise(pfrf_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLforfait; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeforfait.Dictionnaire.Add(frf_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeforfait.Dictionnaire.Remove(frf_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_forfait l_Clone = (clg_forfait) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeconvention.Count > 0) c_EstReference = true;
		if(c_Listeforfait_personnel.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_forfait(null, frf_cn, frf_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_forfait l_clone = (clg_forfait) this.Clone;
		c_frf_cn = l_clone.frf_cn;
		c_frf_a_lib = l_clone.frf_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLforfait.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLforfait.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLforfait.Delete(this);
    }

    /* Accesseur de la propriete frf_cn (frf_cn)
    * @return c_frf_cn */
    public Int64 frf_cn
    {
        get{return c_frf_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_frf_cn != value)
                {
                    CreerClone();
                    c_frf_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete frf_a_lib (frf_a_lib)
    * @return c_frf_a_lib */
    public string frf_a_lib
    {
        get{return c_frf_a_lib;}
        set
        {
            if(c_frf_a_lib != value)
            {
                CreerClone();
                c_frf_a_lib = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type convention */
    public List<clg_convention> Listeconvention
    {
        get { return c_Listeconvention; }
    }    /* Accesseur de la liste des objets de type forfait_personnel */
    public List<clg_forfait_personnel> Listeforfait_personnel
    {
        get { return c_Listeforfait_personnel; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_frf_cn.ToString());
		l_Valeurs.Add(c_frf_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("frf_cn");
		l_Noms.Add("frf_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
