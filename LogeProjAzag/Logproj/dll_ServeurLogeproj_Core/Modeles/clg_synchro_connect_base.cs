namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : synchro_connect_base </summary>
public partial class clg_synchro_connect_base : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLsynchro_connect_base;
    protected clg_Modele c_Modele;

	private Int64 c_sbs_cn;
	private string c_sbs_a_ip;
	private DateTime c_sbs_d_date_heure;
	private Int64 c_sbs_n_tick;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_synchro_connect_base(clg_Modele pModele, Int64 psbs_cn, bool pAMAJ = false) : base(pModele, psbs_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_synchro_connect_base(clg_Modele pModele, Int64 psbs_cn, string psbs_a_ip, DateTime psbs_d_date_heure, Int64 psbs_n_tick, bool pAMAJ = true) : base(pModele, psbs_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(psbs_cn != Int64.MinValue)
            c_sbs_cn = psbs_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_sbs_a_ip = psbs_a_ip;
		        c_sbs_d_date_heure = psbs_d_date_heure;
		        c_sbs_n_tick = psbs_n_tick;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 psbs_cn, string psbs_a_ip, DateTime psbs_d_date_heure, Int64 psbs_n_tick)
    {   
		        c_sbs_cn = psbs_cn;
		        c_sbs_a_ip = psbs_a_ip;
		        c_sbs_d_date_heure = psbs_d_date_heure;
		        c_sbs_n_tick = psbs_n_tick;

        base.Initialise(psbs_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLsynchro_connect_base; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listesynchro_connect_base.Dictionnaire.Add(sbs_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listesynchro_connect_base.Dictionnaire.Remove(sbs_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_synchro_connect_base l_Clone = (clg_synchro_connect_base) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_synchro_connect_base(null, sbs_cn, sbs_a_ip, sbs_d_date_heure, sbs_n_tick,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_synchro_connect_base l_clone = (clg_synchro_connect_base) this.Clone;
		c_sbs_cn = l_clone.sbs_cn;
		c_sbs_a_ip = l_clone.sbs_a_ip;
		c_sbs_d_date_heure = l_clone.sbs_d_date_heure;
		c_sbs_n_tick = l_clone.sbs_n_tick;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLsynchro_connect_base.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLsynchro_connect_base.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLsynchro_connect_base.Delete(this);
    }

    /* Accesseur de la propriete sbs_cn (sbs_cn)
    * @return c_sbs_cn */
    public Int64 sbs_cn
    {
        get{return c_sbs_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_sbs_cn != value)
                {
                    CreerClone();
                    c_sbs_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete sbs_a_ip (sbs_a_ip)
    * @return c_sbs_a_ip */
    public string sbs_a_ip
    {
        get{return c_sbs_a_ip;}
        set
        {
            if(c_sbs_a_ip != value)
            {
                CreerClone();
                c_sbs_a_ip = value;
            }
        }
    }
    /* Accesseur de la propriete sbs_d_date_heure (sbs_d_date_heure)
    * @return c_sbs_d_date_heure */
    public DateTime sbs_d_date_heure
    {
        get{return c_sbs_d_date_heure;}
        set
        {
            if(c_sbs_d_date_heure != value)
            {
                CreerClone();
                c_sbs_d_date_heure = value;
            }
        }
    }
    /* Accesseur de la propriete sbs_n_tick (sbs_n_tick)
    * @return c_sbs_n_tick */
    public Int64 sbs_n_tick
    {
        get{return c_sbs_n_tick;}
        set
        {
            if(c_sbs_n_tick != value)
            {
                CreerClone();
                c_sbs_n_tick = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_sbs_cn.ToString());
		l_Valeurs.Add(c_sbs_a_ip.ToString());
		l_Valeurs.Add(c_sbs_d_date_heure.ToString());
		l_Valeurs.Add(c_sbs_n_tick.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("sbs_cn");
		l_Noms.Add("sbs_a_ip");
		l_Noms.Add("sbs_d_date_heure");
		l_Noms.Add("sbs_n_tick");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
