namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : type_base_mae </summary>
public partial class clg_type_base_mae : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtype_base_mae;
    protected clg_Modele c_Modele;

	private Int64 c_tba_cn;
	private string c_tba_a_lib;
	private List<clg_basemae> c_Listebasemae;


    private void Init()
    {
		c_Listebasemae = new List<clg_basemae>();

    }

    public override void Detruit()
    {
		c_Listebasemae.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_type_base_mae(clg_Modele pModele, Int64 ptba_cn, bool pAMAJ = false) : base(pModele, ptba_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_type_base_mae(clg_Modele pModele, Int64 ptba_cn, string ptba_a_lib, bool pAMAJ = true) : base(pModele, ptba_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptba_cn != Int64.MinValue)
            c_tba_cn = ptba_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_tba_a_lib = ptba_a_lib;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptba_cn, string ptba_a_lib)
    {   
		        c_tba_cn = ptba_cn;
		        c_tba_a_lib = ptba_a_lib;

        base.Initialise(ptba_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtype_base_mae; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetype_base_mae.Dictionnaire.Add(tba_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetype_base_mae.Dictionnaire.Remove(tba_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_type_base_mae l_Clone = (clg_type_base_mae) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listebasemae.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_type_base_mae(null, tba_cn, tba_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_type_base_mae l_clone = (clg_type_base_mae) this.Clone;
		c_tba_cn = l_clone.tba_cn;
		c_tba_a_lib = l_clone.tba_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtype_base_mae.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtype_base_mae.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtype_base_mae.Delete(this);
    }

    /* Accesseur de la propriete tba_cn (tba_cn)
    * @return c_tba_cn */
    public Int64 tba_cn
    {
        get{return c_tba_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tba_cn != value)
                {
                    CreerClone();
                    c_tba_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete tba_a_lib (tba_a_lib)
    * @return c_tba_a_lib */
    public string tba_a_lib
    {
        get{return c_tba_a_lib;}
        set
        {
            if(c_tba_a_lib != value)
            {
                CreerClone();
                c_tba_a_lib = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type basemae */
    public List<clg_basemae> Listebasemae
    {
        get { return c_Listebasemae; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tba_cn.ToString());
		l_Valeurs.Add(c_tba_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tba_cn");
		l_Noms.Add("tba_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
