namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : fenetre </summary>
public partial class clg_fenetre : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfenetre;
    protected clg_Modele c_Modele;

	private Int64 c_fen_cn;
	private string c_fen_a_name;
	private clg_assembly c_assembly;
	private List<clg_limitation_fenetre> c_Listelimitation_fenetre;


    private void Init()
    {
		c_Listelimitation_fenetre = new List<clg_limitation_fenetre>();

    }

    public override void Detruit()
    {
		c_Listelimitation_fenetre.Clear();
		this.c_assembly = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_fenetre(clg_Modele pModele, Int64 pfen_cn, bool pAMAJ = false) : base(pModele, pfen_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_fenetre(clg_Modele pModele, Int64 pfen_cn, string pfen_a_name, clg_assembly passembly, bool pAMAJ = true) : base(pModele, pfen_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfen_cn != Int64.MinValue)
            c_fen_cn = pfen_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pfen_a_name != null)
            c_fen_a_name = pfen_a_name;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(passembly != null)
            c_assembly = passembly;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfen_cn, string pfen_a_name, Int64 passembly)
    {   
		        c_fen_cn = pfen_cn;
		        c_fen_a_name = pfen_a_name;
		c_dicReferences.Add("assembly", passembly);

        base.Initialise(pfen_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfenetre; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_assembly = (clg_assembly) c_ModeleBase.RenvoieObjet(c_dicReferences["assembly"], "clg_assembly");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefenetre.Dictionnaire.Add(fen_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefenetre.Dictionnaire.Remove(fen_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_assembly != null)if(!c_assembly.Listefenetre.Contains(this)) c_assembly.Listefenetre.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_fenetre l_Clone = (clg_fenetre) Clone;
		if(l_Clone.assembly != null)if(l_Clone.assembly.Listefenetre.Contains(this)) l_Clone.assembly.Listefenetre.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listelimitation_fenetre.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_fenetre(null, fen_cn, fen_a_name, assembly,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_fenetre l_clone = (clg_fenetre) this.Clone;
		c_fen_cn = l_clone.fen_cn;
		c_fen_a_name = l_clone.fen_a_name;
		c_assembly = l_clone.assembly;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfenetre.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfenetre.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfenetre.Delete(this);
    }

    /* Accesseur de la propriete fen_cn (fen_cn)
    * @return c_fen_cn */
    public Int64 fen_cn
    {
        get{return c_fen_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_fen_cn != value)
                {
                    CreerClone();
                    c_fen_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete fen_a_name (fen_a_name)
    * @return c_fen_a_name */
    public string fen_a_name
    {
        get{return c_fen_a_name;}
        set
        {
            if(value != null)
            {
                if(c_fen_a_name != value)
                {
                    CreerClone();
                    c_fen_a_name = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete assembly (fen_asc_cn)
    * @return c_assembly */
    public clg_assembly assembly
    {
        get{return c_assembly;}
        set
        {
            if(value != null)
            {
                if(c_assembly != value)
                {
                    CreerClone();
                    c_assembly = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type limitation_fenetre */
    public List<clg_limitation_fenetre> Listelimitation_fenetre
    {
        get { return c_Listelimitation_fenetre; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_fen_cn.ToString());
		l_Valeurs.Add(c_fen_a_name.ToString());
		l_Valeurs.Add(c_assembly==null ? "-1" : c_assembly.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("fen_cn");
		l_Noms.Add("fen_a_name");
		l_Noms.Add("assembly");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
