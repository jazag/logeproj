namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : comptes_financeur </summary>
public partial class clg_comptes_financeur : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcomptes_financeur;
    protected clg_Modele c_Modele;

	private Int64 c_ctf_cn;
	private Int64 c_ctf_t_opf_cn;
	private clg_financeurs c_financeurs;
	private string c_ctf_a_num_compte;
	private string c_ctf_a_comm;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_financeurs = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_comptes_financeur(clg_Modele pModele, Int64 pctf_cn, bool pAMAJ = false) : base(pModele, pctf_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_comptes_financeur(clg_Modele pModele, Int64 pctf_cn, Int64 pctf_t_opf_cn, clg_financeurs pfinanceurs, string pctf_a_num_compte, string pctf_a_comm, bool pAMAJ = true) : base(pModele, pctf_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pctf_cn != Int64.MinValue)
            c_ctf_cn = pctf_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ctf_t_opf_cn = pctf_t_opf_cn;
		        c_financeurs = pfinanceurs;
		        c_ctf_a_num_compte = pctf_a_num_compte;
		        c_ctf_a_comm = pctf_a_comm;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pctf_cn, Int64 pctf_t_opf_cn, Int64 pfinanceurs, string pctf_a_num_compte, string pctf_a_comm)
    {   
		        c_ctf_cn = pctf_cn;
		        c_ctf_t_opf_cn = pctf_t_opf_cn;
		c_dicReferences.Add("financeurs", pfinanceurs);
		        c_ctf_a_num_compte = pctf_a_num_compte;
		        c_ctf_a_comm = pctf_a_comm;

        base.Initialise(pctf_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcomptes_financeur; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_financeurs = (clg_financeurs) c_ModeleBase.RenvoieObjet(c_dicReferences["financeurs"], "clg_financeurs");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecomptes_financeur.Dictionnaire.Add(ctf_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecomptes_financeur.Dictionnaire.Remove(ctf_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_financeurs != null)if(!c_financeurs.Listecomptes_financeur.Contains(this)) c_financeurs.Listecomptes_financeur.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_comptes_financeur l_Clone = (clg_comptes_financeur) Clone;
		if(l_Clone.financeurs != null)if(l_Clone.financeurs.Listecomptes_financeur.Contains(this)) l_Clone.financeurs.Listecomptes_financeur.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_comptes_financeur(null, ctf_cn, ctf_t_opf_cn, financeurs, ctf_a_num_compte, ctf_a_comm,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_comptes_financeur l_clone = (clg_comptes_financeur) this.Clone;
		c_ctf_cn = l_clone.ctf_cn;
		c_ctf_t_opf_cn = l_clone.ctf_t_opf_cn;
		c_financeurs = l_clone.financeurs;
		c_ctf_a_num_compte = l_clone.ctf_a_num_compte;
		c_ctf_a_comm = l_clone.ctf_a_comm;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcomptes_financeur.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcomptes_financeur.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcomptes_financeur.Delete(this);
    }

    /* Accesseur de la propriete ctf_cn (ctf_cn)
    * @return c_ctf_cn */
    public Int64 ctf_cn
    {
        get{return c_ctf_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ctf_cn != value)
                {
                    CreerClone();
                    c_ctf_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ctf_t_opf_cn (ctf_t_opf_cn)
    * @return c_ctf_t_opf_cn */
    public Int64 ctf_t_opf_cn
    {
        get{return c_ctf_t_opf_cn;}
        set
        {
            if(c_ctf_t_opf_cn != value)
            {
                CreerClone();
                c_ctf_t_opf_cn = value;
            }
        }
    }
    /* Accesseur de la propriete financeurs (ctf_fin_cn)
    * @return c_financeurs */
    public clg_financeurs financeurs
    {
        get{return c_financeurs;}
        set
        {
            if(c_financeurs != value)
            {
                CreerClone();
                c_financeurs = value;
            }
        }
    }
    /* Accesseur de la propriete ctf_a_num_compte (ctf_a_num_compte)
    * @return c_ctf_a_num_compte */
    public string ctf_a_num_compte
    {
        get{return c_ctf_a_num_compte;}
        set
        {
            if(c_ctf_a_num_compte != value)
            {
                CreerClone();
                c_ctf_a_num_compte = value;
            }
        }
    }
    /* Accesseur de la propriete ctf_a_comm (ctf_a_comm)
    * @return c_ctf_a_comm */
    public string ctf_a_comm
    {
        get{return c_ctf_a_comm;}
        set
        {
            if(c_ctf_a_comm != value)
            {
                CreerClone();
                c_ctf_a_comm = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ctf_cn.ToString());
		l_Valeurs.Add(c_ctf_t_opf_cn.ToString());
		l_Valeurs.Add(c_financeurs==null ? "-1" : c_financeurs.ID.ToString());
		l_Valeurs.Add(c_ctf_a_num_compte.ToString());
		l_Valeurs.Add(c_ctf_a_comm.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ctf_cn");
		l_Noms.Add("ctf_t_opf_cn");
		l_Noms.Add("financeurs");
		l_Noms.Add("ctf_a_num_compte");
		l_Noms.Add("ctf_a_comm");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
