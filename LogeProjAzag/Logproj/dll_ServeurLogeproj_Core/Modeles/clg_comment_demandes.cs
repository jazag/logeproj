namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : comment_demandes </summary>
public partial class clg_comment_demandes : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcomment_demandes;
    protected clg_Modele c_Modele;

	private Int64 c_cmd_com_cn;
	private clg_demandes_subventions c_demandes_subventions;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_demandes_subventions = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_comment_demandes(clg_Modele pModele, Int64 pcmd_com_cn, bool pAMAJ = false) : base(pModele, pcmd_com_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_comment_demandes(clg_Modele pModele, Int64 pcmd_com_cn, clg_demandes_subventions pdemandes_subventions, bool pAMAJ = true) : base(pModele, pcmd_com_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcmd_com_cn != Int64.MinValue)
            c_cmd_com_cn = pcmd_com_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pdemandes_subventions != null)
            c_demandes_subventions = pdemandes_subventions;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcmd_com_cn, Int64 pdemandes_subventions)
    {   
		        c_cmd_com_cn = pcmd_com_cn;
		c_dicReferences.Add("demandes_subventions", pdemandes_subventions);

        base.Initialise(pcmd_com_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcomment_demandes; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_demandes_subventions = (clg_demandes_subventions) c_ModeleBase.RenvoieObjet(c_dicReferences["demandes_subventions"], "clg_demandes_subventions");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecomment_demandes.Dictionnaire.Add(cmd_com_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecomment_demandes.Dictionnaire.Remove(cmd_com_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_demandes_subventions != null)if(!c_demandes_subventions.Listecomment_demandes.Contains(this)) c_demandes_subventions.Listecomment_demandes.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_comment_demandes l_Clone = (clg_comment_demandes) Clone;
		if(l_Clone.demandes_subventions != null)if(l_Clone.demandes_subventions.Listecomment_demandes.Contains(this)) l_Clone.demandes_subventions.Listecomment_demandes.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_comment_demandes(null, cmd_com_cn, demandes_subventions,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_comment_demandes l_clone = (clg_comment_demandes) this.Clone;
		c_cmd_com_cn = l_clone.cmd_com_cn;
		c_demandes_subventions = l_clone.demandes_subventions;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcomment_demandes.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcomment_demandes.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcomment_demandes.Delete(this);
    }

    /* Accesseur de la propriete cmd_com_cn (cmd_com_cn)
    * @return c_cmd_com_cn */
    public Int64 cmd_com_cn
    {
        get{return c_cmd_com_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cmd_com_cn != value)
                {
                    CreerClone();
                    c_cmd_com_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete demandes_subventions (cmd_dms_cn)
    * @return c_demandes_subventions */
    public clg_demandes_subventions demandes_subventions
    {
        get{return c_demandes_subventions;}
        set
        {
            if(value != null)
            {
                if(c_demandes_subventions != value)
                {
                    CreerClone();
                    c_demandes_subventions = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cmd_com_cn.ToString());
		l_Valeurs.Add(c_demandes_subventions==null ? "-1" : c_demandes_subventions.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cmd_com_cn");
		l_Noms.Add("demandes_subventions");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
