namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_parametre </summary>
public partial class clg_t_parametre : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_parametre;
    protected clg_Modele c_Modele;

	private Int64 c_t_prm_cn;
	private string c_t_prm_a_lib;
	private Int64 c_t_prm_cn_pere;
	private List<clg_parametrage> c_Listeparametrage;


    private void Init()
    {
		c_Listeparametrage = new List<clg_parametrage>();

    }

    public override void Detruit()
    {
		c_Listeparametrage.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_parametre(clg_Modele pModele, Int64 pt_prm_cn, bool pAMAJ = false) : base(pModele, pt_prm_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_parametre(clg_Modele pModele, Int64 pt_prm_cn, string pt_prm_a_lib, Int64 pt_prm_cn_pere, bool pAMAJ = true) : base(pModele, pt_prm_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_prm_cn != Int64.MinValue)
            c_t_prm_cn = pt_prm_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_t_prm_a_lib = pt_prm_a_lib;
		        c_t_prm_cn_pere = pt_prm_cn_pere;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_prm_cn, string pt_prm_a_lib, Int64 pt_prm_cn_pere)
    {   
		        c_t_prm_cn = pt_prm_cn;
		        c_t_prm_a_lib = pt_prm_a_lib;
		        c_t_prm_cn_pere = pt_prm_cn_pere;

        base.Initialise(pt_prm_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_parametre; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_parametre.Dictionnaire.Add(t_prm_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_parametre.Dictionnaire.Remove(t_prm_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_parametre l_Clone = (clg_t_parametre) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeparametrage.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_parametre(null, t_prm_cn, t_prm_a_lib, t_prm_cn_pere,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_parametre l_clone = (clg_t_parametre) this.Clone;
		c_t_prm_cn = l_clone.t_prm_cn;
		c_t_prm_a_lib = l_clone.t_prm_a_lib;
		c_t_prm_cn_pere = l_clone.t_prm_cn_pere;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_parametre.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_parametre.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_parametre.Delete(this);
    }

    /* Accesseur de la propriete t_prm_cn (t_prm_cn)
    * @return c_t_prm_cn */
    public Int64 t_prm_cn
    {
        get{return c_t_prm_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_prm_cn != value)
                {
                    CreerClone();
                    c_t_prm_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_prm_a_lib (t_prm_a_lib)
    * @return c_t_prm_a_lib */
    public string t_prm_a_lib
    {
        get{return c_t_prm_a_lib;}
        set
        {
            if(c_t_prm_a_lib != value)
            {
                CreerClone();
                c_t_prm_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete t_prm_cn_pere (t_prm_cn_pere)
    * @return c_t_prm_cn_pere */
    public Int64 t_prm_cn_pere
    {
        get{return c_t_prm_cn_pere;}
        set
        {
            if(c_t_prm_cn_pere != value)
            {
                CreerClone();
                c_t_prm_cn_pere = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type parametrage */
    public List<clg_parametrage> Listeparametrage
    {
        get { return c_Listeparametrage; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_prm_cn.ToString());
		l_Valeurs.Add(c_t_prm_a_lib.ToString());
		l_Valeurs.Add(c_t_prm_cn_pere.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_prm_cn");
		l_Noms.Add("t_prm_a_lib");
		l_Noms.Add("t_prm_cn_pere");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
