namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : format_carac </summary>
public partial class clg_format_carac : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLformat_carac;
    protected clg_Modele c_Modele;

	private Int64 c_f_car_cn;
	private string c_f_car_a_lib;
	private string c_f_car_a_libcourt;
	private List<clg_carac> c_Listecarac;


    private void Init()
    {
		c_Listecarac = new List<clg_carac>();

    }

    public override void Detruit()
    {
		c_Listecarac.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_format_carac(clg_Modele pModele, Int64 pf_car_cn, bool pAMAJ = false) : base(pModele, pf_car_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_format_carac(clg_Modele pModele, Int64 pf_car_cn, string pf_car_a_lib, string pf_car_a_libcourt, bool pAMAJ = true) : base(pModele, pf_car_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pf_car_cn != Int64.MinValue)
            c_f_car_cn = pf_car_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pf_car_a_lib != null)
            c_f_car_a_lib = pf_car_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pf_car_a_libcourt != null)
            c_f_car_a_libcourt = pf_car_a_libcourt;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pf_car_cn, string pf_car_a_lib, string pf_car_a_libcourt)
    {   
		        c_f_car_cn = pf_car_cn;
		        c_f_car_a_lib = pf_car_a_lib;
		        c_f_car_a_libcourt = pf_car_a_libcourt;

        base.Initialise(pf_car_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLformat_carac; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeformat_carac.Dictionnaire.Add(f_car_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeformat_carac.Dictionnaire.Remove(f_car_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_format_carac l_Clone = (clg_format_carac) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecarac.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_format_carac(null, f_car_cn, f_car_a_lib, f_car_a_libcourt,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_format_carac l_clone = (clg_format_carac) this.Clone;
		c_f_car_cn = l_clone.f_car_cn;
		c_f_car_a_lib = l_clone.f_car_a_lib;
		c_f_car_a_libcourt = l_clone.f_car_a_libcourt;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLformat_carac.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLformat_carac.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLformat_carac.Delete(this);
    }

    /* Accesseur de la propriete f_car_cn (f_car_cn)
    * @return c_f_car_cn */
    public Int64 f_car_cn
    {
        get{return c_f_car_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_f_car_cn != value)
                {
                    CreerClone();
                    c_f_car_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete f_car_a_lib (f_car_a_lib)
    * @return c_f_car_a_lib */
    public string f_car_a_lib
    {
        get{return c_f_car_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_f_car_a_lib != value)
                {
                    CreerClone();
                    c_f_car_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete f_car_a_libcourt (f_car_a_libcourt)
    * @return c_f_car_a_libcourt */
    public string f_car_a_libcourt
    {
        get{return c_f_car_a_libcourt;}
        set
        {
            if(value != null)
            {
                if(c_f_car_a_libcourt != value)
                {
                    CreerClone();
                    c_f_car_a_libcourt = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type carac */
    public List<clg_carac> Listecarac
    {
        get { return c_Listecarac; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_f_car_cn.ToString());
		l_Valeurs.Add(c_f_car_a_lib.ToString());
		l_Valeurs.Add(c_f_car_a_libcourt.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("f_car_cn");
		l_Noms.Add("f_car_a_lib");
		l_Noms.Add("f_car_a_libcourt");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
