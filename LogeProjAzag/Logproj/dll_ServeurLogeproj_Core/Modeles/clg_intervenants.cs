namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : intervenants </summary>
public partial class clg_intervenants : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLintervenants;
    protected clg_Modele c_Modele;

	private Int64 c_itr_cn;
	private clg_elements c_elements;
	private clg_personnels c_personnels;
	private DateTime c_itr_d_debut;
	private Int64 c_itr_n_aff_saisie_temps;
	private clg_t_fonctions c_t_fonctions;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_elements = null;
		this.c_personnels = null;
		this.c_t_fonctions = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_intervenants(clg_Modele pModele, Int64 pitr_cn, bool pAMAJ = false) : base(pModele, pitr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_intervenants(clg_Modele pModele, Int64 pitr_cn, clg_elements pelements, clg_personnels ppersonnels, DateTime pitr_d_debut, Int64 pitr_n_aff_saisie_temps, clg_t_fonctions pt_fonctions, bool pAMAJ = true) : base(pModele, pitr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pitr_cn != Int64.MinValue)
            c_itr_cn = pitr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pelements != null)
            c_elements = pelements;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(ppersonnels != null)
            c_personnels = ppersonnels;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_itr_d_debut = pitr_d_debut;
		        if(pitr_n_aff_saisie_temps != Int64.MinValue)
            c_itr_n_aff_saisie_temps = pitr_n_aff_saisie_temps;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_t_fonctions = pt_fonctions;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pitr_cn, Int64 pelements, Int64 ppersonnels, DateTime pitr_d_debut, Int64 pitr_n_aff_saisie_temps, Int64 pt_fonctions)
    {   
		        c_itr_cn = pitr_cn;
		c_dicReferences.Add("elements", pelements);
		c_dicReferences.Add("personnels", ppersonnels);
		        c_itr_d_debut = pitr_d_debut;
		        c_itr_n_aff_saisie_temps = pitr_n_aff_saisie_temps;
		c_dicReferences.Add("t_fonctions", pt_fonctions);

        base.Initialise(pitr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLintervenants; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_elements = (clg_elements) c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_t_fonctions = (clg_t_fonctions) c_ModeleBase.RenvoieObjet(c_dicReferences["t_fonctions"], "clg_t_fonctions");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeintervenants.Dictionnaire.Add(itr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeintervenants.Dictionnaire.Remove(itr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_elements != null)if(!c_elements.Listeintervenants.Contains(this)) c_elements.Listeintervenants.Add(this);
		if(c_personnels != null)if(!c_personnels.Listeintervenants.Contains(this)) c_personnels.Listeintervenants.Add(this);
		if(c_t_fonctions != null)if(!c_t_fonctions.Listeintervenants.Contains(this)) c_t_fonctions.Listeintervenants.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_intervenants l_Clone = (clg_intervenants) Clone;
		if(l_Clone.elements != null)if(l_Clone.elements.Listeintervenants.Contains(this)) l_Clone.elements.Listeintervenants.Remove(this);
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listeintervenants.Contains(this)) l_Clone.personnels.Listeintervenants.Remove(this);
		if(l_Clone.t_fonctions != null)if(l_Clone.t_fonctions.Listeintervenants.Contains(this)) l_Clone.t_fonctions.Listeintervenants.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_intervenants(null, itr_cn, elements, personnels, itr_d_debut, itr_n_aff_saisie_temps, t_fonctions,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_intervenants l_clone = (clg_intervenants) this.Clone;
		c_itr_cn = l_clone.itr_cn;
		c_elements = l_clone.elements;
		c_personnels = l_clone.personnels;
		c_itr_d_debut = l_clone.itr_d_debut;
		c_itr_n_aff_saisie_temps = l_clone.itr_n_aff_saisie_temps;
		c_t_fonctions = l_clone.t_fonctions;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLintervenants.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLintervenants.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLintervenants.Delete(this);
    }

    /* Accesseur de la propriete itr_cn (itr_cn)
    * @return c_itr_cn */
    public Int64 itr_cn
    {
        get{return c_itr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_itr_cn != value)
                {
                    CreerClone();
                    c_itr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete elements (itr_ele_cn)
    * @return c_elements */
    public clg_elements elements
    {
        get{return c_elements;}
        set
        {
            if(value != null)
            {
                if(c_elements != value)
                {
                    CreerClone();
                    c_elements = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (itr_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(value != null)
            {
                if(c_personnels != value)
                {
                    CreerClone();
                    c_personnels = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete itr_d_debut (itr_d_debut)
    * @return c_itr_d_debut */
    public DateTime itr_d_debut
    {
        get{return c_itr_d_debut;}
        set
        {
            if(c_itr_d_debut != value)
            {
                CreerClone();
                c_itr_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete itr_n_aff_saisie_temps (itr_n_aff_saisie_temps)
    * @return c_itr_n_aff_saisie_temps */
    public Int64 itr_n_aff_saisie_temps
    {
        get{return c_itr_n_aff_saisie_temps;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_itr_n_aff_saisie_temps != value)
                {
                    CreerClone();
                    c_itr_n_aff_saisie_temps = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_fonctions (itr_t_fct_cn)
    * @return c_t_fonctions */
    public clg_t_fonctions t_fonctions
    {
        get{return c_t_fonctions;}
        set
        {
            if(c_t_fonctions != value)
            {
                CreerClone();
                c_t_fonctions = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_itr_cn.ToString());
		l_Valeurs.Add(c_elements==null ? "-1" : c_elements.ID.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_itr_d_debut.ToString());
		l_Valeurs.Add(c_itr_n_aff_saisie_temps.ToString());
		l_Valeurs.Add(c_t_fonctions==null ? "-1" : c_t_fonctions.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("itr_cn");
		l_Noms.Add("elements");
		l_Noms.Add("personnels");
		l_Noms.Add("itr_d_debut");
		l_Noms.Add("itr_n_aff_saisie_temps");
		l_Noms.Add("t_fonctions");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
