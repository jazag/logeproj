﻿using System;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using clg_ReflexionV3_Core;

namespace dll_ServeurLogeproj_Core
{
    public partial class clg_carac_element
    {
        public String ToJson()
        {
            StringBuilder l_sb = new StringBuilder();
            StringWriter l_sw = new StringWriter(l_sb);

            using (JsonWriter l_writer = new JsonTextWriter(l_sw))
            {
                l_writer.Formatting = Formatting.Indented;
                l_writer.WriteStartObject();

                l_writer.WritePropertyName("id_caracteristique_element");
                l_writer.WriteValue(c_cel_cn);

                l_writer.WritePropertyName("caracteristique");
                l_writer.WriteRawValue(carac.ToJson());

                l_writer.WritePropertyName("valeur_caracteristique");
                l_writer.WriteValue(c_cel_a_val);

                l_writer.WritePropertyName("id_parent");
                l_writer.WriteValue(elements.ele_cn);

                l_writer.WritePropertyName("niveau_parent");
                l_writer.WriteValue(elements.ele_n_niveau);

                l_writer.WritePropertyName("libelle_caracteristique");
                l_writer.WriteValue(carac.car_a_lib);

                l_writer.WritePropertyName("Date_Modification");
                l_writer.WriteValue(c_cel_date_modif.ToShortDateString());

                l_writer.WriteEndObject();
            }
            return l_sb.ToString();
        }
    }
}
