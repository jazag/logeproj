namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : jours_feries </summary>
public partial class clg_jours_feries : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLjours_feries;
    protected clg_Modele c_Modele;

	private Int64 c_jrf_cn;
	private DateTime c_jrf_d_date;
	private double c_jrf_n_heures;
	private string c_jrf_a_comment;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_jours_feries(clg_Modele pModele, Int64 pjrf_cn, bool pAMAJ = false) : base(pModele, pjrf_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_jours_feries(clg_Modele pModele, Int64 pjrf_cn, DateTime pjrf_d_date, double pjrf_n_heures, string pjrf_a_comment, bool pAMAJ = true) : base(pModele, pjrf_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pjrf_cn != Int64.MinValue)
            c_jrf_cn = pjrf_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pjrf_d_date != DateTime.MinValue)
            c_jrf_d_date = pjrf_d_date;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_jrf_n_heures = pjrf_n_heures;
		        c_jrf_a_comment = pjrf_a_comment;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pjrf_cn, DateTime pjrf_d_date, double pjrf_n_heures, string pjrf_a_comment)
    {   
		        c_jrf_cn = pjrf_cn;
		        c_jrf_d_date = pjrf_d_date;
		        c_jrf_n_heures = pjrf_n_heures;
		        c_jrf_a_comment = pjrf_a_comment;

        base.Initialise(pjrf_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLjours_feries; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listejours_feries.Dictionnaire.Add(jrf_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listejours_feries.Dictionnaire.Remove(jrf_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_jours_feries l_Clone = (clg_jours_feries) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_jours_feries(null, jrf_cn, jrf_d_date, jrf_n_heures, jrf_a_comment,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_jours_feries l_clone = (clg_jours_feries) this.Clone;
		c_jrf_cn = l_clone.jrf_cn;
		c_jrf_d_date = l_clone.jrf_d_date;
		c_jrf_n_heures = l_clone.jrf_n_heures;
		c_jrf_a_comment = l_clone.jrf_a_comment;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLjours_feries.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLjours_feries.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLjours_feries.Delete(this);
    }

    /* Accesseur de la propriete jrf_cn (jrf_cn)
    * @return c_jrf_cn */
    public Int64 jrf_cn
    {
        get{return c_jrf_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_jrf_cn != value)
                {
                    CreerClone();
                    c_jrf_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete jrf_d_date (jrf_d_date)
    * @return c_jrf_d_date */
    public DateTime jrf_d_date
    {
        get{return c_jrf_d_date;}
        set
        {
            if(value != null)
            {
                if(c_jrf_d_date != value)
                {
                    CreerClone();
                    c_jrf_d_date = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete jrf_n_heures (jrf_n_heures)
    * @return c_jrf_n_heures */
    public double jrf_n_heures
    {
        get{return c_jrf_n_heures;}
        set
        {
            if(c_jrf_n_heures != value)
            {
                CreerClone();
                c_jrf_n_heures = value;
            }
        }
    }
    /* Accesseur de la propriete jrf_a_comment (jrf_a_comment)
    * @return c_jrf_a_comment */
    public string jrf_a_comment
    {
        get{return c_jrf_a_comment;}
        set
        {
            if(c_jrf_a_comment != value)
            {
                CreerClone();
                c_jrf_a_comment = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_jrf_cn.ToString());
		l_Valeurs.Add(c_jrf_d_date.ToString());
		l_Valeurs.Add(c_jrf_n_heures.ToString());
		l_Valeurs.Add(c_jrf_a_comment.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("jrf_cn");
		l_Noms.Add("jrf_d_date");
		l_Noms.Add("jrf_n_heures");
		l_Noms.Add("jrf_a_comment");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
