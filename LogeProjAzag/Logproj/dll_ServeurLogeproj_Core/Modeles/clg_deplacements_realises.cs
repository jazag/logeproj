namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : deplacements_realises </summary>
public partial class clg_deplacements_realises : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLdeplacements_realises;
    protected clg_Modele c_Modele;

	private Int64 c_dpr_cn;
	private clg_vehicules c_vehicules;
	private DateTime c_dpr_d_date;
	private double c_dpr_n_nbkm;
	private double c_dpr_n_kmmin;
	private double c_dpr_n_kmmax;
	private double c_dpr_n_montant;
	private clg_taux_km_reels c_taux_km_reels;
	private string c_dpr_a_dest;
	private string c_dpr_a_utilisateur;
	private string c_dpr_a_codeana;
	private string c_dpr_a_objet;
	private List<clg_frais_mission> c_Listefrais_mission;


    private void Init()
    {
		c_Listefrais_mission = new List<clg_frais_mission>();

    }

    public override void Detruit()
    {
		c_Listefrais_mission.Clear();
		this.c_vehicules = null;
		this.c_taux_km_reels = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_deplacements_realises(clg_Modele pModele, Int64 pdpr_cn, bool pAMAJ = false) : base(pModele, pdpr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_deplacements_realises(clg_Modele pModele, Int64 pdpr_cn, clg_vehicules pvehicules, DateTime pdpr_d_date, double pdpr_n_nbkm, double pdpr_n_kmmin, double pdpr_n_kmmax, double pdpr_n_montant, clg_taux_km_reels ptaux_km_reels, string pdpr_a_dest, string pdpr_a_utilisateur, string pdpr_a_codeana, string pdpr_a_objet, bool pAMAJ = true) : base(pModele, pdpr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pdpr_cn != Int64.MinValue)
            c_dpr_cn = pdpr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_vehicules = pvehicules;
		        c_dpr_d_date = pdpr_d_date;
		        c_dpr_n_nbkm = pdpr_n_nbkm;
		        c_dpr_n_kmmin = pdpr_n_kmmin;
		        c_dpr_n_kmmax = pdpr_n_kmmax;
		        c_dpr_n_montant = pdpr_n_montant;
		        c_taux_km_reels = ptaux_km_reels;
		        c_dpr_a_dest = pdpr_a_dest;
		        c_dpr_a_utilisateur = pdpr_a_utilisateur;
		        c_dpr_a_codeana = pdpr_a_codeana;
		        c_dpr_a_objet = pdpr_a_objet;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pdpr_cn, Int64 pvehicules, DateTime pdpr_d_date, double pdpr_n_nbkm, double pdpr_n_kmmin, double pdpr_n_kmmax, double pdpr_n_montant, Int64 ptaux_km_reels, string pdpr_a_dest, string pdpr_a_utilisateur, string pdpr_a_codeana, string pdpr_a_objet)
    {   
		        c_dpr_cn = pdpr_cn;
		c_dicReferences.Add("vehicules", pvehicules);
		        c_dpr_d_date = pdpr_d_date;
		        c_dpr_n_nbkm = pdpr_n_nbkm;
		        c_dpr_n_kmmin = pdpr_n_kmmin;
		        c_dpr_n_kmmax = pdpr_n_kmmax;
		        c_dpr_n_montant = pdpr_n_montant;
		c_dicReferences.Add("taux_km_reels", ptaux_km_reels);
		        c_dpr_a_dest = pdpr_a_dest;
		        c_dpr_a_utilisateur = pdpr_a_utilisateur;
		        c_dpr_a_codeana = pdpr_a_codeana;
		        c_dpr_a_objet = pdpr_a_objet;

        base.Initialise(pdpr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLdeplacements_realises; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_vehicules = (clg_vehicules) c_ModeleBase.RenvoieObjet(c_dicReferences["vehicules"], "clg_vehicules");
		c_taux_km_reels = (clg_taux_km_reels) c_ModeleBase.RenvoieObjet(c_dicReferences["taux_km_reels"], "clg_taux_km_reels");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listedeplacements_realises.Dictionnaire.Add(dpr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listedeplacements_realises.Dictionnaire.Remove(dpr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_vehicules != null)if(!c_vehicules.Listedeplacements_realises.Contains(this)) c_vehicules.Listedeplacements_realises.Add(this);
		if(c_taux_km_reels != null)if(!c_taux_km_reels.Listedeplacements_realises.Contains(this)) c_taux_km_reels.Listedeplacements_realises.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_deplacements_realises l_Clone = (clg_deplacements_realises) Clone;
		if(l_Clone.vehicules != null)if(l_Clone.vehicules.Listedeplacements_realises.Contains(this)) l_Clone.vehicules.Listedeplacements_realises.Remove(this);
		if(l_Clone.taux_km_reels != null)if(l_Clone.taux_km_reels.Listedeplacements_realises.Contains(this)) l_Clone.taux_km_reels.Listedeplacements_realises.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listefrais_mission.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_deplacements_realises(null, dpr_cn, vehicules, dpr_d_date, dpr_n_nbkm, dpr_n_kmmin, dpr_n_kmmax, dpr_n_montant, taux_km_reels, dpr_a_dest, dpr_a_utilisateur, dpr_a_codeana, dpr_a_objet,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_deplacements_realises l_clone = (clg_deplacements_realises) this.Clone;
		c_dpr_cn = l_clone.dpr_cn;
		c_vehicules = l_clone.vehicules;
		c_dpr_d_date = l_clone.dpr_d_date;
		c_dpr_n_nbkm = l_clone.dpr_n_nbkm;
		c_dpr_n_kmmin = l_clone.dpr_n_kmmin;
		c_dpr_n_kmmax = l_clone.dpr_n_kmmax;
		c_dpr_n_montant = l_clone.dpr_n_montant;
		c_taux_km_reels = l_clone.taux_km_reels;
		c_dpr_a_dest = l_clone.dpr_a_dest;
		c_dpr_a_utilisateur = l_clone.dpr_a_utilisateur;
		c_dpr_a_codeana = l_clone.dpr_a_codeana;
		c_dpr_a_objet = l_clone.dpr_a_objet;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLdeplacements_realises.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLdeplacements_realises.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLdeplacements_realises.Delete(this);
    }

    /* Accesseur de la propriete dpr_cn (dpr_cn)
    * @return c_dpr_cn */
    public Int64 dpr_cn
    {
        get{return c_dpr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_dpr_cn != value)
                {
                    CreerClone();
                    c_dpr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete vehicules (dpr_veh_cn)
    * @return c_vehicules */
    public clg_vehicules vehicules
    {
        get{return c_vehicules;}
        set
        {
            if(c_vehicules != value)
            {
                CreerClone();
                c_vehicules = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_d_date (dpr_d_date)
    * @return c_dpr_d_date */
    public DateTime dpr_d_date
    {
        get{return c_dpr_d_date;}
        set
        {
            if(c_dpr_d_date != value)
            {
                CreerClone();
                c_dpr_d_date = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_n_nbkm (dpr_n_nbkm)
    * @return c_dpr_n_nbkm */
    public double dpr_n_nbkm
    {
        get{return c_dpr_n_nbkm;}
        set
        {
            if(c_dpr_n_nbkm != value)
            {
                CreerClone();
                c_dpr_n_nbkm = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_n_kmmin (dpr_n_kmmin)
    * @return c_dpr_n_kmmin */
    public double dpr_n_kmmin
    {
        get{return c_dpr_n_kmmin;}
        set
        {
            if(c_dpr_n_kmmin != value)
            {
                CreerClone();
                c_dpr_n_kmmin = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_n_kmmax (dpr_n_kmmax)
    * @return c_dpr_n_kmmax */
    public double dpr_n_kmmax
    {
        get{return c_dpr_n_kmmax;}
        set
        {
            if(c_dpr_n_kmmax != value)
            {
                CreerClone();
                c_dpr_n_kmmax = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_n_montant (dpr_n_montant)
    * @return c_dpr_n_montant */
    public double dpr_n_montant
    {
        get{return c_dpr_n_montant;}
        set
        {
            if(c_dpr_n_montant != value)
            {
                CreerClone();
                c_dpr_n_montant = value;
            }
        }
    }
    /* Accesseur de la propriete taux_km_reels (dpr_tkr_cn)
    * @return c_taux_km_reels */
    public clg_taux_km_reels taux_km_reels
    {
        get{return c_taux_km_reels;}
        set
        {
            if(c_taux_km_reels != value)
            {
                CreerClone();
                c_taux_km_reels = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_a_dest (dpr_a_dest)
    * @return c_dpr_a_dest */
    public string dpr_a_dest
    {
        get{return c_dpr_a_dest;}
        set
        {
            if(c_dpr_a_dest != value)
            {
                CreerClone();
                c_dpr_a_dest = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_a_utilisateur (dpr_a_utilisateur)
    * @return c_dpr_a_utilisateur */
    public string dpr_a_utilisateur
    {
        get{return c_dpr_a_utilisateur;}
        set
        {
            if(c_dpr_a_utilisateur != value)
            {
                CreerClone();
                c_dpr_a_utilisateur = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_a_codeana (dpr_a_codeana)
    * @return c_dpr_a_codeana */
    public string dpr_a_codeana
    {
        get{return c_dpr_a_codeana;}
        set
        {
            if(c_dpr_a_codeana != value)
            {
                CreerClone();
                c_dpr_a_codeana = value;
            }
        }
    }
    /* Accesseur de la propriete dpr_a_objet (dpr_a_objet)
    * @return c_dpr_a_objet */
    public string dpr_a_objet
    {
        get{return c_dpr_a_objet;}
        set
        {
            if(c_dpr_a_objet != value)
            {
                CreerClone();
                c_dpr_a_objet = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type frais_mission */
    public List<clg_frais_mission> Listefrais_mission
    {
        get { return c_Listefrais_mission; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_dpr_cn.ToString());
		l_Valeurs.Add(c_vehicules==null ? "-1" : c_vehicules.ID.ToString());
		l_Valeurs.Add(c_dpr_d_date.ToString());
		l_Valeurs.Add(c_dpr_n_nbkm.ToString());
		l_Valeurs.Add(c_dpr_n_kmmin.ToString());
		l_Valeurs.Add(c_dpr_n_kmmax.ToString());
		l_Valeurs.Add(c_dpr_n_montant.ToString());
		l_Valeurs.Add(c_taux_km_reels==null ? "-1" : c_taux_km_reels.ID.ToString());
		l_Valeurs.Add(c_dpr_a_dest.ToString());
		l_Valeurs.Add(c_dpr_a_utilisateur.ToString());
		l_Valeurs.Add(c_dpr_a_codeana.ToString());
		l_Valeurs.Add(c_dpr_a_objet.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("dpr_cn");
		l_Noms.Add("vehicules");
		l_Noms.Add("dpr_d_date");
		l_Noms.Add("dpr_n_nbkm");
		l_Noms.Add("dpr_n_kmmin");
		l_Noms.Add("dpr_n_kmmax");
		l_Noms.Add("dpr_n_montant");
		l_Noms.Add("taux_km_reels");
		l_Noms.Add("dpr_a_dest");
		l_Noms.Add("dpr_a_utilisateur");
		l_Noms.Add("dpr_a_codeana");
		l_Noms.Add("dpr_a_objet");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
