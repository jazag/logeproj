﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dll_ServeurLogeproj_Core
{
    public partial class clg_elements
    {
        public string ToJson()
        {
            StringBuilder l_sb = new StringBuilder();
            StringWriter l_sw = new StringWriter(l_sb);

            using (JsonWriter l_writer = new JsonTextWriter(l_sw))
            {
                l_writer.Formatting = Formatting.Indented;
                l_writer.WriteStartObject();

                l_writer.WritePropertyName("c_ele_cn");
                l_writer.WriteValue(c_ele_cn);

                l_writer.WritePropertyName("c_ele_a_lib");
                l_writer.WriteValue(c_ele_a_lib);

                if (c_elements != null)
                {
                    l_writer.WritePropertyName("c_elements_pere");
                    l_writer.WriteValue(c_elements.ele_cn);
                }

                l_writer.WritePropertyName("c_ele_n_niveau");
                l_writer.WriteValue(c_ele_n_niveau);


                l_writer.WritePropertyName("estParent");
                l_writer.WriteValue(true);

                l_writer.WriteEndObject();

            }
            return l_sb.ToString();
        }
    }
}
