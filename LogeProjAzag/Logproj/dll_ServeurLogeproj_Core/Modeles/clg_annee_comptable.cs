namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : annee_comptable </summary>
public partial class clg_annee_comptable : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLannee_comptable;
    protected clg_Modele c_Modele;

	private Int64 c_acp_cn;
	private DateTime c_acp_d_debut;
	private DateTime c_acp_d_fin;
	private string c_acp_m_comment;
	private string c_acp_a_libel;
	private List<clg_arretes> c_Listearretes;
	private List<clg_demandes_subventions> c_Listedemandes_subventions;
	private List<clg_facture> c_Listefacture;
	private List<clg_frais_generaux> c_Listefrais_generaux;
	private List<clg_operations> c_Listeoperations;
	private List<clg_taux> c_Listetaux;


    private void Init()
    {
		c_Listearretes = new List<clg_arretes>();
		c_Listedemandes_subventions = new List<clg_demandes_subventions>();
		c_Listefacture = new List<clg_facture>();
		c_Listefrais_generaux = new List<clg_frais_generaux>();
		c_Listeoperations = new List<clg_operations>();
		c_Listetaux = new List<clg_taux>();

    }

    public override void Detruit()
    {
		c_Listearretes.Clear();
		c_Listedemandes_subventions.Clear();
		c_Listefacture.Clear();
		c_Listefrais_generaux.Clear();
		c_Listeoperations.Clear();
		c_Listetaux.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_annee_comptable(clg_Modele pModele, Int64 pacp_cn, bool pAMAJ = false) : base(pModele, pacp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_annee_comptable(clg_Modele pModele, Int64 pacp_cn, DateTime pacp_d_debut, DateTime pacp_d_fin, string pacp_m_comment, string pacp_a_libel, bool pAMAJ = true) : base(pModele, pacp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pacp_cn != Int64.MinValue)
            c_acp_cn = pacp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_acp_d_debut = pacp_d_debut;
		        c_acp_d_fin = pacp_d_fin;
		        c_acp_m_comment = pacp_m_comment;
		        c_acp_a_libel = pacp_a_libel;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pacp_cn, DateTime pacp_d_debut, DateTime pacp_d_fin, string pacp_m_comment, string pacp_a_libel)
    {   
		        c_acp_cn = pacp_cn;
		        c_acp_d_debut = pacp_d_debut;
		        c_acp_d_fin = pacp_d_fin;
		        c_acp_m_comment = pacp_m_comment;
		        c_acp_a_libel = pacp_a_libel;

        base.Initialise(pacp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLannee_comptable; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeannee_comptable.Dictionnaire.Add(acp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeannee_comptable.Dictionnaire.Remove(acp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_annee_comptable l_Clone = (clg_annee_comptable) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listearretes.Count > 0) c_EstReference = true;
		if(c_Listedemandes_subventions.Count > 0) c_EstReference = true;
		if(c_Listefacture.Count > 0) c_EstReference = true;
		if(c_Listefrais_generaux.Count > 0) c_EstReference = true;
		if(c_Listeoperations.Count > 0) c_EstReference = true;
		if(c_Listetaux.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_annee_comptable(null, acp_cn, acp_d_debut, acp_d_fin, acp_m_comment, acp_a_libel,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_annee_comptable l_clone = (clg_annee_comptable) this.Clone;
		c_acp_cn = l_clone.acp_cn;
		c_acp_d_debut = l_clone.acp_d_debut;
		c_acp_d_fin = l_clone.acp_d_fin;
		c_acp_m_comment = l_clone.acp_m_comment;
		c_acp_a_libel = l_clone.acp_a_libel;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLannee_comptable.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLannee_comptable.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLannee_comptable.Delete(this);
    }

    /* Accesseur de la propriete acp_cn (acp_cn)
    * @return c_acp_cn */
    public Int64 acp_cn
    {
        get{return c_acp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_acp_cn != value)
                {
                    CreerClone();
                    c_acp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete acp_d_debut (acp_d_debut)
    * @return c_acp_d_debut */
    public DateTime acp_d_debut
    {
        get{return c_acp_d_debut;}
        set
        {
            if(c_acp_d_debut != value)
            {
                CreerClone();
                c_acp_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete acp_d_fin (acp_d_fin)
    * @return c_acp_d_fin */
    public DateTime acp_d_fin
    {
        get{return c_acp_d_fin;}
        set
        {
            if(c_acp_d_fin != value)
            {
                CreerClone();
                c_acp_d_fin = value;
            }
        }
    }
    /* Accesseur de la propriete acp_m_comment (acp_m_comment)
    * @return c_acp_m_comment */
    public string acp_m_comment
    {
        get{return c_acp_m_comment;}
        set
        {
            if(c_acp_m_comment != value)
            {
                CreerClone();
                c_acp_m_comment = value;
            }
        }
    }
    /* Accesseur de la propriete acp_a_libel (acp_a_libel)
    * @return c_acp_a_libel */
    public string acp_a_libel
    {
        get{return c_acp_a_libel;}
        set
        {
            if(c_acp_a_libel != value)
            {
                CreerClone();
                c_acp_a_libel = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type arretes */
    public List<clg_arretes> Listearretes
    {
        get { return c_Listearretes; }
    }    /* Accesseur de la liste des objets de type demandes_subventions */
    public List<clg_demandes_subventions> Listedemandes_subventions
    {
        get { return c_Listedemandes_subventions; }
    }    /* Accesseur de la liste des objets de type facture */
    public List<clg_facture> Listefacture
    {
        get { return c_Listefacture; }
    }    /* Accesseur de la liste des objets de type frais_generaux */
    public List<clg_frais_generaux> Listefrais_generaux
    {
        get { return c_Listefrais_generaux; }
    }    /* Accesseur de la liste des objets de type operations */
    public List<clg_operations> Listeoperations
    {
        get { return c_Listeoperations; }
    }    /* Accesseur de la liste des objets de type taux */
    public List<clg_taux> Listetaux
    {
        get { return c_Listetaux; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_acp_cn.ToString());
		l_Valeurs.Add(c_acp_d_debut.ToString());
		l_Valeurs.Add(c_acp_d_fin.ToString());
		l_Valeurs.Add(c_acp_m_comment.ToString());
		l_Valeurs.Add(c_acp_a_libel.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("acp_cn");
		l_Noms.Add("acp_d_debut");
		l_Noms.Add("acp_d_fin");
		l_Noms.Add("acp_m_comment");
		l_Noms.Add("acp_a_libel");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
