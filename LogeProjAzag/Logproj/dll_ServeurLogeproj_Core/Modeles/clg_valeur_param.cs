namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : valeur_param </summary>
public partial class clg_valeur_param : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLvaleur_param;
    protected clg_Modele c_Modele;

	private Int64 c_vpm_cn;
	private clg_parametrage c_parametrage;
	private string c_vpm_a_valeur;
	private string c_vpm_a_lib;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_parametrage = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_valeur_param(clg_Modele pModele, Int64 pvpm_cn, bool pAMAJ = false) : base(pModele, pvpm_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_valeur_param(clg_Modele pModele, Int64 pvpm_cn, clg_parametrage pparametrage, string pvpm_a_valeur, string pvpm_a_lib, bool pAMAJ = true) : base(pModele, pvpm_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pvpm_cn != Int64.MinValue)
            c_vpm_cn = pvpm_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_parametrage = pparametrage;
		        c_vpm_a_valeur = pvpm_a_valeur;
		        c_vpm_a_lib = pvpm_a_lib;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pvpm_cn, Int64 pparametrage, string pvpm_a_valeur, string pvpm_a_lib)
    {   
		        c_vpm_cn = pvpm_cn;
		c_dicReferences.Add("parametrage", pparametrage);
		        c_vpm_a_valeur = pvpm_a_valeur;
		        c_vpm_a_lib = pvpm_a_lib;

        base.Initialise(pvpm_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLvaleur_param; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_parametrage = (clg_parametrage) c_ModeleBase.RenvoieObjet(c_dicReferences["parametrage"], "clg_parametrage");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listevaleur_param.Dictionnaire.Add(vpm_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listevaleur_param.Dictionnaire.Remove(vpm_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_parametrage != null)if(!c_parametrage.Listevaleur_param.Contains(this)) c_parametrage.Listevaleur_param.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_valeur_param l_Clone = (clg_valeur_param) Clone;
		if(l_Clone.parametrage != null)if(l_Clone.parametrage.Listevaleur_param.Contains(this)) l_Clone.parametrage.Listevaleur_param.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_valeur_param(null, vpm_cn, parametrage, vpm_a_valeur, vpm_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_valeur_param l_clone = (clg_valeur_param) this.Clone;
		c_vpm_cn = l_clone.vpm_cn;
		c_parametrage = l_clone.parametrage;
		c_vpm_a_valeur = l_clone.vpm_a_valeur;
		c_vpm_a_lib = l_clone.vpm_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLvaleur_param.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLvaleur_param.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLvaleur_param.Delete(this);
    }

    /* Accesseur de la propriete vpm_cn (vpm_cn)
    * @return c_vpm_cn */
    public Int64 vpm_cn
    {
        get{return c_vpm_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_vpm_cn != value)
                {
                    CreerClone();
                    c_vpm_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete parametrage (vpm_prm_cn)
    * @return c_parametrage */
    public clg_parametrage parametrage
    {
        get{return c_parametrage;}
        set
        {
            if(c_parametrage != value)
            {
                CreerClone();
                c_parametrage = value;
            }
        }
    }
    /* Accesseur de la propriete vpm_a_valeur (vpm_a_valeur)
    * @return c_vpm_a_valeur */
    public string vpm_a_valeur
    {
        get{return c_vpm_a_valeur;}
        set
        {
            if(c_vpm_a_valeur != value)
            {
                CreerClone();
                c_vpm_a_valeur = value;
            }
        }
    }
    /* Accesseur de la propriete vpm_a_lib (vpm_a_lib)
    * @return c_vpm_a_lib */
    public string vpm_a_lib
    {
        get{return c_vpm_a_lib;}
        set
        {
            if(c_vpm_a_lib != value)
            {
                CreerClone();
                c_vpm_a_lib = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_vpm_cn.ToString());
		l_Valeurs.Add(c_parametrage==null ? "-1" : c_parametrage.ID.ToString());
		l_Valeurs.Add(c_vpm_a_valeur.ToString());
		l_Valeurs.Add(c_vpm_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("vpm_cn");
		l_Noms.Add("parametrage");
		l_Noms.Add("vpm_a_valeur");
		l_Noms.Add("vpm_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
