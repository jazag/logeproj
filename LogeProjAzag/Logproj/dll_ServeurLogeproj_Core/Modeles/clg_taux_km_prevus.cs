namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : taux_km_prevus </summary>
public partial class clg_taux_km_prevus : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtaux_km_prevus;
    protected clg_Modele c_Modele;

	private Int64 c_tkp_cn;
	private clg_vehicules c_vehicules;
	private Int64 c_tkp_acp_cn;
	private double c_tkp_n_taux;
	private List<clg_deplacements_prevus> c_Listedeplacements_prevus;


    private void Init()
    {
		c_Listedeplacements_prevus = new List<clg_deplacements_prevus>();

    }

    public override void Detruit()
    {
		c_Listedeplacements_prevus.Clear();
		this.c_vehicules = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_taux_km_prevus(clg_Modele pModele, Int64 ptkp_cn, bool pAMAJ = false) : base(pModele, ptkp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_taux_km_prevus(clg_Modele pModele, Int64 ptkp_cn, clg_vehicules pvehicules, Int64 ptkp_acp_cn, double ptkp_n_taux, bool pAMAJ = true) : base(pModele, ptkp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptkp_cn != Int64.MinValue)
            c_tkp_cn = ptkp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_vehicules = pvehicules;
		        c_tkp_acp_cn = ptkp_acp_cn;
		        c_tkp_n_taux = ptkp_n_taux;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptkp_cn, Int64 pvehicules, Int64 ptkp_acp_cn, double ptkp_n_taux)
    {   
		        c_tkp_cn = ptkp_cn;
		c_dicReferences.Add("vehicules", pvehicules);
		        c_tkp_acp_cn = ptkp_acp_cn;
		        c_tkp_n_taux = ptkp_n_taux;

        base.Initialise(ptkp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtaux_km_prevus; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_vehicules = (clg_vehicules) c_ModeleBase.RenvoieObjet(c_dicReferences["vehicules"], "clg_vehicules");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetaux_km_prevus.Dictionnaire.Add(tkp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetaux_km_prevus.Dictionnaire.Remove(tkp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_vehicules != null)if(!c_vehicules.Listetaux_km_prevus.Contains(this)) c_vehicules.Listetaux_km_prevus.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_taux_km_prevus l_Clone = (clg_taux_km_prevus) Clone;
		if(l_Clone.vehicules != null)if(l_Clone.vehicules.Listetaux_km_prevus.Contains(this)) l_Clone.vehicules.Listetaux_km_prevus.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listedeplacements_prevus.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_taux_km_prevus(null, tkp_cn, vehicules, tkp_acp_cn, tkp_n_taux,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_taux_km_prevus l_clone = (clg_taux_km_prevus) this.Clone;
		c_tkp_cn = l_clone.tkp_cn;
		c_vehicules = l_clone.vehicules;
		c_tkp_acp_cn = l_clone.tkp_acp_cn;
		c_tkp_n_taux = l_clone.tkp_n_taux;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtaux_km_prevus.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtaux_km_prevus.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtaux_km_prevus.Delete(this);
    }

    /* Accesseur de la propriete tkp_cn (tkp_cn)
    * @return c_tkp_cn */
    public Int64 tkp_cn
    {
        get{return c_tkp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tkp_cn != value)
                {
                    CreerClone();
                    c_tkp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete vehicules (tkp_veh_cn)
    * @return c_vehicules */
    public clg_vehicules vehicules
    {
        get{return c_vehicules;}
        set
        {
            if(c_vehicules != value)
            {
                CreerClone();
                c_vehicules = value;
            }
        }
    }
    /* Accesseur de la propriete tkp_acp_cn (tkp_acp_cn)
    * @return c_tkp_acp_cn */
    public Int64 tkp_acp_cn
    {
        get{return c_tkp_acp_cn;}
        set
        {
            if(c_tkp_acp_cn != value)
            {
                CreerClone();
                c_tkp_acp_cn = value;
            }
        }
    }
    /* Accesseur de la propriete tkp_n_taux (tkp_n_taux)
    * @return c_tkp_n_taux */
    public double tkp_n_taux
    {
        get{return c_tkp_n_taux;}
        set
        {
            if(c_tkp_n_taux != value)
            {
                CreerClone();
                c_tkp_n_taux = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type deplacements_prevus */
    public List<clg_deplacements_prevus> Listedeplacements_prevus
    {
        get { return c_Listedeplacements_prevus; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tkp_cn.ToString());
		l_Valeurs.Add(c_vehicules==null ? "-1" : c_vehicules.ID.ToString());
		l_Valeurs.Add(c_tkp_acp_cn.ToString());
		l_Valeurs.Add(c_tkp_n_taux.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tkp_cn");
		l_Noms.Add("vehicules");
		l_Noms.Add("tkp_acp_cn");
		l_Noms.Add("tkp_n_taux");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
