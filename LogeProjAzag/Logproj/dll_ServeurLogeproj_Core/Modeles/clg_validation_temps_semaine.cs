namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : validation_temps_semaine </summary>
public partial class clg_validation_temps_semaine : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLvalidation_temps_semaine;
    protected clg_Modele c_Modele;

	private Int64 c_vts_cn;
	private double c_vts_n_nb_heure_sup;
	private Int64 c_vts_n_validation_ok;
	private Int64 c_vts_sem_cn;
	private clg_personnels c_personnels;
	private double c_vts_n_recup_possible;
	private double c_vts_n_heures_recuperee;
	private double c_vts_n_tot_heures;
	private double c_vts_n_modulation;
	private double c_vts_n_ecart_theorique;
	private Int64 c_vts_n_total_ok;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_personnels = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_validation_temps_semaine(clg_Modele pModele, Int64 pvts_cn, bool pAMAJ = false) : base(pModele, pvts_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_validation_temps_semaine(clg_Modele pModele, Int64 pvts_cn, double pvts_n_nb_heure_sup, Int64 pvts_n_validation_ok, Int64 pvts_sem_cn, clg_personnels ppersonnels, double pvts_n_recup_possible, double pvts_n_heures_recuperee, double pvts_n_tot_heures, double pvts_n_modulation, double pvts_n_ecart_theorique, Int64 pvts_n_total_ok, bool pAMAJ = true) : base(pModele, pvts_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pvts_cn != Int64.MinValue)
            c_vts_cn = pvts_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_vts_n_nb_heure_sup = pvts_n_nb_heure_sup;
		        c_vts_n_validation_ok = pvts_n_validation_ok;
		        if(pvts_sem_cn != Int64.MinValue)
            c_vts_sem_cn = pvts_sem_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(ppersonnels != null)
            c_personnels = ppersonnels;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_vts_n_recup_possible = pvts_n_recup_possible;
		        c_vts_n_heures_recuperee = pvts_n_heures_recuperee;
		        c_vts_n_tot_heures = pvts_n_tot_heures;
		        c_vts_n_modulation = pvts_n_modulation;
		        c_vts_n_ecart_theorique = pvts_n_ecart_theorique;
		        c_vts_n_total_ok = pvts_n_total_ok;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pvts_cn, double pvts_n_nb_heure_sup, Int64 pvts_n_validation_ok, Int64 pvts_sem_cn, Int64 ppersonnels, double pvts_n_recup_possible, double pvts_n_heures_recuperee, double pvts_n_tot_heures, double pvts_n_modulation, double pvts_n_ecart_theorique, Int64 pvts_n_total_ok)
    {   
		        c_vts_cn = pvts_cn;
		        c_vts_n_nb_heure_sup = pvts_n_nb_heure_sup;
		        c_vts_n_validation_ok = pvts_n_validation_ok;
		        c_vts_sem_cn = pvts_sem_cn;
		c_dicReferences.Add("personnels", ppersonnels);
		        c_vts_n_recup_possible = pvts_n_recup_possible;
		        c_vts_n_heures_recuperee = pvts_n_heures_recuperee;
		        c_vts_n_tot_heures = pvts_n_tot_heures;
		        c_vts_n_modulation = pvts_n_modulation;
		        c_vts_n_ecart_theorique = pvts_n_ecart_theorique;
		        c_vts_n_total_ok = pvts_n_total_ok;

        base.Initialise(pvts_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLvalidation_temps_semaine; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listevalidation_temps_semaine.Dictionnaire.Add(vts_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listevalidation_temps_semaine.Dictionnaire.Remove(vts_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listevalidation_temps_semaine.Contains(this)) c_personnels.Listevalidation_temps_semaine.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_validation_temps_semaine l_Clone = (clg_validation_temps_semaine) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listevalidation_temps_semaine.Contains(this)) l_Clone.personnels.Listevalidation_temps_semaine.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_validation_temps_semaine(null, vts_cn, vts_n_nb_heure_sup, vts_n_validation_ok, vts_sem_cn, personnels, vts_n_recup_possible, vts_n_heures_recuperee, vts_n_tot_heures, vts_n_modulation, vts_n_ecart_theorique, vts_n_total_ok,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_validation_temps_semaine l_clone = (clg_validation_temps_semaine) this.Clone;
		c_vts_cn = l_clone.vts_cn;
		c_vts_n_nb_heure_sup = l_clone.vts_n_nb_heure_sup;
		c_vts_n_validation_ok = l_clone.vts_n_validation_ok;
		c_vts_sem_cn = l_clone.vts_sem_cn;
		c_personnels = l_clone.personnels;
		c_vts_n_recup_possible = l_clone.vts_n_recup_possible;
		c_vts_n_heures_recuperee = l_clone.vts_n_heures_recuperee;
		c_vts_n_tot_heures = l_clone.vts_n_tot_heures;
		c_vts_n_modulation = l_clone.vts_n_modulation;
		c_vts_n_ecart_theorique = l_clone.vts_n_ecart_theorique;
		c_vts_n_total_ok = l_clone.vts_n_total_ok;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLvalidation_temps_semaine.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLvalidation_temps_semaine.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLvalidation_temps_semaine.Delete(this);
    }

    /* Accesseur de la propriete vts_cn (vts_cn)
    * @return c_vts_cn */
    public Int64 vts_cn
    {
        get{return c_vts_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_vts_cn != value)
                {
                    CreerClone();
                    c_vts_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete vts_n_nb_heure_sup (vts_n_nb_heure_sup)
    * @return c_vts_n_nb_heure_sup */
    public double vts_n_nb_heure_sup
    {
        get{return c_vts_n_nb_heure_sup;}
        set
        {
            if(c_vts_n_nb_heure_sup != value)
            {
                CreerClone();
                c_vts_n_nb_heure_sup = value;
            }
        }
    }
    /* Accesseur de la propriete vts_n_validation_ok (vts_n_validation_ok)
    * @return c_vts_n_validation_ok */
    public Int64 vts_n_validation_ok
    {
        get{return c_vts_n_validation_ok;}
        set
        {
            if(c_vts_n_validation_ok != value)
            {
                CreerClone();
                c_vts_n_validation_ok = value;
            }
        }
    }
    /* Accesseur de la propriete vts_sem_cn (vts_sem_cn)
    * @return c_vts_sem_cn */
    public Int64 vts_sem_cn
    {
        get{return c_vts_sem_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_vts_sem_cn != value)
                {
                    CreerClone();
                    c_vts_sem_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (vts_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(value != null)
            {
                if(c_personnels != value)
                {
                    CreerClone();
                    c_personnels = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete vts_n_recup_possible (vts_n_recup_possible)
    * @return c_vts_n_recup_possible */
    public double vts_n_recup_possible
    {
        get{return c_vts_n_recup_possible;}
        set
        {
            if(c_vts_n_recup_possible != value)
            {
                CreerClone();
                c_vts_n_recup_possible = value;
            }
        }
    }
    /* Accesseur de la propriete vts_n_heures_recuperee (vts_n_heures_recuperee)
    * @return c_vts_n_heures_recuperee */
    public double vts_n_heures_recuperee
    {
        get{return c_vts_n_heures_recuperee;}
        set
        {
            if(c_vts_n_heures_recuperee != value)
            {
                CreerClone();
                c_vts_n_heures_recuperee = value;
            }
        }
    }
    /* Accesseur de la propriete vts_n_tot_heures (vts_n_tot_heures)
    * @return c_vts_n_tot_heures */
    public double vts_n_tot_heures
    {
        get{return c_vts_n_tot_heures;}
        set
        {
            if(c_vts_n_tot_heures != value)
            {
                CreerClone();
                c_vts_n_tot_heures = value;
            }
        }
    }
    /* Accesseur de la propriete vts_n_modulation (vts_n_modulation)
    * @return c_vts_n_modulation */
    public double vts_n_modulation
    {
        get{return c_vts_n_modulation;}
        set
        {
            if(c_vts_n_modulation != value)
            {
                CreerClone();
                c_vts_n_modulation = value;
            }
        }
    }
    /* Accesseur de la propriete vts_n_ecart_theorique (vts_n_ecart_theorique)
    * @return c_vts_n_ecart_theorique */
    public double vts_n_ecart_theorique
    {
        get{return c_vts_n_ecart_theorique;}
        set
        {
            if(c_vts_n_ecart_theorique != value)
            {
                CreerClone();
                c_vts_n_ecart_theorique = value;
            }
        }
    }
    /* Accesseur de la propriete vts_n_total_ok (vts_n_total_ok)
    * @return c_vts_n_total_ok */
    public Int64 vts_n_total_ok
    {
        get{return c_vts_n_total_ok;}
        set
        {
            if(c_vts_n_total_ok != value)
            {
                CreerClone();
                c_vts_n_total_ok = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_vts_cn.ToString());
		l_Valeurs.Add(c_vts_n_nb_heure_sup.ToString());
		l_Valeurs.Add(c_vts_n_validation_ok.ToString());
		l_Valeurs.Add(c_vts_sem_cn.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_vts_n_recup_possible.ToString());
		l_Valeurs.Add(c_vts_n_heures_recuperee.ToString());
		l_Valeurs.Add(c_vts_n_tot_heures.ToString());
		l_Valeurs.Add(c_vts_n_modulation.ToString());
		l_Valeurs.Add(c_vts_n_ecart_theorique.ToString());
		l_Valeurs.Add(c_vts_n_total_ok.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("vts_cn");
		l_Noms.Add("vts_n_nb_heure_sup");
		l_Noms.Add("vts_n_validation_ok");
		l_Noms.Add("vts_sem_cn");
		l_Noms.Add("personnels");
		l_Noms.Add("vts_n_recup_possible");
		l_Noms.Add("vts_n_heures_recuperee");
		l_Noms.Add("vts_n_tot_heures");
		l_Noms.Add("vts_n_modulation");
		l_Noms.Add("vts_n_ecart_theorique");
		l_Noms.Add("vts_n_total_ok");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
