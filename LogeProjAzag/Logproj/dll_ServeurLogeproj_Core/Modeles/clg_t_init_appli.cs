namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_init_appli </summary>
public partial class clg_t_init_appli : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_init_appli;
    protected clg_Modele c_Modele;

	private Int64 c_t_ini_cn;
	private string c_t_ini_a_lib;
	private string c_t_ini_m_comment;
	private List<clg_init_appli> c_Listeinit_appli;


    private void Init()
    {
		c_Listeinit_appli = new List<clg_init_appli>();

    }

    public override void Detruit()
    {
		c_Listeinit_appli.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_init_appli(clg_Modele pModele, Int64 pt_ini_cn, bool pAMAJ = false) : base(pModele, pt_ini_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_init_appli(clg_Modele pModele, Int64 pt_ini_cn, string pt_ini_a_lib, string pt_ini_m_comment, bool pAMAJ = true) : base(pModele, pt_ini_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_ini_cn != Int64.MinValue)
            c_t_ini_cn = pt_ini_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_ini_a_lib != null)
            c_t_ini_a_lib = pt_ini_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_t_ini_m_comment = pt_ini_m_comment;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_ini_cn, string pt_ini_a_lib, string pt_ini_m_comment)
    {   
		        c_t_ini_cn = pt_ini_cn;
		        c_t_ini_a_lib = pt_ini_a_lib;
		        c_t_ini_m_comment = pt_ini_m_comment;

        base.Initialise(pt_ini_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_init_appli; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_init_appli.Dictionnaire.Add(t_ini_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_init_appli.Dictionnaire.Remove(t_ini_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_init_appli l_Clone = (clg_t_init_appli) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeinit_appli.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_init_appli(null, t_ini_cn, t_ini_a_lib, t_ini_m_comment,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_init_appli l_clone = (clg_t_init_appli) this.Clone;
		c_t_ini_cn = l_clone.t_ini_cn;
		c_t_ini_a_lib = l_clone.t_ini_a_lib;
		c_t_ini_m_comment = l_clone.t_ini_m_comment;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_init_appli.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_init_appli.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_init_appli.Delete(this);
    }

    /* Accesseur de la propriete t_ini_cn (t_ini_cn)
    * @return c_t_ini_cn */
    public Int64 t_ini_cn
    {
        get{return c_t_ini_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_ini_cn != value)
                {
                    CreerClone();
                    c_t_ini_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_ini_a_lib (t_ini_a_lib)
    * @return c_t_ini_a_lib */
    public string t_ini_a_lib
    {
        get{return c_t_ini_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_t_ini_a_lib != value)
                {
                    CreerClone();
                    c_t_ini_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_ini_m_comment (t_ini_m_comment)
    * @return c_t_ini_m_comment */
    public string t_ini_m_comment
    {
        get{return c_t_ini_m_comment;}
        set
        {
            if(c_t_ini_m_comment != value)
            {
                CreerClone();
                c_t_ini_m_comment = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type init_appli */
    public List<clg_init_appli> Listeinit_appli
    {
        get { return c_Listeinit_appli; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_ini_cn.ToString());
		l_Valeurs.Add(c_t_ini_a_lib.ToString());
		l_Valeurs.Add(c_t_ini_m_comment.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_ini_cn");
		l_Noms.Add("t_ini_a_lib");
		l_Noms.Add("t_ini_m_comment");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
