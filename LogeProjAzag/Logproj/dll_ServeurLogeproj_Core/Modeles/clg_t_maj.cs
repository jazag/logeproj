namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_maj </summary>
public partial class clg_t_maj : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_maj;
    protected clg_Modele c_Modele;

	private Int64 c_t_maj_cn;
	private string c_t_maj_a_libel;
	private List<clg_infos_base> c_Listeinfos_base;


    private void Init()
    {
		c_Listeinfos_base = new List<clg_infos_base>();

    }

    public override void Detruit()
    {
		c_Listeinfos_base.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_maj(clg_Modele pModele, Int64 pt_maj_cn, bool pAMAJ = false) : base(pModele, pt_maj_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_maj(clg_Modele pModele, Int64 pt_maj_cn, string pt_maj_a_libel, bool pAMAJ = true) : base(pModele, pt_maj_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_maj_cn != Int64.MinValue)
            c_t_maj_cn = pt_maj_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_t_maj_a_libel = pt_maj_a_libel;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_maj_cn, string pt_maj_a_libel)
    {   
		        c_t_maj_cn = pt_maj_cn;
		        c_t_maj_a_libel = pt_maj_a_libel;

        base.Initialise(pt_maj_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_maj; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_maj.Dictionnaire.Add(t_maj_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_maj.Dictionnaire.Remove(t_maj_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_maj l_Clone = (clg_t_maj) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeinfos_base.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_maj(null, t_maj_cn, t_maj_a_libel,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_maj l_clone = (clg_t_maj) this.Clone;
		c_t_maj_cn = l_clone.t_maj_cn;
		c_t_maj_a_libel = l_clone.t_maj_a_libel;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_maj.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_maj.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_maj.Delete(this);
    }

    /* Accesseur de la propriete t_maj_cn (t_maj_cn)
    * @return c_t_maj_cn */
    public Int64 t_maj_cn
    {
        get{return c_t_maj_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_maj_cn != value)
                {
                    CreerClone();
                    c_t_maj_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_maj_a_libel (t_maj_a_libel)
    * @return c_t_maj_a_libel */
    public string t_maj_a_libel
    {
        get{return c_t_maj_a_libel;}
        set
        {
            if(c_t_maj_a_libel != value)
            {
                CreerClone();
                c_t_maj_a_libel = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type infos_base */
    public List<clg_infos_base> Listeinfos_base
    {
        get { return c_Listeinfos_base; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_maj_cn.ToString());
		l_Valeurs.Add(c_t_maj_a_libel.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_maj_cn");
		l_Noms.Add("t_maj_a_libel");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
