namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : forfait_personnel </summary>
public partial class clg_forfait_personnel : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLforfait_personnel;
    protected clg_Modele c_Modele;

	private Int64 c_frp_cn;
	private clg_personnels c_personnels;
	private clg_forfait c_forfait;
	private DateTime c_frp_d_debut;
	private Int64 c_frp_n_pourcentage;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_personnels = null;
		this.c_forfait = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_forfait_personnel(clg_Modele pModele, Int64 pfrp_cn, bool pAMAJ = false) : base(pModele, pfrp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_forfait_personnel(clg_Modele pModele, Int64 pfrp_cn, clg_personnels ppersonnels, clg_forfait pforfait, DateTime pfrp_d_debut, Int64 pfrp_n_pourcentage, bool pAMAJ = true) : base(pModele, pfrp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfrp_cn != Int64.MinValue)
            c_frp_cn = pfrp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_personnels = ppersonnels;
		        c_forfait = pforfait;
		        c_frp_d_debut = pfrp_d_debut;
		        c_frp_n_pourcentage = pfrp_n_pourcentage;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfrp_cn, Int64 ppersonnels, Int64 pforfait, DateTime pfrp_d_debut, Int64 pfrp_n_pourcentage)
    {   
		        c_frp_cn = pfrp_cn;
		c_dicReferences.Add("personnels", ppersonnels);
		c_dicReferences.Add("forfait", pforfait);
		        c_frp_d_debut = pfrp_d_debut;
		        c_frp_n_pourcentage = pfrp_n_pourcentage;

        base.Initialise(pfrp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLforfait_personnel; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_forfait = (clg_forfait) c_ModeleBase.RenvoieObjet(c_dicReferences["forfait"], "clg_forfait");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeforfait_personnel.Dictionnaire.Add(frp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeforfait_personnel.Dictionnaire.Remove(frp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listeforfait_personnel.Contains(this)) c_personnels.Listeforfait_personnel.Add(this);
		if(c_forfait != null)if(!c_forfait.Listeforfait_personnel.Contains(this)) c_forfait.Listeforfait_personnel.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_forfait_personnel l_Clone = (clg_forfait_personnel) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listeforfait_personnel.Contains(this)) l_Clone.personnels.Listeforfait_personnel.Remove(this);
		if(l_Clone.forfait != null)if(l_Clone.forfait.Listeforfait_personnel.Contains(this)) l_Clone.forfait.Listeforfait_personnel.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_forfait_personnel(null, frp_cn, personnels, forfait, frp_d_debut, frp_n_pourcentage,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_forfait_personnel l_clone = (clg_forfait_personnel) this.Clone;
		c_frp_cn = l_clone.frp_cn;
		c_personnels = l_clone.personnels;
		c_forfait = l_clone.forfait;
		c_frp_d_debut = l_clone.frp_d_debut;
		c_frp_n_pourcentage = l_clone.frp_n_pourcentage;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLforfait_personnel.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLforfait_personnel.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLforfait_personnel.Delete(this);
    }

    /* Accesseur de la propriete frp_cn (frp_cn)
    * @return c_frp_cn */
    public Int64 frp_cn
    {
        get{return c_frp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_frp_cn != value)
                {
                    CreerClone();
                    c_frp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (frp_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete forfait (frp_frf_cn)
    * @return c_forfait */
    public clg_forfait forfait
    {
        get{return c_forfait;}
        set
        {
            if(c_forfait != value)
            {
                CreerClone();
                c_forfait = value;
            }
        }
    }
    /* Accesseur de la propriete frp_d_debut (frp_d_debut)
    * @return c_frp_d_debut */
    public DateTime frp_d_debut
    {
        get{return c_frp_d_debut;}
        set
        {
            if(c_frp_d_debut != value)
            {
                CreerClone();
                c_frp_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete frp_n_pourcentage (frp_n_pourcentage)
    * @return c_frp_n_pourcentage */
    public Int64 frp_n_pourcentage
    {
        get{return c_frp_n_pourcentage;}
        set
        {
            if(c_frp_n_pourcentage != value)
            {
                CreerClone();
                c_frp_n_pourcentage = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_frp_cn.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_forfait==null ? "-1" : c_forfait.ID.ToString());
		l_Valeurs.Add(c_frp_d_debut.ToString());
		l_Valeurs.Add(c_frp_n_pourcentage.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("frp_cn");
		l_Noms.Add("personnels");
		l_Noms.Add("forfait");
		l_Noms.Add("frp_d_debut");
		l_Noms.Add("frp_n_pourcentage");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
