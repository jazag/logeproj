namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : init_appli </summary>
public partial class clg_init_appli : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLinit_appli;
    protected clg_Modele c_Modele;

	private Int64 c_ini_cn;
	private clg_utilisateur c_utilisateur;
	private DateTime c_ini_d_activation;
	private clg_t_init_appli c_t_init_appli;
	private string c_ini_a_valeur;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_utilisateur = null;
		this.c_t_init_appli = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_init_appli(clg_Modele pModele, Int64 pini_cn, bool pAMAJ = false) : base(pModele, pini_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_init_appli(clg_Modele pModele, Int64 pini_cn, clg_utilisateur putilisateur, DateTime pini_d_activation, clg_t_init_appli pt_init_appli, string pini_a_valeur, bool pAMAJ = true) : base(pModele, pini_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pini_cn != Int64.MinValue)
            c_ini_cn = pini_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(putilisateur != null)
            c_utilisateur = putilisateur;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pini_d_activation != DateTime.MinValue)
            c_ini_d_activation = pini_d_activation;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_init_appli != null)
            c_t_init_appli = pt_init_appli;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_ini_a_valeur = pini_a_valeur;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pini_cn, Int64 putilisateur, DateTime pini_d_activation, Int64 pt_init_appli, string pini_a_valeur)
    {   
		        c_ini_cn = pini_cn;
		c_dicReferences.Add("utilisateur", putilisateur);
		        c_ini_d_activation = pini_d_activation;
		c_dicReferences.Add("t_init_appli", pt_init_appli);
		        c_ini_a_valeur = pini_a_valeur;

        base.Initialise(pini_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLinit_appli; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_utilisateur = (clg_utilisateur) c_ModeleBase.RenvoieObjet(c_dicReferences["utilisateur"], "clg_utilisateur");
		c_t_init_appli = (clg_t_init_appli) c_ModeleBase.RenvoieObjet(c_dicReferences["t_init_appli"], "clg_t_init_appli");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeinit_appli.Dictionnaire.Add(ini_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeinit_appli.Dictionnaire.Remove(ini_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_utilisateur != null)if(!c_utilisateur.Listeinit_appli.Contains(this)) c_utilisateur.Listeinit_appli.Add(this);
		if(c_t_init_appli != null)if(!c_t_init_appli.Listeinit_appli.Contains(this)) c_t_init_appli.Listeinit_appli.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_init_appli l_Clone = (clg_init_appli) Clone;
		if(l_Clone.utilisateur != null)if(l_Clone.utilisateur.Listeinit_appli.Contains(this)) l_Clone.utilisateur.Listeinit_appli.Remove(this);
		if(l_Clone.t_init_appli != null)if(l_Clone.t_init_appli.Listeinit_appli.Contains(this)) l_Clone.t_init_appli.Listeinit_appli.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_init_appli(null, ini_cn, utilisateur, ini_d_activation, t_init_appli, ini_a_valeur,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_init_appli l_clone = (clg_init_appli) this.Clone;
		c_ini_cn = l_clone.ini_cn;
		c_utilisateur = l_clone.utilisateur;
		c_ini_d_activation = l_clone.ini_d_activation;
		c_t_init_appli = l_clone.t_init_appli;
		c_ini_a_valeur = l_clone.ini_a_valeur;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLinit_appli.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLinit_appli.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLinit_appli.Delete(this);
    }

    /* Accesseur de la propriete ini_cn (ini_cn)
    * @return c_ini_cn */
    public Int64 ini_cn
    {
        get{return c_ini_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ini_cn != value)
                {
                    CreerClone();
                    c_ini_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete utilisateur (ini_uti_cn)
    * @return c_utilisateur */
    public clg_utilisateur utilisateur
    {
        get{return c_utilisateur;}
        set
        {
            if(value != null)
            {
                if(c_utilisateur != value)
                {
                    CreerClone();
                    c_utilisateur = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ini_d_activation (ini_d_activation)
    * @return c_ini_d_activation */
    public DateTime ini_d_activation
    {
        get{return c_ini_d_activation;}
        set
        {
            if(value != null)
            {
                if(c_ini_d_activation != value)
                {
                    CreerClone();
                    c_ini_d_activation = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_init_appli (ini_t_ini_cn)
    * @return c_t_init_appli */
    public clg_t_init_appli t_init_appli
    {
        get{return c_t_init_appli;}
        set
        {
            if(value != null)
            {
                if(c_t_init_appli != value)
                {
                    CreerClone();
                    c_t_init_appli = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ini_a_valeur (ini_a_valeur)
    * @return c_ini_a_valeur */
    public string ini_a_valeur
    {
        get{return c_ini_a_valeur;}
        set
        {
            if(c_ini_a_valeur != value)
            {
                CreerClone();
                c_ini_a_valeur = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ini_cn.ToString());
		l_Valeurs.Add(c_utilisateur==null ? "-1" : c_utilisateur.ID.ToString());
		l_Valeurs.Add(c_ini_d_activation.ToString());
		l_Valeurs.Add(c_t_init_appli==null ? "-1" : c_t_init_appli.ID.ToString());
		l_Valeurs.Add(c_ini_a_valeur.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ini_cn");
		l_Noms.Add("utilisateur");
		l_Noms.Add("ini_d_activation");
		l_Noms.Add("t_init_appli");
		l_Noms.Add("ini_a_valeur");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
