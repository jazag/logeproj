namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : heures_sup_prevues </summary>
public partial class clg_heures_sup_prevues : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLheures_sup_prevues;
    protected clg_Modele c_Modele;

	private Int64 c_hsp_cn;
	private double c_hsp_n_nb_heure_sup;
	private Int64 c_hsp_sem_cn;
	private Int64 c_hsp_per_cn;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_heures_sup_prevues(clg_Modele pModele, Int64 phsp_cn, bool pAMAJ = false) : base(pModele, phsp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_heures_sup_prevues(clg_Modele pModele, Int64 phsp_cn, double phsp_n_nb_heure_sup, Int64 phsp_sem_cn, Int64 phsp_per_cn, bool pAMAJ = true) : base(pModele, phsp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(phsp_cn != Int64.MinValue)
            c_hsp_cn = phsp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_hsp_n_nb_heure_sup = phsp_n_nb_heure_sup;
		        if(phsp_sem_cn != Int64.MinValue)
            c_hsp_sem_cn = phsp_sem_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(phsp_per_cn != Int64.MinValue)
            c_hsp_per_cn = phsp_per_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 phsp_cn, double phsp_n_nb_heure_sup, Int64 phsp_sem_cn, Int64 phsp_per_cn)
    {   
		        c_hsp_cn = phsp_cn;
		        c_hsp_n_nb_heure_sup = phsp_n_nb_heure_sup;
		        c_hsp_sem_cn = phsp_sem_cn;
		        c_hsp_per_cn = phsp_per_cn;

        base.Initialise(phsp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLheures_sup_prevues; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeheures_sup_prevues.Dictionnaire.Add(hsp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeheures_sup_prevues.Dictionnaire.Remove(hsp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_heures_sup_prevues l_Clone = (clg_heures_sup_prevues) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_heures_sup_prevues(null, hsp_cn, hsp_n_nb_heure_sup, hsp_sem_cn, hsp_per_cn,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_heures_sup_prevues l_clone = (clg_heures_sup_prevues) this.Clone;
		c_hsp_cn = l_clone.hsp_cn;
		c_hsp_n_nb_heure_sup = l_clone.hsp_n_nb_heure_sup;
		c_hsp_sem_cn = l_clone.hsp_sem_cn;
		c_hsp_per_cn = l_clone.hsp_per_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLheures_sup_prevues.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLheures_sup_prevues.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLheures_sup_prevues.Delete(this);
    }

    /* Accesseur de la propriete hsp_cn (hsp_cn)
    * @return c_hsp_cn */
    public Int64 hsp_cn
    {
        get{return c_hsp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_hsp_cn != value)
                {
                    CreerClone();
                    c_hsp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete hsp_n_nb_heure_sup (hsp_n_nb_heure_sup)
    * @return c_hsp_n_nb_heure_sup */
    public double hsp_n_nb_heure_sup
    {
        get{return c_hsp_n_nb_heure_sup;}
        set
        {
            if(c_hsp_n_nb_heure_sup != value)
            {
                CreerClone();
                c_hsp_n_nb_heure_sup = value;
            }
        }
    }
    /* Accesseur de la propriete hsp_sem_cn (hsp_sem_cn)
    * @return c_hsp_sem_cn */
    public Int64 hsp_sem_cn
    {
        get{return c_hsp_sem_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_hsp_sem_cn != value)
                {
                    CreerClone();
                    c_hsp_sem_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete hsp_per_cn (hsp_per_cn)
    * @return c_hsp_per_cn */
    public Int64 hsp_per_cn
    {
        get{return c_hsp_per_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_hsp_per_cn != value)
                {
                    CreerClone();
                    c_hsp_per_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_hsp_cn.ToString());
		l_Valeurs.Add(c_hsp_n_nb_heure_sup.ToString());
		l_Valeurs.Add(c_hsp_sem_cn.ToString());
		l_Valeurs.Add(c_hsp_per_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("hsp_cn");
		l_Noms.Add("hsp_n_nb_heure_sup");
		l_Noms.Add("hsp_sem_cn");
		l_Noms.Add("hsp_per_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
