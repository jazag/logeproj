namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : fonctions_perso </summary>
public partial class clg_fonctions_perso : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfonctions_perso;
    protected clg_Modele c_Modele;

	private Int64 c_fctp_cn;
	private clg_personnels c_personnels;
	private clg_t_fonctions c_t_fonctions;
	private DateTime c_fctp_d_debut;
	private DateTime c_fctp_d_fin;
	private bool c_fctp_b_archive;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_personnels = null;
		this.c_t_fonctions = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_fonctions_perso(clg_Modele pModele, Int64 pfctp_cn, bool pAMAJ = false) : base(pModele, pfctp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_fonctions_perso(clg_Modele pModele, Int64 pfctp_cn, clg_personnels ppersonnels, clg_t_fonctions pt_fonctions, DateTime pfctp_d_debut, DateTime pfctp_d_fin, bool pfctp_b_archive, bool pAMAJ = true) : base(pModele, pfctp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfctp_cn != Int64.MinValue)
            c_fctp_cn = pfctp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(ppersonnels != null)
            c_personnels = ppersonnels;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pt_fonctions != null)
            c_t_fonctions = pt_fonctions;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pfctp_d_debut != DateTime.MinValue)
            c_fctp_d_debut = pfctp_d_debut;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_fctp_d_fin = pfctp_d_fin;
		        c_fctp_b_archive = pfctp_b_archive;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfctp_cn, Int64 ppersonnels, Int64 pt_fonctions, DateTime pfctp_d_debut, DateTime pfctp_d_fin, bool pfctp_b_archive)
    {   
		        c_fctp_cn = pfctp_cn;
		c_dicReferences.Add("personnels", ppersonnels);
		c_dicReferences.Add("t_fonctions", pt_fonctions);
		        c_fctp_d_debut = pfctp_d_debut;
		        c_fctp_d_fin = pfctp_d_fin;
		        c_fctp_b_archive = pfctp_b_archive;

        base.Initialise(pfctp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfonctions_perso; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_t_fonctions = (clg_t_fonctions) c_ModeleBase.RenvoieObjet(c_dicReferences["t_fonctions"], "clg_t_fonctions");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefonctions_perso.Dictionnaire.Add(fctp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefonctions_perso.Dictionnaire.Remove(fctp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listefonctions_perso.Contains(this)) c_personnels.Listefonctions_perso.Add(this);
		if(c_t_fonctions != null)if(!c_t_fonctions.Listefonctions_perso.Contains(this)) c_t_fonctions.Listefonctions_perso.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_fonctions_perso l_Clone = (clg_fonctions_perso) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listefonctions_perso.Contains(this)) l_Clone.personnels.Listefonctions_perso.Remove(this);
		if(l_Clone.t_fonctions != null)if(l_Clone.t_fonctions.Listefonctions_perso.Contains(this)) l_Clone.t_fonctions.Listefonctions_perso.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_fonctions_perso(null, fctp_cn, personnels, t_fonctions, fctp_d_debut, fctp_d_fin, fctp_b_archive,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_fonctions_perso l_clone = (clg_fonctions_perso) this.Clone;
		c_fctp_cn = l_clone.fctp_cn;
		c_personnels = l_clone.personnels;
		c_t_fonctions = l_clone.t_fonctions;
		c_fctp_d_debut = l_clone.fctp_d_debut;
		c_fctp_d_fin = l_clone.fctp_d_fin;
		c_fctp_b_archive = l_clone.fctp_b_archive;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfonctions_perso.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfonctions_perso.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfonctions_perso.Delete(this);
    }

    /* Accesseur de la propriete fctp_cn (fctp_cn)
    * @return c_fctp_cn */
    public Int64 fctp_cn
    {
        get{return c_fctp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_fctp_cn != value)
                {
                    CreerClone();
                    c_fctp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (fctp_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(value != null)
            {
                if(c_personnels != value)
                {
                    CreerClone();
                    c_personnels = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_fonctions (fctp_t_fct_cn)
    * @return c_t_fonctions */
    public clg_t_fonctions t_fonctions
    {
        get{return c_t_fonctions;}
        set
        {
            if(value != null)
            {
                if(c_t_fonctions != value)
                {
                    CreerClone();
                    c_t_fonctions = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete fctp_d_debut (fctp_d_debut)
    * @return c_fctp_d_debut */
    public DateTime fctp_d_debut
    {
        get{return c_fctp_d_debut;}
        set
        {
            if(value != null)
            {
                if(c_fctp_d_debut != value)
                {
                    CreerClone();
                    c_fctp_d_debut = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete fctp_d_fin (fctp_d_fin)
    * @return c_fctp_d_fin */
    public DateTime fctp_d_fin
    {
        get{return c_fctp_d_fin;}
        set
        {
            if(c_fctp_d_fin != value)
            {
                CreerClone();
                c_fctp_d_fin = value;
            }
        }
    }
    /* Accesseur de la propriete fctp_b_archive (fctp_b_archive)
    * @return c_fctp_b_archive */
    public bool fctp_b_archive
    {
        get{return c_fctp_b_archive;}
        set
        {
            if(c_fctp_b_archive != value)
            {
                CreerClone();
                c_fctp_b_archive = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_fctp_cn.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_t_fonctions==null ? "-1" : c_t_fonctions.ID.ToString());
		l_Valeurs.Add(c_fctp_d_debut.ToString());
		l_Valeurs.Add(c_fctp_d_fin.ToString());
		l_Valeurs.Add(c_fctp_b_archive.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("fctp_cn");
		l_Noms.Add("personnels");
		l_Noms.Add("t_fonctions");
		l_Noms.Add("fctp_d_debut");
		l_Noms.Add("fctp_d_fin");
		l_Noms.Add("fctp_b_archive");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
