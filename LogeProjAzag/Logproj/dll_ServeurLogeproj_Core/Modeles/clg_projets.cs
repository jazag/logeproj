namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : projets </summary>
public partial class clg_projets : clg_elements
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLprojets;
    protected clg_Modele c_Modele;

	private clg_t_projet c_t_projet;
	private clg_personnels c_personnels;
	private clg_t_cout_projet c_t_cout_projet;
	private Int64 c_prj_acp_cn;
	private List<clg_concepteurs> c_Listeconcepteurs;


    private void Init()
    {
		c_Listeconcepteurs = new List<clg_concepteurs>();

    }

    public override void Detruit()
    {
		c_Listeconcepteurs.Clear();
		this.c_t_projet = null;
		this.c_personnels = null;
		this.c_t_cout_projet = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_projets(clg_Modele pModele, Int64 pele_cn, bool pAMAJ = false) : base(pModele, pele_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_projets(clg_Modele pModele, Int64 pelements3, clg_t_projet pt_projet, clg_personnels ppersonnels, clg_t_cout_projet pt_cout_projet, Int64 pprj_acp_cn, clg_elements pelements, Int64 pele_n_niveau, string pele_a_lib, clg_t_element pt_element, string pele_a_code, clg_elements pelements2, Int64 pele_n_archive, DateTime pele_d_debut_prev, DateTime pele_d_fin_reelle, DateTime pele_d_fin_prev, DateTime pele_d_debut_reelle, DateTime pele_d_archivage, Int64 pele_n_visibleetat, bool pAMAJ = true) : base(pModele, pelements3, pelements, pele_n_niveau, pele_a_lib, pt_element, pele_a_code, pelements2, pele_n_archive, pele_d_debut_prev, pele_d_fin_reelle, pele_d_fin_prev, pele_d_debut_reelle, pele_d_archivage, pele_n_visibleetat, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        c_t_projet = pt_projet;
		        c_personnels = ppersonnels;
		        if(pt_cout_projet != null)
            c_t_cout_projet = pt_cout_projet;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_prj_acp_cn = pprj_acp_cn;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pelements3, Int64 pt_projet, Int64 ppersonnels, Int64 pt_cout_projet, Int64 pprj_acp_cn, Int64 pelements, Int64 pele_n_niveau, string pele_a_lib, Int64 pt_element, string pele_a_code, Int64 pelements2, Int64 pele_n_archive, DateTime pele_d_debut_prev, DateTime pele_d_fin_reelle, DateTime pele_d_fin_prev, DateTime pele_d_debut_reelle, DateTime pele_d_archivage, Int64 pele_n_visibleetat)
    {   
		c_dicReferences.Add("t_projet", pt_projet);
		c_dicReferences.Add("personnels", ppersonnels);
		c_dicReferences.Add("t_cout_projet", pt_cout_projet);
		        c_prj_acp_cn = pprj_acp_cn;

        base.Initialise(pelements3, pelements, pele_n_niveau, pele_a_lib, pt_element, pele_a_code, pelements2, pele_n_archive, pele_d_debut_prev, pele_d_fin_reelle, pele_d_fin_prev, pele_d_debut_reelle, pele_d_archivage, pele_n_visibleetat);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLprojets; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		base.CreeLiens();
		c_t_projet = (clg_t_projet) c_ModeleBase.RenvoieObjet(c_dicReferences["t_projet"], "clg_t_projet");
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_t_cout_projet = (clg_t_cout_projet) c_ModeleBase.RenvoieObjet(c_dicReferences["t_cout_projet"], "clg_t_cout_projet");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeprojets.Dictionnaire.Add(ele_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeprojets.Dictionnaire.Remove(ele_cn);
		base.SupprimeDansListe();

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		base.CreeReferences();
		if(c_t_projet != null)if(!c_t_projet.Listeprojets.Contains(this)) c_t_projet.Listeprojets.Add(this);
		if(c_personnels != null)if(!c_personnels.Listeprojets.Contains(this)) c_personnels.Listeprojets.Add(this);
		if(c_t_cout_projet != null)if(!c_t_cout_projet.Listeprojets.Contains(this)) c_t_cout_projet.Listeprojets.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_projets l_Clone = (clg_projets) Clone;
		base.DetruitReferences();
		if(l_Clone.t_projet != null)if(l_Clone.t_projet.Listeprojets.Contains(this)) l_Clone.t_projet.Listeprojets.Remove(this);
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listeprojets.Contains(this)) l_Clone.personnels.Listeprojets.Remove(this);
		if(l_Clone.t_cout_projet != null)if(l_Clone.t_cout_projet.Listeprojets.Contains(this)) l_Clone.t_cout_projet.Listeprojets.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeconcepteurs.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_projets(null, ele_cn, t_projet, personnels, t_cout_projet, prj_acp_cn, elements, ele_n_niveau, ele_a_lib, t_element, ele_a_code, elements2, ele_n_archive, ele_d_debut_prev, ele_d_fin_reelle, ele_d_fin_prev, ele_d_debut_reelle, ele_d_archivage, ele_n_visibleetat,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_projets l_clone = (clg_projets) this.Clone;
		c_t_projet = l_clone.t_projet;
		c_personnels = l_clone.personnels;
		c_t_cout_projet = l_clone.t_cout_projet;
		c_prj_acp_cn = l_clone.prj_acp_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLprojets.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLprojets.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLprojets.Delete(this);
    }

    /* Accesseur de la propriete t_projet (prj_t_prj_cn)
    * @return c_t_projet */
    public clg_t_projet t_projet
    {
        get{return c_t_projet;}
        set
        {
            if(c_t_projet != value)
            {
                CreerClone();
                c_t_projet = value;
            }
        }
    }
    /* Accesseur de la propriete personnels (prj_per_cn_resp)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete t_cout_projet (prj_t_cout_projet_cn)
    * @return c_t_cout_projet */
    public clg_t_cout_projet t_cout_projet
    {
        get{return c_t_cout_projet;}
        set
        {
            if(value != null)
            {
                if(c_t_cout_projet != value)
                {
                    CreerClone();
                    c_t_cout_projet = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete prj_acp_cn (prj_acp_cn)
    * @return c_prj_acp_cn */
    public Int64 prj_acp_cn
    {
        get{return c_prj_acp_cn;}
        set
        {
            if(c_prj_acp_cn != value)
            {
                CreerClone();
                c_prj_acp_cn = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type concepteurs */
    public List<clg_concepteurs> Listeconcepteurs
    {
        get { return c_Listeconcepteurs; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_projet==null ? "-1" : c_t_projet.ID.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_t_cout_projet==null ? "-1" : c_t_cout_projet.ID.ToString());
		l_Valeurs.Add(c_prj_acp_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("elements3");
		l_Noms.Add("t_projet");
		l_Noms.Add("personnels");
		l_Noms.Add("t_cout_projet");
		l_Noms.Add("prj_acp_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
