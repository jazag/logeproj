namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : financeurs </summary>
public partial class clg_financeurs : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfinanceurs;
    protected clg_Modele c_Modele;

	private Int64 c_fin_cn;
	private string c_fin_a_code;
	private string c_fin_a_nom;
	private string c_fin_a_lib;
	private Int64 c_fin_n_archive;
	private string c_fin_a_adresse;
	private string c_fin_a_cp;
	private string c_fin_a_ville;
	private string c_fin_a_tel;
	private string c_fin_a_fax;
	private string c_fin_a_mail;
	private List<clg_arretes> c_Listearretes;
	private List<clg_comment> c_Listecomment;
	private List<clg_comptes_financeur> c_Listecomptes_financeur;
	private List<clg_contacts> c_Listecontacts;
	private List<clg_demandes_subventions> c_Listedemandes_subventions;
	private List<clg_financement_prevus> c_Listefinancement_prevus;
	private List<clg_tb_sage> c_Listetb_sage;


    private void Init()
    {
		c_Listearretes = new List<clg_arretes>();
		c_Listecomment = new List<clg_comment>();
		c_Listecomptes_financeur = new List<clg_comptes_financeur>();
		c_Listecontacts = new List<clg_contacts>();
		c_Listedemandes_subventions = new List<clg_demandes_subventions>();
		c_Listefinancement_prevus = new List<clg_financement_prevus>();
		c_Listetb_sage = new List<clg_tb_sage>();

    }

    public override void Detruit()
    {
		c_Listearretes.Clear();
		c_Listecomment.Clear();
		c_Listecomptes_financeur.Clear();
		c_Listecontacts.Clear();
		c_Listedemandes_subventions.Clear();
		c_Listefinancement_prevus.Clear();
		c_Listetb_sage.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_financeurs(clg_Modele pModele, Int64 pfin_cn, bool pAMAJ = false) : base(pModele, pfin_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_financeurs(clg_Modele pModele, Int64 pfin_cn, string pfin_a_code, string pfin_a_nom, string pfin_a_lib, Int64 pfin_n_archive, string pfin_a_adresse, string pfin_a_cp, string pfin_a_ville, string pfin_a_tel, string pfin_a_fax, string pfin_a_mail, bool pAMAJ = true) : base(pModele, pfin_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfin_cn != Int64.MinValue)
            c_fin_cn = pfin_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_fin_a_code = pfin_a_code;
		        c_fin_a_nom = pfin_a_nom;
		        if(pfin_a_lib != null)
            c_fin_a_lib = pfin_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_fin_n_archive = pfin_n_archive;
		        c_fin_a_adresse = pfin_a_adresse;
		        c_fin_a_cp = pfin_a_cp;
		        c_fin_a_ville = pfin_a_ville;
		        c_fin_a_tel = pfin_a_tel;
		        c_fin_a_fax = pfin_a_fax;
		        c_fin_a_mail = pfin_a_mail;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfin_cn, string pfin_a_code, string pfin_a_nom, string pfin_a_lib, Int64 pfin_n_archive, string pfin_a_adresse, string pfin_a_cp, string pfin_a_ville, string pfin_a_tel, string pfin_a_fax, string pfin_a_mail)
    {   
		        c_fin_cn = pfin_cn;
		        c_fin_a_code = pfin_a_code;
		        c_fin_a_nom = pfin_a_nom;
		        c_fin_a_lib = pfin_a_lib;
		        c_fin_n_archive = pfin_n_archive;
		        c_fin_a_adresse = pfin_a_adresse;
		        c_fin_a_cp = pfin_a_cp;
		        c_fin_a_ville = pfin_a_ville;
		        c_fin_a_tel = pfin_a_tel;
		        c_fin_a_fax = pfin_a_fax;
		        c_fin_a_mail = pfin_a_mail;

        base.Initialise(pfin_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfinanceurs; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefinanceurs.Dictionnaire.Add(fin_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefinanceurs.Dictionnaire.Remove(fin_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_financeurs l_Clone = (clg_financeurs) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listearretes.Count > 0) c_EstReference = true;
		if(c_Listecomment.Count > 0) c_EstReference = true;
		if(c_Listecomptes_financeur.Count > 0) c_EstReference = true;
		if(c_Listecontacts.Count > 0) c_EstReference = true;
		if(c_Listedemandes_subventions.Count > 0) c_EstReference = true;
		if(c_Listefinancement_prevus.Count > 0) c_EstReference = true;
		if(c_Listetb_sage.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_financeurs(null, fin_cn, fin_a_code, fin_a_nom, fin_a_lib, fin_n_archive, fin_a_adresse, fin_a_cp, fin_a_ville, fin_a_tel, fin_a_fax, fin_a_mail,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_financeurs l_clone = (clg_financeurs) this.Clone;
		c_fin_cn = l_clone.fin_cn;
		c_fin_a_code = l_clone.fin_a_code;
		c_fin_a_nom = l_clone.fin_a_nom;
		c_fin_a_lib = l_clone.fin_a_lib;
		c_fin_n_archive = l_clone.fin_n_archive;
		c_fin_a_adresse = l_clone.fin_a_adresse;
		c_fin_a_cp = l_clone.fin_a_cp;
		c_fin_a_ville = l_clone.fin_a_ville;
		c_fin_a_tel = l_clone.fin_a_tel;
		c_fin_a_fax = l_clone.fin_a_fax;
		c_fin_a_mail = l_clone.fin_a_mail;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfinanceurs.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfinanceurs.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfinanceurs.Delete(this);
    }

    /* Accesseur de la propriete fin_cn (fin_cn)
    * @return c_fin_cn */
    public Int64 fin_cn
    {
        get{return c_fin_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_fin_cn != value)
                {
                    CreerClone();
                    c_fin_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete fin_a_code (fin_a_code)
    * @return c_fin_a_code */
    public string fin_a_code
    {
        get{return c_fin_a_code;}
        set
        {
            if(c_fin_a_code != value)
            {
                CreerClone();
                c_fin_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete fin_a_nom (fin_a_nom)
    * @return c_fin_a_nom */
    public string fin_a_nom
    {
        get{return c_fin_a_nom;}
        set
        {
            if(c_fin_a_nom != value)
            {
                CreerClone();
                c_fin_a_nom = value;
            }
        }
    }
    /* Accesseur de la propriete fin_a_lib (fin_a_lib)
    * @return c_fin_a_lib */
    public string fin_a_lib
    {
        get{return c_fin_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_fin_a_lib != value)
                {
                    CreerClone();
                    c_fin_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete fin_n_archive (fin_n_archive)
    * @return c_fin_n_archive */
    public Int64 fin_n_archive
    {
        get{return c_fin_n_archive;}
        set
        {
            if(c_fin_n_archive != value)
            {
                CreerClone();
                c_fin_n_archive = value;
            }
        }
    }
    /* Accesseur de la propriete fin_a_adresse (fin_a_adresse)
    * @return c_fin_a_adresse */
    public string fin_a_adresse
    {
        get{return c_fin_a_adresse;}
        set
        {
            if(c_fin_a_adresse != value)
            {
                CreerClone();
                c_fin_a_adresse = value;
            }
        }
    }
    /* Accesseur de la propriete fin_a_cp (fin_a_cp)
    * @return c_fin_a_cp */
    public string fin_a_cp
    {
        get{return c_fin_a_cp;}
        set
        {
            if(c_fin_a_cp != value)
            {
                CreerClone();
                c_fin_a_cp = value;
            }
        }
    }
    /* Accesseur de la propriete fin_a_ville (fin_a_ville)
    * @return c_fin_a_ville */
    public string fin_a_ville
    {
        get{return c_fin_a_ville;}
        set
        {
            if(c_fin_a_ville != value)
            {
                CreerClone();
                c_fin_a_ville = value;
            }
        }
    }
    /* Accesseur de la propriete fin_a_tel (fin_a_tel)
    * @return c_fin_a_tel */
    public string fin_a_tel
    {
        get{return c_fin_a_tel;}
        set
        {
            if(c_fin_a_tel != value)
            {
                CreerClone();
                c_fin_a_tel = value;
            }
        }
    }
    /* Accesseur de la propriete fin_a_fax (fin_a_fax)
    * @return c_fin_a_fax */
    public string fin_a_fax
    {
        get{return c_fin_a_fax;}
        set
        {
            if(c_fin_a_fax != value)
            {
                CreerClone();
                c_fin_a_fax = value;
            }
        }
    }
    /* Accesseur de la propriete fin_a_mail (fin_a_mail)
    * @return c_fin_a_mail */
    public string fin_a_mail
    {
        get{return c_fin_a_mail;}
        set
        {
            if(c_fin_a_mail != value)
            {
                CreerClone();
                c_fin_a_mail = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type arretes */
    public List<clg_arretes> Listearretes
    {
        get { return c_Listearretes; }
    }    /* Accesseur de la liste des objets de type comment */
    public List<clg_comment> Listecomment
    {
        get { return c_Listecomment; }
    }    /* Accesseur de la liste des objets de type comptes_financeur */
    public List<clg_comptes_financeur> Listecomptes_financeur
    {
        get { return c_Listecomptes_financeur; }
    }    /* Accesseur de la liste des objets de type contacts */
    public List<clg_contacts> Listecontacts
    {
        get { return c_Listecontacts; }
    }    /* Accesseur de la liste des objets de type demandes_subventions */
    public List<clg_demandes_subventions> Listedemandes_subventions
    {
        get { return c_Listedemandes_subventions; }
    }    /* Accesseur de la liste des objets de type financement_prevus */
    public List<clg_financement_prevus> Listefinancement_prevus
    {
        get { return c_Listefinancement_prevus; }
    }    /* Accesseur de la liste des objets de type tb_sage */
    public List<clg_tb_sage> Listetb_sage
    {
        get { return c_Listetb_sage; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_fin_cn.ToString());
		l_Valeurs.Add(c_fin_a_code.ToString());
		l_Valeurs.Add(c_fin_a_nom.ToString());
		l_Valeurs.Add(c_fin_a_lib.ToString());
		l_Valeurs.Add(c_fin_n_archive.ToString());
		l_Valeurs.Add(c_fin_a_adresse.ToString());
		l_Valeurs.Add(c_fin_a_cp.ToString());
		l_Valeurs.Add(c_fin_a_ville.ToString());
		l_Valeurs.Add(c_fin_a_tel.ToString());
		l_Valeurs.Add(c_fin_a_fax.ToString());
		l_Valeurs.Add(c_fin_a_mail.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("fin_cn");
		l_Noms.Add("fin_a_code");
		l_Noms.Add("fin_a_nom");
		l_Noms.Add("fin_a_lib");
		l_Noms.Add("fin_n_archive");
		l_Noms.Add("fin_a_adresse");
		l_Noms.Add("fin_a_cp");
		l_Noms.Add("fin_a_ville");
		l_Noms.Add("fin_a_tel");
		l_Noms.Add("fin_a_fax");
		l_Noms.Add("fin_a_mail");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
