namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : contacts </summary>
public partial class clg_contacts : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcontacts;
    protected clg_Modele c_Modele;

	private Int64 c_ctc_cn;
	private string c_ctc_a_nom;
	private string c_ctc_a_prenom;
	private clg_financeurs c_financeurs;
	private clg_t_fonction_contact c_t_fonction_contact;
	private string c_ctc_a_tel;
	private string c_ctc_a_fax;
	private string c_ctc_a_mail;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_financeurs = null;
		this.c_t_fonction_contact = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_contacts(clg_Modele pModele, Int64 pctc_cn, bool pAMAJ = false) : base(pModele, pctc_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_contacts(clg_Modele pModele, Int64 pctc_cn, string pctc_a_nom, string pctc_a_prenom, clg_financeurs pfinanceurs, clg_t_fonction_contact pt_fonction_contact, string pctc_a_tel, string pctc_a_fax, string pctc_a_mail, bool pAMAJ = true) : base(pModele, pctc_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pctc_cn != Int64.MinValue)
            c_ctc_cn = pctc_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pctc_a_nom != null)
            c_ctc_a_nom = pctc_a_nom;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_ctc_a_prenom = pctc_a_prenom;
		        if(pfinanceurs != null)
            c_financeurs = pfinanceurs;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_t_fonction_contact = pt_fonction_contact;
		        c_ctc_a_tel = pctc_a_tel;
		        c_ctc_a_fax = pctc_a_fax;
		        c_ctc_a_mail = pctc_a_mail;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pctc_cn, string pctc_a_nom, string pctc_a_prenom, Int64 pfinanceurs, Int64 pt_fonction_contact, string pctc_a_tel, string pctc_a_fax, string pctc_a_mail)
    {   
		        c_ctc_cn = pctc_cn;
		        c_ctc_a_nom = pctc_a_nom;
		        c_ctc_a_prenom = pctc_a_prenom;
		c_dicReferences.Add("financeurs", pfinanceurs);
		c_dicReferences.Add("t_fonction_contact", pt_fonction_contact);
		        c_ctc_a_tel = pctc_a_tel;
		        c_ctc_a_fax = pctc_a_fax;
		        c_ctc_a_mail = pctc_a_mail;

        base.Initialise(pctc_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcontacts; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_financeurs = (clg_financeurs) c_ModeleBase.RenvoieObjet(c_dicReferences["financeurs"], "clg_financeurs");
		c_t_fonction_contact = (clg_t_fonction_contact) c_ModeleBase.RenvoieObjet(c_dicReferences["t_fonction_contact"], "clg_t_fonction_contact");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecontacts.Dictionnaire.Add(ctc_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecontacts.Dictionnaire.Remove(ctc_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_financeurs != null)if(!c_financeurs.Listecontacts.Contains(this)) c_financeurs.Listecontacts.Add(this);
		if(c_t_fonction_contact != null)if(!c_t_fonction_contact.Listecontacts.Contains(this)) c_t_fonction_contact.Listecontacts.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_contacts l_Clone = (clg_contacts) Clone;
		if(l_Clone.financeurs != null)if(l_Clone.financeurs.Listecontacts.Contains(this)) l_Clone.financeurs.Listecontacts.Remove(this);
		if(l_Clone.t_fonction_contact != null)if(l_Clone.t_fonction_contact.Listecontacts.Contains(this)) l_Clone.t_fonction_contact.Listecontacts.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_contacts(null, ctc_cn, ctc_a_nom, ctc_a_prenom, financeurs, t_fonction_contact, ctc_a_tel, ctc_a_fax, ctc_a_mail,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_contacts l_clone = (clg_contacts) this.Clone;
		c_ctc_cn = l_clone.ctc_cn;
		c_ctc_a_nom = l_clone.ctc_a_nom;
		c_ctc_a_prenom = l_clone.ctc_a_prenom;
		c_financeurs = l_clone.financeurs;
		c_t_fonction_contact = l_clone.t_fonction_contact;
		c_ctc_a_tel = l_clone.ctc_a_tel;
		c_ctc_a_fax = l_clone.ctc_a_fax;
		c_ctc_a_mail = l_clone.ctc_a_mail;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcontacts.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcontacts.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcontacts.Delete(this);
    }

    /* Accesseur de la propriete ctc_cn (ctc_cn)
    * @return c_ctc_cn */
    public Int64 ctc_cn
    {
        get{return c_ctc_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ctc_cn != value)
                {
                    CreerClone();
                    c_ctc_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ctc_a_nom (ctc_a_nom)
    * @return c_ctc_a_nom */
    public string ctc_a_nom
    {
        get{return c_ctc_a_nom;}
        set
        {
            if(value != null)
            {
                if(c_ctc_a_nom != value)
                {
                    CreerClone();
                    c_ctc_a_nom = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ctc_a_prenom (ctc_a_prenom)
    * @return c_ctc_a_prenom */
    public string ctc_a_prenom
    {
        get{return c_ctc_a_prenom;}
        set
        {
            if(c_ctc_a_prenom != value)
            {
                CreerClone();
                c_ctc_a_prenom = value;
            }
        }
    }
    /* Accesseur de la propriete financeurs (ctc_fin_cn)
    * @return c_financeurs */
    public clg_financeurs financeurs
    {
        get{return c_financeurs;}
        set
        {
            if(value != null)
            {
                if(c_financeurs != value)
                {
                    CreerClone();
                    c_financeurs = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_fonction_contact (ctc_t_fct_ctc_cn)
    * @return c_t_fonction_contact */
    public clg_t_fonction_contact t_fonction_contact
    {
        get{return c_t_fonction_contact;}
        set
        {
            if(c_t_fonction_contact != value)
            {
                CreerClone();
                c_t_fonction_contact = value;
            }
        }
    }
    /* Accesseur de la propriete ctc_a_tel (ctc_a_tel)
    * @return c_ctc_a_tel */
    public string ctc_a_tel
    {
        get{return c_ctc_a_tel;}
        set
        {
            if(c_ctc_a_tel != value)
            {
                CreerClone();
                c_ctc_a_tel = value;
            }
        }
    }
    /* Accesseur de la propriete ctc_a_fax (ctc_a_fax)
    * @return c_ctc_a_fax */
    public string ctc_a_fax
    {
        get{return c_ctc_a_fax;}
        set
        {
            if(c_ctc_a_fax != value)
            {
                CreerClone();
                c_ctc_a_fax = value;
            }
        }
    }
    /* Accesseur de la propriete ctc_a_mail (ctc_a_mail)
    * @return c_ctc_a_mail */
    public string ctc_a_mail
    {
        get{return c_ctc_a_mail;}
        set
        {
            if(c_ctc_a_mail != value)
            {
                CreerClone();
                c_ctc_a_mail = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ctc_cn.ToString());
		l_Valeurs.Add(c_ctc_a_nom.ToString());
		l_Valeurs.Add(c_ctc_a_prenom.ToString());
		l_Valeurs.Add(c_financeurs==null ? "-1" : c_financeurs.ID.ToString());
		l_Valeurs.Add(c_t_fonction_contact==null ? "-1" : c_t_fonction_contact.ID.ToString());
		l_Valeurs.Add(c_ctc_a_tel.ToString());
		l_Valeurs.Add(c_ctc_a_fax.ToString());
		l_Valeurs.Add(c_ctc_a_mail.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ctc_cn");
		l_Noms.Add("ctc_a_nom");
		l_Noms.Add("ctc_a_prenom");
		l_Noms.Add("financeurs");
		l_Noms.Add("t_fonction_contact");
		l_Noms.Add("ctc_a_tel");
		l_Noms.Add("ctc_a_fax");
		l_Noms.Add("ctc_a_mail");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
