namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : infos_base </summary>
public partial class clg_infos_base : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLinfos_base;
    protected clg_Modele c_Modele;

	private Int64 c_ifb_cn;
	private string c_ifb_a_guid;
	private string c_ifb_a_nom;
	private clg_infos_conservatoire c_infos_conservatoire;
	private string c_ifb_a_version;
	private clg_t_base c_t_base;
	private Int64 c_ifb_n_synchro;
	private Int64 c_ifb_n_der_id_sync;
	private clg_t_maj c_t_maj;
	private string c_ifb_a_ip;
	private string c_ifb_m_msg_maj;
	private Int64 c_ifb_n_desynchro;
	private List<clg_poste> c_Listeposte;


    private void Init()
    {
		c_Listeposte = new List<clg_poste>();

    }

    public override void Detruit()
    {
		c_Listeposte.Clear();
		this.c_infos_conservatoire = null;
		this.c_t_base = null;
		this.c_t_maj = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_infos_base(clg_Modele pModele, Int64 pifb_cn, bool pAMAJ = false) : base(pModele, pifb_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_infos_base(clg_Modele pModele, Int64 pifb_cn, string pifb_a_guid, string pifb_a_nom, clg_infos_conservatoire pinfos_conservatoire, string pifb_a_version, clg_t_base pt_base, Int64 pifb_n_synchro, Int64 pifb_n_der_id_sync, clg_t_maj pt_maj, string pifb_a_ip, string pifb_m_msg_maj, Int64 pifb_n_desynchro, bool pAMAJ = true) : base(pModele, pifb_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pifb_cn != Int64.MinValue)
            c_ifb_cn = pifb_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pifb_a_guid != null)
            c_ifb_a_guid = pifb_a_guid;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pifb_a_nom != null)
            c_ifb_a_nom = pifb_a_nom;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_infos_conservatoire = pinfos_conservatoire;
		        if(pifb_a_version != null)
            c_ifb_a_version = pifb_a_version;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pt_base != null)
            c_t_base = pt_base;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pifb_n_synchro != Int64.MinValue)
            c_ifb_n_synchro = pifb_n_synchro;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ifb_n_der_id_sync = pifb_n_der_id_sync;
		        c_t_maj = pt_maj;
		        c_ifb_a_ip = pifb_a_ip;
		        c_ifb_m_msg_maj = pifb_m_msg_maj;
		        c_ifb_n_desynchro = pifb_n_desynchro;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pifb_cn, string pifb_a_guid, string pifb_a_nom, Int64 pinfos_conservatoire, string pifb_a_version, Int64 pt_base, Int64 pifb_n_synchro, Int64 pifb_n_der_id_sync, Int64 pt_maj, string pifb_a_ip, string pifb_m_msg_maj, Int64 pifb_n_desynchro)
    {   
		        c_ifb_cn = pifb_cn;
		        c_ifb_a_guid = pifb_a_guid;
		        c_ifb_a_nom = pifb_a_nom;
		c_dicReferences.Add("infos_conservatoire", pinfos_conservatoire);
		        c_ifb_a_version = pifb_a_version;
		c_dicReferences.Add("t_base", pt_base);
		        c_ifb_n_synchro = pifb_n_synchro;
		        c_ifb_n_der_id_sync = pifb_n_der_id_sync;
		c_dicReferences.Add("t_maj", pt_maj);
		        c_ifb_a_ip = pifb_a_ip;
		        c_ifb_m_msg_maj = pifb_m_msg_maj;
		        c_ifb_n_desynchro = pifb_n_desynchro;

        base.Initialise(pifb_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLinfos_base; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_infos_conservatoire = (clg_infos_conservatoire) c_ModeleBase.RenvoieObjet(c_dicReferences["infos_conservatoire"], "clg_infos_conservatoire");
		c_t_base = (clg_t_base) c_ModeleBase.RenvoieObjet(c_dicReferences["t_base"], "clg_t_base");
		c_t_maj = (clg_t_maj) c_ModeleBase.RenvoieObjet(c_dicReferences["t_maj"], "clg_t_maj");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeinfos_base.Dictionnaire.Add(ifb_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeinfos_base.Dictionnaire.Remove(ifb_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_infos_conservatoire != null)if(!c_infos_conservatoire.Listeinfos_base.Contains(this)) c_infos_conservatoire.Listeinfos_base.Add(this);
		if(c_t_base != null)if(!c_t_base.Listeinfos_base.Contains(this)) c_t_base.Listeinfos_base.Add(this);
		if(c_t_maj != null)if(!c_t_maj.Listeinfos_base.Contains(this)) c_t_maj.Listeinfos_base.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_infos_base l_Clone = (clg_infos_base) Clone;
		if(l_Clone.infos_conservatoire != null)if(l_Clone.infos_conservatoire.Listeinfos_base.Contains(this)) l_Clone.infos_conservatoire.Listeinfos_base.Remove(this);
		if(l_Clone.t_base != null)if(l_Clone.t_base.Listeinfos_base.Contains(this)) l_Clone.t_base.Listeinfos_base.Remove(this);
		if(l_Clone.t_maj != null)if(l_Clone.t_maj.Listeinfos_base.Contains(this)) l_Clone.t_maj.Listeinfos_base.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeposte.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_infos_base(null, ifb_cn, ifb_a_guid, ifb_a_nom, infos_conservatoire, ifb_a_version, t_base, ifb_n_synchro, ifb_n_der_id_sync, t_maj, ifb_a_ip, ifb_m_msg_maj, ifb_n_desynchro,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_infos_base l_clone = (clg_infos_base) this.Clone;
		c_ifb_cn = l_clone.ifb_cn;
		c_ifb_a_guid = l_clone.ifb_a_guid;
		c_ifb_a_nom = l_clone.ifb_a_nom;
		c_infos_conservatoire = l_clone.infos_conservatoire;
		c_ifb_a_version = l_clone.ifb_a_version;
		c_t_base = l_clone.t_base;
		c_ifb_n_synchro = l_clone.ifb_n_synchro;
		c_ifb_n_der_id_sync = l_clone.ifb_n_der_id_sync;
		c_t_maj = l_clone.t_maj;
		c_ifb_a_ip = l_clone.ifb_a_ip;
		c_ifb_m_msg_maj = l_clone.ifb_m_msg_maj;
		c_ifb_n_desynchro = l_clone.ifb_n_desynchro;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLinfos_base.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLinfos_base.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLinfos_base.Delete(this);
    }

    /* Accesseur de la propriete ifb_cn (ifb_cn)
    * @return c_ifb_cn */
    public Int64 ifb_cn
    {
        get{return c_ifb_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ifb_cn != value)
                {
                    CreerClone();
                    c_ifb_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ifb_a_guid (ifb_a_guid)
    * @return c_ifb_a_guid */
    public string ifb_a_guid
    {
        get{return c_ifb_a_guid;}
        set
        {
            if(value != null)
            {
                if(c_ifb_a_guid != value)
                {
                    CreerClone();
                    c_ifb_a_guid = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ifb_a_nom (ifb_a_nom)
    * @return c_ifb_a_nom */
    public string ifb_a_nom
    {
        get{return c_ifb_a_nom;}
        set
        {
            if(value != null)
            {
                if(c_ifb_a_nom != value)
                {
                    CreerClone();
                    c_ifb_a_nom = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete infos_conservatoire (ifb_csr_cn)
    * @return c_infos_conservatoire */
    public clg_infos_conservatoire infos_conservatoire
    {
        get{return c_infos_conservatoire;}
        set
        {
            if(c_infos_conservatoire != value)
            {
                CreerClone();
                c_infos_conservatoire = value;
            }
        }
    }
    /* Accesseur de la propriete ifb_a_version (ifb_a_version)
    * @return c_ifb_a_version */
    public string ifb_a_version
    {
        get{return c_ifb_a_version;}
        set
        {
            if(value != null)
            {
                if(c_ifb_a_version != value)
                {
                    CreerClone();
                    c_ifb_a_version = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_base (ifb_t_bas_cn)
    * @return c_t_base */
    public clg_t_base t_base
    {
        get{return c_t_base;}
        set
        {
            if(value != null)
            {
                if(c_t_base != value)
                {
                    CreerClone();
                    c_t_base = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ifb_n_synchro (ifb_n_synchro)
    * @return c_ifb_n_synchro */
    public Int64 ifb_n_synchro
    {
        get{return c_ifb_n_synchro;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ifb_n_synchro != value)
                {
                    CreerClone();
                    c_ifb_n_synchro = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ifb_n_der_id_sync (ifb_n_der_id_sync)
    * @return c_ifb_n_der_id_sync */
    public Int64 ifb_n_der_id_sync
    {
        get{return c_ifb_n_der_id_sync;}
        set
        {
            if(c_ifb_n_der_id_sync != value)
            {
                CreerClone();
                c_ifb_n_der_id_sync = value;
            }
        }
    }
    /* Accesseur de la propriete t_maj (ifb_t_maj_cn)
    * @return c_t_maj */
    public clg_t_maj t_maj
    {
        get{return c_t_maj;}
        set
        {
            if(c_t_maj != value)
            {
                CreerClone();
                c_t_maj = value;
            }
        }
    }
    /* Accesseur de la propriete ifb_a_ip (ifb_a_ip)
    * @return c_ifb_a_ip */
    public string ifb_a_ip
    {
        get{return c_ifb_a_ip;}
        set
        {
            if(c_ifb_a_ip != value)
            {
                CreerClone();
                c_ifb_a_ip = value;
            }
        }
    }
    /* Accesseur de la propriete ifb_m_msg_maj (ifb_m_msg_maj)
    * @return c_ifb_m_msg_maj */
    public string ifb_m_msg_maj
    {
        get{return c_ifb_m_msg_maj;}
        set
        {
            if(c_ifb_m_msg_maj != value)
            {
                CreerClone();
                c_ifb_m_msg_maj = value;
            }
        }
    }
    /* Accesseur de la propriete ifb_n_desynchro (ifb_n_desynchro)
    * @return c_ifb_n_desynchro */
    public Int64 ifb_n_desynchro
    {
        get{return c_ifb_n_desynchro;}
        set
        {
            if(c_ifb_n_desynchro != value)
            {
                CreerClone();
                c_ifb_n_desynchro = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type poste */
    public List<clg_poste> Listeposte
    {
        get { return c_Listeposte; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ifb_cn.ToString());
		l_Valeurs.Add(c_ifb_a_guid.ToString());
		l_Valeurs.Add(c_ifb_a_nom.ToString());
		l_Valeurs.Add(c_infos_conservatoire==null ? "-1" : c_infos_conservatoire.ID.ToString());
		l_Valeurs.Add(c_ifb_a_version.ToString());
		l_Valeurs.Add(c_t_base==null ? "-1" : c_t_base.ID.ToString());
		l_Valeurs.Add(c_ifb_n_synchro.ToString());
		l_Valeurs.Add(c_ifb_n_der_id_sync.ToString());
		l_Valeurs.Add(c_t_maj==null ? "-1" : c_t_maj.ID.ToString());
		l_Valeurs.Add(c_ifb_a_ip.ToString());
		l_Valeurs.Add(c_ifb_m_msg_maj.ToString());
		l_Valeurs.Add(c_ifb_n_desynchro.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ifb_cn");
		l_Noms.Add("ifb_a_guid");
		l_Noms.Add("ifb_a_nom");
		l_Noms.Add("infos_conservatoire");
		l_Noms.Add("ifb_a_version");
		l_Noms.Add("t_base");
		l_Noms.Add("ifb_n_synchro");
		l_Noms.Add("ifb_n_der_id_sync");
		l_Noms.Add("t_maj");
		l_Noms.Add("ifb_a_ip");
		l_Noms.Add("ifb_m_msg_maj");
		l_Noms.Add("ifb_n_desynchro");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
