namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : groupe_metaprojet </summary>
public partial class clg_groupe_metaprojet : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLgroupe_metaprojet;
    protected clg_Modele c_Modele;

	private Int64 c_gpm_cn;
	private string c_gpm_a_lib;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_groupe_metaprojet(clg_Modele pModele, Int64 pgpm_cn, bool pAMAJ = false) : base(pModele, pgpm_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_groupe_metaprojet(clg_Modele pModele, Int64 pgpm_cn, string pgpm_a_lib, bool pAMAJ = true) : base(pModele, pgpm_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pgpm_cn != Int64.MinValue)
            c_gpm_cn = pgpm_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_gpm_a_lib = pgpm_a_lib;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pgpm_cn, string pgpm_a_lib)
    {   
		        c_gpm_cn = pgpm_cn;
		        c_gpm_a_lib = pgpm_a_lib;

        base.Initialise(pgpm_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLgroupe_metaprojet; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listegroupe_metaprojet.Dictionnaire.Add(gpm_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listegroupe_metaprojet.Dictionnaire.Remove(gpm_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_groupe_metaprojet l_Clone = (clg_groupe_metaprojet) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_groupe_metaprojet(null, gpm_cn, gpm_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_groupe_metaprojet l_clone = (clg_groupe_metaprojet) this.Clone;
		c_gpm_cn = l_clone.gpm_cn;
		c_gpm_a_lib = l_clone.gpm_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLgroupe_metaprojet.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLgroupe_metaprojet.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLgroupe_metaprojet.Delete(this);
    }

    /* Accesseur de la propriete gpm_cn (gpm_cn)
    * @return c_gpm_cn */
    public Int64 gpm_cn
    {
        get{return c_gpm_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_gpm_cn != value)
                {
                    CreerClone();
                    c_gpm_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete gpm_a_lib (gpm_a_lib)
    * @return c_gpm_a_lib */
    public string gpm_a_lib
    {
        get{return c_gpm_a_lib;}
        set
        {
            if(c_gpm_a_lib != value)
            {
                CreerClone();
                c_gpm_a_lib = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_gpm_cn.ToString());
		l_Valeurs.Add(c_gpm_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("gpm_cn");
		l_Noms.Add("gpm_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
