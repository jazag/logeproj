namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : ventilation_deplacement_csa </summary>
public partial class clg_ventilation_deplacement_csa : clg_deplacements_realises
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLventilation_deplacement_csa;
    protected clg_Modele c_Modele;

	private DateTime c_vdr_d_datesaisie;
	private clg_elements c_elements;
	private Int64 c_vdr_t_fct_cn;
	private Int64 c_vdr_n_validation;
	private DateTime c_vdr_d_validation;
	private Int64 c_vdr_per_cn;
	private clg_temps_intervenants c_temps_intervenants;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_elements = null;
		this.c_temps_intervenants = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_ventilation_deplacement_csa(clg_Modele pModele, Int64 pdpr_cn, bool pAMAJ = false) : base(pModele, pdpr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_ventilation_deplacement_csa(clg_Modele pModele, Int64 pdeplacements_realises, DateTime pvdr_d_datesaisie, clg_elements pelements, Int64 pvdr_t_fct_cn, Int64 pvdr_n_validation, DateTime pvdr_d_validation, Int64 pvdr_per_cn, clg_temps_intervenants ptemps_intervenants, clg_vehicules pvehicules, DateTime pdpr_d_date, double pdpr_n_nbkm, double pdpr_n_kmmin, double pdpr_n_kmmax, double pdpr_n_montant, clg_taux_km_reels ptaux_km_reels, string pdpr_a_dest, string pdpr_a_utilisateur, string pdpr_a_codeana, string pdpr_a_objet, bool pAMAJ = true) : base(pModele, pdeplacements_realises, pvehicules, pdpr_d_date, pdpr_n_nbkm, pdpr_n_kmmin, pdpr_n_kmmax, pdpr_n_montant, ptaux_km_reels, pdpr_a_dest, pdpr_a_utilisateur, pdpr_a_codeana, pdpr_a_objet, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        c_vdr_d_datesaisie = pvdr_d_datesaisie;
		        c_elements = pelements;
		        c_vdr_t_fct_cn = pvdr_t_fct_cn;
		        if(pvdr_n_validation != Int64.MinValue)
            c_vdr_n_validation = pvdr_n_validation;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_vdr_d_validation = pvdr_d_validation;
		        c_vdr_per_cn = pvdr_per_cn;
		        c_temps_intervenants = ptemps_intervenants;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pdeplacements_realises, DateTime pvdr_d_datesaisie, Int64 pelements, Int64 pvdr_t_fct_cn, Int64 pvdr_n_validation, DateTime pvdr_d_validation, Int64 pvdr_per_cn, Int64 ptemps_intervenants, Int64 pvehicules, DateTime pdpr_d_date, double pdpr_n_nbkm, double pdpr_n_kmmin, double pdpr_n_kmmax, double pdpr_n_montant, Int64 ptaux_km_reels, string pdpr_a_dest, string pdpr_a_utilisateur, string pdpr_a_codeana, string pdpr_a_objet)
    {   
		        c_vdr_d_datesaisie = pvdr_d_datesaisie;
		c_dicReferences.Add("elements", pelements);
		        c_vdr_t_fct_cn = pvdr_t_fct_cn;
		        c_vdr_n_validation = pvdr_n_validation;
		        c_vdr_d_validation = pvdr_d_validation;
		        c_vdr_per_cn = pvdr_per_cn;
		c_dicReferences.Add("temps_intervenants", ptemps_intervenants);

        base.Initialise(pdeplacements_realises, pvehicules, pdpr_d_date, pdpr_n_nbkm, pdpr_n_kmmin, pdpr_n_kmmax, pdpr_n_montant, ptaux_km_reels, pdpr_a_dest, pdpr_a_utilisateur, pdpr_a_codeana, pdpr_a_objet);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLventilation_deplacement_csa; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		base.CreeLiens();
		c_elements = (clg_elements) c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");
		c_temps_intervenants = (clg_temps_intervenants) c_ModeleBase.RenvoieObjet(c_dicReferences["temps_intervenants"], "clg_temps_intervenants");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeventilation_deplacement_csa.Dictionnaire.Add(dpr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeventilation_deplacement_csa.Dictionnaire.Remove(dpr_cn);
		base.SupprimeDansListe();

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		base.CreeReferences();
		if(c_elements != null)if(!c_elements.Listeventilation_deplacement_csa.Contains(this)) c_elements.Listeventilation_deplacement_csa.Add(this);
		if(c_temps_intervenants != null)if(!c_temps_intervenants.Listeventilation_deplacement_csa.Contains(this)) c_temps_intervenants.Listeventilation_deplacement_csa.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_ventilation_deplacement_csa l_Clone = (clg_ventilation_deplacement_csa) Clone;
		base.DetruitReferences();
		if(l_Clone.elements != null)if(l_Clone.elements.Listeventilation_deplacement_csa.Contains(this)) l_Clone.elements.Listeventilation_deplacement_csa.Remove(this);
		if(l_Clone.temps_intervenants != null)if(l_Clone.temps_intervenants.Listeventilation_deplacement_csa.Contains(this)) l_Clone.temps_intervenants.Listeventilation_deplacement_csa.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_ventilation_deplacement_csa(null, dpr_cn, vdr_d_datesaisie, elements, vdr_t_fct_cn, vdr_n_validation, vdr_d_validation, vdr_per_cn, temps_intervenants, vehicules, dpr_d_date, dpr_n_nbkm, dpr_n_kmmin, dpr_n_kmmax, dpr_n_montant, taux_km_reels, dpr_a_dest, dpr_a_utilisateur, dpr_a_codeana, dpr_a_objet,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_ventilation_deplacement_csa l_clone = (clg_ventilation_deplacement_csa) this.Clone;
		c_vdr_d_datesaisie = l_clone.vdr_d_datesaisie;
		c_elements = l_clone.elements;
		c_vdr_t_fct_cn = l_clone.vdr_t_fct_cn;
		c_vdr_n_validation = l_clone.vdr_n_validation;
		c_vdr_d_validation = l_clone.vdr_d_validation;
		c_vdr_per_cn = l_clone.vdr_per_cn;
		c_temps_intervenants = l_clone.temps_intervenants;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLventilation_deplacement_csa.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLventilation_deplacement_csa.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLventilation_deplacement_csa.Delete(this);
    }

    /* Accesseur de la propriete vdr_d_datesaisie (vdr_d_datesaisie)
    * @return c_vdr_d_datesaisie */
    public DateTime vdr_d_datesaisie
    {
        get{return c_vdr_d_datesaisie;}
        set
        {
            if(c_vdr_d_datesaisie != value)
            {
                CreerClone();
                c_vdr_d_datesaisie = value;
            }
        }
    }
    /* Accesseur de la propriete elements (vdr_ele_cn)
    * @return c_elements */
    public clg_elements elements
    {
        get{return c_elements;}
        set
        {
            if(c_elements != value)
            {
                CreerClone();
                c_elements = value;
            }
        }
    }
    /* Accesseur de la propriete vdr_t_fct_cn (vdr_t_fct_cn)
    * @return c_vdr_t_fct_cn */
    public Int64 vdr_t_fct_cn
    {
        get{return c_vdr_t_fct_cn;}
        set
        {
            if(c_vdr_t_fct_cn != value)
            {
                CreerClone();
                c_vdr_t_fct_cn = value;
            }
        }
    }
    /* Accesseur de la propriete vdr_n_validation (vdr_n_validation)
    * @return c_vdr_n_validation */
    public Int64 vdr_n_validation
    {
        get{return c_vdr_n_validation;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_vdr_n_validation != value)
                {
                    CreerClone();
                    c_vdr_n_validation = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete vdr_d_validation (vdr_d_validation)
    * @return c_vdr_d_validation */
    public DateTime vdr_d_validation
    {
        get{return c_vdr_d_validation;}
        set
        {
            if(c_vdr_d_validation != value)
            {
                CreerClone();
                c_vdr_d_validation = value;
            }
        }
    }
    /* Accesseur de la propriete vdr_per_cn (vdr_per_cn)
    * @return c_vdr_per_cn */
    public Int64 vdr_per_cn
    {
        get{return c_vdr_per_cn;}
        set
        {
            if(c_vdr_per_cn != value)
            {
                CreerClone();
                c_vdr_per_cn = value;
            }
        }
    }
    /* Accesseur de la propriete temps_intervenants (vdr_tit_cn)
    * @return c_temps_intervenants */
    public clg_temps_intervenants temps_intervenants
    {
        get{return c_temps_intervenants;}
        set
        {
            if(c_temps_intervenants != value)
            {
                CreerClone();
                c_temps_intervenants = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_vdr_d_datesaisie.ToString());
		l_Valeurs.Add(c_elements==null ? "-1" : c_elements.ID.ToString());
		l_Valeurs.Add(c_vdr_t_fct_cn.ToString());
		l_Valeurs.Add(c_vdr_n_validation.ToString());
		l_Valeurs.Add(c_vdr_d_validation.ToString());
		l_Valeurs.Add(c_vdr_per_cn.ToString());
		l_Valeurs.Add(c_temps_intervenants==null ? "-1" : c_temps_intervenants.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("deplacements_realises");
		l_Noms.Add("vdr_d_datesaisie");
		l_Noms.Add("elements");
		l_Noms.Add("vdr_t_fct_cn");
		l_Noms.Add("vdr_n_validation");
		l_Noms.Add("vdr_d_validation");
		l_Noms.Add("vdr_per_cn");
		l_Noms.Add("temps_intervenants");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
