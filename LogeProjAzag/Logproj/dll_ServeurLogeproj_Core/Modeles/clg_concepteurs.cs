namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : concepteurs </summary>
public partial class clg_concepteurs : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLconcepteurs;
    protected clg_Modele c_Modele;

	private Int64 c_cpt_cn;
	private clg_projets c_projets;
	private clg_personnels c_personnels;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_projets = null;
		this.c_personnels = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_concepteurs(clg_Modele pModele, Int64 pcpt_cn, bool pAMAJ = false) : base(pModele, pcpt_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_concepteurs(clg_Modele pModele, Int64 pcpt_cn, clg_projets pprojets, clg_personnels ppersonnels, bool pAMAJ = true) : base(pModele, pcpt_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcpt_cn != Int64.MinValue)
            c_cpt_cn = pcpt_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pprojets != null)
            c_projets = pprojets;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(ppersonnels != null)
            c_personnels = ppersonnels;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcpt_cn, Int64 pprojets, Int64 ppersonnels)
    {   
		        c_cpt_cn = pcpt_cn;
		c_dicReferences.Add("projets", pprojets);
		c_dicReferences.Add("personnels", ppersonnels);

        base.Initialise(pcpt_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLconcepteurs; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_projets = (clg_projets) c_ModeleBase.RenvoieObjet(c_dicReferences["projets"], "clg_projets");
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeconcepteurs.Dictionnaire.Add(cpt_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeconcepteurs.Dictionnaire.Remove(cpt_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_projets != null)if(!c_projets.Listeconcepteurs.Contains(this)) c_projets.Listeconcepteurs.Add(this);
		if(c_personnels != null)if(!c_personnels.Listeconcepteurs.Contains(this)) c_personnels.Listeconcepteurs.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_concepteurs l_Clone = (clg_concepteurs) Clone;
		if(l_Clone.projets != null)if(l_Clone.projets.Listeconcepteurs.Contains(this)) l_Clone.projets.Listeconcepteurs.Remove(this);
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listeconcepteurs.Contains(this)) l_Clone.personnels.Listeconcepteurs.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_concepteurs(null, cpt_cn, projets, personnels,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_concepteurs l_clone = (clg_concepteurs) this.Clone;
		c_cpt_cn = l_clone.cpt_cn;
		c_projets = l_clone.projets;
		c_personnels = l_clone.personnels;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLconcepteurs.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLconcepteurs.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLconcepteurs.Delete(this);
    }

    /* Accesseur de la propriete cpt_cn (cpt_cn)
    * @return c_cpt_cn */
    public Int64 cpt_cn
    {
        get{return c_cpt_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cpt_cn != value)
                {
                    CreerClone();
                    c_cpt_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete projets (cpt_prj_ele_cn)
    * @return c_projets */
    public clg_projets projets
    {
        get{return c_projets;}
        set
        {
            if(value != null)
            {
                if(c_projets != value)
                {
                    CreerClone();
                    c_projets = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (cpt_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(value != null)
            {
                if(c_personnels != value)
                {
                    CreerClone();
                    c_personnels = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cpt_cn.ToString());
		l_Valeurs.Add(c_projets==null ? "-1" : c_projets.ID.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cpt_cn");
		l_Noms.Add("projets");
		l_Noms.Add("personnels");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
