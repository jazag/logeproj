namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : param_compte </summary>
public partial class clg_param_compte : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLparam_compte;
    protected clg_Modele c_Modele;

	private Int64 c_pct_cn;
	private string c_pct_jou_a_code;
	private string c_pct_a_cpt;
	private string c_pct_tsc_cn;
	private clg_type_fonct_compta c_type_fonct_compta;
	private string c_pct_a_cheminico;
	private string c_pct_a_cmt;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_type_fonct_compta = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_param_compte(clg_Modele pModele, Int64 ppct_cn, bool pAMAJ = false) : base(pModele, ppct_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_param_compte(clg_Modele pModele, Int64 ppct_cn, string ppct_jou_a_code, string ppct_a_cpt, string ppct_tsc_cn, clg_type_fonct_compta ptype_fonct_compta, string ppct_a_cheminico, string ppct_a_cmt, bool pAMAJ = true) : base(pModele, ppct_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ppct_cn != Int64.MinValue)
            c_pct_cn = ppct_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_pct_jou_a_code = ppct_jou_a_code;
		        c_pct_a_cpt = ppct_a_cpt;
		        c_pct_tsc_cn = ppct_tsc_cn;
		        c_type_fonct_compta = ptype_fonct_compta;
		        c_pct_a_cheminico = ppct_a_cheminico;
		        c_pct_a_cmt = ppct_a_cmt;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ppct_cn, string ppct_jou_a_code, string ppct_a_cpt, string ppct_tsc_cn, Int64 ptype_fonct_compta, string ppct_a_cheminico, string ppct_a_cmt)
    {   
		        c_pct_cn = ppct_cn;
		        c_pct_jou_a_code = ppct_jou_a_code;
		        c_pct_a_cpt = ppct_a_cpt;
		        c_pct_tsc_cn = ppct_tsc_cn;
		c_dicReferences.Add("type_fonct_compta", ptype_fonct_compta);
		        c_pct_a_cheminico = ppct_a_cheminico;
		        c_pct_a_cmt = ppct_a_cmt;

        base.Initialise(ppct_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLparam_compte; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_type_fonct_compta = (clg_type_fonct_compta) c_ModeleBase.RenvoieObjet(c_dicReferences["type_fonct_compta"], "clg_type_fonct_compta");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeparam_compte.Dictionnaire.Add(pct_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeparam_compte.Dictionnaire.Remove(pct_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_type_fonct_compta != null)if(!c_type_fonct_compta.Listeparam_compte.Contains(this)) c_type_fonct_compta.Listeparam_compte.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_param_compte l_Clone = (clg_param_compte) Clone;
		if(l_Clone.type_fonct_compta != null)if(l_Clone.type_fonct_compta.Listeparam_compte.Contains(this)) l_Clone.type_fonct_compta.Listeparam_compte.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_param_compte(null, pct_cn, pct_jou_a_code, pct_a_cpt, pct_tsc_cn, type_fonct_compta, pct_a_cheminico, pct_a_cmt,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_param_compte l_clone = (clg_param_compte) this.Clone;
		c_pct_cn = l_clone.pct_cn;
		c_pct_jou_a_code = l_clone.pct_jou_a_code;
		c_pct_a_cpt = l_clone.pct_a_cpt;
		c_pct_tsc_cn = l_clone.pct_tsc_cn;
		c_type_fonct_compta = l_clone.type_fonct_compta;
		c_pct_a_cheminico = l_clone.pct_a_cheminico;
		c_pct_a_cmt = l_clone.pct_a_cmt;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLparam_compte.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLparam_compte.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLparam_compte.Delete(this);
    }

    /* Accesseur de la propriete pct_cn (pct_cn)
    * @return c_pct_cn */
    public Int64 pct_cn
    {
        get{return c_pct_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_pct_cn != value)
                {
                    CreerClone();
                    c_pct_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete pct_jou_a_code (pct_jou_a_code)
    * @return c_pct_jou_a_code */
    public string pct_jou_a_code
    {
        get{return c_pct_jou_a_code;}
        set
        {
            if(c_pct_jou_a_code != value)
            {
                CreerClone();
                c_pct_jou_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete pct_a_cpt (pct_a_cpt)
    * @return c_pct_a_cpt */
    public string pct_a_cpt
    {
        get{return c_pct_a_cpt;}
        set
        {
            if(c_pct_a_cpt != value)
            {
                CreerClone();
                c_pct_a_cpt = value;
            }
        }
    }
    /* Accesseur de la propriete pct_tsc_cn (pct_tsc_cn)
    * @return c_pct_tsc_cn */
    public string pct_tsc_cn
    {
        get{return c_pct_tsc_cn;}
        set
        {
            if(c_pct_tsc_cn != value)
            {
                CreerClone();
                c_pct_tsc_cn = value;
            }
        }
    }
    /* Accesseur de la propriete type_fonct_compta (pct_tfc_cn)
    * @return c_type_fonct_compta */
    public clg_type_fonct_compta type_fonct_compta
    {
        get{return c_type_fonct_compta;}
        set
        {
            if(c_type_fonct_compta != value)
            {
                CreerClone();
                c_type_fonct_compta = value;
            }
        }
    }
    /* Accesseur de la propriete pct_a_cheminico (pct_a_cheminico)
    * @return c_pct_a_cheminico */
    public string pct_a_cheminico
    {
        get{return c_pct_a_cheminico;}
        set
        {
            if(c_pct_a_cheminico != value)
            {
                CreerClone();
                c_pct_a_cheminico = value;
            }
        }
    }
    /* Accesseur de la propriete pct_a_cmt (pct_a_cmt)
    * @return c_pct_a_cmt */
    public string pct_a_cmt
    {
        get{return c_pct_a_cmt;}
        set
        {
            if(c_pct_a_cmt != value)
            {
                CreerClone();
                c_pct_a_cmt = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_pct_cn.ToString());
		l_Valeurs.Add(c_pct_jou_a_code.ToString());
		l_Valeurs.Add(c_pct_a_cpt.ToString());
		l_Valeurs.Add(c_pct_tsc_cn.ToString());
		l_Valeurs.Add(c_type_fonct_compta==null ? "-1" : c_type_fonct_compta.ID.ToString());
		l_Valeurs.Add(c_pct_a_cheminico.ToString());
		l_Valeurs.Add(c_pct_a_cmt.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("pct_cn");
		l_Noms.Add("pct_jou_a_code");
		l_Noms.Add("pct_a_cpt");
		l_Noms.Add("pct_tsc_cn");
		l_Noms.Add("type_fonct_compta");
		l_Noms.Add("pct_a_cheminico");
		l_Noms.Add("pct_a_cmt");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
