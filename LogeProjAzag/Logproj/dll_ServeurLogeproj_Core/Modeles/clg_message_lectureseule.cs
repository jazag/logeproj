namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : message_lectureseule </summary>
public partial class clg_message_lectureseule : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLmessage_lectureseule;
    protected clg_Modele c_Modele;

	private Int64 c_msl_cn;
	private string c_msl_a_type;
	private string c_msl_a_msg;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_message_lectureseule(clg_Modele pModele, Int64 pmsl_cn, bool pAMAJ = false) : base(pModele, pmsl_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_message_lectureseule(clg_Modele pModele, Int64 pmsl_cn, string pmsl_a_type, string pmsl_a_msg, bool pAMAJ = true) : base(pModele, pmsl_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pmsl_cn != Int64.MinValue)
            c_msl_cn = pmsl_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_msl_a_type = pmsl_a_type;
		        c_msl_a_msg = pmsl_a_msg;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pmsl_cn, string pmsl_a_type, string pmsl_a_msg)
    {   
		        c_msl_cn = pmsl_cn;
		        c_msl_a_type = pmsl_a_type;
		        c_msl_a_msg = pmsl_a_msg;

        base.Initialise(pmsl_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLmessage_lectureseule; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listemessage_lectureseule.Dictionnaire.Add(msl_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listemessage_lectureseule.Dictionnaire.Remove(msl_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_message_lectureseule l_Clone = (clg_message_lectureseule) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_message_lectureseule(null, msl_cn, msl_a_type, msl_a_msg,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_message_lectureseule l_clone = (clg_message_lectureseule) this.Clone;
		c_msl_cn = l_clone.msl_cn;
		c_msl_a_type = l_clone.msl_a_type;
		c_msl_a_msg = l_clone.msl_a_msg;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLmessage_lectureseule.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLmessage_lectureseule.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLmessage_lectureseule.Delete(this);
    }

    /* Accesseur de la propriete msl_cn (msl_cn)
    * @return c_msl_cn */
    public Int64 msl_cn
    {
        get{return c_msl_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_msl_cn != value)
                {
                    CreerClone();
                    c_msl_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete msl_a_type (msl_a_type)
    * @return c_msl_a_type */
    public string msl_a_type
    {
        get{return c_msl_a_type;}
        set
        {
            if(c_msl_a_type != value)
            {
                CreerClone();
                c_msl_a_type = value;
            }
        }
    }
    /* Accesseur de la propriete msl_a_msg (msl_a_msg)
    * @return c_msl_a_msg */
    public string msl_a_msg
    {
        get{return c_msl_a_msg;}
        set
        {
            if(c_msl_a_msg != value)
            {
                CreerClone();
                c_msl_a_msg = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_msl_cn.ToString());
		l_Valeurs.Add(c_msl_a_type.ToString());
		l_Valeurs.Add(c_msl_a_msg.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("msl_cn");
		l_Noms.Add("msl_a_type");
		l_Noms.Add("msl_a_msg");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
