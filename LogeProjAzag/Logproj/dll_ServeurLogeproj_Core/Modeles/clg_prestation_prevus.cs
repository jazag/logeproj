namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : prestation_prevus </summary>
public partial class clg_prestation_prevus : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLprestation_prevus;
    protected clg_Modele c_Modele;

	private Int64 c_ptp_cn;
	private Int64 c_ptp_four_cn;
	private clg_operations c_operations;
	private double c_ptp_n_montant;
	private DateTime c_ptp_d_debut;
	private DateTime c_ptp_d_fin;
	private clg_t_prestation c_t_prestation;
	private double c_ptp_n_nb_unite;
	private double c_ptp_n_prix_unit;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_operations = null;
		this.c_t_prestation = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_prestation_prevus(clg_Modele pModele, Int64 pptp_cn, bool pAMAJ = false) : base(pModele, pptp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_prestation_prevus(clg_Modele pModele, Int64 pptp_cn, Int64 pptp_four_cn, clg_operations poperations, double pptp_n_montant, DateTime pptp_d_debut, DateTime pptp_d_fin, clg_t_prestation pt_prestation, double pptp_n_nb_unite, double pptp_n_prix_unit, bool pAMAJ = true) : base(pModele, pptp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pptp_cn != Int64.MinValue)
            c_ptp_cn = pptp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ptp_four_cn = pptp_four_cn;
		        if(poperations != null)
            c_operations = poperations;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_ptp_n_montant = pptp_n_montant;
		        c_ptp_d_debut = pptp_d_debut;
		        c_ptp_d_fin = pptp_d_fin;
		        c_t_prestation = pt_prestation;
		        c_ptp_n_nb_unite = pptp_n_nb_unite;
		        c_ptp_n_prix_unit = pptp_n_prix_unit;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pptp_cn, Int64 pptp_four_cn, Int64 poperations, double pptp_n_montant, DateTime pptp_d_debut, DateTime pptp_d_fin, Int64 pt_prestation, double pptp_n_nb_unite, double pptp_n_prix_unit)
    {   
		        c_ptp_cn = pptp_cn;
		        c_ptp_four_cn = pptp_four_cn;
		c_dicReferences.Add("operations", poperations);
		        c_ptp_n_montant = pptp_n_montant;
		        c_ptp_d_debut = pptp_d_debut;
		        c_ptp_d_fin = pptp_d_fin;
		c_dicReferences.Add("t_prestation", pt_prestation);
		        c_ptp_n_nb_unite = pptp_n_nb_unite;
		        c_ptp_n_prix_unit = pptp_n_prix_unit;

        base.Initialise(pptp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLprestation_prevus; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_operations = (clg_operations) c_ModeleBase.RenvoieObjet(c_dicReferences["operations"], "clg_operations");
		c_t_prestation = (clg_t_prestation) c_ModeleBase.RenvoieObjet(c_dicReferences["t_prestation"], "clg_t_prestation");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeprestation_prevus.Dictionnaire.Add(ptp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeprestation_prevus.Dictionnaire.Remove(ptp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_operations != null)if(!c_operations.Listeprestation_prevus.Contains(this)) c_operations.Listeprestation_prevus.Add(this);
		if(c_t_prestation != null)if(!c_t_prestation.Listeprestation_prevus.Contains(this)) c_t_prestation.Listeprestation_prevus.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_prestation_prevus l_Clone = (clg_prestation_prevus) Clone;
		if(l_Clone.operations != null)if(l_Clone.operations.Listeprestation_prevus.Contains(this)) l_Clone.operations.Listeprestation_prevus.Remove(this);
		if(l_Clone.t_prestation != null)if(l_Clone.t_prestation.Listeprestation_prevus.Contains(this)) l_Clone.t_prestation.Listeprestation_prevus.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_prestation_prevus(null, ptp_cn, ptp_four_cn, operations, ptp_n_montant, ptp_d_debut, ptp_d_fin, t_prestation, ptp_n_nb_unite, ptp_n_prix_unit,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_prestation_prevus l_clone = (clg_prestation_prevus) this.Clone;
		c_ptp_cn = l_clone.ptp_cn;
		c_ptp_four_cn = l_clone.ptp_four_cn;
		c_operations = l_clone.operations;
		c_ptp_n_montant = l_clone.ptp_n_montant;
		c_ptp_d_debut = l_clone.ptp_d_debut;
		c_ptp_d_fin = l_clone.ptp_d_fin;
		c_t_prestation = l_clone.t_prestation;
		c_ptp_n_nb_unite = l_clone.ptp_n_nb_unite;
		c_ptp_n_prix_unit = l_clone.ptp_n_prix_unit;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLprestation_prevus.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLprestation_prevus.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLprestation_prevus.Delete(this);
    }

    /* Accesseur de la propriete ptp_cn (ptp_cn)
    * @return c_ptp_cn */
    public Int64 ptp_cn
    {
        get{return c_ptp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ptp_cn != value)
                {
                    CreerClone();
                    c_ptp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptp_four_cn (ptp_four_cn)
    * @return c_ptp_four_cn */
    public Int64 ptp_four_cn
    {
        get{return c_ptp_four_cn;}
        set
        {
            if(c_ptp_four_cn != value)
            {
                CreerClone();
                c_ptp_four_cn = value;
            }
        }
    }
    /* Accesseur de la propriete operations (ptp_ope_ele_cn)
    * @return c_operations */
    public clg_operations operations
    {
        get{return c_operations;}
        set
        {
            if(value != null)
            {
                if(c_operations != value)
                {
                    CreerClone();
                    c_operations = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ptp_n_montant (ptp_n_montant)
    * @return c_ptp_n_montant */
    public double ptp_n_montant
    {
        get{return c_ptp_n_montant;}
        set
        {
            if(c_ptp_n_montant != value)
            {
                CreerClone();
                c_ptp_n_montant = value;
            }
        }
    }
    /* Accesseur de la propriete ptp_d_debut (ptp_d_debut)
    * @return c_ptp_d_debut */
    public DateTime ptp_d_debut
    {
        get{return c_ptp_d_debut;}
        set
        {
            if(c_ptp_d_debut != value)
            {
                CreerClone();
                c_ptp_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete ptp_d_fin (ptp_d_fin)
    * @return c_ptp_d_fin */
    public DateTime ptp_d_fin
    {
        get{return c_ptp_d_fin;}
        set
        {
            if(c_ptp_d_fin != value)
            {
                CreerClone();
                c_ptp_d_fin = value;
            }
        }
    }
    /* Accesseur de la propriete t_prestation (ptp_t_ptp_cn)
    * @return c_t_prestation */
    public clg_t_prestation t_prestation
    {
        get{return c_t_prestation;}
        set
        {
            if(c_t_prestation != value)
            {
                CreerClone();
                c_t_prestation = value;
            }
        }
    }
    /* Accesseur de la propriete ptp_n_nb_unite (ptp_n_nb_unite)
    * @return c_ptp_n_nb_unite */
    public double ptp_n_nb_unite
    {
        get{return c_ptp_n_nb_unite;}
        set
        {
            if(c_ptp_n_nb_unite != value)
            {
                CreerClone();
                c_ptp_n_nb_unite = value;
            }
        }
    }
    /* Accesseur de la propriete ptp_n_prix_unit (ptp_n_prix_unit)
    * @return c_ptp_n_prix_unit */
    public double ptp_n_prix_unit
    {
        get{return c_ptp_n_prix_unit;}
        set
        {
            if(c_ptp_n_prix_unit != value)
            {
                CreerClone();
                c_ptp_n_prix_unit = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ptp_cn.ToString());
		l_Valeurs.Add(c_ptp_four_cn.ToString());
		l_Valeurs.Add(c_operations==null ? "-1" : c_operations.ID.ToString());
		l_Valeurs.Add(c_ptp_n_montant.ToString());
		l_Valeurs.Add(c_ptp_d_debut.ToString());
		l_Valeurs.Add(c_ptp_d_fin.ToString());
		l_Valeurs.Add(c_t_prestation==null ? "-1" : c_t_prestation.ID.ToString());
		l_Valeurs.Add(c_ptp_n_nb_unite.ToString());
		l_Valeurs.Add(c_ptp_n_prix_unit.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ptp_cn");
		l_Noms.Add("ptp_four_cn");
		l_Noms.Add("operations");
		l_Noms.Add("ptp_n_montant");
		l_Noms.Add("ptp_d_debut");
		l_Noms.Add("ptp_d_fin");
		l_Noms.Add("t_prestation");
		l_Noms.Add("ptp_n_nb_unite");
		l_Noms.Add("ptp_n_prix_unit");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
