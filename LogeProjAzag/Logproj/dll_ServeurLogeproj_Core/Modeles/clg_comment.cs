namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : comment </summary>
public partial class clg_comment : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcomment;
    protected clg_Modele c_Modele;

	private Int64 c_com_cn;
	private string c_com_m_memo;
	private DateTime c_com_d_date;
	private List<clg_arretes> c_Listearretes;
	private List<clg_elements> c_Listeelements;
	private List<clg_financeurs> c_Listefinanceurs;
	private List<clg_fournisseurs> c_Listefournisseurs;
	private List<clg_vehicules> c_Listevehicules;


    private void Init()
    {
		c_Listearretes = new List<clg_arretes>();
		c_Listeelements = new List<clg_elements>();
		c_Listefinanceurs = new List<clg_financeurs>();
		c_Listefournisseurs = new List<clg_fournisseurs>();
		c_Listevehicules = new List<clg_vehicules>();

    }

    public override void Detruit()
    {
		c_Listearretes.Clear();
		c_Listeelements.Clear();
		c_Listefinanceurs.Clear();
		c_Listefournisseurs.Clear();
		c_Listevehicules.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_comment(clg_Modele pModele, Int64 pcom_cn, bool pAMAJ = false) : base(pModele, pcom_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_comment(clg_Modele pModele, Int64 pcom_cn, string pcom_m_memo, DateTime pcom_d_date, bool pAMAJ = true) : base(pModele, pcom_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcom_cn != Int64.MinValue)
            c_com_cn = pcom_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_com_m_memo = pcom_m_memo;
		        c_com_d_date = pcom_d_date;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcom_cn, string pcom_m_memo, DateTime pcom_d_date)
    {   
		        c_com_cn = pcom_cn;
		        c_com_m_memo = pcom_m_memo;
		        c_com_d_date = pcom_d_date;

        base.Initialise(pcom_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcomment; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecomment.Dictionnaire.Add(com_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecomment.Dictionnaire.Remove(com_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_comment l_Clone = (clg_comment) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listearretes.Count > 0) c_EstReference = true;
		if(c_Listeelements.Count > 0) c_EstReference = true;
		if(c_Listefinanceurs.Count > 0) c_EstReference = true;
		if(c_Listefournisseurs.Count > 0) c_EstReference = true;
		if(c_Listevehicules.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_comment(null, com_cn, com_m_memo, com_d_date,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_comment l_clone = (clg_comment) this.Clone;
		c_com_cn = l_clone.com_cn;
		c_com_m_memo = l_clone.com_m_memo;
		c_com_d_date = l_clone.com_d_date;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcomment.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcomment.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcomment.Delete(this);
    }

    /* Accesseur de la propriete com_cn (com_cn)
    * @return c_com_cn */
    public Int64 com_cn
    {
        get{return c_com_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_com_cn != value)
                {
                    CreerClone();
                    c_com_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete com_m_memo (com_m_memo)
    * @return c_com_m_memo */
    public string com_m_memo
    {
        get{return c_com_m_memo;}
        set
        {
            if(c_com_m_memo != value)
            {
                CreerClone();
                c_com_m_memo = value;
            }
        }
    }
    /* Accesseur de la propriete com_d_date (com_d_date)
    * @return c_com_d_date */
    public DateTime com_d_date
    {
        get{return c_com_d_date;}
        set
        {
            if(c_com_d_date != value)
            {
                CreerClone();
                c_com_d_date = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type arretes */
    public List<clg_arretes> Listearretes
    {
        get { return c_Listearretes; }
    }    /* Accesseur de la liste des objets de type elements */
    public List<clg_elements> Listeelements
    {
        get { return c_Listeelements; }
    }    /* Accesseur de la liste des objets de type financeurs */
    public List<clg_financeurs> Listefinanceurs
    {
        get { return c_Listefinanceurs; }
    }    /* Accesseur de la liste des objets de type fournisseurs */
    public List<clg_fournisseurs> Listefournisseurs
    {
        get { return c_Listefournisseurs; }
    }    /* Accesseur de la liste des objets de type vehicules */
    public List<clg_vehicules> Listevehicules
    {
        get { return c_Listevehicules; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_com_cn.ToString());
		l_Valeurs.Add(c_com_m_memo.ToString());
		l_Valeurs.Add(c_com_d_date.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("com_cn");
		l_Noms.Add("com_m_memo");
		l_Noms.Add("com_d_date");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }






#endregion
}
}
