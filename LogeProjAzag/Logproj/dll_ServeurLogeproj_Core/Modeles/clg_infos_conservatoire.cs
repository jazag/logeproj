namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : infos_conservatoire </summary>
public partial class clg_infos_conservatoire : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLinfos_conservatoire;
    protected clg_Modele c_Modele;

	private Int64 c_csr_cn;
	private string c_csr_a_guid;
	private string c_csr_a_version_3_0;
	private string c_csr_a_nom;
	private string c_csr_a_libel;
	private Int64 c_csr_n_lock;
	private string c_csr_a_msglock;
	private string c_csr_a_adresse_serv_sync;
	private Int64 c_csr_n_port_serv_sync_env;
	private Int64 c_csr_n_port_serv_sync_rec;
	private Int64 c_csr_n_der_id_sync;
	private string c_csr_a_adresse_serv_maj;
	private Int64 c_csr_n_port_serv_maj_env;
	private Int64 c_csr_n_port_serv_maj_rec;
	private Int64 c_csr_basemae_cn;
	private Int64 c_csr_v_tx;
	private Int64 c_csr_d_debut_tx;
	private List<clg_param_conservatoire> c_Listeparam_conservatoire;
	private List<clg_infos_base> c_Listeinfos_base;


    private void Init()
    {
		c_Listeparam_conservatoire = new List<clg_param_conservatoire>();
		c_Listeinfos_base = new List<clg_infos_base>();

    }

    public override void Detruit()
    {
		c_Listeparam_conservatoire.Clear();
		c_Listeinfos_base.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_infos_conservatoire(clg_Modele pModele, Int64 pcsr_cn, bool pAMAJ = false) : base(pModele, pcsr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_infos_conservatoire(clg_Modele pModele, Int64 pcsr_cn, string pcsr_a_guid, string pcsr_a_version_3_0, string pcsr_a_nom, string pcsr_a_libel, Int64 pcsr_n_lock, string pcsr_a_msglock, string pcsr_a_adresse_serv_sync, Int64 pcsr_n_port_serv_sync_env, Int64 pcsr_n_port_serv_sync_rec, Int64 pcsr_n_der_id_sync, string pcsr_a_adresse_serv_maj, Int64 pcsr_n_port_serv_maj_env, Int64 pcsr_n_port_serv_maj_rec, Int64 pcsr_basemae_cn, Int64 pcsr_v_tx, Int64 pcsr_d_debut_tx, bool pAMAJ = true) : base(pModele, pcsr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcsr_cn != Int64.MinValue)
            c_csr_cn = pcsr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_csr_a_guid = pcsr_a_guid;
		        c_csr_a_version_3_0 = pcsr_a_version_3_0;
		        if(pcsr_a_nom != null)
            c_csr_a_nom = pcsr_a_nom;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_csr_a_libel = pcsr_a_libel;
		        c_csr_n_lock = pcsr_n_lock;
		        c_csr_a_msglock = pcsr_a_msglock;
		        c_csr_a_adresse_serv_sync = pcsr_a_adresse_serv_sync;
		        c_csr_n_port_serv_sync_env = pcsr_n_port_serv_sync_env;
		        c_csr_n_port_serv_sync_rec = pcsr_n_port_serv_sync_rec;
		        c_csr_n_der_id_sync = pcsr_n_der_id_sync;
		        c_csr_a_adresse_serv_maj = pcsr_a_adresse_serv_maj;
		        c_csr_n_port_serv_maj_env = pcsr_n_port_serv_maj_env;
		        c_csr_n_port_serv_maj_rec = pcsr_n_port_serv_maj_rec;
		        c_csr_basemae_cn = pcsr_basemae_cn;
		        c_csr_v_tx = pcsr_v_tx;
		        c_csr_d_debut_tx = pcsr_d_debut_tx;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcsr_cn, string pcsr_a_guid, string pcsr_a_version_3_0, string pcsr_a_nom, string pcsr_a_libel, Int64 pcsr_n_lock, string pcsr_a_msglock, string pcsr_a_adresse_serv_sync, Int64 pcsr_n_port_serv_sync_env, Int64 pcsr_n_port_serv_sync_rec, Int64 pcsr_n_der_id_sync, string pcsr_a_adresse_serv_maj, Int64 pcsr_n_port_serv_maj_env, Int64 pcsr_n_port_serv_maj_rec, Int64 pcsr_basemae_cn, Int64 pcsr_v_tx, Int64 pcsr_d_debut_tx)
    {   
		        c_csr_cn = pcsr_cn;
		        c_csr_a_guid = pcsr_a_guid;
		        c_csr_a_version_3_0 = pcsr_a_version_3_0;
		        c_csr_a_nom = pcsr_a_nom;
		        c_csr_a_libel = pcsr_a_libel;
		        c_csr_n_lock = pcsr_n_lock;
		        c_csr_a_msglock = pcsr_a_msglock;
		        c_csr_a_adresse_serv_sync = pcsr_a_adresse_serv_sync;
		        c_csr_n_port_serv_sync_env = pcsr_n_port_serv_sync_env;
		        c_csr_n_port_serv_sync_rec = pcsr_n_port_serv_sync_rec;
		        c_csr_n_der_id_sync = pcsr_n_der_id_sync;
		        c_csr_a_adresse_serv_maj = pcsr_a_adresse_serv_maj;
		        c_csr_n_port_serv_maj_env = pcsr_n_port_serv_maj_env;
		        c_csr_n_port_serv_maj_rec = pcsr_n_port_serv_maj_rec;
		        c_csr_basemae_cn = pcsr_basemae_cn;
		        c_csr_v_tx = pcsr_v_tx;
		        c_csr_d_debut_tx = pcsr_d_debut_tx;

        base.Initialise(pcsr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLinfos_conservatoire; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeinfos_conservatoire.Dictionnaire.Add(csr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeinfos_conservatoire.Dictionnaire.Remove(csr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_infos_conservatoire l_Clone = (clg_infos_conservatoire) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeparam_conservatoire.Count > 0) c_EstReference = true;
		if(c_Listeinfos_base.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_infos_conservatoire(null, csr_cn, csr_a_guid, csr_a_version_3_0, csr_a_nom, csr_a_libel, csr_n_lock, csr_a_msglock, csr_a_adresse_serv_sync, csr_n_port_serv_sync_env, csr_n_port_serv_sync_rec, csr_n_der_id_sync, csr_a_adresse_serv_maj, csr_n_port_serv_maj_env, csr_n_port_serv_maj_rec, csr_basemae_cn, csr_v_tx, csr_d_debut_tx,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_infos_conservatoire l_clone = (clg_infos_conservatoire) this.Clone;
		c_csr_cn = l_clone.csr_cn;
		c_csr_a_guid = l_clone.csr_a_guid;
		c_csr_a_version_3_0 = l_clone.csr_a_version_3_0;
		c_csr_a_nom = l_clone.csr_a_nom;
		c_csr_a_libel = l_clone.csr_a_libel;
		c_csr_n_lock = l_clone.csr_n_lock;
		c_csr_a_msglock = l_clone.csr_a_msglock;
		c_csr_a_adresse_serv_sync = l_clone.csr_a_adresse_serv_sync;
		c_csr_n_port_serv_sync_env = l_clone.csr_n_port_serv_sync_env;
		c_csr_n_port_serv_sync_rec = l_clone.csr_n_port_serv_sync_rec;
		c_csr_n_der_id_sync = l_clone.csr_n_der_id_sync;
		c_csr_a_adresse_serv_maj = l_clone.csr_a_adresse_serv_maj;
		c_csr_n_port_serv_maj_env = l_clone.csr_n_port_serv_maj_env;
		c_csr_n_port_serv_maj_rec = l_clone.csr_n_port_serv_maj_rec;
		c_csr_basemae_cn = l_clone.csr_basemae_cn;
		c_csr_v_tx = l_clone.csr_v_tx;
		c_csr_d_debut_tx = l_clone.csr_d_debut_tx;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLinfos_conservatoire.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLinfos_conservatoire.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLinfos_conservatoire.Delete(this);
    }

    /* Accesseur de la propriete csr_cn (csr_cn)
    * @return c_csr_cn */
    public Int64 csr_cn
    {
        get{return c_csr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_csr_cn != value)
                {
                    CreerClone();
                    c_csr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete csr_a_guid (csr_a_guid)
    * @return c_csr_a_guid */
    public string csr_a_guid
    {
        get{return c_csr_a_guid;}
        set
        {
            if(c_csr_a_guid != value)
            {
                CreerClone();
                c_csr_a_guid = value;
            }
        }
    }
    /* Accesseur de la propriete csr_a_version_3_0 (csr_a_version_3_0)
    * @return c_csr_a_version_3_0 */
    public string csr_a_version_3_0
    {
        get{return c_csr_a_version_3_0;}
        set
        {
            if(c_csr_a_version_3_0 != value)
            {
                CreerClone();
                c_csr_a_version_3_0 = value;
            }
        }
    }
    /* Accesseur de la propriete csr_a_nom (csr_a_nom)
    * @return c_csr_a_nom */
    public string csr_a_nom
    {
        get{return c_csr_a_nom;}
        set
        {
            if(value != null)
            {
                if(c_csr_a_nom != value)
                {
                    CreerClone();
                    c_csr_a_nom = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete csr_a_libel (csr_a_libel)
    * @return c_csr_a_libel */
    public string csr_a_libel
    {
        get{return c_csr_a_libel;}
        set
        {
            if(c_csr_a_libel != value)
            {
                CreerClone();
                c_csr_a_libel = value;
            }
        }
    }
    /* Accesseur de la propriete csr_n_lock (csr_n_lock)
    * @return c_csr_n_lock */
    public Int64 csr_n_lock
    {
        get{return c_csr_n_lock;}
        set
        {
            if(c_csr_n_lock != value)
            {
                CreerClone();
                c_csr_n_lock = value;
            }
        }
    }
    /* Accesseur de la propriete csr_a_msglock (csr_a_msglock)
    * @return c_csr_a_msglock */
    public string csr_a_msglock
    {
        get{return c_csr_a_msglock;}
        set
        {
            if(c_csr_a_msglock != value)
            {
                CreerClone();
                c_csr_a_msglock = value;
            }
        }
    }
    /* Accesseur de la propriete csr_a_adresse_serv_sync (csr_a_adresse_serv_sync)
    * @return c_csr_a_adresse_serv_sync */
    public string csr_a_adresse_serv_sync
    {
        get{return c_csr_a_adresse_serv_sync;}
        set
        {
            if(c_csr_a_adresse_serv_sync != value)
            {
                CreerClone();
                c_csr_a_adresse_serv_sync = value;
            }
        }
    }
    /* Accesseur de la propriete csr_n_port_serv_sync_env (csr_n_port_serv_sync_env)
    * @return c_csr_n_port_serv_sync_env */
    public Int64 csr_n_port_serv_sync_env
    {
        get{return c_csr_n_port_serv_sync_env;}
        set
        {
            if(c_csr_n_port_serv_sync_env != value)
            {
                CreerClone();
                c_csr_n_port_serv_sync_env = value;
            }
        }
    }
    /* Accesseur de la propriete csr_n_port_serv_sync_rec (csr_n_port_serv_sync_rec)
    * @return c_csr_n_port_serv_sync_rec */
    public Int64 csr_n_port_serv_sync_rec
    {
        get{return c_csr_n_port_serv_sync_rec;}
        set
        {
            if(c_csr_n_port_serv_sync_rec != value)
            {
                CreerClone();
                c_csr_n_port_serv_sync_rec = value;
            }
        }
    }
    /* Accesseur de la propriete csr_n_der_id_sync (csr_n_der_id_sync)
    * @return c_csr_n_der_id_sync */
    public Int64 csr_n_der_id_sync
    {
        get{return c_csr_n_der_id_sync;}
        set
        {
            if(c_csr_n_der_id_sync != value)
            {
                CreerClone();
                c_csr_n_der_id_sync = value;
            }
        }
    }
    /* Accesseur de la propriete csr_a_adresse_serv_maj (csr_a_adresse_serv_maj)
    * @return c_csr_a_adresse_serv_maj */
    public string csr_a_adresse_serv_maj
    {
        get{return c_csr_a_adresse_serv_maj;}
        set
        {
            if(c_csr_a_adresse_serv_maj != value)
            {
                CreerClone();
                c_csr_a_adresse_serv_maj = value;
            }
        }
    }
    /* Accesseur de la propriete csr_n_port_serv_maj_env (csr_n_port_serv_maj_env)
    * @return c_csr_n_port_serv_maj_env */
    public Int64 csr_n_port_serv_maj_env
    {
        get{return c_csr_n_port_serv_maj_env;}
        set
        {
            if(c_csr_n_port_serv_maj_env != value)
            {
                CreerClone();
                c_csr_n_port_serv_maj_env = value;
            }
        }
    }
    /* Accesseur de la propriete csr_n_port_serv_maj_rec (csr_n_port_serv_maj_rec)
    * @return c_csr_n_port_serv_maj_rec */
    public Int64 csr_n_port_serv_maj_rec
    {
        get{return c_csr_n_port_serv_maj_rec;}
        set
        {
            if(c_csr_n_port_serv_maj_rec != value)
            {
                CreerClone();
                c_csr_n_port_serv_maj_rec = value;
            }
        }
    }
    /* Accesseur de la propriete csr_basemae_cn (csr_basemae_cn)
    * @return c_csr_basemae_cn */
    public Int64 csr_basemae_cn
    {
        get{return c_csr_basemae_cn;}
        set
        {
            if(c_csr_basemae_cn != value)
            {
                CreerClone();
                c_csr_basemae_cn = value;
            }
        }
    }
    /* Accesseur de la propriete csr_v_tx (csr_v_tx)
    * @return c_csr_v_tx */
    public Int64 csr_v_tx
    {
        get{return c_csr_v_tx;}
        set
        {
            if(c_csr_v_tx != value)
            {
                CreerClone();
                c_csr_v_tx = value;
            }
        }
    }
    /* Accesseur de la propriete csr_d_debut_tx (csr_d_debut_tx)
    * @return c_csr_d_debut_tx */
    public Int64 csr_d_debut_tx
    {
        get{return c_csr_d_debut_tx;}
        set
        {
            if(c_csr_d_debut_tx != value)
            {
                CreerClone();
                c_csr_d_debut_tx = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type param_conservatoire */
    public List<clg_param_conservatoire> Listeparam_conservatoire
    {
        get { return c_Listeparam_conservatoire; }
    }    /* Accesseur de la liste des objets de type infos_base */
    public List<clg_infos_base> Listeinfos_base
    {
        get { return c_Listeinfos_base; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_csr_cn.ToString());
		l_Valeurs.Add(c_csr_a_guid.ToString());
		l_Valeurs.Add(c_csr_a_version_3_0.ToString());
		l_Valeurs.Add(c_csr_a_nom.ToString());
		l_Valeurs.Add(c_csr_a_libel.ToString());
		l_Valeurs.Add(c_csr_n_lock.ToString());
		l_Valeurs.Add(c_csr_a_msglock.ToString());
		l_Valeurs.Add(c_csr_a_adresse_serv_sync.ToString());
		l_Valeurs.Add(c_csr_n_port_serv_sync_env.ToString());
		l_Valeurs.Add(c_csr_n_port_serv_sync_rec.ToString());
		l_Valeurs.Add(c_csr_n_der_id_sync.ToString());
		l_Valeurs.Add(c_csr_a_adresse_serv_maj.ToString());
		l_Valeurs.Add(c_csr_n_port_serv_maj_env.ToString());
		l_Valeurs.Add(c_csr_n_port_serv_maj_rec.ToString());
		l_Valeurs.Add(c_csr_basemae_cn.ToString());
		l_Valeurs.Add(c_csr_v_tx.ToString());
		l_Valeurs.Add(c_csr_d_debut_tx.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("csr_cn");
		l_Noms.Add("csr_a_guid");
		l_Noms.Add("csr_a_version_3_0");
		l_Noms.Add("csr_a_nom");
		l_Noms.Add("csr_a_libel");
		l_Noms.Add("csr_n_lock");
		l_Noms.Add("csr_a_msglock");
		l_Noms.Add("csr_a_adresse_serv_sync");
		l_Noms.Add("csr_n_port_serv_sync_env");
		l_Noms.Add("csr_n_port_serv_sync_rec");
		l_Noms.Add("csr_n_der_id_sync");
		l_Noms.Add("csr_a_adresse_serv_maj");
		l_Noms.Add("csr_n_port_serv_maj_env");
		l_Noms.Add("csr_n_port_serv_maj_rec");
		l_Noms.Add("csr_basemae_cn");
		l_Noms.Add("csr_v_tx");
		l_Noms.Add("csr_d_debut_tx");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
