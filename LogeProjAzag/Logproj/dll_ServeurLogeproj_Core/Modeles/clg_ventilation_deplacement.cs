namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : ventilation_deplacement </summary>
public partial class clg_ventilation_deplacement : clg_deplacements_realises
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLventilation_deplacement;
    protected clg_Modele c_Modele;

	private DateTime c_vdr_d_daterappro;
	private clg_elements c_elements;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_elements = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_ventilation_deplacement(clg_Modele pModele, Int64 pdpr_cn, bool pAMAJ = false) : base(pModele, pdpr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_ventilation_deplacement(clg_Modele pModele, Int64 pdeplacements_realises, DateTime pvdr_d_daterappro, clg_elements pelements, clg_vehicules pvehicules, DateTime pdpr_d_date, double pdpr_n_nbkm, double pdpr_n_kmmin, double pdpr_n_kmmax, double pdpr_n_montant, clg_taux_km_reels ptaux_km_reels, string pdpr_a_dest, string pdpr_a_utilisateur, string pdpr_a_codeana, string pdpr_a_objet, bool pAMAJ = true) : base(pModele, pdeplacements_realises, pvehicules, pdpr_d_date, pdpr_n_nbkm, pdpr_n_kmmin, pdpr_n_kmmax, pdpr_n_montant, ptaux_km_reels, pdpr_a_dest, pdpr_a_utilisateur, pdpr_a_codeana, pdpr_a_objet, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        c_vdr_d_daterappro = pvdr_d_daterappro;
		        c_elements = pelements;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pdeplacements_realises, DateTime pvdr_d_daterappro, Int64 pelements, Int64 pvehicules, DateTime pdpr_d_date, double pdpr_n_nbkm, double pdpr_n_kmmin, double pdpr_n_kmmax, double pdpr_n_montant, Int64 ptaux_km_reels, string pdpr_a_dest, string pdpr_a_utilisateur, string pdpr_a_codeana, string pdpr_a_objet)
    {   
		        c_vdr_d_daterappro = pvdr_d_daterappro;
		c_dicReferences.Add("elements", pelements);

        base.Initialise(pdeplacements_realises, pvehicules, pdpr_d_date, pdpr_n_nbkm, pdpr_n_kmmin, pdpr_n_kmmax, pdpr_n_montant, ptaux_km_reels, pdpr_a_dest, pdpr_a_utilisateur, pdpr_a_codeana, pdpr_a_objet);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLventilation_deplacement; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		base.CreeLiens();
		c_elements = (clg_elements) c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeventilation_deplacement.Dictionnaire.Add(dpr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeventilation_deplacement.Dictionnaire.Remove(dpr_cn);
		base.SupprimeDansListe();

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		base.CreeReferences();
		if(c_elements != null)if(!c_elements.Listeventilation_deplacement.Contains(this)) c_elements.Listeventilation_deplacement.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_ventilation_deplacement l_Clone = (clg_ventilation_deplacement) Clone;
		base.DetruitReferences();
		if(l_Clone.elements != null)if(l_Clone.elements.Listeventilation_deplacement.Contains(this)) l_Clone.elements.Listeventilation_deplacement.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_ventilation_deplacement(null, dpr_cn, vdr_d_daterappro, elements, vehicules, dpr_d_date, dpr_n_nbkm, dpr_n_kmmin, dpr_n_kmmax, dpr_n_montant, taux_km_reels, dpr_a_dest, dpr_a_utilisateur, dpr_a_codeana, dpr_a_objet,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_ventilation_deplacement l_clone = (clg_ventilation_deplacement) this.Clone;
		c_vdr_d_daterappro = l_clone.vdr_d_daterappro;
		c_elements = l_clone.elements;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLventilation_deplacement.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLventilation_deplacement.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLventilation_deplacement.Delete(this);
    }

    /* Accesseur de la propriete vdr_d_daterappro (vdr_d_daterappro)
    * @return c_vdr_d_daterappro */
    public DateTime vdr_d_daterappro
    {
        get{return c_vdr_d_daterappro;}
        set
        {
            if(c_vdr_d_daterappro != value)
            {
                CreerClone();
                c_vdr_d_daterappro = value;
            }
        }
    }
    /* Accesseur de la propriete elements (vdr_ele_cn)
    * @return c_elements */
    public clg_elements elements
    {
        get{return c_elements;}
        set
        {
            if(c_elements != value)
            {
                CreerClone();
                c_elements = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_vdr_d_daterappro.ToString());
		l_Valeurs.Add(c_elements==null ? "-1" : c_elements.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("deplacements_realises");
		l_Noms.Add("vdr_d_daterappro");
		l_Noms.Add("elements");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
