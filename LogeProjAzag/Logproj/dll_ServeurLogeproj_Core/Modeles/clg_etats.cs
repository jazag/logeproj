namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : etats </summary>
public partial class clg_etats : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLetats;
    protected clg_Modele c_Modele;

	private Int64 c_ett_cn;
	private string c_ett_a_lib;
	private string c_ett_a_modele;
	private clg_t_etat c_t_etat;
	private clg_utilisateur c_utilisateur;
	private Int64 c_ett_n_public;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_t_etat = null;
		this.c_utilisateur = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_etats(clg_Modele pModele, Int64 pett_cn, bool pAMAJ = false) : base(pModele, pett_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_etats(clg_Modele pModele, Int64 pett_cn, string pett_a_lib, string pett_a_modele, clg_t_etat pt_etat, clg_utilisateur putilisateur, Int64 pett_n_public, bool pAMAJ = true) : base(pModele, pett_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pett_cn != Int64.MinValue)
            c_ett_cn = pett_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pett_a_lib != null)
            c_ett_a_lib = pett_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_ett_a_modele = pett_a_modele;
		        if(pt_etat != null)
            c_t_etat = pt_etat;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_utilisateur = putilisateur;
		        c_ett_n_public = pett_n_public;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pett_cn, string pett_a_lib, string pett_a_modele, Int64 pt_etat, Int64 putilisateur, Int64 pett_n_public)
    {   
		        c_ett_cn = pett_cn;
		        c_ett_a_lib = pett_a_lib;
		        c_ett_a_modele = pett_a_modele;
		c_dicReferences.Add("t_etat", pt_etat);
		c_dicReferences.Add("utilisateur", putilisateur);
		        c_ett_n_public = pett_n_public;

        base.Initialise(pett_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLetats; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_t_etat = (clg_t_etat) c_ModeleBase.RenvoieObjet(c_dicReferences["t_etat"], "clg_t_etat");
		c_utilisateur = (clg_utilisateur) c_ModeleBase.RenvoieObjet(c_dicReferences["utilisateur"], "clg_utilisateur");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeetats.Dictionnaire.Add(ett_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeetats.Dictionnaire.Remove(ett_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_t_etat != null)if(!c_t_etat.Listeetats.Contains(this)) c_t_etat.Listeetats.Add(this);
		if(c_utilisateur != null)if(!c_utilisateur.Listeetats.Contains(this)) c_utilisateur.Listeetats.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_etats l_Clone = (clg_etats) Clone;
		if(l_Clone.t_etat != null)if(l_Clone.t_etat.Listeetats.Contains(this)) l_Clone.t_etat.Listeetats.Remove(this);
		if(l_Clone.utilisateur != null)if(l_Clone.utilisateur.Listeetats.Contains(this)) l_Clone.utilisateur.Listeetats.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_etats(null, ett_cn, ett_a_lib, ett_a_modele, t_etat, utilisateur, ett_n_public,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_etats l_clone = (clg_etats) this.Clone;
		c_ett_cn = l_clone.ett_cn;
		c_ett_a_lib = l_clone.ett_a_lib;
		c_ett_a_modele = l_clone.ett_a_modele;
		c_t_etat = l_clone.t_etat;
		c_utilisateur = l_clone.utilisateur;
		c_ett_n_public = l_clone.ett_n_public;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLetats.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLetats.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLetats.Delete(this);
    }

    /* Accesseur de la propriete ett_cn (ett_cn)
    * @return c_ett_cn */
    public Int64 ett_cn
    {
        get{return c_ett_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ett_cn != value)
                {
                    CreerClone();
                    c_ett_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ett_a_lib (ett_a_lib)
    * @return c_ett_a_lib */
    public string ett_a_lib
    {
        get{return c_ett_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_ett_a_lib != value)
                {
                    CreerClone();
                    c_ett_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ett_a_modele (ett_a_modele)
    * @return c_ett_a_modele */
    public string ett_a_modele
    {
        get{return c_ett_a_modele;}
        set
        {
            if(c_ett_a_modele != value)
            {
                CreerClone();
                c_ett_a_modele = value;
            }
        }
    }
    /* Accesseur de la propriete t_etat (ett_t_ett_cn)
    * @return c_t_etat */
    public clg_t_etat t_etat
    {
        get{return c_t_etat;}
        set
        {
            if(value != null)
            {
                if(c_t_etat != value)
                {
                    CreerClone();
                    c_t_etat = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete utilisateur (ett_uti_cn)
    * @return c_utilisateur */
    public clg_utilisateur utilisateur
    {
        get{return c_utilisateur;}
        set
        {
            if(c_utilisateur != value)
            {
                CreerClone();
                c_utilisateur = value;
            }
        }
    }
    /* Accesseur de la propriete ett_n_public (ett_n_public)
    * @return c_ett_n_public */
    public Int64 ett_n_public
    {
        get{return c_ett_n_public;}
        set
        {
            if(c_ett_n_public != value)
            {
                CreerClone();
                c_ett_n_public = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ett_cn.ToString());
		l_Valeurs.Add(c_ett_a_lib.ToString());
		l_Valeurs.Add(c_ett_a_modele.ToString());
		l_Valeurs.Add(c_t_etat==null ? "-1" : c_t_etat.ID.ToString());
		l_Valeurs.Add(c_utilisateur==null ? "-1" : c_utilisateur.ID.ToString());
		l_Valeurs.Add(c_ett_n_public.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ett_cn");
		l_Noms.Add("ett_a_lib");
		l_Noms.Add("ett_a_modele");
		l_Noms.Add("t_etat");
		l_Noms.Add("utilisateur");
		l_Noms.Add("ett_n_public");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
