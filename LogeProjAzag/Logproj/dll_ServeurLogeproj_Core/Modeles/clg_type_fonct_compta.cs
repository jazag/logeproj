namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : type_fonct_compta </summary>
public partial class clg_type_fonct_compta : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtype_fonct_compta;
    protected clg_Modele c_Modele;

	private Int64 c_tfc_cn;
	private string c_tfc_a_code;
	private string c_tfc_a_cmt;
	private string c_tfc_a_fonction;
	private List<clg_param_compte> c_Listeparam_compte;
	private List<clg_tb_sage> c_Listetb_sage;


    private void Init()
    {
		c_Listeparam_compte = new List<clg_param_compte>();
		c_Listetb_sage = new List<clg_tb_sage>();

    }

    public override void Detruit()
    {
		c_Listeparam_compte.Clear();
		c_Listetb_sage.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_type_fonct_compta(clg_Modele pModele, Int64 ptfc_cn, bool pAMAJ = false) : base(pModele, ptfc_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_type_fonct_compta(clg_Modele pModele, Int64 ptfc_cn, string ptfc_a_code, string ptfc_a_cmt, string ptfc_a_fonction, bool pAMAJ = true) : base(pModele, ptfc_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptfc_cn != Int64.MinValue)
            c_tfc_cn = ptfc_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_tfc_a_code = ptfc_a_code;
		        c_tfc_a_cmt = ptfc_a_cmt;
		        c_tfc_a_fonction = ptfc_a_fonction;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptfc_cn, string ptfc_a_code, string ptfc_a_cmt, string ptfc_a_fonction)
    {   
		        c_tfc_cn = ptfc_cn;
		        c_tfc_a_code = ptfc_a_code;
		        c_tfc_a_cmt = ptfc_a_cmt;
		        c_tfc_a_fonction = ptfc_a_fonction;

        base.Initialise(ptfc_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtype_fonct_compta; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetype_fonct_compta.Dictionnaire.Add(tfc_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetype_fonct_compta.Dictionnaire.Remove(tfc_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_type_fonct_compta l_Clone = (clg_type_fonct_compta) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeparam_compte.Count > 0) c_EstReference = true;
		if(c_Listetb_sage.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_type_fonct_compta(null, tfc_cn, tfc_a_code, tfc_a_cmt, tfc_a_fonction,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_type_fonct_compta l_clone = (clg_type_fonct_compta) this.Clone;
		c_tfc_cn = l_clone.tfc_cn;
		c_tfc_a_code = l_clone.tfc_a_code;
		c_tfc_a_cmt = l_clone.tfc_a_cmt;
		c_tfc_a_fonction = l_clone.tfc_a_fonction;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtype_fonct_compta.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtype_fonct_compta.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtype_fonct_compta.Delete(this);
    }

    /* Accesseur de la propriete tfc_cn (tfc_cn)
    * @return c_tfc_cn */
    public Int64 tfc_cn
    {
        get{return c_tfc_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tfc_cn != value)
                {
                    CreerClone();
                    c_tfc_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete tfc_a_code (tfc_a_code)
    * @return c_tfc_a_code */
    public string tfc_a_code
    {
        get{return c_tfc_a_code;}
        set
        {
            if(c_tfc_a_code != value)
            {
                CreerClone();
                c_tfc_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete tfc_a_cmt (tfc_a_cmt)
    * @return c_tfc_a_cmt */
    public string tfc_a_cmt
    {
        get{return c_tfc_a_cmt;}
        set
        {
            if(c_tfc_a_cmt != value)
            {
                CreerClone();
                c_tfc_a_cmt = value;
            }
        }
    }
    /* Accesseur de la propriete tfc_a_fonction (tfc_a_fonction)
    * @return c_tfc_a_fonction */
    public string tfc_a_fonction
    {
        get{return c_tfc_a_fonction;}
        set
        {
            if(c_tfc_a_fonction != value)
            {
                CreerClone();
                c_tfc_a_fonction = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type param_compte */
    public List<clg_param_compte> Listeparam_compte
    {
        get { return c_Listeparam_compte; }
    }    /* Accesseur de la liste des objets de type tb_sage */
    public List<clg_tb_sage> Listetb_sage
    {
        get { return c_Listetb_sage; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tfc_cn.ToString());
		l_Valeurs.Add(c_tfc_a_code.ToString());
		l_Valeurs.Add(c_tfc_a_cmt.ToString());
		l_Valeurs.Add(c_tfc_a_fonction.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tfc_cn");
		l_Noms.Add("tfc_a_code");
		l_Noms.Add("tfc_a_cmt");
		l_Noms.Add("tfc_a_fonction");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
