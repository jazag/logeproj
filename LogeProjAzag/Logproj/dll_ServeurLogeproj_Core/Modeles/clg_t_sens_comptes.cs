namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_sens_comptes </summary>
public partial class clg_t_sens_comptes : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_sens_comptes;
    protected clg_Modele c_Modele;

	private Int64 c_tsc_cn;
	private string c_tsc_a_libel;
	private string c_tsc_a_lettre;
	private string c_tsc_m_desc;
	private List<clg_comptes_non_affichables> c_Listecomptes_non_affichables;


    private void Init()
    {
		c_Listecomptes_non_affichables = new List<clg_comptes_non_affichables>();

    }

    public override void Detruit()
    {
		c_Listecomptes_non_affichables.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_sens_comptes(clg_Modele pModele, Int64 ptsc_cn, bool pAMAJ = false) : base(pModele, ptsc_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_sens_comptes(clg_Modele pModele, Int64 ptsc_cn, string ptsc_a_libel, string ptsc_a_lettre, string ptsc_m_desc, bool pAMAJ = true) : base(pModele, ptsc_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptsc_cn != Int64.MinValue)
            c_tsc_cn = ptsc_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_tsc_a_libel = ptsc_a_libel;
		        c_tsc_a_lettre = ptsc_a_lettre;
		        c_tsc_m_desc = ptsc_m_desc;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptsc_cn, string ptsc_a_libel, string ptsc_a_lettre, string ptsc_m_desc)
    {   
		        c_tsc_cn = ptsc_cn;
		        c_tsc_a_libel = ptsc_a_libel;
		        c_tsc_a_lettre = ptsc_a_lettre;
		        c_tsc_m_desc = ptsc_m_desc;

        base.Initialise(ptsc_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_sens_comptes; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_sens_comptes.Dictionnaire.Add(tsc_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_sens_comptes.Dictionnaire.Remove(tsc_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_sens_comptes l_Clone = (clg_t_sens_comptes) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecomptes_non_affichables.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_sens_comptes(null, tsc_cn, tsc_a_libel, tsc_a_lettre, tsc_m_desc,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_sens_comptes l_clone = (clg_t_sens_comptes) this.Clone;
		c_tsc_cn = l_clone.tsc_cn;
		c_tsc_a_libel = l_clone.tsc_a_libel;
		c_tsc_a_lettre = l_clone.tsc_a_lettre;
		c_tsc_m_desc = l_clone.tsc_m_desc;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_sens_comptes.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_sens_comptes.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_sens_comptes.Delete(this);
    }

    /* Accesseur de la propriete tsc_cn (tsc_cn)
    * @return c_tsc_cn */
    public Int64 tsc_cn
    {
        get{return c_tsc_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tsc_cn != value)
                {
                    CreerClone();
                    c_tsc_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete tsc_a_libel (tsc_a_libel)
    * @return c_tsc_a_libel */
    public string tsc_a_libel
    {
        get{return c_tsc_a_libel;}
        set
        {
            if(c_tsc_a_libel != value)
            {
                CreerClone();
                c_tsc_a_libel = value;
            }
        }
    }
    /* Accesseur de la propriete tsc_a_lettre (tsc_a_lettre)
    * @return c_tsc_a_lettre */
    public string tsc_a_lettre
    {
        get{return c_tsc_a_lettre;}
        set
        {
            if(c_tsc_a_lettre != value)
            {
                CreerClone();
                c_tsc_a_lettre = value;
            }
        }
    }
    /* Accesseur de la propriete tsc_m_desc (tsc_m_desc)
    * @return c_tsc_m_desc */
    public string tsc_m_desc
    {
        get{return c_tsc_m_desc;}
        set
        {
            if(c_tsc_m_desc != value)
            {
                CreerClone();
                c_tsc_m_desc = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type comptes_non_affichables */
    public List<clg_comptes_non_affichables> Listecomptes_non_affichables
    {
        get { return c_Listecomptes_non_affichables; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tsc_cn.ToString());
		l_Valeurs.Add(c_tsc_a_libel.ToString());
		l_Valeurs.Add(c_tsc_a_lettre.ToString());
		l_Valeurs.Add(c_tsc_m_desc.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tsc_cn");
		l_Noms.Add("tsc_a_libel");
		l_Noms.Add("tsc_a_lettre");
		l_Noms.Add("tsc_m_desc");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
