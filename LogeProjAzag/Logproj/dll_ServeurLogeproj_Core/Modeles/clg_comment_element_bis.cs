namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : comment_element_bis </summary>
public partial class clg_comment_element_bis : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcomment_element_bis;
    protected clg_Modele c_Modele;

	private Int64 c_cme_com_cn;
	private Int64 c_cme_ele_cn;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_comment_element_bis(clg_Modele pModele, Int64 pcme_com_cn, bool pAMAJ = false) : base(pModele, pcme_com_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_comment_element_bis(clg_Modele pModele, Int64 pcme_com_cn, Int64 pcme_ele_cn, bool pAMAJ = true) : base(pModele, pcme_com_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcme_com_cn != Int64.MinValue)
            c_cme_com_cn = pcme_com_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pcme_ele_cn != Int64.MinValue)
            c_cme_ele_cn = pcme_ele_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcme_com_cn, Int64 pcme_ele_cn)
    {   
		        c_cme_com_cn = pcme_com_cn;
		        c_cme_ele_cn = pcme_ele_cn;

        base.Initialise(pcme_com_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcomment_element_bis; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecomment_element_bis.Dictionnaire.Add(cme_com_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecomment_element_bis.Dictionnaire.Remove(cme_com_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_comment_element_bis l_Clone = (clg_comment_element_bis) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_comment_element_bis(null, cme_com_cn, cme_ele_cn,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_comment_element_bis l_clone = (clg_comment_element_bis) this.Clone;
		c_cme_com_cn = l_clone.cme_com_cn;
		c_cme_ele_cn = l_clone.cme_ele_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcomment_element_bis.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcomment_element_bis.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcomment_element_bis.Delete(this);
    }

    /* Accesseur de la propriete cme_com_cn (cme_com_cn)
    * @return c_cme_com_cn */
    public Int64 cme_com_cn
    {
        get{return c_cme_com_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cme_com_cn != value)
                {
                    CreerClone();
                    c_cme_com_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete cme_ele_cn (cme_ele_cn)
    * @return c_cme_ele_cn */
    public Int64 cme_ele_cn
    {
        get{return c_cme_ele_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cme_ele_cn != value)
                {
                    CreerClone();
                    c_cme_ele_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cme_com_cn.ToString());
		l_Valeurs.Add(c_cme_ele_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cme_com_cn");
		l_Noms.Add("cme_ele_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
