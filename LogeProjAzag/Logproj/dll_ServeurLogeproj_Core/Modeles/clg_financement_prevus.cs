namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : financement_prevus </summary>
public partial class clg_financement_prevus : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfinancement_prevus;
    protected clg_Modele c_Modele;

	private Int64 c_fnp_cn;
	private clg_financeurs c_financeurs;
	private clg_operations c_operations;
	private double c_fnp_n_montant;
	private double c_fmp_n_pourcent;
	private Int64 c_fmp_n_bloc_montant;
	private List<clg_taux_fin> c_Listetaux_fin;


    private void Init()
    {
		c_Listetaux_fin = new List<clg_taux_fin>();

    }

    public override void Detruit()
    {
		c_Listetaux_fin.Clear();
		this.c_financeurs = null;
		this.c_operations = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_financement_prevus(clg_Modele pModele, Int64 pfnp_cn, bool pAMAJ = false) : base(pModele, pfnp_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_financement_prevus(clg_Modele pModele, Int64 pfnp_cn, clg_financeurs pfinanceurs, clg_operations poperations, double pfnp_n_montant, double pfmp_n_pourcent, Int64 pfmp_n_bloc_montant, bool pAMAJ = true) : base(pModele, pfnp_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfnp_cn != Int64.MinValue)
            c_fnp_cn = pfnp_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pfinanceurs != null)
            c_financeurs = pfinanceurs;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(poperations != null)
            c_operations = poperations;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_fnp_n_montant = pfnp_n_montant;
		        c_fmp_n_pourcent = pfmp_n_pourcent;
		        c_fmp_n_bloc_montant = pfmp_n_bloc_montant;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfnp_cn, Int64 pfinanceurs, Int64 poperations, double pfnp_n_montant, double pfmp_n_pourcent, Int64 pfmp_n_bloc_montant)
    {   
		        c_fnp_cn = pfnp_cn;
		c_dicReferences.Add("financeurs", pfinanceurs);
		c_dicReferences.Add("operations", poperations);
		        c_fnp_n_montant = pfnp_n_montant;
		        c_fmp_n_pourcent = pfmp_n_pourcent;
		        c_fmp_n_bloc_montant = pfmp_n_bloc_montant;

        base.Initialise(pfnp_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfinancement_prevus; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_financeurs = (clg_financeurs) c_ModeleBase.RenvoieObjet(c_dicReferences["financeurs"], "clg_financeurs");
		c_operations = (clg_operations) c_ModeleBase.RenvoieObjet(c_dicReferences["operations"], "clg_operations");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefinancement_prevus.Dictionnaire.Add(fnp_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefinancement_prevus.Dictionnaire.Remove(fnp_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_financeurs != null)if(!c_financeurs.Listefinancement_prevus.Contains(this)) c_financeurs.Listefinancement_prevus.Add(this);
		if(c_operations != null)if(!c_operations.Listefinancement_prevus.Contains(this)) c_operations.Listefinancement_prevus.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_financement_prevus l_Clone = (clg_financement_prevus) Clone;
		if(l_Clone.financeurs != null)if(l_Clone.financeurs.Listefinancement_prevus.Contains(this)) l_Clone.financeurs.Listefinancement_prevus.Remove(this);
		if(l_Clone.operations != null)if(l_Clone.operations.Listefinancement_prevus.Contains(this)) l_Clone.operations.Listefinancement_prevus.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listetaux_fin.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_financement_prevus(null, fnp_cn, financeurs, operations, fnp_n_montant, fmp_n_pourcent, fmp_n_bloc_montant,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_financement_prevus l_clone = (clg_financement_prevus) this.Clone;
		c_fnp_cn = l_clone.fnp_cn;
		c_financeurs = l_clone.financeurs;
		c_operations = l_clone.operations;
		c_fnp_n_montant = l_clone.fnp_n_montant;
		c_fmp_n_pourcent = l_clone.fmp_n_pourcent;
		c_fmp_n_bloc_montant = l_clone.fmp_n_bloc_montant;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfinancement_prevus.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfinancement_prevus.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfinancement_prevus.Delete(this);
    }

    /* Accesseur de la propriete fnp_cn (fnp_cn)
    * @return c_fnp_cn */
    public Int64 fnp_cn
    {
        get{return c_fnp_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_fnp_cn != value)
                {
                    CreerClone();
                    c_fnp_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete financeurs (fnp_fin_cn)
    * @return c_financeurs */
    public clg_financeurs financeurs
    {
        get{return c_financeurs;}
        set
        {
            if(value != null)
            {
                if(c_financeurs != value)
                {
                    CreerClone();
                    c_financeurs = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete operations (fnp_ope_ele_cn)
    * @return c_operations */
    public clg_operations operations
    {
        get{return c_operations;}
        set
        {
            if(value != null)
            {
                if(c_operations != value)
                {
                    CreerClone();
                    c_operations = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete fnp_n_montant (fnp_n_montant)
    * @return c_fnp_n_montant */
    public double fnp_n_montant
    {
        get{return c_fnp_n_montant;}
        set
        {
            if(c_fnp_n_montant != value)
            {
                CreerClone();
                c_fnp_n_montant = value;
            }
        }
    }
    /* Accesseur de la propriete fmp_n_pourcent (fmp_n_pourcent)
    * @return c_fmp_n_pourcent */
    public double fmp_n_pourcent
    {
        get{return c_fmp_n_pourcent;}
        set
        {
            if(c_fmp_n_pourcent != value)
            {
                CreerClone();
                c_fmp_n_pourcent = value;
            }
        }
    }
    /* Accesseur de la propriete fmp_n_bloc_montant (fmp_n_bloc_montant)
    * @return c_fmp_n_bloc_montant */
    public Int64 fmp_n_bloc_montant
    {
        get{return c_fmp_n_bloc_montant;}
        set
        {
            if(c_fmp_n_bloc_montant != value)
            {
                CreerClone();
                c_fmp_n_bloc_montant = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type taux_fin */
    public List<clg_taux_fin> Listetaux_fin
    {
        get { return c_Listetaux_fin; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_fnp_cn.ToString());
		l_Valeurs.Add(c_financeurs==null ? "-1" : c_financeurs.ID.ToString());
		l_Valeurs.Add(c_operations==null ? "-1" : c_operations.ID.ToString());
		l_Valeurs.Add(c_fnp_n_montant.ToString());
		l_Valeurs.Add(c_fmp_n_pourcent.ToString());
		l_Valeurs.Add(c_fmp_n_bloc_montant.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("fnp_cn");
		l_Noms.Add("financeurs");
		l_Noms.Add("operations");
		l_Noms.Add("fnp_n_montant");
		l_Noms.Add("fmp_n_pourcent");
		l_Noms.Add("fmp_n_bloc_montant");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
