namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_fonctions </summary>
public partial class clg_t_fonctions : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_fonctions;
    protected clg_Modele c_Modele;

	private Int64 c_t_fct_cn;
	private string c_t_fct_a_lib;
	private double c_t_fct_n_taux_env;
	private List<clg_fonctions_perso> c_Listefonctions_perso;
	private List<clg_intervenants> c_Listeintervenants;
	private List<clg_taux> c_Listetaux;


    private void Init()
    {
		c_Listefonctions_perso = new List<clg_fonctions_perso>();
		c_Listeintervenants = new List<clg_intervenants>();
		c_Listetaux = new List<clg_taux>();

    }

    public override void Detruit()
    {
		c_Listefonctions_perso.Clear();
		c_Listeintervenants.Clear();
		c_Listetaux.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_fonctions(clg_Modele pModele, Int64 pt_fct_cn, bool pAMAJ = false) : base(pModele, pt_fct_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_fonctions(clg_Modele pModele, Int64 pt_fct_cn, string pt_fct_a_lib, double pt_fct_n_taux_env, bool pAMAJ = true) : base(pModele, pt_fct_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_fct_cn != Int64.MinValue)
            c_t_fct_cn = pt_fct_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_fct_a_lib != null)
            c_t_fct_a_lib = pt_fct_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pt_fct_n_taux_env != double.MinValue)
            c_t_fct_n_taux_env = pt_fct_n_taux_env;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_fct_cn, string pt_fct_a_lib, double pt_fct_n_taux_env)
    {   
		        c_t_fct_cn = pt_fct_cn;
		        c_t_fct_a_lib = pt_fct_a_lib;
		        c_t_fct_n_taux_env = pt_fct_n_taux_env;

        base.Initialise(pt_fct_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_fonctions; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_fonctions.Dictionnaire.Add(t_fct_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_fonctions.Dictionnaire.Remove(t_fct_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_fonctions l_Clone = (clg_t_fonctions) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listefonctions_perso.Count > 0) c_EstReference = true;
		if(c_Listeintervenants.Count > 0) c_EstReference = true;
		if(c_Listetaux.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_fonctions(null, t_fct_cn, t_fct_a_lib, t_fct_n_taux_env,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_fonctions l_clone = (clg_t_fonctions) this.Clone;
		c_t_fct_cn = l_clone.t_fct_cn;
		c_t_fct_a_lib = l_clone.t_fct_a_lib;
		c_t_fct_n_taux_env = l_clone.t_fct_n_taux_env;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_fonctions.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_fonctions.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_fonctions.Delete(this);
    }

    /* Accesseur de la propriete t_fct_cn (t_fct_cn)
    * @return c_t_fct_cn */
    public Int64 t_fct_cn
    {
        get{return c_t_fct_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_fct_cn != value)
                {
                    CreerClone();
                    c_t_fct_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_fct_a_lib (t_fct_a_lib)
    * @return c_t_fct_a_lib */
    public string t_fct_a_lib
    {
        get{return c_t_fct_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_t_fct_a_lib != value)
                {
                    CreerClone();
                    c_t_fct_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_fct_n_taux_env (t_fct_n_taux_env)
    * @return c_t_fct_n_taux_env */
    public double t_fct_n_taux_env
    {
        get{return c_t_fct_n_taux_env;}
        set
        {
            if(value != double.MinValue)
            {
                if(c_t_fct_n_taux_env != value)
                {
                    CreerClone();
                    c_t_fct_n_taux_env = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type fonctions_perso */
    public List<clg_fonctions_perso> Listefonctions_perso
    {
        get { return c_Listefonctions_perso; }
    }    /* Accesseur de la liste des objets de type intervenants */
    public List<clg_intervenants> Listeintervenants
    {
        get { return c_Listeintervenants; }
    }    /* Accesseur de la liste des objets de type taux */
    public List<clg_taux> Listetaux
    {
        get { return c_Listetaux; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_fct_cn.ToString());
		l_Valeurs.Add(c_t_fct_a_lib.ToString());
		l_Valeurs.Add(c_t_fct_n_taux_env.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_fct_cn");
		l_Noms.Add("t_fct_a_lib");
		l_Noms.Add("t_fct_n_taux_env");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
