namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : journal </summary>
public partial class clg_journal : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLjournal;
    protected clg_Modele c_Modele;

	private string c_jou_a_code;
	private string c_jou_a_lib;
	private string c_jou_a_cmt;
	private string c_jou_a_chemin_icone;
	private string c_jou_a_chemin_icone_ventil;
	private List<clg_lien_sage> c_Listelien_sage;


    private void Init()
    {
		c_Listelien_sage = new List<clg_lien_sage>();

    }

    public override void Detruit()
    {
		c_Listelien_sage.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_journal(clg_Modele pModele, string pjou_a_code, bool pAMAJ = false) : base(pModele, pjou_a_code, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_journal(clg_Modele pModele, string pjou_a_code, string pjou_a_lib, string pjou_a_cmt, string pjou_a_chemin_icone, string pjou_a_chemin_icone_ventil, bool pAMAJ = true) : base(pModele, pjou_a_code, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pjou_a_code != null)
            c_jou_a_code = pjou_a_code;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_jou_a_lib = pjou_a_lib;
		        c_jou_a_cmt = pjou_a_cmt;
		        c_jou_a_chemin_icone = pjou_a_chemin_icone;
		        c_jou_a_chemin_icone_ventil = pjou_a_chemin_icone_ventil;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(string pjou_a_code, string pjou_a_lib, string pjou_a_cmt, string pjou_a_chemin_icone, string pjou_a_chemin_icone_ventil)
    {   
		        c_jou_a_code = pjou_a_code;
		        c_jou_a_lib = pjou_a_lib;
		        c_jou_a_cmt = pjou_a_cmt;
		        c_jou_a_chemin_icone = pjou_a_chemin_icone;
		        c_jou_a_chemin_icone_ventil = pjou_a_chemin_icone_ventil;

        base.Initialise(pjou_a_code);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLjournal; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listejournal.Dictionnaire.Add(jou_a_code, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listejournal.Dictionnaire.Remove(jou_a_code);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_journal l_Clone = (clg_journal) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listelien_sage.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_journal(null, jou_a_code, jou_a_lib, jou_a_cmt, jou_a_chemin_icone, jou_a_chemin_icone_ventil,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_journal l_clone = (clg_journal) this.Clone;
		c_jou_a_code = l_clone.jou_a_code;
		c_jou_a_lib = l_clone.jou_a_lib;
		c_jou_a_cmt = l_clone.jou_a_cmt;
		c_jou_a_chemin_icone = l_clone.jou_a_chemin_icone;
		c_jou_a_chemin_icone_ventil = l_clone.jou_a_chemin_icone_ventil;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLjournal.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLjournal.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLjournal.Delete(this);
    }

    /* Accesseur de la propriete jou_a_code (jou_a_code)
    * @return c_jou_a_code */
    public string jou_a_code
    {
        get{return c_jou_a_code;}
        set
        {
            if(value != null)
            {
                if(c_jou_a_code != value)
                {
                    CreerClone();
                    c_jou_a_code = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete jou_a_lib (jou_a_lib)
    * @return c_jou_a_lib */
    public string jou_a_lib
    {
        get{return c_jou_a_lib;}
        set
        {
            if(c_jou_a_lib != value)
            {
                CreerClone();
                c_jou_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete jou_a_cmt (jou_a_cmt)
    * @return c_jou_a_cmt */
    public string jou_a_cmt
    {
        get{return c_jou_a_cmt;}
        set
        {
            if(c_jou_a_cmt != value)
            {
                CreerClone();
                c_jou_a_cmt = value;
            }
        }
    }
    /* Accesseur de la propriete jou_a_chemin_icone (jou_a_chemin_icone)
    * @return c_jou_a_chemin_icone */
    public string jou_a_chemin_icone
    {
        get{return c_jou_a_chemin_icone;}
        set
        {
            if(c_jou_a_chemin_icone != value)
            {
                CreerClone();
                c_jou_a_chemin_icone = value;
            }
        }
    }
    /* Accesseur de la propriete jou_a_chemin_icone_ventil (jou_a_chemin_icone_ventil)
    * @return c_jou_a_chemin_icone_ventil */
    public string jou_a_chemin_icone_ventil
    {
        get{return c_jou_a_chemin_icone_ventil;}
        set
        {
            if(c_jou_a_chemin_icone_ventil != value)
            {
                CreerClone();
                c_jou_a_chemin_icone_ventil = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type lien_sage */
    public List<clg_lien_sage> Listelien_sage
    {
        get { return c_Listelien_sage; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_jou_a_code.ToString());
		l_Valeurs.Add(c_jou_a_lib.ToString());
		l_Valeurs.Add(c_jou_a_cmt.ToString());
		l_Valeurs.Add(c_jou_a_chemin_icone.ToString());
		l_Valeurs.Add(c_jou_a_chemin_icone_ventil.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("jou_a_code");
		l_Noms.Add("jou_a_lib");
		l_Noms.Add("jou_a_cmt");
		l_Noms.Add("jou_a_chemin_icone");
		l_Noms.Add("jou_a_chemin_icone_ventil");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
