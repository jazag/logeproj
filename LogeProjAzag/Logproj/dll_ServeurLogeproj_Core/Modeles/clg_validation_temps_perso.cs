namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : validation_temps_perso </summary>
public partial class clg_validation_temps_perso : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLvalidation_temps_perso;
    protected clg_Modele c_Modele;

	private Int64 c_v_tps_cn;
	private DateTime c_v_tps_d_date;
	private clg_personnels c_personnels;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_personnels = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_validation_temps_perso(clg_Modele pModele, Int64 pv_tps_cn, bool pAMAJ = false) : base(pModele, pv_tps_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_validation_temps_perso(clg_Modele pModele, Int64 pv_tps_cn, DateTime pv_tps_d_date, clg_personnels ppersonnels, bool pAMAJ = true) : base(pModele, pv_tps_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pv_tps_cn != Int64.MinValue)
            c_v_tps_cn = pv_tps_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pv_tps_d_date != DateTime.MinValue)
            c_v_tps_d_date = pv_tps_d_date;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(ppersonnels != null)
            c_personnels = ppersonnels;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pv_tps_cn, DateTime pv_tps_d_date, Int64 ppersonnels)
    {   
		        c_v_tps_cn = pv_tps_cn;
		        c_v_tps_d_date = pv_tps_d_date;
		c_dicReferences.Add("personnels", ppersonnels);

        base.Initialise(pv_tps_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLvalidation_temps_perso; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listevalidation_temps_perso.Dictionnaire.Add(v_tps_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listevalidation_temps_perso.Dictionnaire.Remove(v_tps_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listevalidation_temps_perso.Contains(this)) c_personnels.Listevalidation_temps_perso.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_validation_temps_perso l_Clone = (clg_validation_temps_perso) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listevalidation_temps_perso.Contains(this)) l_Clone.personnels.Listevalidation_temps_perso.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_validation_temps_perso(null, v_tps_cn, v_tps_d_date, personnels,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_validation_temps_perso l_clone = (clg_validation_temps_perso) this.Clone;
		c_v_tps_cn = l_clone.v_tps_cn;
		c_v_tps_d_date = l_clone.v_tps_d_date;
		c_personnels = l_clone.personnels;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLvalidation_temps_perso.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLvalidation_temps_perso.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLvalidation_temps_perso.Delete(this);
    }

    /* Accesseur de la propriete v_tps_cn (v_tps_cn)
    * @return c_v_tps_cn */
    public Int64 v_tps_cn
    {
        get{return c_v_tps_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_v_tps_cn != value)
                {
                    CreerClone();
                    c_v_tps_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete v_tps_d_date (v_tps_d_date)
    * @return c_v_tps_d_date */
    public DateTime v_tps_d_date
    {
        get{return c_v_tps_d_date;}
        set
        {
            if(value != null)
            {
                if(c_v_tps_d_date != value)
                {
                    CreerClone();
                    c_v_tps_d_date = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (v_tps_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(value != null)
            {
                if(c_personnels != value)
                {
                    CreerClone();
                    c_personnels = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_v_tps_cn.ToString());
		l_Valeurs.Add(c_v_tps_d_date.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("v_tps_cn");
		l_Noms.Add("v_tps_d_date");
		l_Noms.Add("personnels");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
