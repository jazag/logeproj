namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : convention_regle </summary>
public partial class clg_convention_regle : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLconvention_regle;
    protected clg_Modele c_Modele;

	private Int64 c_cvr_cn;
	private clg_convention c_convention;
	private clg_regle c_regle;
	private string c_cvr_n_valeur;
	private bool c_cvr_b_bloquant;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_convention = null;
		this.c_regle = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_convention_regle(clg_Modele pModele, Int64 pcvr_cn, bool pAMAJ = false) : base(pModele, pcvr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_convention_regle(clg_Modele pModele, Int64 pcvr_cn, clg_convention pconvention, clg_regle pregle, string pcvr_n_valeur, bool pcvr_b_bloquant, bool pAMAJ = true) : base(pModele, pcvr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcvr_cn != Int64.MinValue)
            c_cvr_cn = pcvr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pconvention != null)
            c_convention = pconvention;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_regle = pregle;
		        c_cvr_n_valeur = pcvr_n_valeur;
		        c_cvr_b_bloquant = pcvr_b_bloquant;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcvr_cn, Int64 pconvention, Int64 pregle, string pcvr_n_valeur, bool pcvr_b_bloquant)
    {   
		        c_cvr_cn = pcvr_cn;
		c_dicReferences.Add("convention", pconvention);
		c_dicReferences.Add("regle", pregle);
		        c_cvr_n_valeur = pcvr_n_valeur;
		        c_cvr_b_bloquant = pcvr_b_bloquant;

        base.Initialise(pcvr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLconvention_regle; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_convention = (clg_convention) c_ModeleBase.RenvoieObjet(c_dicReferences["convention"], "clg_convention");
		c_regle = (clg_regle) c_ModeleBase.RenvoieObjet(c_dicReferences["regle"], "clg_regle");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeconvention_regle.Dictionnaire.Add(cvr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeconvention_regle.Dictionnaire.Remove(cvr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_convention != null)if(!c_convention.Listeconvention_regle.Contains(this)) c_convention.Listeconvention_regle.Add(this);
		if(c_regle != null)if(!c_regle.Listeconvention_regle.Contains(this)) c_regle.Listeconvention_regle.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_convention_regle l_Clone = (clg_convention_regle) Clone;
		if(l_Clone.convention != null)if(l_Clone.convention.Listeconvention_regle.Contains(this)) l_Clone.convention.Listeconvention_regle.Remove(this);
		if(l_Clone.regle != null)if(l_Clone.regle.Listeconvention_regle.Contains(this)) l_Clone.regle.Listeconvention_regle.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_convention_regle(null, cvr_cn, convention, regle, cvr_n_valeur, cvr_b_bloquant,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_convention_regle l_clone = (clg_convention_regle) this.Clone;
		c_cvr_cn = l_clone.cvr_cn;
		c_convention = l_clone.convention;
		c_regle = l_clone.regle;
		c_cvr_n_valeur = l_clone.cvr_n_valeur;
		c_cvr_b_bloquant = l_clone.cvr_b_bloquant;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLconvention_regle.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLconvention_regle.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLconvention_regle.Delete(this);
    }

    /* Accesseur de la propriete cvr_cn (cvr_cn)
    * @return c_cvr_cn */
    public Int64 cvr_cn
    {
        get{return c_cvr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cvr_cn != value)
                {
                    CreerClone();
                    c_cvr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete convention (cvr_cvn_cn)
    * @return c_convention */
    public clg_convention convention
    {
        get{return c_convention;}
        set
        {
            if(value != null)
            {
                if(c_convention != value)
                {
                    CreerClone();
                    c_convention = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete regle (cvr_reg_cn)
    * @return c_regle */
    public clg_regle regle
    {
        get{return c_regle;}
        set
        {
            if(c_regle != value)
            {
                CreerClone();
                c_regle = value;
            }
        }
    }
    /* Accesseur de la propriete cvr_n_valeur (cvr_n_valeur)
    * @return c_cvr_n_valeur */
    public string cvr_n_valeur
    {
        get{return c_cvr_n_valeur;}
        set
        {
            if(c_cvr_n_valeur != value)
            {
                CreerClone();
                c_cvr_n_valeur = value;
            }
        }
    }
    /* Accesseur de la propriete cvr_b_bloquant (cvr_b_bloquant)
    * @return c_cvr_b_bloquant */
    public bool cvr_b_bloquant
    {
        get{return c_cvr_b_bloquant;}
        set
        {
            if(c_cvr_b_bloquant != value)
            {
                CreerClone();
                c_cvr_b_bloquant = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cvr_cn.ToString());
		l_Valeurs.Add(c_convention==null ? "-1" : c_convention.ID.ToString());
		l_Valeurs.Add(c_regle==null ? "-1" : c_regle.ID.ToString());
		l_Valeurs.Add(c_cvr_n_valeur.ToString());
		l_Valeurs.Add(c_cvr_b_bloquant.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cvr_cn");
		l_Noms.Add("convention");
		l_Noms.Add("regle");
		l_Noms.Add("cvr_n_valeur");
		l_Noms.Add("cvr_b_bloquant");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
