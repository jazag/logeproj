namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : utilisateur </summary>
public partial class clg_utilisateur : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLutilisateur;
    protected clg_Modele c_Modele;

	private Int64 c_uti_cn;
	private string c_uti_a_nom_login;
	private clg_personnels c_personnels;
	private string c_uti_a_mp;
	private List<clg_etats> c_Listeetats;
	private List<clg_init_appli> c_Listeinit_appli;
	private List<clg_groupes> c_Listegroupes;


    private void Init()
    {
		c_Listeetats = new List<clg_etats>();
		c_Listeinit_appli = new List<clg_init_appli>();
		c_Listegroupes = new List<clg_groupes>();

    }

    public override void Detruit()
    {
		c_Listeetats.Clear();
		c_Listeinit_appli.Clear();
		c_Listegroupes.Clear();
		this.c_personnels = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_utilisateur(clg_Modele pModele, Int64 puti_cn, bool pAMAJ = false) : base(pModele, puti_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_utilisateur(clg_Modele pModele, Int64 puti_cn, string puti_a_nom_login, clg_personnels ppersonnels, string puti_a_mp, bool pAMAJ = true) : base(pModele, puti_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(puti_cn != Int64.MinValue)
            c_uti_cn = puti_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(puti_a_nom_login != null)
            c_uti_a_nom_login = puti_a_nom_login;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_personnels = ppersonnels;
		        c_uti_a_mp = puti_a_mp;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 puti_cn, string puti_a_nom_login, Int64 ppersonnels, string puti_a_mp)
    {   
		        c_uti_cn = puti_cn;
		        c_uti_a_nom_login = puti_a_nom_login;
		c_dicReferences.Add("personnels", ppersonnels);
		        c_uti_a_mp = puti_a_mp;

        base.Initialise(puti_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLutilisateur; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeutilisateur.Dictionnaire.Add(uti_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeutilisateur.Dictionnaire.Remove(uti_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listeutilisateur.Contains(this)) c_personnels.Listeutilisateur.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_utilisateur l_Clone = (clg_utilisateur) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listeutilisateur.Contains(this)) l_Clone.personnels.Listeutilisateur.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeetats.Count > 0) c_EstReference = true;
		if(c_Listeinit_appli.Count > 0) c_EstReference = true;
		if(c_Listegroupes.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_utilisateur(null, uti_cn, uti_a_nom_login, personnels, uti_a_mp,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_utilisateur l_clone = (clg_utilisateur) this.Clone;
		c_uti_cn = l_clone.uti_cn;
		c_uti_a_nom_login = l_clone.uti_a_nom_login;
		c_personnels = l_clone.personnels;
		c_uti_a_mp = l_clone.uti_a_mp;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLutilisateur.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLutilisateur.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLutilisateur.Delete(this);
    }

    /* Accesseur de la propriete uti_cn (uti_cn)
    * @return c_uti_cn */
    public Int64 uti_cn
    {
        get{return c_uti_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_uti_cn != value)
                {
                    CreerClone();
                    c_uti_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete uti_a_nom_login (uti_a_nom_login)
    * @return c_uti_a_nom_login */
    public string uti_a_nom_login
    {
        get{return c_uti_a_nom_login;}
        set
        {
            if(value != null)
            {
                if(c_uti_a_nom_login != value)
                {
                    CreerClone();
                    c_uti_a_nom_login = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (uti_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete uti_a_mp (uti_a_mp)
    * @return c_uti_a_mp */
    public string uti_a_mp
    {
        get{return c_uti_a_mp;}
        set
        {
            if(c_uti_a_mp != value)
            {
                CreerClone();
                c_uti_a_mp = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type etats */
    public List<clg_etats> Listeetats
    {
        get { return c_Listeetats; }
    }    /* Accesseur de la liste des objets de type init_appli */
    public List<clg_init_appli> Listeinit_appli
    {
        get { return c_Listeinit_appli; }
    }    /* Accesseur de la liste des objets de type groupes */
    public List<clg_groupes> Listegroupes
    {
        get { return c_Listegroupes; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_uti_cn.ToString());
		l_Valeurs.Add(c_uti_a_nom_login.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_uti_a_mp.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("uti_cn");
		l_Noms.Add("uti_a_nom_login");
		l_Noms.Add("personnels");
		l_Noms.Add("uti_a_mp");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
