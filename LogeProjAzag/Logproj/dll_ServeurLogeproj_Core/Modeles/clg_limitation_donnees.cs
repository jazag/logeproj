namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : limitation_donnees </summary>
public partial class clg_limitation_donnees : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLlimitation_donnees;
    protected clg_Modele c_Modele;

	private Int64 c_lmd_cn;
	private clg_groupes c_groupes;
	private clg_t_limitation_donnees c_t_limitation_donnees;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_groupes = null;
		this.c_t_limitation_donnees = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_limitation_donnees(clg_Modele pModele, Int64 plmd_cn, bool pAMAJ = false) : base(pModele, plmd_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_limitation_donnees(clg_Modele pModele, Int64 plmd_cn, clg_groupes pgroupes, clg_t_limitation_donnees pt_limitation_donnees, bool pAMAJ = true) : base(pModele, plmd_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(plmd_cn != Int64.MinValue)
            c_lmd_cn = plmd_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pgroupes != null)
            c_groupes = pgroupes;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pt_limitation_donnees != null)
            c_t_limitation_donnees = pt_limitation_donnees;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 plmd_cn, Int64 pgroupes, Int64 pt_limitation_donnees)
    {   
		        c_lmd_cn = plmd_cn;
		c_dicReferences.Add("groupes", pgroupes);
		c_dicReferences.Add("t_limitation_donnees", pt_limitation_donnees);

        base.Initialise(plmd_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLlimitation_donnees; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_groupes = (clg_groupes) c_ModeleBase.RenvoieObjet(c_dicReferences["groupes"], "clg_groupes");
		c_t_limitation_donnees = (clg_t_limitation_donnees) c_ModeleBase.RenvoieObjet(c_dicReferences["t_limitation_donnees"], "clg_t_limitation_donnees");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listelimitation_donnees.Dictionnaire.Add(lmd_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listelimitation_donnees.Dictionnaire.Remove(lmd_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_groupes != null)if(!c_groupes.Listelimitation_donnees.Contains(this)) c_groupes.Listelimitation_donnees.Add(this);
		if(c_t_limitation_donnees != null)if(!c_t_limitation_donnees.Listelimitation_donnees.Contains(this)) c_t_limitation_donnees.Listelimitation_donnees.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_limitation_donnees l_Clone = (clg_limitation_donnees) Clone;
		if(l_Clone.groupes != null)if(l_Clone.groupes.Listelimitation_donnees.Contains(this)) l_Clone.groupes.Listelimitation_donnees.Remove(this);
		if(l_Clone.t_limitation_donnees != null)if(l_Clone.t_limitation_donnees.Listelimitation_donnees.Contains(this)) l_Clone.t_limitation_donnees.Listelimitation_donnees.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_limitation_donnees(null, lmd_cn, groupes, t_limitation_donnees,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_limitation_donnees l_clone = (clg_limitation_donnees) this.Clone;
		c_lmd_cn = l_clone.lmd_cn;
		c_groupes = l_clone.groupes;
		c_t_limitation_donnees = l_clone.t_limitation_donnees;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLlimitation_donnees.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLlimitation_donnees.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLlimitation_donnees.Delete(this);
    }

    /* Accesseur de la propriete lmd_cn (lmd_cn)
    * @return c_lmd_cn */
    public Int64 lmd_cn
    {
        get{return c_lmd_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_lmd_cn != value)
                {
                    CreerClone();
                    c_lmd_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete groupes (lmd_grp_cn)
    * @return c_groupes */
    public clg_groupes groupes
    {
        get{return c_groupes;}
        set
        {
            if(value != null)
            {
                if(c_groupes != value)
                {
                    CreerClone();
                    c_groupes = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_limitation_donnees (lmd_t_lmd_cn)
    * @return c_t_limitation_donnees */
    public clg_t_limitation_donnees t_limitation_donnees
    {
        get{return c_t_limitation_donnees;}
        set
        {
            if(value != null)
            {
                if(c_t_limitation_donnees != value)
                {
                    CreerClone();
                    c_t_limitation_donnees = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_lmd_cn.ToString());
		l_Valeurs.Add(c_groupes==null ? "-1" : c_groupes.ID.ToString());
		l_Valeurs.Add(c_t_limitation_donnees==null ? "-1" : c_t_limitation_donnees.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("lmd_cn");
		l_Noms.Add("groupes");
		l_Noms.Add("t_limitation_donnees");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
