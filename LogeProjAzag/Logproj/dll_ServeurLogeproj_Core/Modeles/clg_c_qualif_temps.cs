namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : c_qualif_temps </summary>
public partial class clg_c_qualif_temps : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLc_qualif_temps;
    protected clg_Modele c_Modele;

	private Int64 c_c_qlt_cn;
	private string c_c_qlt_a_libelle;
	private List<clg_temps_realises> c_Listetemps_realises;


    private void Init()
    {
		c_Listetemps_realises = new List<clg_temps_realises>();

    }

    public override void Detruit()
    {
		c_Listetemps_realises.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_c_qualif_temps(clg_Modele pModele, Int64 pc_qlt_cn, bool pAMAJ = false) : base(pModele, pc_qlt_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_c_qualif_temps(clg_Modele pModele, Int64 pc_qlt_cn, string pc_qlt_a_libelle, bool pAMAJ = true) : base(pModele, pc_qlt_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pc_qlt_cn != Int64.MinValue)
            c_c_qlt_cn = pc_qlt_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_c_qlt_a_libelle = pc_qlt_a_libelle;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pc_qlt_cn, string pc_qlt_a_libelle)
    {   
		        c_c_qlt_cn = pc_qlt_cn;
		        c_c_qlt_a_libelle = pc_qlt_a_libelle;

        base.Initialise(pc_qlt_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLc_qualif_temps; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listec_qualif_temps.Dictionnaire.Add(c_qlt_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listec_qualif_temps.Dictionnaire.Remove(c_qlt_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_c_qualif_temps l_Clone = (clg_c_qualif_temps) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listetemps_realises.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_c_qualif_temps(null, c_qlt_cn, c_qlt_a_libelle,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_c_qualif_temps l_clone = (clg_c_qualif_temps) this.Clone;
		c_c_qlt_cn = l_clone.c_qlt_cn;
		c_c_qlt_a_libelle = l_clone.c_qlt_a_libelle;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLc_qualif_temps.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLc_qualif_temps.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLc_qualif_temps.Delete(this);
    }

    /* Accesseur de la propriete c_qlt_cn (c_qlt_cn)
    * @return c_c_qlt_cn */
    public Int64 c_qlt_cn
    {
        get{return c_c_qlt_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_c_qlt_cn != value)
                {
                    CreerClone();
                    c_c_qlt_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete c_qlt_a_libelle (c_qlt_a_libelle)
    * @return c_c_qlt_a_libelle */
    public string c_qlt_a_libelle
    {
        get{return c_c_qlt_a_libelle;}
        set
        {
            if(c_c_qlt_a_libelle != value)
            {
                CreerClone();
                c_c_qlt_a_libelle = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type temps_realises */
    public List<clg_temps_realises> Listetemps_realises
    {
        get { return c_Listetemps_realises; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_c_qlt_cn.ToString());
		l_Valeurs.Add(c_c_qlt_a_libelle.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("c_qlt_cn");
		l_Noms.Add("c_qlt_a_libelle");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
