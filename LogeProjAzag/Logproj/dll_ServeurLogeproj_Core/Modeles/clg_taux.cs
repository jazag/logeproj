namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : taux </summary>
public partial class clg_taux : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtaux;
    protected clg_Modele c_Modele;

	private Int64 c_tx_cn;
	private double c_tx_n_taux_prev;
	private double c_tx_n_taux_real;
	private Int64 c_tx_n_cout_revient;
	private string c_tx_a_lib;
	private clg_elements c_elements;
	private clg_t_fonctions c_t_fonctions;
	private clg_annee_comptable c_annee_comptable;
	private DateTime c_tx_d_debut;
	private DateTime c_tx_d_fin;
	private clg_personnels c_personnels;
	private string c_tx_com;
	private Int64 c_tx_fin_cn;
	private clg_t_cout_projet c_t_cout_projet;
	private bool c_tx_b_archive;
	private List<clg_taux_fin> c_Listetaux_fin;


    private void Init()
    {
		c_Listetaux_fin = new List<clg_taux_fin>();

    }

    public override void Detruit()
    {
		c_Listetaux_fin.Clear();
		this.c_elements = null;
		this.c_t_fonctions = null;
		this.c_annee_comptable = null;
		this.c_personnels = null;
		this.c_t_cout_projet = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_taux(clg_Modele pModele, Int64 ptx_cn, bool pAMAJ = false) : base(pModele, ptx_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_taux(clg_Modele pModele, Int64 ptx_cn, double ptx_n_taux_prev, double ptx_n_taux_real, Int64 ptx_n_cout_revient, string ptx_a_lib, clg_elements pelements, clg_t_fonctions pt_fonctions, clg_annee_comptable pannee_comptable, DateTime ptx_d_debut, DateTime ptx_d_fin, clg_personnels ppersonnels, string ptx_com, Int64 ptx_fin_cn, clg_t_cout_projet pt_cout_projet, bool ptx_b_archive, bool pAMAJ = true) : base(pModele, ptx_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptx_cn != Int64.MinValue)
            c_tx_cn = ptx_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_tx_n_taux_prev = ptx_n_taux_prev;
		        c_tx_n_taux_real = ptx_n_taux_real;
		        c_tx_n_cout_revient = ptx_n_cout_revient;
		        c_tx_a_lib = ptx_a_lib;
		        c_elements = pelements;
		        c_t_fonctions = pt_fonctions;
		        c_annee_comptable = pannee_comptable;
		        c_tx_d_debut = ptx_d_debut;
		        c_tx_d_fin = ptx_d_fin;
		        c_personnels = ppersonnels;
		        c_tx_com = ptx_com;
		        c_tx_fin_cn = ptx_fin_cn;
		        c_t_cout_projet = pt_cout_projet;
		        c_tx_b_archive = ptx_b_archive;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptx_cn, double ptx_n_taux_prev, double ptx_n_taux_real, Int64 ptx_n_cout_revient, string ptx_a_lib, Int64 pelements, Int64 pt_fonctions, Int64 pannee_comptable, DateTime ptx_d_debut, DateTime ptx_d_fin, Int64 ppersonnels, string ptx_com, Int64 ptx_fin_cn, Int64 pt_cout_projet, bool ptx_b_archive)
    {   
		        c_tx_cn = ptx_cn;
		        c_tx_n_taux_prev = ptx_n_taux_prev;
		        c_tx_n_taux_real = ptx_n_taux_real;
		        c_tx_n_cout_revient = ptx_n_cout_revient;
		        c_tx_a_lib = ptx_a_lib;
		c_dicReferences.Add("elements", pelements);
		c_dicReferences.Add("t_fonctions", pt_fonctions);
		c_dicReferences.Add("annee_comptable", pannee_comptable);
		        c_tx_d_debut = ptx_d_debut;
		        c_tx_d_fin = ptx_d_fin;
		c_dicReferences.Add("personnels", ppersonnels);
		        c_tx_com = ptx_com;
		        c_tx_fin_cn = ptx_fin_cn;
		c_dicReferences.Add("t_cout_projet", pt_cout_projet);
		        c_tx_b_archive = ptx_b_archive;

        base.Initialise(ptx_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtaux; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_elements = (clg_elements) c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");
		c_t_fonctions = (clg_t_fonctions) c_ModeleBase.RenvoieObjet(c_dicReferences["t_fonctions"], "clg_t_fonctions");
		c_annee_comptable = (clg_annee_comptable) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_comptable"], "clg_annee_comptable");
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_t_cout_projet = (clg_t_cout_projet) c_ModeleBase.RenvoieObjet(c_dicReferences["t_cout_projet"], "clg_t_cout_projet");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetaux.Dictionnaire.Add(tx_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetaux.Dictionnaire.Remove(tx_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_elements != null)if(!c_elements.Listetaux.Contains(this)) c_elements.Listetaux.Add(this);
		if(c_t_fonctions != null)if(!c_t_fonctions.Listetaux.Contains(this)) c_t_fonctions.Listetaux.Add(this);
		if(c_annee_comptable != null)if(!c_annee_comptable.Listetaux.Contains(this)) c_annee_comptable.Listetaux.Add(this);
		if(c_personnels != null)if(!c_personnels.Listetaux.Contains(this)) c_personnels.Listetaux.Add(this);
		if(c_t_cout_projet != null)if(!c_t_cout_projet.Listetaux.Contains(this)) c_t_cout_projet.Listetaux.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_taux l_Clone = (clg_taux) Clone;
		if(l_Clone.elements != null)if(l_Clone.elements.Listetaux.Contains(this)) l_Clone.elements.Listetaux.Remove(this);
		if(l_Clone.t_fonctions != null)if(l_Clone.t_fonctions.Listetaux.Contains(this)) l_Clone.t_fonctions.Listetaux.Remove(this);
		if(l_Clone.annee_comptable != null)if(l_Clone.annee_comptable.Listetaux.Contains(this)) l_Clone.annee_comptable.Listetaux.Remove(this);
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listetaux.Contains(this)) l_Clone.personnels.Listetaux.Remove(this);
		if(l_Clone.t_cout_projet != null)if(l_Clone.t_cout_projet.Listetaux.Contains(this)) l_Clone.t_cout_projet.Listetaux.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listetaux_fin.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_taux(null, tx_cn, tx_n_taux_prev, tx_n_taux_real, tx_n_cout_revient, tx_a_lib, elements, t_fonctions, annee_comptable, tx_d_debut, tx_d_fin, personnels, tx_com, tx_fin_cn, t_cout_projet, tx_b_archive,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_taux l_clone = (clg_taux) this.Clone;
		c_tx_cn = l_clone.tx_cn;
		c_tx_n_taux_prev = l_clone.tx_n_taux_prev;
		c_tx_n_taux_real = l_clone.tx_n_taux_real;
		c_tx_n_cout_revient = l_clone.tx_n_cout_revient;
		c_tx_a_lib = l_clone.tx_a_lib;
		c_elements = l_clone.elements;
		c_t_fonctions = l_clone.t_fonctions;
		c_annee_comptable = l_clone.annee_comptable;
		c_tx_d_debut = l_clone.tx_d_debut;
		c_tx_d_fin = l_clone.tx_d_fin;
		c_personnels = l_clone.personnels;
		c_tx_com = l_clone.tx_com;
		c_tx_fin_cn = l_clone.tx_fin_cn;
		c_t_cout_projet = l_clone.t_cout_projet;
		c_tx_b_archive = l_clone.tx_b_archive;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtaux.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtaux.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtaux.Delete(this);
    }

    /* Accesseur de la propriete tx_cn (tx_cn)
    * @return c_tx_cn */
    public Int64 tx_cn
    {
        get{return c_tx_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tx_cn != value)
                {
                    CreerClone();
                    c_tx_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete tx_n_taux_prev (tx_n_taux_prev)
    * @return c_tx_n_taux_prev */
    public double tx_n_taux_prev
    {
        get{return c_tx_n_taux_prev;}
        set
        {
            if(c_tx_n_taux_prev != value)
            {
                CreerClone();
                c_tx_n_taux_prev = value;
            }
        }
    }
    /* Accesseur de la propriete tx_n_taux_real (tx_n_taux_real)
    * @return c_tx_n_taux_real */
    public double tx_n_taux_real
    {
        get{return c_tx_n_taux_real;}
        set
        {
            if(c_tx_n_taux_real != value)
            {
                CreerClone();
                c_tx_n_taux_real = value;
            }
        }
    }
    /* Accesseur de la propriete tx_n_cout_revient (tx_n_cout_revient)
    * @return c_tx_n_cout_revient */
    public Int64 tx_n_cout_revient
    {
        get{return c_tx_n_cout_revient;}
        set
        {
            if(c_tx_n_cout_revient != value)
            {
                CreerClone();
                c_tx_n_cout_revient = value;
            }
        }
    }
    /* Accesseur de la propriete tx_a_lib (tx_a_lib)
    * @return c_tx_a_lib */
    public string tx_a_lib
    {
        get{return c_tx_a_lib;}
        set
        {
            if(c_tx_a_lib != value)
            {
                CreerClone();
                c_tx_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete elements (tx_ele_cn)
    * @return c_elements */
    public clg_elements elements
    {
        get{return c_elements;}
        set
        {
            if(c_elements != value)
            {
                CreerClone();
                c_elements = value;
            }
        }
    }
    /* Accesseur de la propriete t_fonctions (tx_t_fct_cn)
    * @return c_t_fonctions */
    public clg_t_fonctions t_fonctions
    {
        get{return c_t_fonctions;}
        set
        {
            if(c_t_fonctions != value)
            {
                CreerClone();
                c_t_fonctions = value;
            }
        }
    }
    /* Accesseur de la propriete annee_comptable (tx_acp_cn)
    * @return c_annee_comptable */
    public clg_annee_comptable annee_comptable
    {
        get{return c_annee_comptable;}
        set
        {
            if(c_annee_comptable != value)
            {
                CreerClone();
                c_annee_comptable = value;
            }
        }
    }
    /* Accesseur de la propriete tx_d_debut (tx_d_debut)
    * @return c_tx_d_debut */
    public DateTime tx_d_debut
    {
        get{return c_tx_d_debut;}
        set
        {
            if(c_tx_d_debut != value)
            {
                CreerClone();
                c_tx_d_debut = value;
            }
        }
    }
    /* Accesseur de la propriete tx_d_fin (tx_d_fin)
    * @return c_tx_d_fin */
    public DateTime tx_d_fin
    {
        get{return c_tx_d_fin;}
        set
        {
            if(c_tx_d_fin != value)
            {
                CreerClone();
                c_tx_d_fin = value;
            }
        }
    }
    /* Accesseur de la propriete personnels (tx_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete tx_com (tx_com)
    * @return c_tx_com */
    public string tx_com
    {
        get{return c_tx_com;}
        set
        {
            if(c_tx_com != value)
            {
                CreerClone();
                c_tx_com = value;
            }
        }
    }
    /* Accesseur de la propriete tx_fin_cn (tx_fin_cn)
    * @return c_tx_fin_cn */
    public Int64 tx_fin_cn
    {
        get{return c_tx_fin_cn;}
        set
        {
            if(c_tx_fin_cn != value)
            {
                CreerClone();
                c_tx_fin_cn = value;
            }
        }
    }
    /* Accesseur de la propriete t_cout_projet (tx_t_cout_projet_cn)
    * @return c_t_cout_projet */
    public clg_t_cout_projet t_cout_projet
    {
        get{return c_t_cout_projet;}
        set
        {
            if(c_t_cout_projet != value)
            {
                CreerClone();
                c_t_cout_projet = value;
            }
        }
    }
    /* Accesseur de la propriete tx_b_archive (tx_b_archive)
    * @return c_tx_b_archive */
    public bool tx_b_archive
    {
        get{return c_tx_b_archive;}
        set
        {
            if(c_tx_b_archive != value)
            {
                CreerClone();
                c_tx_b_archive = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type taux_fin */
    public List<clg_taux_fin> Listetaux_fin
    {
        get { return c_Listetaux_fin; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tx_cn.ToString());
		l_Valeurs.Add(c_tx_n_taux_prev.ToString());
		l_Valeurs.Add(c_tx_n_taux_real.ToString());
		l_Valeurs.Add(c_tx_n_cout_revient.ToString());
		l_Valeurs.Add(c_tx_a_lib.ToString());
		l_Valeurs.Add(c_elements==null ? "-1" : c_elements.ID.ToString());
		l_Valeurs.Add(c_t_fonctions==null ? "-1" : c_t_fonctions.ID.ToString());
		l_Valeurs.Add(c_annee_comptable==null ? "-1" : c_annee_comptable.ID.ToString());
		l_Valeurs.Add(c_tx_d_debut.ToString());
		l_Valeurs.Add(c_tx_d_fin.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_tx_com.ToString());
		l_Valeurs.Add(c_tx_fin_cn.ToString());
		l_Valeurs.Add(c_t_cout_projet==null ? "-1" : c_t_cout_projet.ID.ToString());
		l_Valeurs.Add(c_tx_b_archive.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tx_cn");
		l_Noms.Add("tx_n_taux_prev");
		l_Noms.Add("tx_n_taux_real");
		l_Noms.Add("tx_n_cout_revient");
		l_Noms.Add("tx_a_lib");
		l_Noms.Add("elements");
		l_Noms.Add("t_fonctions");
		l_Noms.Add("annee_comptable");
		l_Noms.Add("tx_d_debut");
		l_Noms.Add("tx_d_fin");
		l_Noms.Add("personnels");
		l_Noms.Add("tx_com");
		l_Noms.Add("tx_fin_cn");
		l_Noms.Add("t_cout_projet");
		l_Noms.Add("tx_b_archive");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
