namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : solde_conges </summary>
public partial class clg_solde_conges : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLsolde_conges;
    protected clg_Modele c_Modele;

	private Int64 c_scg_cn;
	private clg_personnels c_personnels;
	private clg_annee_civile c_annee_civile;
	private double c_scg_n_solde;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_personnels = null;
		this.c_annee_civile = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_solde_conges(clg_Modele pModele, Int64 pscg_cn, bool pAMAJ = false) : base(pModele, pscg_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_solde_conges(clg_Modele pModele, Int64 pscg_cn, clg_personnels ppersonnels, clg_annee_civile pannee_civile, double pscg_n_solde, bool pAMAJ = true) : base(pModele, pscg_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pscg_cn != Int64.MinValue)
            c_scg_cn = pscg_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_personnels = ppersonnels;
		        c_annee_civile = pannee_civile;
		        c_scg_n_solde = pscg_n_solde;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pscg_cn, Int64 ppersonnels, Int64 pannee_civile, double pscg_n_solde)
    {   
		        c_scg_cn = pscg_cn;
		c_dicReferences.Add("personnels", ppersonnels);
		c_dicReferences.Add("annee_civile", pannee_civile);
		        c_scg_n_solde = pscg_n_solde;

        base.Initialise(pscg_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLsolde_conges; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_annee_civile = (clg_annee_civile) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_civile"], "clg_annee_civile");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listesolde_conges.Dictionnaire.Add(scg_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listesolde_conges.Dictionnaire.Remove(scg_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listesolde_conges.Contains(this)) c_personnels.Listesolde_conges.Add(this);
		if(c_annee_civile != null)if(!c_annee_civile.Listesolde_conges.Contains(this)) c_annee_civile.Listesolde_conges.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_solde_conges l_Clone = (clg_solde_conges) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listesolde_conges.Contains(this)) l_Clone.personnels.Listesolde_conges.Remove(this);
		if(l_Clone.annee_civile != null)if(l_Clone.annee_civile.Listesolde_conges.Contains(this)) l_Clone.annee_civile.Listesolde_conges.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_solde_conges(null, scg_cn, personnels, annee_civile, scg_n_solde,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_solde_conges l_clone = (clg_solde_conges) this.Clone;
		c_scg_cn = l_clone.scg_cn;
		c_personnels = l_clone.personnels;
		c_annee_civile = l_clone.annee_civile;
		c_scg_n_solde = l_clone.scg_n_solde;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLsolde_conges.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLsolde_conges.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLsolde_conges.Delete(this);
    }

    /* Accesseur de la propriete scg_cn (scg_cn)
    * @return c_scg_cn */
    public Int64 scg_cn
    {
        get{return c_scg_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_scg_cn != value)
                {
                    CreerClone();
                    c_scg_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (scg_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete annee_civile (scg_ann_cn)
    * @return c_annee_civile */
    public clg_annee_civile annee_civile
    {
        get{return c_annee_civile;}
        set
        {
            if(c_annee_civile != value)
            {
                CreerClone();
                c_annee_civile = value;
            }
        }
    }
    /* Accesseur de la propriete scg_n_solde (scg_n_solde)
    * @return c_scg_n_solde */
    public double scg_n_solde
    {
        get{return c_scg_n_solde;}
        set
        {
            if(c_scg_n_solde != value)
            {
                CreerClone();
                c_scg_n_solde = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_scg_cn.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_annee_civile==null ? "-1" : c_annee_civile.ID.ToString());
		l_Valeurs.Add(c_scg_n_solde.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("scg_cn");
		l_Noms.Add("personnels");
		l_Noms.Add("annee_civile");
		l_Noms.Add("scg_n_solde");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
