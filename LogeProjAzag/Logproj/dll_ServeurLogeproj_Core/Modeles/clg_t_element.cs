namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_element </summary>
public partial class clg_t_element : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_element;
    protected clg_Modele c_Modele;

	private Int64 c_t_ele_cn;
	private string c_t_ele_a_lib;
	private Int64 c_t_ele_niv_min;
	private Int64 c_t_ele_niv_max;
	private string c_t_ele_a_chemin_icone;
	private string c_t_ele_a_lib_msg;
	private string c_t_ele_a_chemin_icone_grise;
	private List<clg_elements> c_Listeelements;
	private List<clg_carac> c_Listecarac;


    private void Init()
    {
		c_Listeelements = new List<clg_elements>();
		c_Listecarac = new List<clg_carac>();

    }

    public override void Detruit()
    {
		c_Listeelements.Clear();
		c_Listecarac.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_element(clg_Modele pModele, Int64 pt_ele_cn, bool pAMAJ = false) : base(pModele, pt_ele_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_element(clg_Modele pModele, Int64 pt_ele_cn, string pt_ele_a_lib, Int64 pt_ele_niv_min, Int64 pt_ele_niv_max, string pt_ele_a_chemin_icone, string pt_ele_a_lib_msg, string pt_ele_a_chemin_icone_grise, bool pAMAJ = true) : base(pModele, pt_ele_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_ele_cn != Int64.MinValue)
            c_t_ele_cn = pt_ele_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_t_ele_a_lib = pt_ele_a_lib;
		        c_t_ele_niv_min = pt_ele_niv_min;
		        c_t_ele_niv_max = pt_ele_niv_max;
		        c_t_ele_a_chemin_icone = pt_ele_a_chemin_icone;
		        c_t_ele_a_lib_msg = pt_ele_a_lib_msg;
		        c_t_ele_a_chemin_icone_grise = pt_ele_a_chemin_icone_grise;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_ele_cn, string pt_ele_a_lib, Int64 pt_ele_niv_min, Int64 pt_ele_niv_max, string pt_ele_a_chemin_icone, string pt_ele_a_lib_msg, string pt_ele_a_chemin_icone_grise)
    {   
		        c_t_ele_cn = pt_ele_cn;
		        c_t_ele_a_lib = pt_ele_a_lib;
		        c_t_ele_niv_min = pt_ele_niv_min;
		        c_t_ele_niv_max = pt_ele_niv_max;
		        c_t_ele_a_chemin_icone = pt_ele_a_chemin_icone;
		        c_t_ele_a_lib_msg = pt_ele_a_lib_msg;
		        c_t_ele_a_chemin_icone_grise = pt_ele_a_chemin_icone_grise;

        base.Initialise(pt_ele_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_element; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_element.Dictionnaire.Add(t_ele_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_element.Dictionnaire.Remove(t_ele_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_element l_Clone = (clg_t_element) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeelements.Count > 0) c_EstReference = true;
		if(c_Listecarac.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_element(null, t_ele_cn, t_ele_a_lib, t_ele_niv_min, t_ele_niv_max, t_ele_a_chemin_icone, t_ele_a_lib_msg, t_ele_a_chemin_icone_grise,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_element l_clone = (clg_t_element) this.Clone;
		c_t_ele_cn = l_clone.t_ele_cn;
		c_t_ele_a_lib = l_clone.t_ele_a_lib;
		c_t_ele_niv_min = l_clone.t_ele_niv_min;
		c_t_ele_niv_max = l_clone.t_ele_niv_max;
		c_t_ele_a_chemin_icone = l_clone.t_ele_a_chemin_icone;
		c_t_ele_a_lib_msg = l_clone.t_ele_a_lib_msg;
		c_t_ele_a_chemin_icone_grise = l_clone.t_ele_a_chemin_icone_grise;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_element.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_element.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_element.Delete(this);
    }

    /* Accesseur de la propriete t_ele_cn (t_ele_cn)
    * @return c_t_ele_cn */
    public Int64 t_ele_cn
    {
        get{return c_t_ele_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_ele_cn != value)
                {
                    CreerClone();
                    c_t_ele_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_ele_a_lib (t_ele_a_lib)
    * @return c_t_ele_a_lib */
    public string t_ele_a_lib
    {
        get{return c_t_ele_a_lib;}
        set
        {
            if(c_t_ele_a_lib != value)
            {
                CreerClone();
                c_t_ele_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete t_ele_niv_min (t_ele_niv_min)
    * @return c_t_ele_niv_min */
    public Int64 t_ele_niv_min
    {
        get{return c_t_ele_niv_min;}
        set
        {
            if(c_t_ele_niv_min != value)
            {
                CreerClone();
                c_t_ele_niv_min = value;
            }
        }
    }
    /* Accesseur de la propriete t_ele_niv_max (t_ele_niv_max)
    * @return c_t_ele_niv_max */
    public Int64 t_ele_niv_max
    {
        get{return c_t_ele_niv_max;}
        set
        {
            if(c_t_ele_niv_max != value)
            {
                CreerClone();
                c_t_ele_niv_max = value;
            }
        }
    }
    /* Accesseur de la propriete t_ele_a_chemin_icone (t_ele_a_chemin_icone)
    * @return c_t_ele_a_chemin_icone */
    public string t_ele_a_chemin_icone
    {
        get{return c_t_ele_a_chemin_icone;}
        set
        {
            if(c_t_ele_a_chemin_icone != value)
            {
                CreerClone();
                c_t_ele_a_chemin_icone = value;
            }
        }
    }
    /* Accesseur de la propriete t_ele_a_lib_msg (t_ele_a_lib_msg)
    * @return c_t_ele_a_lib_msg */
    public string t_ele_a_lib_msg
    {
        get{return c_t_ele_a_lib_msg;}
        set
        {
            if(c_t_ele_a_lib_msg != value)
            {
                CreerClone();
                c_t_ele_a_lib_msg = value;
            }
        }
    }
    /* Accesseur de la propriete t_ele_a_chemin_icone_grise (t_ele_a_chemin_icone_grise)
    * @return c_t_ele_a_chemin_icone_grise */
    public string t_ele_a_chemin_icone_grise
    {
        get{return c_t_ele_a_chemin_icone_grise;}
        set
        {
            if(c_t_ele_a_chemin_icone_grise != value)
            {
                CreerClone();
                c_t_ele_a_chemin_icone_grise = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type elements */
    public List<clg_elements> Listeelements
    {
        get { return c_Listeelements; }
    }    /* Accesseur de la liste des objets de type carac */
    public List<clg_carac> Listecarac
    {
        get { return c_Listecarac; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_ele_cn.ToString());
		l_Valeurs.Add(c_t_ele_a_lib.ToString());
		l_Valeurs.Add(c_t_ele_niv_min.ToString());
		l_Valeurs.Add(c_t_ele_niv_max.ToString());
		l_Valeurs.Add(c_t_ele_a_chemin_icone.ToString());
		l_Valeurs.Add(c_t_ele_a_lib_msg.ToString());
		l_Valeurs.Add(c_t_ele_a_chemin_icone_grise.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_ele_cn");
		l_Noms.Add("t_ele_a_lib");
		l_Noms.Add("t_ele_niv_min");
		l_Noms.Add("t_ele_niv_max");
		l_Noms.Add("t_ele_a_chemin_icone");
		l_Noms.Add("t_ele_a_lib_msg");
		l_Noms.Add("t_ele_a_chemin_icone_grise");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
