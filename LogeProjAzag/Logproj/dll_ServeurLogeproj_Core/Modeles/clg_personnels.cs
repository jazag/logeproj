namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : personnels </summary>
public partial class clg_personnels : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLpersonnels;
    protected clg_Modele c_Modele;

	private Int64 c_per_cn;
	private string c_per_a_nom;
	private string c_per_a_prenom;
	private Int64 c_per_n_archive;
	private string c_per_a_code;
	private string c_per_a_comment;
	private DateTime c_per_d_arrivee;
	private DateTime c_per_d_depart;
	private string c_per_a_tel;
	private string c_per_a_fax;
	private string c_per_a_mail;
	private DateTime c_per_d_debut_saisie;
	private List<clg_conge_prevu> c_Listeconge_prevu;
	private List<clg_conge_realise> c_Listeconge_realise;
	private List<clg_forfait_personnel> c_Listeforfait_personnel;
	private List<clg_concepteurs> c_Listeconcepteurs;
	private List<clg_fonctions_perso> c_Listefonctions_perso;
	private List<clg_intervenants> c_Listeintervenants;
	private List<clg_projets> c_Listeprojets;
	private List<clg_taux> c_Listetaux;
	private List<clg_temps_intervenants> c_Listetemps_intervenants;
	private List<clg_temps_perso_generaux> c_Listetemps_perso_generaux;
	private List<clg_temps_travail_annuel> c_Listetemps_travail_annuel;
	private List<clg_utilisateur> c_Listeutilisateur;
	private List<clg_validation_temps_perso> c_Listevalidation_temps_perso;
	private List<clg_validation_temps_semaine> c_Listevalidation_temps_semaine;
	private List<clg_planning> c_Listeplanning;
	private List<clg_solde_conges> c_Listesolde_conges;


    private void Init()
    {
		c_Listeconge_prevu = new List<clg_conge_prevu>();
		c_Listeconge_realise = new List<clg_conge_realise>();
		c_Listeforfait_personnel = new List<clg_forfait_personnel>();
		c_Listeconcepteurs = new List<clg_concepteurs>();
		c_Listefonctions_perso = new List<clg_fonctions_perso>();
		c_Listeintervenants = new List<clg_intervenants>();
		c_Listeprojets = new List<clg_projets>();
		c_Listetaux = new List<clg_taux>();
		c_Listetemps_intervenants = new List<clg_temps_intervenants>();
		c_Listetemps_perso_generaux = new List<clg_temps_perso_generaux>();
		c_Listetemps_travail_annuel = new List<clg_temps_travail_annuel>();
		c_Listeutilisateur = new List<clg_utilisateur>();
		c_Listevalidation_temps_perso = new List<clg_validation_temps_perso>();
		c_Listevalidation_temps_semaine = new List<clg_validation_temps_semaine>();
		c_Listeplanning = new List<clg_planning>();
		c_Listesolde_conges = new List<clg_solde_conges>();

    }

    public override void Detruit()
    {
		c_Listeconge_prevu.Clear();
		c_Listeconge_realise.Clear();
		c_Listeforfait_personnel.Clear();
		c_Listeconcepteurs.Clear();
		c_Listefonctions_perso.Clear();
		c_Listeintervenants.Clear();
		c_Listeprojets.Clear();
		c_Listetaux.Clear();
		c_Listetemps_intervenants.Clear();
		c_Listetemps_perso_generaux.Clear();
		c_Listetemps_travail_annuel.Clear();
		c_Listeutilisateur.Clear();
		c_Listevalidation_temps_perso.Clear();
		c_Listevalidation_temps_semaine.Clear();
		c_Listeplanning.Clear();
		c_Listesolde_conges.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_personnels(clg_Modele pModele, Int64 pper_cn, bool pAMAJ = false) : base(pModele, pper_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_personnels(clg_Modele pModele, Int64 pper_cn, string pper_a_nom, string pper_a_prenom, Int64 pper_n_archive, string pper_a_code, string pper_a_comment, DateTime pper_d_arrivee, DateTime pper_d_depart, string pper_a_tel, string pper_a_fax, string pper_a_mail, DateTime pper_d_debut_saisie, bool pAMAJ = true) : base(pModele, pper_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pper_cn != Int64.MinValue)
            c_per_cn = pper_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pper_a_nom != null)
            c_per_a_nom = pper_a_nom;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_per_a_prenom = pper_a_prenom;
		        if(pper_n_archive != Int64.MinValue)
            c_per_n_archive = pper_n_archive;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_per_a_code = pper_a_code;
		        c_per_a_comment = pper_a_comment;
		        c_per_d_arrivee = pper_d_arrivee;
		        c_per_d_depart = pper_d_depart;
		        c_per_a_tel = pper_a_tel;
		        c_per_a_fax = pper_a_fax;
		        c_per_a_mail = pper_a_mail;
		        c_per_d_debut_saisie = pper_d_debut_saisie;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pper_cn, string pper_a_nom, string pper_a_prenom, Int64 pper_n_archive, string pper_a_code, string pper_a_comment, DateTime pper_d_arrivee, DateTime pper_d_depart, string pper_a_tel, string pper_a_fax, string pper_a_mail, DateTime pper_d_debut_saisie)
    {   
		        c_per_cn = pper_cn;
		        c_per_a_nom = pper_a_nom;
		        c_per_a_prenom = pper_a_prenom;
		        c_per_n_archive = pper_n_archive;
		        c_per_a_code = pper_a_code;
		        c_per_a_comment = pper_a_comment;
		        c_per_d_arrivee = pper_d_arrivee;
		        c_per_d_depart = pper_d_depart;
		        c_per_a_tel = pper_a_tel;
		        c_per_a_fax = pper_a_fax;
		        c_per_a_mail = pper_a_mail;
		        c_per_d_debut_saisie = pper_d_debut_saisie;

        base.Initialise(pper_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLpersonnels; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listepersonnels.Dictionnaire.Add(per_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listepersonnels.Dictionnaire.Remove(per_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_personnels l_Clone = (clg_personnels) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeconge_prevu.Count > 0) c_EstReference = true;
		if(c_Listeconge_realise.Count > 0) c_EstReference = true;
		if(c_Listeforfait_personnel.Count > 0) c_EstReference = true;
		if(c_Listeconcepteurs.Count > 0) c_EstReference = true;
		if(c_Listefonctions_perso.Count > 0) c_EstReference = true;
		if(c_Listeintervenants.Count > 0) c_EstReference = true;
		if(c_Listeprojets.Count > 0) c_EstReference = true;
		if(c_Listetaux.Count > 0) c_EstReference = true;
		if(c_Listetemps_intervenants.Count > 0) c_EstReference = true;
		if(c_Listetemps_perso_generaux.Count > 0) c_EstReference = true;
		if(c_Listetemps_travail_annuel.Count > 0) c_EstReference = true;
		if(c_Listeutilisateur.Count > 0) c_EstReference = true;
		if(c_Listevalidation_temps_perso.Count > 0) c_EstReference = true;
		if(c_Listevalidation_temps_semaine.Count > 0) c_EstReference = true;
		if(c_Listeplanning.Count > 0) c_EstReference = true;
		if(c_Listesolde_conges.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_personnels(null, per_cn, per_a_nom, per_a_prenom, per_n_archive, per_a_code, per_a_comment, per_d_arrivee, per_d_depart, per_a_tel, per_a_fax, per_a_mail, per_d_debut_saisie,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_personnels l_clone = (clg_personnels) this.Clone;
		c_per_cn = l_clone.per_cn;
		c_per_a_nom = l_clone.per_a_nom;
		c_per_a_prenom = l_clone.per_a_prenom;
		c_per_n_archive = l_clone.per_n_archive;
		c_per_a_code = l_clone.per_a_code;
		c_per_a_comment = l_clone.per_a_comment;
		c_per_d_arrivee = l_clone.per_d_arrivee;
		c_per_d_depart = l_clone.per_d_depart;
		c_per_a_tel = l_clone.per_a_tel;
		c_per_a_fax = l_clone.per_a_fax;
		c_per_a_mail = l_clone.per_a_mail;
		c_per_d_debut_saisie = l_clone.per_d_debut_saisie;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLpersonnels.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLpersonnels.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLpersonnels.Delete(this);
    }

    /* Accesseur de la propriete per_cn (per_cn)
    * @return c_per_cn */
    public Int64 per_cn
    {
        get{return c_per_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_per_cn != value)
                {
                    CreerClone();
                    c_per_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete per_a_nom (per_a_nom)
    * @return c_per_a_nom */
    public string per_a_nom
    {
        get{return c_per_a_nom;}
        set
        {
            if(value != null)
            {
                if(c_per_a_nom != value)
                {
                    CreerClone();
                    c_per_a_nom = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete per_a_prenom (per_a_prenom)
    * @return c_per_a_prenom */
    public string per_a_prenom
    {
        get{return c_per_a_prenom;}
        set
        {
            if(c_per_a_prenom != value)
            {
                CreerClone();
                c_per_a_prenom = value;
            }
        }
    }
    /* Accesseur de la propriete per_n_archive (per_n_archive)
    * @return c_per_n_archive */
    public Int64 per_n_archive
    {
        get{return c_per_n_archive;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_per_n_archive != value)
                {
                    CreerClone();
                    c_per_n_archive = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete per_a_code (per_a_code)
    * @return c_per_a_code */
    public string per_a_code
    {
        get{return c_per_a_code;}
        set
        {
            if(c_per_a_code != value)
            {
                CreerClone();
                c_per_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete per_a_comment (per_a_comment)
    * @return c_per_a_comment */
    public string per_a_comment
    {
        get{return c_per_a_comment;}
        set
        {
            if(c_per_a_comment != value)
            {
                CreerClone();
                c_per_a_comment = value;
            }
        }
    }
    /* Accesseur de la propriete per_d_arrivee (per_d_arrivee)
    * @return c_per_d_arrivee */
    public DateTime per_d_arrivee
    {
        get{return c_per_d_arrivee;}
        set
        {
            if(c_per_d_arrivee != value)
            {
                CreerClone();
                c_per_d_arrivee = value;
            }
        }
    }
    /* Accesseur de la propriete per_d_depart (per_d_depart)
    * @return c_per_d_depart */
    public DateTime per_d_depart
    {
        get{return c_per_d_depart;}
        set
        {
            if(c_per_d_depart != value)
            {
                CreerClone();
                c_per_d_depart = value;
            }
        }
    }
    /* Accesseur de la propriete per_a_tel (per_a_tel)
    * @return c_per_a_tel */
    public string per_a_tel
    {
        get{return c_per_a_tel;}
        set
        {
            if(c_per_a_tel != value)
            {
                CreerClone();
                c_per_a_tel = value;
            }
        }
    }
    /* Accesseur de la propriete per_a_fax (per_a_fax)
    * @return c_per_a_fax */
    public string per_a_fax
    {
        get{return c_per_a_fax;}
        set
        {
            if(c_per_a_fax != value)
            {
                CreerClone();
                c_per_a_fax = value;
            }
        }
    }
    /* Accesseur de la propriete per_a_mail (per_a_mail)
    * @return c_per_a_mail */
    public string per_a_mail
    {
        get{return c_per_a_mail;}
        set
        {
            if(c_per_a_mail != value)
            {
                CreerClone();
                c_per_a_mail = value;
            }
        }
    }
    /* Accesseur de la propriete per_d_debut_saisie (per_d_debut_saisie)
    * @return c_per_d_debut_saisie */
    public DateTime per_d_debut_saisie
    {
        get{return c_per_d_debut_saisie;}
        set
        {
            if(c_per_d_debut_saisie != value)
            {
                CreerClone();
                c_per_d_debut_saisie = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type conge_prevu */
    public List<clg_conge_prevu> Listeconge_prevu
    {
        get { return c_Listeconge_prevu; }
    }    /* Accesseur de la liste des objets de type conge_realise */
    public List<clg_conge_realise> Listeconge_realise
    {
        get { return c_Listeconge_realise; }
    }    /* Accesseur de la liste des objets de type forfait_personnel */
    public List<clg_forfait_personnel> Listeforfait_personnel
    {
        get { return c_Listeforfait_personnel; }
    }    /* Accesseur de la liste des objets de type concepteurs */
    public List<clg_concepteurs> Listeconcepteurs
    {
        get { return c_Listeconcepteurs; }
    }    /* Accesseur de la liste des objets de type fonctions_perso */
    public List<clg_fonctions_perso> Listefonctions_perso
    {
        get { return c_Listefonctions_perso; }
    }    /* Accesseur de la liste des objets de type intervenants */
    public List<clg_intervenants> Listeintervenants
    {
        get { return c_Listeintervenants; }
    }    /* Accesseur de la liste des objets de type projets */
    public List<clg_projets> Listeprojets
    {
        get { return c_Listeprojets; }
    }    /* Accesseur de la liste des objets de type taux */
    public List<clg_taux> Listetaux
    {
        get { return c_Listetaux; }
    }    /* Accesseur de la liste des objets de type temps_intervenants */
    public List<clg_temps_intervenants> Listetemps_intervenants
    {
        get { return c_Listetemps_intervenants; }
    }    /* Accesseur de la liste des objets de type temps_perso_generaux */
    public List<clg_temps_perso_generaux> Listetemps_perso_generaux
    {
        get { return c_Listetemps_perso_generaux; }
    }    /* Accesseur de la liste des objets de type temps_travail_annuel */
    public List<clg_temps_travail_annuel> Listetemps_travail_annuel
    {
        get { return c_Listetemps_travail_annuel; }
    }    /* Accesseur de la liste des objets de type utilisateur */
    public List<clg_utilisateur> Listeutilisateur
    {
        get { return c_Listeutilisateur; }
    }    /* Accesseur de la liste des objets de type validation_temps_perso */
    public List<clg_validation_temps_perso> Listevalidation_temps_perso
    {
        get { return c_Listevalidation_temps_perso; }
    }    /* Accesseur de la liste des objets de type validation_temps_semaine */
    public List<clg_validation_temps_semaine> Listevalidation_temps_semaine
    {
        get { return c_Listevalidation_temps_semaine; }
    }    /* Accesseur de la liste des objets de type planning */
    public List<clg_planning> Listeplanning
    {
        get { return c_Listeplanning; }
    }    /* Accesseur de la liste des objets de type solde_conges */
    public List<clg_solde_conges> Listesolde_conges
    {
        get { return c_Listesolde_conges; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_per_cn.ToString());
		l_Valeurs.Add(c_per_a_nom.ToString());
		l_Valeurs.Add(c_per_a_prenom.ToString());
		l_Valeurs.Add(c_per_n_archive.ToString());
		l_Valeurs.Add(c_per_a_code.ToString());
		l_Valeurs.Add(c_per_a_comment.ToString());
		l_Valeurs.Add(c_per_d_arrivee.ToString());
		l_Valeurs.Add(c_per_d_depart.ToString());
		l_Valeurs.Add(c_per_a_tel.ToString());
		l_Valeurs.Add(c_per_a_fax.ToString());
		l_Valeurs.Add(c_per_a_mail.ToString());
		l_Valeurs.Add(c_per_d_debut_saisie.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("per_cn");
		l_Noms.Add("per_a_nom");
		l_Noms.Add("per_a_prenom");
		l_Noms.Add("per_n_archive");
		l_Noms.Add("per_a_code");
		l_Noms.Add("per_a_comment");
		l_Noms.Add("per_d_arrivee");
		l_Noms.Add("per_d_depart");
		l_Noms.Add("per_a_tel");
		l_Noms.Add("per_a_fax");
		l_Noms.Add("per_a_mail");
		l_Noms.Add("per_d_debut_saisie");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
