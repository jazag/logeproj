namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : arretes </summary>
public partial class clg_arretes : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLarretes;
    protected clg_Modele c_Modele;

	private Int64 c_ara_cn;
	private string c_ara_a_lib;
	private DateTime c_ara_d_piece;
	private DateTime c_ara_d_limite;
	private clg_annee_comptable c_annee_comptable;
	private double c_ara_n_montant;
	private Int64 c_ara_n_ecno;
	private string c_ara_n_cpt;
	private clg_financeurs c_financeurs;
	private DateTime c_ara_d_solde;
	private DateTime c_ara_d_engagement;
	private Int64 c_ara_n_archive;
	private clg_etat_arrete c_etat_arrete;
	private string c_ara_a_numara;
	private Int64 c_ara_n_engage;
	private List<clg_comment> c_Listecomment;
	private List<clg_lien_arretes> c_Listelien_arretes;
	private List<clg_lien_sage> c_Listelien_sage;


    private void Init()
    {
		c_Listecomment = new List<clg_comment>();
		c_Listelien_arretes = new List<clg_lien_arretes>();
		c_Listelien_sage = new List<clg_lien_sage>();

    }

    public override void Detruit()
    {
		c_Listecomment.Clear();
		c_Listelien_arretes.Clear();
		c_Listelien_sage.Clear();
		this.c_annee_comptable = null;
		this.c_financeurs = null;
		this.c_etat_arrete = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_arretes(clg_Modele pModele, Int64 para_cn, bool pAMAJ = false) : base(pModele, para_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_arretes(clg_Modele pModele, Int64 para_cn, string para_a_lib, DateTime para_d_piece, DateTime para_d_limite, clg_annee_comptable pannee_comptable, double para_n_montant, Int64 para_n_ecno, string para_n_cpt, clg_financeurs pfinanceurs, DateTime para_d_solde, DateTime para_d_engagement, Int64 para_n_archive, clg_etat_arrete petat_arrete, string para_a_numara, Int64 para_n_engage, bool pAMAJ = true) : base(pModele, para_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(para_cn != Int64.MinValue)
            c_ara_cn = para_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(para_a_lib != null)
            c_ara_a_lib = para_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_ara_d_piece = para_d_piece;
		        c_ara_d_limite = para_d_limite;
		        c_annee_comptable = pannee_comptable;
		        c_ara_n_montant = para_n_montant;
		        c_ara_n_ecno = para_n_ecno;
		        c_ara_n_cpt = para_n_cpt;
		        if(pfinanceurs != null)
            c_financeurs = pfinanceurs;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_ara_d_solde = para_d_solde;
		        c_ara_d_engagement = para_d_engagement;
		        c_ara_n_archive = para_n_archive;
		        c_etat_arrete = petat_arrete;
		        c_ara_a_numara = para_a_numara;
		        c_ara_n_engage = para_n_engage;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 para_cn, string para_a_lib, DateTime para_d_piece, DateTime para_d_limite, Int64 pannee_comptable, double para_n_montant, Int64 para_n_ecno, string para_n_cpt, Int64 pfinanceurs, DateTime para_d_solde, DateTime para_d_engagement, Int64 para_n_archive, Int64 petat_arrete, string para_a_numara, Int64 para_n_engage)
    {   
		        c_ara_cn = para_cn;
		        c_ara_a_lib = para_a_lib;
		        c_ara_d_piece = para_d_piece;
		        c_ara_d_limite = para_d_limite;
		c_dicReferences.Add("annee_comptable", pannee_comptable);
		        c_ara_n_montant = para_n_montant;
		        c_ara_n_ecno = para_n_ecno;
		        c_ara_n_cpt = para_n_cpt;
		c_dicReferences.Add("financeurs", pfinanceurs);
		        c_ara_d_solde = para_d_solde;
		        c_ara_d_engagement = para_d_engagement;
		        c_ara_n_archive = para_n_archive;
		c_dicReferences.Add("etat_arrete", petat_arrete);
		        c_ara_a_numara = para_a_numara;
		        c_ara_n_engage = para_n_engage;

        base.Initialise(para_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLarretes; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_annee_comptable = (clg_annee_comptable) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_comptable"], "clg_annee_comptable");
		c_financeurs = (clg_financeurs) c_ModeleBase.RenvoieObjet(c_dicReferences["financeurs"], "clg_financeurs");
		c_etat_arrete = (clg_etat_arrete) c_ModeleBase.RenvoieObjet(c_dicReferences["etat_arrete"], "clg_etat_arrete");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listearretes.Dictionnaire.Add(ara_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listearretes.Dictionnaire.Remove(ara_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_annee_comptable != null)if(!c_annee_comptable.Listearretes.Contains(this)) c_annee_comptable.Listearretes.Add(this);
		if(c_financeurs != null)if(!c_financeurs.Listearretes.Contains(this)) c_financeurs.Listearretes.Add(this);
		if(c_etat_arrete != null)if(!c_etat_arrete.Listearretes.Contains(this)) c_etat_arrete.Listearretes.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_arretes l_Clone = (clg_arretes) Clone;
		if(l_Clone.annee_comptable != null)if(l_Clone.annee_comptable.Listearretes.Contains(this)) l_Clone.annee_comptable.Listearretes.Remove(this);
		if(l_Clone.financeurs != null)if(l_Clone.financeurs.Listearretes.Contains(this)) l_Clone.financeurs.Listearretes.Remove(this);
		if(l_Clone.etat_arrete != null)if(l_Clone.etat_arrete.Listearretes.Contains(this)) l_Clone.etat_arrete.Listearretes.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecomment.Count > 0) c_EstReference = true;
		if(c_Listelien_arretes.Count > 0) c_EstReference = true;
		if(c_Listelien_sage.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_arretes(null, ara_cn, ara_a_lib, ara_d_piece, ara_d_limite, annee_comptable, ara_n_montant, ara_n_ecno, ara_n_cpt, financeurs, ara_d_solde, ara_d_engagement, ara_n_archive, etat_arrete, ara_a_numara, ara_n_engage,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_arretes l_clone = (clg_arretes) this.Clone;
		c_ara_cn = l_clone.ara_cn;
		c_ara_a_lib = l_clone.ara_a_lib;
		c_ara_d_piece = l_clone.ara_d_piece;
		c_ara_d_limite = l_clone.ara_d_limite;
		c_annee_comptable = l_clone.annee_comptable;
		c_ara_n_montant = l_clone.ara_n_montant;
		c_ara_n_ecno = l_clone.ara_n_ecno;
		c_ara_n_cpt = l_clone.ara_n_cpt;
		c_financeurs = l_clone.financeurs;
		c_ara_d_solde = l_clone.ara_d_solde;
		c_ara_d_engagement = l_clone.ara_d_engagement;
		c_ara_n_archive = l_clone.ara_n_archive;
		c_etat_arrete = l_clone.etat_arrete;
		c_ara_a_numara = l_clone.ara_a_numara;
		c_ara_n_engage = l_clone.ara_n_engage;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLarretes.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLarretes.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLarretes.Delete(this);
    }

    /* Accesseur de la propriete ara_cn (ara_cn)
    * @return c_ara_cn */
    public Int64 ara_cn
    {
        get{return c_ara_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ara_cn != value)
                {
                    CreerClone();
                    c_ara_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ara_a_lib (ara_a_lib)
    * @return c_ara_a_lib */
    public string ara_a_lib
    {
        get{return c_ara_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_ara_a_lib != value)
                {
                    CreerClone();
                    c_ara_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ara_d_piece (ara_d_piece)
    * @return c_ara_d_piece */
    public DateTime ara_d_piece
    {
        get{return c_ara_d_piece;}
        set
        {
            if(c_ara_d_piece != value)
            {
                CreerClone();
                c_ara_d_piece = value;
            }
        }
    }
    /* Accesseur de la propriete ara_d_limite (ara_d_limite)
    * @return c_ara_d_limite */
    public DateTime ara_d_limite
    {
        get{return c_ara_d_limite;}
        set
        {
            if(c_ara_d_limite != value)
            {
                CreerClone();
                c_ara_d_limite = value;
            }
        }
    }
    /* Accesseur de la propriete annee_comptable (ara_acp_cn)
    * @return c_annee_comptable */
    public clg_annee_comptable annee_comptable
    {
        get{return c_annee_comptable;}
        set
        {
            if(c_annee_comptable != value)
            {
                CreerClone();
                c_annee_comptable = value;
            }
        }
    }
    /* Accesseur de la propriete ara_n_montant (ara_n_montant)
    * @return c_ara_n_montant */
    public double ara_n_montant
    {
        get{return c_ara_n_montant;}
        set
        {
            if(c_ara_n_montant != value)
            {
                CreerClone();
                c_ara_n_montant = value;
            }
        }
    }
    /* Accesseur de la propriete ara_n_ecno (ara_n_ecno)
    * @return c_ara_n_ecno */
    public Int64 ara_n_ecno
    {
        get{return c_ara_n_ecno;}
        set
        {
            if(c_ara_n_ecno != value)
            {
                CreerClone();
                c_ara_n_ecno = value;
            }
        }
    }
    /* Accesseur de la propriete ara_n_cpt (ara_n_cpt)
    * @return c_ara_n_cpt */
    public string ara_n_cpt
    {
        get{return c_ara_n_cpt;}
        set
        {
            if(c_ara_n_cpt != value)
            {
                CreerClone();
                c_ara_n_cpt = value;
            }
        }
    }
    /* Accesseur de la propriete financeurs (ara_fin_cn)
    * @return c_financeurs */
    public clg_financeurs financeurs
    {
        get{return c_financeurs;}
        set
        {
            if(value != null)
            {
                if(c_financeurs != value)
                {
                    CreerClone();
                    c_financeurs = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ara_d_solde (ara_d_solde)
    * @return c_ara_d_solde */
    public DateTime ara_d_solde
    {
        get{return c_ara_d_solde;}
        set
        {
            if(c_ara_d_solde != value)
            {
                CreerClone();
                c_ara_d_solde = value;
            }
        }
    }
    /* Accesseur de la propriete ara_d_engagement (ara_d_engagement)
    * @return c_ara_d_engagement */
    public DateTime ara_d_engagement
    {
        get{return c_ara_d_engagement;}
        set
        {
            if(c_ara_d_engagement != value)
            {
                CreerClone();
                c_ara_d_engagement = value;
            }
        }
    }
    /* Accesseur de la propriete ara_n_archive (ara_n_archive)
    * @return c_ara_n_archive */
    public Int64 ara_n_archive
    {
        get{return c_ara_n_archive;}
        set
        {
            if(c_ara_n_archive != value)
            {
                CreerClone();
                c_ara_n_archive = value;
            }
        }
    }
    /* Accesseur de la propriete etat_arrete (ara_n_etat)
    * @return c_etat_arrete */
    public clg_etat_arrete etat_arrete
    {
        get{return c_etat_arrete;}
        set
        {
            if(c_etat_arrete != value)
            {
                CreerClone();
                c_etat_arrete = value;
            }
        }
    }
    /* Accesseur de la propriete ara_a_numara (ara_a_numara)
    * @return c_ara_a_numara */
    public string ara_a_numara
    {
        get{return c_ara_a_numara;}
        set
        {
            if(c_ara_a_numara != value)
            {
                CreerClone();
                c_ara_a_numara = value;
            }
        }
    }
    /* Accesseur de la propriete ara_n_engage (ara_n_engage)
    * @return c_ara_n_engage */
    public Int64 ara_n_engage
    {
        get{return c_ara_n_engage;}
        set
        {
            if(c_ara_n_engage != value)
            {
                CreerClone();
                c_ara_n_engage = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type comment */
    public List<clg_comment> Listecomment
    {
        get { return c_Listecomment; }
    }    /* Accesseur de la liste des objets de type lien_arretes */
    public List<clg_lien_arretes> Listelien_arretes
    {
        get { return c_Listelien_arretes; }
    }    /* Accesseur de la liste des objets de type lien_sage */
    public List<clg_lien_sage> Listelien_sage
    {
        get { return c_Listelien_sage; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ara_cn.ToString());
		l_Valeurs.Add(c_ara_a_lib.ToString());
		l_Valeurs.Add(c_ara_d_piece.ToString());
		l_Valeurs.Add(c_ara_d_limite.ToString());
		l_Valeurs.Add(c_annee_comptable==null ? "-1" : c_annee_comptable.ID.ToString());
		l_Valeurs.Add(c_ara_n_montant.ToString());
		l_Valeurs.Add(c_ara_n_ecno.ToString());
		l_Valeurs.Add(c_ara_n_cpt.ToString());
		l_Valeurs.Add(c_financeurs==null ? "-1" : c_financeurs.ID.ToString());
		l_Valeurs.Add(c_ara_d_solde.ToString());
		l_Valeurs.Add(c_ara_d_engagement.ToString());
		l_Valeurs.Add(c_ara_n_archive.ToString());
		l_Valeurs.Add(c_etat_arrete==null ? "-1" : c_etat_arrete.ID.ToString());
		l_Valeurs.Add(c_ara_a_numara.ToString());
		l_Valeurs.Add(c_ara_n_engage.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ara_cn");
		l_Noms.Add("ara_a_lib");
		l_Noms.Add("ara_d_piece");
		l_Noms.Add("ara_d_limite");
		l_Noms.Add("annee_comptable");
		l_Noms.Add("ara_n_montant");
		l_Noms.Add("ara_n_ecno");
		l_Noms.Add("ara_n_cpt");
		l_Noms.Add("financeurs");
		l_Noms.Add("ara_d_solde");
		l_Noms.Add("ara_d_engagement");
		l_Noms.Add("ara_n_archive");
		l_Noms.Add("etat_arrete");
		l_Noms.Add("ara_a_numara");
		l_Noms.Add("ara_n_engage");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
