namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : operations </summary>
public partial class clg_operations : clg_elements
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLoperations;
    protected clg_Modele c_Modele;

	private clg_annee_comptable c_annee_comptable;
	private clg_t_ope_financiere c_t_ope_financiere;
	private clg_c_operation c_c_operation;
	private Int64 c_ope_n_validation_prev;
	private Int64 c_ope_n_validation_real;
	private string c_ope_d_date_deb_valid_real;
	private string c_ope_d_date_fin_valid_real;
	private Int64 c_ope_n_synchro_tps_prevus;
	private Int64 c_ope_ann_cn;
	private Int64 c_ope_n_taux_financeurs;
	private bool c_ope_b_ope_terminee;
	private List<clg_financement_prevus> c_Listefinancement_prevus;
	private List<clg_annee_civile> c_Listeannee_civile;
	private List<clg_prestation_prevus> c_Listeprestation_prevus;
	private List<clg_report> c_Listereport;
	private List<clg_temps_intervenants> c_Listetemps_intervenants;


    private void Init()
    {
		c_Listefinancement_prevus = new List<clg_financement_prevus>();
		c_Listeannee_civile = new List<clg_annee_civile>();
		c_Listeprestation_prevus = new List<clg_prestation_prevus>();
		c_Listereport = new List<clg_report>();
		c_Listetemps_intervenants = new List<clg_temps_intervenants>();

    }

    public override void Detruit()
    {
		c_Listefinancement_prevus.Clear();
		c_Listeannee_civile.Clear();
		c_Listeprestation_prevus.Clear();
		c_Listereport.Clear();
		c_Listetemps_intervenants.Clear();
		this.c_annee_comptable = null;
		this.c_t_ope_financiere = null;
		this.c_c_operation = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_operations(clg_Modele pModele, Int64 pele_cn, bool pAMAJ = false) : base(pModele, pele_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_operations(clg_Modele pModele, Int64 pelements3, clg_annee_comptable pannee_comptable, clg_t_ope_financiere pt_ope_financiere, clg_c_operation pc_operation, Int64 pope_n_validation_prev, Int64 pope_n_validation_real, string pope_d_date_deb_valid_real, string pope_d_date_fin_valid_real, Int64 pope_n_synchro_tps_prevus, Int64 pope_ann_cn, Int64 pope_n_taux_financeurs, bool pope_b_ope_terminee, clg_elements pelements, Int64 pele_n_niveau, string pele_a_lib, clg_t_element pt_element, string pele_a_code, clg_elements pelements2, Int64 pele_n_archive, DateTime pele_d_debut_prev, DateTime pele_d_fin_reelle, DateTime pele_d_fin_prev, DateTime pele_d_debut_reelle, DateTime pele_d_archivage, Int64 pele_n_visibleetat, bool pAMAJ = true) : base(pModele, pelements3, pelements, pele_n_niveau, pele_a_lib, pt_element, pele_a_code, pelements2, pele_n_archive, pele_d_debut_prev, pele_d_fin_reelle, pele_d_fin_prev, pele_d_debut_reelle, pele_d_archivage, pele_n_visibleetat, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        c_annee_comptable = pannee_comptable;
		        c_t_ope_financiere = pt_ope_financiere;
		        c_c_operation = pc_operation;
		        c_ope_n_validation_prev = pope_n_validation_prev;
		        c_ope_n_validation_real = pope_n_validation_real;
		        c_ope_d_date_deb_valid_real = pope_d_date_deb_valid_real;
		        c_ope_d_date_fin_valid_real = pope_d_date_fin_valid_real;
		        c_ope_n_synchro_tps_prevus = pope_n_synchro_tps_prevus;
		        c_ope_ann_cn = pope_ann_cn;
		        c_ope_n_taux_financeurs = pope_n_taux_financeurs;
		        c_ope_b_ope_terminee = pope_b_ope_terminee;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pelements3, Int64 pannee_comptable, Int64 pt_ope_financiere, Int64 pc_operation, Int64 pope_n_validation_prev, Int64 pope_n_validation_real, string pope_d_date_deb_valid_real, string pope_d_date_fin_valid_real, Int64 pope_n_synchro_tps_prevus, Int64 pope_ann_cn, Int64 pope_n_taux_financeurs, bool pope_b_ope_terminee, Int64 pelements, Int64 pele_n_niveau, string pele_a_lib, Int64 pt_element, string pele_a_code, Int64 pelements2, Int64 pele_n_archive, DateTime pele_d_debut_prev, DateTime pele_d_fin_reelle, DateTime pele_d_fin_prev, DateTime pele_d_debut_reelle, DateTime pele_d_archivage, Int64 pele_n_visibleetat)
    {   
		c_dicReferences.Add("annee_comptable", pannee_comptable);
		c_dicReferences.Add("t_ope_financiere", pt_ope_financiere);
		c_dicReferences.Add("c_operation", pc_operation);
		        c_ope_n_validation_prev = pope_n_validation_prev;
		        c_ope_n_validation_real = pope_n_validation_real;
		        c_ope_d_date_deb_valid_real = pope_d_date_deb_valid_real;
		        c_ope_d_date_fin_valid_real = pope_d_date_fin_valid_real;
		        c_ope_n_synchro_tps_prevus = pope_n_synchro_tps_prevus;
		        c_ope_ann_cn = pope_ann_cn;
		        c_ope_n_taux_financeurs = pope_n_taux_financeurs;
		        c_ope_b_ope_terminee = pope_b_ope_terminee;

        base.Initialise(pelements3, pelements, pele_n_niveau, pele_a_lib, pt_element, pele_a_code, pelements2, pele_n_archive, pele_d_debut_prev, pele_d_fin_reelle, pele_d_fin_prev, pele_d_debut_reelle, pele_d_archivage, pele_n_visibleetat);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLoperations; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		base.CreeLiens();
		c_annee_comptable = (clg_annee_comptable) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_comptable"], "clg_annee_comptable");
		c_t_ope_financiere = (clg_t_ope_financiere) c_ModeleBase.RenvoieObjet(c_dicReferences["t_ope_financiere"], "clg_t_ope_financiere");
		c_c_operation = (clg_c_operation) c_ModeleBase.RenvoieObjet(c_dicReferences["c_operation"], "clg_c_operation");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeoperations.Dictionnaire.Add(ele_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeoperations.Dictionnaire.Remove(ele_cn);
		base.SupprimeDansListe();

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		base.CreeReferences();
		if(c_annee_comptable != null)if(!c_annee_comptable.Listeoperations.Contains(this)) c_annee_comptable.Listeoperations.Add(this);
		if(c_t_ope_financiere != null)if(!c_t_ope_financiere.Listeoperations.Contains(this)) c_t_ope_financiere.Listeoperations.Add(this);
		if(c_c_operation != null)if(!c_c_operation.Listeoperations.Contains(this)) c_c_operation.Listeoperations.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_operations l_Clone = (clg_operations) Clone;
		base.DetruitReferences();
		if(l_Clone.annee_comptable != null)if(l_Clone.annee_comptable.Listeoperations.Contains(this)) l_Clone.annee_comptable.Listeoperations.Remove(this);
		if(l_Clone.t_ope_financiere != null)if(l_Clone.t_ope_financiere.Listeoperations.Contains(this)) l_Clone.t_ope_financiere.Listeoperations.Remove(this);
		if(l_Clone.c_operation != null)if(l_Clone.c_operation.Listeoperations.Contains(this)) l_Clone.c_operation.Listeoperations.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listefinancement_prevus.Count > 0) c_EstReference = true;
		if(c_Listeannee_civile.Count > 0) c_EstReference = true;
		if(c_Listeprestation_prevus.Count > 0) c_EstReference = true;
		if(c_Listereport.Count > 0) c_EstReference = true;
		if(c_Listetemps_intervenants.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_operations(null, ele_cn, annee_comptable, t_ope_financiere, c_operation, ope_n_validation_prev, ope_n_validation_real, ope_d_date_deb_valid_real, ope_d_date_fin_valid_real, ope_n_synchro_tps_prevus, ope_ann_cn, ope_n_taux_financeurs, ope_b_ope_terminee, elements, ele_n_niveau, ele_a_lib, t_element, ele_a_code, elements2, ele_n_archive, ele_d_debut_prev, ele_d_fin_reelle, ele_d_fin_prev, ele_d_debut_reelle, ele_d_archivage, ele_n_visibleetat,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_operations l_clone = (clg_operations) this.Clone;
		c_annee_comptable = l_clone.annee_comptable;
		c_t_ope_financiere = l_clone.t_ope_financiere;
		c_c_operation = l_clone.c_operation;
		c_ope_n_validation_prev = l_clone.ope_n_validation_prev;
		c_ope_n_validation_real = l_clone.ope_n_validation_real;
		c_ope_d_date_deb_valid_real = l_clone.ope_d_date_deb_valid_real;
		c_ope_d_date_fin_valid_real = l_clone.ope_d_date_fin_valid_real;
		c_ope_n_synchro_tps_prevus = l_clone.ope_n_synchro_tps_prevus;
		c_ope_ann_cn = l_clone.ope_ann_cn;
		c_ope_n_taux_financeurs = l_clone.ope_n_taux_financeurs;
		c_ope_b_ope_terminee = l_clone.ope_b_ope_terminee;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLoperations.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLoperations.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLoperations.Delete(this);
    }

    /* Accesseur de la propriete annee_comptable (ope_acp_cn)
    * @return c_annee_comptable */
    public clg_annee_comptable annee_comptable
    {
        get{return c_annee_comptable;}
        set
        {
            if(c_annee_comptable != value)
            {
                CreerClone();
                c_annee_comptable = value;
            }
        }
    }
    /* Accesseur de la propriete t_ope_financiere (ope_t_opf_cn)
    * @return c_t_ope_financiere */
    public clg_t_ope_financiere t_ope_financiere
    {
        get{return c_t_ope_financiere;}
        set
        {
            if(c_t_ope_financiere != value)
            {
                CreerClone();
                c_t_ope_financiere = value;
            }
        }
    }
    /* Accesseur de la propriete c_operation (ope_c_ope_cn)
    * @return c_c_operation */
    public clg_c_operation c_operation
    {
        get{return c_c_operation;}
        set
        {
            if(c_c_operation != value)
            {
                CreerClone();
                c_c_operation = value;
            }
        }
    }
    /* Accesseur de la propriete ope_n_validation_prev (ope_n_validation_prev)
    * @return c_ope_n_validation_prev */
    public Int64 ope_n_validation_prev
    {
        get{return c_ope_n_validation_prev;}
        set
        {
            if(c_ope_n_validation_prev != value)
            {
                CreerClone();
                c_ope_n_validation_prev = value;
            }
        }
    }
    /* Accesseur de la propriete ope_n_validation_real (ope_n_validation_real)
    * @return c_ope_n_validation_real */
    public Int64 ope_n_validation_real
    {
        get{return c_ope_n_validation_real;}
        set
        {
            if(c_ope_n_validation_real != value)
            {
                CreerClone();
                c_ope_n_validation_real = value;
            }
        }
    }
    /* Accesseur de la propriete ope_d_date_deb_valid_real (ope_d_date_deb_valid_real)
    * @return c_ope_d_date_deb_valid_real */
    public string ope_d_date_deb_valid_real
    {
        get{return c_ope_d_date_deb_valid_real;}
        set
        {
            if(c_ope_d_date_deb_valid_real != value)
            {
                CreerClone();
                c_ope_d_date_deb_valid_real = value;
            }
        }
    }
    /* Accesseur de la propriete ope_d_date_fin_valid_real (ope_d_date_fin_valid_real)
    * @return c_ope_d_date_fin_valid_real */
    public string ope_d_date_fin_valid_real
    {
        get{return c_ope_d_date_fin_valid_real;}
        set
        {
            if(c_ope_d_date_fin_valid_real != value)
            {
                CreerClone();
                c_ope_d_date_fin_valid_real = value;
            }
        }
    }
    /* Accesseur de la propriete ope_n_synchro_tps_prevus (ope_n_synchro_tps_prevus)
    * @return c_ope_n_synchro_tps_prevus */
    public Int64 ope_n_synchro_tps_prevus
    {
        get{return c_ope_n_synchro_tps_prevus;}
        set
        {
            if(c_ope_n_synchro_tps_prevus != value)
            {
                CreerClone();
                c_ope_n_synchro_tps_prevus = value;
            }
        }
    }
    /* Accesseur de la propriete ope_ann_cn (ope_ann_cn)
    * @return c_ope_ann_cn */
    public Int64 ope_ann_cn
    {
        get{return c_ope_ann_cn;}
        set
        {
            if(c_ope_ann_cn != value)
            {
                CreerClone();
                c_ope_ann_cn = value;
            }
        }
    }
    /* Accesseur de la propriete ope_n_taux_financeurs (ope_n_taux_financeurs)
    * @return c_ope_n_taux_financeurs */
    public Int64 ope_n_taux_financeurs
    {
        get{return c_ope_n_taux_financeurs;}
        set
        {
            if(c_ope_n_taux_financeurs != value)
            {
                CreerClone();
                c_ope_n_taux_financeurs = value;
            }
        }
    }
    /* Accesseur de la propriete ope_b_ope_terminee (ope_b_ope_terminee)
    * @return c_ope_b_ope_terminee */
    public bool ope_b_ope_terminee
    {
        get{return c_ope_b_ope_terminee;}
        set
        {
            if(c_ope_b_ope_terminee != value)
            {
                CreerClone();
                c_ope_b_ope_terminee = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type financement_prevus */
    public List<clg_financement_prevus> Listefinancement_prevus
    {
        get { return c_Listefinancement_prevus; }
    }    /* Accesseur de la liste des objets de type annee_civile */
    public List<clg_annee_civile> Listeannee_civile
    {
        get { return c_Listeannee_civile; }
    }    /* Accesseur de la liste des objets de type prestation_prevus */
    public List<clg_prestation_prevus> Listeprestation_prevus
    {
        get { return c_Listeprestation_prevus; }
    }    /* Accesseur de la liste des objets de type report */
    public List<clg_report> Listereport
    {
        get { return c_Listereport; }
    }    /* Accesseur de la liste des objets de type temps_intervenants */
    public List<clg_temps_intervenants> Listetemps_intervenants
    {
        get { return c_Listetemps_intervenants; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_annee_comptable==null ? "-1" : c_annee_comptable.ID.ToString());
		l_Valeurs.Add(c_t_ope_financiere==null ? "-1" : c_t_ope_financiere.ID.ToString());
		l_Valeurs.Add(c_c_operation==null ? "-1" : c_c_operation.ID.ToString());
		l_Valeurs.Add(c_ope_n_validation_prev.ToString());
		l_Valeurs.Add(c_ope_n_validation_real.ToString());
		l_Valeurs.Add(c_ope_d_date_deb_valid_real.ToString());
		l_Valeurs.Add(c_ope_d_date_fin_valid_real.ToString());
		l_Valeurs.Add(c_ope_n_synchro_tps_prevus.ToString());
		l_Valeurs.Add(c_ope_ann_cn.ToString());
		l_Valeurs.Add(c_ope_n_taux_financeurs.ToString());
		l_Valeurs.Add(c_ope_b_ope_terminee.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("elements3");
		l_Noms.Add("annee_comptable");
		l_Noms.Add("t_ope_financiere");
		l_Noms.Add("c_operation");
		l_Noms.Add("ope_n_validation_prev");
		l_Noms.Add("ope_n_validation_real");
		l_Noms.Add("ope_d_date_deb_valid_real");
		l_Noms.Add("ope_d_date_fin_valid_real");
		l_Noms.Add("ope_n_synchro_tps_prevus");
		l_Noms.Add("ope_ann_cn");
		l_Noms.Add("ope_n_taux_financeurs");
		l_Noms.Add("ope_b_ope_terminee");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
    }
}
