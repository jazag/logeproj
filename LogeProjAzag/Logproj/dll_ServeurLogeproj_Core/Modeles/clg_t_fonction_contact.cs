namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_fonction_contact </summary>
public partial class clg_t_fonction_contact : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_fonction_contact;
    protected clg_Modele c_Modele;

	private Int64 c_t_fct_ctc_cn;
	private string c_t_fct_ctc_a_lib;
	private List<clg_contacts> c_Listecontacts;


    private void Init()
    {
		c_Listecontacts = new List<clg_contacts>();

    }

    public override void Detruit()
    {
		c_Listecontacts.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_fonction_contact(clg_Modele pModele, Int64 pt_fct_ctc_cn, bool pAMAJ = false) : base(pModele, pt_fct_ctc_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_fonction_contact(clg_Modele pModele, Int64 pt_fct_ctc_cn, string pt_fct_ctc_a_lib, bool pAMAJ = true) : base(pModele, pt_fct_ctc_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_fct_ctc_cn != Int64.MinValue)
            c_t_fct_ctc_cn = pt_fct_ctc_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_fct_ctc_a_lib != null)
            c_t_fct_ctc_a_lib = pt_fct_ctc_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_fct_ctc_cn, string pt_fct_ctc_a_lib)
    {   
		        c_t_fct_ctc_cn = pt_fct_ctc_cn;
		        c_t_fct_ctc_a_lib = pt_fct_ctc_a_lib;

        base.Initialise(pt_fct_ctc_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_fonction_contact; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_fonction_contact.Dictionnaire.Add(t_fct_ctc_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_fonction_contact.Dictionnaire.Remove(t_fct_ctc_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_fonction_contact l_Clone = (clg_t_fonction_contact) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecontacts.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_fonction_contact(null, t_fct_ctc_cn, t_fct_ctc_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_fonction_contact l_clone = (clg_t_fonction_contact) this.Clone;
		c_t_fct_ctc_cn = l_clone.t_fct_ctc_cn;
		c_t_fct_ctc_a_lib = l_clone.t_fct_ctc_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_fonction_contact.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_fonction_contact.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_fonction_contact.Delete(this);
    }

    /* Accesseur de la propriete t_fct_ctc_cn (t_fct_ctc_cn)
    * @return c_t_fct_ctc_cn */
    public Int64 t_fct_ctc_cn
    {
        get{return c_t_fct_ctc_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_fct_ctc_cn != value)
                {
                    CreerClone();
                    c_t_fct_ctc_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_fct_ctc_a_lib (t_fct_ctc_a_lib)
    * @return c_t_fct_ctc_a_lib */
    public string t_fct_ctc_a_lib
    {
        get{return c_t_fct_ctc_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_t_fct_ctc_a_lib != value)
                {
                    CreerClone();
                    c_t_fct_ctc_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type contacts */
    public List<clg_contacts> Listecontacts
    {
        get { return c_Listecontacts; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_fct_ctc_cn.ToString());
		l_Valeurs.Add(c_t_fct_ctc_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_fct_ctc_cn");
		l_Noms.Add("t_fct_ctc_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
