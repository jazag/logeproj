namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : param_conservatoire </summary>
public partial class clg_param_conservatoire : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLparam_conservatoire;
    protected clg_Modele c_Modele;

	private Int64 c_prmc_cn;
	private clg_infos_conservatoire c_infos_conservatoire;
	private clg_parametrage c_parametrage;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_infos_conservatoire = null;
		this.c_parametrage = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_param_conservatoire(clg_Modele pModele, Int64 pprmc_cn, bool pAMAJ = false) : base(pModele, pprmc_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_param_conservatoire(clg_Modele pModele, Int64 pprmc_cn, clg_infos_conservatoire pinfos_conservatoire, clg_parametrage pparametrage, bool pAMAJ = true) : base(pModele, pprmc_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pprmc_cn != Int64.MinValue)
            c_prmc_cn = pprmc_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_infos_conservatoire = pinfos_conservatoire;
		        c_parametrage = pparametrage;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pprmc_cn, Int64 pinfos_conservatoire, Int64 pparametrage)
    {   
		        c_prmc_cn = pprmc_cn;
		c_dicReferences.Add("infos_conservatoire", pinfos_conservatoire);
		c_dicReferences.Add("parametrage", pparametrage);

        base.Initialise(pprmc_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLparam_conservatoire; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_infos_conservatoire = (clg_infos_conservatoire) c_ModeleBase.RenvoieObjet(c_dicReferences["infos_conservatoire"], "clg_infos_conservatoire");
		c_parametrage = (clg_parametrage) c_ModeleBase.RenvoieObjet(c_dicReferences["parametrage"], "clg_parametrage");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeparam_conservatoire.Dictionnaire.Add(prmc_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeparam_conservatoire.Dictionnaire.Remove(prmc_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_infos_conservatoire != null)if(!c_infos_conservatoire.Listeparam_conservatoire.Contains(this)) c_infos_conservatoire.Listeparam_conservatoire.Add(this);
		if(c_parametrage != null)if(!c_parametrage.Listeparam_conservatoire.Contains(this)) c_parametrage.Listeparam_conservatoire.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_param_conservatoire l_Clone = (clg_param_conservatoire) Clone;
		if(l_Clone.infos_conservatoire != null)if(l_Clone.infos_conservatoire.Listeparam_conservatoire.Contains(this)) l_Clone.infos_conservatoire.Listeparam_conservatoire.Remove(this);
		if(l_Clone.parametrage != null)if(l_Clone.parametrage.Listeparam_conservatoire.Contains(this)) l_Clone.parametrage.Listeparam_conservatoire.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_param_conservatoire(null, prmc_cn, infos_conservatoire, parametrage,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_param_conservatoire l_clone = (clg_param_conservatoire) this.Clone;
		c_prmc_cn = l_clone.prmc_cn;
		c_infos_conservatoire = l_clone.infos_conservatoire;
		c_parametrage = l_clone.parametrage;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLparam_conservatoire.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLparam_conservatoire.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLparam_conservatoire.Delete(this);
    }

    /* Accesseur de la propriete prmc_cn (prmc_cn)
    * @return c_prmc_cn */
    public Int64 prmc_cn
    {
        get{return c_prmc_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_prmc_cn != value)
                {
                    CreerClone();
                    c_prmc_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete infos_conservatoire (prmc_csr_cn)
    * @return c_infos_conservatoire */
    public clg_infos_conservatoire infos_conservatoire
    {
        get{return c_infos_conservatoire;}
        set
        {
            if(c_infos_conservatoire != value)
            {
                CreerClone();
                c_infos_conservatoire = value;
            }
        }
    }
    /* Accesseur de la propriete parametrage (prmc_prm_cn)
    * @return c_parametrage */
    public clg_parametrage parametrage
    {
        get{return c_parametrage;}
        set
        {
            if(c_parametrage != value)
            {
                CreerClone();
                c_parametrage = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_prmc_cn.ToString());
		l_Valeurs.Add(c_infos_conservatoire==null ? "-1" : c_infos_conservatoire.ID.ToString());
		l_Valeurs.Add(c_parametrage==null ? "-1" : c_parametrage.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("prmc_cn");
		l_Noms.Add("infos_conservatoire");
		l_Noms.Add("parametrage");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
