namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : facture </summary>
public partial class clg_facture : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfacture;
    protected clg_Modele c_Modele;

	private int c_fac_cn;
	private string c_fac_num;
	private clg_elements c_elements;
	private DateTime c_fac_dt_generation;
	private clg_annee_comptable c_annee_comptable;
	private DateTime c_fac_dt_debut;
	private DateTime c_fac_dt_fin;
	private double c_fac_mtt_tot;
	private Int64 c_fac_fin_cn;
	private string c_fac_commentaire;
	private double c_fac_mtt_tps;
	private double c_fac_mtt_prestation;
	private double c_fac_mtt_deplacement;
	private double c_fac_mtt_avoir;
	private double c_fac_n_mtt_sollicite;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_elements = null;
		this.c_annee_comptable = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_facture(clg_Modele pModele, int pfac_cn, bool pAMAJ = false) : base(pModele, pfac_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_facture(clg_Modele pModele, int pfac_cn, string pfac_num, clg_elements pelements, DateTime pfac_dt_generation, clg_annee_comptable pannee_comptable, DateTime pfac_dt_debut, DateTime pfac_dt_fin, double pfac_mtt_tot, Int64 pfac_fin_cn, string pfac_commentaire, double pfac_mtt_tps, double pfac_mtt_prestation, double pfac_mtt_deplacement, double pfac_mtt_avoir, double pfac_n_mtt_sollicite, bool pAMAJ = true) : base(pModele, pfac_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfac_cn != int.MinValue)
            c_fac_cn = pfac_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_fac_num = pfac_num;
		        c_elements = pelements;
		        c_fac_dt_generation = pfac_dt_generation;
		        c_annee_comptable = pannee_comptable;
		        c_fac_dt_debut = pfac_dt_debut;
		        c_fac_dt_fin = pfac_dt_fin;
		        c_fac_mtt_tot = pfac_mtt_tot;
		        c_fac_fin_cn = pfac_fin_cn;
		        c_fac_commentaire = pfac_commentaire;
		        c_fac_mtt_tps = pfac_mtt_tps;
		        c_fac_mtt_prestation = pfac_mtt_prestation;
		        c_fac_mtt_deplacement = pfac_mtt_deplacement;
		        c_fac_mtt_avoir = pfac_mtt_avoir;
		        c_fac_n_mtt_sollicite = pfac_n_mtt_sollicite;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(int pfac_cn, string pfac_num, Int64 pelements, DateTime pfac_dt_generation, Int64 pannee_comptable, DateTime pfac_dt_debut, DateTime pfac_dt_fin, double pfac_mtt_tot, Int64 pfac_fin_cn, string pfac_commentaire, double pfac_mtt_tps, double pfac_mtt_prestation, double pfac_mtt_deplacement, double pfac_mtt_avoir, double pfac_n_mtt_sollicite)
    {   
		        c_fac_cn = pfac_cn;
		        c_fac_num = pfac_num;
		c_dicReferences.Add("elements", pelements);
		        c_fac_dt_generation = pfac_dt_generation;
		c_dicReferences.Add("annee_comptable", pannee_comptable);
		        c_fac_dt_debut = pfac_dt_debut;
		        c_fac_dt_fin = pfac_dt_fin;
		        c_fac_mtt_tot = pfac_mtt_tot;
		        c_fac_fin_cn = pfac_fin_cn;
		        c_fac_commentaire = pfac_commentaire;
		        c_fac_mtt_tps = pfac_mtt_tps;
		        c_fac_mtt_prestation = pfac_mtt_prestation;
		        c_fac_mtt_deplacement = pfac_mtt_deplacement;
		        c_fac_mtt_avoir = pfac_mtt_avoir;
		        c_fac_n_mtt_sollicite = pfac_n_mtt_sollicite;

        base.Initialise(pfac_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfacture; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_elements = (clg_elements) c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");
		c_annee_comptable = (clg_annee_comptable) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_comptable"], "clg_annee_comptable");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefacture.Dictionnaire.Add(fac_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefacture.Dictionnaire.Remove(fac_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_elements != null)if(!c_elements.Listefacture.Contains(this)) c_elements.Listefacture.Add(this);
		if(c_annee_comptable != null)if(!c_annee_comptable.Listefacture.Contains(this)) c_annee_comptable.Listefacture.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_facture l_Clone = (clg_facture) Clone;
		if(l_Clone.elements != null)if(l_Clone.elements.Listefacture.Contains(this)) l_Clone.elements.Listefacture.Remove(this);
		if(l_Clone.annee_comptable != null)if(l_Clone.annee_comptable.Listefacture.Contains(this)) l_Clone.annee_comptable.Listefacture.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_facture(null, fac_cn, fac_num, elements, fac_dt_generation, annee_comptable, fac_dt_debut, fac_dt_fin, fac_mtt_tot, fac_fin_cn, fac_commentaire, fac_mtt_tps, fac_mtt_prestation, fac_mtt_deplacement, fac_mtt_avoir, fac_n_mtt_sollicite,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_facture l_clone = (clg_facture) this.Clone;
		c_fac_cn = l_clone.fac_cn;
		c_fac_num = l_clone.fac_num;
		c_elements = l_clone.elements;
		c_fac_dt_generation = l_clone.fac_dt_generation;
		c_annee_comptable = l_clone.annee_comptable;
		c_fac_dt_debut = l_clone.fac_dt_debut;
		c_fac_dt_fin = l_clone.fac_dt_fin;
		c_fac_mtt_tot = l_clone.fac_mtt_tot;
		c_fac_fin_cn = l_clone.fac_fin_cn;
		c_fac_commentaire = l_clone.fac_commentaire;
		c_fac_mtt_tps = l_clone.fac_mtt_tps;
		c_fac_mtt_prestation = l_clone.fac_mtt_prestation;
		c_fac_mtt_deplacement = l_clone.fac_mtt_deplacement;
		c_fac_mtt_avoir = l_clone.fac_mtt_avoir;
		c_fac_n_mtt_sollicite = l_clone.fac_n_mtt_sollicite;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfacture.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfacture.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfacture.Delete(this);
    }

    /* Accesseur de la propriete fac_cn (fac_cn)
    * @return c_fac_cn */
    public int fac_cn
    {
        get{return c_fac_cn;}
        set
        {
            if(value != int.MinValue)
            {
                if(c_fac_cn != value)
                {
                    CreerClone();
                    c_fac_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete fac_num (fac_num)
    * @return c_fac_num */
    public string fac_num
    {
        get{return c_fac_num;}
        set
        {
            if(c_fac_num != value)
            {
                CreerClone();
                c_fac_num = value;
            }
        }
    }
    /* Accesseur de la propriete elements (fac_ele_cn)
    * @return c_elements */
    public clg_elements elements
    {
        get{return c_elements;}
        set
        {
            if(c_elements != value)
            {
                CreerClone();
                c_elements = value;
            }
        }
    }
    /* Accesseur de la propriete fac_dt_generation (fac_dt_generation)
    * @return c_fac_dt_generation */
    public DateTime fac_dt_generation
    {
        get{return c_fac_dt_generation;}
        set
        {
            if(c_fac_dt_generation != value)
            {
                CreerClone();
                c_fac_dt_generation = value;
            }
        }
    }
    /* Accesseur de la propriete annee_comptable (fac_acp_cn)
    * @return c_annee_comptable */
    public clg_annee_comptable annee_comptable
    {
        get{return c_annee_comptable;}
        set
        {
            if(c_annee_comptable != value)
            {
                CreerClone();
                c_annee_comptable = value;
            }
        }
    }
    /* Accesseur de la propriete fac_dt_debut (fac_dt_debut)
    * @return c_fac_dt_debut */
    public DateTime fac_dt_debut
    {
        get{return c_fac_dt_debut;}
        set
        {
            if(c_fac_dt_debut != value)
            {
                CreerClone();
                c_fac_dt_debut = value;
            }
        }
    }
    /* Accesseur de la propriete fac_dt_fin (fac_dt_fin)
    * @return c_fac_dt_fin */
    public DateTime fac_dt_fin
    {
        get{return c_fac_dt_fin;}
        set
        {
            if(c_fac_dt_fin != value)
            {
                CreerClone();
                c_fac_dt_fin = value;
            }
        }
    }
    /* Accesseur de la propriete fac_mtt_tot (fac_mtt_tot)
    * @return c_fac_mtt_tot */
    public double fac_mtt_tot
    {
        get{return c_fac_mtt_tot;}
        set
        {
            if(c_fac_mtt_tot != value)
            {
                CreerClone();
                c_fac_mtt_tot = value;
            }
        }
    }
    /* Accesseur de la propriete fac_fin_cn (fac_fin_cn)
    * @return c_fac_fin_cn */
    public Int64 fac_fin_cn
    {
        get{return c_fac_fin_cn;}
        set
        {
            if(c_fac_fin_cn != value)
            {
                CreerClone();
                c_fac_fin_cn = value;
            }
        }
    }
    /* Accesseur de la propriete fac_commentaire (fac_commentaire)
    * @return c_fac_commentaire */
    public string fac_commentaire
    {
        get{return c_fac_commentaire;}
        set
        {
            if(c_fac_commentaire != value)
            {
                CreerClone();
                c_fac_commentaire = value;
            }
        }
    }
    /* Accesseur de la propriete fac_mtt_tps (fac_mtt_tps)
    * @return c_fac_mtt_tps */
    public double fac_mtt_tps
    {
        get{return c_fac_mtt_tps;}
        set
        {
            if(c_fac_mtt_tps != value)
            {
                CreerClone();
                c_fac_mtt_tps = value;
            }
        }
    }
    /* Accesseur de la propriete fac_mtt_prestation (fac_mtt_prestation)
    * @return c_fac_mtt_prestation */
    public double fac_mtt_prestation
    {
        get{return c_fac_mtt_prestation;}
        set
        {
            if(c_fac_mtt_prestation != value)
            {
                CreerClone();
                c_fac_mtt_prestation = value;
            }
        }
    }
    /* Accesseur de la propriete fac_mtt_deplacement (fac_mtt_deplacement)
    * @return c_fac_mtt_deplacement */
    public double fac_mtt_deplacement
    {
        get{return c_fac_mtt_deplacement;}
        set
        {
            if(c_fac_mtt_deplacement != value)
            {
                CreerClone();
                c_fac_mtt_deplacement = value;
            }
        }
    }
    /* Accesseur de la propriete fac_mtt_avoir (fac_mtt_avoir)
    * @return c_fac_mtt_avoir */
    public double fac_mtt_avoir
    {
        get{return c_fac_mtt_avoir;}
        set
        {
            if(c_fac_mtt_avoir != value)
            {
                CreerClone();
                c_fac_mtt_avoir = value;
            }
        }
    }
    /* Accesseur de la propriete fac_n_mtt_sollicite (fac_n_mtt_sollicite)
    * @return c_fac_n_mtt_sollicite */
    public double fac_n_mtt_sollicite
    {
        get{return c_fac_n_mtt_sollicite;}
        set
        {
            if(c_fac_n_mtt_sollicite != value)
            {
                CreerClone();
                c_fac_n_mtt_sollicite = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_fac_cn.ToString());
		l_Valeurs.Add(c_fac_num.ToString());
		l_Valeurs.Add(c_elements==null ? "-1" : c_elements.ID.ToString());
		l_Valeurs.Add(c_fac_dt_generation.ToString());
		l_Valeurs.Add(c_annee_comptable==null ? "-1" : c_annee_comptable.ID.ToString());
		l_Valeurs.Add(c_fac_dt_debut.ToString());
		l_Valeurs.Add(c_fac_dt_fin.ToString());
		l_Valeurs.Add(c_fac_mtt_tot.ToString());
		l_Valeurs.Add(c_fac_fin_cn.ToString());
		l_Valeurs.Add(c_fac_commentaire.ToString());
		l_Valeurs.Add(c_fac_mtt_tps.ToString());
		l_Valeurs.Add(c_fac_mtt_prestation.ToString());
		l_Valeurs.Add(c_fac_mtt_deplacement.ToString());
		l_Valeurs.Add(c_fac_mtt_avoir.ToString());
		l_Valeurs.Add(c_fac_n_mtt_sollicite.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("fac_cn");
		l_Noms.Add("fac_num");
		l_Noms.Add("elements");
		l_Noms.Add("fac_dt_generation");
		l_Noms.Add("annee_comptable");
		l_Noms.Add("fac_dt_debut");
		l_Noms.Add("fac_dt_fin");
		l_Noms.Add("fac_mtt_tot");
		l_Noms.Add("fac_fin_cn");
		l_Noms.Add("fac_commentaire");
		l_Noms.Add("fac_mtt_tps");
		l_Noms.Add("fac_mtt_prestation");
		l_Noms.Add("fac_mtt_deplacement");
		l_Noms.Add("fac_mtt_avoir");
		l_Noms.Add("fac_n_mtt_sollicite");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
