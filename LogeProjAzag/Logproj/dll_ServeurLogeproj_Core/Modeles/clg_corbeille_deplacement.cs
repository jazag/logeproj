namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : corbeille_deplacement </summary>
public partial class clg_corbeille_deplacement : clg_deplacements_realises
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcorbeille_deplacement;
    protected clg_Modele c_Modele;

	private DateTime c_cdpr_d_misecorbeille;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_corbeille_deplacement(clg_Modele pModele, Int64 pdpr_cn, bool pAMAJ = false) : base(pModele, pdpr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_corbeille_deplacement(clg_Modele pModele, Int64 pdeplacements_realises, DateTime pcdpr_d_misecorbeille, clg_vehicules pvehicules, DateTime pdpr_d_date, double pdpr_n_nbkm, double pdpr_n_kmmin, double pdpr_n_kmmax, double pdpr_n_montant, clg_taux_km_reels ptaux_km_reels, string pdpr_a_dest, string pdpr_a_utilisateur, string pdpr_a_codeana, string pdpr_a_objet, bool pAMAJ = true) : base(pModele, pdeplacements_realises, pvehicules, pdpr_d_date, pdpr_n_nbkm, pdpr_n_kmmin, pdpr_n_kmmax, pdpr_n_montant, ptaux_km_reels, pdpr_a_dest, pdpr_a_utilisateur, pdpr_a_codeana, pdpr_a_objet, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        c_cdpr_d_misecorbeille = pcdpr_d_misecorbeille;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pdeplacements_realises, DateTime pcdpr_d_misecorbeille, Int64 pvehicules, DateTime pdpr_d_date, double pdpr_n_nbkm, double pdpr_n_kmmin, double pdpr_n_kmmax, double pdpr_n_montant, Int64 ptaux_km_reels, string pdpr_a_dest, string pdpr_a_utilisateur, string pdpr_a_codeana, string pdpr_a_objet)
    {   
		        c_cdpr_d_misecorbeille = pcdpr_d_misecorbeille;

        base.Initialise(pdeplacements_realises, pvehicules, pdpr_d_date, pdpr_n_nbkm, pdpr_n_kmmin, pdpr_n_kmmax, pdpr_n_montant, ptaux_km_reels, pdpr_a_dest, pdpr_a_utilisateur, pdpr_a_codeana, pdpr_a_objet);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcorbeille_deplacement; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		base.CreeLiens();

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecorbeille_deplacement.Dictionnaire.Add(dpr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecorbeille_deplacement.Dictionnaire.Remove(dpr_cn);
		base.SupprimeDansListe();

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		base.CreeReferences();

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_corbeille_deplacement l_Clone = (clg_corbeille_deplacement) Clone;
		base.DetruitReferences();

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_corbeille_deplacement(null, dpr_cn, cdpr_d_misecorbeille, vehicules, dpr_d_date, dpr_n_nbkm, dpr_n_kmmin, dpr_n_kmmax, dpr_n_montant, taux_km_reels, dpr_a_dest, dpr_a_utilisateur, dpr_a_codeana, dpr_a_objet,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_corbeille_deplacement l_clone = (clg_corbeille_deplacement) this.Clone;
		c_cdpr_d_misecorbeille = l_clone.cdpr_d_misecorbeille;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcorbeille_deplacement.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcorbeille_deplacement.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcorbeille_deplacement.Delete(this);
    }

    /* Accesseur de la propriete cdpr_d_misecorbeille (cdpr_d_misecorbeille)
    * @return c_cdpr_d_misecorbeille */
    public DateTime cdpr_d_misecorbeille
    {
        get{return c_cdpr_d_misecorbeille;}
        set
        {
            if(c_cdpr_d_misecorbeille != value)
            {
                CreerClone();
                c_cdpr_d_misecorbeille = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cdpr_d_misecorbeille.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("deplacements_realises");
		l_Noms.Add("cdpr_d_misecorbeille");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
