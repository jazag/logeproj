namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : import_sage </summary>
public partial class clg_import_sage : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLimport_sage;
    protected clg_Modele c_Modele;

	private Int64 c_ims_cn;
	private Int64 c_ims_base_cn;
	private Int64 c_ims_n_numlig;
	private Int64 c_ims_n_operation;
	private Int64 c_ims_n_ordre;
	private string c_ims_jou_a_code;
	private DateTime c_ims_d_piece;
	private string c_ims_a_cptgen;
	private string c_ims_a_libcpt;
	private string c_ims_n_piece;
	private string c_ims_n_fact;
	private string c_ims_a_ref;
	private string c_ims_a_refrapp;
	private string c_ims_n_cpttiers;
	private string c_ims_a_libec;
	private string c_ims_a_sens;
	private double c_ims_n_mtt;
	private string c_ims_t_ec;
	private Int64 c_ims_n_planana;
	private string c_ims_n_section;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_import_sage(clg_Modele pModele, Int64 pims_cn, bool pAMAJ = false) : base(pModele, pims_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_import_sage(clg_Modele pModele, Int64 pims_cn, Int64 pims_base_cn, Int64 pims_n_numlig, Int64 pims_n_operation, Int64 pims_n_ordre, string pims_jou_a_code, DateTime pims_d_piece, string pims_a_cptgen, string pims_a_libcpt, string pims_n_piece, string pims_n_fact, string pims_a_ref, string pims_a_refrapp, string pims_n_cpttiers, string pims_a_libec, string pims_a_sens, double pims_n_mtt, string pims_t_ec, Int64 pims_n_planana, string pims_n_section, bool pAMAJ = true) : base(pModele, pims_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pims_cn != Int64.MinValue)
            c_ims_cn = pims_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ims_base_cn = pims_base_cn;
		        if(pims_n_numlig != Int64.MinValue)
            c_ims_n_numlig = pims_n_numlig;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ims_n_operation = pims_n_operation;
		        c_ims_n_ordre = pims_n_ordre;
		        c_ims_jou_a_code = pims_jou_a_code;
		        c_ims_d_piece = pims_d_piece;
		        c_ims_a_cptgen = pims_a_cptgen;
		        c_ims_a_libcpt = pims_a_libcpt;
		        c_ims_n_piece = pims_n_piece;
		        c_ims_n_fact = pims_n_fact;
		        c_ims_a_ref = pims_a_ref;
		        c_ims_a_refrapp = pims_a_refrapp;
		        c_ims_n_cpttiers = pims_n_cpttiers;
		        c_ims_a_libec = pims_a_libec;
		        c_ims_a_sens = pims_a_sens;
		        c_ims_n_mtt = pims_n_mtt;
		        c_ims_t_ec = pims_t_ec;
		        c_ims_n_planana = pims_n_planana;
		        c_ims_n_section = pims_n_section;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pims_cn, Int64 pims_base_cn, Int64 pims_n_numlig, Int64 pims_n_operation, Int64 pims_n_ordre, string pims_jou_a_code, DateTime pims_d_piece, string pims_a_cptgen, string pims_a_libcpt, string pims_n_piece, string pims_n_fact, string pims_a_ref, string pims_a_refrapp, string pims_n_cpttiers, string pims_a_libec, string pims_a_sens, double pims_n_mtt, string pims_t_ec, Int64 pims_n_planana, string pims_n_section)
    {   
		        c_ims_cn = pims_cn;
		        c_ims_base_cn = pims_base_cn;
		        c_ims_n_numlig = pims_n_numlig;
		        c_ims_n_operation = pims_n_operation;
		        c_ims_n_ordre = pims_n_ordre;
		        c_ims_jou_a_code = pims_jou_a_code;
		        c_ims_d_piece = pims_d_piece;
		        c_ims_a_cptgen = pims_a_cptgen;
		        c_ims_a_libcpt = pims_a_libcpt;
		        c_ims_n_piece = pims_n_piece;
		        c_ims_n_fact = pims_n_fact;
		        c_ims_a_ref = pims_a_ref;
		        c_ims_a_refrapp = pims_a_refrapp;
		        c_ims_n_cpttiers = pims_n_cpttiers;
		        c_ims_a_libec = pims_a_libec;
		        c_ims_a_sens = pims_a_sens;
		        c_ims_n_mtt = pims_n_mtt;
		        c_ims_t_ec = pims_t_ec;
		        c_ims_n_planana = pims_n_planana;
		        c_ims_n_section = pims_n_section;

        base.Initialise(pims_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLimport_sage; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeimport_sage.Dictionnaire.Add(ims_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeimport_sage.Dictionnaire.Remove(ims_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_import_sage l_Clone = (clg_import_sage) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_import_sage(null, ims_cn, ims_base_cn, ims_n_numlig, ims_n_operation, ims_n_ordre, ims_jou_a_code, ims_d_piece, ims_a_cptgen, ims_a_libcpt, ims_n_piece, ims_n_fact, ims_a_ref, ims_a_refrapp, ims_n_cpttiers, ims_a_libec, ims_a_sens, ims_n_mtt, ims_t_ec, ims_n_planana, ims_n_section,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_import_sage l_clone = (clg_import_sage) this.Clone;
		c_ims_cn = l_clone.ims_cn;
		c_ims_base_cn = l_clone.ims_base_cn;
		c_ims_n_numlig = l_clone.ims_n_numlig;
		c_ims_n_operation = l_clone.ims_n_operation;
		c_ims_n_ordre = l_clone.ims_n_ordre;
		c_ims_jou_a_code = l_clone.ims_jou_a_code;
		c_ims_d_piece = l_clone.ims_d_piece;
		c_ims_a_cptgen = l_clone.ims_a_cptgen;
		c_ims_a_libcpt = l_clone.ims_a_libcpt;
		c_ims_n_piece = l_clone.ims_n_piece;
		c_ims_n_fact = l_clone.ims_n_fact;
		c_ims_a_ref = l_clone.ims_a_ref;
		c_ims_a_refrapp = l_clone.ims_a_refrapp;
		c_ims_n_cpttiers = l_clone.ims_n_cpttiers;
		c_ims_a_libec = l_clone.ims_a_libec;
		c_ims_a_sens = l_clone.ims_a_sens;
		c_ims_n_mtt = l_clone.ims_n_mtt;
		c_ims_t_ec = l_clone.ims_t_ec;
		c_ims_n_planana = l_clone.ims_n_planana;
		c_ims_n_section = l_clone.ims_n_section;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLimport_sage.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLimport_sage.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLimport_sage.Delete(this);
    }

    /* Accesseur de la propriete ims_cn (ims_cn)
    * @return c_ims_cn */
    public Int64 ims_cn
    {
        get{return c_ims_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ims_cn != value)
                {
                    CreerClone();
                    c_ims_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ims_base_cn (ims_base_cn)
    * @return c_ims_base_cn */
    public Int64 ims_base_cn
    {
        get{return c_ims_base_cn;}
        set
        {
            if(c_ims_base_cn != value)
            {
                CreerClone();
                c_ims_base_cn = value;
            }
        }
    }
    /* Accesseur de la propriete ims_n_numlig (ims_n_numlig)
    * @return c_ims_n_numlig */
    public Int64 ims_n_numlig
    {
        get{return c_ims_n_numlig;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ims_n_numlig != value)
                {
                    CreerClone();
                    c_ims_n_numlig = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ims_n_operation (ims_n_operation)
    * @return c_ims_n_operation */
    public Int64 ims_n_operation
    {
        get{return c_ims_n_operation;}
        set
        {
            if(c_ims_n_operation != value)
            {
                CreerClone();
                c_ims_n_operation = value;
            }
        }
    }
    /* Accesseur de la propriete ims_n_ordre (ims_n_ordre)
    * @return c_ims_n_ordre */
    public Int64 ims_n_ordre
    {
        get{return c_ims_n_ordre;}
        set
        {
            if(c_ims_n_ordre != value)
            {
                CreerClone();
                c_ims_n_ordre = value;
            }
        }
    }
    /* Accesseur de la propriete ims_jou_a_code (ims_jou_a_code)
    * @return c_ims_jou_a_code */
    public string ims_jou_a_code
    {
        get{return c_ims_jou_a_code;}
        set
        {
            if(c_ims_jou_a_code != value)
            {
                CreerClone();
                c_ims_jou_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete ims_d_piece (ims_d_piece)
    * @return c_ims_d_piece */
    public DateTime ims_d_piece
    {
        get{return c_ims_d_piece;}
        set
        {
            if(c_ims_d_piece != value)
            {
                CreerClone();
                c_ims_d_piece = value;
            }
        }
    }
    /* Accesseur de la propriete ims_a_cptgen (ims_a_cptgen)
    * @return c_ims_a_cptgen */
    public string ims_a_cptgen
    {
        get{return c_ims_a_cptgen;}
        set
        {
            if(c_ims_a_cptgen != value)
            {
                CreerClone();
                c_ims_a_cptgen = value;
            }
        }
    }
    /* Accesseur de la propriete ims_a_libcpt (ims_a_libcpt)
    * @return c_ims_a_libcpt */
    public string ims_a_libcpt
    {
        get{return c_ims_a_libcpt;}
        set
        {
            if(c_ims_a_libcpt != value)
            {
                CreerClone();
                c_ims_a_libcpt = value;
            }
        }
    }
    /* Accesseur de la propriete ims_n_piece (ims_n_piece)
    * @return c_ims_n_piece */
    public string ims_n_piece
    {
        get{return c_ims_n_piece;}
        set
        {
            if(c_ims_n_piece != value)
            {
                CreerClone();
                c_ims_n_piece = value;
            }
        }
    }
    /* Accesseur de la propriete ims_n_fact (ims_n_fact)
    * @return c_ims_n_fact */
    public string ims_n_fact
    {
        get{return c_ims_n_fact;}
        set
        {
            if(c_ims_n_fact != value)
            {
                CreerClone();
                c_ims_n_fact = value;
            }
        }
    }
    /* Accesseur de la propriete ims_a_ref (ims_a_ref)
    * @return c_ims_a_ref */
    public string ims_a_ref
    {
        get{return c_ims_a_ref;}
        set
        {
            if(c_ims_a_ref != value)
            {
                CreerClone();
                c_ims_a_ref = value;
            }
        }
    }
    /* Accesseur de la propriete ims_a_refrapp (ims_a_refrapp)
    * @return c_ims_a_refrapp */
    public string ims_a_refrapp
    {
        get{return c_ims_a_refrapp;}
        set
        {
            if(c_ims_a_refrapp != value)
            {
                CreerClone();
                c_ims_a_refrapp = value;
            }
        }
    }
    /* Accesseur de la propriete ims_n_cpttiers (ims_n_cpttiers)
    * @return c_ims_n_cpttiers */
    public string ims_n_cpttiers
    {
        get{return c_ims_n_cpttiers;}
        set
        {
            if(c_ims_n_cpttiers != value)
            {
                CreerClone();
                c_ims_n_cpttiers = value;
            }
        }
    }
    /* Accesseur de la propriete ims_a_libec (ims_a_libec)
    * @return c_ims_a_libec */
    public string ims_a_libec
    {
        get{return c_ims_a_libec;}
        set
        {
            if(c_ims_a_libec != value)
            {
                CreerClone();
                c_ims_a_libec = value;
            }
        }
    }
    /* Accesseur de la propriete ims_a_sens (ims_a_sens)
    * @return c_ims_a_sens */
    public string ims_a_sens
    {
        get{return c_ims_a_sens;}
        set
        {
            if(c_ims_a_sens != value)
            {
                CreerClone();
                c_ims_a_sens = value;
            }
        }
    }
    /* Accesseur de la propriete ims_n_mtt (ims_n_mtt)
    * @return c_ims_n_mtt */
    public double ims_n_mtt
    {
        get{return c_ims_n_mtt;}
        set
        {
            if(c_ims_n_mtt != value)
            {
                CreerClone();
                c_ims_n_mtt = value;
            }
        }
    }
    /* Accesseur de la propriete ims_t_ec (ims_t_ec)
    * @return c_ims_t_ec */
    public string ims_t_ec
    {
        get{return c_ims_t_ec;}
        set
        {
            if(c_ims_t_ec != value)
            {
                CreerClone();
                c_ims_t_ec = value;
            }
        }
    }
    /* Accesseur de la propriete ims_n_planana (ims_n_planana)
    * @return c_ims_n_planana */
    public Int64 ims_n_planana
    {
        get{return c_ims_n_planana;}
        set
        {
            if(c_ims_n_planana != value)
            {
                CreerClone();
                c_ims_n_planana = value;
            }
        }
    }
    /* Accesseur de la propriete ims_n_section (ims_n_section)
    * @return c_ims_n_section */
    public string ims_n_section
    {
        get{return c_ims_n_section;}
        set
        {
            if(c_ims_n_section != value)
            {
                CreerClone();
                c_ims_n_section = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ims_cn.ToString());
		l_Valeurs.Add(c_ims_base_cn.ToString());
		l_Valeurs.Add(c_ims_n_numlig.ToString());
		l_Valeurs.Add(c_ims_n_operation.ToString());
		l_Valeurs.Add(c_ims_n_ordre.ToString());
		l_Valeurs.Add(c_ims_jou_a_code.ToString());
		l_Valeurs.Add(c_ims_d_piece.ToString());
		l_Valeurs.Add(c_ims_a_cptgen.ToString());
		l_Valeurs.Add(c_ims_a_libcpt.ToString());
		l_Valeurs.Add(c_ims_n_piece.ToString());
		l_Valeurs.Add(c_ims_n_fact.ToString());
		l_Valeurs.Add(c_ims_a_ref.ToString());
		l_Valeurs.Add(c_ims_a_refrapp.ToString());
		l_Valeurs.Add(c_ims_n_cpttiers.ToString());
		l_Valeurs.Add(c_ims_a_libec.ToString());
		l_Valeurs.Add(c_ims_a_sens.ToString());
		l_Valeurs.Add(c_ims_n_mtt.ToString());
		l_Valeurs.Add(c_ims_t_ec.ToString());
		l_Valeurs.Add(c_ims_n_planana.ToString());
		l_Valeurs.Add(c_ims_n_section.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ims_cn");
		l_Noms.Add("ims_base_cn");
		l_Noms.Add("ims_n_numlig");
		l_Noms.Add("ims_n_operation");
		l_Noms.Add("ims_n_ordre");
		l_Noms.Add("ims_jou_a_code");
		l_Noms.Add("ims_d_piece");
		l_Noms.Add("ims_a_cptgen");
		l_Noms.Add("ims_a_libcpt");
		l_Noms.Add("ims_n_piece");
		l_Noms.Add("ims_n_fact");
		l_Noms.Add("ims_a_ref");
		l_Noms.Add("ims_a_refrapp");
		l_Noms.Add("ims_n_cpttiers");
		l_Noms.Add("ims_a_libec");
		l_Noms.Add("ims_a_sens");
		l_Noms.Add("ims_n_mtt");
		l_Noms.Add("ims_t_ec");
		l_Noms.Add("ims_n_planana");
		l_Noms.Add("ims_n_section");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
