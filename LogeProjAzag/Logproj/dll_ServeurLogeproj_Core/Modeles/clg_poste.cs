namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : poste </summary>
public partial class clg_poste : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLposte;
    protected clg_Modele c_Modele;

	private Int64 c_pst_cn;
	private string c_pst_a_guid;
	private clg_infos_base c_infos_base;
	private string c_pst_a_nom;
	private string c_pst_a_ip;
	private DateTime c_pst_d_creation;
	private DateTime c_pst_d_der_connexion;
	private DateTime c_pst_d_der_sdv;
	private Int64 c_pst_n_connecte;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_infos_base = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_poste(clg_Modele pModele, Int64 ppst_cn, bool pAMAJ = false) : base(pModele, ppst_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_poste(clg_Modele pModele, Int64 ppst_cn, string ppst_a_guid, clg_infos_base pinfos_base, string ppst_a_nom, string ppst_a_ip, DateTime ppst_d_creation, DateTime ppst_d_der_connexion, DateTime ppst_d_der_sdv, Int64 ppst_n_connecte, bool pAMAJ = true) : base(pModele, ppst_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ppst_cn != Int64.MinValue)
            c_pst_cn = ppst_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(ppst_a_guid != null)
            c_pst_a_guid = ppst_a_guid;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pinfos_base != null)
            c_infos_base = pinfos_base;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(ppst_a_nom != null)
            c_pst_a_nom = ppst_a_nom;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_pst_a_ip = ppst_a_ip;
		        c_pst_d_creation = ppst_d_creation;
		        c_pst_d_der_connexion = ppst_d_der_connexion;
		        c_pst_d_der_sdv = ppst_d_der_sdv;
		        c_pst_n_connecte = ppst_n_connecte;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ppst_cn, string ppst_a_guid, Int64 pinfos_base, string ppst_a_nom, string ppst_a_ip, DateTime ppst_d_creation, DateTime ppst_d_der_connexion, DateTime ppst_d_der_sdv, Int64 ppst_n_connecte)
    {   
		        c_pst_cn = ppst_cn;
		        c_pst_a_guid = ppst_a_guid;
		c_dicReferences.Add("infos_base", pinfos_base);
		        c_pst_a_nom = ppst_a_nom;
		        c_pst_a_ip = ppst_a_ip;
		        c_pst_d_creation = ppst_d_creation;
		        c_pst_d_der_connexion = ppst_d_der_connexion;
		        c_pst_d_der_sdv = ppst_d_der_sdv;
		        c_pst_n_connecte = ppst_n_connecte;

        base.Initialise(ppst_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLposte; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_infos_base = (clg_infos_base) c_ModeleBase.RenvoieObjet(c_dicReferences["infos_base"], "clg_infos_base");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeposte.Dictionnaire.Add(pst_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeposte.Dictionnaire.Remove(pst_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_infos_base != null)if(!c_infos_base.Listeposte.Contains(this)) c_infos_base.Listeposte.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_poste l_Clone = (clg_poste) Clone;
		if(l_Clone.infos_base != null)if(l_Clone.infos_base.Listeposte.Contains(this)) l_Clone.infos_base.Listeposte.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_poste(null, pst_cn, pst_a_guid, infos_base, pst_a_nom, pst_a_ip, pst_d_creation, pst_d_der_connexion, pst_d_der_sdv, pst_n_connecte,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_poste l_clone = (clg_poste) this.Clone;
		c_pst_cn = l_clone.pst_cn;
		c_pst_a_guid = l_clone.pst_a_guid;
		c_infos_base = l_clone.infos_base;
		c_pst_a_nom = l_clone.pst_a_nom;
		c_pst_a_ip = l_clone.pst_a_ip;
		c_pst_d_creation = l_clone.pst_d_creation;
		c_pst_d_der_connexion = l_clone.pst_d_der_connexion;
		c_pst_d_der_sdv = l_clone.pst_d_der_sdv;
		c_pst_n_connecte = l_clone.pst_n_connecte;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLposte.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLposte.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLposte.Delete(this);
    }

    /* Accesseur de la propriete pst_cn (pst_cn)
    * @return c_pst_cn */
    public Int64 pst_cn
    {
        get{return c_pst_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_pst_cn != value)
                {
                    CreerClone();
                    c_pst_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete pst_a_guid (pst_a_guid)
    * @return c_pst_a_guid */
    public string pst_a_guid
    {
        get{return c_pst_a_guid;}
        set
        {
            if(value != null)
            {
                if(c_pst_a_guid != value)
                {
                    CreerClone();
                    c_pst_a_guid = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete infos_base (pst_ifb_cn)
    * @return c_infos_base */
    public clg_infos_base infos_base
    {
        get{return c_infos_base;}
        set
        {
            if(value != null)
            {
                if(c_infos_base != value)
                {
                    CreerClone();
                    c_infos_base = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete pst_a_nom (pst_a_nom)
    * @return c_pst_a_nom */
    public string pst_a_nom
    {
        get{return c_pst_a_nom;}
        set
        {
            if(value != null)
            {
                if(c_pst_a_nom != value)
                {
                    CreerClone();
                    c_pst_a_nom = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete pst_a_ip (pst_a_ip)
    * @return c_pst_a_ip */
    public string pst_a_ip
    {
        get{return c_pst_a_ip;}
        set
        {
            if(c_pst_a_ip != value)
            {
                CreerClone();
                c_pst_a_ip = value;
            }
        }
    }
    /* Accesseur de la propriete pst_d_creation (pst_d_creation)
    * @return c_pst_d_creation */
    public DateTime pst_d_creation
    {
        get{return c_pst_d_creation;}
        set
        {
            if(c_pst_d_creation != value)
            {
                CreerClone();
                c_pst_d_creation = value;
            }
        }
    }
    /* Accesseur de la propriete pst_d_der_connexion (pst_d_der_connexion)
    * @return c_pst_d_der_connexion */
    public DateTime pst_d_der_connexion
    {
        get{return c_pst_d_der_connexion;}
        set
        {
            if(c_pst_d_der_connexion != value)
            {
                CreerClone();
                c_pst_d_der_connexion = value;
            }
        }
    }
    /* Accesseur de la propriete pst_d_der_sdv (pst_d_der_sdv)
    * @return c_pst_d_der_sdv */
    public DateTime pst_d_der_sdv
    {
        get{return c_pst_d_der_sdv;}
        set
        {
            if(c_pst_d_der_sdv != value)
            {
                CreerClone();
                c_pst_d_der_sdv = value;
            }
        }
    }
    /* Accesseur de la propriete pst_n_connecte (pst_n_connecte)
    * @return c_pst_n_connecte */
    public Int64 pst_n_connecte
    {
        get{return c_pst_n_connecte;}
        set
        {
            if(c_pst_n_connecte != value)
            {
                CreerClone();
                c_pst_n_connecte = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_pst_cn.ToString());
		l_Valeurs.Add(c_pst_a_guid.ToString());
		l_Valeurs.Add(c_infos_base==null ? "-1" : c_infos_base.ID.ToString());
		l_Valeurs.Add(c_pst_a_nom.ToString());
		l_Valeurs.Add(c_pst_a_ip.ToString());
		l_Valeurs.Add(c_pst_d_creation.ToString());
		l_Valeurs.Add(c_pst_d_der_connexion.ToString());
		l_Valeurs.Add(c_pst_d_der_sdv.ToString());
		l_Valeurs.Add(c_pst_n_connecte.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("pst_cn");
		l_Noms.Add("pst_a_guid");
		l_Noms.Add("infos_base");
		l_Noms.Add("pst_a_nom");
		l_Noms.Add("pst_a_ip");
		l_Noms.Add("pst_d_creation");
		l_Noms.Add("pst_d_der_connexion");
		l_Noms.Add("pst_d_der_sdv");
		l_Noms.Add("pst_n_connecte");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
