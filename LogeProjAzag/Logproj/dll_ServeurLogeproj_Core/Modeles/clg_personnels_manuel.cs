﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dll_ServeurLogeproj_Core
{
    public partial class clg_personnels
    {
        public String ToJson()
        {
            StringBuilder l_sb = new StringBuilder();
            StringWriter l_sw = new StringWriter(l_sb);

            using (JsonWriter l_writer = new JsonTextWriter(l_sw))
            {
                l_writer.Formatting = Formatting.Indented;
                l_writer.WriteStartObject();

                l_writer.WritePropertyName("c_per_cn");
                l_writer.WriteValue(c_per_cn);

                l_writer.WritePropertyName("c_per_a_nom");
                l_writer.WriteValue(c_per_a_nom);

                l_writer.WritePropertyName("c_per_a_prenom");
                l_writer.WriteValue(c_per_a_prenom);

                l_writer.WriteEndObject();
            }
            return l_sb.ToString();
        }
    }
}
