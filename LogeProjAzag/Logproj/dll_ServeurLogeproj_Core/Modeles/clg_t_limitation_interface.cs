namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_limitation_interface </summary>
public partial class clg_t_limitation_interface : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_limitation_interface;
    protected clg_Modele c_Modele;

	private Int64 c_t_lmi_cn;
	private string c_t_lmi_a_libel;
	private string c_t_lmi_m_comment;
	private List<clg_limitation_interface> c_Listelimitation_interface;


    private void Init()
    {
		c_Listelimitation_interface = new List<clg_limitation_interface>();

    }

    public override void Detruit()
    {
		c_Listelimitation_interface.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_limitation_interface(clg_Modele pModele, Int64 pt_lmi_cn, bool pAMAJ = false) : base(pModele, pt_lmi_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_limitation_interface(clg_Modele pModele, Int64 pt_lmi_cn, string pt_lmi_a_libel, string pt_lmi_m_comment, bool pAMAJ = true) : base(pModele, pt_lmi_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_lmi_cn != Int64.MinValue)
            c_t_lmi_cn = pt_lmi_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pt_lmi_a_libel != null)
            c_t_lmi_a_libel = pt_lmi_a_libel;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_t_lmi_m_comment = pt_lmi_m_comment;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_lmi_cn, string pt_lmi_a_libel, string pt_lmi_m_comment)
    {   
		        c_t_lmi_cn = pt_lmi_cn;
		        c_t_lmi_a_libel = pt_lmi_a_libel;
		        c_t_lmi_m_comment = pt_lmi_m_comment;

        base.Initialise(pt_lmi_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_limitation_interface; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_limitation_interface.Dictionnaire.Add(t_lmi_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_limitation_interface.Dictionnaire.Remove(t_lmi_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_limitation_interface l_Clone = (clg_t_limitation_interface) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listelimitation_interface.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_limitation_interface(null, t_lmi_cn, t_lmi_a_libel, t_lmi_m_comment,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_limitation_interface l_clone = (clg_t_limitation_interface) this.Clone;
		c_t_lmi_cn = l_clone.t_lmi_cn;
		c_t_lmi_a_libel = l_clone.t_lmi_a_libel;
		c_t_lmi_m_comment = l_clone.t_lmi_m_comment;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_limitation_interface.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_limitation_interface.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_limitation_interface.Delete(this);
    }

    /* Accesseur de la propriete t_lmi_cn (t_lmi_cn)
    * @return c_t_lmi_cn */
    public Int64 t_lmi_cn
    {
        get{return c_t_lmi_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_lmi_cn != value)
                {
                    CreerClone();
                    c_t_lmi_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_lmi_a_libel (t_lmi_a_libel)
    * @return c_t_lmi_a_libel */
    public string t_lmi_a_libel
    {
        get{return c_t_lmi_a_libel;}
        set
        {
            if(value != null)
            {
                if(c_t_lmi_a_libel != value)
                {
                    CreerClone();
                    c_t_lmi_a_libel = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_lmi_m_comment (t_lmi_m_comment)
    * @return c_t_lmi_m_comment */
    public string t_lmi_m_comment
    {
        get{return c_t_lmi_m_comment;}
        set
        {
            if(c_t_lmi_m_comment != value)
            {
                CreerClone();
                c_t_lmi_m_comment = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type limitation_interface */
    public List<clg_limitation_interface> Listelimitation_interface
    {
        get { return c_Listelimitation_interface; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_lmi_cn.ToString());
		l_Valeurs.Add(c_t_lmi_a_libel.ToString());
		l_Valeurs.Add(c_t_lmi_m_comment.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_lmi_cn");
		l_Noms.Add("t_lmi_a_libel");
		l_Noms.Add("t_lmi_m_comment");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
