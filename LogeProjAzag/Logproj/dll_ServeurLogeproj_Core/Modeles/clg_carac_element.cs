namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
    using System;
    using clg_ReflexionV3_Core;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Globalization;

    /// <summary> Classe modele de la table : carac_element </summary>
    public partial class clg_carac_element : clg_ObjetBase
    {
        private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
        public static clg_ControleurBase c_CTLcarac_element;
        protected clg_Modele c_Modele;

        private Int64 c_cel_cn;
        private clg_elements c_elements;
        private clg_carac c_carac;
        private string c_cel_a_val;
        private string c_cel_m_val;
        private bool appartiensAuProjet = false;
        private DateTime c_cel_date_modif;

        private void Init()
        {

        }



        public override void Detruit()
        {
            this.c_elements = null;
            this.c_carac = null;

        }

        /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
        public clg_carac_element(clg_Modele pModele, Int64 pcel_cn, bool pAMAJ = false) : base(pModele, pcel_cn, pAMAJ)
        {
            Init();
            c_Modele = pModele;
        }

        /// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
        public clg_carac_element(clg_Modele pModele, Int64 pcel_cn, clg_elements pelements, clg_carac pcarac, string pcel_a_val, string pcel_m_val, DateTime pcel_date_modif, bool pAMAJ = true) : base(pModele, pcel_cn, pAMAJ)
        {
            c_Modele = pModele;
            Init();
            if (pcel_cn != Int64.MinValue)
                c_cel_cn = pcel_cn;
            else
                throw new System.InvalidOperationException("MinValue interdite");

            if (pelements != null)
                c_elements = pelements;
            else
                throw new System.InvalidOperationException("Valeur nulle interdite");

            if (pcarac != null)
                c_carac = pcarac;
            else
                throw new System.InvalidOperationException("Valeur nulle interdite");

            c_cel_a_val = pcel_a_val;

            c_cel_m_val = pcel_m_val;

            c_cel_date_modif = pcel_date_modif;


            if (pAMAJ) c_Modele.AjouteObjetAMAJ(this);
        }

        /// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
        public void Initialise(Int64 pcel_cn, Int64 pelements, Int64 pcarac, string pcel_a_val, string pcel_m_val, DateTime pcel_date_modif)
        {
            c_cel_cn = pcel_cn;

            c_dicReferences.Add("elements", pelements);
            c_dicReferences.Add("carac", pcarac);
            c_cel_a_val = pcel_a_val;


            c_cel_m_val = pcel_m_val;

            c_cel_date_modif = pcel_date_modif;

            base.Initialise(pcel_cn);
        }

        /// <summary> Accesseur vers le controleur de cette classe </summary>
        public override clg_ControleurBase Controleur
        {
            get { return c_CTLcarac_element; }
        }

        /// <summary> Instanciation des proprietes referencant d'autres objets </summary>
        public override void CreeLiens()
        {
            c_elements = (clg_elements)c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");
            c_carac = (clg_carac)c_ModeleBase.RenvoieObjet(c_dicReferences["carac"], "clg_carac");

        }

        /// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
        public override void AjouteDansListe()
        {
            c_Modele.Listecarac_element.Dictionnaire.Add(cel_cn, this);
        }

        /// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
        public override void SupprimeDansListe()
        {
            c_Modele.Listecarac_element.Dictionnaire.Remove(cel_cn);

        }

        /// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
        public override void CreeReferences()
        {
            if (c_elements != null) if (!c_elements.Listecarac_element.Contains(this)) c_elements.Listecarac_element.Add(this);
            if (c_carac != null) if (!c_carac.Listecarac_element.Contains(this)) c_carac.Listecarac_element.Add(this);

        }

        /// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
        public override void DetruitReferences()
        {
            if (Clone != null)
            {
                clg_carac_element l_Clone = (clg_carac_element)Clone;
                if (l_Clone.elements != null) if (l_Clone.elements.Listecarac_element.Contains(this)) l_Clone.elements.Listecarac_element.Remove(this);
                if (l_Clone.carac != null) if (l_Clone.carac.Listecarac_element.Contains(this)) l_Clone.carac.Listecarac_element.Remove(this);

            }
        }

        /// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
        protected override void AnalyseReferences()
        {
            base.AnalyseReferences();

        }

        /// <summary> Creation d'un clone de l'objet </summary>
        protected override void CreerClone()
        {
            if (base.Clone == null)
            {
                base.Clone = Cloner();
                c_Modele.AjouteObjetAMAJ(this);
            }
        }

        /// <summary> Creation d'un clone de l'objet </summary>
        public override clg_ObjetBase Cloner()
        {
            return new clg_carac_element(null, cel_cn, elements, carac, cel_a_val, cel_m_val, cel_date_modif, false);
        }

        /// <summary> Annulation des modifications sur l'objet </summary>
        public override void AnnuleModification()
        {
            clg_carac_element l_clone = (clg_carac_element)this.Clone;
            c_cel_cn = l_clone.cel_cn;
            c_elements = l_clone.elements;
            c_carac = l_clone.carac;
            c_cel_a_val = l_clone.cel_a_val;
            c_cel_m_val = l_clone.cel_m_val;
            c_cel_date_modif = l_clone.cel_date_modif;

        }

        public override void InsereEnBase()
        {
            base.InsereEnBase();
            c_CTLcarac_element.Insert(this);
        }

        public override void MAJEnBase()
        {
            base.MAJEnBase();
            c_CTLcarac_element.Update(this);
        }

        public override void SupprimeEnBase()
        {
            base.SupprimeEnBase();
            c_CTLcarac_element.Delete(this);
        }

        /* Accesseur de la propriete cel_cn (cel_cn)
        * @return c_cel_cn */
        public Int64 cel_cn
        {
            get { return c_cel_cn; }

            set
            {
                if (value != Int64.MinValue)
                {
                    if (c_cel_cn != value)
                    {
                        CreerClone();
                        c_cel_cn = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur MinValue interdite");
                }
            }
        }

        /* Accesseur de la propriete cel_cn (cel_date_modif)
       * @return c_cel_date_modif */
        public DateTime cel_date_modif
        {
            get { return c_cel_date_modif; }

            set
            {
                if (value != DateTime.MinValue)
                {
                    if (c_cel_date_modif != value)
                    {
                        CreerClone();
                        c_cel_date_modif = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur MinValue interdite");
                }
            }
        }

        /* Accesseur de la propriete elements (cel_ele_cn)
        * @return c_elements */
        public clg_elements elements
        {
            get { return c_elements; }

            set
            {
                if (value != null)
                {
                    if (c_elements != value)
                    {
                        CreerClone();
                        c_elements = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur nulle interdite");
                }
            }
        }

        /* Accesseur de la propriete carac (cel_car_cn)
        * @return c_carac */
        public clg_carac carac
        {
            get { return c_carac; }

            set
            {
                if (value != null)
                {
                    if (c_carac != value)
                    {
                        CreerClone();
                        c_carac = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur nulle interdite");
                }
            }
        }

        public bool appartientAuProjet
        {
            get { return appartiensAuProjet; }
            set { appartiensAuProjet = value; }
        }

        /* Accesseur de la propriete cel_a_val (cel_a_val)
        * @return c_cel_a_val */
        public string cel_a_val
        {
            get { return c_cel_a_val; }

            set
            {
                if (c_cel_a_val != value)
                {
                    CreerClone();
                    c_cel_a_val = value;
                }
            }
        }

        /* Accesseur de la propriete cel_m_val (cel_m_val)
        * @return c_cel_m_val */
        public string cel_m_val
        {
            get { return c_cel_m_val; }

            set
            {
                if (c_cel_m_val != value)
                {
                    CreerClone();
                    c_cel_m_val = value;
                }
            }
        }



        public override List<string> ListeValeursProprietes()
        {
            List<string> l_Valeurs = new List<string>();
            l_Valeurs.Add(c_cel_cn.ToString());
            l_Valeurs.Add(c_elements == null ? "-1" : c_elements.ID.ToString());
            l_Valeurs.Add(c_carac == null ? "-1" : c_carac.ID.ToString());
            l_Valeurs.Add(c_cel_a_val.ToString());
            l_Valeurs.Add(c_cel_m_val.ToString());
            string a = c_cel_date_modif.ToString();
            l_Valeurs.Add(c_cel_date_modif.ToString());

            l_Valeurs.AddRange(base.ListeValeursProprietes());
            return l_Valeurs;
        }

        public override List<string> ListeNomsProprietes()
        {
            List<string> l_Noms = new List<string>();
            l_Noms.Add("cel_cn");
            l_Noms.Add("elements");
            l_Noms.Add("carac");
            l_Noms.Add("cel_a_val");
            l_Noms.Add("cel_m_val");
            l_Noms.Add("cel_date_modif");

            l_Noms.AddRange(base.ListeNomsProprietes());
            return l_Noms;
        }

        #endregion
    }
}
