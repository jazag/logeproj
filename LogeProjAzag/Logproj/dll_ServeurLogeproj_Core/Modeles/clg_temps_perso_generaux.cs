namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : temps_perso_generaux </summary>
public partial class clg_temps_perso_generaux : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtemps_perso_generaux;
    protected clg_Modele c_Modele;

	private Int64 c_tpg_cn;
	private clg_personnels c_personnels;
	private double c_tpg_n_heures;
	private DateTime c_tpg_d_date;
	private clg_rubriques_temps c_rubriques_temps;
	private Int64 c_tpg_fct_cn;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_personnels = null;
		this.c_rubriques_temps = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_temps_perso_generaux(clg_Modele pModele, Int64 ptpg_cn, bool pAMAJ = false) : base(pModele, ptpg_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_temps_perso_generaux(clg_Modele pModele, Int64 ptpg_cn, clg_personnels ppersonnels, double ptpg_n_heures, DateTime ptpg_d_date, clg_rubriques_temps prubriques_temps, Int64 ptpg_fct_cn, bool pAMAJ = true) : base(pModele, ptpg_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ptpg_cn != Int64.MinValue)
            c_tpg_cn = ptpg_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(ppersonnels != null)
            c_personnels = ppersonnels;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_tpg_n_heures = ptpg_n_heures;
		        if(ptpg_d_date != DateTime.MinValue)
            c_tpg_d_date = ptpg_d_date;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(prubriques_temps != null)
            c_rubriques_temps = prubriques_temps;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_tpg_fct_cn = ptpg_fct_cn;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ptpg_cn, Int64 ppersonnels, double ptpg_n_heures, DateTime ptpg_d_date, Int64 prubriques_temps, Int64 ptpg_fct_cn)
    {   
		        c_tpg_cn = ptpg_cn;
		c_dicReferences.Add("personnels", ppersonnels);
		        c_tpg_n_heures = ptpg_n_heures;
		        c_tpg_d_date = ptpg_d_date;
		c_dicReferences.Add("rubriques_temps", prubriques_temps);
		        c_tpg_fct_cn = ptpg_fct_cn;

        base.Initialise(ptpg_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtemps_perso_generaux; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");
		c_rubriques_temps = (clg_rubriques_temps) c_ModeleBase.RenvoieObjet(c_dicReferences["rubriques_temps"], "clg_rubriques_temps");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetemps_perso_generaux.Dictionnaire.Add(tpg_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetemps_perso_generaux.Dictionnaire.Remove(tpg_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_personnels != null)if(!c_personnels.Listetemps_perso_generaux.Contains(this)) c_personnels.Listetemps_perso_generaux.Add(this);
		if(c_rubriques_temps != null)if(!c_rubriques_temps.Listetemps_perso_generaux.Contains(this)) c_rubriques_temps.Listetemps_perso_generaux.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_temps_perso_generaux l_Clone = (clg_temps_perso_generaux) Clone;
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listetemps_perso_generaux.Contains(this)) l_Clone.personnels.Listetemps_perso_generaux.Remove(this);
		if(l_Clone.rubriques_temps != null)if(l_Clone.rubriques_temps.Listetemps_perso_generaux.Contains(this)) l_Clone.rubriques_temps.Listetemps_perso_generaux.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_temps_perso_generaux(null, tpg_cn, personnels, tpg_n_heures, tpg_d_date, rubriques_temps, tpg_fct_cn,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_temps_perso_generaux l_clone = (clg_temps_perso_generaux) this.Clone;
		c_tpg_cn = l_clone.tpg_cn;
		c_personnels = l_clone.personnels;
		c_tpg_n_heures = l_clone.tpg_n_heures;
		c_tpg_d_date = l_clone.tpg_d_date;
		c_rubriques_temps = l_clone.rubriques_temps;
		c_tpg_fct_cn = l_clone.tpg_fct_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtemps_perso_generaux.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtemps_perso_generaux.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtemps_perso_generaux.Delete(this);
    }

    /* Accesseur de la propriete tpg_cn (tpg_cn)
    * @return c_tpg_cn */
    public Int64 tpg_cn
    {
        get{return c_tpg_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_tpg_cn != value)
                {
                    CreerClone();
                    c_tpg_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (tpg_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(value != null)
            {
                if(c_personnels != value)
                {
                    CreerClone();
                    c_personnels = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete tpg_n_heures (tpg_n_heures)
    * @return c_tpg_n_heures */
    public double tpg_n_heures
    {
        get{return c_tpg_n_heures;}
        set
        {
            if(c_tpg_n_heures != value)
            {
                CreerClone();
                c_tpg_n_heures = value;
            }
        }
    }
    /* Accesseur de la propriete tpg_d_date (tpg_d_date)
    * @return c_tpg_d_date */
    public DateTime tpg_d_date
    {
        get{return c_tpg_d_date;}
        set
        {
            if(value != null)
            {
                if(c_tpg_d_date != value)
                {
                    CreerClone();
                    c_tpg_d_date = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete rubriques_temps (tpg_rbr_cn)
    * @return c_rubriques_temps */
    public clg_rubriques_temps rubriques_temps
    {
        get{return c_rubriques_temps;}
        set
        {
            if(value != null)
            {
                if(c_rubriques_temps != value)
                {
                    CreerClone();
                    c_rubriques_temps = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete tpg_fct_cn (tpg_fct_cn)
    * @return c_tpg_fct_cn */
    public Int64 tpg_fct_cn
    {
        get{return c_tpg_fct_cn;}
        set
        {
            if(c_tpg_fct_cn != value)
            {
                CreerClone();
                c_tpg_fct_cn = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_tpg_cn.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_tpg_n_heures.ToString());
		l_Valeurs.Add(c_tpg_d_date.ToString());
		l_Valeurs.Add(c_rubriques_temps==null ? "-1" : c_rubriques_temps.ID.ToString());
		l_Valeurs.Add(c_tpg_fct_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("tpg_cn");
		l_Noms.Add("personnels");
		l_Noms.Add("tpg_n_heures");
		l_Noms.Add("tpg_d_date");
		l_Noms.Add("rubriques_temps");
		l_Noms.Add("tpg_fct_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
