namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : limitation_interface </summary>
public partial class clg_limitation_interface : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLlimitation_interface;
    protected clg_Modele c_Modele;

	private Int64 c_lmi_cn;
	private clg_groupes c_groupes;
	private clg_objets_interface c_objets_interface;
	private clg_t_limitation_interface c_t_limitation_interface;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_groupes = null;
		this.c_objets_interface = null;
		this.c_t_limitation_interface = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_limitation_interface(clg_Modele pModele, Int64 plmi_cn, bool pAMAJ = false) : base(pModele, plmi_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_limitation_interface(clg_Modele pModele, Int64 plmi_cn, clg_groupes pgroupes, clg_objets_interface pobjets_interface, clg_t_limitation_interface pt_limitation_interface, bool pAMAJ = true) : base(pModele, plmi_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(plmi_cn != Int64.MinValue)
            c_lmi_cn = plmi_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pgroupes != null)
            c_groupes = pgroupes;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pobjets_interface != null)
            c_objets_interface = pobjets_interface;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pt_limitation_interface != null)
            c_t_limitation_interface = pt_limitation_interface;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 plmi_cn, Int64 pgroupes, Int64 pobjets_interface, Int64 pt_limitation_interface)
    {   
		        c_lmi_cn = plmi_cn;
		c_dicReferences.Add("groupes", pgroupes);
		c_dicReferences.Add("objets_interface", pobjets_interface);
		c_dicReferences.Add("t_limitation_interface", pt_limitation_interface);

        base.Initialise(plmi_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLlimitation_interface; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_groupes = (clg_groupes) c_ModeleBase.RenvoieObjet(c_dicReferences["groupes"], "clg_groupes");
		c_objets_interface = (clg_objets_interface) c_ModeleBase.RenvoieObjet(c_dicReferences["objets_interface"], "clg_objets_interface");
		c_t_limitation_interface = (clg_t_limitation_interface) c_ModeleBase.RenvoieObjet(c_dicReferences["t_limitation_interface"], "clg_t_limitation_interface");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listelimitation_interface.Dictionnaire.Add(lmi_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listelimitation_interface.Dictionnaire.Remove(lmi_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_groupes != null)if(!c_groupes.Listelimitation_interface.Contains(this)) c_groupes.Listelimitation_interface.Add(this);
		if(c_objets_interface != null)if(!c_objets_interface.Listelimitation_interface.Contains(this)) c_objets_interface.Listelimitation_interface.Add(this);
		if(c_t_limitation_interface != null)if(!c_t_limitation_interface.Listelimitation_interface.Contains(this)) c_t_limitation_interface.Listelimitation_interface.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_limitation_interface l_Clone = (clg_limitation_interface) Clone;
		if(l_Clone.groupes != null)if(l_Clone.groupes.Listelimitation_interface.Contains(this)) l_Clone.groupes.Listelimitation_interface.Remove(this);
		if(l_Clone.objets_interface != null)if(l_Clone.objets_interface.Listelimitation_interface.Contains(this)) l_Clone.objets_interface.Listelimitation_interface.Remove(this);
		if(l_Clone.t_limitation_interface != null)if(l_Clone.t_limitation_interface.Listelimitation_interface.Contains(this)) l_Clone.t_limitation_interface.Listelimitation_interface.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_limitation_interface(null, lmi_cn, groupes, objets_interface, t_limitation_interface,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_limitation_interface l_clone = (clg_limitation_interface) this.Clone;
		c_lmi_cn = l_clone.lmi_cn;
		c_groupes = l_clone.groupes;
		c_objets_interface = l_clone.objets_interface;
		c_t_limitation_interface = l_clone.t_limitation_interface;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLlimitation_interface.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLlimitation_interface.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLlimitation_interface.Delete(this);
    }

    /* Accesseur de la propriete lmi_cn (lmi_cn)
    * @return c_lmi_cn */
    public Int64 lmi_cn
    {
        get{return c_lmi_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_lmi_cn != value)
                {
                    CreerClone();
                    c_lmi_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete groupes (lmi_grp_cn)
    * @return c_groupes */
    public clg_groupes groupes
    {
        get{return c_groupes;}
        set
        {
            if(value != null)
            {
                if(c_groupes != value)
                {
                    CreerClone();
                    c_groupes = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete objets_interface (lmi_obi_cn)
    * @return c_objets_interface */
    public clg_objets_interface objets_interface
    {
        get{return c_objets_interface;}
        set
        {
            if(value != null)
            {
                if(c_objets_interface != value)
                {
                    CreerClone();
                    c_objets_interface = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_limitation_interface (lmi_t_lmi_cn)
    * @return c_t_limitation_interface */
    public clg_t_limitation_interface t_limitation_interface
    {
        get{return c_t_limitation_interface;}
        set
        {
            if(value != null)
            {
                if(c_t_limitation_interface != value)
                {
                    CreerClone();
                    c_t_limitation_interface = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_lmi_cn.ToString());
		l_Valeurs.Add(c_groupes==null ? "-1" : c_groupes.ID.ToString());
		l_Valeurs.Add(c_objets_interface==null ? "-1" : c_objets_interface.ID.ToString());
		l_Valeurs.Add(c_t_limitation_interface==null ? "-1" : c_t_limitation_interface.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("lmi_cn");
		l_Noms.Add("groupes");
		l_Noms.Add("objets_interface");
		l_Noms.Add("t_limitation_interface");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
