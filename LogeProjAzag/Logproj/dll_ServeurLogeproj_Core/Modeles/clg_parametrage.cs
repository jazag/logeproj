namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : parametrage </summary>
public partial class clg_parametrage : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLparametrage;
    protected clg_Modele c_Modele;

	private Int64 c_prm_cn;
	private string c_prm_a_lib;
	private string c_prm_a_desc;
	private bool c_prm_b_activation;
	private clg_t_parametre c_t_parametre;
	private bool c_prm_b_act_util;
	private List<clg_param_conservatoire> c_Listeparam_conservatoire;
	private List<clg_valeur_param> c_Listevaleur_param;


    private void Init()
    {
		c_Listeparam_conservatoire = new List<clg_param_conservatoire>();
		c_Listevaleur_param = new List<clg_valeur_param>();

    }

    public override void Detruit()
    {
		c_Listeparam_conservatoire.Clear();
		c_Listevaleur_param.Clear();
		this.c_t_parametre = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_parametrage(clg_Modele pModele, Int64 pprm_cn, bool pAMAJ = false) : base(pModele, pprm_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_parametrage(clg_Modele pModele, Int64 pprm_cn, string pprm_a_lib, string pprm_a_desc, bool pprm_b_activation, clg_t_parametre pt_parametre, bool pprm_b_act_util, bool pAMAJ = true) : base(pModele, pprm_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pprm_cn != Int64.MinValue)
            c_prm_cn = pprm_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_prm_a_lib = pprm_a_lib;
		        c_prm_a_desc = pprm_a_desc;
		        c_prm_b_activation = pprm_b_activation;
		        c_t_parametre = pt_parametre;
		        c_prm_b_act_util = pprm_b_act_util;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pprm_cn, string pprm_a_lib, string pprm_a_desc, bool pprm_b_activation, Int64 pt_parametre, bool pprm_b_act_util)
    {   
		        c_prm_cn = pprm_cn;
		        c_prm_a_lib = pprm_a_lib;
		        c_prm_a_desc = pprm_a_desc;
		        c_prm_b_activation = pprm_b_activation;
		c_dicReferences.Add("t_parametre", pt_parametre);
		        c_prm_b_act_util = pprm_b_act_util;

        base.Initialise(pprm_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLparametrage; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_t_parametre = (clg_t_parametre) c_ModeleBase.RenvoieObjet(c_dicReferences["t_parametre"], "clg_t_parametre");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeparametrage.Dictionnaire.Add(prm_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeparametrage.Dictionnaire.Remove(prm_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_t_parametre != null)if(!c_t_parametre.Listeparametrage.Contains(this)) c_t_parametre.Listeparametrage.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_parametrage l_Clone = (clg_parametrage) Clone;
		if(l_Clone.t_parametre != null)if(l_Clone.t_parametre.Listeparametrage.Contains(this)) l_Clone.t_parametre.Listeparametrage.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeparam_conservatoire.Count > 0) c_EstReference = true;
		if(c_Listevaleur_param.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_parametrage(null, prm_cn, prm_a_lib, prm_a_desc, prm_b_activation, t_parametre, prm_b_act_util,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_parametrage l_clone = (clg_parametrage) this.Clone;
		c_prm_cn = l_clone.prm_cn;
		c_prm_a_lib = l_clone.prm_a_lib;
		c_prm_a_desc = l_clone.prm_a_desc;
		c_prm_b_activation = l_clone.prm_b_activation;
		c_t_parametre = l_clone.t_parametre;
		c_prm_b_act_util = l_clone.prm_b_act_util;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLparametrage.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLparametrage.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLparametrage.Delete(this);
    }

    /* Accesseur de la propriete prm_cn (prm_cn)
    * @return c_prm_cn */
    public Int64 prm_cn
    {
        get{return c_prm_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_prm_cn != value)
                {
                    CreerClone();
                    c_prm_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete prm_a_lib (prm_a_lib)
    * @return c_prm_a_lib */
    public string prm_a_lib
    {
        get{return c_prm_a_lib;}
        set
        {
            if(c_prm_a_lib != value)
            {
                CreerClone();
                c_prm_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete prm_a_desc (prm_a_desc)
    * @return c_prm_a_desc */
    public string prm_a_desc
    {
        get{return c_prm_a_desc;}
        set
        {
            if(c_prm_a_desc != value)
            {
                CreerClone();
                c_prm_a_desc = value;
            }
        }
    }
    /* Accesseur de la propriete prm_b_activation (prm_b_activation)
    * @return c_prm_b_activation */
    public bool prm_b_activation
    {
        get{return c_prm_b_activation;}
        set
        {
            if(c_prm_b_activation != value)
            {
                CreerClone();
                c_prm_b_activation = value;
            }
        }
    }
    /* Accesseur de la propriete t_parametre (prm_t_prm_cn)
    * @return c_t_parametre */
    public clg_t_parametre t_parametre
    {
        get{return c_t_parametre;}
        set
        {
            if(c_t_parametre != value)
            {
                CreerClone();
                c_t_parametre = value;
            }
        }
    }
    /* Accesseur de la propriete prm_b_act_util (prm_b_act_util)
    * @return c_prm_b_act_util */
    public bool prm_b_act_util
    {
        get{return c_prm_b_act_util;}
        set
        {
            if(c_prm_b_act_util != value)
            {
                CreerClone();
                c_prm_b_act_util = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type param_conservatoire */
    public List<clg_param_conservatoire> Listeparam_conservatoire
    {
        get { return c_Listeparam_conservatoire; }
    }    /* Accesseur de la liste des objets de type valeur_param */
    public List<clg_valeur_param> Listevaleur_param
    {
        get { return c_Listevaleur_param; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_prm_cn.ToString());
		l_Valeurs.Add(c_prm_a_lib.ToString());
		l_Valeurs.Add(c_prm_a_desc.ToString());
		l_Valeurs.Add(c_prm_b_activation.ToString());
		l_Valeurs.Add(c_t_parametre==null ? "-1" : c_t_parametre.ID.ToString());
		l_Valeurs.Add(c_prm_b_act_util.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("prm_cn");
		l_Noms.Add("prm_a_lib");
		l_Noms.Add("prm_a_desc");
		l_Noms.Add("prm_b_activation");
		l_Noms.Add("t_parametre");
		l_Noms.Add("prm_b_act_util");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
