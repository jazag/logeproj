namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
    using System;
    using clg_ReflexionV3_Core;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Globalization;

    /// <summary> Classe modele de la table : elements </summary>
    public partial class clg_elements : clg_ObjetBase
    {
        private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
        public static clg_ControleurBase c_CTLelements;
        protected clg_Modele c_Modele;

        private Int64 c_ele_cn;
        private clg_elements c_elements;
        private Int64 c_ele_n_niveau;
        private string c_ele_a_lib;
        private clg_t_element c_t_element;
        private string c_ele_a_code;
        private clg_elements c_elements2;
        private Int64 c_ele_n_archive;
        private DateTime c_ele_d_debut_prev;
        private DateTime c_ele_d_fin_reelle;
        private DateTime c_ele_d_fin_prev;
        private DateTime c_ele_d_debut_reelle;
        private DateTime c_ele_d_archivage;
        private Int64 c_ele_n_visibleetat;
        private List<clg_carac_element> c_Listecarac_element;
        private List<clg_comment> c_Listecomment;
        private List<clg_deplacements_prevus> c_Listedeplacements_prevus;
        private List<clg_elements> c_Listeelements;
        private List<clg_elements> c_Listeelements2;
        private List<clg_facture> c_Listefacture;
        private List<clg_intervenants> c_Listeintervenants;
        private List<clg_lien_arretes> c_Listelien_arretes;
        private List<clg_lien_sage> c_Listelien_sage;
        private List<clg_taux> c_Listetaux;
        private List<clg_ventilation_deplacement> c_Listeventilation_deplacement;
        private List<clg_ventilation_deplacement_csa> c_Listeventilation_deplacement_csa;
        private bool okay = false;



        private void Init()
        {
            c_Listecarac_element = new List<clg_carac_element>();
            c_Listecomment = new List<clg_comment>();
            c_Listedeplacements_prevus = new List<clg_deplacements_prevus>();
            c_Listeelements = new List<clg_elements>();
            c_Listeelements2 = new List<clg_elements>();
            c_Listefacture = new List<clg_facture>();
            c_Listeintervenants = new List<clg_intervenants>();
            c_Listelien_arretes = new List<clg_lien_arretes>();
            c_Listelien_sage = new List<clg_lien_sage>();
            c_Listetaux = new List<clg_taux>();
            c_Listeventilation_deplacement = new List<clg_ventilation_deplacement>();
            c_Listeventilation_deplacement_csa = new List<clg_ventilation_deplacement_csa>();

        }

        public override void Detruit()
        {
            c_Listecarac_element.Clear();
            c_Listecomment.Clear();
            c_Listedeplacements_prevus.Clear();
            c_Listeelements.Clear();
            c_Listeelements2.Clear();
            c_Listefacture.Clear();
            c_Listeintervenants.Clear();
            c_Listelien_arretes.Clear();
            c_Listelien_sage.Clear();
            c_Listetaux.Clear();
            c_Listeventilation_deplacement.Clear();
            c_Listeventilation_deplacement_csa.Clear();
            this.c_elements = null;
            this.c_t_element = null;
            this.c_elements2 = null;

        }

        /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
        public clg_elements(clg_Modele pModele, Int64 pele_cn, bool pAMAJ = false) : base(pModele, pele_cn, pAMAJ)
        {
            Init();
            c_Modele = pModele;
        }

        /// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
        public clg_elements(clg_Modele pModele, Int64 pele_cn, clg_elements pelements, Int64 pele_n_niveau, string pele_a_lib, clg_t_element pt_element, string pele_a_code, clg_elements pelements2, Int64 pele_n_archive, DateTime pele_d_debut_prev, DateTime pele_d_fin_reelle, DateTime pele_d_fin_prev, DateTime pele_d_debut_reelle, DateTime pele_d_archivage, Int64 pele_n_visibleetat, bool pAMAJ = true) : base(pModele, pele_cn, pAMAJ)
        {
            c_Modele = pModele;
            Init();
            if (pele_cn != Int64.MinValue)
                c_ele_cn = pele_cn;
            else
                throw new System.InvalidOperationException("MinValue interdite");

            c_elements = pelements;

            if (pele_n_niveau != Int64.MinValue)
                c_ele_n_niveau = pele_n_niveau;
            else
                throw new System.InvalidOperationException("MinValue interdite");

            c_ele_a_lib = pele_a_lib;

            if (pt_element != null)
                c_t_element = pt_element;
            else
                throw new System.InvalidOperationException("Valeur nulle interdite");

            c_ele_a_code = pele_a_code;

            if (pelements2 != null)
                c_elements2 = pelements2;
            else
                throw new System.InvalidOperationException("Valeur nulle interdite");

            c_ele_n_archive = pele_n_archive;

            c_ele_d_debut_prev = pele_d_debut_prev;

            c_ele_d_fin_reelle = pele_d_fin_reelle;

            c_ele_d_fin_prev = pele_d_fin_prev;

            c_ele_d_debut_reelle = pele_d_debut_reelle;

            c_ele_d_archivage = pele_d_archivage;

            if (pele_n_visibleetat != Int64.MinValue)
                c_ele_n_visibleetat = pele_n_visibleetat;
            else
                throw new System.InvalidOperationException("MinValue interdite");


            if (pAMAJ) c_Modele.AjouteObjetAMAJ(this);
        }

        /// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
        public void Initialise(Int64 pele_cn, Int64 pelements, Int64 pele_n_niveau, string pele_a_lib, Int64 pt_element, string pele_a_code, Int64 pelements2, Int64 pele_n_archive, DateTime pele_d_debut_prev, DateTime pele_d_fin_reelle, DateTime pele_d_fin_prev, DateTime pele_d_debut_reelle, DateTime pele_d_archivage, Int64 pele_n_visibleetat)
        {
            c_ele_cn = pele_cn;

            c_dicReferences.Add("elements", pelements);
            c_ele_n_niveau = pele_n_niveau;

            c_ele_a_lib = pele_a_lib;

            c_dicReferences.Add("t_element", pt_element);
            c_ele_a_code = pele_a_code;

            c_dicReferences.Add("elements2", pelements2);
            c_ele_n_archive = pele_n_archive;

            c_ele_d_debut_prev = pele_d_debut_prev;

            c_ele_d_fin_reelle = pele_d_fin_reelle;

            c_ele_d_fin_prev = pele_d_fin_prev;

            c_ele_d_debut_reelle = pele_d_debut_reelle;

            c_ele_d_archivage = pele_d_archivage;

            c_ele_n_visibleetat = pele_n_visibleetat;


            base.Initialise(pele_cn);
        }

        /// <summary> Accesseur vers le controleur de cette classe </summary>
        public override clg_ControleurBase Controleur
        {
            get { return c_CTLelements; }
        }

        /// <summary> Instanciation des proprietes referencant d'autres objets </summary>
        public override void CreeLiens()
        {
            c_elements = (clg_elements)c_ModeleBase.RenvoieObjet(c_dicReferences["elements"], "clg_elements");
            c_t_element = (clg_t_element)c_ModeleBase.RenvoieObjet(c_dicReferences["t_element"], "clg_t_element");
            c_elements2 = (clg_elements)c_ModeleBase.RenvoieObjet(c_dicReferences["elements2"], "clg_elements");

        }

        /// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
        public override void AjouteDansListe()
        {
            c_Modele.Listeelements.Dictionnaire.Add(ele_cn, this);
        }

        /// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
        public override void SupprimeDansListe()
        {
            c_Modele.Listeelements.Dictionnaire.Remove(ele_cn);

        }

        /// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
        public override void CreeReferences()
        {
            if (c_elements != null) if (!c_elements.Listeelements.Contains(this)) c_elements.Listeelements.Add(this);
            if (c_t_element != null) if (!c_t_element.Listeelements.Contains(this)) c_t_element.Listeelements.Add(this);
            if (c_elements2 != null) if (!c_elements2.Listeelements2.Contains(this)) c_elements2.Listeelements2.Add(this);

        }

        /// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
        public override void DetruitReferences()
        {
            if (Clone != null)
            {
                clg_elements l_Clone = (clg_elements)Clone;
                if (l_Clone.elements != null) if (l_Clone.elements.Listeelements.Contains(this)) l_Clone.elements.Listeelements.Remove(this);
                if (l_Clone.t_element != null) if (l_Clone.t_element.Listeelements.Contains(this)) l_Clone.t_element.Listeelements.Remove(this);
                if (l_Clone.elements2 != null) if (l_Clone.elements2.Listeelements2.Contains(this)) l_Clone.elements2.Listeelements2.Remove(this);

            }
        }

        /// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
        protected override void AnalyseReferences()
        {
            base.AnalyseReferences();
            if (c_Listecarac_element.Count > 0) c_EstReference = true;
            if (c_Listecomment.Count > 0) c_EstReference = true;
            if (c_Listedeplacements_prevus.Count > 0) c_EstReference = true;
            if (c_Listeelements.Count > 0) c_EstReference = true;
            if (c_Listeelements2.Count > 0) c_EstReference = true;
            if (c_Listefacture.Count > 0) c_EstReference = true;
            if (c_Listeintervenants.Count > 0) c_EstReference = true;
            if (c_Listelien_arretes.Count > 0) c_EstReference = true;
            if (c_Listelien_sage.Count > 0) c_EstReference = true;
            if (c_Listetaux.Count > 0) c_EstReference = true;
            if (c_Listeventilation_deplacement.Count > 0) c_EstReference = true;
            if (c_Listeventilation_deplacement_csa.Count > 0) c_EstReference = true;

        }

        /// <summary> Creation d'un clone de l'objet </summary>
        protected override void CreerClone()
        {
            if (base.Clone == null)
            {
                base.Clone = Cloner();
                c_Modele.AjouteObjetAMAJ(this);
            }
        }

        /// <summary> Creation d'un clone de l'objet </summary>
        public override clg_ObjetBase Cloner()
        {
            return new clg_elements(null, ele_cn, elements, ele_n_niveau, ele_a_lib, t_element, ele_a_code, elements2, ele_n_archive, ele_d_debut_prev, ele_d_fin_reelle, ele_d_fin_prev, ele_d_debut_reelle, ele_d_archivage, ele_n_visibleetat, false);
        }

        /// <summary> Annulation des modifications sur l'objet </summary>
        public override void AnnuleModification()
        {
            clg_elements l_clone = (clg_elements)this.Clone;
            c_ele_cn = l_clone.ele_cn;
            c_elements = l_clone.elements;
            c_ele_n_niveau = l_clone.ele_n_niveau;
            c_ele_a_lib = l_clone.ele_a_lib;
            c_t_element = l_clone.t_element;
            c_ele_a_code = l_clone.ele_a_code;
            c_elements2 = l_clone.elements2;
            c_ele_n_archive = l_clone.ele_n_archive;
            c_ele_d_debut_prev = l_clone.ele_d_debut_prev;
            c_ele_d_fin_reelle = l_clone.ele_d_fin_reelle;
            c_ele_d_fin_prev = l_clone.ele_d_fin_prev;
            c_ele_d_debut_reelle = l_clone.ele_d_debut_reelle;
            c_ele_d_archivage = l_clone.ele_d_archivage;
            c_ele_n_visibleetat = l_clone.ele_n_visibleetat;

        }

        public override void InsereEnBase()
        {
            base.InsereEnBase();
            c_CTLelements.Insert(this);
        }

        public override void MAJEnBase()
        {
            base.MAJEnBase();
            c_CTLelements.Update(this);
        }

        public override void SupprimeEnBase()
        {
            base.SupprimeEnBase();
            c_CTLelements.Delete(this);
        }

        /* Accesseur de la propriete ele_cn (ele_cn)
        * @return c_ele_cn */
        public Int64 ele_cn
        {
            get { return c_ele_cn; }

            set
            {
                if (value != Int64.MinValue)
                {
                    if (c_ele_cn != value)
                    {
                        CreerClone();
                        c_ele_cn = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur MinValue interdite");
                }
            }
        }

        /* Accesseur de la propriete elements (ele_cn_pere)
        * @return c_elements */
        public clg_elements elements
        {
            get { return c_elements; }

            set
            {
                if (c_elements != value)
                {
                    CreerClone();
                    c_elements = value;
                }
            }
        }

        /* Accesseur de la propriete ele_n_niveau (ele_n_niveau)
        * @return c_ele_n_niveau */
        public Int64 ele_n_niveau
        {
            get { return c_ele_n_niveau; }

            set
            {
                if (value != Int64.MinValue)
                {
                    if (c_ele_n_niveau != value)
                    {
                        CreerClone();
                        c_ele_n_niveau = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur MinValue interdite");
                }
            }
        }

        /* Accesseur de la propriete ele_a_lib (ele_a_lib)
        * @return c_ele_a_lib */
        public string ele_a_lib
        {
            get { return c_ele_a_lib; }

            set
            {
                if (c_ele_a_lib != value)
                {
                    CreerClone();
                    c_ele_a_lib = value;
                }
            }
        }

        /* Accesseur de la propriete t_element (ele_t_ele_cn)
        * @return c_t_element */
        public clg_t_element t_element
        {
            get { return c_t_element; }

            set
            {
                if (value != null)
                {
                    if (c_t_element != value)
                    {
                        CreerClone();
                        c_t_element = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur nulle interdite");
                }
            }
        }

        /* Accesseur de la propriete ele_a_code (ele_a_code)
        * @return c_ele_a_code */
        public string ele_a_code
        {
            get { return c_ele_a_code; }

            set
            {
                if (c_ele_a_code != value)
                {
                    CreerClone();
                    c_ele_a_code = value;
                }
            }
        }

        /* Accesseur de la propriete elements2 (ele_cn_racine)
        * @return c_elements2 */
        public clg_elements elements2
        {
            get { return c_elements2; }

            set
            {
                if (value != null)
                {
                    if (c_elements2 != value)
                    {
                        CreerClone();
                        c_elements2 = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur nulle interdite");
                }
            }
        }

        /* Accesseur de la propriete ele_n_archive (ele_n_archive)
        * @return c_ele_n_archive */
        public Int64 ele_n_archive
        {
            get { return c_ele_n_archive; }

            set
            {
                if (c_ele_n_archive != value)
                {
                    CreerClone();
                    c_ele_n_archive = value;
                }
            }
        }

        /* Accesseur de la propriete ele_d_debut_prev (ele_d_debut_prev)
        * @return c_ele_d_debut_prev */
        public DateTime ele_d_debut_prev
        {
            get { return c_ele_d_debut_prev; }

            set
            {
                if (c_ele_d_debut_prev != value)
                {
                    CreerClone();
                    c_ele_d_debut_prev = value;
                }
            }
        }

        /* Accesseur de la propriete ele_d_fin_reelle (ele_d_fin_reelle)
        * @return c_ele_d_fin_reelle */
        public DateTime ele_d_fin_reelle
        {
            get { return c_ele_d_fin_reelle; }

            set
            {
                if (c_ele_d_fin_reelle != value)
                {
                    CreerClone();
                    c_ele_d_fin_reelle = value;
                }
            }
        }

        /* Accesseur de la propriete ele_d_fin_prev (ele_d_fin_prev)
        * @return c_ele_d_fin_prev */
        public DateTime ele_d_fin_prev
        {
            get { return c_ele_d_fin_prev; }

            set
            {
                if (c_ele_d_fin_prev != value)
                {
                    CreerClone();
                    c_ele_d_fin_prev = value;
                }
            }
        }

        /* Accesseur de la propriete ele_d_debut_reelle (ele_d_debut_reelle)
        * @return c_ele_d_debut_reelle */
        public DateTime ele_d_debut_reelle
        {
            get { return c_ele_d_debut_reelle; }

            set
            {
                if (c_ele_d_debut_reelle != value)
                {
                    CreerClone();
                    c_ele_d_debut_reelle = value;
                }
            }
        }

        /* Accesseur de la propriete ele_d_archivage (ele_d_archivage)
        * @return c_ele_d_archivage */
        public DateTime ele_d_archivage
        {
            get { return c_ele_d_archivage; }

            set
            {
                if (c_ele_d_archivage != value)
                {
                    CreerClone();
                    c_ele_d_archivage = value;
                }
            }
        }

        /* Accesseur de la propriete ele_n_visibleetat (ele_n_visibleetat)
        * @return c_ele_n_visibleetat */
        public Int64 ele_n_visibleetat
        {
            get { return c_ele_n_visibleetat; }

            set
            {
                if (value != Int64.MinValue)
                {
                    if (c_ele_n_visibleetat != value)
                    {
                        CreerClone();
                        c_ele_n_visibleetat = value;
                    }
                }
                else
                {
                    throw new System.InvalidOperationException("Valeur MinValue interdite");
                }
            }
        }

        /* Accesseur de la liste des objets de type carac_element */
        public List<clg_carac_element> Listecarac_element
        {
            get { return c_Listecarac_element; }
        }
        /* Accesseur de la liste des objets de type comment */
        public List<clg_comment> Listecomment
        {
            get { return c_Listecomment; }
        }
        /* Accesseur de la liste des objets de type deplacements_prevus */
        public List<clg_deplacements_prevus> Listedeplacements_prevus
        {
            get { return c_Listedeplacements_prevus; }
        }
        /* Accesseur de la liste des objets de type elements */
        public List<clg_elements> Listeelements
        {
            get { return c_Listeelements; }
        }
        /* Accesseur de la liste des objets de type elements */
        public List<clg_elements> Listeelements2
        {
            get { return c_Listeelements2; }
        }
        /* Accesseur de la liste des objets de type facture */
        public List<clg_facture> Listefacture
        {
            get { return c_Listefacture; }
        }
        /* Accesseur de la liste des objets de type intervenants */
        public List<clg_intervenants> Listeintervenants
        {
            get { return c_Listeintervenants; }
        }
        /* Accesseur de la liste des objets de type lien_arretes */
        public List<clg_lien_arretes> Listelien_arretes
        {
            get { return c_Listelien_arretes; }
        }
        /* Accesseur de la liste des objets de type lien_sage */
        public List<clg_lien_sage> Listelien_sage
        {
            get { return c_Listelien_sage; }
        }
        /* Accesseur de la liste des objets de type taux */
        public List<clg_taux> Listetaux
        {
            get { return c_Listetaux; }
        }
        /* Accesseur de la liste des objets de type ventilation_deplacement */
        public List<clg_ventilation_deplacement> Listeventilation_deplacement
        {
            get { return c_Listeventilation_deplacement; }
        }
        /* Accesseur de la liste des objets de type ventilation_deplacement_csa */
        public List<clg_ventilation_deplacement_csa> Listeventilation_deplacement_csa
        {
            get { return c_Listeventilation_deplacement_csa; }
        }

        public bool Okay
        {
            get { return okay; }
            set { okay = value; }
        }

        public override List<string> ListeValeursProprietes()
        {
            List<string> l_Valeurs = new List<string>();
            l_Valeurs.Add(c_ele_cn.ToString());
            l_Valeurs.Add(c_elements == null ? "-1" : c_elements.ID.ToString());
            l_Valeurs.Add(c_ele_n_niveau.ToString());
            l_Valeurs.Add(c_ele_a_lib.ToString());
            l_Valeurs.Add(c_t_element == null ? "-1" : c_t_element.ID.ToString());
            l_Valeurs.Add(c_ele_a_code.ToString());
            l_Valeurs.Add(c_elements2 == null ? "-1" : c_elements2.ID.ToString());
            l_Valeurs.Add(c_ele_n_archive.ToString());
            l_Valeurs.Add(c_ele_d_debut_prev.ToString());
            l_Valeurs.Add(c_ele_d_fin_reelle.ToString());
            l_Valeurs.Add(c_ele_d_fin_prev.ToString());
            l_Valeurs.Add(c_ele_d_debut_reelle.ToString());
            l_Valeurs.Add(c_ele_d_archivage.ToString());
            l_Valeurs.Add(c_ele_n_visibleetat.ToString());

            l_Valeurs.AddRange(base.ListeValeursProprietes());
            return l_Valeurs;
        }

        public override List<string> ListeNomsProprietes()
        {
            List<string> l_Noms = new List<string>();
            l_Noms.Add("ele_cn");
            l_Noms.Add("elements");
            l_Noms.Add("ele_n_niveau");
            l_Noms.Add("ele_a_lib");
            l_Noms.Add("t_element");
            l_Noms.Add("ele_a_code");
            l_Noms.Add("elements2");
            l_Noms.Add("ele_n_archive");
            l_Noms.Add("ele_d_debut_prev");
            l_Noms.Add("ele_d_fin_reelle");
            l_Noms.Add("ele_d_fin_prev");
            l_Noms.Add("ele_d_debut_reelle");
            l_Noms.Add("ele_d_archivage");
            l_Noms.Add("ele_n_visibleetat");

            l_Noms.AddRange(base.ListeNomsProprietes());
            return l_Noms;
        }


        #endregion
    }
}
