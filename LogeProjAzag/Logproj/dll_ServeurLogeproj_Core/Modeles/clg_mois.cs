namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : mois </summary>
public partial class clg_mois : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLmois;
    protected clg_Modele c_Modele;

	private Int64 c_mois_cn;
	private string c_mois_a_lib;
	private List<clg_mensuel> c_Listemensuel;


    private void Init()
    {
		c_Listemensuel = new List<clg_mensuel>();

    }

    public override void Detruit()
    {
		c_Listemensuel.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_mois(clg_Modele pModele, Int64 pmois_cn, bool pAMAJ = false) : base(pModele, pmois_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_mois(clg_Modele pModele, Int64 pmois_cn, string pmois_a_lib, bool pAMAJ = true) : base(pModele, pmois_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pmois_cn != Int64.MinValue)
            c_mois_cn = pmois_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pmois_a_lib != null)
            c_mois_a_lib = pmois_a_lib;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pmois_cn, string pmois_a_lib)
    {   
		        c_mois_cn = pmois_cn;
		        c_mois_a_lib = pmois_a_lib;

        base.Initialise(pmois_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLmois; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listemois.Dictionnaire.Add(mois_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listemois.Dictionnaire.Remove(mois_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_mois l_Clone = (clg_mois) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listemensuel.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_mois(null, mois_cn, mois_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_mois l_clone = (clg_mois) this.Clone;
		c_mois_cn = l_clone.mois_cn;
		c_mois_a_lib = l_clone.mois_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLmois.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLmois.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLmois.Delete(this);
    }

    /* Accesseur de la propriete mois_cn (mois_cn)
    * @return c_mois_cn */
    public Int64 mois_cn
    {
        get{return c_mois_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_mois_cn != value)
                {
                    CreerClone();
                    c_mois_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete mois_a_lib (mois_a_lib)
    * @return c_mois_a_lib */
    public string mois_a_lib
    {
        get{return c_mois_a_lib;}
        set
        {
            if(value != null)
            {
                if(c_mois_a_lib != value)
                {
                    CreerClone();
                    c_mois_a_lib = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type mensuel */
    public List<clg_mensuel> Listemensuel
    {
        get { return c_Listemensuel; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_mois_cn.ToString());
		l_Valeurs.Add(c_mois_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("mois_cn");
		l_Noms.Add("mois_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
