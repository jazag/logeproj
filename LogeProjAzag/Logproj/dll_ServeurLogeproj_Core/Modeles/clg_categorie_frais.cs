namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : categorie_frais </summary>
public partial class clg_categorie_frais : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcategorie_frais;
    protected clg_Modele c_Modele;

	private Int64 c_cfr_cn;
	private string c_cfr_a_libelle;
	private List<clg_frais_mission> c_Listefrais_mission;


    private void Init()
    {
		c_Listefrais_mission = new List<clg_frais_mission>();

    }

    public override void Detruit()
    {
		c_Listefrais_mission.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_categorie_frais(clg_Modele pModele, Int64 pcfr_cn, bool pAMAJ = false) : base(pModele, pcfr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_categorie_frais(clg_Modele pModele, Int64 pcfr_cn, string pcfr_a_libelle, bool pAMAJ = true) : base(pModele, pcfr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcfr_cn != Int64.MinValue)
            c_cfr_cn = pcfr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_cfr_a_libelle = pcfr_a_libelle;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcfr_cn, string pcfr_a_libelle)
    {   
		        c_cfr_cn = pcfr_cn;
		        c_cfr_a_libelle = pcfr_a_libelle;

        base.Initialise(pcfr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcategorie_frais; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecategorie_frais.Dictionnaire.Add(cfr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecategorie_frais.Dictionnaire.Remove(cfr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_categorie_frais l_Clone = (clg_categorie_frais) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listefrais_mission.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_categorie_frais(null, cfr_cn, cfr_a_libelle,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_categorie_frais l_clone = (clg_categorie_frais) this.Clone;
		c_cfr_cn = l_clone.cfr_cn;
		c_cfr_a_libelle = l_clone.cfr_a_libelle;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcategorie_frais.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcategorie_frais.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcategorie_frais.Delete(this);
    }

    /* Accesseur de la propriete cfr_cn (cfr_cn)
    * @return c_cfr_cn */
    public Int64 cfr_cn
    {
        get{return c_cfr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cfr_cn != value)
                {
                    CreerClone();
                    c_cfr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete cfr_a_libelle (cfr_a_libelle)
    * @return c_cfr_a_libelle */
    public string cfr_a_libelle
    {
        get{return c_cfr_a_libelle;}
        set
        {
            if(c_cfr_a_libelle != value)
            {
                CreerClone();
                c_cfr_a_libelle = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type frais_mission */
    public List<clg_frais_mission> Listefrais_mission
    {
        get { return c_Listefrais_mission; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cfr_cn.ToString());
		l_Valeurs.Add(c_cfr_a_libelle.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cfr_cn");
		l_Noms.Add("cfr_a_libelle");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
