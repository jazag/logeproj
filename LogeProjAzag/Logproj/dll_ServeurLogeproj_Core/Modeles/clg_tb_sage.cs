namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : tb_sage </summary>
public partial class clg_tb_sage : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLtb_sage;
    protected clg_Modele c_Modele;

	private Int64 c_sag_ec_no;
	private Int64 c_sag_ea_ligne;
	private DateTime c_sag_d_sage;
	private DateTime c_sag_jm_date;
	private Int64 c_sag_ec_jour;
	private Int64 c_sag_ec_sens;
	private string c_sag_ec_intitule;
	private string c_sag_ca_num;
	private double c_sag_ea_montant;
	private string c_sag_jo_num;
	private string c_sag_cg_num;
	private Int64 c_sag_annee_exe;
	private clg_basemae c_basemae;
	private Int64 c_sag_ara_cn;
	private Int64 c_sag_four_cn;
	private clg_type_fonct_compta c_type_fonct_compta;
	private int c_sag_cn;
	private clg_financeurs c_financeurs;
	private string c_sag_n_piece;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_basemae = null;
		this.c_type_fonct_compta = null;
		this.c_financeurs = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_tb_sage(clg_Modele pModele, int psag_cn, bool pAMAJ = false) : base(pModele, psag_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_tb_sage(clg_Modele pModele, int psag_cn, Int64 psag_ec_no, Int64 psag_ea_ligne, DateTime psag_d_sage, DateTime psag_jm_date, Int64 psag_ec_jour, Int64 psag_ec_sens, string psag_ec_intitule, string psag_ca_num, double psag_ea_montant, string psag_jo_num, string psag_cg_num, Int64 psag_annee_exe, clg_basemae pbasemae, Int64 psag_ara_cn, Int64 psag_four_cn, clg_type_fonct_compta ptype_fonct_compta, clg_financeurs pfinanceurs, string psag_n_piece, bool pAMAJ = true) : base(pModele, psag_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        c_sag_ec_no = psag_ec_no;
		        c_sag_ea_ligne = psag_ea_ligne;
		        c_sag_d_sage = psag_d_sage;
		        c_sag_jm_date = psag_jm_date;
		        c_sag_ec_jour = psag_ec_jour;
		        c_sag_ec_sens = psag_ec_sens;
		        c_sag_ec_intitule = psag_ec_intitule;
		        c_sag_ca_num = psag_ca_num;
		        c_sag_ea_montant = psag_ea_montant;
		        c_sag_jo_num = psag_jo_num;
		        c_sag_cg_num = psag_cg_num;
		        c_sag_annee_exe = psag_annee_exe;
		        c_basemae = pbasemae;
		        c_sag_ara_cn = psag_ara_cn;
		        c_sag_four_cn = psag_four_cn;
		        c_type_fonct_compta = ptype_fonct_compta;
		        if(psag_cn != int.MinValue)
            c_sag_cn = psag_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_financeurs = pfinanceurs;
		        c_sag_n_piece = psag_n_piece;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(int psag_cn, Int64 psag_ec_no, Int64 psag_ea_ligne, DateTime psag_d_sage, DateTime psag_jm_date, Int64 psag_ec_jour, Int64 psag_ec_sens, string psag_ec_intitule, string psag_ca_num, double psag_ea_montant, string psag_jo_num, string psag_cg_num, Int64 psag_annee_exe, Int64 pbasemae, Int64 psag_ara_cn, Int64 psag_four_cn, Int64 ptype_fonct_compta, Int64 pfinanceurs, string psag_n_piece)
    {   
		        c_sag_ec_no = psag_ec_no;
		        c_sag_ea_ligne = psag_ea_ligne;
		        c_sag_d_sage = psag_d_sage;
		        c_sag_jm_date = psag_jm_date;
		        c_sag_ec_jour = psag_ec_jour;
		        c_sag_ec_sens = psag_ec_sens;
		        c_sag_ec_intitule = psag_ec_intitule;
		        c_sag_ca_num = psag_ca_num;
		        c_sag_ea_montant = psag_ea_montant;
		        c_sag_jo_num = psag_jo_num;
		        c_sag_cg_num = psag_cg_num;
		        c_sag_annee_exe = psag_annee_exe;
		c_dicReferences.Add("basemae", pbasemae);
		        c_sag_ara_cn = psag_ara_cn;
		        c_sag_four_cn = psag_four_cn;
		c_dicReferences.Add("type_fonct_compta", ptype_fonct_compta);
		        c_sag_cn = psag_cn;
		c_dicReferences.Add("financeurs", pfinanceurs);
		        c_sag_n_piece = psag_n_piece;

        base.Initialise(psag_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLtb_sage; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_basemae = (clg_basemae) c_ModeleBase.RenvoieObjet(c_dicReferences["basemae"], "clg_basemae");
		c_type_fonct_compta = (clg_type_fonct_compta) c_ModeleBase.RenvoieObjet(c_dicReferences["type_fonct_compta"], "clg_type_fonct_compta");
		c_financeurs = (clg_financeurs) c_ModeleBase.RenvoieObjet(c_dicReferences["financeurs"], "clg_financeurs");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listetb_sage.Dictionnaire.Add(sag_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listetb_sage.Dictionnaire.Remove(sag_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_basemae != null)if(!c_basemae.Listetb_sage.Contains(this)) c_basemae.Listetb_sage.Add(this);
		if(c_type_fonct_compta != null)if(!c_type_fonct_compta.Listetb_sage.Contains(this)) c_type_fonct_compta.Listetb_sage.Add(this);
		if(c_financeurs != null)if(!c_financeurs.Listetb_sage.Contains(this)) c_financeurs.Listetb_sage.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_tb_sage l_Clone = (clg_tb_sage) Clone;
		if(l_Clone.basemae != null)if(l_Clone.basemae.Listetb_sage.Contains(this)) l_Clone.basemae.Listetb_sage.Remove(this);
		if(l_Clone.type_fonct_compta != null)if(l_Clone.type_fonct_compta.Listetb_sage.Contains(this)) l_Clone.type_fonct_compta.Listetb_sage.Remove(this);
		if(l_Clone.financeurs != null)if(l_Clone.financeurs.Listetb_sage.Contains(this)) l_Clone.financeurs.Listetb_sage.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_tb_sage(null, sag_cn, sag_ec_no, sag_ea_ligne, sag_d_sage, sag_jm_date, sag_ec_jour, sag_ec_sens, sag_ec_intitule, sag_ca_num, sag_ea_montant, sag_jo_num, sag_cg_num, sag_annee_exe, basemae, sag_ara_cn, sag_four_cn, type_fonct_compta, financeurs, sag_n_piece,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_tb_sage l_clone = (clg_tb_sage) this.Clone;
		c_sag_ec_no = l_clone.sag_ec_no;
		c_sag_ea_ligne = l_clone.sag_ea_ligne;
		c_sag_d_sage = l_clone.sag_d_sage;
		c_sag_jm_date = l_clone.sag_jm_date;
		c_sag_ec_jour = l_clone.sag_ec_jour;
		c_sag_ec_sens = l_clone.sag_ec_sens;
		c_sag_ec_intitule = l_clone.sag_ec_intitule;
		c_sag_ca_num = l_clone.sag_ca_num;
		c_sag_ea_montant = l_clone.sag_ea_montant;
		c_sag_jo_num = l_clone.sag_jo_num;
		c_sag_cg_num = l_clone.sag_cg_num;
		c_sag_annee_exe = l_clone.sag_annee_exe;
		c_basemae = l_clone.basemae;
		c_sag_ara_cn = l_clone.sag_ara_cn;
		c_sag_four_cn = l_clone.sag_four_cn;
		c_type_fonct_compta = l_clone.type_fonct_compta;
		c_sag_cn = l_clone.sag_cn;
		c_financeurs = l_clone.financeurs;
		c_sag_n_piece = l_clone.sag_n_piece;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLtb_sage.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLtb_sage.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLtb_sage.Delete(this);
    }

    /* Accesseur de la propriete sag_ec_no (sag_ec_no)
    * @return c_sag_ec_no */
    public Int64 sag_ec_no
    {
        get{return c_sag_ec_no;}
        set
        {
            if(c_sag_ec_no != value)
            {
                CreerClone();
                c_sag_ec_no = value;
            }
        }
    }
    /* Accesseur de la propriete sag_ea_ligne (sag_ea_ligne)
    * @return c_sag_ea_ligne */
    public Int64 sag_ea_ligne
    {
        get{return c_sag_ea_ligne;}
        set
        {
            if(c_sag_ea_ligne != value)
            {
                CreerClone();
                c_sag_ea_ligne = value;
            }
        }
    }
    /* Accesseur de la propriete sag_d_sage (sag_d_sage)
    * @return c_sag_d_sage */
    public DateTime sag_d_sage
    {
        get{return c_sag_d_sage;}
        set
        {
            if(c_sag_d_sage != value)
            {
                CreerClone();
                c_sag_d_sage = value;
            }
        }
    }
    /* Accesseur de la propriete sag_jm_date (sag_jm_date)
    * @return c_sag_jm_date */
    public DateTime sag_jm_date
    {
        get{return c_sag_jm_date;}
        set
        {
            if(c_sag_jm_date != value)
            {
                CreerClone();
                c_sag_jm_date = value;
            }
        }
    }
    /* Accesseur de la propriete sag_ec_jour (sag_ec_jour)
    * @return c_sag_ec_jour */
    public Int64 sag_ec_jour
    {
        get{return c_sag_ec_jour;}
        set
        {
            if(c_sag_ec_jour != value)
            {
                CreerClone();
                c_sag_ec_jour = value;
            }
        }
    }
    /* Accesseur de la propriete sag_ec_sens (sag_ec_sens)
    * @return c_sag_ec_sens */
    public Int64 sag_ec_sens
    {
        get{return c_sag_ec_sens;}
        set
        {
            if(c_sag_ec_sens != value)
            {
                CreerClone();
                c_sag_ec_sens = value;
            }
        }
    }
    /* Accesseur de la propriete sag_ec_intitule (sag_ec_intitule)
    * @return c_sag_ec_intitule */
    public string sag_ec_intitule
    {
        get{return c_sag_ec_intitule;}
        set
        {
            if(c_sag_ec_intitule != value)
            {
                CreerClone();
                c_sag_ec_intitule = value;
            }
        }
    }
    /* Accesseur de la propriete sag_ca_num (sag_ca_num)
    * @return c_sag_ca_num */
    public string sag_ca_num
    {
        get{return c_sag_ca_num;}
        set
        {
            if(c_sag_ca_num != value)
            {
                CreerClone();
                c_sag_ca_num = value;
            }
        }
    }
    /* Accesseur de la propriete sag_ea_montant (sag_ea_montant)
    * @return c_sag_ea_montant */
    public double sag_ea_montant
    {
        get{return c_sag_ea_montant;}
        set
        {
            if(c_sag_ea_montant != value)
            {
                CreerClone();
                c_sag_ea_montant = value;
            }
        }
    }
    /* Accesseur de la propriete sag_jo_num (sag_jo_num)
    * @return c_sag_jo_num */
    public string sag_jo_num
    {
        get{return c_sag_jo_num;}
        set
        {
            if(c_sag_jo_num != value)
            {
                CreerClone();
                c_sag_jo_num = value;
            }
        }
    }
    /* Accesseur de la propriete sag_cg_num (sag_cg_num)
    * @return c_sag_cg_num */
    public string sag_cg_num
    {
        get{return c_sag_cg_num;}
        set
        {
            if(c_sag_cg_num != value)
            {
                CreerClone();
                c_sag_cg_num = value;
            }
        }
    }
    /* Accesseur de la propriete sag_annee_exe (sag_annee_exe)
    * @return c_sag_annee_exe */
    public Int64 sag_annee_exe
    {
        get{return c_sag_annee_exe;}
        set
        {
            if(c_sag_annee_exe != value)
            {
                CreerClone();
                c_sag_annee_exe = value;
            }
        }
    }
    /* Accesseur de la propriete basemae (sag_bas_cn)
    * @return c_basemae */
    public clg_basemae basemae
    {
        get{return c_basemae;}
        set
        {
            if(c_basemae != value)
            {
                CreerClone();
                c_basemae = value;
            }
        }
    }
    /* Accesseur de la propriete sag_ara_cn (sag_ara_cn)
    * @return c_sag_ara_cn */
    public Int64 sag_ara_cn
    {
        get{return c_sag_ara_cn;}
        set
        {
            if(c_sag_ara_cn != value)
            {
                CreerClone();
                c_sag_ara_cn = value;
            }
        }
    }
    /* Accesseur de la propriete sag_four_cn (sag_four_cn)
    * @return c_sag_four_cn */
    public Int64 sag_four_cn
    {
        get{return c_sag_four_cn;}
        set
        {
            if(c_sag_four_cn != value)
            {
                CreerClone();
                c_sag_four_cn = value;
            }
        }
    }
    /* Accesseur de la propriete type_fonct_compta (sag_tfc_cn)
    * @return c_type_fonct_compta */
    public clg_type_fonct_compta type_fonct_compta
    {
        get{return c_type_fonct_compta;}
        set
        {
            if(c_type_fonct_compta != value)
            {
                CreerClone();
                c_type_fonct_compta = value;
            }
        }
    }
    /* Accesseur de la propriete sag_cn (sag_cn)
    * @return c_sag_cn */
    public int sag_cn
    {
        get{return c_sag_cn;}
        set
        {
            if(value != int.MinValue)
            {
                if(c_sag_cn != value)
                {
                    CreerClone();
                    c_sag_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete financeurs (sag_fin_cn)
    * @return c_financeurs */
    public clg_financeurs financeurs
    {
        get{return c_financeurs;}
        set
        {
            if(c_financeurs != value)
            {
                CreerClone();
                c_financeurs = value;
            }
        }
    }
    /* Accesseur de la propriete sag_n_piece (sag_n_piece)
    * @return c_sag_n_piece */
    public string sag_n_piece
    {
        get{return c_sag_n_piece;}
        set
        {
            if(c_sag_n_piece != value)
            {
                CreerClone();
                c_sag_n_piece = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_sag_ec_no.ToString());
		l_Valeurs.Add(c_sag_ea_ligne.ToString());
		l_Valeurs.Add(c_sag_d_sage.ToString());
		l_Valeurs.Add(c_sag_jm_date.ToString());
		l_Valeurs.Add(c_sag_ec_jour.ToString());
		l_Valeurs.Add(c_sag_ec_sens.ToString());
		l_Valeurs.Add(c_sag_ec_intitule.ToString());
		l_Valeurs.Add(c_sag_ca_num.ToString());
		l_Valeurs.Add(c_sag_ea_montant.ToString());
		l_Valeurs.Add(c_sag_jo_num.ToString());
		l_Valeurs.Add(c_sag_cg_num.ToString());
		l_Valeurs.Add(c_sag_annee_exe.ToString());
		l_Valeurs.Add(c_basemae==null ? "-1" : c_basemae.ID.ToString());
		l_Valeurs.Add(c_sag_ara_cn.ToString());
		l_Valeurs.Add(c_sag_four_cn.ToString());
		l_Valeurs.Add(c_type_fonct_compta==null ? "-1" : c_type_fonct_compta.ID.ToString());
		l_Valeurs.Add(c_sag_cn.ToString());
		l_Valeurs.Add(c_financeurs==null ? "-1" : c_financeurs.ID.ToString());
		l_Valeurs.Add(c_sag_n_piece.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("sag_ec_no");
		l_Noms.Add("sag_ea_ligne");
		l_Noms.Add("sag_d_sage");
		l_Noms.Add("sag_jm_date");
		l_Noms.Add("sag_ec_jour");
		l_Noms.Add("sag_ec_sens");
		l_Noms.Add("sag_ec_intitule");
		l_Noms.Add("sag_ca_num");
		l_Noms.Add("sag_ea_montant");
		l_Noms.Add("sag_jo_num");
		l_Noms.Add("sag_cg_num");
		l_Noms.Add("sag_annee_exe");
		l_Noms.Add("basemae");
		l_Noms.Add("sag_ara_cn");
		l_Noms.Add("sag_four_cn");
		l_Noms.Add("type_fonct_compta");
		l_Noms.Add("sag_cn");
		l_Noms.Add("financeurs");
		l_Noms.Add("sag_n_piece");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
    }
}
