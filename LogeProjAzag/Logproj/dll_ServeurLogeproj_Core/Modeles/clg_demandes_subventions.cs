namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : demandes_subventions </summary>
public partial class clg_demandes_subventions : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLdemandes_subventions;
    protected clg_Modele c_Modele;

	private Int64 c_dms_cn;
	private clg_annee_comptable c_annee_comptable;
	private string c_dms_a_lib;
	private DateTime c_dms_d_envoi;
	private double c_dms_n_montant;
	private clg_financeurs c_financeurs;
	private List<clg_comment_demandes> c_Listecomment_demandes;


    private void Init()
    {
		c_Listecomment_demandes = new List<clg_comment_demandes>();

    }

    public override void Detruit()
    {
		c_Listecomment_demandes.Clear();
		this.c_annee_comptable = null;
		this.c_financeurs = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_demandes_subventions(clg_Modele pModele, Int64 pdms_cn, bool pAMAJ = false) : base(pModele, pdms_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_demandes_subventions(clg_Modele pModele, Int64 pdms_cn, clg_annee_comptable pannee_comptable, string pdms_a_lib, DateTime pdms_d_envoi, double pdms_n_montant, clg_financeurs pfinanceurs, bool pAMAJ = true) : base(pModele, pdms_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pdms_cn != Int64.MinValue)
            c_dms_cn = pdms_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pannee_comptable != null)
            c_annee_comptable = pannee_comptable;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_dms_a_lib = pdms_a_lib;
		        c_dms_d_envoi = pdms_d_envoi;
		        c_dms_n_montant = pdms_n_montant;
		        if(pfinanceurs != null)
            c_financeurs = pfinanceurs;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pdms_cn, Int64 pannee_comptable, string pdms_a_lib, DateTime pdms_d_envoi, double pdms_n_montant, Int64 pfinanceurs)
    {   
		        c_dms_cn = pdms_cn;
		c_dicReferences.Add("annee_comptable", pannee_comptable);
		        c_dms_a_lib = pdms_a_lib;
		        c_dms_d_envoi = pdms_d_envoi;
		        c_dms_n_montant = pdms_n_montant;
		c_dicReferences.Add("financeurs", pfinanceurs);

        base.Initialise(pdms_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLdemandes_subventions; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_annee_comptable = (clg_annee_comptable) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_comptable"], "clg_annee_comptable");
		c_financeurs = (clg_financeurs) c_ModeleBase.RenvoieObjet(c_dicReferences["financeurs"], "clg_financeurs");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listedemandes_subventions.Dictionnaire.Add(dms_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listedemandes_subventions.Dictionnaire.Remove(dms_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_annee_comptable != null)if(!c_annee_comptable.Listedemandes_subventions.Contains(this)) c_annee_comptable.Listedemandes_subventions.Add(this);
		if(c_financeurs != null)if(!c_financeurs.Listedemandes_subventions.Contains(this)) c_financeurs.Listedemandes_subventions.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_demandes_subventions l_Clone = (clg_demandes_subventions) Clone;
		if(l_Clone.annee_comptable != null)if(l_Clone.annee_comptable.Listedemandes_subventions.Contains(this)) l_Clone.annee_comptable.Listedemandes_subventions.Remove(this);
		if(l_Clone.financeurs != null)if(l_Clone.financeurs.Listedemandes_subventions.Contains(this)) l_Clone.financeurs.Listedemandes_subventions.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecomment_demandes.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_demandes_subventions(null, dms_cn, annee_comptable, dms_a_lib, dms_d_envoi, dms_n_montant, financeurs,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_demandes_subventions l_clone = (clg_demandes_subventions) this.Clone;
		c_dms_cn = l_clone.dms_cn;
		c_annee_comptable = l_clone.annee_comptable;
		c_dms_a_lib = l_clone.dms_a_lib;
		c_dms_d_envoi = l_clone.dms_d_envoi;
		c_dms_n_montant = l_clone.dms_n_montant;
		c_financeurs = l_clone.financeurs;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLdemandes_subventions.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLdemandes_subventions.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLdemandes_subventions.Delete(this);
    }

    /* Accesseur de la propriete dms_cn (dms_cn)
    * @return c_dms_cn */
    public Int64 dms_cn
    {
        get{return c_dms_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_dms_cn != value)
                {
                    CreerClone();
                    c_dms_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete annee_comptable (dms_acp_cn)
    * @return c_annee_comptable */
    public clg_annee_comptable annee_comptable
    {
        get{return c_annee_comptable;}
        set
        {
            if(value != null)
            {
                if(c_annee_comptable != value)
                {
                    CreerClone();
                    c_annee_comptable = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete dms_a_lib (dms_a_lib)
    * @return c_dms_a_lib */
    public string dms_a_lib
    {
        get{return c_dms_a_lib;}
        set
        {
            if(c_dms_a_lib != value)
            {
                CreerClone();
                c_dms_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete dms_d_envoi (dms_d_envoi)
    * @return c_dms_d_envoi */
    public DateTime dms_d_envoi
    {
        get{return c_dms_d_envoi;}
        set
        {
            if(c_dms_d_envoi != value)
            {
                CreerClone();
                c_dms_d_envoi = value;
            }
        }
    }
    /* Accesseur de la propriete dms_n_montant (dms_n_montant)
    * @return c_dms_n_montant */
    public double dms_n_montant
    {
        get{return c_dms_n_montant;}
        set
        {
            if(c_dms_n_montant != value)
            {
                CreerClone();
                c_dms_n_montant = value;
            }
        }
    }
    /* Accesseur de la propriete financeurs (dms_fin_cn)
    * @return c_financeurs */
    public clg_financeurs financeurs
    {
        get{return c_financeurs;}
        set
        {
            if(value != null)
            {
                if(c_financeurs != value)
                {
                    CreerClone();
                    c_financeurs = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type comment_demandes */
    public List<clg_comment_demandes> Listecomment_demandes
    {
        get { return c_Listecomment_demandes; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_dms_cn.ToString());
		l_Valeurs.Add(c_annee_comptable==null ? "-1" : c_annee_comptable.ID.ToString());
		l_Valeurs.Add(c_dms_a_lib.ToString());
		l_Valeurs.Add(c_dms_d_envoi.ToString());
		l_Valeurs.Add(c_dms_n_montant.ToString());
		l_Valeurs.Add(c_financeurs==null ? "-1" : c_financeurs.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("dms_cn");
		l_Noms.Add("annee_comptable");
		l_Noms.Add("dms_a_lib");
		l_Noms.Add("dms_d_envoi");
		l_Noms.Add("dms_n_montant");
		l_Noms.Add("financeurs");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
