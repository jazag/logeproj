namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : frais_generaux </summary>
public partial class clg_frais_generaux : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfrais_generaux;
    protected clg_Modele c_Modele;

	private Int64 c_frg_cn;
	private clg_annee_comptable c_annee_comptable;
	private double c_frg_n_frais;
	private Int64 c_frg_n_massesal;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_annee_comptable = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_frais_generaux(clg_Modele pModele, Int64 pfrg_cn, bool pAMAJ = false) : base(pModele, pfrg_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_frais_generaux(clg_Modele pModele, Int64 pfrg_cn, clg_annee_comptable pannee_comptable, double pfrg_n_frais, Int64 pfrg_n_massesal, bool pAMAJ = true) : base(pModele, pfrg_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfrg_cn != Int64.MinValue)
            c_frg_cn = pfrg_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pannee_comptable != null)
            c_annee_comptable = pannee_comptable;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_frg_n_frais = pfrg_n_frais;
		        c_frg_n_massesal = pfrg_n_massesal;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfrg_cn, Int64 pannee_comptable, double pfrg_n_frais, Int64 pfrg_n_massesal)
    {   
		        c_frg_cn = pfrg_cn;
		c_dicReferences.Add("annee_comptable", pannee_comptable);
		        c_frg_n_frais = pfrg_n_frais;
		        c_frg_n_massesal = pfrg_n_massesal;

        base.Initialise(pfrg_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfrais_generaux; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_annee_comptable = (clg_annee_comptable) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_comptable"], "clg_annee_comptable");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefrais_generaux.Dictionnaire.Add(frg_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefrais_generaux.Dictionnaire.Remove(frg_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_annee_comptable != null)if(!c_annee_comptable.Listefrais_generaux.Contains(this)) c_annee_comptable.Listefrais_generaux.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_frais_generaux l_Clone = (clg_frais_generaux) Clone;
		if(l_Clone.annee_comptable != null)if(l_Clone.annee_comptable.Listefrais_generaux.Contains(this)) l_Clone.annee_comptable.Listefrais_generaux.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_frais_generaux(null, frg_cn, annee_comptable, frg_n_frais, frg_n_massesal,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_frais_generaux l_clone = (clg_frais_generaux) this.Clone;
		c_frg_cn = l_clone.frg_cn;
		c_annee_comptable = l_clone.annee_comptable;
		c_frg_n_frais = l_clone.frg_n_frais;
		c_frg_n_massesal = l_clone.frg_n_massesal;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfrais_generaux.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfrais_generaux.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfrais_generaux.Delete(this);
    }

    /* Accesseur de la propriete frg_cn (frg_cn)
    * @return c_frg_cn */
    public Int64 frg_cn
    {
        get{return c_frg_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_frg_cn != value)
                {
                    CreerClone();
                    c_frg_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete annee_comptable (frg_acp_cn)
    * @return c_annee_comptable */
    public clg_annee_comptable annee_comptable
    {
        get{return c_annee_comptable;}
        set
        {
            if(value != null)
            {
                if(c_annee_comptable != value)
                {
                    CreerClone();
                    c_annee_comptable = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete frg_n_frais (frg_n_frais)
    * @return c_frg_n_frais */
    public double frg_n_frais
    {
        get{return c_frg_n_frais;}
        set
        {
            if(c_frg_n_frais != value)
            {
                CreerClone();
                c_frg_n_frais = value;
            }
        }
    }
    /* Accesseur de la propriete frg_n_massesal (frg_n_massesal)
    * @return c_frg_n_massesal */
    public Int64 frg_n_massesal
    {
        get{return c_frg_n_massesal;}
        set
        {
            if(c_frg_n_massesal != value)
            {
                CreerClone();
                c_frg_n_massesal = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_frg_cn.ToString());
		l_Valeurs.Add(c_annee_comptable==null ? "-1" : c_annee_comptable.ID.ToString());
		l_Valeurs.Add(c_frg_n_frais.ToString());
		l_Valeurs.Add(c_frg_n_massesal.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("frg_cn");
		l_Noms.Add("annee_comptable");
		l_Noms.Add("frg_n_frais");
		l_Noms.Add("frg_n_massesal");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
