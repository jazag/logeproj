namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : annee_civile </summary>
public partial class clg_annee_civile : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLannee_civile;
    protected clg_Modele c_Modele;

	private Int64 c_ann_cn;
	private string c_ann_a_lib;
	private List<clg_operations> c_Listeoperations;
	private List<clg_report> c_Listereport;
	private List<clg_mensuel> c_Listemensuel;
	private List<clg_temps_travail_annuel> c_Listetemps_travail_annuel;
	private List<clg_solde_conges> c_Listesolde_conges;


    private void Init()
    {
		c_Listeoperations = new List<clg_operations>();
		c_Listereport = new List<clg_report>();
		c_Listemensuel = new List<clg_mensuel>();
		c_Listetemps_travail_annuel = new List<clg_temps_travail_annuel>();
		c_Listesolde_conges = new List<clg_solde_conges>();

    }

    public override void Detruit()
    {
		c_Listeoperations.Clear();
		c_Listereport.Clear();
		c_Listemensuel.Clear();
		c_Listetemps_travail_annuel.Clear();
		c_Listesolde_conges.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_annee_civile(clg_Modele pModele, Int64 pann_cn, bool pAMAJ = false) : base(pModele, pann_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_annee_civile(clg_Modele pModele, Int64 pann_cn, string pann_a_lib, bool pAMAJ = true) : base(pModele, pann_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pann_cn != Int64.MinValue)
            c_ann_cn = pann_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ann_a_lib = pann_a_lib;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pann_cn, string pann_a_lib)
    {   
		        c_ann_cn = pann_cn;
		        c_ann_a_lib = pann_a_lib;

        base.Initialise(pann_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLannee_civile; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeannee_civile.Dictionnaire.Add(ann_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeannee_civile.Dictionnaire.Remove(ann_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_annee_civile l_Clone = (clg_annee_civile) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeoperations.Count > 0) c_EstReference = true;
		if(c_Listereport.Count > 0) c_EstReference = true;
		if(c_Listemensuel.Count > 0) c_EstReference = true;
		if(c_Listetemps_travail_annuel.Count > 0) c_EstReference = true;
		if(c_Listesolde_conges.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_annee_civile(null, ann_cn, ann_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_annee_civile l_clone = (clg_annee_civile) this.Clone;
		c_ann_cn = l_clone.ann_cn;
		c_ann_a_lib = l_clone.ann_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLannee_civile.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLannee_civile.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLannee_civile.Delete(this);
    }

    /* Accesseur de la propriete ann_cn (ann_cn)
    * @return c_ann_cn */
    public Int64 ann_cn
    {
        get{return c_ann_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ann_cn != value)
                {
                    CreerClone();
                    c_ann_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ann_a_lib (ann_a_lib)
    * @return c_ann_a_lib */
    public string ann_a_lib
    {
        get{return c_ann_a_lib;}
        set
        {
            if(c_ann_a_lib != value)
            {
                CreerClone();
                c_ann_a_lib = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type operations */
    public List<clg_operations> Listeoperations
    {
        get { return c_Listeoperations; }
    }    /* Accesseur de la liste des objets de type report */
    public List<clg_report> Listereport
    {
        get { return c_Listereport; }
    }    /* Accesseur de la liste des objets de type mensuel */
    public List<clg_mensuel> Listemensuel
    {
        get { return c_Listemensuel; }
    }    /* Accesseur de la liste des objets de type temps_travail_annuel */
    public List<clg_temps_travail_annuel> Listetemps_travail_annuel
    {
        get { return c_Listetemps_travail_annuel; }
    }    /* Accesseur de la liste des objets de type solde_conges */
    public List<clg_solde_conges> Listesolde_conges
    {
        get { return c_Listesolde_conges; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ann_cn.ToString());
		l_Valeurs.Add(c_ann_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ann_cn");
		l_Noms.Add("ann_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
