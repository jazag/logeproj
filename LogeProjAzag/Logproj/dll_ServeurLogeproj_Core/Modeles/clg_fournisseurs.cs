namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : fournisseurs </summary>
public partial class clg_fournisseurs : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfournisseurs;
    protected clg_Modele c_Modele;

	private Int64 c_four_cn;
	private string c_four_a_lib;
	private string c_four_a_nom_res;
	private string c_four_a_prenom_res;
	private Int64 c_four_n_archive;
	private string c_four_a_code;
	private string c_four_a_adresse;
	private string c_four_a_cp;
	private string c_four_a_ville;
	private string c_four_a_tel;
	private string c_four_a_fax;
	private string c_four_a_mail;
	private List<clg_comment> c_Listecomment;
	private List<clg_lien_sage> c_Listelien_sage;


    private void Init()
    {
		c_Listecomment = new List<clg_comment>();
		c_Listelien_sage = new List<clg_lien_sage>();

    }

    public override void Detruit()
    {
		c_Listecomment.Clear();
		c_Listelien_sage.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_fournisseurs(clg_Modele pModele, Int64 pfour_cn, bool pAMAJ = false) : base(pModele, pfour_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_fournisseurs(clg_Modele pModele, Int64 pfour_cn, string pfour_a_lib, string pfour_a_nom_res, string pfour_a_prenom_res, Int64 pfour_n_archive, string pfour_a_code, string pfour_a_adresse, string pfour_a_cp, string pfour_a_ville, string pfour_a_tel, string pfour_a_fax, string pfour_a_mail, bool pAMAJ = true) : base(pModele, pfour_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfour_cn != Int64.MinValue)
            c_four_cn = pfour_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_four_a_lib = pfour_a_lib;
		        c_four_a_nom_res = pfour_a_nom_res;
		        c_four_a_prenom_res = pfour_a_prenom_res;
		        if(pfour_n_archive != Int64.MinValue)
            c_four_n_archive = pfour_n_archive;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_four_a_code = pfour_a_code;
		        c_four_a_adresse = pfour_a_adresse;
		        c_four_a_cp = pfour_a_cp;
		        c_four_a_ville = pfour_a_ville;
		        c_four_a_tel = pfour_a_tel;
		        c_four_a_fax = pfour_a_fax;
		        c_four_a_mail = pfour_a_mail;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfour_cn, string pfour_a_lib, string pfour_a_nom_res, string pfour_a_prenom_res, Int64 pfour_n_archive, string pfour_a_code, string pfour_a_adresse, string pfour_a_cp, string pfour_a_ville, string pfour_a_tel, string pfour_a_fax, string pfour_a_mail)
    {   
		        c_four_cn = pfour_cn;
		        c_four_a_lib = pfour_a_lib;
		        c_four_a_nom_res = pfour_a_nom_res;
		        c_four_a_prenom_res = pfour_a_prenom_res;
		        c_four_n_archive = pfour_n_archive;
		        c_four_a_code = pfour_a_code;
		        c_four_a_adresse = pfour_a_adresse;
		        c_four_a_cp = pfour_a_cp;
		        c_four_a_ville = pfour_a_ville;
		        c_four_a_tel = pfour_a_tel;
		        c_four_a_fax = pfour_a_fax;
		        c_four_a_mail = pfour_a_mail;

        base.Initialise(pfour_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfournisseurs; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefournisseurs.Dictionnaire.Add(four_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefournisseurs.Dictionnaire.Remove(four_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_fournisseurs l_Clone = (clg_fournisseurs) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listecomment.Count > 0) c_EstReference = true;
		if(c_Listelien_sage.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_fournisseurs(null, four_cn, four_a_lib, four_a_nom_res, four_a_prenom_res, four_n_archive, four_a_code, four_a_adresse, four_a_cp, four_a_ville, four_a_tel, four_a_fax, four_a_mail,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_fournisseurs l_clone = (clg_fournisseurs) this.Clone;
		c_four_cn = l_clone.four_cn;
		c_four_a_lib = l_clone.four_a_lib;
		c_four_a_nom_res = l_clone.four_a_nom_res;
		c_four_a_prenom_res = l_clone.four_a_prenom_res;
		c_four_n_archive = l_clone.four_n_archive;
		c_four_a_code = l_clone.four_a_code;
		c_four_a_adresse = l_clone.four_a_adresse;
		c_four_a_cp = l_clone.four_a_cp;
		c_four_a_ville = l_clone.four_a_ville;
		c_four_a_tel = l_clone.four_a_tel;
		c_four_a_fax = l_clone.four_a_fax;
		c_four_a_mail = l_clone.four_a_mail;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfournisseurs.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfournisseurs.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfournisseurs.Delete(this);
    }

    /* Accesseur de la propriete four_cn (four_cn)
    * @return c_four_cn */
    public Int64 four_cn
    {
        get{return c_four_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_four_cn != value)
                {
                    CreerClone();
                    c_four_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete four_a_lib (four_a_lib)
    * @return c_four_a_lib */
    public string four_a_lib
    {
        get{return c_four_a_lib;}
        set
        {
            if(c_four_a_lib != value)
            {
                CreerClone();
                c_four_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete four_a_nom_res (four_a_nom_res)
    * @return c_four_a_nom_res */
    public string four_a_nom_res
    {
        get{return c_four_a_nom_res;}
        set
        {
            if(c_four_a_nom_res != value)
            {
                CreerClone();
                c_four_a_nom_res = value;
            }
        }
    }
    /* Accesseur de la propriete four_a_prenom_res (four_a_prenom_res)
    * @return c_four_a_prenom_res */
    public string four_a_prenom_res
    {
        get{return c_four_a_prenom_res;}
        set
        {
            if(c_four_a_prenom_res != value)
            {
                CreerClone();
                c_four_a_prenom_res = value;
            }
        }
    }
    /* Accesseur de la propriete four_n_archive (four_n_archive)
    * @return c_four_n_archive */
    public Int64 four_n_archive
    {
        get{return c_four_n_archive;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_four_n_archive != value)
                {
                    CreerClone();
                    c_four_n_archive = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete four_a_code (four_a_code)
    * @return c_four_a_code */
    public string four_a_code
    {
        get{return c_four_a_code;}
        set
        {
            if(c_four_a_code != value)
            {
                CreerClone();
                c_four_a_code = value;
            }
        }
    }
    /* Accesseur de la propriete four_a_adresse (four_a_adresse)
    * @return c_four_a_adresse */
    public string four_a_adresse
    {
        get{return c_four_a_adresse;}
        set
        {
            if(c_four_a_adresse != value)
            {
                CreerClone();
                c_four_a_adresse = value;
            }
        }
    }
    /* Accesseur de la propriete four_a_cp (four_a_cp)
    * @return c_four_a_cp */
    public string four_a_cp
    {
        get{return c_four_a_cp;}
        set
        {
            if(c_four_a_cp != value)
            {
                CreerClone();
                c_four_a_cp = value;
            }
        }
    }
    /* Accesseur de la propriete four_a_ville (four_a_ville)
    * @return c_four_a_ville */
    public string four_a_ville
    {
        get{return c_four_a_ville;}
        set
        {
            if(c_four_a_ville != value)
            {
                CreerClone();
                c_four_a_ville = value;
            }
        }
    }
    /* Accesseur de la propriete four_a_tel (four_a_tel)
    * @return c_four_a_tel */
    public string four_a_tel
    {
        get{return c_four_a_tel;}
        set
        {
            if(c_four_a_tel != value)
            {
                CreerClone();
                c_four_a_tel = value;
            }
        }
    }
    /* Accesseur de la propriete four_a_fax (four_a_fax)
    * @return c_four_a_fax */
    public string four_a_fax
    {
        get{return c_four_a_fax;}
        set
        {
            if(c_four_a_fax != value)
            {
                CreerClone();
                c_four_a_fax = value;
            }
        }
    }
    /* Accesseur de la propriete four_a_mail (four_a_mail)
    * @return c_four_a_mail */
    public string four_a_mail
    {
        get{return c_four_a_mail;}
        set
        {
            if(c_four_a_mail != value)
            {
                CreerClone();
                c_four_a_mail = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type comment */
    public List<clg_comment> Listecomment
    {
        get { return c_Listecomment; }
    }    /* Accesseur de la liste des objets de type lien_sage */
    public List<clg_lien_sage> Listelien_sage
    {
        get { return c_Listelien_sage; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_four_cn.ToString());
		l_Valeurs.Add(c_four_a_lib.ToString());
		l_Valeurs.Add(c_four_a_nom_res.ToString());
		l_Valeurs.Add(c_four_a_prenom_res.ToString());
		l_Valeurs.Add(c_four_n_archive.ToString());
		l_Valeurs.Add(c_four_a_code.ToString());
		l_Valeurs.Add(c_four_a_adresse.ToString());
		l_Valeurs.Add(c_four_a_cp.ToString());
		l_Valeurs.Add(c_four_a_ville.ToString());
		l_Valeurs.Add(c_four_a_tel.ToString());
		l_Valeurs.Add(c_four_a_fax.ToString());
		l_Valeurs.Add(c_four_a_mail.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("four_cn");
		l_Noms.Add("four_a_lib");
		l_Noms.Add("four_a_nom_res");
		l_Noms.Add("four_a_prenom_res");
		l_Noms.Add("four_n_archive");
		l_Noms.Add("four_a_code");
		l_Noms.Add("four_a_adresse");
		l_Noms.Add("four_a_cp");
		l_Noms.Add("four_a_ville");
		l_Noms.Add("four_a_tel");
		l_Noms.Add("four_a_fax");
		l_Noms.Add("four_a_mail");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }


#endregion
}
}
