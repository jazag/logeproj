namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : report </summary>
public partial class clg_report : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLreport;
    protected clg_Modele c_Modele;

	private clg_operations c_operations;
	private clg_annee_civile c_annee_civile;
	private clg_temps_intervenants c_temps_intervenants;
	private double c_rpt_n_heures;
	private Int64 c_rpt_cn;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_operations = null;
		this.c_annee_civile = null;
		this.c_temps_intervenants = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_report(clg_Modele pModele, Int64 prpt_cn, bool pAMAJ = false) : base(pModele, prpt_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_report(clg_Modele pModele, Int64 prpt_cn, clg_operations poperations, clg_annee_civile pannee_civile, clg_temps_intervenants ptemps_intervenants, double prpt_n_heures, bool pAMAJ = true) : base(pModele, prpt_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        c_operations = poperations;
		        c_annee_civile = pannee_civile;
		        c_temps_intervenants = ptemps_intervenants;
		        c_rpt_n_heures = prpt_n_heures;
		        if(prpt_cn != Int64.MinValue)
            c_rpt_cn = prpt_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 prpt_cn, Int64 poperations, Int64 pannee_civile, Int64 ptemps_intervenants, double prpt_n_heures)
    {   
		c_dicReferences.Add("operations", poperations);
		c_dicReferences.Add("annee_civile", pannee_civile);
		c_dicReferences.Add("temps_intervenants", ptemps_intervenants);
		        c_rpt_n_heures = prpt_n_heures;
		        c_rpt_cn = prpt_cn;

        base.Initialise(prpt_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLreport; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_operations = (clg_operations) c_ModeleBase.RenvoieObjet(c_dicReferences["operations"], "clg_operations");
		c_annee_civile = (clg_annee_civile) c_ModeleBase.RenvoieObjet(c_dicReferences["annee_civile"], "clg_annee_civile");
		c_temps_intervenants = (clg_temps_intervenants) c_ModeleBase.RenvoieObjet(c_dicReferences["temps_intervenants"], "clg_temps_intervenants");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listereport.Dictionnaire.Add(rpt_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listereport.Dictionnaire.Remove(rpt_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_operations != null)if(!c_operations.Listereport.Contains(this)) c_operations.Listereport.Add(this);
		if(c_annee_civile != null)if(!c_annee_civile.Listereport.Contains(this)) c_annee_civile.Listereport.Add(this);
		if(c_temps_intervenants != null)if(!c_temps_intervenants.Listereport.Contains(this)) c_temps_intervenants.Listereport.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_report l_Clone = (clg_report) Clone;
		if(l_Clone.operations != null)if(l_Clone.operations.Listereport.Contains(this)) l_Clone.operations.Listereport.Remove(this);
		if(l_Clone.annee_civile != null)if(l_Clone.annee_civile.Listereport.Contains(this)) l_Clone.annee_civile.Listereport.Remove(this);
		if(l_Clone.temps_intervenants != null)if(l_Clone.temps_intervenants.Listereport.Contains(this)) l_Clone.temps_intervenants.Listereport.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_report(null, rpt_cn, operations, annee_civile, temps_intervenants, rpt_n_heures,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_report l_clone = (clg_report) this.Clone;
		c_operations = l_clone.operations;
		c_annee_civile = l_clone.annee_civile;
		c_temps_intervenants = l_clone.temps_intervenants;
		c_rpt_n_heures = l_clone.rpt_n_heures;
		c_rpt_cn = l_clone.rpt_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLreport.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLreport.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLreport.Delete(this);
    }

    /* Accesseur de la propriete operations (rpt_ope_ele_cn)
    * @return c_operations */
    public clg_operations operations
    {
        get{return c_operations;}
        set
        {
            if(c_operations != value)
            {
                CreerClone();
                c_operations = value;
            }
        }
    }
    /* Accesseur de la propriete annee_civile (rpt_ann_cn)
    * @return c_annee_civile */
    public clg_annee_civile annee_civile
    {
        get{return c_annee_civile;}
        set
        {
            if(c_annee_civile != value)
            {
                CreerClone();
                c_annee_civile = value;
            }
        }
    }
    /* Accesseur de la propriete temps_intervenants (rpt_tit_cn)
    * @return c_temps_intervenants */
    public clg_temps_intervenants temps_intervenants
    {
        get{return c_temps_intervenants;}
        set
        {
            if(c_temps_intervenants != value)
            {
                CreerClone();
                c_temps_intervenants = value;
            }
        }
    }
    /* Accesseur de la propriete rpt_n_heures (rpt_n_heures)
    * @return c_rpt_n_heures */
    public double rpt_n_heures
    {
        get{return c_rpt_n_heures;}
        set
        {
            if(c_rpt_n_heures != value)
            {
                CreerClone();
                c_rpt_n_heures = value;
            }
        }
    }
    /* Accesseur de la propriete rpt_cn (rpt_cn)
    * @return c_rpt_cn */
    public Int64 rpt_cn
    {
        get{return c_rpt_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_rpt_cn != value)
                {
                    CreerClone();
                    c_rpt_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_operations==null ? "-1" : c_operations.ID.ToString());
		l_Valeurs.Add(c_annee_civile==null ? "-1" : c_annee_civile.ID.ToString());
		l_Valeurs.Add(c_temps_intervenants==null ? "-1" : c_temps_intervenants.ID.ToString());
		l_Valeurs.Add(c_rpt_n_heures.ToString());
		l_Valeurs.Add(c_rpt_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("operations");
		l_Noms.Add("annee_civile");
		l_Noms.Add("temps_intervenants");
		l_Noms.Add("rpt_n_heures");
		l_Noms.Add("rpt_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
