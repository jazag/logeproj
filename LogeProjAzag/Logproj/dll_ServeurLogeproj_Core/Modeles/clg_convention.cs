namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : convention </summary>
public partial class clg_convention : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLconvention;
    protected clg_Modele c_Modele;

	private Int64 c_cvn_cn;
	private clg_forfait c_forfait;
	private string c_cvn_a_lib;
	private DateTime c_cvn_d_debut;
	private List<clg_convention_regle> c_Listeconvention_regle;


    private void Init()
    {
		c_Listeconvention_regle = new List<clg_convention_regle>();

    }

    public override void Detruit()
    {
		c_Listeconvention_regle.Clear();
		this.c_forfait = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_convention(clg_Modele pModele, Int64 pcvn_cn, bool pAMAJ = false) : base(pModele, pcvn_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_convention(clg_Modele pModele, Int64 pcvn_cn, clg_forfait pforfait, string pcvn_a_lib, DateTime pcvn_d_debut, bool pAMAJ = true) : base(pModele, pcvn_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcvn_cn != Int64.MinValue)
            c_cvn_cn = pcvn_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_forfait = pforfait;
		        c_cvn_a_lib = pcvn_a_lib;
		        c_cvn_d_debut = pcvn_d_debut;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcvn_cn, Int64 pforfait, string pcvn_a_lib, DateTime pcvn_d_debut)
    {   
		        c_cvn_cn = pcvn_cn;
		c_dicReferences.Add("forfait", pforfait);
		        c_cvn_a_lib = pcvn_a_lib;
		        c_cvn_d_debut = pcvn_d_debut;

        base.Initialise(pcvn_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLconvention; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_forfait = (clg_forfait) c_ModeleBase.RenvoieObjet(c_dicReferences["forfait"], "clg_forfait");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeconvention.Dictionnaire.Add(cvn_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeconvention.Dictionnaire.Remove(cvn_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_forfait != null)if(!c_forfait.Listeconvention.Contains(this)) c_forfait.Listeconvention.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_convention l_Clone = (clg_convention) Clone;
		if(l_Clone.forfait != null)if(l_Clone.forfait.Listeconvention.Contains(this)) l_Clone.forfait.Listeconvention.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeconvention_regle.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_convention(null, cvn_cn, forfait, cvn_a_lib, cvn_d_debut,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_convention l_clone = (clg_convention) this.Clone;
		c_cvn_cn = l_clone.cvn_cn;
		c_forfait = l_clone.forfait;
		c_cvn_a_lib = l_clone.cvn_a_lib;
		c_cvn_d_debut = l_clone.cvn_d_debut;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLconvention.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLconvention.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLconvention.Delete(this);
    }

    /* Accesseur de la propriete cvn_cn (cvn_cn)
    * @return c_cvn_cn */
    public Int64 cvn_cn
    {
        get{return c_cvn_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cvn_cn != value)
                {
                    CreerClone();
                    c_cvn_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete forfait (cvn_frf_cn)
    * @return c_forfait */
    public clg_forfait forfait
    {
        get{return c_forfait;}
        set
        {
            if(c_forfait != value)
            {
                CreerClone();
                c_forfait = value;
            }
        }
    }
    /* Accesseur de la propriete cvn_a_lib (cvn_a_lib)
    * @return c_cvn_a_lib */
    public string cvn_a_lib
    {
        get{return c_cvn_a_lib;}
        set
        {
            if(c_cvn_a_lib != value)
            {
                CreerClone();
                c_cvn_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete cvn_d_debut (cvn_d_debut)
    * @return c_cvn_d_debut */
    public DateTime cvn_d_debut
    {
        get{return c_cvn_d_debut;}
        set
        {
            if(c_cvn_d_debut != value)
            {
                CreerClone();
                c_cvn_d_debut = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type convention_regle */
    public List<clg_convention_regle> Listeconvention_regle
    {
        get { return c_Listeconvention_regle; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cvn_cn.ToString());
		l_Valeurs.Add(c_forfait==null ? "-1" : c_forfait.ID.ToString());
		l_Valeurs.Add(c_cvn_a_lib.ToString());
		l_Valeurs.Add(c_cvn_d_debut.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cvn_cn");
		l_Noms.Add("forfait");
		l_Noms.Add("cvn_a_lib");
		l_Noms.Add("cvn_d_debut");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
