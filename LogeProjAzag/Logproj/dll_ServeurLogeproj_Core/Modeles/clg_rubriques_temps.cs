namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : rubriques_temps </summary>
public partial class clg_rubriques_temps : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLrubriques_temps;
    protected clg_Modele c_Modele;

	private Int64 c_rbr_cn;
	private string c_rbr_a_libel;
	private clg_t_rubriques_temps c_t_rubriques_temps;
	private Int64 c_rbr_n_archive;
	private Int64 c_rbr_n_ordre;
	private List<clg_temps_perso_generaux> c_Listetemps_perso_generaux;


    private void Init()
    {
		c_Listetemps_perso_generaux = new List<clg_temps_perso_generaux>();

    }

    public override void Detruit()
    {
		c_Listetemps_perso_generaux.Clear();
		this.c_t_rubriques_temps = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_rubriques_temps(clg_Modele pModele, Int64 prbr_cn, bool pAMAJ = false) : base(pModele, prbr_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_rubriques_temps(clg_Modele pModele, Int64 prbr_cn, string prbr_a_libel, clg_t_rubriques_temps pt_rubriques_temps, Int64 prbr_n_archive, Int64 prbr_n_ordre, bool pAMAJ = true) : base(pModele, prbr_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(prbr_cn != Int64.MinValue)
            c_rbr_cn = prbr_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(prbr_a_libel != null)
            c_rbr_a_libel = prbr_a_libel;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pt_rubriques_temps != null)
            c_t_rubriques_temps = pt_rubriques_temps;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(prbr_n_archive != Int64.MinValue)
            c_rbr_n_archive = prbr_n_archive;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(prbr_n_ordre != Int64.MinValue)
            c_rbr_n_ordre = prbr_n_ordre;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 prbr_cn, string prbr_a_libel, Int64 pt_rubriques_temps, Int64 prbr_n_archive, Int64 prbr_n_ordre)
    {   
		        c_rbr_cn = prbr_cn;
		        c_rbr_a_libel = prbr_a_libel;
		c_dicReferences.Add("t_rubriques_temps", pt_rubriques_temps);
		        c_rbr_n_archive = prbr_n_archive;
		        c_rbr_n_ordre = prbr_n_ordre;

        base.Initialise(prbr_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLrubriques_temps; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_t_rubriques_temps = (clg_t_rubriques_temps) c_ModeleBase.RenvoieObjet(c_dicReferences["t_rubriques_temps"], "clg_t_rubriques_temps");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listerubriques_temps.Dictionnaire.Add(rbr_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listerubriques_temps.Dictionnaire.Remove(rbr_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_t_rubriques_temps != null)if(!c_t_rubriques_temps.Listerubriques_temps.Contains(this)) c_t_rubriques_temps.Listerubriques_temps.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_rubriques_temps l_Clone = (clg_rubriques_temps) Clone;
		if(l_Clone.t_rubriques_temps != null)if(l_Clone.t_rubriques_temps.Listerubriques_temps.Contains(this)) l_Clone.t_rubriques_temps.Listerubriques_temps.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listetemps_perso_generaux.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_rubriques_temps(null, rbr_cn, rbr_a_libel, t_rubriques_temps, rbr_n_archive, rbr_n_ordre,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_rubriques_temps l_clone = (clg_rubriques_temps) this.Clone;
		c_rbr_cn = l_clone.rbr_cn;
		c_rbr_a_libel = l_clone.rbr_a_libel;
		c_t_rubriques_temps = l_clone.t_rubriques_temps;
		c_rbr_n_archive = l_clone.rbr_n_archive;
		c_rbr_n_ordre = l_clone.rbr_n_ordre;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLrubriques_temps.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLrubriques_temps.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLrubriques_temps.Delete(this);
    }

    /* Accesseur de la propriete rbr_cn (rbr_cn)
    * @return c_rbr_cn */
    public Int64 rbr_cn
    {
        get{return c_rbr_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_rbr_cn != value)
                {
                    CreerClone();
                    c_rbr_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete rbr_a_libel (rbr_a_libel)
    * @return c_rbr_a_libel */
    public string rbr_a_libel
    {
        get{return c_rbr_a_libel;}
        set
        {
            if(value != null)
            {
                if(c_rbr_a_libel != value)
                {
                    CreerClone();
                    c_rbr_a_libel = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete t_rubriques_temps (rbr_t_rbr_cn)
    * @return c_t_rubriques_temps */
    public clg_t_rubriques_temps t_rubriques_temps
    {
        get{return c_t_rubriques_temps;}
        set
        {
            if(value != null)
            {
                if(c_t_rubriques_temps != value)
                {
                    CreerClone();
                    c_t_rubriques_temps = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete rbr_n_archive (rbr_n_archive)
    * @return c_rbr_n_archive */
    public Int64 rbr_n_archive
    {
        get{return c_rbr_n_archive;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_rbr_n_archive != value)
                {
                    CreerClone();
                    c_rbr_n_archive = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete rbr_n_ordre (rbr_n_ordre)
    * @return c_rbr_n_ordre */
    public Int64 rbr_n_ordre
    {
        get{return c_rbr_n_ordre;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_rbr_n_ordre != value)
                {
                    CreerClone();
                    c_rbr_n_ordre = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la liste des objets de type temps_perso_generaux */
    public List<clg_temps_perso_generaux> Listetemps_perso_generaux
    {
        get { return c_Listetemps_perso_generaux; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_rbr_cn.ToString());
		l_Valeurs.Add(c_rbr_a_libel.ToString());
		l_Valeurs.Add(c_t_rubriques_temps==null ? "-1" : c_t_rubriques_temps.ID.ToString());
		l_Valeurs.Add(c_rbr_n_archive.ToString());
		l_Valeurs.Add(c_rbr_n_ordre.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("rbr_cn");
		l_Noms.Add("rbr_a_libel");
		l_Noms.Add("t_rubriques_temps");
		l_Noms.Add("rbr_n_archive");
		l_Noms.Add("rbr_n_ordre");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
