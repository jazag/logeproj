namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_compte </summary>
public partial class clg_t_compte : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_compte;
    protected clg_Modele c_Modele;

	private Int64 c_t_cpt_cn;
	private string c_t_cpt_a_num;
	private Int64 c_t_cpt_jou_cn;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_compte(clg_Modele pModele, Int64 pt_cpt_cn, bool pAMAJ = false) : base(pModele, pt_cpt_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_compte(clg_Modele pModele, Int64 pt_cpt_cn, string pt_cpt_a_num, Int64 pt_cpt_jou_cn, bool pAMAJ = true) : base(pModele, pt_cpt_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_cpt_cn != Int64.MinValue)
            c_t_cpt_cn = pt_cpt_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_t_cpt_a_num = pt_cpt_a_num;
		        c_t_cpt_jou_cn = pt_cpt_jou_cn;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_cpt_cn, string pt_cpt_a_num, Int64 pt_cpt_jou_cn)
    {   
		        c_t_cpt_cn = pt_cpt_cn;
		        c_t_cpt_a_num = pt_cpt_a_num;
		        c_t_cpt_jou_cn = pt_cpt_jou_cn;

        base.Initialise(pt_cpt_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_compte; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_compte.Dictionnaire.Add(t_cpt_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_compte.Dictionnaire.Remove(t_cpt_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_compte l_Clone = (clg_t_compte) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_compte(null, t_cpt_cn, t_cpt_a_num, t_cpt_jou_cn,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_compte l_clone = (clg_t_compte) this.Clone;
		c_t_cpt_cn = l_clone.t_cpt_cn;
		c_t_cpt_a_num = l_clone.t_cpt_a_num;
		c_t_cpt_jou_cn = l_clone.t_cpt_jou_cn;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_compte.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_compte.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_compte.Delete(this);
    }

    /* Accesseur de la propriete t_cpt_cn (t_cpt_cn)
    * @return c_t_cpt_cn */
    public Int64 t_cpt_cn
    {
        get{return c_t_cpt_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_cpt_cn != value)
                {
                    CreerClone();
                    c_t_cpt_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_cpt_a_num (t_cpt_a_num)
    * @return c_t_cpt_a_num */
    public string t_cpt_a_num
    {
        get{return c_t_cpt_a_num;}
        set
        {
            if(c_t_cpt_a_num != value)
            {
                CreerClone();
                c_t_cpt_a_num = value;
            }
        }
    }
    /* Accesseur de la propriete t_cpt_jou_cn (t_cpt_jou_cn)
    * @return c_t_cpt_jou_cn */
    public Int64 t_cpt_jou_cn
    {
        get{return c_t_cpt_jou_cn;}
        set
        {
            if(c_t_cpt_jou_cn != value)
            {
                CreerClone();
                c_t_cpt_jou_cn = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_cpt_cn.ToString());
		l_Valeurs.Add(c_t_cpt_a_num.ToString());
		l_Valeurs.Add(c_t_cpt_jou_cn.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_cpt_cn");
		l_Noms.Add("t_cpt_a_num");
		l_Noms.Add("t_cpt_jou_cn");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
