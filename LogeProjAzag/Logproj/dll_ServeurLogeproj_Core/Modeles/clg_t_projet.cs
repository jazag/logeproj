namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : t_projet </summary>
public partial class clg_t_projet : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLt_projet;
    protected clg_Modele c_Modele;

	private Int64 c_t_prj_cn;
	private string c_t_prj_a_lib;
	private List<clg_projets> c_Listeprojets;


    private void Init()
    {
		c_Listeprojets = new List<clg_projets>();

    }

    public override void Detruit()
    {
		c_Listeprojets.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_t_projet(clg_Modele pModele, Int64 pt_prj_cn, bool pAMAJ = false) : base(pModele, pt_prj_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_t_projet(clg_Modele pModele, Int64 pt_prj_cn, string pt_prj_a_lib, bool pAMAJ = true) : base(pModele, pt_prj_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pt_prj_cn != Int64.MinValue)
            c_t_prj_cn = pt_prj_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_t_prj_a_lib = pt_prj_a_lib;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pt_prj_cn, string pt_prj_a_lib)
    {   
		        c_t_prj_cn = pt_prj_cn;
		        c_t_prj_a_lib = pt_prj_a_lib;

        base.Initialise(pt_prj_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLt_projet; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listet_projet.Dictionnaire.Add(t_prj_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listet_projet.Dictionnaire.Remove(t_prj_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_t_projet l_Clone = (clg_t_projet) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeprojets.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_t_projet(null, t_prj_cn, t_prj_a_lib,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_t_projet l_clone = (clg_t_projet) this.Clone;
		c_t_prj_cn = l_clone.t_prj_cn;
		c_t_prj_a_lib = l_clone.t_prj_a_lib;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLt_projet.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLt_projet.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLt_projet.Delete(this);
    }

    /* Accesseur de la propriete t_prj_cn (t_prj_cn)
    * @return c_t_prj_cn */
    public Int64 t_prj_cn
    {
        get{return c_t_prj_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_t_prj_cn != value)
                {
                    CreerClone();
                    c_t_prj_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete t_prj_a_lib (t_prj_a_lib)
    * @return c_t_prj_a_lib */
    public string t_prj_a_lib
    {
        get{return c_t_prj_a_lib;}
        set
        {
            if(c_t_prj_a_lib != value)
            {
                CreerClone();
                c_t_prj_a_lib = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type projets */
    public List<clg_projets> Listeprojets
    {
        get { return c_Listeprojets; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_t_prj_cn.ToString());
		l_Valeurs.Add(c_t_prj_a_lib.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("t_prj_cn");
		l_Noms.Add("t_prj_a_lib");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
