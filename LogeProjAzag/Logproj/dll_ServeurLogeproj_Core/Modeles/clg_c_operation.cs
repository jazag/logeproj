namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : c_operation </summary>
public partial class clg_c_operation : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLc_operation;
    protected clg_Modele c_Modele;

	private Int64 c_c_ope_cn;
	private string c_c_ope_a_lib;
	private string c_c_ope_m_desc;
	private Int64 c_c_ope_cn_pere;
	private Int64 c_c_ope_cn_racine;
	private string c_c_ope_a_code;
	private Int64 c_c_ope_n_niveau;
	private string c_c_ope_a_cheminicone;
	private string c_c_ope_a_cheminicogrise;
	private List<clg_operations> c_Listeoperations;


    private void Init()
    {
		c_Listeoperations = new List<clg_operations>();

    }

    public override void Detruit()
    {
		c_Listeoperations.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_c_operation(clg_Modele pModele, Int64 pc_ope_cn, bool pAMAJ = false) : base(pModele, pc_ope_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_c_operation(clg_Modele pModele, Int64 pc_ope_cn, string pc_ope_a_lib, string pc_ope_m_desc, Int64 pc_ope_cn_pere, Int64 pc_ope_cn_racine, string pc_ope_a_code, Int64 pc_ope_n_niveau, string pc_ope_a_cheminicone, string pc_ope_a_cheminicogrise, bool pAMAJ = true) : base(pModele, pc_ope_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pc_ope_cn != Int64.MinValue)
            c_c_ope_cn = pc_ope_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_c_ope_a_lib = pc_ope_a_lib;
		        c_c_ope_m_desc = pc_ope_m_desc;
		        c_c_ope_cn_pere = pc_ope_cn_pere;
		        if(pc_ope_cn_racine != Int64.MinValue)
            c_c_ope_cn_racine = pc_ope_cn_racine;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pc_ope_a_code != null)
            c_c_ope_a_code = pc_ope_a_code;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        if(pc_ope_n_niveau != Int64.MinValue)
            c_c_ope_n_niveau = pc_ope_n_niveau;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_c_ope_a_cheminicone = pc_ope_a_cheminicone;
		        c_c_ope_a_cheminicogrise = pc_ope_a_cheminicogrise;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pc_ope_cn, string pc_ope_a_lib, string pc_ope_m_desc, Int64 pc_ope_cn_pere, Int64 pc_ope_cn_racine, string pc_ope_a_code, Int64 pc_ope_n_niveau, string pc_ope_a_cheminicone, string pc_ope_a_cheminicogrise)
    {   
		        c_c_ope_cn = pc_ope_cn;
		        c_c_ope_a_lib = pc_ope_a_lib;
		        c_c_ope_m_desc = pc_ope_m_desc;
		        c_c_ope_cn_pere = pc_ope_cn_pere;
		        c_c_ope_cn_racine = pc_ope_cn_racine;
		        c_c_ope_a_code = pc_ope_a_code;
		        c_c_ope_n_niveau = pc_ope_n_niveau;
		        c_c_ope_a_cheminicone = pc_ope_a_cheminicone;
		        c_c_ope_a_cheminicogrise = pc_ope_a_cheminicogrise;

        base.Initialise(pc_ope_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLc_operation; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listec_operation.Dictionnaire.Add(c_ope_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listec_operation.Dictionnaire.Remove(c_ope_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_c_operation l_Clone = (clg_c_operation) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeoperations.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_c_operation(null, c_ope_cn, c_ope_a_lib, c_ope_m_desc, c_ope_cn_pere, c_ope_cn_racine, c_ope_a_code, c_ope_n_niveau, c_ope_a_cheminicone, c_ope_a_cheminicogrise,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_c_operation l_clone = (clg_c_operation) this.Clone;
		c_c_ope_cn = l_clone.c_ope_cn;
		c_c_ope_a_lib = l_clone.c_ope_a_lib;
		c_c_ope_m_desc = l_clone.c_ope_m_desc;
		c_c_ope_cn_pere = l_clone.c_ope_cn_pere;
		c_c_ope_cn_racine = l_clone.c_ope_cn_racine;
		c_c_ope_a_code = l_clone.c_ope_a_code;
		c_c_ope_n_niveau = l_clone.c_ope_n_niveau;
		c_c_ope_a_cheminicone = l_clone.c_ope_a_cheminicone;
		c_c_ope_a_cheminicogrise = l_clone.c_ope_a_cheminicogrise;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLc_operation.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLc_operation.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLc_operation.Delete(this);
    }

    /* Accesseur de la propriete c_ope_cn (c_ope_cn)
    * @return c_c_ope_cn */
    public Int64 c_ope_cn
    {
        get{return c_c_ope_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_c_ope_cn != value)
                {
                    CreerClone();
                    c_c_ope_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete c_ope_a_lib (c_ope_a_lib)
    * @return c_c_ope_a_lib */
    public string c_ope_a_lib
    {
        get{return c_c_ope_a_lib;}
        set
        {
            if(c_c_ope_a_lib != value)
            {
                CreerClone();
                c_c_ope_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete c_ope_m_desc (c_ope_m_desc)
    * @return c_c_ope_m_desc */
    public string c_ope_m_desc
    {
        get{return c_c_ope_m_desc;}
        set
        {
            if(c_c_ope_m_desc != value)
            {
                CreerClone();
                c_c_ope_m_desc = value;
            }
        }
    }
    /* Accesseur de la propriete c_ope_cn_pere (c_ope_cn_pere)
    * @return c_c_ope_cn_pere */
    public Int64 c_ope_cn_pere
    {
        get{return c_c_ope_cn_pere;}
        set
        {
            if(c_c_ope_cn_pere != value)
            {
                CreerClone();
                c_c_ope_cn_pere = value;
            }
        }
    }
    /* Accesseur de la propriete c_ope_cn_racine (c_ope_cn_racine)
    * @return c_c_ope_cn_racine */
    public Int64 c_ope_cn_racine
    {
        get{return c_c_ope_cn_racine;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_c_ope_cn_racine != value)
                {
                    CreerClone();
                    c_c_ope_cn_racine = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete c_ope_a_code (c_ope_a_code)
    * @return c_c_ope_a_code */
    public string c_ope_a_code
    {
        get{return c_c_ope_a_code;}
        set
        {
            if(value != null)
            {
                if(c_c_ope_a_code != value)
                {
                    CreerClone();
                    c_c_ope_a_code = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete c_ope_n_niveau (c_ope_n_niveau)
    * @return c_c_ope_n_niveau */
    public Int64 c_ope_n_niveau
    {
        get{return c_c_ope_n_niveau;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_c_ope_n_niveau != value)
                {
                    CreerClone();
                    c_c_ope_n_niveau = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete c_ope_a_cheminicone (c_ope_a_cheminicone)
    * @return c_c_ope_a_cheminicone */
    public string c_ope_a_cheminicone
    {
        get{return c_c_ope_a_cheminicone;}
        set
        {
            if(c_c_ope_a_cheminicone != value)
            {
                CreerClone();
                c_c_ope_a_cheminicone = value;
            }
        }
    }
    /* Accesseur de la propriete c_ope_a_cheminicogrise (c_ope_a_cheminicogrise)
    * @return c_c_ope_a_cheminicogrise */
    public string c_ope_a_cheminicogrise
    {
        get{return c_c_ope_a_cheminicogrise;}
        set
        {
            if(c_c_ope_a_cheminicogrise != value)
            {
                CreerClone();
                c_c_ope_a_cheminicogrise = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type operations */
    public List<clg_operations> Listeoperations
    {
        get { return c_Listeoperations; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_c_ope_cn.ToString());
		l_Valeurs.Add(c_c_ope_a_lib.ToString());
		l_Valeurs.Add(c_c_ope_m_desc.ToString());
		l_Valeurs.Add(c_c_ope_cn_pere.ToString());
		l_Valeurs.Add(c_c_ope_cn_racine.ToString());
		l_Valeurs.Add(c_c_ope_a_code.ToString());
		l_Valeurs.Add(c_c_ope_n_niveau.ToString());
		l_Valeurs.Add(c_c_ope_a_cheminicone.ToString());
		l_Valeurs.Add(c_c_ope_a_cheminicogrise.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("c_ope_cn");
		l_Noms.Add("c_ope_a_lib");
		l_Noms.Add("c_ope_m_desc");
		l_Noms.Add("c_ope_cn_pere");
		l_Noms.Add("c_ope_cn_racine");
		l_Noms.Add("c_ope_a_code");
		l_Noms.Add("c_ope_n_niveau");
		l_Noms.Add("c_ope_a_cheminicone");
		l_Noms.Add("c_ope_a_cheminicogrise");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
