namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : regle </summary>
public partial class clg_regle : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLregle;
    protected clg_Modele c_Modele;

	private Int64 c_reg_cn;
	private string c_reg_a_lib;
	private Int64 c_reg_n_t_forfait;
	private bool c_reg_n_bloquant_possible;
	private List<clg_convention_regle> c_Listeconvention_regle;


    private void Init()
    {
		c_Listeconvention_regle = new List<clg_convention_regle>();

    }

    public override void Detruit()
    {
		c_Listeconvention_regle.Clear();

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_regle(clg_Modele pModele, Int64 preg_cn, bool pAMAJ = false) : base(pModele, preg_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_regle(clg_Modele pModele, Int64 preg_cn, string preg_a_lib, Int64 preg_n_t_forfait, bool preg_n_bloquant_possible, bool pAMAJ = true) : base(pModele, preg_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(preg_cn != Int64.MinValue)
            c_reg_cn = preg_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_reg_a_lib = preg_a_lib;
		        c_reg_n_t_forfait = preg_n_t_forfait;
		        c_reg_n_bloquant_possible = preg_n_bloquant_possible;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 preg_cn, string preg_a_lib, Int64 preg_n_t_forfait, bool preg_n_bloquant_possible)
    {   
		        c_reg_cn = preg_cn;
		        c_reg_a_lib = preg_a_lib;
		        c_reg_n_t_forfait = preg_n_t_forfait;
		        c_reg_n_bloquant_possible = preg_n_bloquant_possible;

        base.Initialise(preg_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLregle; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeregle.Dictionnaire.Add(reg_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeregle.Dictionnaire.Remove(reg_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_regle l_Clone = (clg_regle) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();
		if(c_Listeconvention_regle.Count > 0) c_EstReference = true;

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_regle(null, reg_cn, reg_a_lib, reg_n_t_forfait, reg_n_bloquant_possible,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_regle l_clone = (clg_regle) this.Clone;
		c_reg_cn = l_clone.reg_cn;
		c_reg_a_lib = l_clone.reg_a_lib;
		c_reg_n_t_forfait = l_clone.reg_n_t_forfait;
		c_reg_n_bloquant_possible = l_clone.reg_n_bloquant_possible;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLregle.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLregle.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLregle.Delete(this);
    }

    /* Accesseur de la propriete reg_cn (reg_cn)
    * @return c_reg_cn */
    public Int64 reg_cn
    {
        get{return c_reg_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_reg_cn != value)
                {
                    CreerClone();
                    c_reg_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete reg_a_lib (reg_a_lib)
    * @return c_reg_a_lib */
    public string reg_a_lib
    {
        get{return c_reg_a_lib;}
        set
        {
            if(c_reg_a_lib != value)
            {
                CreerClone();
                c_reg_a_lib = value;
            }
        }
    }
    /* Accesseur de la propriete reg_n_t_forfait (reg_n_t_forfait)
    * @return c_reg_n_t_forfait */
    public Int64 reg_n_t_forfait
    {
        get{return c_reg_n_t_forfait;}
        set
        {
            if(c_reg_n_t_forfait != value)
            {
                CreerClone();
                c_reg_n_t_forfait = value;
            }
        }
    }
    /* Accesseur de la propriete reg_n_bloquant_possible (reg_n_bloquant_possible)
    * @return c_reg_n_bloquant_possible */
    public bool reg_n_bloquant_possible
    {
        get{return c_reg_n_bloquant_possible;}
        set
        {
            if(c_reg_n_bloquant_possible != value)
            {
                CreerClone();
                c_reg_n_bloquant_possible = value;
            }
        }
    }
    /* Accesseur de la liste des objets de type convention_regle */
    public List<clg_convention_regle> Listeconvention_regle
    {
        get { return c_Listeconvention_regle; }
    }

	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_reg_cn.ToString());
		l_Valeurs.Add(c_reg_a_lib.ToString());
		l_Valeurs.Add(c_reg_n_t_forfait.ToString());
		l_Valeurs.Add(c_reg_n_bloquant_possible.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("reg_cn");
		l_Noms.Add("reg_a_lib");
		l_Noms.Add("reg_n_t_forfait");
		l_Noms.Add("reg_n_bloquant_possible");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
