namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : param_temps_travail </summary>
public partial class clg_param_temps_travail : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLparam_temps_travail;
    protected clg_Modele c_Modele;

	private Int64 c_ptt_cn;
	private Int64 c_ptt_n_nb_h_semaine;
	private double c_ptt_n_nb_h_semaine_max;
	private Int64 c_ptt_n_recup_max;
	private double c_ptt_n_nb_h_jour_normal;
	private double c_ptt_n_nb_h_jour_max;
	private double c_ptt_n_nb_h_jour_conges;
	private Int64 c_ptt_n_delai_recup;
	private DateTime c_ptt_d_validite;
	private DateTime c_ptt_d_fin_validite;
	private Int64 c_ptt_n_mod_debloquer;


    private void Init()
    {

    }

    public override void Detruit()
    {

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_param_temps_travail(clg_Modele pModele, Int64 pptt_cn, bool pAMAJ = false) : base(pModele, pptt_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_param_temps_travail(clg_Modele pModele, Int64 pptt_cn, Int64 pptt_n_nb_h_semaine, double pptt_n_nb_h_semaine_max, Int64 pptt_n_recup_max, double pptt_n_nb_h_jour_normal, double pptt_n_nb_h_jour_max, double pptt_n_nb_h_jour_conges, Int64 pptt_n_delai_recup, DateTime pptt_d_validite, DateTime pptt_d_fin_validite, Int64 pptt_n_mod_debloquer, bool pAMAJ = true) : base(pModele, pptt_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pptt_cn != Int64.MinValue)
            c_ptt_cn = pptt_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pptt_n_nb_h_semaine != Int64.MinValue)
            c_ptt_n_nb_h_semaine = pptt_n_nb_h_semaine;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pptt_n_nb_h_semaine_max != double.MinValue)
            c_ptt_n_nb_h_semaine_max = pptt_n_nb_h_semaine_max;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pptt_n_recup_max != Int64.MinValue)
            c_ptt_n_recup_max = pptt_n_recup_max;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pptt_n_nb_h_jour_normal != double.MinValue)
            c_ptt_n_nb_h_jour_normal = pptt_n_nb_h_jour_normal;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pptt_n_nb_h_jour_max != double.MinValue)
            c_ptt_n_nb_h_jour_max = pptt_n_nb_h_jour_max;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pptt_n_nb_h_jour_conges != double.MinValue)
            c_ptt_n_nb_h_jour_conges = pptt_n_nb_h_jour_conges;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pptt_n_delai_recup != Int64.MinValue)
            c_ptt_n_delai_recup = pptt_n_delai_recup;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pptt_d_validite != DateTime.MinValue)
            c_ptt_d_validite = pptt_d_validite;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_ptt_d_fin_validite = pptt_d_fin_validite;
		        if(pptt_n_mod_debloquer != Int64.MinValue)
            c_ptt_n_mod_debloquer = pptt_n_mod_debloquer;
        else
            throw new System.InvalidOperationException("MinValue interdite");

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pptt_cn, Int64 pptt_n_nb_h_semaine, double pptt_n_nb_h_semaine_max, Int64 pptt_n_recup_max, double pptt_n_nb_h_jour_normal, double pptt_n_nb_h_jour_max, double pptt_n_nb_h_jour_conges, Int64 pptt_n_delai_recup, DateTime pptt_d_validite, DateTime pptt_d_fin_validite, Int64 pptt_n_mod_debloquer)
    {   
		        c_ptt_cn = pptt_cn;
		        c_ptt_n_nb_h_semaine = pptt_n_nb_h_semaine;
		        c_ptt_n_nb_h_semaine_max = pptt_n_nb_h_semaine_max;
		        c_ptt_n_recup_max = pptt_n_recup_max;
		        c_ptt_n_nb_h_jour_normal = pptt_n_nb_h_jour_normal;
		        c_ptt_n_nb_h_jour_max = pptt_n_nb_h_jour_max;
		        c_ptt_n_nb_h_jour_conges = pptt_n_nb_h_jour_conges;
		        c_ptt_n_delai_recup = pptt_n_delai_recup;
		        c_ptt_d_validite = pptt_d_validite;
		        c_ptt_d_fin_validite = pptt_d_fin_validite;
		        c_ptt_n_mod_debloquer = pptt_n_mod_debloquer;

        base.Initialise(pptt_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLparam_temps_travail; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeparam_temps_travail.Dictionnaire.Add(ptt_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeparam_temps_travail.Dictionnaire.Remove(ptt_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_param_temps_travail l_Clone = (clg_param_temps_travail) Clone;

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_param_temps_travail(null, ptt_cn, ptt_n_nb_h_semaine, ptt_n_nb_h_semaine_max, ptt_n_recup_max, ptt_n_nb_h_jour_normal, ptt_n_nb_h_jour_max, ptt_n_nb_h_jour_conges, ptt_n_delai_recup, ptt_d_validite, ptt_d_fin_validite, ptt_n_mod_debloquer,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_param_temps_travail l_clone = (clg_param_temps_travail) this.Clone;
		c_ptt_cn = l_clone.ptt_cn;
		c_ptt_n_nb_h_semaine = l_clone.ptt_n_nb_h_semaine;
		c_ptt_n_nb_h_semaine_max = l_clone.ptt_n_nb_h_semaine_max;
		c_ptt_n_recup_max = l_clone.ptt_n_recup_max;
		c_ptt_n_nb_h_jour_normal = l_clone.ptt_n_nb_h_jour_normal;
		c_ptt_n_nb_h_jour_max = l_clone.ptt_n_nb_h_jour_max;
		c_ptt_n_nb_h_jour_conges = l_clone.ptt_n_nb_h_jour_conges;
		c_ptt_n_delai_recup = l_clone.ptt_n_delai_recup;
		c_ptt_d_validite = l_clone.ptt_d_validite;
		c_ptt_d_fin_validite = l_clone.ptt_d_fin_validite;
		c_ptt_n_mod_debloquer = l_clone.ptt_n_mod_debloquer;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLparam_temps_travail.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLparam_temps_travail.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLparam_temps_travail.Delete(this);
    }

    /* Accesseur de la propriete ptt_cn (ptt_cn)
    * @return c_ptt_cn */
    public Int64 ptt_cn
    {
        get{return c_ptt_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ptt_cn != value)
                {
                    CreerClone();
                    c_ptt_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_n_nb_h_semaine (ptt_n_nb_h_semaine)
    * @return c_ptt_n_nb_h_semaine */
    public Int64 ptt_n_nb_h_semaine
    {
        get{return c_ptt_n_nb_h_semaine;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ptt_n_nb_h_semaine != value)
                {
                    CreerClone();
                    c_ptt_n_nb_h_semaine = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_n_nb_h_semaine_max (ptt_n_nb_h_semaine_max)
    * @return c_ptt_n_nb_h_semaine_max */
    public double ptt_n_nb_h_semaine_max
    {
        get{return c_ptt_n_nb_h_semaine_max;}
        set
        {
            if(value != double.MinValue)
            {
                if(c_ptt_n_nb_h_semaine_max != value)
                {
                    CreerClone();
                    c_ptt_n_nb_h_semaine_max = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_n_recup_max (ptt_n_recup_max)
    * @return c_ptt_n_recup_max */
    public Int64 ptt_n_recup_max
    {
        get{return c_ptt_n_recup_max;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ptt_n_recup_max != value)
                {
                    CreerClone();
                    c_ptt_n_recup_max = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_n_nb_h_jour_normal (ptt_n_nb_h_jour_normal)
    * @return c_ptt_n_nb_h_jour_normal */
    public double ptt_n_nb_h_jour_normal
    {
        get{return c_ptt_n_nb_h_jour_normal;}
        set
        {
            if(value != double.MinValue)
            {
                if(c_ptt_n_nb_h_jour_normal != value)
                {
                    CreerClone();
                    c_ptt_n_nb_h_jour_normal = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_n_nb_h_jour_max (ptt_n_nb_h_jour_max)
    * @return c_ptt_n_nb_h_jour_max */
    public double ptt_n_nb_h_jour_max
    {
        get{return c_ptt_n_nb_h_jour_max;}
        set
        {
            if(value != double.MinValue)
            {
                if(c_ptt_n_nb_h_jour_max != value)
                {
                    CreerClone();
                    c_ptt_n_nb_h_jour_max = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_n_nb_h_jour_conges (ptt_n_nb_h_jour_conges)
    * @return c_ptt_n_nb_h_jour_conges */
    public double ptt_n_nb_h_jour_conges
    {
        get{return c_ptt_n_nb_h_jour_conges;}
        set
        {
            if(value != double.MinValue)
            {
                if(c_ptt_n_nb_h_jour_conges != value)
                {
                    CreerClone();
                    c_ptt_n_nb_h_jour_conges = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_n_delai_recup (ptt_n_delai_recup)
    * @return c_ptt_n_delai_recup */
    public Int64 ptt_n_delai_recup
    {
        get{return c_ptt_n_delai_recup;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ptt_n_delai_recup != value)
                {
                    CreerClone();
                    c_ptt_n_delai_recup = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_d_validite (ptt_d_validite)
    * @return c_ptt_d_validite */
    public DateTime ptt_d_validite
    {
        get{return c_ptt_d_validite;}
        set
        {
            if(value != null)
            {
                if(c_ptt_d_validite != value)
                {
                    CreerClone();
                    c_ptt_d_validite = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete ptt_d_fin_validite (ptt_d_fin_validite)
    * @return c_ptt_d_fin_validite */
    public DateTime ptt_d_fin_validite
    {
        get{return c_ptt_d_fin_validite;}
        set
        {
            if(c_ptt_d_fin_validite != value)
            {
                CreerClone();
                c_ptt_d_fin_validite = value;
            }
        }
    }
    /* Accesseur de la propriete ptt_n_mod_debloquer (ptt_n_mod_debloquer)
    * @return c_ptt_n_mod_debloquer */
    public Int64 ptt_n_mod_debloquer
    {
        get{return c_ptt_n_mod_debloquer;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_ptt_n_mod_debloquer != value)
                {
                    CreerClone();
                    c_ptt_n_mod_debloquer = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_ptt_cn.ToString());
		l_Valeurs.Add(c_ptt_n_nb_h_semaine.ToString());
		l_Valeurs.Add(c_ptt_n_nb_h_semaine_max.ToString());
		l_Valeurs.Add(c_ptt_n_recup_max.ToString());
		l_Valeurs.Add(c_ptt_n_nb_h_jour_normal.ToString());
		l_Valeurs.Add(c_ptt_n_nb_h_jour_max.ToString());
		l_Valeurs.Add(c_ptt_n_nb_h_jour_conges.ToString());
		l_Valeurs.Add(c_ptt_n_delai_recup.ToString());
		l_Valeurs.Add(c_ptt_d_validite.ToString());
		l_Valeurs.Add(c_ptt_d_fin_validite.ToString());
		l_Valeurs.Add(c_ptt_n_mod_debloquer.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("ptt_cn");
		l_Noms.Add("ptt_n_nb_h_semaine");
		l_Noms.Add("ptt_n_nb_h_semaine_max");
		l_Noms.Add("ptt_n_recup_max");
		l_Noms.Add("ptt_n_nb_h_jour_normal");
		l_Noms.Add("ptt_n_nb_h_jour_max");
		l_Noms.Add("ptt_n_nb_h_jour_conges");
		l_Noms.Add("ptt_n_delai_recup");
		l_Noms.Add("ptt_d_validite");
		l_Noms.Add("ptt_d_fin_validite");
		l_Noms.Add("ptt_n_mod_debloquer");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
