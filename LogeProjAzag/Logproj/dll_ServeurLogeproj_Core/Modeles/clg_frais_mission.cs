namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : frais_mission </summary>
public partial class clg_frais_mission : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLfrais_mission;
    protected clg_Modele c_Modele;

	private Int64 c_frm_cn;
	private clg_categorie_frais c_categorie_frais;
	private string c_frm_a_objet;
	private double c_frm_n_montant;
	private clg_deplacements_realises c_deplacements_realises;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_categorie_frais = null;
		this.c_deplacements_realises = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_frais_mission(clg_Modele pModele, Int64 pfrm_cn, bool pAMAJ = false) : base(pModele, pfrm_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_frais_mission(clg_Modele pModele, Int64 pfrm_cn, clg_categorie_frais pcategorie_frais, string pfrm_a_objet, double pfrm_n_montant, clg_deplacements_realises pdeplacements_realises, bool pAMAJ = true) : base(pModele, pfrm_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pfrm_cn != Int64.MinValue)
            c_frm_cn = pfrm_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_categorie_frais = pcategorie_frais;
		        c_frm_a_objet = pfrm_a_objet;
		        c_frm_n_montant = pfrm_n_montant;
		        c_deplacements_realises = pdeplacements_realises;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pfrm_cn, Int64 pcategorie_frais, string pfrm_a_objet, double pfrm_n_montant, Int64 pdeplacements_realises)
    {   
		        c_frm_cn = pfrm_cn;
		c_dicReferences.Add("categorie_frais", pcategorie_frais);
		        c_frm_a_objet = pfrm_a_objet;
		        c_frm_n_montant = pfrm_n_montant;
		c_dicReferences.Add("deplacements_realises", pdeplacements_realises);

        base.Initialise(pfrm_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLfrais_mission; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_categorie_frais = (clg_categorie_frais) c_ModeleBase.RenvoieObjet(c_dicReferences["categorie_frais"], "clg_categorie_frais");
		c_deplacements_realises = (clg_deplacements_realises) c_ModeleBase.RenvoieObjet(c_dicReferences["deplacements_realises"], "clg_deplacements_realises");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listefrais_mission.Dictionnaire.Add(frm_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listefrais_mission.Dictionnaire.Remove(frm_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_categorie_frais != null)if(!c_categorie_frais.Listefrais_mission.Contains(this)) c_categorie_frais.Listefrais_mission.Add(this);
		if(c_deplacements_realises != null)if(!c_deplacements_realises.Listefrais_mission.Contains(this)) c_deplacements_realises.Listefrais_mission.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_frais_mission l_Clone = (clg_frais_mission) Clone;
		if(l_Clone.categorie_frais != null)if(l_Clone.categorie_frais.Listefrais_mission.Contains(this)) l_Clone.categorie_frais.Listefrais_mission.Remove(this);
		if(l_Clone.deplacements_realises != null)if(l_Clone.deplacements_realises.Listefrais_mission.Contains(this)) l_Clone.deplacements_realises.Listefrais_mission.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_frais_mission(null, frm_cn, categorie_frais, frm_a_objet, frm_n_montant, deplacements_realises,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_frais_mission l_clone = (clg_frais_mission) this.Clone;
		c_frm_cn = l_clone.frm_cn;
		c_categorie_frais = l_clone.categorie_frais;
		c_frm_a_objet = l_clone.frm_a_objet;
		c_frm_n_montant = l_clone.frm_n_montant;
		c_deplacements_realises = l_clone.deplacements_realises;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLfrais_mission.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLfrais_mission.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLfrais_mission.Delete(this);
    }

    /* Accesseur de la propriete frm_cn (frm_cn)
    * @return c_frm_cn */
    public Int64 frm_cn
    {
        get{return c_frm_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_frm_cn != value)
                {
                    CreerClone();
                    c_frm_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete categorie_frais (frm_cfr_cn)
    * @return c_categorie_frais */
    public clg_categorie_frais categorie_frais
    {
        get{return c_categorie_frais;}
        set
        {
            if(c_categorie_frais != value)
            {
                CreerClone();
                c_categorie_frais = value;
            }
        }
    }
    /* Accesseur de la propriete frm_a_objet (frm_a_objet)
    * @return c_frm_a_objet */
    public string frm_a_objet
    {
        get{return c_frm_a_objet;}
        set
        {
            if(c_frm_a_objet != value)
            {
                CreerClone();
                c_frm_a_objet = value;
            }
        }
    }
    /* Accesseur de la propriete frm_n_montant (frm_n_montant)
    * @return c_frm_n_montant */
    public double frm_n_montant
    {
        get{return c_frm_n_montant;}
        set
        {
            if(c_frm_n_montant != value)
            {
                CreerClone();
                c_frm_n_montant = value;
            }
        }
    }
    /* Accesseur de la propriete deplacements_realises (frm_dpr_cn)
    * @return c_deplacements_realises */
    public clg_deplacements_realises deplacements_realises
    {
        get{return c_deplacements_realises;}
        set
        {
            if(c_deplacements_realises != value)
            {
                CreerClone();
                c_deplacements_realises = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_frm_cn.ToString());
		l_Valeurs.Add(c_categorie_frais==null ? "-1" : c_categorie_frais.ID.ToString());
		l_Valeurs.Add(c_frm_a_objet.ToString());
		l_Valeurs.Add(c_frm_n_montant.ToString());
		l_Valeurs.Add(c_deplacements_realises==null ? "-1" : c_deplacements_realises.ID.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("frm_cn");
		l_Noms.Add("categorie_frais");
		l_Noms.Add("frm_a_objet");
		l_Noms.Add("frm_n_montant");
		l_Noms.Add("deplacements_realises");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
