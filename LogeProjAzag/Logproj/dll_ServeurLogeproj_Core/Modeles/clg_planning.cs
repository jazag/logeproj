namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : planning </summary>
public partial class clg_planning : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLplanning;
    protected clg_Modele c_Modele;

	private Int64 c_pln_cn;
	private clg_calendrier c_calendrier;
	private clg_personnels c_personnels;
	private DateTime c_pln_h_deb_mat_pre;
	private DateTime c_pln_h_fin_mat_pre;
	private DateTime c_pln_h_deb_apr_pre;
	private DateTime c_pln_h_fin_apr_pre;
	private DateTime c_pln_h_deb_mat_rea;
	private DateTime c_pln_h_fin_mat_rea;
	private DateTime c_pln_h_deb_apr_rea;
	private DateTime c_pln_h_fin_apr_rea;
	private DateTime c_pln_h_trajet;
	private bool c_pln_b_bloque;
	private bool c_pln_b_bloque_real;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_calendrier = null;
		this.c_personnels = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_planning(clg_Modele pModele, Int64 ppln_cn, bool pAMAJ = false) : base(pModele, ppln_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_planning(clg_Modele pModele, Int64 ppln_cn, clg_calendrier pcalendrier, clg_personnels ppersonnels, DateTime ppln_h_deb_mat_pre, DateTime ppln_h_fin_mat_pre, DateTime ppln_h_deb_apr_pre, DateTime ppln_h_fin_apr_pre, DateTime ppln_h_deb_mat_rea, DateTime ppln_h_fin_mat_rea, DateTime ppln_h_deb_apr_rea, DateTime ppln_h_fin_apr_rea, DateTime ppln_h_trajet, bool ppln_b_bloque, bool ppln_b_bloque_real, bool pAMAJ = true) : base(pModele, ppln_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(ppln_cn != Int64.MinValue)
            c_pln_cn = ppln_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        if(pcalendrier != null)
            c_calendrier = pcalendrier;
        else
            throw new System.InvalidOperationException("Valeur nulle interdite");
		        c_personnels = ppersonnels;
		        c_pln_h_deb_mat_pre = ppln_h_deb_mat_pre;
		        c_pln_h_fin_mat_pre = ppln_h_fin_mat_pre;
		        c_pln_h_deb_apr_pre = ppln_h_deb_apr_pre;
		        c_pln_h_fin_apr_pre = ppln_h_fin_apr_pre;
		        c_pln_h_deb_mat_rea = ppln_h_deb_mat_rea;
		        c_pln_h_fin_mat_rea = ppln_h_fin_mat_rea;
		        c_pln_h_deb_apr_rea = ppln_h_deb_apr_rea;
		        c_pln_h_fin_apr_rea = ppln_h_fin_apr_rea;
		        c_pln_h_trajet = ppln_h_trajet;
		        c_pln_b_bloque = ppln_b_bloque;
		        c_pln_b_bloque_real = ppln_b_bloque_real;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 ppln_cn, DateTime pcalendrier, Int64 ppersonnels, DateTime ppln_h_deb_mat_pre, DateTime ppln_h_fin_mat_pre, DateTime ppln_h_deb_apr_pre, DateTime ppln_h_fin_apr_pre, DateTime ppln_h_deb_mat_rea, DateTime ppln_h_fin_mat_rea, DateTime ppln_h_deb_apr_rea, DateTime ppln_h_fin_apr_rea, DateTime ppln_h_trajet, bool ppln_b_bloque, bool ppln_b_bloque_real)
    {   
		        c_pln_cn = ppln_cn;
		c_dicReferences.Add("calendrier", pcalendrier);
		c_dicReferences.Add("personnels", ppersonnels);
		        c_pln_h_deb_mat_pre = ppln_h_deb_mat_pre;
		        c_pln_h_fin_mat_pre = ppln_h_fin_mat_pre;
		        c_pln_h_deb_apr_pre = ppln_h_deb_apr_pre;
		        c_pln_h_fin_apr_pre = ppln_h_fin_apr_pre;
		        c_pln_h_deb_mat_rea = ppln_h_deb_mat_rea;
		        c_pln_h_fin_mat_rea = ppln_h_fin_mat_rea;
		        c_pln_h_deb_apr_rea = ppln_h_deb_apr_rea;
		        c_pln_h_fin_apr_rea = ppln_h_fin_apr_rea;
		        c_pln_h_trajet = ppln_h_trajet;
		        c_pln_b_bloque = ppln_b_bloque;
		        c_pln_b_bloque_real = ppln_b_bloque_real;

        base.Initialise(ppln_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLplanning; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_calendrier = (clg_calendrier) c_ModeleBase.RenvoieObjet(c_dicReferences["calendrier"], "clg_calendrier");
		c_personnels = (clg_personnels) c_ModeleBase.RenvoieObjet(c_dicReferences["personnels"], "clg_personnels");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listeplanning.Dictionnaire.Add(pln_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listeplanning.Dictionnaire.Remove(pln_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_calendrier != null)if(!c_calendrier.Listeplanning.Contains(this)) c_calendrier.Listeplanning.Add(this);
		if(c_personnels != null)if(!c_personnels.Listeplanning.Contains(this)) c_personnels.Listeplanning.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_planning l_Clone = (clg_planning) Clone;
		if(l_Clone.calendrier != null)if(l_Clone.calendrier.Listeplanning.Contains(this)) l_Clone.calendrier.Listeplanning.Remove(this);
		if(l_Clone.personnels != null)if(l_Clone.personnels.Listeplanning.Contains(this)) l_Clone.personnels.Listeplanning.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_planning(null, pln_cn, calendrier, personnels, pln_h_deb_mat_pre, pln_h_fin_mat_pre, pln_h_deb_apr_pre, pln_h_fin_apr_pre, pln_h_deb_mat_rea, pln_h_fin_mat_rea, pln_h_deb_apr_rea, pln_h_fin_apr_rea, pln_h_trajet, pln_b_bloque, pln_b_bloque_real,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_planning l_clone = (clg_planning) this.Clone;
		c_pln_cn = l_clone.pln_cn;
		c_calendrier = l_clone.calendrier;
		c_personnels = l_clone.personnels;
		c_pln_h_deb_mat_pre = l_clone.pln_h_deb_mat_pre;
		c_pln_h_fin_mat_pre = l_clone.pln_h_fin_mat_pre;
		c_pln_h_deb_apr_pre = l_clone.pln_h_deb_apr_pre;
		c_pln_h_fin_apr_pre = l_clone.pln_h_fin_apr_pre;
		c_pln_h_deb_mat_rea = l_clone.pln_h_deb_mat_rea;
		c_pln_h_fin_mat_rea = l_clone.pln_h_fin_mat_rea;
		c_pln_h_deb_apr_rea = l_clone.pln_h_deb_apr_rea;
		c_pln_h_fin_apr_rea = l_clone.pln_h_fin_apr_rea;
		c_pln_h_trajet = l_clone.pln_h_trajet;
		c_pln_b_bloque = l_clone.pln_b_bloque;
		c_pln_b_bloque_real = l_clone.pln_b_bloque_real;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLplanning.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLplanning.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLplanning.Delete(this);
    }

    /* Accesseur de la propriete pln_cn (pln_cn)
    * @return c_pln_cn */
    public Int64 pln_cn
    {
        get{return c_pln_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_pln_cn != value)
                {
                    CreerClone();
                    c_pln_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete calendrier (pln_cal_d_date)
    * @return c_calendrier */
    public clg_calendrier calendrier
    {
        get{return c_calendrier;}
        set
        {
            if(value != null)
            {
                if(c_calendrier != value)
                {
                    CreerClone();
                    c_calendrier = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur nulle interdite");
            }
        }
    }
    /* Accesseur de la propriete personnels (pln_per_cn)
    * @return c_personnels */
    public clg_personnels personnels
    {
        get{return c_personnels;}
        set
        {
            if(c_personnels != value)
            {
                CreerClone();
                c_personnels = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_deb_mat_pre (pln_h_deb_mat_pre)
    * @return c_pln_h_deb_mat_pre */
    public DateTime pln_h_deb_mat_pre
    {
        get{return c_pln_h_deb_mat_pre;}
        set
        {
            if(c_pln_h_deb_mat_pre != value)
            {
                CreerClone();
                c_pln_h_deb_mat_pre = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_fin_mat_pre (pln_h_fin_mat_pre)
    * @return c_pln_h_fin_mat_pre */
    public DateTime pln_h_fin_mat_pre
    {
        get{return c_pln_h_fin_mat_pre;}
        set
        {
            if(c_pln_h_fin_mat_pre != value)
            {
                CreerClone();
                c_pln_h_fin_mat_pre = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_deb_apr_pre (pln_h_deb_apr_pre)
    * @return c_pln_h_deb_apr_pre */
    public DateTime pln_h_deb_apr_pre
    {
        get{return c_pln_h_deb_apr_pre;}
        set
        {
            if(c_pln_h_deb_apr_pre != value)
            {
                CreerClone();
                c_pln_h_deb_apr_pre = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_fin_apr_pre (pln_h_fin_apr_pre)
    * @return c_pln_h_fin_apr_pre */
    public DateTime pln_h_fin_apr_pre
    {
        get{return c_pln_h_fin_apr_pre;}
        set
        {
            if(c_pln_h_fin_apr_pre != value)
            {
                CreerClone();
                c_pln_h_fin_apr_pre = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_deb_mat_rea (pln_h_deb_mat_rea)
    * @return c_pln_h_deb_mat_rea */
    public DateTime pln_h_deb_mat_rea
    {
        get{return c_pln_h_deb_mat_rea;}
        set
        {
            if(c_pln_h_deb_mat_rea != value)
            {
                CreerClone();
                c_pln_h_deb_mat_rea = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_fin_mat_rea (pln_h_fin_mat_rea)
    * @return c_pln_h_fin_mat_rea */
    public DateTime pln_h_fin_mat_rea
    {
        get{return c_pln_h_fin_mat_rea;}
        set
        {
            if(c_pln_h_fin_mat_rea != value)
            {
                CreerClone();
                c_pln_h_fin_mat_rea = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_deb_apr_rea (pln_h_deb_apr_rea)
    * @return c_pln_h_deb_apr_rea */
    public DateTime pln_h_deb_apr_rea
    {
        get{return c_pln_h_deb_apr_rea;}
        set
        {
            if(c_pln_h_deb_apr_rea != value)
            {
                CreerClone();
                c_pln_h_deb_apr_rea = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_fin_apr_rea (pln_h_fin_apr_rea)
    * @return c_pln_h_fin_apr_rea */
    public DateTime pln_h_fin_apr_rea
    {
        get{return c_pln_h_fin_apr_rea;}
        set
        {
            if(c_pln_h_fin_apr_rea != value)
            {
                CreerClone();
                c_pln_h_fin_apr_rea = value;
            }
        }
    }
    /* Accesseur de la propriete pln_h_trajet (pln_h_trajet)
    * @return c_pln_h_trajet */
    public DateTime pln_h_trajet
    {
        get{return c_pln_h_trajet;}
        set
        {
            if(c_pln_h_trajet != value)
            {
                CreerClone();
                c_pln_h_trajet = value;
            }
        }
    }
    /* Accesseur de la propriete pln_b_bloque (pln_b_bloque)
    * @return c_pln_b_bloque */
    public bool pln_b_bloque
    {
        get{return c_pln_b_bloque;}
        set
        {
            if(c_pln_b_bloque != value)
            {
                CreerClone();
                c_pln_b_bloque = value;
            }
        }
    }
    /* Accesseur de la propriete pln_b_bloque_real (pln_b_bloque_real)
    * @return c_pln_b_bloque_real */
    public bool pln_b_bloque_real
    {
        get{return c_pln_b_bloque_real;}
        set
        {
            if(c_pln_b_bloque_real != value)
            {
                CreerClone();
                c_pln_b_bloque_real = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_pln_cn.ToString());
		l_Valeurs.Add(c_calendrier==null ? "-1" : c_calendrier.ID.ToString());
		l_Valeurs.Add(c_personnels==null ? "-1" : c_personnels.ID.ToString());
		l_Valeurs.Add(c_pln_h_deb_mat_pre.ToString());
		l_Valeurs.Add(c_pln_h_fin_mat_pre.ToString());
		l_Valeurs.Add(c_pln_h_deb_apr_pre.ToString());
		l_Valeurs.Add(c_pln_h_fin_apr_pre.ToString());
		l_Valeurs.Add(c_pln_h_deb_mat_rea.ToString());
		l_Valeurs.Add(c_pln_h_fin_mat_rea.ToString());
		l_Valeurs.Add(c_pln_h_deb_apr_rea.ToString());
		l_Valeurs.Add(c_pln_h_fin_apr_rea.ToString());
		l_Valeurs.Add(c_pln_h_trajet.ToString());
		l_Valeurs.Add(c_pln_b_bloque.ToString());
		l_Valeurs.Add(c_pln_b_bloque_real.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("pln_cn");
		l_Noms.Add("calendrier");
		l_Noms.Add("personnels");
		l_Noms.Add("pln_h_deb_mat_pre");
		l_Noms.Add("pln_h_fin_mat_pre");
		l_Noms.Add("pln_h_deb_apr_pre");
		l_Noms.Add("pln_h_fin_apr_pre");
		l_Noms.Add("pln_h_deb_mat_rea");
		l_Noms.Add("pln_h_fin_mat_rea");
		l_Noms.Add("pln_h_deb_apr_rea");
		l_Noms.Add("pln_h_fin_apr_rea");
		l_Noms.Add("pln_h_trajet");
		l_Noms.Add("pln_b_bloque");
		l_Noms.Add("pln_b_bloque_real");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
