namespace dll_ServeurLogeproj_Core{
#region Code genere automatiquement par GenerateurDALModele
using System;
using clg_ReflexionV3_Core;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

/// <summary> Classe modele de la table : comptes_non_affichables </summary>
public partial class clg_comptes_non_affichables : clg_ObjetBase
{
    private static List<PropertyInfo> c_Proprietes = new List<PropertyInfo>();
    public static clg_ControleurBase c_CTLcomptes_non_affichables;
    protected clg_Modele c_Modele;

	private Int64 c_cna_cn;
	private string c_cna_a_num_compte;
	private clg_t_sens_comptes c_t_sens_comptes;
	private string c_cna_m_desc;


    private void Init()
    {

    }

    public override void Detruit()
    {
		this.c_t_sens_comptes = null;

    }

    /// <summary> Constructeur de la classe utilisable pour les chargements depuis une BDD ou une chaine CSV </summary>
	public clg_comptes_non_affichables(clg_Modele pModele, Int64 pcna_cn, bool pAMAJ = false) : base(pModele, pcna_cn, pAMAJ)
    {
        Init();
        c_Modele = pModele;
    }

	/// <summary> Constructeur de la classe utilisable lorsque toutes les donnees sont chargees </summary>
	public clg_comptes_non_affichables(clg_Modele pModele, Int64 pcna_cn, string pcna_a_num_compte, clg_t_sens_comptes pt_sens_comptes, string pcna_m_desc, bool pAMAJ = true) : base(pModele, pcna_cn, pAMAJ)
    {
        c_Modele = pModele;
        Init();
		        if(pcna_cn != Int64.MinValue)
            c_cna_cn = pcna_cn;
        else
            throw new System.InvalidOperationException("MinValue interdite");
		        c_cna_a_num_compte = pcna_a_num_compte;
		        c_t_sens_comptes = pt_sens_comptes;
		        c_cna_m_desc = pcna_m_desc;

        if(pAMAJ) c_Modele.AjouteObjetAMAJ(this);
    }

	/// <summary> Initialisation des proprietes non referentes d'autres objets et stockage des ids des proprietes referentes d'autres objets </summary>
	public void Initialise(Int64 pcna_cn, string pcna_a_num_compte, Int64 pt_sens_comptes, string pcna_m_desc)
    {   
		        c_cna_cn = pcna_cn;
		        c_cna_a_num_compte = pcna_a_num_compte;
		c_dicReferences.Add("t_sens_comptes", pt_sens_comptes);
		        c_cna_m_desc = pcna_m_desc;

        base.Initialise(pcna_cn);
    }

	/// <summary> Accesseur vers le controleur de cette classe </summary>
	public override clg_ControleurBase Controleur
	{
		get { return c_CTLcomptes_non_affichables; }
	}

	/// <summary> Instanciation des proprietes referencant d'autres objets </summary>
	public override void CreeLiens()
	{
		c_t_sens_comptes = (clg_t_sens_comptes) c_ModeleBase.RenvoieObjet(c_dicReferences["t_sens_comptes"], "clg_t_sens_comptes");

	}

	/// <summary> Ajout de cet objet dans les listes generiques du modele </summary>
	public override void AjouteDansListe()
	{
        c_Modele.Listecomptes_non_affichables.Dictionnaire.Add(cna_cn, this);	
	}

	/// <summary> Suppression de cet objet dans les listes generiques du modele </summary>
	public override void SupprimeDansListe()
	{
        c_Modele.Listecomptes_non_affichables.Dictionnaire.Remove(cna_cn);

	}

	/// <summary> Ajout des references de cet objet dans les listes des objets dont il fait reference </summary>
	public override void CreeReferences()
	{
		if(c_t_sens_comptes != null)if(!c_t_sens_comptes.Listecomptes_non_affichables.Contains(this)) c_t_sens_comptes.Listecomptes_non_affichables.Add(this);

	}

	/// <summary> Suppression des references de cet objet dans les listes des objets dont il faisait reference </summary>
	public override void DetruitReferences()
	{
        if (Clone != null)
        {
            clg_comptes_non_affichables l_Clone = (clg_comptes_non_affichables) Clone;
		if(l_Clone.t_sens_comptes != null)if(l_Clone.t_sens_comptes.Listecomptes_non_affichables.Contains(this)) l_Clone.t_sens_comptes.Listecomptes_non_affichables.Remove(this);

        }
	}

	/// <summary> Analyse si l'objet est reference dans d'autres objets </summary>
	protected override void AnalyseReferences()
	{
        base.AnalyseReferences();

	}

	/// <summary> Creation d'un clone de l'objet </summary>
	protected override void CreerClone()
	{
		if (base.Clone == null)
		{
			base.Clone = Cloner();
			c_Modele.AjouteObjetAMAJ(this);
		}
	}

	/// <summary> Creation d'un clone de l'objet </summary>
	public override clg_ObjetBase Cloner()
	{
        return new clg_comptes_non_affichables(null, cna_cn, cna_a_num_compte, t_sens_comptes, cna_m_desc,  false);
	}

	/// <summary> Annulation des modifications sur l'objet </summary>
	public override void AnnuleModification()
	{
        clg_comptes_non_affichables l_clone = (clg_comptes_non_affichables) this.Clone;
		c_cna_cn = l_clone.cna_cn;
		c_cna_a_num_compte = l_clone.cna_a_num_compte;
		c_t_sens_comptes = l_clone.t_sens_comptes;
		c_cna_m_desc = l_clone.cna_m_desc;
		
	}

    public override void InsereEnBase()
    {
        base.InsereEnBase();
        c_CTLcomptes_non_affichables.Insert(this);
    }

    public override void MAJEnBase()
    {
        base.MAJEnBase();
        c_CTLcomptes_non_affichables.Update(this);
    }

    public override void SupprimeEnBase()
    {
        base.SupprimeEnBase();
        c_CTLcomptes_non_affichables.Delete(this);
    }

    /* Accesseur de la propriete cna_cn (cna_cn)
    * @return c_cna_cn */
    public Int64 cna_cn
    {
        get{return c_cna_cn;}
        set
        {
            if(value != Int64.MinValue)
            {
                if(c_cna_cn != value)
                {
                    CreerClone();
                    c_cna_cn = value;
                }
            }
            else
            {
                throw new System.InvalidOperationException("Valeur MinValue interdite");
            }
        }
    }
    /* Accesseur de la propriete cna_a_num_compte (cna_a_num_compte)
    * @return c_cna_a_num_compte */
    public string cna_a_num_compte
    {
        get{return c_cna_a_num_compte;}
        set
        {
            if(c_cna_a_num_compte != value)
            {
                CreerClone();
                c_cna_a_num_compte = value;
            }
        }
    }
    /* Accesseur de la propriete t_sens_comptes (cna_tsc_cn)
    * @return c_t_sens_comptes */
    public clg_t_sens_comptes t_sens_comptes
    {
        get{return c_t_sens_comptes;}
        set
        {
            if(c_t_sens_comptes != value)
            {
                CreerClone();
                c_t_sens_comptes = value;
            }
        }
    }
    /* Accesseur de la propriete cna_m_desc (cna_m_desc)
    * @return c_cna_m_desc */
    public string cna_m_desc
    {
        get{return c_cna_m_desc;}
        set
        {
            if(c_cna_m_desc != value)
            {
                CreerClone();
                c_cna_m_desc = value;
            }
        }
    }


	public override List<string> ListeValeursProprietes()
    {
         List<string> l_Valeurs = new List<string>();
		l_Valeurs.Add(c_cna_cn.ToString());
		l_Valeurs.Add(c_cna_a_num_compte.ToString());
		l_Valeurs.Add(c_t_sens_comptes==null ? "-1" : c_t_sens_comptes.ID.ToString());
		l_Valeurs.Add(c_cna_m_desc.ToString());

        l_Valeurs.AddRange(base.ListeValeursProprietes());
        return l_Valeurs;
    }
	
	public override List<string> ListeNomsProprietes()
    {
         List<string> l_Noms = new List<string>();
		l_Noms.Add("cna_cn");
		l_Noms.Add("cna_a_num_compte");
		l_Noms.Add("t_sens_comptes");
		l_Noms.Add("cna_m_desc");

        l_Noms.AddRange(base.ListeNomsProprietes());
        return l_Noms;
    }

#endregion
}
}
