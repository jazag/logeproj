namespace dll_ServeurLogeproj_Core
{
    #region Code genere automatiquement par GenerateurDALModele
    using clg_ReflexionV3_Core;
    using System.Collections.Generic;

    /// <summary> Classe du modele general des donnees </summary>
    public partial class clg_Modele : clg_ModeleBase
    {
        public bool c_EstServeur;

        clg_Liste<object, clg_ObjetBase> c_Listeoperations;
        clg_Liste<object, clg_ObjetBase> c_Listeannee_comptable;
        clg_Liste<object, clg_ObjetBase> c_Listefinanceurs;
        clg_Liste<object, clg_ObjetBase> c_Listecarac;
        clg_Liste<object, clg_ObjetBase> c_Listec_operation;
        clg_Liste<object, clg_ObjetBase> c_Listeassembly;
        clg_Liste<object, clg_ObjetBase> c_Listec_qualif_temps;
        clg_Liste<object, clg_ObjetBase> c_Listebasemae;
        clg_Liste<object, clg_ObjetBase> c_Listecalendrier;
        clg_Liste<object, clg_ObjetBase> c_Listeelements;
        clg_Liste<object, clg_ObjetBase> c_Listeannee_civile;
        clg_Liste<object, clg_ObjetBase> c_Listet_lien_sage;
        clg_Liste<object, clg_ObjetBase> c_Listecarac_element;
        clg_Liste<object, clg_ObjetBase> c_Listelien_sage;
        clg_Liste<object, clg_ObjetBase> c_Listefinancement_prevus;
        clg_Liste<object, clg_ObjetBase> c_Listecomment;
        clg_Liste<object, clg_ObjetBase> c_Listecategorie_frais;
        clg_Liste<object, clg_ObjetBase> c_Listecomment_element_bis;
        clg_Liste<object, clg_ObjetBase> c_Listeconge_prevu;
        clg_Liste<object, clg_ObjetBase> c_Listecomptes_non_affichables;
        clg_Liste<object, clg_ObjetBase> c_Listecomptes_financeur;
        clg_Liste<object, clg_ObjetBase> c_Listedemandes_subventions;
        clg_Liste<object, clg_ObjetBase> c_Listeconge_realise;
        clg_Liste<object, clg_ObjetBase> c_Listeconcepteurs;
        clg_Liste<object, clg_ObjetBase> c_Listeconvention;
        clg_Liste<object, clg_ObjetBase> c_Listedeplacements_prevus;
        clg_Liste<object, clg_ObjetBase> c_Listecpt_ana_exclu;
        clg_Liste<object, clg_ObjetBase> c_Listeetat_arrete;
        clg_Liste<object, clg_ObjetBase> c_Listecontacts;
        clg_Liste<object, clg_ObjetBase> c_Listeelements_bis;
        clg_Liste<object, clg_ObjetBase> c_Listedeplacements_realises;
        clg_Liste<object, clg_ObjetBase> c_Listeetats;
        clg_Liste<object, clg_ObjetBase> c_Listecorbeille_deplacement;
        clg_Liste<object, clg_ObjetBase> c_Listefacture;
        clg_Liste<object, clg_ObjetBase> c_Listefiltre_car_perso;
        clg_Liste<object, clg_ObjetBase> c_Listeformat_carac;
        clg_Liste<object, clg_ObjetBase> c_Listegroupe_metaprojet;
        clg_Liste<object, clg_ObjetBase> c_Listeinfos_base;
        clg_Liste<object, clg_ObjetBase> c_Listefournisseurs;
        clg_Liste<object, clg_ObjetBase> c_Listefrais_mission;
        clg_Liste<object, clg_ObjetBase> c_Listeforfait;
        clg_Liste<object, clg_ObjetBase> c_Listegroupes;
        clg_Liste<object, clg_ObjetBase> c_Listeheures_sup_prevues;
        clg_Liste<object, clg_ObjetBase> c_Listeimport_sage;
        clg_Liste<object, clg_ObjetBase> c_Listefonctions_perso;
        clg_Liste<object, clg_ObjetBase> c_Listeinfos_conservatoire;
        clg_Liste<object, clg_ObjetBase> c_Listefrais_generaux;
        clg_Liste<object, clg_ObjetBase> c_Listelimitation_donnees;
        clg_Liste<object, clg_ObjetBase> c_Listejours;
        clg_Liste<object, clg_ObjetBase> c_Listeinit_appli;
        clg_Liste<object, clg_ObjetBase> c_Listejours_feries;
        clg_Liste<object, clg_ObjetBase> c_Listelien_arretes;
        clg_Liste<object, clg_ObjetBase> c_Listelimitation_fenetre;
        clg_Liste<object, clg_ObjetBase> c_Listelimitation_interface;
        clg_Liste<object, clg_ObjetBase> c_Listejournal;
        clg_Liste<object, clg_ObjetBase> c_Listemensuel;
        clg_Liste<object, clg_ObjetBase> c_Listeintervenants;
        clg_Liste<object, clg_ObjetBase> c_Listemessage_lectureseule;
        clg_Liste<object, clg_ObjetBase> c_Listeprojets;
        clg_Liste<object, clg_ObjetBase> c_Listepersonnels;
        clg_Liste<object, clg_ObjetBase> c_Listemois;
        clg_Liste<object, clg_ObjetBase> c_Listeparam_temps_travail;
        clg_Liste<object, clg_ObjetBase> c_Listeparam_compte;
        clg_Liste<object, clg_ObjetBase> c_Listeperiode_planning;
        clg_Liste<object, clg_ObjetBase> c_Listeparametrage;
        clg_Liste<object, clg_ObjetBase> c_Listeposte;
        clg_Liste<object, clg_ObjetBase> c_Listeplugin;
        clg_Liste<object, clg_ObjetBase> c_Listeplanning;
        clg_Liste<object, clg_ObjetBase> c_Listeparam_conservatoire;
        clg_Liste<object, clg_ObjetBase> c_Listeprojets_bis;
        clg_Liste<object, clg_ObjetBase> c_Listeprestation_prevus;
        clg_Liste<object, clg_ObjetBase> c_Listereajustement_fonds_dedies;
        clg_Liste<object, clg_ObjetBase> c_Listeregle;
        clg_Liste<object, clg_ObjetBase> c_Listetemps_realises;
        clg_Liste<object, clg_ObjetBase> c_Listetemps_intervenants;
        clg_Liste<object, clg_ObjetBase> c_Listetaux_km_reels;
        clg_Liste<object, clg_ObjetBase> c_Listetaux;
        clg_Liste<object, clg_ObjetBase> c_Listereport;
        clg_Liste<object, clg_ObjetBase> c_Listeventilation_deplacement;
        clg_Liste<object, clg_ObjetBase> c_Listeventilation_deplacement_csa;
        clg_Liste<object, clg_ObjetBase> c_Listerubriques_temps;
        clg_Liste<object, clg_ObjetBase> c_Listetaux_km_prevus;
        clg_Liste<object, clg_ObjetBase> c_Listet_base;
        clg_Liste<object, clg_ObjetBase> c_Listesolde_conges;
        clg_Liste<object, clg_ObjetBase> c_Listet_prestation;
        clg_Liste<object, clg_ObjetBase> c_Listet_fonctions;
        clg_Liste<object, clg_ObjetBase> c_Listesynchro_connect_base;
        clg_Liste<object, clg_ObjetBase> c_Listet_element;
        clg_Liste<object, clg_ObjetBase> c_Listet_compte;
        clg_Liste<object, clg_ObjetBase> c_Listet_conge;
        clg_Liste<object, clg_ObjetBase> c_Listet_cout_projet;
        clg_Liste<object, clg_ObjetBase> c_Listet_init_appli;
        clg_Liste<object, clg_ObjetBase> c_Listet_fonction_contact;
        clg_Liste<object, clg_ObjetBase> c_Listet_info;
        clg_Liste<object, clg_ObjetBase> c_Listet_limitation_donnees;
        clg_Liste<object, clg_ObjetBase> c_Listet_journaux;
        clg_Liste<object, clg_ObjetBase> c_Listet_limitation_interface;
        clg_Liste<object, clg_ObjetBase> c_Listet_ope_financiere;
        clg_Liste<object, clg_ObjetBase> c_Listet_etat;
        clg_Liste<object, clg_ObjetBase> c_Listet_parametre;
        clg_Liste<object, clg_ObjetBase> c_Listet_projet;
        clg_Liste<object, clg_ObjetBase> c_Listet_rubriques_temps;
        clg_Liste<object, clg_ObjetBase> c_Listet_sens_comptes;
        clg_Liste<object, clg_ObjetBase> c_Listetype_fonct_compta;
        clg_Liste<object, clg_ObjetBase> c_Listetype_base_mae;
        clg_Liste<object, clg_ObjetBase> c_Listet_objets_interface;
        clg_Liste<object, clg_ObjetBase> c_Listevaleur_param;
        clg_Liste<object, clg_ObjetBase> c_Listeutilisateur;
        clg_Liste<object, clg_ObjetBase> c_Listevaleur_carac;
        clg_Liste<object, clg_ObjetBase> c_Listevehicules;
        clg_Liste<object, clg_ObjetBase> c_Listetemps_perso_generaux;
        clg_Liste<object, clg_ObjetBase> c_Listetaux_fin;
        clg_Liste<object, clg_ObjetBase> c_Listevalidation_temps_perso;
        clg_Liste<object, clg_ObjetBase> c_Listet_maj;
        clg_Liste<object, clg_ObjetBase> c_Listevalidation_temps_semaine;
        clg_Liste<object, clg_ObjetBase> c_Listetemps_travail_annuel;
        clg_Liste<object, clg_ObjetBase> c_Listeconvention_regle;
        clg_Liste<object, clg_ObjetBase> c_Listeobjets_interface;
        clg_Liste<object, clg_ObjetBase> c_Listetb_sage;
        clg_Liste<object, clg_ObjetBase> c_Listecomment_demandes;
        clg_Liste<object, clg_ObjetBase> c_Listefenetre;
        clg_Liste<object, clg_ObjetBase> c_Listearretes;
        clg_Liste<object, clg_ObjetBase> c_Listeforfait_personnel;



        /// <summary> Methode d'instanciation des listes d'objets </summary>
        public void InitListes(bool pEstServeur)
        {
            c_EstServeur = pEstServeur;

            c_Listeoperations = new clg_Liste<object, clg_ObjetBase>();
            c_Listeannee_comptable = new clg_Liste<object, clg_ObjetBase>();
            c_Listefinanceurs = new clg_Liste<object, clg_ObjetBase>();
            c_Listecarac = new clg_Liste<object, clg_ObjetBase>();
            c_Listec_operation = new clg_Liste<object, clg_ObjetBase>();
            c_Listeassembly = new clg_Liste<object, clg_ObjetBase>();
            c_Listec_qualif_temps = new clg_Liste<object, clg_ObjetBase>();
            c_Listebasemae = new clg_Liste<object, clg_ObjetBase>();
            c_Listecalendrier = new clg_Liste<object, clg_ObjetBase>();
            c_Listeelements = new clg_Liste<object, clg_ObjetBase>();
            c_Listeannee_civile = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_lien_sage = new clg_Liste<object, clg_ObjetBase>();
            c_Listecarac_element = new clg_Liste<object, clg_ObjetBase>();
            c_Listelien_sage = new clg_Liste<object, clg_ObjetBase>();
            c_Listefinancement_prevus = new clg_Liste<object, clg_ObjetBase>();
            c_Listecomment = new clg_Liste<object, clg_ObjetBase>();
            c_Listecategorie_frais = new clg_Liste<object, clg_ObjetBase>();
            c_Listecomment_element_bis = new clg_Liste<object, clg_ObjetBase>();
            c_Listeconge_prevu = new clg_Liste<object, clg_ObjetBase>();
            c_Listecomptes_non_affichables = new clg_Liste<object, clg_ObjetBase>();
            c_Listecomptes_financeur = new clg_Liste<object, clg_ObjetBase>();
            c_Listedemandes_subventions = new clg_Liste<object, clg_ObjetBase>();
            c_Listeconge_realise = new clg_Liste<object, clg_ObjetBase>();
            c_Listeconcepteurs = new clg_Liste<object, clg_ObjetBase>();
            c_Listeconvention = new clg_Liste<object, clg_ObjetBase>();
            c_Listedeplacements_prevus = new clg_Liste<object, clg_ObjetBase>();
            c_Listecpt_ana_exclu = new clg_Liste<object, clg_ObjetBase>();
            c_Listeetat_arrete = new clg_Liste<object, clg_ObjetBase>();
            c_Listecontacts = new clg_Liste<object, clg_ObjetBase>();
            c_Listeelements_bis = new clg_Liste<object, clg_ObjetBase>();
            c_Listedeplacements_realises = new clg_Liste<object, clg_ObjetBase>();
            c_Listeetats = new clg_Liste<object, clg_ObjetBase>();
            c_Listecorbeille_deplacement = new clg_Liste<object, clg_ObjetBase>();
            c_Listefacture = new clg_Liste<object, clg_ObjetBase>();
            c_Listefiltre_car_perso = new clg_Liste<object, clg_ObjetBase>();
            c_Listeformat_carac = new clg_Liste<object, clg_ObjetBase>();
            c_Listegroupe_metaprojet = new clg_Liste<object, clg_ObjetBase>();
            c_Listeinfos_base = new clg_Liste<object, clg_ObjetBase>();
            c_Listefournisseurs = new clg_Liste<object, clg_ObjetBase>();
            c_Listefrais_mission = new clg_Liste<object, clg_ObjetBase>();
            c_Listeforfait = new clg_Liste<object, clg_ObjetBase>();
            c_Listegroupes = new clg_Liste<object, clg_ObjetBase>();
            c_Listeheures_sup_prevues = new clg_Liste<object, clg_ObjetBase>();
            c_Listeimport_sage = new clg_Liste<object, clg_ObjetBase>();
            c_Listefonctions_perso = new clg_Liste<object, clg_ObjetBase>();
            c_Listeinfos_conservatoire = new clg_Liste<object, clg_ObjetBase>();
            c_Listefrais_generaux = new clg_Liste<object, clg_ObjetBase>();
            c_Listelimitation_donnees = new clg_Liste<object, clg_ObjetBase>();
            c_Listejours = new clg_Liste<object, clg_ObjetBase>();
            c_Listeinit_appli = new clg_Liste<object, clg_ObjetBase>();
            c_Listejours_feries = new clg_Liste<object, clg_ObjetBase>();
            c_Listelien_arretes = new clg_Liste<object, clg_ObjetBase>();
            c_Listelimitation_fenetre = new clg_Liste<object, clg_ObjetBase>();
            c_Listelimitation_interface = new clg_Liste<object, clg_ObjetBase>();
            c_Listejournal = new clg_Liste<object, clg_ObjetBase>();
            c_Listemensuel = new clg_Liste<object, clg_ObjetBase>();
            c_Listeintervenants = new clg_Liste<object, clg_ObjetBase>();
            c_Listemessage_lectureseule = new clg_Liste<object, clg_ObjetBase>();
            c_Listeprojets = new clg_Liste<object, clg_ObjetBase>();
            c_Listepersonnels = new clg_Liste<object, clg_ObjetBase>();
            c_Listemois = new clg_Liste<object, clg_ObjetBase>();
            c_Listeparam_temps_travail = new clg_Liste<object, clg_ObjetBase>();
            c_Listeparam_compte = new clg_Liste<object, clg_ObjetBase>();
            c_Listeperiode_planning = new clg_Liste<object, clg_ObjetBase>();
            c_Listeparametrage = new clg_Liste<object, clg_ObjetBase>();
            c_Listeposte = new clg_Liste<object, clg_ObjetBase>();
            c_Listeplugin = new clg_Liste<object, clg_ObjetBase>();
            c_Listeplanning = new clg_Liste<object, clg_ObjetBase>();
            c_Listeparam_conservatoire = new clg_Liste<object, clg_ObjetBase>();
            c_Listeprojets_bis = new clg_Liste<object, clg_ObjetBase>();
            c_Listeprestation_prevus = new clg_Liste<object, clg_ObjetBase>();
            c_Listereajustement_fonds_dedies = new clg_Liste<object, clg_ObjetBase>();
            c_Listeregle = new clg_Liste<object, clg_ObjetBase>();
            c_Listetemps_realises = new clg_Liste<object, clg_ObjetBase>();
            c_Listetemps_intervenants = new clg_Liste<object, clg_ObjetBase>();
            c_Listetaux_km_reels = new clg_Liste<object, clg_ObjetBase>();
            c_Listetaux = new clg_Liste<object, clg_ObjetBase>();
            c_Listereport = new clg_Liste<object, clg_ObjetBase>();
            c_Listeventilation_deplacement = new clg_Liste<object, clg_ObjetBase>();
            c_Listeventilation_deplacement_csa = new clg_Liste<object, clg_ObjetBase>();
            c_Listerubriques_temps = new clg_Liste<object, clg_ObjetBase>();
            c_Listetaux_km_prevus = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_base = new clg_Liste<object, clg_ObjetBase>();
            c_Listesolde_conges = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_prestation = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_fonctions = new clg_Liste<object, clg_ObjetBase>();
            c_Listesynchro_connect_base = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_element = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_compte = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_conge = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_cout_projet = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_init_appli = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_fonction_contact = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_info = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_limitation_donnees = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_journaux = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_limitation_interface = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_ope_financiere = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_etat = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_parametre = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_projet = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_rubriques_temps = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_sens_comptes = new clg_Liste<object, clg_ObjetBase>();
            c_Listetype_fonct_compta = new clg_Liste<object, clg_ObjetBase>();
            c_Listetype_base_mae = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_objets_interface = new clg_Liste<object, clg_ObjetBase>();
            c_Listevaleur_param = new clg_Liste<object, clg_ObjetBase>();
            c_Listeutilisateur = new clg_Liste<object, clg_ObjetBase>();
            c_Listevaleur_carac = new clg_Liste<object, clg_ObjetBase>();
            c_Listevehicules = new clg_Liste<object, clg_ObjetBase>();
            c_Listetemps_perso_generaux = new clg_Liste<object, clg_ObjetBase>();
            c_Listetaux_fin = new clg_Liste<object, clg_ObjetBase>();
            c_Listevalidation_temps_perso = new clg_Liste<object, clg_ObjetBase>();
            c_Listet_maj = new clg_Liste<object, clg_ObjetBase>();
            c_Listevalidation_temps_semaine = new clg_Liste<object, clg_ObjetBase>();
            c_Listetemps_travail_annuel = new clg_Liste<object, clg_ObjetBase>();
            c_Listeconvention_regle = new clg_Liste<object, clg_ObjetBase>();
            c_Listeobjets_interface = new clg_Liste<object, clg_ObjetBase>();
            c_Listetb_sage = new clg_Liste<object, clg_ObjetBase>();
            c_Listecomment_demandes = new clg_Liste<object, clg_ObjetBase>();
            c_Listefenetre = new clg_Liste<object, clg_ObjetBase>();
            c_Listearretes = new clg_Liste<object, clg_ObjetBase>();
            c_Listeforfait_personnel = new clg_Liste<object, clg_ObjetBase>();


        }

        public override Dictionary<object, clg_ObjetBase> ListeObjetsParClasse(string pNomClasse)
        {
            switch (pNomClasse)
            {
                case "clg_operations": return c_Listeoperations.Dictionnaire;
                case "clg_annee_comptable": return c_Listeannee_comptable.Dictionnaire;
                case "clg_financeurs": return c_Listefinanceurs.Dictionnaire;
                case "clg_carac": return c_Listecarac.Dictionnaire;
                case "clg_c_operation": return c_Listec_operation.Dictionnaire;
                case "clg_assembly": return c_Listeassembly.Dictionnaire;
                case "clg_c_qualif_temps": return c_Listec_qualif_temps.Dictionnaire;
                case "clg_basemae": return c_Listebasemae.Dictionnaire;
                case "clg_calendrier": return c_Listecalendrier.Dictionnaire;
                case "clg_elements": return c_Listeelements.Dictionnaire;
                case "clg_annee_civile": return c_Listeannee_civile.Dictionnaire;
                case "clg_t_lien_sage": return c_Listet_lien_sage.Dictionnaire;
                case "clg_carac_element": return c_Listecarac_element.Dictionnaire;
                case "clg_lien_sage": return c_Listelien_sage.Dictionnaire;
                case "clg_financement_prevus": return c_Listefinancement_prevus.Dictionnaire;
                case "clg_comment": return c_Listecomment.Dictionnaire;
                case "clg_categorie_frais": return c_Listecategorie_frais.Dictionnaire;
                case "clg_comment_element_bis": return c_Listecomment_element_bis.Dictionnaire;
                case "clg_conge_prevu": return c_Listeconge_prevu.Dictionnaire;
                case "clg_comptes_non_affichables": return c_Listecomptes_non_affichables.Dictionnaire;
                case "clg_comptes_financeur": return c_Listecomptes_financeur.Dictionnaire;
                case "clg_demandes_subventions": return c_Listedemandes_subventions.Dictionnaire;
                case "clg_conge_realise": return c_Listeconge_realise.Dictionnaire;
                case "clg_concepteurs": return c_Listeconcepteurs.Dictionnaire;
                case "clg_convention": return c_Listeconvention.Dictionnaire;
                case "clg_deplacements_prevus": return c_Listedeplacements_prevus.Dictionnaire;
                case "clg_cpt_ana_exclu": return c_Listecpt_ana_exclu.Dictionnaire;
                case "clg_etat_arrete": return c_Listeetat_arrete.Dictionnaire;
                case "clg_contacts": return c_Listecontacts.Dictionnaire;
                case "clg_elements_bis": return c_Listeelements_bis.Dictionnaire;
                case "clg_deplacements_realises": return c_Listedeplacements_realises.Dictionnaire;
                case "clg_etats": return c_Listeetats.Dictionnaire;
                case "clg_corbeille_deplacement": return c_Listecorbeille_deplacement.Dictionnaire;
                case "clg_facture": return c_Listefacture.Dictionnaire;
                case "clg_filtre_car_perso": return c_Listefiltre_car_perso.Dictionnaire;
                case "clg_format_carac": return c_Listeformat_carac.Dictionnaire;
                case "clg_groupe_metaprojet": return c_Listegroupe_metaprojet.Dictionnaire;
                case "clg_infos_base": return c_Listeinfos_base.Dictionnaire;
                case "clg_fournisseurs": return c_Listefournisseurs.Dictionnaire;
                case "clg_frais_mission": return c_Listefrais_mission.Dictionnaire;
                case "clg_forfait": return c_Listeforfait.Dictionnaire;
                case "clg_groupes": return c_Listegroupes.Dictionnaire;
                case "clg_heures_sup_prevues": return c_Listeheures_sup_prevues.Dictionnaire;
                case "clg_import_sage": return c_Listeimport_sage.Dictionnaire;
                case "clg_fonctions_perso": return c_Listefonctions_perso.Dictionnaire;
                case "clg_infos_conservatoire": return c_Listeinfos_conservatoire.Dictionnaire;
                case "clg_frais_generaux": return c_Listefrais_generaux.Dictionnaire;
                case "clg_limitation_donnees": return c_Listelimitation_donnees.Dictionnaire;
                case "clg_jours": return c_Listejours.Dictionnaire;
                case "clg_init_appli": return c_Listeinit_appli.Dictionnaire;
                case "clg_jours_feries": return c_Listejours_feries.Dictionnaire;
                case "clg_lien_arretes": return c_Listelien_arretes.Dictionnaire;
                case "clg_limitation_fenetre": return c_Listelimitation_fenetre.Dictionnaire;
                case "clg_limitation_interface": return c_Listelimitation_interface.Dictionnaire;
                case "clg_journal": return c_Listejournal.Dictionnaire;
                case "clg_mensuel": return c_Listemensuel.Dictionnaire;
                case "clg_intervenants": return c_Listeintervenants.Dictionnaire;
                case "clg_message_lectureseule": return c_Listemessage_lectureseule.Dictionnaire;
                case "clg_projets": return c_Listeprojets.Dictionnaire;
                case "clg_personnels": return c_Listepersonnels.Dictionnaire;
                case "clg_mois": return c_Listemois.Dictionnaire;
                case "clg_param_temps_travail": return c_Listeparam_temps_travail.Dictionnaire;
                case "clg_param_compte": return c_Listeparam_compte.Dictionnaire;
                case "clg_periode_planning": return c_Listeperiode_planning.Dictionnaire;
                case "clg_parametrage": return c_Listeparametrage.Dictionnaire;
                case "clg_poste": return c_Listeposte.Dictionnaire;
                case "clg_plugin": return c_Listeplugin.Dictionnaire;
                case "clg_planning": return c_Listeplanning.Dictionnaire;
                case "clg_param_conservatoire": return c_Listeparam_conservatoire.Dictionnaire;
                case "clg_projets_bis": return c_Listeprojets_bis.Dictionnaire;
                case "clg_prestation_prevus": return c_Listeprestation_prevus.Dictionnaire;
                case "clg_reajustement_fonds_dedies": return c_Listereajustement_fonds_dedies.Dictionnaire;
                case "clg_regle": return c_Listeregle.Dictionnaire;
                case "clg_temps_realises": return c_Listetemps_realises.Dictionnaire;
                case "clg_temps_intervenants": return c_Listetemps_intervenants.Dictionnaire;
                case "clg_taux_km_reels": return c_Listetaux_km_reels.Dictionnaire;
                case "clg_taux": return c_Listetaux.Dictionnaire;
                case "clg_report": return c_Listereport.Dictionnaire;
                case "clg_ventilation_deplacement": return c_Listeventilation_deplacement.Dictionnaire;
                case "clg_ventilation_deplacement_csa": return c_Listeventilation_deplacement_csa.Dictionnaire;
                case "clg_rubriques_temps": return c_Listerubriques_temps.Dictionnaire;
                case "clg_taux_km_prevus": return c_Listetaux_km_prevus.Dictionnaire;
                case "clg_t_base": return c_Listet_base.Dictionnaire;
                case "clg_solde_conges": return c_Listesolde_conges.Dictionnaire;
                case "clg_t_prestation": return c_Listet_prestation.Dictionnaire;
                case "clg_t_fonctions": return c_Listet_fonctions.Dictionnaire;
                case "clg_synchro_connect_base": return c_Listesynchro_connect_base.Dictionnaire;
                case "clg_t_element": return c_Listet_element.Dictionnaire;
                case "clg_t_compte": return c_Listet_compte.Dictionnaire;
                case "clg_t_conge": return c_Listet_conge.Dictionnaire;
                case "clg_t_cout_projet": return c_Listet_cout_projet.Dictionnaire;
                case "clg_t_init_appli": return c_Listet_init_appli.Dictionnaire;
                case "clg_t_fonction_contact": return c_Listet_fonction_contact.Dictionnaire;
                case "clg_t_info": return c_Listet_info.Dictionnaire;
                case "clg_t_limitation_donnees": return c_Listet_limitation_donnees.Dictionnaire;
                case "clg_t_journaux": return c_Listet_journaux.Dictionnaire;
                case "clg_t_limitation_interface": return c_Listet_limitation_interface.Dictionnaire;
                case "clg_t_ope_financiere": return c_Listet_ope_financiere.Dictionnaire;
                case "clg_t_etat": return c_Listet_etat.Dictionnaire;
                case "clg_t_parametre": return c_Listet_parametre.Dictionnaire;
                case "clg_t_projet": return c_Listet_projet.Dictionnaire;
                case "clg_t_rubriques_temps": return c_Listet_rubriques_temps.Dictionnaire;
                case "clg_t_sens_comptes": return c_Listet_sens_comptes.Dictionnaire;
                case "clg_type_fonct_compta": return c_Listetype_fonct_compta.Dictionnaire;
                case "clg_type_base_mae": return c_Listetype_base_mae.Dictionnaire;
                case "clg_t_objets_interface": return c_Listet_objets_interface.Dictionnaire;
                case "clg_valeur_param": return c_Listevaleur_param.Dictionnaire;
                case "clg_utilisateur": return c_Listeutilisateur.Dictionnaire;
                case "clg_valeur_carac": return c_Listevaleur_carac.Dictionnaire;
                case "clg_vehicules": return c_Listevehicules.Dictionnaire;
                case "clg_temps_perso_generaux": return c_Listetemps_perso_generaux.Dictionnaire;
                case "clg_taux_fin": return c_Listetaux_fin.Dictionnaire;
                case "clg_validation_temps_perso": return c_Listevalidation_temps_perso.Dictionnaire;
                case "clg_t_maj": return c_Listet_maj.Dictionnaire;
                case "clg_validation_temps_semaine": return c_Listevalidation_temps_semaine.Dictionnaire;
                case "clg_temps_travail_annuel": return c_Listetemps_travail_annuel.Dictionnaire;
                case "clg_convention_regle": return c_Listeconvention_regle.Dictionnaire;
                case "clg_objets_interface": return c_Listeobjets_interface.Dictionnaire;
                case "clg_tb_sage": return c_Listetb_sage.Dictionnaire;
                case "clg_comment_demandes": return c_Listecomment_demandes.Dictionnaire;
                case "clg_fenetre": return c_Listefenetre.Dictionnaire;
                case "clg_arretes": return c_Listearretes.Dictionnaire;
                case "clg_forfait_personnel": return c_Listeforfait_personnel.Dictionnaire;


                default:
                    return null;
            }
        }

        /*  Accesseur vers la liste des objets de type annee_comptable */
        public clg_Liste<object, clg_ObjetBase> Listeannee_comptable
        {
            get { return c_Listeannee_comptable; }
        }

        /*  Accesseur vers la liste des objets de type financeurs */
        public clg_Liste<object, clg_ObjetBase> Listefinanceurs
        {
            get { return c_Listefinanceurs; }
        }

        /*  Accesseur vers la liste des objets de type c_operation */
        public clg_Liste<object, clg_ObjetBase> Listec_operation
        {
            get { return c_Listec_operation; }
        }

        /*  Accesseur vers la liste des objets de type assembly */
        public clg_Liste<object, clg_ObjetBase> Listeassembly
        {
            get { return c_Listeassembly; }
        }

        /*  Accesseur vers la liste des objets de type c_qualif_temps */
        public clg_Liste<object, clg_ObjetBase> Listec_qualif_temps
        {
            get { return c_Listec_qualif_temps; }
        }

        /*  Accesseur vers la liste des objets de type annee_civile */
        public clg_Liste<object, clg_ObjetBase> Listeannee_civile
        {
            get { return c_Listeannee_civile; }
        }

        /*  Accesseur vers la liste des objets de type t_lien_sage */
        public clg_Liste<object, clg_ObjetBase> Listet_lien_sage
        {
            get { return c_Listet_lien_sage; }
        }

        /*  Accesseur vers la liste des objets de type comment */
        public clg_Liste<object, clg_ObjetBase> Listecomment
        {
            get { return c_Listecomment; }
        }

        /*  Accesseur vers la liste des objets de type categorie_frais */
        public clg_Liste<object, clg_ObjetBase> Listecategorie_frais
        {
            get { return c_Listecategorie_frais; }
        }

        /*  Accesseur vers la liste des objets de type comment_element_bis */
        public clg_Liste<object, clg_ObjetBase> Listecomment_element_bis
        {
            get { return c_Listecomment_element_bis; }
        }

        /*  Accesseur vers la liste des objets de type comptes_financeur */
        public clg_Liste<object, clg_ObjetBase> Listecomptes_financeur
        {
            get { return c_Listecomptes_financeur; }
        }

        /*  Accesseur vers la liste des objets de type demandes_subventions */
        public clg_Liste<object, clg_ObjetBase> Listedemandes_subventions
        {
            get { return c_Listedemandes_subventions; }
        }

        /*  Accesseur vers la liste des objets de type cpt_ana_exclu */
        public clg_Liste<object, clg_ObjetBase> Listecpt_ana_exclu
        {
            get { return c_Listecpt_ana_exclu; }
        }

        /*  Accesseur vers la liste des objets de type etat_arrete */
        public clg_Liste<object, clg_ObjetBase> Listeetat_arrete
        {
            get { return c_Listeetat_arrete; }
        }

        /*  Accesseur vers la liste des objets de type elements_bis */
        public clg_Liste<object, clg_ObjetBase> Listeelements_bis
        {
            get { return c_Listeelements_bis; }
        }

        /*  Accesseur vers la liste des objets de type filtre_car_perso */
        public clg_Liste<object, clg_ObjetBase> Listefiltre_car_perso
        {
            get { return c_Listefiltre_car_perso; }
        }

        /*  Accesseur vers la liste des objets de type format_carac */
        public clg_Liste<object, clg_ObjetBase> Listeformat_carac
        {
            get { return c_Listeformat_carac; }
        }

        /*  Accesseur vers la liste des objets de type groupe_metaprojet */
        public clg_Liste<object, clg_ObjetBase> Listegroupe_metaprojet
        {
            get { return c_Listegroupe_metaprojet; }
        }

        /*  Accesseur vers la liste des objets de type fournisseurs */
        public clg_Liste<object, clg_ObjetBase> Listefournisseurs
        {
            get { return c_Listefournisseurs; }
        }

        /*  Accesseur vers la liste des objets de type forfait */
        public clg_Liste<object, clg_ObjetBase> Listeforfait
        {
            get { return c_Listeforfait; }
        }

        /*  Accesseur vers la liste des objets de type groupes */
        public clg_Liste<object, clg_ObjetBase> Listegroupes
        {
            get { return c_Listegroupes; }
        }

        /*  Accesseur vers la liste des objets de type heures_sup_prevues */
        public clg_Liste<object, clg_ObjetBase> Listeheures_sup_prevues
        {
            get { return c_Listeheures_sup_prevues; }
        }

        /*  Accesseur vers la liste des objets de type import_sage */
        public clg_Liste<object, clg_ObjetBase> Listeimport_sage
        {
            get { return c_Listeimport_sage; }
        }

        /*  Accesseur vers la liste des objets de type infos_conservatoire */
        public clg_Liste<object, clg_ObjetBase> Listeinfos_conservatoire
        {
            get { return c_Listeinfos_conservatoire; }
        }

        /*  Accesseur vers la liste des objets de type frais_generaux */
        public clg_Liste<object, clg_ObjetBase> Listefrais_generaux
        {
            get { return c_Listefrais_generaux; }
        }

        /*  Accesseur vers la liste des objets de type jours */
        public clg_Liste<object, clg_ObjetBase> Listejours
        {
            get { return c_Listejours; }
        }

        /*  Accesseur vers la liste des objets de type jours_feries */
        public clg_Liste<object, clg_ObjetBase> Listejours_feries
        {
            get { return c_Listejours_feries; }
        }

        /*  Accesseur vers la liste des objets de type journal */
        public clg_Liste<object, clg_ObjetBase> Listejournal
        {
            get { return c_Listejournal; }
        }

        /*  Accesseur vers la liste des objets de type message_lectureseule */
        public clg_Liste<object, clg_ObjetBase> Listemessage_lectureseule
        {
            get { return c_Listemessage_lectureseule; }
        }

        /*  Accesseur vers la liste des objets de type personnels */
        public clg_Liste<object, clg_ObjetBase> Listepersonnels
        {
            get { return c_Listepersonnels; }
        }

        /*  Accesseur vers la liste des objets de type mois */
        public clg_Liste<object, clg_ObjetBase> Listemois
        {
            get { return c_Listemois; }
        }

        /*  Accesseur vers la liste des objets de type param_temps_travail */
        public clg_Liste<object, clg_ObjetBase> Listeparam_temps_travail
        {
            get { return c_Listeparam_temps_travail; }
        }

        /*  Accesseur vers la liste des objets de type periode_planning */
        public clg_Liste<object, clg_ObjetBase> Listeperiode_planning
        {
            get { return c_Listeperiode_planning; }
        }

        /*  Accesseur vers la liste des objets de type plugin */
        public clg_Liste<object, clg_ObjetBase> Listeplugin
        {
            get { return c_Listeplugin; }
        }

        /*  Accesseur vers la liste des objets de type projets_bis */
        public clg_Liste<object, clg_ObjetBase> Listeprojets_bis
        {
            get { return c_Listeprojets_bis; }
        }

        /*  Accesseur vers la liste des objets de type reajustement_fonds_dedies */
        public clg_Liste<object, clg_ObjetBase> Listereajustement_fonds_dedies
        {
            get { return c_Listereajustement_fonds_dedies; }
        }

        /*  Accesseur vers la liste des objets de type regle */
        public clg_Liste<object, clg_ObjetBase> Listeregle
        {
            get { return c_Listeregle; }
        }

        /*  Accesseur vers la liste des objets de type t_base */
        public clg_Liste<object, clg_ObjetBase> Listet_base
        {
            get { return c_Listet_base; }
        }

        /*  Accesseur vers la liste des objets de type solde_conges */
        public clg_Liste<object, clg_ObjetBase> Listesolde_conges
        {
            get { return c_Listesolde_conges; }
        }

        /*  Accesseur vers la liste des objets de type t_prestation */
        public clg_Liste<object, clg_ObjetBase> Listet_prestation
        {
            get { return c_Listet_prestation; }
        }

        /*  Accesseur vers la liste des objets de type t_fonctions */
        public clg_Liste<object, clg_ObjetBase> Listet_fonctions
        {
            get { return c_Listet_fonctions; }
        }

        /*  Accesseur vers la liste des objets de type synchro_connect_base */
        public clg_Liste<object, clg_ObjetBase> Listesynchro_connect_base
        {
            get { return c_Listesynchro_connect_base; }
        }

        /*  Accesseur vers la liste des objets de type t_element */
        public clg_Liste<object, clg_ObjetBase> Listet_element
        {
            get { return c_Listet_element; }
        }

        /*  Accesseur vers la liste des objets de type t_compte */
        public clg_Liste<object, clg_ObjetBase> Listet_compte
        {
            get { return c_Listet_compte; }
        }

        /*  Accesseur vers la liste des objets de type t_conge */
        public clg_Liste<object, clg_ObjetBase> Listet_conge
        {
            get { return c_Listet_conge; }
        }

        /*  Accesseur vers la liste des objets de type t_cout_projet */
        public clg_Liste<object, clg_ObjetBase> Listet_cout_projet
        {
            get { return c_Listet_cout_projet; }
        }

        /*  Accesseur vers la liste des objets de type t_init_appli */
        public clg_Liste<object, clg_ObjetBase> Listet_init_appli
        {
            get { return c_Listet_init_appli; }
        }

        /*  Accesseur vers la liste des objets de type t_fonction_contact */
        public clg_Liste<object, clg_ObjetBase> Listet_fonction_contact
        {
            get { return c_Listet_fonction_contact; }
        }

        /*  Accesseur vers la liste des objets de type t_info */
        public clg_Liste<object, clg_ObjetBase> Listet_info
        {
            get { return c_Listet_info; }
        }

        /*  Accesseur vers la liste des objets de type t_limitation_donnees */
        public clg_Liste<object, clg_ObjetBase> Listet_limitation_donnees
        {
            get { return c_Listet_limitation_donnees; }
        }

        /*  Accesseur vers la liste des objets de type t_journaux */
        public clg_Liste<object, clg_ObjetBase> Listet_journaux
        {
            get { return c_Listet_journaux; }
        }

        /*  Accesseur vers la liste des objets de type t_limitation_interface */
        public clg_Liste<object, clg_ObjetBase> Listet_limitation_interface
        {
            get { return c_Listet_limitation_interface; }
        }

        /*  Accesseur vers la liste des objets de type t_ope_financiere */
        public clg_Liste<object, clg_ObjetBase> Listet_ope_financiere
        {
            get { return c_Listet_ope_financiere; }
        }

        /*  Accesseur vers la liste des objets de type t_etat */
        public clg_Liste<object, clg_ObjetBase> Listet_etat
        {
            get { return c_Listet_etat; }
        }

        /*  Accesseur vers la liste des objets de type t_parametre */
        public clg_Liste<object, clg_ObjetBase> Listet_parametre
        {
            get { return c_Listet_parametre; }
        }

        /*  Accesseur vers la liste des objets de type t_projet */
        public clg_Liste<object, clg_ObjetBase> Listet_projet
        {
            get { return c_Listet_projet; }
        }

        /*  Accesseur vers la liste des objets de type t_rubriques_temps */
        public clg_Liste<object, clg_ObjetBase> Listet_rubriques_temps
        {
            get { return c_Listet_rubriques_temps; }
        }

        /*  Accesseur vers la liste des objets de type t_sens_comptes */
        public clg_Liste<object, clg_ObjetBase> Listet_sens_comptes
        {
            get { return c_Listet_sens_comptes; }
        }

        /*  Accesseur vers la liste des objets de type type_fonct_compta */
        public clg_Liste<object, clg_ObjetBase> Listetype_fonct_compta
        {
            get { return c_Listetype_fonct_compta; }
        }

        /*  Accesseur vers la liste des objets de type type_base_mae */
        public clg_Liste<object, clg_ObjetBase> Listetype_base_mae
        {
            get { return c_Listetype_base_mae; }
        }

        /*  Accesseur vers la liste des objets de type t_objets_interface */
        public clg_Liste<object, clg_ObjetBase> Listet_objets_interface
        {
            get { return c_Listet_objets_interface; }
        }

        /*  Accesseur vers la liste des objets de type utilisateur */
        public clg_Liste<object, clg_ObjetBase> Listeutilisateur
        {
            get { return c_Listeutilisateur; }
        }

        /*  Accesseur vers la liste des objets de type vehicules */
        public clg_Liste<object, clg_ObjetBase> Listevehicules
        {
            get { return c_Listevehicules; }
        }

        /*  Accesseur vers la liste des objets de type validation_temps_perso */
        public clg_Liste<object, clg_ObjetBase> Listevalidation_temps_perso
        {
            get { return c_Listevalidation_temps_perso; }
        }

        /*  Accesseur vers la liste des objets de type t_maj */
        public clg_Liste<object, clg_ObjetBase> Listet_maj
        {
            get { return c_Listet_maj; }
        }

        /*  Accesseur vers la liste des objets de type validation_temps_semaine */
        public clg_Liste<object, clg_ObjetBase> Listevalidation_temps_semaine
        {
            get { return c_Listevalidation_temps_semaine; }
        }

        /*  Accesseur vers la liste des objets de type temps_travail_annuel */
        public clg_Liste<object, clg_ObjetBase> Listetemps_travail_annuel
        {
            get { return c_Listetemps_travail_annuel; }
        }

        /*  Accesseur vers la liste des objets de type comment_demandes */
        public clg_Liste<object, clg_ObjetBase> Listecomment_demandes
        {
            get { return c_Listecomment_demandes; }
        }

        /*  Accesseur vers la liste des objets de type fenetre */
        public clg_Liste<object, clg_ObjetBase> Listefenetre
        {
            get { return c_Listefenetre; }
        }

        /*  Accesseur vers la liste des objets de type arretes */
        public clg_Liste<object, clg_ObjetBase> Listearretes
        {
            get { return c_Listearretes; }
        }

        /*  Accesseur vers la liste des objets de type forfait_personnel */
        public clg_Liste<object, clg_ObjetBase> Listeforfait_personnel
        {
            get { return c_Listeforfait_personnel; }
        }

        /*  Accesseur vers la liste des objets de type carac */
        public clg_Liste<object, clg_ObjetBase> Listecarac
        {
            get { return c_Listecarac; }
        }

        /*  Accesseur vers la liste des objets de type basemae */
        public clg_Liste<object, clg_ObjetBase> Listebasemae
        {
            get { return c_Listebasemae; }
        }

        /*  Accesseur vers la liste des objets de type comptes_non_affichables */
        public clg_Liste<object, clg_ObjetBase> Listecomptes_non_affichables
        {
            get { return c_Listecomptes_non_affichables; }
        }

        /*  Accesseur vers la liste des objets de type convention */
        public clg_Liste<object, clg_ObjetBase> Listeconvention
        {
            get { return c_Listeconvention; }
        }

        /*  Accesseur vers la liste des objets de type contacts */
        public clg_Liste<object, clg_ObjetBase> Listecontacts
        {
            get { return c_Listecontacts; }
        }

        /*  Accesseur vers la liste des objets de type etats */
        public clg_Liste<object, clg_ObjetBase> Listeetats
        {
            get { return c_Listeetats; }
        }

        /*  Accesseur vers la liste des objets de type infos_base */
        public clg_Liste<object, clg_ObjetBase> Listeinfos_base
        {
            get { return c_Listeinfos_base; }
        }

        /*  Accesseur vers la liste des objets de type fonctions_perso */
        public clg_Liste<object, clg_ObjetBase> Listefonctions_perso
        {
            get { return c_Listefonctions_perso; }
        }

        /*  Accesseur vers la liste des objets de type limitation_donnees */
        public clg_Liste<object, clg_ObjetBase> Listelimitation_donnees
        {
            get { return c_Listelimitation_donnees; }
        }

        /*  Accesseur vers la liste des objets de type init_appli */
        public clg_Liste<object, clg_ObjetBase> Listeinit_appli
        {
            get { return c_Listeinit_appli; }
        }

        /*  Accesseur vers la liste des objets de type limitation_fenetre */
        public clg_Liste<object, clg_ObjetBase> Listelimitation_fenetre
        {
            get { return c_Listelimitation_fenetre; }
        }

        /*  Accesseur vers la liste des objets de type mensuel */
        public clg_Liste<object, clg_ObjetBase> Listemensuel
        {
            get { return c_Listemensuel; }
        }

        /*  Accesseur vers la liste des objets de type param_compte */
        public clg_Liste<object, clg_ObjetBase> Listeparam_compte
        {
            get { return c_Listeparam_compte; }
        }

        /*  Accesseur vers la liste des objets de type parametrage */
        public clg_Liste<object, clg_ObjetBase> Listeparametrage
        {
            get { return c_Listeparametrage; }
        }

        /*  Accesseur vers la liste des objets de type poste */
        public clg_Liste<object, clg_ObjetBase> Listeposte
        {
            get { return c_Listeposte; }
        }

        /*  Accesseur vers la liste des objets de type param_conservatoire */
        public clg_Liste<object, clg_ObjetBase> Listeparam_conservatoire
        {
            get { return c_Listeparam_conservatoire; }
        }

        /*  Accesseur vers la liste des objets de type taux_km_reels */
        public clg_Liste<object, clg_ObjetBase> Listetaux_km_reels
        {
            get { return c_Listetaux_km_reels; }
        }

        /*  Accesseur vers la liste des objets de type rubriques_temps */
        public clg_Liste<object, clg_ObjetBase> Listerubriques_temps
        {
            get { return c_Listerubriques_temps; }
        }

        /*  Accesseur vers la liste des objets de type taux_km_prevus */
        public clg_Liste<object, clg_ObjetBase> Listetaux_km_prevus
        {
            get { return c_Listetaux_km_prevus; }
        }

        /*  Accesseur vers la liste des objets de type valeur_param */
        public clg_Liste<object, clg_ObjetBase> Listevaleur_param
        {
            get { return c_Listevaleur_param; }
        }

        /*  Accesseur vers la liste des objets de type valeur_carac */
        public clg_Liste<object, clg_ObjetBase> Listevaleur_carac
        {
            get { return c_Listevaleur_carac; }
        }

        /*  Accesseur vers la liste des objets de type temps_perso_generaux */
        public clg_Liste<object, clg_ObjetBase> Listetemps_perso_generaux
        {
            get { return c_Listetemps_perso_generaux; }
        }

        /*  Accesseur vers la liste des objets de type convention_regle */
        public clg_Liste<object, clg_ObjetBase> Listeconvention_regle
        {
            get { return c_Listeconvention_regle; }
        }

        /*  Accesseur vers la liste des objets de type tb_sage */
        public clg_Liste<object, clg_ObjetBase> Listetb_sage
        {
            get { return c_Listetb_sage; }
        }

        /*  Accesseur vers la liste des objets de type calendrier */
        public clg_Liste<object, clg_ObjetBase> Listecalendrier
        {
            get { return c_Listecalendrier; }
        }

        /*  Accesseur vers la liste des objets de type conge_prevu */
        public clg_Liste<object, clg_ObjetBase> Listeconge_prevu
        {
            get { return c_Listeconge_prevu; }
        }

        /*  Accesseur vers la liste des objets de type conge_realise */
        public clg_Liste<object, clg_ObjetBase> Listeconge_realise
        {
            get { return c_Listeconge_realise; }
        }

        /*  Accesseur vers la liste des objets de type deplacements_realises */
        public clg_Liste<object, clg_ObjetBase> Listedeplacements_realises
        {
            get { return c_Listedeplacements_realises; }
        }

        /*  Accesseur vers la liste des objets de type corbeille_deplacement */
        public clg_Liste<object, clg_ObjetBase> Listecorbeille_deplacement
        {
            get { return c_Listecorbeille_deplacement; }
        }

        /*  Accesseur vers la liste des objets de type frais_mission */
        public clg_Liste<object, clg_ObjetBase> Listefrais_mission
        {
            get { return c_Listefrais_mission; }
        }

        /*  Accesseur vers la liste des objets de type planning */
        public clg_Liste<object, clg_ObjetBase> Listeplanning
        {
            get { return c_Listeplanning; }
        }

        /*  Accesseur vers la liste des objets de type elements */
        public clg_Liste<object, clg_ObjetBase> Listeelements
        {
            get { return c_Listeelements; }
        }

        /*  Accesseur vers la liste des objets de type carac_element */
        public clg_Liste<object, clg_ObjetBase> Listecarac_element
        {
            get { return c_Listecarac_element; }
        }

        /*  Accesseur vers la liste des objets de type lien_sage */
        public clg_Liste<object, clg_ObjetBase> Listelien_sage
        {
            get { return c_Listelien_sage; }
        }

        /*  Accesseur vers la liste des objets de type financement_prevus */
        public clg_Liste<object, clg_ObjetBase> Listefinancement_prevus
        {
            get { return c_Listefinancement_prevus; }
        }

        /*  Accesseur vers la liste des objets de type concepteurs */
        public clg_Liste<object, clg_ObjetBase> Listeconcepteurs
        {
            get { return c_Listeconcepteurs; }
        }

        /*  Accesseur vers la liste des objets de type deplacements_prevus */
        public clg_Liste<object, clg_ObjetBase> Listedeplacements_prevus
        {
            get { return c_Listedeplacements_prevus; }
        }

        /*  Accesseur vers la liste des objets de type facture */
        public clg_Liste<object, clg_ObjetBase> Listefacture
        {
            get { return c_Listefacture; }
        }

        /*  Accesseur vers la liste des objets de type lien_arretes */
        public clg_Liste<object, clg_ObjetBase> Listelien_arretes
        {
            get { return c_Listelien_arretes; }
        }

        /*  Accesseur vers la liste des objets de type limitation_interface */
        public clg_Liste<object, clg_ObjetBase> Listelimitation_interface
        {
            get { return c_Listelimitation_interface; }
        }

        /*  Accesseur vers la liste des objets de type intervenants */
        public clg_Liste<object, clg_ObjetBase> Listeintervenants
        {
            get { return c_Listeintervenants; }
        }

        /*  Accesseur vers la liste des objets de type projets */
        public clg_Liste<object, clg_ObjetBase> Listeprojets
        {
            get { return c_Listeprojets; }
        }

        /*  Accesseur vers la liste des objets de type prestation_prevus */
        public clg_Liste<object, clg_ObjetBase> Listeprestation_prevus
        {
            get { return c_Listeprestation_prevus; }
        }

        /*  Accesseur vers la liste des objets de type temps_realises */
        public clg_Liste<object, clg_ObjetBase> Listetemps_realises
        {
            get { return c_Listetemps_realises; }
        }

        /*  Accesseur vers la liste des objets de type temps_intervenants */
        public clg_Liste<object, clg_ObjetBase> Listetemps_intervenants
        {
            get { return c_Listetemps_intervenants; }
        }

        /*  Accesseur vers la liste des objets de type taux */
        public clg_Liste<object, clg_ObjetBase> Listetaux
        {
            get { return c_Listetaux; }
        }

        /*  Accesseur vers la liste des objets de type report */
        public clg_Liste<object, clg_ObjetBase> Listereport
        {
            get { return c_Listereport; }
        }

        /*  Accesseur vers la liste des objets de type ventilation_deplacement */
        public clg_Liste<object, clg_ObjetBase> Listeventilation_deplacement
        {
            get { return c_Listeventilation_deplacement; }
        }

        /*  Accesseur vers la liste des objets de type ventilation_deplacement_csa */
        public clg_Liste<object, clg_ObjetBase> Listeventilation_deplacement_csa
        {
            get { return c_Listeventilation_deplacement_csa; }
        }

        /*  Accesseur vers la liste des objets de type taux_fin */
        public clg_Liste<object, clg_ObjetBase> Listetaux_fin
        {
            get { return c_Listetaux_fin; }
        }

        /*  Accesseur vers la liste des objets de type objets_interface */
        public clg_Liste<object, clg_ObjetBase> Listeobjets_interface
        {
            get { return c_Listeobjets_interface; }
        }

        /*  Accesseur vers la liste des objets de type operations */
        public clg_Liste<object, clg_ObjetBase> Listeoperations
        {
            get { return c_Listeoperations; }
        }


        #endregion
    }
}
