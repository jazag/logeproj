﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dll_ServeurLogeproj_Core;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace TableauDesCaracteristiques
{
    public class ChargerDonnees
    {
        private static long c_IdMax;

         static ChargerDonnees()
        {
            c_IdMax = IdMax();
        }

        public static Task HomeGet(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            return context.Response.SendFileAsync("htmlpage.html");
        }

        public static Task FiltreParProjet(HttpContext context)
        {
            Dictionary<Int64, clg_elements> ProjetCaracteristique = new Dictionary<Int64, clg_elements>();

            foreach (clg_carac_element c_Carac_Element in clg_ServeurLogeproj.Modele.Listecarac_element)
            {
                c_Carac_Element.appartientAuProjet = true;
                c_Carac_Element.elements.Okay = true;

                clg_elements racine = c_Carac_Element.elements.elements2;
                clg_elements elementActuel = c_Carac_Element.elements;
                while (elementActuel.ele_cn != racine.ele_cn)
                {
                    elementActuel.elements.Okay = true;
                    elementActuel = elementActuel.elements;
                }
                if (!ProjetCaracteristique.ContainsKey(elementActuel.ele_cn))
                {
                    ProjetCaracteristique.Add(elementActuel.ele_cn, elementActuel);
                }
            }

            StringBuilder JsonEnvoi = new StringBuilder();
            StringWriter l_sw = new StringWriter(JsonEnvoi);

            using (JsonWriter l_writer = new JsonTextWriter(l_sw))
            {
                l_writer.Formatting = Formatting.Indented;
                l_writer.WriteStartObject();

                l_writer.WritePropertyName("Elements");
                l_writer.WriteStartArray();
                foreach (clg_elements racine in ProjetCaracteristique.Values)
                {
                    l_writer.WriteRawValue(racine.ToJson());
                    foreach (clg_carac_element cCaracElem in racine.Listecarac_element)
                    {
                        if (cCaracElem.appartientAuProjet)
                        {
                            l_writer.WriteRawValue(cCaracElem.ToJson());
                            cCaracElem.appartientAuProjet = false;
                        }
                    }
                    WriteCaracs(racine, l_writer, 1);
                    break;
                }

                l_writer.WriteEndArray();

                l_writer.WriteEndObject();

                context.Response.ContentType = "application/json";
                return context.Response.WriteAsync(JsonEnvoi.ToString());
            }

        }

        public static void WriteCaracs(clg_elements elem, JsonWriter l_writer, int niveau)
        {
            foreach (clg_elements elemInf in elem.Listeelements)
            {
                if (elemInf.Okay)
                {
                    l_writer.WriteRawValue(elemInf.ToJson());
                    elemInf.Okay = false;
                    foreach (clg_carac_element cCaracElem in elemInf.Listecarac_element)
                    {
                        if (cCaracElem.appartientAuProjet)
                        {
                            l_writer.WriteRawValue(cCaracElem.ToJson());
                            cCaracElem.appartientAuProjet = false;
                        }
                    }
                    niveau += 1;
                    if (niveau < 5)
                    {
                        WriteCaracs(elemInf, l_writer, niveau);
                    }
                }
                niveau = 1;
            }
        }

        public static Task DetailCaracteristique(HttpContext context)
        {
            Int64 idCarac = int.Parse(context.Request.Query["id"]);
            clg_carac_element carac = null;

            foreach (clg_carac_element c_carac in clg_ServeurLogeproj.Modele.Listecarac_element)
            {
                if (c_carac.cel_cn == idCarac)
                {
                    carac = c_carac;
                    break;
                }
            }
            context.Response.ContentType = "application/json";
            return context.Response.WriteAsync(carac.ToJson());
        }

        public static Task RecupererListes(HttpContext context)
        {
            StringBuilder JsonEnvoi = new StringBuilder();
            StringWriter l_sw = new StringWriter(JsonEnvoi);

            using (JsonWriter l_writer = new JsonTextWriter(l_sw))
            {
                l_writer.Formatting = Formatting.Indented;
                l_writer.WriteStartObject();

                l_writer.WritePropertyName("ListeCarac");
                l_writer.WriteStartArray();


                foreach (clg_carac c_ListeCarac in clg_ServeurLogeproj.Modele.Listecarac)
                {
                    l_writer.WriteRawValue(c_ListeCarac.ToJson());
                }

                l_writer.WriteEndArray();

                l_writer.WritePropertyName("ListeValeurCarac");
                l_writer.WriteStartArray();

                foreach (clg_valeur_carac c_ListeValeurCarac in clg_ServeurLogeproj.Modele.Listevaleur_carac)
                {
                    l_writer.WriteRawValue(c_ListeValeurCarac.ToJson());
                }

                l_writer.WriteEndArray();


                l_writer.WriteEndObject();

            }

            context.Response.ContentType = "application/json";
            return context.Response.WriteAsync(JsonEnvoi.ToString());
        }

        public static Task CreerCaracElement(HttpContext context)
        {
            DateTime datemodifs;
            String idCarac = context.Request.Form["CaracId"];
            String idCaracteristiqueElement = context.Request.Form["idCaracteristiqueElement"];
            String idElement = context.Request.Form["idElement"];
            String libelleValeur = context.Request.Form["ValeurId"];
            String datemodif = context.Request.Form["DateModification"];
         
            Int64 IdCaracs = Int64.Parse(idCarac);
            Int64 IdEle = Int64.Parse(idElement);

            if(datemodif == "" || datemodif == null)
            {
                datemodifs = DateTime.Now;
            }
            else
            {
                datemodifs = DateTime.Parse(datemodif);
            }

            datemodifs = DateTime.Now;

            clg_carac_element l_Carac_Element = null;
            if (idCaracteristiqueElement != "")
            {

                Int64 idCaracEle = Int64.Parse(idCaracteristiqueElement);
                
                clg_fct_carac_element.Modifier(idCaracEle, IdEle, IdCaracs, libelleValeur, null, datemodifs);
                foreach (clg_carac_element c_CaracEle in clg_ServeurLogeproj.Modele.Listecarac_element)
                {
                    if (c_CaracEle.cel_cn == idCaracEle)
                    {
                        l_Carac_Element = c_CaracEle;
                        break;
                    }
                }
            }
            else
            {
                c_IdMax++;
                l_Carac_Element = clg_fct_carac_element.Ajouter(c_IdMax, IdEle, IdCaracs, libelleValeur, null, datemodifs);
            }
            context.Request.ContentType = "application/json charset=utf-8";
            return context.Response.WriteAsync(l_Carac_Element.ToJson());
        }

        public static Task SupprimerCaracElement(HttpContext context)
        {
            String idCaracteristiqueElement = context.Request.Form["idCaracteristiqueElementSupprime"];

            Int64 idCaracEle = Int64.Parse(idCaracteristiqueElement);

            clg_carac_element l_Carac_Element = null;

            l_Carac_Element = clg_fct_carac_element.Supprimer(idCaracEle);

            context.Response.ContentType = "text/plain";
            return context.Response.WriteAsync("ok");
        }

        public static long IdMax()
        {
            long l_IDMax = 0;
            foreach (clg_carac_element l_caracElement in clg_ServeurLogeproj.Modele.Listecarac_element)
            {
                if (l_caracElement.cel_cn > l_IDMax)
                {
                    l_IDMax = (long)l_caracElement.ID;
                }
            }
            return l_IDMax;
        }
    }
}
