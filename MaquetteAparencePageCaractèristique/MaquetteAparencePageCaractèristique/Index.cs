﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaquetteAparencePageCaractèristique
{
    public static class Index
    {
        public static Task HomeGet(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            return context.Response.SendFileAsync("SlickGrid2.html");
        }
    }
}
