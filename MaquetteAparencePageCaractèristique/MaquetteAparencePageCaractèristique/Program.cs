﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using dll_ServeurLogeproj_Core;

namespace MaquetteAparencePageCaractèristique
{
    public class Program
    {
        private static clg_ServeurLogeproj s_ServeurLogeproj;

        public static void Main(string[] args)
        {
            
            s_ServeurLogeproj = new clg_ServeurLogeproj("127.0.0.1", 5432, "LogProj", "postgres", "asse42800");
            s_ServeurLogeproj.Demarrer();
            TestCaracteristique.LancerTest();
            CreateWebHostBuilder(args).Build().Run();
            
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
